﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIVE
{
    public class fConditions
    {
        public int PerturbSetSize;
        public double FractionActive;
        public double FractionDeliveryDead;
        public int CellsTotal;
        public double MOI;
        public int RaftsTotal;
        public double FractionUnPickable;
        public double TopFractionCellsPick { get => TopFractionCellsPickList[0]; }

        public double SelectionPercentage = 0.95; //0 means no selection, 1 means perfect selection (selection means cells with no gRNAs go away)

        public double PerCellChanceOfCutting = 0.20; //Need both gRNA AND Cas9 to realize this amount, and cutting  (but gRNA is set, so this just combines change Cas9 * Chance cutting)
        public double AbilityToMeasurePhenotype = 0.7; //1 means the phenotype is measured perfectly . . tries to account for PCR errors, sequence errors, etc

        public List<double> TopFractionCellsPickList;
        public int MaxCellsPerRaftPick;
        public double PerturbsToPickFraction_AfterCells;
        public fPhenotype ActivePeturb;
        public fPhenotype SilentPerturb;

        public fConditions()
        {

        }

        public fConditions(int PerturbSetSize, double FractionActive, double FractionDeliveryDead, int CellsTotal, double MOI, int RaftsTotal, double FractionUnPickable, double TopFractionCellsPick, int MaxCellsPerRaftPick, double FractionGRNAsAfterCells)
        {
            Init(PerturbSetSize, FractionActive, FractionDeliveryDead, CellsTotal, MOI, RaftsTotal, FractionUnPickable, new List<double>(1) { TopFractionCellsPick }, MaxCellsPerRaftPick, FractionGRNAsAfterCells);
        }

        public fConditions(int PerturbSetSize, double FractionActive, double FractionDeliveryDead, int CellsTotal, double MOI, int RaftsTotal, double FractionUnPickable, List<double> TopFractionCellsPick, int MaxCellsPerRaftPick, double FractionGRNAsAfterCells)
        {
            Init(PerturbSetSize, FractionActive, FractionDeliveryDead, CellsTotal, MOI, RaftsTotal, FractionUnPickable, TopFractionCellsPick, MaxCellsPerRaftPick, FractionGRNAsAfterCells);
        }

        private void Init(int PerturbSetSize, double FractionActive, double FractionDeliveryDead, int CellsTotal, double MOI, int RaftsTotal, double FractionUnPickable, List<double> TopFractionCellsPick, int MaxCellsPerRaftPick, double FractionGRNAsAfterCells)
        {
            this.PerturbSetSize = PerturbSetSize;
            this.FractionActive = FractionActive;
            this.FractionDeliveryDead = FractionDeliveryDead;
            this.CellsTotal = CellsTotal;
            this.MOI = MOI;
            this.RaftsTotal = RaftsTotal;
            this.FractionUnPickable = FractionUnPickable;
            this.TopFractionCellsPickList = TopFractionCellsPick;
            this.MaxCellsPerRaftPick = MaxCellsPerRaftPick;
            this.PerturbsToPickFraction_AfterCells = FractionGRNAsAfterCells;
        }

        internal string Export(bool Headers)
        {
            if (Headers)
                return "PerturbSetSize" + "\t" + "FractionActive" + "\t" + "FractionDeliveryDead" + "\t" + "CellsTotal" + "\t" + "MOI" + "\t" + "RaftsTotal" + "\t" + "FractionUnPickable" + "\t" + "TopFractionCellsPick" + "\t" + "FractiongRNAsAfterCells" + "\t" + "MaxCellsPerRaftPick" + "\t" + "ProbCutting" + "\t" + "SurvivingInPositive" + "\t" + "SurvivingInNegative";
            else
                return PerturbSetSize + "\t" + FractionActive + "\t" + FractionDeliveryDead + "\t" + CellsTotal + "\t" + MOI + "\t" + RaftsTotal + "\t" + FractionUnPickable + "\t" + TopFractionCellsPick + "\t" + PerturbsToPickFraction_AfterCells + "\t" + MaxCellsPerRaftPick + "\t" + PerCellChanceOfCutting +"\t" + fPhenotype.ProbOfLifePositive +"\t" + fPhenotype.ProbOfLifeNegative;
        }

        internal fConditions Copy()
        {
            return (fConditions)this.MemberwiseClone();
        }
    }

    public class fResults
    {
        public fConditions Conditions;
        public SortedList<double, fPerturb> SeqList;
        public int CellsPickedRealized;
        public double FalsePositives;
        public double FalseNegatives;
        public double TruePositives;
        public double TrueNegatives;

        public double FalsePositives_Delivers;
        public double FalseNegatives_Delivers;
        public double TruePositives_Delivers;
        public double TrueNegatives_Delivers;

        public double SeqList_TP = -1;
        public double SeqList_FP = -1;
        public double Enrichment;

        private void SeqList_ReCalc()
        {
            SeqList_TP = 0; SeqList_FP = 0;
            int gRNAsToPick = (int)(Conditions.PerturbsToPickFraction_AfterCells * Conditions.PerturbSetSize);
            for (int i = 0; i < Math.Min(gRNAsToPick, SeqList.Count); i++)
            {
                if (SeqList.Values[i].Phenotype.State == 1) SeqList_TP++; else SeqList_FP++;
            }
        }

        public double Specificity { get => TrueNegatives / (TrueNegatives + FalsePositives); }
        public double Sensitivity { get => TruePositives / (TruePositives + FalseNegatives); }
        public double Accuracy { get => (TruePositives + TrueNegatives) / (TrueNegatives + TruePositives + FalsePositives + FalseNegatives); }

        public double SeqList_Spec
        {
            get
            {
                if (SeqList_TP == -1) SeqList_ReCalc();
                double SeqList_TN = TrueNegatives + (FalsePositives - SeqList_FP);
                return SeqList_TN / (SeqList_TN + SeqList_FP); ;
            }
        }

        public double SeqList_Sens
        {
            get
            {
                if (SeqList_TP == -1) SeqList_ReCalc();
                return SeqList_TP / (SeqList_TP + (FalseNegatives + (TruePositives - SeqList_TP)));
            }
        }

        public fResults(fConditions Cond)
        {
            Conditions = Cond;
        }
    }

    public class fTrial
    {
        public fResults Result { get => Results[0]; }
        public List<fResults> Results;
        public fConditions c;

        public fTrial(fConditions Conditions)
        {
            c = Conditions;
        }

        public void RunTrial_RaftSeq()
        {
            fPerturbSet PS = new fPerturbSet(c.PerturbSetSize, c.FractionActive);
            fDeliverSet DS = new fDeliverSet(PS, c.FractionDeliveryDead, 12); //Some of these will have nothing since they are "dead"
            fCellSet Cells = new fCellSet(c.CellsTotal, DS, c.MOI, c.SelectionPercentage, c.PerCellChanceOfCutting);

            Results = new List<fResults>(c.TopFractionCellsPickList.Count);

            fRaftSet Rafts = new fRaftSet(c.RaftsTotal, Cells, c.FractionUnPickable);

            foreach (double TopFractCells in c.TopFractionCellsPickList)
            {
                Results.Add(
                    Rafts.PickTop(TopFractCells, c.MaxCellsPerRaftPick, (int)(c.PerturbSetSize * c.FractionActive), (int)(c.PerturbSetSize - (c.PerturbSetSize * c.FractionActive)), c));
            }
        }

        public void RunTrial_Survival()
        {
            fPerturbSet PS = new fPerturbSet(c.PerturbSetSize, c.FractionActive);
            fDeliverSet DS = new fDeliverSet(PS, c.FractionDeliveryDead, 12); //Some of these will have nothing since they are "dead"
            fCellSet Cells = new fCellSet(c.CellsTotal, DS, c.MOI, c.SelectionPercentage, c.PerCellChanceOfCutting);

            Results = new List<fResults>();

            Results.Add(PickSurvival(c, PS, Cells));
        }

        public fResults PickSurvival(fConditions Conditions, fPerturbSet OriginalPerturbSet, fCellSet Cells)
        {
            List<fCell> LiveCells = new List<fCell>();
            foreach (var cell in Cells)
            {
                double m = cell.Measure2();
                if (m == 1) LiveCells.Add(cell);
            }

            fResults tRes = new fResults(Conditions.Copy());
            //tRes.Conditions.TopFractionCellsPickList = new List<double>(1) { TopFractionCells };
            tRes.CellsPickedRealized = LiveCells.Count;

            Dictionary<fPerturb, int> PerturbCounts = new Dictionary<fPerturb, int>();
            foreach (fCell cell in LiveCells)
            {
                foreach (fDeliver deliver in cell.Delivereds)
                {
                    if (!PerturbCounts.ContainsKey(deliver.Perturb)) PerturbCounts.Add(deliver.Perturb, 0);
                    PerturbCounts[deliver.Perturb]++;
                }
            }

            tRes.SeqList = new SortedList<double, fPerturb>(OriginalPerturbSet.Count);
            double Sum_Neg = 0;
            double Sum_Pos = 0;
            foreach (var ptb in OriginalPerturbSet) //Doing it this way incase any of the originals are missing
            {
                double CellsCounted = PerturbCounts.ContainsKey(ptb) ? PerturbCounts[ptb] : 0;
                if (ptb.Phenotype.State == 0) Sum_Neg += CellsCounted;
                if (ptb.Phenotype.State == 1) Sum_Pos += CellsCounted;

                CellsCounted = -CellsCounted; //so the best come to top
                while (tRes.SeqList.ContainsKey(CellsCounted)) CellsCounted -= RND.ZeroOne() / 10000;
                tRes.SeqList.Add(CellsCounted, ptb);
            }
            int TotalNegatives = (int)(OriginalPerturbSet.Count * (1 - Conditions.FractionActive)); 
            int TotalPositives = (int)(OriginalPerturbSet.Count * (Conditions.FractionActive));
            double Sum_All = Sum_Neg + Sum_Pos;
            double Avg_Pos = Sum_Pos / TotalPositives;
            double Avg_Neg = Sum_Neg / TotalNegatives;
            double Avg_All = Sum_All / OriginalPerturbSet.Count;

            tRes.Enrichment = Avg_Pos / Avg_Neg;

            //The threshold will just be how many of the positives are in the top of the list
            int TPCount = 0; int FPCount = 0;
            for (int i = 0; i < TotalPositives; i++)
            {
                var T =  tRes.SeqList.Values[i];
                if (T.Phenotype.State == 1) TPCount++;
                else FPCount++;
            }

            tRes.TruePositives = TPCount;
            tRes.TrueNegatives = TotalNegatives - FPCount;
            tRes.FalsePositives = FPCount;
            tRes.FalseNegatives = TotalPositives - TPCount;
            return tRes;
        }

    }

    public class fTrialSet : IEnumerable<fTrial>
    {
        private List<fTrial> _List;
        public int Count { get => _List.Count; }
        public fTrial this[int index] { get => _List[index]; }
        public double Specificity { get => Sum_TrueNegatives / (Sum_TrueNegatives + Sum_FalsePositives); }
        public double Sensitivity { get => Sum_TruePositives / (Sum_TruePositives + Sum_FalseNegatives); }
        public double Accuarcy { get => (Sum_TruePositives + Sum_TrueNegatives) / (Sum_TrueNegatives + Sum_TruePositives + Sum_FalsePositives + Sum_FalseNegatives); }
        public double Sum_FalsePositives;
        public double Sum_FalseNegatives;
        public double Sum_TruePositives;
        public double Sum_TrueNegatives;

        public fTrialSet(fConditions Conditions, int Runs)
        {
            _List = new List<fTrial>();
            fTrial tTrial;
            for (int i = 0; i < Runs; i++)
            {
                tTrial = new fTrial(Conditions);
                //tTrial.RunTrial_RaftSeq();
                tTrial.RunTrial_Survival();
                _List.Add(tTrial);
                Sum_FalsePositives += tTrial.Result.FalsePositives;
                Sum_TruePositives += tTrial.Result.TruePositives;
                Sum_FalseNegatives += tTrial.Result.FalseNegatives;
                Sum_TrueNegatives += tTrial.Result.TrueNegatives;
            }
        }

        public void Export(StreamWriter SW, bool Headers)
        {
            //A function to help us below
            string sExp(SortedList<double, fPerturb> List, int Idx)
            {
                return List.Values[Idx].ExportName + " " + (-List.Keys[Idx]).ToString("00.0");
            }

            //Now actually export
            if (Headers) SW.WriteLine(this[0].Result.Conditions.Export(true) + "\t" + "Sensitivity" + "\t" + "Specificity" + "\t" + "SeqList Sensitivity" + "\t" + "SeqList Specificity" + "\t" + "Enrichment" + "\t" + "s1" + "\t" + "s2" + "\t" + "s3" + "\t" + "s4" + "\t" + "s5" + "\t" + "s6" + "\t" + "s7" + "\t" + "s8");
            foreach (fTrial aTrial in this)
            {
                foreach (fResults Res in aTrial.Results)
                {
                    //Debug.Print(Res.Conditions.PerturbSetSize + " " + Res.Conditions.TopFractionCellsPick);
                    SW.WriteLine(Res.Conditions.Export(false) + "\t" + Res.Sensitivity + "\t" + Res.Specificity + "\t" + Res.SeqList_Sens + "\t" + Res.SeqList_Spec + "\t" + Res.Enrichment + "\t" + sExp(Res.SeqList, 0) + "\t" + sExp(Res.SeqList, 1)  + "\t" + sExp(Res.SeqList, 2) + "\t" + sExp(Res.SeqList, 3) + "\t" + sExp(Res.SeqList, 4) + "\t" + sExp(Res.SeqList, 5) + "\t" + sExp(Res.SeqList, 6) + "\t" + sExp(Res.SeqList, 7));
                }
            }
        }

        public IEnumerator<fTrial> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }
    }

    public class fPerturb
    {
        public double Representation = 1;
        public fPhenotype Phenotype;
        public fPerturb()
        {
            //From 0.5 to 2
            Representation = (RND.ZeroOne() + RND.ZeroOne() + RND.ZeroOne()) / 3;
            Representation = Representation > 0.5 ? (Representation * 2) : (0.5 + Representation);
            Phenotype = fPhenotype.NegativePhenotype();
        }

        public void SetActive()
        {
            Phenotype = fPhenotype.ActivePhenotype();
        }

        public List<fDeliver> CreateParticles(int DeliveryTotal, int PerturbTotal)
        {
            double Particles = (Representation * DeliveryTotal / PerturbTotal) * 0.965;
            int ParticlesWhole = (int)Math.Floor(Particles);
            List<fDeliver> Set = new List<fDeliver>(ParticlesWhole + 1);
            for (int i = 0; i < ParticlesWhole; i++) Set.Add(new fDeliver(this));
            double ParticlesLeft = Particles - ParticlesWhole;
            if (RND.ZeroOne() < ParticlesLeft) Set.Add(new fDeliver(this));
            return Set;
        }

        public string ExportName { get => (Phenotype.State == 1 ? "#" : ":"); }

        public override string ToString()
        {
            return (Phenotype.State == 1 ? "Active" : "Silent") + " " + Representation.ToString("0.0");
        }
    }

    public class fPerturbSet : IEnumerable<fPerturb>
    {
        public int Count { get => _List.Count; }
        private List<fPerturb> _List;

        public fPerturbSet()
        {
            _List = new List<fPerturb>();
        }

        /// <summary>
        /// Create a library of Perturbations.  Each is unique, and representation is encoded inside the perturbation
        /// </summary>
        public fPerturbSet(int CountTotal, double FractionActive)
        {
            if (FractionActive > 1) FractionActive = FractionActive / 100;
            _List = new List<fPerturb>(CountTotal);
            int i;
            for (i = 0; i < CountTotal; i++)
            {
                _List.Add(new fPerturb());
            }
            double CountActiveTotal = (double)CountTotal * FractionActive;
            int CountActiveWhole = (int)Math.Floor(CountActiveTotal);
            double CountActivePart = CountActiveTotal - CountActiveWhole;
            for (i = 0; i < CountActiveWhole; i++)
            {
                _List[i].SetActive();
            }
            if (RND.ZeroOne() < CountActivePart) _List[i].SetActive();
        }

        public fPerturb this[int index]
        {
            get { return _List[index]; }
        }

        public IEnumerator<fPerturb> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }
    }

    public class fDeliver
    {
        public fPerturb Perturb;

        public fDeliver(fPerturb fPerturb)
        {
            Perturb = fPerturb;
        }

        public override string ToString()
        {
            return Perturb.ToString();
        }
    }

    public class fDeliverSet : IEnumerable<fDeliver>
    {
        public fPerturbSet PerturbSet;
        private List<fDeliver> _List;
        public int Count { get => _List.Count; }

        public fDeliver this[int index]
        {
            get { return _List[index]; }
        }

        /// <summary>
        /// Create the set of physical deliverable particles (like Viral Particles) that will deliver the Perturb set.
        /// These are directly countable based on the perturbations representation.  
        /// </summary>
        public fDeliverSet(fPerturbSet Perturb_Set, double FractionDeliveryDead, int ParticleMultiplier = 23)
        {
            PerturbSet = Perturb_Set;
            int TotalDeliveryParticles = PerturbSet.Count * ParticleMultiplier; //This gives us a pretty good actual object representation
            _List = new List<fDeliver>(TotalDeliveryParticles);
            for (int i = 0; i < PerturbSet.Count; i++)
            {
                _List.AddRange(PerturbSet[i].CreateParticles(TotalDeliveryParticles, PerturbSet.Count));
            }
            double AdditionalDead = (FractionDeliveryDead * Count) / (1 - FractionDeliveryDead);
            for (int i = 0; i < AdditionalDead; i++)
            {
                _List.Add(new fDeliver(null));
            }
        }

        public IEnumerator<fDeliver> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        internal void Deliver(fCellSet fCellSet, double mOI)
        {
            int Particles = (int)((double)fCellSet.Count * mOI);
            List<fDeliver> DeliverSet = GenerateDeliverSet(Particles); //THIS STEP was too slow, switchng to sorted list was massive improvement
            int CellChoice;
            foreach (fDeliver Particle in DeliverSet)
            {
                CellChoice = RND.Next(0, fCellSet.Count);
                fCellSet[CellChoice].AddDeliver(Particle);
            }
            fCellSet.ReCalculate(); //Just tells the gRNAs which cells they are in
        }

        private List<fDeliver> GenerateDeliverSet(int Particles)
        {
            //The idea here is to make a deliver set that is a large set of particles that will choose a cell

            //SortedList<double, fDeliver> ToSort = new SortedList<double, fDeliver>(Particles); //This is way too slow
            SortedDictionary<double, fDeliver> ToSort = new SortedDictionary<double, fDeliver>();
            double Key;
            while (ToSort.Count < Particles)
            {
                for (int i = 0; i < Count; i++)
                {
                    Key = RND.ZeroOne();
                    while (ToSort.ContainsKey(Key)) Key = RND.ZeroOne();
                    ToSort.Add(Key, _List[i]);
                }
                //List<fDeliver> Test2 = _List.GetRange(6000,_List.Count - 6000); //6591
            }
            List<fDeliver> Set = ToSort.Values.ToList();
            return Set.GetRange(0, Particles);
        }
    }

    public class fCell
    {
        public fCellSet Parent;
        public List<fDeliver> Delivereds = new List<fDeliver>();
        internal fRaft Raft;
        public int SilentsCount { get; private set; }
        public int ActivesCount { get; private set; }

        public override string ToString()
        {
            return Delivereds.Count.ToString();
        }

        internal void AddDeliver(fDeliver particle)
        {
            if (particle.Perturb == null) return; //Don't add a Dead particle
            Delivereds.Add(particle);
            if (particle.Perturb.Phenotype.State == 1)
                ActivesCount++;
            else
                SilentsCount++;
        }

        public fCell(fCellSet Parent)
        {
            ActivesCount = 0; SilentsCount = 0;
            this.Parent = Parent;
        }

        internal double Measure()
        {
            //TODO: doesn't take into account change of cutting

            // Old version >
            // Math.Pow(RND.ZeroOne(), fPhenotype.SilentExponent);
            for (int i = 0; i < Delivereds.Count; i++)
            {
                if (Delivereds[i].Perturb.Phenotype.State == 1)
                    return fPhenotype.Random_Active;
            }
            return fPhenotype.Random_Negative;
        }

        internal double Measure2()
        {
            for (int i = 0; i < Delivereds.Count; i++)
            {
                if (Delivereds[i].Perturb.Phenotype.State == 1)
                    if (RND.ZeroOne() < Parent.PerCellChanceOfCutting) //Only counts if there was cutting
                    {
                        return RND.ZeroOne() < fPhenotype.ProbOfLifePositive ? 1 : 0;
                    }
            }
            return RND.ZeroOne() < fPhenotype.ProbOfLifeNegative ? 1 : 0;
        }
    }

    public class fCellSet : IEnumerable<fCell>
    {
        public fDeliverSet DeliverySet;
        private Dictionary<int, fCell> _List;
        public int Count { get => _List.Count(); }
        public double PerCellChanceOfCutting;

        public fCell this[int index] { get => _List[index]; }
        public Dictionary<int, double> ParticlePerCell;

        public fCellSet(int TotalCells, fDeliverSet DeliverySet, double MOI, double SelectionPercentage, double PerCellProbCutting)
        //SelectionPercentage = 0 means no selection, 1 means selection is perfect (at least 1 gRNA keeps the cell)
        {
            PerCellChanceOfCutting = PerCellProbCutting;
            _List = new Dictionary<int, fCell>(TotalCells);
            this.DeliverySet = DeliverySet;
            for (int i = 0; i < TotalCells; i++) _List.Add(i, new fCell(this));

            DeliverySet.Deliver(this, MOI);

            SelectCells(SelectionPercentage);
        }

        private void SelectCells(double selectionPercentage)
        {
            //List<fCell> ToRemove = new List<fCell>();
            List<int> ToRemove = new List<int>();
            foreach (var KVP in _List)
            {
                if (KVP.Value.Delivereds.Count == 0)
                {
                    if (RND.ZeroOne() < selectionPercentage) ToRemove.Add(KVP.Key);
                }
            }
            foreach (int c in ToRemove) _List.Remove(c);
        }

        public IEnumerator<fCell> GetEnumerator()
        {
            return _List.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        internal void ReCalculate()
        {
            ParticlePerCell = new Dictionary<int, double>();
            foreach (fCell Cell in this)
            {
                if (!ParticlePerCell.ContainsKey(Cell.Delivereds.Count)) ParticlePerCell.Add(Cell.Delivereds.Count, 0);
                ParticlePerCell[Cell.Delivereds.Count]++;
            }
        }
    }

    public class fRaft
    {
        public List<fCell> Cells = new List<fCell>();
        public HashSet<fPerturb> Perturbations = new HashSet<fPerturb>();

        public void ReCalculate()
        {
            Perturbations = new HashSet<fPerturb>();
            foreach (fCell Cell in Cells)
            {
                foreach (fDeliver deliver in Cell.Delivereds)
                {
                    if (deliver.Perturb != null)
                        Perturbations.Add(deliver.Perturb);
                }
            }
        }
    }

    public class fRaftSet : IEnumerable<fRaft>
    {
        private List<fRaft> _List;
        public List<fCell> UnPickable;
        public SortedList<double, fCell> Pickable;
        public int Count { get => _List.Count; }
        public object this[int index] { get => _List[index]; }
        public int ActiveDelivers = 0;
        public int SilentDelivers = 0;

        public IEnumerator<fRaft> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        public fRaftSet(int TotalRafts, fCellSet Cells, double FractionUnPickable)
        {
            //Unpickable represents cells that fall between rafts or on un-pickable rafts (that get stuck, etc)
            _List = new List<fRaft>(TotalRafts);
            for (int i = 0; i < TotalRafts; i++) _List.Add(new fRaft());
            int RaftChoice;
            UnPickable = new List<fCell>((int)((double)Cells.Count * FractionUnPickable));
            Pickable = new SortedList<double, fCell>(Cells.Count); double Measure;
            foreach (fCell Cell in Cells)
            {
                SilentDelivers += Cell.SilentsCount;
                ActiveDelivers += Cell.ActivesCount;
                if (RND.ZeroOne() < FractionUnPickable)
                {
                    UnPickable.Add(Cell);
                }
                else
                {
                    RaftChoice = RND.Next(0, TotalRafts);
                    _List[RaftChoice].Cells.Add(Cell);
                    Cell.Raft = _List[RaftChoice];
                    Measure = -Cell.Measure();
                    while (Pickable.ContainsKey(Measure)) Measure = -Cell.Measure();
                    Pickable.Add(Measure, Cell);
                }
            }
            ReCalculate();
        }

        public void ReCalculate()
        {
            foreach (fRaft Raft in _List)
            {
                Raft.ReCalculate();
            }
        }

        internal fResults PickTop(double TopFractionCells, int MaxCellsPerRaft, int TotalPositives, int TotalNegatives, fConditions Conditions)
        {
            //Go through the cell images, and pick the rafts with the top ones
            HashSet<fRaft> RaftsToPick = new HashSet<fRaft>();
            fRaft Candidate;
            int CellsToPick = (int)((double)TopFractionCells * Pickable.Count);
            for (int i = 0; i < CellsToPick; i++)
            {
                Candidate = Pickable.Values[i].Raft;
                if (Candidate.Cells.Count <= MaxCellsPerRaft)
                    RaftsToPick.Add(Candidate);
                else
                    CellsToPick++; //Try the next cell since we couldn't pick that raft (too many cells)
            }
            //Now go through all the Rafts and get the sequences 
            Dictionary<fPerturb, int> SequencedSet = new Dictionary<fPerturb, int>();
            int TPCount = 0; int FPCount = 0;
            HashSet<fPerturb> TP = new HashSet<fPerturb>();
            HashSet<fPerturb> FP = new HashSet<fPerturb>();
            foreach (fRaft Raft in RaftsToPick)
            {
                foreach (fPerturb Perturb in Raft.Perturbations)
                {
                    if (!SequencedSet.ContainsKey(Perturb)) SequencedSet.Add(Perturb, 0);
                    SequencedSet[Perturb]++;
                    if (Perturb.Phenotype.State == 1)
                    {
                        TPCount++; TP.Add(Perturb);
                    }
                    else
                    {
                        FPCount++; FP.Add(Perturb);
                    }
                }
            }
            //Now sort
            SortedList<double, fPerturb> Sorted = new SortedList<double, fPerturb>(SequencedSet.Count);
            foreach (KeyValuePair<fPerturb, int> KVP in SequencedSet)
                Sorted.Add(-(KVP.Value + (RND.ZeroOne() / 100)), KVP.Key);

            fResults tRes = new fResults(Conditions.Copy());
            tRes.Conditions.TopFractionCellsPickList = new List<double>(1) { TopFractionCells };
            tRes.CellsPickedRealized = (int)((double)TopFractionCells * Pickable.Count);

            tRes.SeqList = Sorted;
            tRes.TruePositives_Delivers = TPCount;
            tRes.TrueNegatives_Delivers = SilentDelivers - FPCount;
            tRes.FalsePositives_Delivers = FPCount;
            tRes.FalseNegatives_Delivers = ActiveDelivers - TPCount;

            if (TP.Count > TotalPositives) TotalPositives = TP.Count;

            tRes.TruePositives = TP.Count;
            tRes.TrueNegatives = TotalNegatives - FP.Count;
            tRes.FalsePositives = FP.Count;
            tRes.FalseNegatives = TotalPositives - TP.Count;
            return tRes;
        }
    }

    public class fPhenotype
    {
        public static double SilentExponent = 2.323; //2.323 Median 0.20;
        public static double ActiveExponent = 0.321; //0.321 Median 0.80

        public double State = 0;

        //public static double Random_Active { get => RandomFrom_TriangleDist(0, 1, 0.8); }
        //public static double Random_Negative { get => RandomFrom_TriangleDist(0, 1, 0.2); }

        //public static double Random_Active { get => RandomFrom_TriangleDist(0.2, 1, 0.8); }
        //public static double Random_Negative { get => RandomFrom_TriangleDist(0, 0.8, 0.2); }

        public static double Random_Active { get => RandomFrom_TriangleDist(0.2, 1, 0.6); }
        public static double Random_Negative { get => RandomFrom_TriangleDist(0, 0.8, 0.4); }
        public static double ProbOfLifePositive = 0.95;
        public static double ProbOfLifeNegative = 0.05;

        internal static fPhenotype ActivePhenotype()
        {
            return new fPhenotype() { State = 1 };
        }

        internal static fPhenotype NegativePhenotype()
        {
            return new fPhenotype() { State = 0 };
        }

        public static double Gaussian(double x, double mean, double stDev)
        {
            double top = Math.Exp(Math.Pow(-(x - mean), 2) / (2 * Math.Pow(stDev, 2)));
            double bottom = stDev * Math.Sqrt(2 * Math.PI);
            double gaussian = top / bottom;
            return gaussian;
        }

        public static double RandomFrom_TriangleDist(double Min, double Max, double Mid)
        {
            double U = RND.ZeroOne();
            double F = (Mid - Min) / (Max - Min);
            if (U <= F)
                return Min + Math.Sqrt(U * (Max - Min) * (Mid - Min));
            else
                return Max - Math.Sqrt((1 - U) * (Max - Min) * (Max - Mid));
        }
    }
}
