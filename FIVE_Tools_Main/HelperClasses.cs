﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Drawing;
using System.Diagnostics;
using System.Collections;
using FIVE.Masks;
using FIVE.InCellLibrary;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.VisualBasic.FileIO;
using System.Security.Cryptography.X509Certificates;
using System.Drawing.Text;
using DocumentFormat.OpenXml.Presentation;
using System.DirectoryServices.ActiveDirectory;
using System.Threading;

using FuzzySharp.PreProcess;
using FuzzySharp;

namespace FIVE
{

    public static class RND
    {
        private static Random Rnd = new Random();

        public static double ZeroOne()
        {
            return Rnd.NextDouble();
        }

        public static int Next(int Start, int End)
        {
            return Rnd.Next(Start, End);
        }
    }

    public static class Compile_AzureML_xN_Results
    {
        static StreamWriter SW_main;
        static StreamWriter SW_auc;
        static char D = ',';

        public static string CompileFolder(string Folder, string FileNameHeader, System.ComponentModel.BackgroundWorker BW = null)
        {
            DirectoryInfo DI = new DirectoryInfo(Folder);
            if (!DI.Exists) return "Couldn't find the folder you provided.";
            string Header = FileNameHeader;
            ColumnsUsed = new HashSet<string>(); //This makes sure we don't have duplicate columns

            //First get all the files that we are going to combine
            List<StreamReader> SRs = new List<StreamReader>();
            List<Tuple<int, int>> Indices = new List<Tuple<int, int>>();
            foreach (FileInfo file in DI.GetFiles())
            {
                if (file.Extension.ToUpper() != ".CSV" && file.Extension.ToUpper() != ".TXT" && file.Extension.ToUpper() != ".TSV") continue;
                if (file.Name.ToUpper().EndsWith("_FULL_OUT.TXT") || file.Name.ToUpper().EndsWith("_AUC_OUT.TXT"))
                    continue;
                if (file.Name.StartsWith("._")) continue;
                if (file.Extension.ToUpper() == ".CSV") D = ','; else D = '\t';
                SRs.Add(new StreamReader(file.FullName));
            }
            if (SRs.Count < 1)
            {
                return "No files found";
            }

            //Now open the new destination files
            SW_main = new StreamWriter(Path.Combine(Folder, Header + "_xNN_Full_Out.txt"));
            SW_auc = new StreamWriter(Path.Combine(Folder, Header + "_xNN_AUC_Out.txt"));
            int row = 0; string tRow; string[] arr; List<List<int>> ColListMain = new List<List<int>>(); List<List<int>> ColListAUC = new List<List<int>>();
            while (!SRs.First().EndOfStream)
            {
                row++; if (row % 500 == 0)
                {
                    if (BW != null) BW.ReportProgress(0, row.ToString());
                }
                for (int i = 0; i < SRs.Count; i++)
                {
                    StreamReader SR = SRs[i]; if (SR.EndOfStream) break;
                    tRow = SR.ReadLine(); arr = tRow.Split(D);
                    if (row == 1)
                    {  //Header rows, lets figure out the indices to use
                        //Indices.Add(FindCols(arr)); //6/16/2021 stopped using this version
                        ColListMain.Add(new List<int>()); ColListAUC.Add(new List<int>());
                        FindCols2(arr, ColListMain[i], ColListAUC[i]);
                    }
                    if (ColListMain[i].Count == 0) continue;
                    SW_main.Write(PrepColList(arr, ColListMain[i], false, i == 0));
                    //SW_main.Write(Prep(arr, i == 0 ? 0 : Indices[i].Item1, Indices[i].Item2, false)); //6/16/2021 stopped using this version
                    if (row <= 3)
                    {
                        SW_auc.Write(PrepColList(arr, ColListAUC[i], true)); //One of the Rows that also has the AUCs at the end
                    }
                }
                //Goto next line
                SW_main.WriteLine(); if (row <= 3) SW_auc.WriteLine();
            }
            foreach (StreamReader SR in SRs) SR.Close();
            SW_main.Close(); SW_auc.Close();
            return "Success";
        }

        private static string PrepColList(string[] RowArr, List<int> Collist, bool AUC, bool firstColumns = false)
        {
            char D = '\t';
            StringBuilder sB = new StringBuilder();
            if (!AUC && firstColumns) { for (int i = 0; i < Collist.First(); i++) { sB.Append(RowArr[i]); sB.Append(D); } }
            for (int i = 0; i < Collist.Count; i++) { sB.Append(RowArr[Collist[i]]); sB.Append(D); }
            return sB.ToString();
        }

        private static string Prep(string[] RowArr, int FirstIndex, int LastIndex, bool AUC)
        {
            StringBuilder sB = new StringBuilder();
            for (int i = FirstIndex; i < LastIndex; i++) { sB.Append(RowArr[i]); sB.Append(D); }
            return sB.ToString();
        }

        static HashSet<string> ColumnsUsed;

        static void FindCols2(string[] Arr, List<int> ColsMain, List<int> ColsAUC)
        {
            string Pipe = "|"; string Ending = " ("; int pipesCount;
            List<int> Track = new List<int>();
            for (int i = 0; i < Arr.Length; i++)
            {
                pipesCount = Arr[i].Length - Arr[i].Replace(Pipe, "").Length;
                if (pipesCount > 3)
                {
                    Track.Add(i);
                    if (Arr[i].Contains(Ending))
                    {
                        Arr[i] = Arr[i].Substring(0, Arr[i].IndexOf(Ending)); //This fixes the header
                    }
                }
            }
            //First half of piped variables are for the "Main", and second half are for the "AUC" portion
            //But first, lets remove any accidental duplicates
            string Header;
            for (int i = 0; i < Track.Count / 2; i++)
            {
                Header = Arr[Track[i]];
                if (!ColumnsUsed.Contains(Header)) //Include it
                {
                    ColumnsUsed.Add(Header);
                    ColsMain.Add(Track[i]);
                    ColsAUC.Add(Track[i + (Track.Count / 2)]);
                }
                else
                {
                    //Means there was a duplicate
                }
            }
        }
        

        static Tuple<int, int> FindCols(string[] RowArr)
        {
            int Main = -1; int AUC = -1; string Pipe = "|"; string Ending = " (2)";
            for (int i = 0; i < RowArr.Length; i++)
            {
                if (RowArr[i].Contains(Pipe))
                {
                    int pipes = RowArr[i].Length - RowArr[i].Replace(Pipe, "").Length;
                    if (pipes >= 3 && Main < 0)
                    {
                        Main = i;
                    }
                    if (RowArr[i].EndsWith(Ending))
                    {
                        if (AUC < 0) AUC = i;
                        RowArr[i] = RowArr[i].Replace(Ending, ""); //This fixes the header
                    }
                }
            }
            return new Tuple<int, int>(Main, AUC);
        }
    }
    
    public static class Compile_TFml_xN_Results
    {
        static StreamWriter SW_long;
        static StreamWriter SW_wide;
        static StreamWriter SW_colDefs;
        static char D = ',';

        public static string CompileFolder(string Folder, string FileNameHeader, string Folder2, System.ComponentModel.BackgroundWorker BW = null)
        {
            string MustHaveKey = "G2";

            if (BW != null) BW.ReportProgress(0, "Searching first path . .");
            var DI = new DirectoryInfo(Folder);
            if (!DI.Exists) return "Couldn't find the folder you provided.";
            List<FileInfo> AllFiles = GetFiles(DI);
            if (Folder2 != "")
            {
                if (BW != null) BW.ReportProgress(0, "Searching 2nd path . .");
                DI = new DirectoryInfo(Folder2);
                if (DI.Exists) AllFiles.AddRange(GetFiles(DI));
            }

            if (BW != null) BW.ReportProgress(0, "Found Files . .");
            string Header = FileNameHeader;
            ColumnsUsed = new HashSet<string>(); //This makes sure we don't have duplicate columns

            //First get all the files that we are going to combine
            var SRs = new List<Tuple<string, TextFieldParser>>();
            var Indices = new List<Tuple<int, int>>();
            foreach (var file in AllFiles)
            {
                if (file.Extension.ToUpper() != ".CSV" && file.Extension.ToUpper() != ".TXT" && file.Extension.ToUpper() != ".TSV") continue;
                if (file.Name.ToUpper().EndsWith("_FULL_OUT.TXT") || file.Name.ToUpper().EndsWith("_AUC_OUT.TXT") || file.Name.ToUpper().EndsWith("_COLDEFS_OUT.TXT"))
                    continue;
                if (file.Name.StartsWith("._")) continue;
                if (file.Extension.ToUpper() == ".CSV") D = ','; else D = '\t';
                var P = new TextFieldParser(file.FullName); P.Delimiters = new string[1] { D.ToString() };
                SRs.Add(new Tuple<string, TextFieldParser>(file.FullName, P));
            }
            if (SRs.Count < 1) { return "No files found"; }
            if (BW != null) BW.ReportProgress(0, "Setup TextParsers . .");

            //Now open the new destination files
            SW_wide = new StreamWriter(Path.Combine(Folder, Header + "_xTF_Wide_Out.txt"));
            SW_long = new StreamWriter(Path.Combine(Folder, Header + "_xTF_Long_Out.txt"));
            SW_colDefs = new StreamWriter(Path.Combine(Folder, Header + "_xTF_ColDefs_Out.txt"));
            int row = 0; string[] arr; int TotalColumns = 0; var headers = new List<string[]>(SRs.Count);
            var ColListMain = new List<List<int>>(); var ColListAUC = new List<List<int>>();
            while (!SRs.First().Item2.EndOfData)
            {
                if (++row % 100 == 0) { if (BW != null) BW.ReportProgress(0, row.ToString()); }
                for (int i = 0; i < SRs.Count; i++)
                {
                    var SR = SRs[i].Item2; if (SR.EndOfData) break;
                    arr = SR.ReadFields();
                    if (row == 1)
                    {
                        if (arr[0] == "") arr[0] = "idx";
                        headers.Add(arr);
                        ColListMain.Add(new List<int>()); ColListAUC.Add(new List<int>());
                        FindCols2(arr, ColListMain[i], ColListAUC[i], MustHaveKey);
                        TotalColumns += ColListMain[i].Count;
                    }
                    if (ColListMain[i].Count == 0) continue;

                    WriteColList_WideAndLong(SRs[i].Item1, arr, headers[i], ColListMain[i], SW_wide, SW_long, SW_colDefs, i == 0, row == 1);
                }
                //Goto next line
                SW_wide.WriteLine();
                if (row == 1)
                {
                    if (BW != null) BW.ReportProgress(0, "Total Columns = " + TotalColumns);
                }
            }
            if (BW != null) BW.ReportProgress(0, "Closing writers . .");
            foreach (var SR in SRs) SR.Item2.Close();
            SW_wide.Close(); SW_long.Close(); SW_colDefs.Close();
            return "Success";

            static List<FileInfo> GetFiles(DirectoryInfo DI)
            {
                var AllFiles = new List<FileInfo>(DI.GetFiles("*.csv"));
                foreach (var fldr in DI.GetDirectories())
                {
                    AllFiles.AddRange(fldr.GetFiles("*.csv"));
                    foreach (var sfldr in fldr.GetDirectories())
                        AllFiles.AddRange(sfldr.GetFiles("*.csv"));
                }

                return AllFiles;
            }
        }

        private static int GlobalCounter = 0;

        public static string NextColKey()
        {
            GlobalCounter++;
            //byte[] b = BitConverter.GetBytes(GlobalCounter);
            return "h" + GlobalCounter.ToString("X5");
        }

        public static string ShortColName(string FullName, string ColKey)
        {
            int e1 = FullName.IndexOf(" ", 20);
            e1 = Math.Min(32, e1);
            return FullName.Substring(0, e1) + " " + ColKey;
        }

        private static void WriteColList_WideAndLong(string FileName, string[] RowArr, string[] HeaderArr, List<int> Collist, StreamWriter Wides, StreamWriter Longs, StreamWriter ColDefs, bool firstColumns, bool justHeaders)
        {
            char D = '\t'; var sWide = new StringBuilder(); var sLong = new StringBuilder(); var sFront = new StringBuilder(); string sFrontsz; string PlateKey = "";
            if (!justHeaders && HeaderArr[1] == "PlateID") { PlateKey = RowArr[1].Substring(0, 8); }
            sFront.Append((justHeaders ? "Platekey" : PlateKey) + D);
            for (int i = 0; i < Collist.First(); i++) sFront.Append(RowArr[i] + D);
            sFrontsz = sFront.ToString();
            if (firstColumns)
            {
                //This gets the first few (repeated in every file but just once in the compilation
                sWide = new StringBuilder(sFrontsz);
                //Here we record the model name and other info into the col definition file
                if (justHeaders)
                {
                    ColDefs.WriteLine("ColName" + D + "ColKey" + D + "FullName" + D + "Folder" + D);
                    SW_long.WriteLine(sFrontsz + "ScoreName" + D + "ScoreValue");
                }
            }
            //This only does the rest ater the first
            string Folder = Path.GetDirectoryName(FileName);
            for (int i = 0; i < Collist.Count; i++)
            {
                if (justHeaders)
                {
                    string FullName = RowArr[Collist[i]];
                    string ColKey = NextColKey();
                    string ColName = ShortColName(FullName, ColKey);
                    ColDefs.WriteLine(ColName + "\t" + ColKey + "\t" + FullName + "\t" + Folder + "\t");
                    sWide.Append(ColName);
                }
                else
                {
                    sWide.Append(RowArr[Collist[i]]);
                    sLong.Append(sFrontsz + HeaderArr[Collist[i]] + D + RowArr[Collist[i]] + "\r\n");
                }
                sWide.Append(D);
            }
            Wides.Write(sWide.ToString());
            Longs.Write(sLong.ToString());
        }



        private static string Prep(string[] RowArr, int FirstIndex, int LastIndex, bool AUC)
        {
            StringBuilder sB = new StringBuilder();
            for (int i = FirstIndex; i < LastIndex; i++) { sB.Append(RowArr[i]); sB.Append(D); }
            return sB.ToString();
        }

        static HashSet<string> ColumnsUsed;

        static void FindCols2(string[] Arr, List<int> ColsMain, List<int> ColsAUC, string MustHave)
        {
            for (int i = 0; i < Arr.Length; i++)
            {
                string colHead = Arr[i].Trim();
                if (colHead == "") continue;
                if (colHead.Length > 15)
                { if (colHead.Contains(MustHave)) ColsMain.Add(i); }
                else ColsAUC.Add(i);
            }

            //ColumnsUsed.Add(Header);
            //ColsMain.Add(Track[i]);
            //ColsAUC.Add(Track[i + (Track.Count / 2)]);
        }

        static Tuple<int, int> FindCols(string[] RowArr)
        {
            int Main = -1; int AUC = -1; string Pipe = "|"; string Ending = " (2)";
            for (int i = 0; i < RowArr.Length; i++)
            {
                if (RowArr[i].Contains(Pipe))
                {
                    int pipes = RowArr[i].Length - RowArr[i].Replace(Pipe, "").Length;
                    if (pipes >= 3 && Main < 0)
                    {
                        Main = i;
                    }
                    if (RowArr[i].EndsWith(Ending))
                    {
                        if (AUC < 0) AUC = i;
                        RowArr[i] = RowArr[i].Replace(Ending, ""); //This fixes the header
                    }
                }
            }
            return new Tuple<int, int>(Main, AUC);
        }
    }

    public static class FuzzyMatching
    {
        public enum RatioType
        {
            Levenshtein,     // Standard Levenshtein Distance Matching returns int.
            Partial,         // Partial Matching
            TokenSort,       // Token Matching with strings sorted alphabetically
            TokenSet,        // Token Matching with duplicates removed
            TokenDifference, // Token Anti-Matching (looks at differences)
            Weighted,        // Weighted Matching of multiple algorithms (Levenshtein, Partial, TokenSort, TokenSet)
            LevenshteinExact, //Standard Levehnstein Distance returns double.
        }

        //Compare two strings using different types of Fuzzy Logic.  Default is Levenshtein
        public static double StringSimilarity(string s1, string s2, RatioType ratioType = RatioType.Levenshtein)
        {
            switch (ratioType)
            {
                case RatioType.LevenshteinExact:
                    return Levenshtein.GetRatio(s1, s2);
                case RatioType.TokenDifference:
                    return Fuzz.TokenDifferenceRatio(s1, s2);
                case RatioType.Partial:
                    return Fuzz.PartialRatio(s1, s2);
                case RatioType.TokenSort:
                    return Fuzz.TokenSortRatio(s1, s2);
                case RatioType.TokenSet:
                    return Fuzz.TokenSetRatio(s1, s2);
                case RatioType.Weighted:
                    return Fuzz.WeightedRatio(s1, s2);
                case RatioType.Levenshtein:
                default:
                    return Fuzz.Ratio(s1, s2);
            }
        }
    }
    public static class PPT_Wrap
    {
        public static void MakePPTGrid(string Title, string[] Files, string[] Captions, int Columns = 7, int Rows = 3, float cellborder = 3)
        {
            Microsoft.Office.Interop.PowerPoint.Application oApp = new Microsoft.Office.Interop.PowerPoint.Application();
            oApp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
            oApp.WindowState = Microsoft.Office.Interop.PowerPoint.PpWindowState.ppWindowNormal;

            Microsoft.Office.Interop.PowerPoint.Presentation oPres = oApp.Presentations.Add(Microsoft.Office.Core.MsoTriState.msoTrue);
            Microsoft.Office.Interop.PowerPoint.Slide oSlide;
            Microsoft.Office.Interop.PowerPoint.Shape Shp;
            //oSlide = oPres.Slides.Add(oPres.Slides.Count + 1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutTitleOnly);

            int Counter = 0; float x, y;
            float width, heigh, xM, yM;
            float halfWidthBorder = oPres.PageSetup.SlideWidth * 0.036f; //Gives some borders on the edge
            float startingTop = oPres.PageSetup.SlideHeight * 0.18f; //Gives room for the title
            xM = (oPres.PageSetup.SlideWidth - halfWidthBorder * 2) / Columns;
            yM = xM; //(oPres.PageSetup.SlideHeight - 1.5f)/Rows;
            width = xM - cellborder;
            heigh = width; //yM - cellborder; //Should be square
            while (true)
            {
                oSlide = oPres.Slides.Add(oPres.Slides.Count + 1, Microsoft.Office.Interop.PowerPoint.PpSlideLayout.ppLayoutTitleOnly);
                oSlide.Shapes[1].TextFrame.TextRange.Text = Title; oSlide.Shapes[1].Top *= 0.7f;
                for (int r = 0; r < Rows; r++)
                {
                    for (int c = 0; c < Columns; c++)
                    {
                        x = halfWidthBorder + c * xM; y = startingTop + r * yM;
                        oSlide.Shapes.AddPicture(Files[Counter], Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoTrue, x, y, width, heigh);
                        Shp = oSlide.Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, x, y, width, heigh / 3);
                        Shp.TextFrame.TextRange.Text = Captions[Counter]; Shp.TextFrame.TextRange.Font.Color.RGB = 0x0000FF;
                        if (++Counter >= Files.Length) return;
                    }
                }
            }
            //oSlide.Shapes[1].TextFrame.TextRange.Text = "Final Test";
            //oSlide.Shapes[1].TextFrame.TextRange.Font.Name = "Comic Sans MS";
            //oSlide.Shapes[1].TextFrame.TextRange.Font.Size = 48;

            //oPres.Save();
            //oPres.Close();

            //oSlide = null;
            //oPres = null;
            //oApp.Quit();
        }
    }

    public static class CompileText
    {
        public static string InitialHeader;
        public static List<string> FileErrors;

        public static void Run()
        {
            Debugger.Break();
            string PathT = @"C:\Users\william\Desktop\InDelphi\indelphi_data";
            PathT = @"E:\F\Box Sync\Projects\GEiC\CRISPR Library\20190821 YiHsien\Human\";
            PathT = @"\\storage1.ris.wustl.edu\wbuchser\Active\db\gRNAs\inDelphi\";

            InitialHeader = "";
            FileErrors = new List<string>();

            string[] Fs = Directory.GetFiles(PathT, "*.txt", System.IO.SearchOption.AllDirectories); int i = 0;
            StreamWriter SW = new StreamWriter(Path.Combine(PathT, "compiled.txt"));
            foreach (string File in Fs) AddFile(File, SW, i++ == 0);
            SW.Close();
            if (FileErrors.Count > 0)
            {
                Debugger.Break();
            }
        }

        private static void AddFile(string FilePath, StreamWriter SW, bool First = false)
        {
            string t;
            using (StreamReader SR = new StreamReader(FilePath))
            {
                t = SR.ReadLine();
                if (First)
                {
                    InitialHeader = t;
                    SW.WriteLine(t);
                }
                else
                {
                    if (t != InitialHeader)
                    {
                        if (t == null) return; //just empty, don't really care
                        FileErrors.Add(FilePath);
                        return;
                    }
                }
                while (!SR.EndOfStream)
                {
                    SW.WriteLine(SR.ReadLine());
                }
                SR.Close();
            }
        }
    }

    namespace gRNA_Bootstrap
    {
        public static class gRNA_Bootstrap_Main
        {
            public static GBS_Columns Columns { get; private set; }
            public static GBS_ValueSets Sets { get; private set; }

            public static Dictionary<string, int> LoadErrorCount = new Dictionary<string, int>();

            public static void aaTestRun()
            {
                char delim = '\t';
                string LoadPath = @"C:\Users\wbuchser\Downloads\LFC Dox vs NoDox all exp 39_47V4.txt";
                //string LoadPath = @"R:\dB\temp\WM39-47 20210921 v01.txt";  // @"R:\People\Waleed Minzal\WM45\WM39_45 A.txt";
                GBS_ValueSets.BootstrapReplicates = 30000;
                double param = 0.63; //Trying to get the best two
                string func = nameof(GBS_Gene.PcntlVal);

                Columns = new GBS_Columns();
                Columns.Add(new GBS_Column() { Idx = 0, Name = "Gene", Meta = true, Type = ColumnTypes.Gene });
                Columns.Add(new GBS_Column() { Idx = 1, Name = "rName", Meta = true, Type = ColumnTypes.gRNA });

                Columns.Add(new GBS_Column() { Idx = 2, Name = "Exp", Meta = true, Type = ColumnTypes.Split });
                Columns.Add(new GBS_Column() { Idx = 3, Name = "Name3", Meta = true, Type = ColumnTypes.Split });
                Columns.Add(new GBS_Column() { Idx = 4, Name = "Time", Meta = true, Type = ColumnTypes.Split });

                Columns.Add(new GBS_Column() { Idx = 9, Name = "Min(Reads)", Meta = true, Type = ColumnTypes.Filter, Filter = -1 });
                Columns.Add(new GBS_Column() { Idx = 10, Name = "LFC Dox-NoDox", Meta = true, Type = ColumnTypes.Value });

                Sets = GBS_ValueSets.Load(LoadPath, delim, Columns);
                Console.WriteLine(Sets.ErrorReport());
                foreach (var Set in Sets)
                    Set.Bootstrap_Gene(func, param);
                Sets.Export(LoadPath + "_ex " + func + ".txt", func, param);
            }

            public static string LoadPath;
            public static string Function;
            public static double Param;

            public static string Load(GBS_Columns ColumnsMetadata, string LoadPath, char delim = '\t')
            {
                gRNA_Bootstrap_Main.LoadPath = LoadPath;
                Columns = ColumnsMetadata;
                Sets = GBS_ValueSets.Load(LoadPath, delim, Columns);
                return "";
            }

            public static void Bootstrap(string functionTorun, double param, int Bootstrap_Reps = 10000, System.ComponentModel.BackgroundWorker BW = null)
            {
                gRNA_Bootstrap_Main.Function = functionTorun; gRNA_Bootstrap_Main.Param = param;
                GBS_ValueSets.BootstrapReplicates = Bootstrap_Reps;
                foreach (var Set in Sets)
                    Set.Bootstrap_Gene(functionTorun, param, BW);
            }

            public static void Export()
            {
                Sets.Export(LoadPath + "_ex " + Function + ".txt", Function, Param);
            }
        }

        public class GBS_Gene
        {
            private string Name;
            public SortedList<double, string> List;
            public double Percentile = -1;
            private double SumVal = 0;

            public override string ToString()
            {
                return Name;
            }

            public GBS_Gene(string gene)
            {
                List = new SortedList<double, string>();
                this.Name = gene;
            }

            public double this[int index]
            {
                get { return List.Keys[index]; }
            }

            public int Count { get => List.Count; }

            private static Random _Rand = new Random();

            internal void Add(string gRNA, double d)
            {
                SumVal += d;
                while (List.ContainsKey(d)) d += _Rand.NextDouble() * 0.000001;
                List.Add(d, gRNA);
            }

            internal double GetValue(string func, double param)
            {
                var T = this.GetType().GetMethod(func);
                return (double)T.Invoke(this, new object[1] { param });
            }

            public double PcntlVal(double PercentileBetween0and1)
            {
                //0 is the smallest, the last number is the largest
                int idx = (int)Math.Round((double)(Count - 1) * PercentileBetween0and1);
                return List.Keys[idx];
            }

            public double Q1(double param = double.NaN)
            {
                //0 is the smallest, the last number is the largest
                return PcntlVal(0.251);
            }

            public double Avg(double param = double.NaN)
            {
                return SumVal / Count;
            }

            public string Export(string Prefix, char delim, string func, double param, bool headers = false)
            {
                if (headers)
                {
                    return Prefix + delim + "Gene" + delim + "gRNA Count" + delim + func + " " + param + delim + "p btsp";
                }
                else
                {
                    return Prefix + delim + Name + delim + Count + delim + GetValue(func, param) + delim + Percentile;
                }
            }
        }

        public class GBS_ValueSets : IEnumerable<GBS_ValueSet>
        {
            private Dictionary<string, GBS_ValueSet> Sets;
            internal static int BootstrapReplicates;

            public GBS_ValueSets()
            {
                Sets = new Dictionary<string, GBS_ValueSet>();
            }

            public int Count { get => Sets.Count; }
            public string Report() { return string.Join("\r\n", this.Select(x => x.Report())); }

            public string ErrorReport() { return string.Join("\r\n", this.Where(x => x.ParseErrors > 0).Select(x => x.Report())); }

            public GBS_ValueSet this[string key] { get => Sets[key]; }

            public void Add(string Key, GBS_ValueSet Set)
            {
                Sets.Add(Key, Set);
            }

            public static GBS_ValueSets Load(string loadPath, char delim, GBS_Columns Columns)
            {
                GBS_ValueSets Sets = new GBS_ValueSets(); string Key;
                using (StreamReader SR = new StreamReader(loadPath))
                {
                    string[] Header = SR.ReadLine().Split(delim); string[] Line;
                    while (!SR.EndOfStream)
                    {
                        Line = SR.ReadLine().Split(delim);
                        Key = Columns.KeyMaker(Line);
                        if (Key == GBS_Columns.KeyIgnore) continue;
                        if (!Sets.ContainsKey(Key)) Sets.Add(Key, new GBS_ValueSet(Key, Columns));
                        Sets[Key].Add(Line);
                    }
                    SR.Close();
                }
                return Sets;
            }

            public bool ContainsKey(string Key) { return Sets.ContainsKey(Key); }

            public IEnumerator<GBS_ValueSet> GetEnumerator()
            {
                return Sets.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return Sets.Values.GetEnumerator();
            }

            public void Export(string ExportPath, string func, double param)
            {
                //Lets export both gene-level and gRNA-Level data here

                //File.WriteAllLines(@"e:\temp\tryboot.txt", Gene_KVP.Select(x => x.Key + '\t' + x.Value.GetValue(func, param) + '\t' + x.Value.Percentile + '\t' + x.Value.Count));
                bool First = true;
                using (var SW = new StreamWriter(ExportPath))
                {
                    foreach (var Set in Sets.Values)
                    {
                        if (First)
                        {
                            Set.Export(SW, func, param, true);
                            First = false;
                        }
                        Set.Export(SW, func, param);
                    }
                    SW.Close();
                }
            }
        }

        public class GBS_ValueSet
        {
            public List<double> Values;
            public Dictionary<string, GBS_Gene> Gene_KVP;
            public string Name;
            public GBS_Columns Cols;
            public int ParseErrors;

            public override string ToString()
            {
                return Name;
            }

            public GBS_ValueSet(string key, GBS_Columns Columns)
            {
                Name = key;
                Cols = Columns;
                Values = new List<double>();
                Gene_KVP = new Dictionary<string, GBS_Gene>();
                ParseErrors = 0;
            }

            internal void Add(string[] line)
            {
                string Gene = line[Cols.Idx_Gene];
                string gRNA = line[Cols.Idx_GRNA];
                double d = 0;
                if (!double.TryParse(line[Cols.Idx_Value], out d)) { ParseErrors++; return; }
                Values.Add(d);
                if (!Gene_KVP.ContainsKey(Gene)) Gene_KVP.Add(Gene, new GBS_Gene(Gene));
                Gene_KVP[Gene].Add(gRNA, d);
            }

            public void Bootstrap_Gene(string func, double Test_gRNA_Percentile, System.ComponentModel.BackgroundWorker BW = null)
            {
                if (BW != null) BW.ReportProgress(0, "Starting " + Name);
                //Test_gRNA_Percentile is intended if you put 0 that is the smallest value, 33% is the 2/4 gRNA, 66% is 3/4 and 100% is the 4/4 (Highest Value) 
                //if (Name == "39 500 HI 12") { }

                //First go through and calculate the distribution for 4 gRNA sample size
                double param = 0;

                double d; bool rem;
                var BootstrapCurves = new Dictionary<int, SortedList<double, int>>();

                foreach (var Gene in Gene_KVP.Values)
                {
                    if (!BootstrapCurves.ContainsKey(Gene.Count))
                        BootstrapCurves.Add(Gene.Count,
                            ConstructCurve(Gene.Count, func, param,
                                Math.Min(GBS_ValueSets.BootstrapReplicates, GBS_ValueSets.BootstrapReplicates * 4 / Gene.Count)));
                    var Curve = BootstrapCurves[Gene.Count];

                    d = Gene.GetValue(func, param); rem = false;
                    if (!Curve.ContainsKey(d)) { rem = true; Curve.Add(d, -1); }
                    Gene.Percentile = (double)Curve.IndexOfKey(d) / Curve.Count;
                    if (rem) Curve.Remove(d);
                }
            }

            public SortedList<double, int> ConstructCurve(int size, string Func, double param, int bootstraps = 10000)
            {
                var Curve = new SortedList<double, int>();
                double d; GBS_Gene FakeGene;
                for (int i = 0; i < bootstraps; i++)
                {
                    FakeGene = new GBS_Gene("");
                    for (int k = 0; k < size; k++) FakeGene.Add("", Values[_Rand.Next(0, Values.Count)]);
                    d = FakeGene.GetValue(Func, param); //This calls a function inside of Gene that aggregates the reads from the gRNAs
                    while (Curve.ContainsKey(d)) d += _Rand.NextDouble() * inc;
                    Curve.Add(d, 0);
                }
                return Curve;
            }

            public string Prefix { get => this.Name; }
            public string Report()
            {
                return Name + " Count=" + Values.Count + " Errors=" + this.ParseErrors;
            }

            internal void Export(StreamWriter SW, string func, double param, bool Headers = false)
            {
                char delim = '\t';
                if (Headers)
                {
                    SW.WriteLine(Gene_KVP.First().Value.Export(Prefix, delim, func, param, Headers));
                    return;
                }
                foreach (var Gene in Gene_KVP.Values)
                    SW.WriteLine(Gene.Export(Prefix, delim, func, param));
            }

            private static double inc = 0.0000001;
            private static Random _Rand = new Random();
        }

        public enum ColumnTypes
        {
            Gene = 1,
            gRNA = 2,
            Filter = 10,
            Split = 50, //Do the experiment for unique values in this column
            Value = 100
        }

        public class GBS_Columns : IEnumerable<GBS_Column>
        {
            internal static string KeyIgnore = "**!!IgnoreThisLine!!**";

            private List<GBS_Column> Cols;
            internal int Idx_Value;
            internal int Idx_Gene;
            internal int Idx_GRNA;

            public GBS_Columns()
            {
                Cols = new List<GBS_Column>();
            }

            public void Add(GBS_Column col)
            {
                Cols.Add(col);
                if (col.Type == ColumnTypes.Value) Idx_Value = col.Idx;
                if (col.Type == ColumnTypes.Gene) Idx_Gene = col.Idx;
                if (col.Type == ColumnTypes.gRNA) Idx_GRNA = col.Idx;
            }

            public int Count { get => Cols.Count; }

            internal string KeyMaker(string[] line)
            {
                string tKey = "";
                foreach (var Col in Cols)
                {
                    if (Col.Type == ColumnTypes.Split)
                    {
                        tKey += line[Col.Idx].Trim().ToUpper() + " ";
                    }
                    else if (Col.Type == ColumnTypes.Filter)
                    {
                        try
                        {
                            if (double.Parse(line[Col.Idx]) <= Col.Filter) return KeyIgnore;
                        }
                        catch
                        {
                            return KeyIgnore;
                        }
                    }
                }
                return tKey.Trim();
            }

            public IEnumerator<GBS_Column> GetEnumerator()
            {
                return Cols.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return Cols.GetEnumerator();
            }

            internal static void SplitsFromList(ListBox listBox_Splits, GBS_Columns columns)
            {
                foreach (int selidx in listBox_Splits.SelectedIndices)
                {
                    var Col = new GBS_Column() { Idx = selidx, Name = listBox_Splits.Items[selidx].ToString(), Type = ColumnTypes.Split, Meta = true };
                    columns.Add(Col);
                }
            }
        }

        public class GBS_Column
        {
            public int Idx;
            public string Name;
            public bool Meta;
            public double Filter;
            public ColumnTypes Type;

            public GBS_Column()
            {

            }

            public GBS_Column(ColumnTypes type, ComboBox coBo, string ExtraParam = "")
            {
                Idx = coBo.SelectedIndex; //coBo_Gene.Items.IndexOf(coBo_Gene.SelectedItem);
                Name = coBo.Text;
                Meta = (type != ColumnTypes.Value);
                Type = type;
                if (ExtraParam != "")
                {
                    double d = -1;
                    double.TryParse(ExtraParam, out d);
                    Filter = d;
                }
            }

            public override string ToString()
            {
                return Name + " " + Type.ToString();
            }
        }
    }


    namespace InCellLibrary
    {
        /// <summary>
        /// The class for stitching together GE images
        /// </summary>
        public class XDCE_ImageStitcher
        {
            #region Instance Variables 

            /// <summary>
            /// The associated XDCE_File
            /// </summary>
            public XDCE XDCE_File;
            /// <summary>
            /// The path that the final image will be written to
            /// </summary>
            public string OutPath;
            /// <summary>
            /// The folder with images and xdce file
            /// </summary>
            public string ImageFolder;
            /// <summary>
            /// The width (px) of each field image in the final stitched image
            /// </summary>
            public int scaledWidth = 200;
            /// <summary>
            /// The height (px) of each field image in the final stitched image
            /// </summary>
            public int scaledHeight = 200;

            private int ImageWidth { get { return (int)(scaledWidth * (XDCE_File.Col_Count + 3)); } } // The extra bit is to adjust for spacing in between images
            private int ImageHeight { get { return (int)(scaledHeight * (XDCE_File.Row_Count + 3)); } }

            private double XDCExMax { get { return XDCE_File.PlateXMax_um; } }
            private double XDCExMin { get { return XDCE_File.PlateXMin_um; } }
            private double XDCEyMax { get { return XDCE_File.PlateYMax_um; } }
            private double XDCEyMin { get { return XDCE_File.PlateYMin_um; } }

            #endregion

            /// <param name="FilePath">The file path for the folder with the images or for the xdce file</param>
            /// <param name="outPath">The folder that the stitched image will be written to. Defaults to the image folder</param>
            public XDCE_ImageStitcher(string FilePath, string outPath = null)
            {
                XDCE_File = new XDCE(FilePath, true);
                XDCE_File.ParentFolder = new INCELL_Folder(XDCE_File.FilePath, XDCE_File.NameString);
                ImageFolder = new DirectoryInfo(XDCE_File.FilePath).FullName;
                if (outPath == null) OutPath = ImageFolder;
            }

            public XDCE_ImageStitcher(INCELL_Folder folder, string outPath = null)
            {
                XDCE_File = folder.XDCE;
                if (folder.isNewFormat)
                {
                    ImageFolder = new DirectoryInfo(folder.timePointFullPath).FullName;
                }
                else
                {
                    ImageFolder = new DirectoryInfo(XDCE_File.FilePath).FullName;
                }
                if (outPath == null) OutPath = ImageFolder;
            }
            /// <summary>
            /// Stitches together all images for every channel
            /// </summary>
            /// <param name="openAtEnd">true if you want the image to be opened at the end</param>
            public void StitchAll(bool openAtEnd = false)
            {
                for (int i = 0; i < XDCE_File.Wavelength_Count; i++)
                {
                    Stitch(i, openAtEnd);
                }
            }

            /// <summary>
            /// Stitches all the images together and saves them
            /// </summary>
            /// <param name="wavelength">An integer specifying the </param>
            /// <param name="openAtEnd">true if you want the image to be opened at the end</param>
            public void Stitch(int wavelength = 0, bool openAtEnd = false)
            {
                Bitmap finalImage = new Bitmap(ImageWidth, ImageHeight);
                var font = new System.Drawing.Font("Sans Serif", 20);
                SolidBrush brush = new SolidBrush(Color.Purple);
                using (Graphics g = Graphics.FromImage(finalImage))
                {
                    foreach (XDCE_Image image in XDCE_File.Images)
                    {
                        if (image.wavelength != wavelength) continue; // Only use specified images

                        string fullImagePath = Path.Combine(ImageFolder, image.FileName);
                        Bitmap field = new Bitmap(fullImagePath);
                        Bitmap resized = new Bitmap(field, scaledWidth, scaledHeight);

                        Point pixelLoc = MicronToPixelLocation(image.PlateX_um, image.PlateY_um);
                        pixelLoc -= new Size(60 * image.ColNumber, 70 * image.RowNumber);
                        g.DrawImage(resized, pixelLoc);
                        g.DrawString($"{"ABCDEFGH"[image.RowNumber]}{image.ColNumber + 1}", font, brush, pixelLoc);
                        resized.Dispose();
                        field.Dispose();

                    }
                }

                // This is just so that the file doesn't get saved over if you run it again
                string name = Path.Combine(OutPath, $"{XDCE_File.NameString}_{wavelength}-" + "{0}.png");
                int counter = 1;
                string fname = String.Format(name, counter);
                while (File.Exists(fname))
                {
                    counter++;
                    fname = String.Format(name, counter);
                }

                finalImage.Save(fname);
                finalImage.Dispose();

                if (openAtEnd) System.Diagnostics.Process.Start(fname);
            }

            /// <summary>
            /// Converts a plate location (microns) to a location in the final stitched image
            /// </summary>
            /// <param name="platLocX">The x-coordinate (microns) of the top-left corner of the image</param>
            /// <param name="platLocY">The y-coordinate (microns) of the top-left corner of the image</param>
            /// <returns>A Point representing the pixel location in the final stitched image</returns>
            public Point MicronToPixelLocation(double platLocX, double platLocY)
            {
                int pixelX = (int)(ImageWidth * (platLocX - XDCExMin) / (XDCExMax - XDCExMin));
                int pixelY = (int)(ImageHeight * (platLocY - XDCEyMin) / (XDCEyMax - XDCEyMin));
                return new Point(pixelX, pixelY);
            }

        }

    }

    namespace HCS
    {
        public static class CellomicsImageFormat
        {
            public static void CMS_To_Cellomics()
            {
                string Source = @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\CMS_Machine\CMSData\SORT001\190214-1734\";
                string Dest = @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\CMS_Machine\CellomicsConvert\";
                CMS_To_Cellomics(Source, Dest); //Will create subfolder
            }

            public static void CMS_To_Cellomics(string SourcePath, string DestPath)
            {
                CMS_To_Cellomics(SourcePath, DestPath, "default", false, null);
            }

            public static void CMS_To_Cellomics(string SourcePath, string DestPath, string PlateName, bool CropSquare, System.ComponentModel.BackgroundWorker BG)
            {
                BG.ReportProgress(0, "Starting File Renaming . . ");

                DirectoryInfo DI = new DirectoryInfo(SourcePath);
                DirectoryInfo DINew = new DirectoryInfo(Path.Combine(DestPath, DI.Name));
                if (!DINew.Exists) DINew.Create();
                List<FileInfo> Files = DI.GetFiles().ToList();

                //Figure out the new naming
                Dictionary<string, string> NewRows, NewCols;
                NamingConversion(Files, out NewRows, out NewCols);

                //Now actually move and rename
                try
                {
                    int idx = 0;
                    foreach (FileInfo fi in Files)
                    {
                        if (Update_Check(BG, Files, ref idx, fi)) break;
                        CMS_To_Cellomics_ProcessFile(fi, DINew, PlateName, CropSquare, NewRows, NewCols);
                    }
                }
                catch (Exception E)
                {
                    BG.ReportProgress(0, "Error during process.  Files may already exist at this location or the folder isn't writeable.\r\n" + E.Message);
                }
            }

            private static bool Update_Check(System.ComponentModel.BackgroundWorker BG, List<FileInfo> Files, ref int idx, FileInfo fi)
            {
                string Update;
                if ((idx++ % 10) == 0)
                {
                    Update = ((double)idx / Files.Count).ToString("00.0%") + "\t" + fi.Name;
                    if (BG != null) BG.ReportProgress(0, Update);
                    Debug.Print(Update);
                }
                if (BG != null) if (BG.CancellationPending) return true;
                return false;
            }

            private static void NamingConversion(List<FileInfo> Files, out Dictionary<string, string> NewRows, out Dictionary<string, string> NewCols)
            {
                SortedList<string, string> RowSort = new SortedList<string, string>();
                SortedList<string, string> ColSort = new SortedList<string, string>();
                string Row, Column, Channel;
                foreach (FileInfo fi in Files)
                {
                    Row = fi.Name.Substring(0, 2); if (!RowSort.ContainsKey(Row)) RowSort.Add(Row, "");
                    Column = fi.Name.Substring(2, 2); if (!ColSort.ContainsKey(Column)) ColSort.Add(Column, "");
                    Channel = fi.Name.Substring(4, 1);
                }
                int i = 0;
                NewRows = new Dictionary<string, string>();
                foreach (string Key in RowSort.Keys) NewRows.Add(Key, ((char)(65 + i++)).ToString());
                i = 0;
                NewCols = new Dictionary<string, string>();
                foreach (string Key in ColSort.Keys) NewCols.Add(Key, (++i).ToString("00"));
            }

            private static void CMS_To_Cellomics_ProcessFile(FileInfo fi, DirectoryInfo dINew, string PlateName, bool CropSquare, Dictionary<string, string> NewRows, Dictionary<string, string> NewCols)
            {
                string Row, Column, Channel, NewRow, NewColumn, NewCh = "0", NewName;
                Row = fi.Name.Substring(0, 2);
                Column = fi.Name.Substring(2, 2);
                Channel = fi.Name.Substring(4, 1);
                if (Channel == "A") return;

                NewRow = NewRows[Row];
                NewColumn = NewCols[Column];

                //F0A0 F .TIFF
                switch (Channel)
                {
                    case "F":
                        NewCh = "2";
                        break;
                    case "B":
                        NewCh = "3";
                        break;
                    case "G":
                        NewCh = "0";
                        break;
                    case "R":
                        NewCh = "1";
                        break;
                    default:
                        break;
                } //mav104p5redrem_ b02f01d1.tif
                NewName = PlateName + "_" + NewRow.ToLower() + NewColumn + "f00" + "d" + NewCh + ".bmp";

                string NewFileName = Path.Combine(dINew.FullName, NewName);
                if (CropSquare)
                {
                    ImageHelper.CopyCropSquare(fi.FullName, NewFileName);
                }
                else
                {
                    fi.CopyTo(NewFileName);
                }
            }
        }

        public static class ImageHelper
        {
            public static void CropPlay()
            {
                string iPath = @"C:\Users\william\Desktop\Rest\Graphics\ECB 005.jpg";
                //Image image = Image.FromFile(iPath);
                Bitmap bitmap = new Bitmap(iPath);
                Image cropped = bitmap.Clone(new Rectangle(100, 200, 30, 400), bitmap.PixelFormat);
                cropped.Save(iPath + "_crop.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
            }

            public static void CopyCrop(string FileSource, string FileDest, Rectangle CropRect)
            {
                Bitmap bitmap = new Bitmap(FileSource);
                CopyCrop(bitmap, FileDest, CropRect);
            }

            public static void CopyCrop(Bitmap bitmap, string FileDest, Rectangle CropRect)
            {
                Image cropped = bitmap.Clone(CropRect, bitmap.PixelFormat);
                //cropped.Save(FileDest, bitmap.RawFormat  /* System.Drawing.Imaging.ImageFormat.Bmp*/);
                cropped.Save(FileDest, System.Drawing.Imaging.ImageFormat.Bmp);
            }

            public static void CopyCropSquare(string FileSource, string FileDest)
            {
                Bitmap bitmap = new Bitmap(FileSource);
                Rectangle Rect1 = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                int manip = Math.Abs(bitmap.Width - bitmap.Height) / 2;
                if (bitmap.Height > bitmap.Width)
                {
                    Rect1 = new Rectangle(0, manip, bitmap.Width, bitmap.Width);
                }
                else if (bitmap.Height < bitmap.Width)
                {
                    Rect1 = new Rectangle(manip, 0, bitmap.Height, bitmap.Height);
                }
                CopyCrop(bitmap, FileDest, Rect1);
            }
        }
    }

    namespace NGS_Assist
    {
        public class Score_Track
        {
            public static string PredScoreNameSep = "\t";
            public Dictionary<string, SortedList<double, int>> KVPs;
            public Dictionary<string, double> MultiplierHash; //This helps to reverse the scores that are mapped backwards from the others
            public Dictionary<string, Score_Column> Columns;
            public IEnumerable<string> Keys => KVPs.Keys;
            public NGS_Assist_Params AssociatedParams;
            public string ClassAssignedToOne;

            public Score_Track(NGS_Assist_Params Params)
            {
                Columns = new Dictionary<string, Score_Column>();
                MultiplierHash = new Dictionary<string, double>();
                KVPs = new Dictionary<string, SortedList<double, int>>();
                AssociatedParams = Params;
            }

            public void AddScore(string Key, double Value)
            {
                int Mult = (int)Math.Pow(10, AssociatedParams.PRScan_DecimalPlaces);
                double RVer = Math.Round(Math.Round(Value * Mult, 0) / Mult, (int)Math.Ceiling(AssociatedParams.PRScan_DecimalPlaces + 1));

                //string Key = ScoreName + Score_Track.PredScoreNameSep + NameSub;
                if (!KVPs.ContainsKey(Key)) KVPs.Add(Key, new SortedList<double, int>());
                if (!KVPs[Key].ContainsKey(RVer)) KVPs[Key].Add(RVer, 1);
            }

            internal void AddScore(Score_Item sI)
            {
                AddScore(sI.Key, sI.Val);
            }

            internal void AssignMultipliers(NGS_Node_Set nSet_Control)
            {
                bool OldMethod = true;
                MultiplierHash = new Dictionary<string, double>();
                if (OldMethod)
                {
                    //This helps to swap things into the right orientation and also removes these scores and adds the final one
                    double SumZero; double SumOther; int CountZero; int CountOther; double AvgZero, AvgOther; double ValToUse; double Represented; double Min; double Max;
                    foreach (string ScoreType in Keys)
                    {
                        //Take the average for each mlClass within the controls, then if Class X isn't the highest, turn the multiplier to -1
                        SumZero = SumOther = CountZero = CountOther = 0; Min = double.MaxValue; Max = double.MinValue;
                        foreach (NGSBI_Node node in nSet_Control)
                        {
                            if (!node.PredictionScore.ContainsKey(ScoreType))
                            {
                                continue; //AvgZero = AvgOther = 0; break; //Don't know why I was doing this
                            }
                            ValToUse = node.PredictionScore[ScoreType].Val;
                            if (ValToUse > Max) Max = ValToUse; if (ValToUse < Min) Min = ValToUse;
                            if (node.mlClassLayout == ClassAssignedToOne)
                            {
                                SumZero += ValToUse; CountZero++;
                            }
                            else
                            {
                                SumOther += ValToUse; CountOther++;
                            }
                        }
                        Represented = (double)(CountZero + CountOther) / nSet_Control.Count;
                        AvgZero = SumZero / CountZero; AvgOther = SumOther / CountOther;
                        //Debug.Print(ScoreType + " " + AvgZero.ToString("0.0 ") + AvgOther.ToString("0.0"));
                        MultiplierHash.Add(ScoreType, AvgZero > AvgOther ? 1 : -1);
                        //MultiplierHash.Add(ScoreType, 1); //Use to turn this feature off
                    }
                }

                //This is also where we add the simplified score for the control:
                foreach (NGSBI_Node node in nSet_Control) node.ResetScores_AddControl(ClassAssignedToOne);
            }

            internal Score_Column Column_GetCreate(string scoreName, string nameSub)
            {
                string tKey = Score_Column.GenerateKey(scoreName, nameSub);
                if (Columns.ContainsKey(tKey)) return Columns[tKey];
                return new Score_Column(scoreName, nameSub, this);
            }

            internal Score_Column Column_GetCreate(Score_Column toCopyFrom, int randSet)
            {
                string tKey = Score_Column.GenerateKey(toCopyFrom.Name1, toCopyFrom.Name2, randSet);
                if (Columns.ContainsKey(tKey)) return Columns[tKey];
                return new Score_Column(toCopyFrom, randSet);
            }
        }

        public class Score_Column
        {
            public Score_Track Parent;

            public int RandSet = 0;
            public bool ToCopy = false;
            public string Key;
            public string Name1;
            public string Name2;
            public double Multiplier => Parent.MultiplierHash.ContainsKey(Key) ? Parent.MultiplierHash[Key] : 1;

            public Score_Column(string scoreName, string nameSub, Score_Track Parent)
            {
                Key = GenerateKey(scoreName, nameSub);
                this.Parent = Parent;
                Name1 = scoreName.Replace("\"", "").Trim();
                Name2 = nameSub.Trim();
                ToCopy = true;
            }

            public Score_Column(Score_Column ToCopyFrom, int RandSet)
            {
                ToCopy = false;
                Name1 = ToCopyFrom.Name1;
                Name2 = ToCopyFrom.Name2;
                this.RandSet = RandSet;
                Key = GenerateKey(Name1, Name2, RandSet);

                Parent = ToCopyFrom.Parent;
                if (!Parent.MultiplierHash.ContainsKey(Key)) Parent.MultiplierHash.Add(Key, Parent.MultiplierHash.ContainsKey(ToCopyFrom.Key) ? Parent.MultiplierHash[ToCopyFrom.Key] : 1);
            }

            internal static string GenerateKey(string scoreName, string nameSub, int RandomSet_NegativeOne_IsStd = -1)
            {
                return (RandomSet_NegativeOne_IsStd == -1 ? "Std\t" : "Rnd" + RandomSet_NegativeOne_IsStd + "\t") + scoreName + Score_Track.PredScoreNameSep + nameSub;
            }
        }

        public class Score_Item
        {
            public Score_Column Column;
            public string Key { get => Column.Key; }
            public bool ToCopy { get => Column.ToCopy; }

            public double Val
            {
                get
                {
                    //if (Multiplier != 1) Debugger.Break();
                    return (Column.Multiplier == 1 ? _Val : 1 - _Val);
                }
                set { _Val = value; }
            }

            public string OriginalValue { get => _Val.ToString(); }

            private double _Val = double.NaN;


            public Score_Item(string scoreName, string nameSub, string value, Score_Track Parent)
            {
                //TODOFIXA
                Column = Parent.Column_GetCreate(scoreName, nameSub);
                Val = double.Parse(value);
            }

            public Score_Item(Score_Item ToCopyFrom, int RandSet)
            {
                Column = ToCopyFrom.Column.Parent.Column_GetCreate(ToCopyFrom.Column, RandSet);
                _Val = ToCopyFrom._Val;
            }
        }

        public class NGS_Assist_Params
        {
            public string ByIdx_MutantReads = "%MU";
            public string ByIdx_ArraID = "ArrayID";
            public string ByIdx_RaftID = "RaftID";
            public string ByIdx_ClassLayout = "mlClassLayout";
            public string ByIdx_BLabLongID = "BlabLongID";
            public string ByIdx_CellsPerRaft = "CellsPerRaft";

            public string Classes_Controls = "G1|G2";
            public List<string> _xControls { get => Classes_Controls.Split('|').ToList(); }
            public string Classes_ClassForMutant = "G2";
            public string Classes_Mixes = "Mix50|Mix80|Mix90|Mix95|MixHL|MixLH";
            public List<string> _xMixes { get => Classes_Mixes.Split('|').ToList(); }

            public int _ColReads;
            public int _Col1ArrayIndex;
            public int _Col1RaftID;
            public int _ColPredGroup;
            public int _ColLongID_Idx;
            public int _ColCellsPerRaftIdx;

            public string MSR_ArrayID = "mlPlateIndex";
            public string MSR_RaftID = "RaftID";
            public string MSR_ScoreName = "ScoreName";
            public string MSR_Aggregation = "Avg-ScoreValue";  // |Min-ScoreValue|Max-ScoreValue";
            public List<string> _Col2PredScoresNames { get => MSR_Aggregation.Split('|').ToList(); }

            public string _ReadsFile = "";
            public string _ModelFile = "";
            public string _DefaultName = "AUC_results_001.txt";

            public int _Col2ArrayIndex;
            public int _Col2RaftID;
            public int _Col2ScoreSName;
            public List<int> _Col2PredScores = new List<int>();

            public double MUScan_Min = 0.05;
            public double MUScan_Max = 0.95;
            public double MUScan_DecimalPlaces = 1;

            public double PRScan_DecimalPlaces = 2;
            public double PR_LowerMax = 0.4;
            public double PR_UpperMin = 0.6;

            public double RandomModels = 10;
            public double EntropyMinimum = 0.05;

            public string _Delimiter = "\t";

            public NGS_Assist_Params()
            {

            }

            public Dictionary<string, string> GetValues()
            {
                return this.GetType().GetFields().ToDictionary(k => k.Name, v => v.GetValue(this).ToString());
            }

            public static string _DefaultPath = Path.Combine(Path.GetTempPath(), "NGS_Assist_Settings.xml");

            public static NGS_Assist_Params Load()
            {
                return Load(_DefaultPath);
            }

            public static NGS_Assist_Params Load(string Path)
            {
                if (!File.Exists(Path)) return null;
                try
                {
                    using (var stream = File.OpenRead(Path))
                    {
                        var serializer = new XmlSerializer(typeof(NGS_Assist_Params));
                        NGS_Assist_Params Params = (NGS_Assist_Params)serializer.Deserialize(stream);
                        if (Params._Delimiter == null) Params._Delimiter = "\t";
                        if (Params._Delimiter == "") Params._Delimiter = "\t";
                        return Params;
                    }
                }
                catch
                {
                    return null;
                }
            }

            public void Save()
            {
                Save(_DefaultPath);
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }

            internal void SaveCheckLocal()
            {
                SaveCheckLocal("NGS_Assist_Settings.xml");
            }

            internal void SaveCheckLocal(string Name)
            {
                Save(Path.Combine(Local_Results_Folder, Name));
            }

            private static string Local_Result_Folder_LeafName = "8 Results";

            /// <summary>
            /// Gets the name of or creates the local results folder
            /// </summary>
            /// <returns></returns>
            public string Local_Results_Folder
            {
                get
                {
                    string fldr = Path.GetDirectoryName(this._ReadsFile);
                    System.Text.RegularExpressions.MatchCollection MC = _RegexFIVSubFolder.Matches(fldr);
                    if (MC.Count < 1)
                    {
                        return fldr;
                    }
                    else
                    {
                        string tFolder = fldr.Substring(0, MC[MC.Count - 1].Index);
                        tFolder = Path.Combine(tFolder, Local_Result_Folder_LeafName);
                        DirectoryInfo DI = new DirectoryInfo(tFolder);
                        if (!DI.Exists) DI.Create();
                        return tFolder;
                    }
                }
            }

            public NGS_Assist_ParamsList GetList()
            {
                NGS_Assist_ParamsList List = new NGS_Assist_ParamsList();
                foreach (System.Reflection.FieldInfo FI in this.GetType().GetFields())
                {
                    if (!FI.Name.StartsWith("_"))
                    {
                        KVP_Param_Item KI = new KVP_Param_Item(FI.Name, FI.GetValue(this).ToString());
                        List.Add(KI);
                    }
                }
                return List;
            }

            internal void UpdateFrom(IEnumerable<KVP_Param_Item> dataSource)
            {
                NGS_Assist_ParamsList NA_List = new NGS_Assist_ParamsList(dataSource);
                Type t = this.GetType(); System.Reflection.FieldInfo FI;
                foreach (KVP_Param_Item kI in NA_List)
                {
                    FI = t.GetField(kI.Key);
                    if (FI.FieldType.FullName.Contains("String"))
                        FI.SetValue(this, kI.Value);
                    else
                        FI.SetValue(this, double.Parse(kI.Value));
                }
            }

            private static System.Text.RegularExpressions.Regex _RegexFIVName = new System.Text.RegularExpressions.Regex(@"FIV\d\d\d+");
            private static System.Text.RegularExpressions.Regex _RegexFIVSubFolder = new System.Text.RegularExpressions.Regex(@"\\\d+ \D+");

            public void Build_AUCResults_Name_NoExt(out string Result, out string Error)
            {
                Result = "";
                string ByIdx = Path.GetFileNameWithoutExtension(this._ReadsFile);
                string MSR = Path.GetFileNameWithoutExtension(this._ModelFile);

                //Get the FIV code from each name
                Error = "Couldn't find Experiment #"; //If it doesn't have an FIV number, just return
                System.Text.RegularExpressions.Match MI = _RegexFIVName.Match(ByIdx); if (!MI.Success) { return; }
                System.Text.RegularExpressions.Match MM = _RegexFIVName.Match(MSR); if (!MM.Success) { return; }

                //Ensure that the names match
                if (MI.Value != MM.Value) { Error = "FIV # don't match"; return; }

                //Check the log file to get the latest index
                int IdxAppend = LogFile_GetAUCIndex_Increment(this._ReadsFile);

                //Construct new Name
                string delim = " ";
                string FIV = MI.Value;
                ByIdx = ByIdx.Replace(FIV, "").Trim(); if (ByIdx.StartsWith("_")) ByIdx = ByIdx.Substring(1);
                MSR = MSR.Replace(FIV, "").Trim();
                string Append = "PR=" + PR_LowerMax + "-" + PR_UpperMin;
                string IdIdx = "AUC" + IdxAppend.ToString("000");
                Result = FIV + delim + IdIdx + delim + ByIdx + delim + MSR + delim + Append;
                Error = "";
            }

            internal static string LogFile_GetSpecificFilePath(string PathOfAnyFile)
            {
                string Folder = Path.GetDirectoryName(PathOfAnyFile);
                System.Text.RegularExpressions.Match MI = _RegexFIVName.Match(Folder);
                if (MI.Success)
                {
                    Folder = Folder.Substring(0, MI.Index + MI.Length);
                }
                string FileName = Path.Combine(Folder, "5TLog.txt");
                FileInfo FI = new FileInfo(FileName);
                if (!FI.Exists)
                {
                    FI.Create().Close();
                    File.SetAttributes(FileName, File.GetAttributes(FileName) | FileAttributes.Hidden);
                }
                return FileName;
            }

            private static string AUCIndexKey = "AUCIndex\t";
            public static int LogFile_GetAUCIndex_Increment(string PathOfAnyFile)
            {
                int IdxRet = 0;
                string[] Arr = LogFile_Read(PathOfAnyFile);
                if (Arr == null) goto jumpHere;
                if (Arr.Length == 0) goto jumpHere;
                for (int i = Arr.Length - 1; i >= 0; i--)
                {
                    if (Arr[i].Contains(AUCIndexKey))
                    {
                        int l1 = Arr[i].IndexOf(AUCIndexKey) + AUCIndexKey.Length;
                        IdxRet = int.Parse(Arr[i].Substring(l1));
                        break;
                    }
                }
            jumpHere:
                LogFile_Append(PathOfAnyFile, AUCIndexKey + (IdxRet + 1));
                return IdxRet;
            }

            public static string[] LogFile_Read(string PathOfAnyFile)
            {
                string tFile = LogFile_GetSpecificFilePath(PathOfAnyFile);
                //FileInfo FI = new FileInfo(tFile);
                //if (FI.Exists) 
                return File.ReadAllLines(tFile);
                //return null;
            }

            public static bool LogFile_Append(string PathOfAnyFile, string TextToAppend)
            {
                string tFile = LogFile_GetSpecificFilePath(PathOfAnyFile);
                StreamWriter SW = new StreamWriter(tFile, true);
                SW.WriteLine(DateTime.Now + "\t" + TextToAppend);
                SW.Close();
                return true;
            }

        }

        public class KVP_Param_Item
        {
            public KVP_Param_Item(string name, string v)
            {
                Key = name; Value = v;
            }

            public string Key { get; }
            public string Value { get; set; }
        }

        public class NGS_Assist_ParamsList : IEnumerable<KVP_Param_Item>
        {
            List<KVP_Param_Item> List;

            public NGS_Assist_ParamsList()
            {
                List = new List<KVP_Param_Item>();
            }

            public NGS_Assist_ParamsList(IEnumerable<KVP_Param_Item> dataSource)
            {
                List = dataSource.ToList();
            }

            public void Add(KVP_Param_Item Item)
            {
                List.Add(Item);
            }

            public int Count => List.Count;

            public NGS_Assist_Params GetParams()
            {
                NGS_Assist_Params Params = new NGS_Assist_Params(); Type t = Params.GetType();
                System.Reflection.FieldInfo FI;
                foreach (KVP_Param_Item kI in List)
                {
                    FI = t.GetField(kI.Key);
                    FI.SetValue(Params, kI.Value);
                }
                return Params;
            }

            public IEnumerator<KVP_Param_Item> GetEnumerator()
            {
                return List.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return List.GetEnumerator();
            }
        }

        public class NGSBI_Node
        {
            public string ArrayID;
            public string RaftID;
            public string BlabLongID;
            public string Key => ArrayID + "." + RaftID;
            public List<string> Values;
            public double MutantReads;
            public string mlClassLayout;
            public int CellsPerRaft;
            private static Random _Rand = new Random();
            private Score_Track Master;
            public List<NGS_Node_Set> Parent { get; internal set; }

            public override string ToString()
            {
                return Key;
            }

            public Dictionary<string, Score_Item> PredictionScore;

            public NGSBI_Node(string ArrayId, string RaftID, double MutantReads, string mlClassLayout, string BlabLongID, string CellsPerRaft, List<string> Cols, Score_Track scoreMaster)
            {
                if (CellsPerRaft == "" || CellsPerRaft == "UNK") CellsPerRaft = "1";
                this.ArrayID = ArrayId; this.RaftID = RaftID; this.MutantReads = MutantReads; this.mlClassLayout = mlClassLayout; this.BlabLongID = BlabLongID; this.CellsPerRaft = int.Parse(CellsPerRaft);
                Values = Cols; Master = scoreMaster;
                PredictionScore = new Dictionary<string, Score_Item>(); Parent = new List<NGS_Node_Set>();
            }

            //Shallow Copies the node provided
            public NGSBI_Node(NGSBI_Node tNode)
            {
                Type T = this.GetType();
                System.Reflection.FieldInfo[] Fields = tNode.GetType().GetFields();
                foreach (System.Reflection.FieldInfo field in Fields) field.SetValue(this, field.GetValue(tNode));

                Parent = new List<NGS_Node_Set>();

                this.Master = tNode.Master;
                PredictionScore = new Dictionary<string, Score_Item>();
                foreach (Score_Item item in tNode.PredictionScore.Values)
                {
                    AddScore(item.Column.Name1, item.Column.Name2, item.OriginalValue);
                }
            }

            internal void AddScore(string ScoreName, string NameSub, string Value)
            {
                Score_Item SI = new Score_Item(ScoreName, NameSub, Value, Master);
                if (!PredictionScore.ContainsKey(SI.Key))
                {
                    PredictionScore.Add(SI.Key, SI);
                    Master.AddScore(SI);
                }
            }

            internal void AddScoreControl(double ScoreValue)
            {
                AddScore("_Ctl", "Control", ScoreValue.ToString());
            }

            internal void ScoresFromShuffled(NGSBI_Node Node, int RandomSet)
            {
                List<Score_Item> SIs = Node.PredictionScore.Where(x => x.Value.ToCopy).Select(y => y.Value).ToList();
                foreach (Score_Item scoreType in SIs)
                {
                    Score_Item SI2 = new Score_Item(scoreType, RandomSet);
                    PredictionScore.Add(SI2.Key, SI2);
                    Master.AddScore(SI2);
                }
            }

            internal void ResetScores_AddControl(string classAssignedToOne)
            {
                bool DeleteAllScores = false;
                if (DeleteAllScores) PredictionScore = new Dictionary<string, Score_Item>();
                AddScoreControl(mlClassLayout == classAssignedToOne ? (0.9 + (_Rand.NextDouble() / 10)) : (_Rand.NextDouble() / 10));
            }
        }

        public class NGS_Node_Set : ICollection<NGSBI_Node>
        {
            public override string ToString() { return Name + " " + Nodes.Count; }

            public string Name;
            public bool IsControlSet;

            private Dictionary<string, NGSBI_Node> Nodes;
            private List<List<NGSBI_Node>> NodesShuffled;
            private HashSet<string> mlClassLayoutKeys;

            public string mlClassLayout_Keys => string.Join(" ", mlClassLayoutKeys);
            public int Count => Nodes.Count;
            public bool IsReadOnly => false;
            public IEnumerable<NGSBI_Node> Values { get => Nodes.Values; }

            public NGS_Node_Set()
            {
                Init("", false);
            }

            public NGS_Node_Set(string NSetName, bool IsControlSet)
            {
                Init(NSetName, IsControlSet);
            }

            private void Init(string NSetName, bool IsControlSet)
            {
                Name = NSetName; this.IsControlSet = IsControlSet;
                Nodes = new Dictionary<string, NGSBI_Node>();
                NodesShuffled = new List<List<NGSBI_Node>>();
                mlClassLayoutKeys = new HashSet<string>();
            }

            public NGSBI_Node this[string Key]
            {
                get { return Nodes[Key]; }
                //set { Nodes[Key] = value; }
            }

            public void Add(string Key, NGSBI_Node Val)
            {
                //This is the Important Add Function that does everything
                if (!Nodes.ContainsKey(Key))
                {
                    Nodes.Add(Key, Val);
                    mlClassLayoutKeys.Add(Val.mlClassLayout);
                }
            }

            public void Add(NGSBI_Node item)
            {
                Add(item.Key, item);
            }

            public void Clear()
            {
                Nodes = new Dictionary<string, NGSBI_Node>();
            }

            public bool Contains(NGSBI_Node item)
            {
                return Nodes.ContainsValue(item);
            }

            public bool ContainsKey(string Key)
            {
                return Nodes.ContainsKey(Key);
            }

            public void CopyTo(NGSBI_Node[] array, int arrayIndex)
            {
                Debugger.Break();
                throw new NotImplementedException();
            }

            public IEnumerator<NGSBI_Node> GetEnumerator()
            {
                return Nodes.Values.GetEnumerator();
                //return Nodes.GetEnumerator();
            }

            public bool Remove(NGSBI_Node item)
            {
                return Nodes.Remove(item.Key);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return Nodes.GetEnumerator();
            }

            private Random _Rand = new Random();

            //Pa.RandomToInsert
            public void Shuffle(int ShuffledSets)
            {
                NodesShuffled = new List<List<NGSBI_Node>>();
                List<NGSBI_Node> tNodes = Nodes.Values.ToList();
                SortedList<double, NGSBI_Node> tSort;
                for (int i = 0; i < ShuffledSets; i++)
                {
                    tSort = new SortedList<double, NGSBI_Node>();
                    foreach (NGSBI_Node node in tNodes) tSort.Add(_Rand.NextDouble(), node);
                    NodesShuffled.Add(tSort.Values.ToList());
                }
            }

            internal void AddScoresToShuffled()
            {
                if (NodesShuffled.Count < 1) return;
                //Now go through and add the randomized scores . . 
                List<NGSBI_Node> tList = Nodes.Values.ToList();
                for (int j = 0; j < tList.Count; j++)
                {
                    for (int i = 0; i < NodesShuffled.Count; i++) NodesShuffled[i][j].ScoresFromShuffled(tList[j], i);
                }
            }

            public double Calculate_R2(string ScoreKey)
            {
                //https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
                double x = 0; double y = 0; double xy = 0; double x2 = 0; double y2 = 0; double n = 0;
                foreach (NGSBI_Node node in this)
                {
                    if (!node.PredictionScore.ContainsKey(ScoreKey)) continue;
                    double ty = node.PredictionScore[ScoreKey].Val;
                    double tx = node.MutantReads;
                    x += tx; y += ty; xy += (tx * ty); x2 += (tx * tx); y2 += (ty * ty); n++;
                }
                double top = (n * xy) - (x * y);
                double b1 = Math.Sqrt((n * x2) - (x * x));
                double b2 = Math.Sqrt((n * y2) - (y * y));
                return top / (b1 * b2);
            }
        }

        public class NGS_Assist_Start
        {
            private NGS_Node_Set AllNodes;
            private Dictionary<string, NGS_Node_Set> NodeSets;
            private SortedList<double, int> MutantReads;
            private string ImportPathFile;
            public NGS_Assist_Params Pa = new NGS_Assist_Params();
            private Score_Track _Master;
            private Random _Rand = new Random();

            public string ErrorLast;
            public int ScoresCount => _Master.KVPs.Count;
            public int ScoreSteps => Math.Max(_Master.KVPs.First().Value.Count, _Master.KVPs.Last().Value.Count);
            public int IndexCount => AllNodes.Count;
            public string ClassKeys => AllNodes.mlClassLayout_Keys;
            public int MutantSteps => MutantReads.Count;
            public double TotalCountKilo => Math.Round((double)(ScoresCount * ScoreSteps * MutantSteps) / 1000, 0);

            public static void AAA_GO()
            {
                //string PathMutants = @"\\storage1.ris.wustl.edu\wbuchser\Active\five\EXP\FIV238\FIV238 NGS_ByIndex Export01.txt";
                string PathMutants = @"\\storage1.ris.wustl.edu\wbuchser\Active\five\EXP\FIV238\NGSbyIndex_JCB2.txt";
                string PathScores = @"\\storage1.ris.wustl.edu\wbuchser\Active\five\EXP\FIV238\FIV238 20191209 RafLong.txt";
                NGS_Assist_Start NGSA = new NGS_Assist_Start(PathMutants);
                NGSA.ImportScores(PathScores);
                NGSA.Calculate_Export(PathMutants + "_res2.txt");
            }

            public NGS_Assist_Start(string ImportPath)
            {
                ErrorLast = "";
                Pa = new NGS_Assist_Params();
                ImportPathFile = ImportPath;
                ImportReads();
            }

            public NGS_Assist_Start(string ImportPath, NGS_Assist_Params Params)
            {
                Pa = Params; ImportPathFile = ImportPath;
                ImportReads();
            }

            private static string ControlsKey = "Controls";
            private static string MixesKey = "Mixes All";
            private static string MixesKey_Singlecell = "Mixes Single Cell";
            private static string MixesKey_Multicell = "Mixes Multi Cell";

            internal void AddToSubsets_1(NGSBI_Node tNode, string mlClassLayout, string BlabLongID)
            {
                string NodeSetKey = Pa._xControls.Contains(mlClassLayout) ? ControlsKey : mlClassLayout;
                //First add to main NodeSet (Specific Mix vs. Control)
                AddToSubset_Internal(tNode, NodeSetKey);
            }

            internal void AddToSubsets_2(NGSBI_Node tNode)
            {
                string NodeSetKey = Pa._xControls.Contains(tNode.mlClassLayout) ? ControlsKey : tNode.mlClassLayout;
                if (NodeSetKey != ControlsKey)
                {
                    //Now add to the Combined Mixes
                    AddToSubset_Internal(tNode, MixesKey, true);

                    //Add to the Single Cell / Multi Mixes
                    AddToSubset_Internal(tNode, tNode.CellsPerRaft == 1 ? MixesKey_Singlecell : MixesKey_Multicell, true);

                    //Now add to the plate-specific set
                    AddToSubset_Internal(tNode, tNode.BlabLongID, true);
                }
            }

            private void AddToSubset_Internal(NGSBI_Node tNode, string NodeSetKey, bool MakeCopyBeforeAdd = false)
            {
                if (!NodeSets.ContainsKey(NodeSetKey)) NodeSets.Add(NodeSetKey, new NGS_Node_Set(NodeSetKey, NodeSetKey == ControlsKey));
                NGSBI_Node cNode;
                if (MakeCopyBeforeAdd)
                    cNode = new NGSBI_Node(tNode); //Copy rather than use directly
                else
                    cNode = tNode;

                NodeSets[NodeSetKey].Add(cNode); cNode.Parent.Add(NodeSets[NodeSetKey]);
            }

            private void ShuffleNodes()
            {
                //Don't shuffle the all nodes, since no calculations will be made with that one, it is just a placeholder to access everyone
                foreach (NGS_Node_Set Nodeset in NodeSets.Values)
                {
                    //if (!Nodeset.IsControlSet) - - Yes, we can generate for controls as well . . 
                    Nodeset.Shuffle((int)Pa.RandomModels);
                }
            }

            private void ImportReads()
            {
                string[] Lines = File.ReadAllLines(ImportPathFile);
                List<string> Col = Lines[0].Split(Pa._Delimiter[0]).ToList();
                Pa._ColReads = Col.IndexOf(Pa.ByIdx_MutantReads); if (Pa._ColReads == -1) { ErrorLast = "MutantReads?"; return; }
                Pa._ColPredGroup = Col.IndexOf(Pa.ByIdx_ClassLayout); if (Pa._ColPredGroup == -1) { ErrorLast = "_ColPredGroup?"; return; }
                Pa._Col1ArrayIndex = Col.IndexOf(Pa.ByIdx_ArraID); if (Pa._Col1ArrayIndex == -1) { ErrorLast = "_Col1ArrayIndex?"; return; }
                Pa._Col1RaftID = Col.IndexOf(Pa.ByIdx_RaftID); if (Pa._Col1RaftID == -1) { ErrorLast = "_Col1RaftID?"; return; }
                Pa._ColLongID_Idx = Col.IndexOf(Pa.ByIdx_BLabLongID); if (Pa._ColLongID_Idx == -1) { ErrorLast = "_ColLongID_Idx?"; return; }
                Pa._ColCellsPerRaftIdx = Col.IndexOf(Pa.ByIdx_CellsPerRaft); if (Pa._ColCellsPerRaftIdx == -1) { ErrorLast = "_ColCellsPerRaftIdx?"; return; }

                AllNodes = new NGS_Node_Set();
                NodeSets = new Dictionary<string, NGS_Node_Set>(); NodeSets.Add(ControlsKey, new NGS_Node_Set(ControlsKey, true));
                MutantReads = new SortedList<double, int>();
                _Master = new Score_Track(Pa);
                _Master.ClassAssignedToOne = Pa._xControls.Contains(Pa.Classes_ClassForMutant) ? Pa.Classes_ClassForMutant : Pa._xControls[0];

                //Need a combined node set for all mixes, need to split out node sets for each BLabLongID

                NGSBI_Node tNode; double MU; string ArrayID; string RaftID; string mlClassLayout; string BlabLongID; string CellsPerRaft;
                foreach (string Line in Lines)
                {
                    //First make sure this node should be included at all
                    Col = Line.Split(Pa._Delimiter[0]).ToList();
                    mlClassLayout = Col[Pa._ColPredGroup];
                    if (!Pa._xControls.Contains(mlClassLayout) && !Pa._xMixes.Contains(mlClassLayout)) continue;

                    //Make a new Node and add it to the main set
                    MU = double.Parse(Col[Pa._ColReads]); AddToList(MutantReads, MU, Pa.MUScan_Min, Pa.MUScan_Max);
                    ArrayID = Col[Pa._Col1ArrayIndex]; RaftID = Col[Pa._Col1RaftID]; BlabLongID = Col[Pa._ColLongID_Idx]; CellsPerRaft = Col[Pa._ColCellsPerRaftIdx];
                    tNode = new NGSBI_Node(ArrayID, RaftID, MU, mlClassLayout, BlabLongID, CellsPerRaft, Col, _Master);
                    AllNodes.Add(tNode.Key, tNode); //Calculations shouldn't be done with this one, just to store all the good nodes

                    //Add it to the correct subset
                    AddToSubsets_1(tNode, mlClassLayout, BlabLongID);
                }
            }

            public string ImportScores(string ScoresPath)
            {
                StreamReader SR = new StreamReader(ScoresPath);
                //string[] Lines = File.ReadAllLines();
                string line = SR.ReadLine();
                List<string> Col = line.Split(Pa._Delimiter[0]).ToList();
                Pa._Col2ArrayIndex = Col.IndexOf(Pa.MSR_ArrayID); if (Pa._Col2ArrayIndex < 0) return "Check Array Index Name.";
                Pa._Col2RaftID = Col.IndexOf(Pa.MSR_RaftID); if (Pa._Col2RaftID < 0) return "Check RaftID Name.";
                Pa._Col2ScoreSName = Col.IndexOf(Pa.MSR_ScoreName); if (Pa._Col2ScoreSName < 0) return "Check Score Naming Column.";
                Pa._Col2PredScores = new List<int>();
                foreach (string scorename in Pa._Col2PredScoresNames)
                {
                    Pa._Col2PredScores.Add(Col.IndexOf(scorename));
                    if (Pa._Col2PredScores.Last() < 0) return "Check Score Value Column.";
                }

                //Basically link up the Nodes (Dictionary), and add a dictionary of the Scores
                string Key; string Name; int NodesProcessed = 0;
                while (!SR.EndOfStream)
                {
                    line = SR.ReadLine();
                    Col = line.Split(Pa._Delimiter[0]).ToList();
                    Key = Col[Pa._Col2ArrayIndex] + "." + Col[Pa._Col2RaftID];
                    if (AllNodes.ContainsKey(Key))
                    {
                        NodesProcessed++;
                        Name = Col[Pa._Col2ScoreSName];
                        //if (Name != "Ensemble G2") continue;
                        //- - I was thinking that I didn't need to add to controls . . but I temporarily need to so I can determine the right multiplier >>  if (!AllNodes[Key].Parent.IsControlSet)
                        for (int i = 0; i < Pa._Col2PredScoresNames.Count; i++)
                            AllNodes[Key].AddScore(Name, Pa._Col2PredScoresNames[i], Col[Pa._Col2PredScores[i]]);
                    }
                }
                SR.Close();
                if (NodesProcessed == 0) return "Array.RaftID in Score file don't match what was in the read file.";

                //Now reverse any scores that are coded backwards
                foreach (NGS_Node_Set nSet in NodeSets.Values)
                {
                    if (nSet.IsControlSet) { _Master.AssignMultipliers(nSet); break; }
                }

                //Now Add to the Other Subsets (copies of these nodes with their scores)
                foreach (NGSBI_Node node in AllNodes) { AddToSubsets_2(node); }

                ShuffleNodes(); //For Background Predictions

                //Now add the scores onto the Shuffled nodes
                foreach (NGS_Node_Set nSet in NodeSets.Values) { nSet.AddScoresToShuffled(); }

                return "";
            }

            public void Calculate_Export(string ExportPath, System.ComponentModel.BackgroundWorker BW = null)
            {
                //Now go through and generate the results
                double TP; double TN; double FP; double FN; int i = 0; double Acc;
                double Sen; double Spec; double Prec; double Entropy;
                int MCounter = 0; double auc; double PRScan; double r2; double n;
                double TPprev; double TNprev; double FPprev; double FNprev;
                List<double> x, y; List<string> LinesToWrite; string d = "\t";

                StreamWriter SW = new StreamWriter(ExportPath);
                SW.WriteLine("NodeSet" + d + "ScoreType" + d + "ScoreName" + d + "ScoreAgg" + d + "i" + d + "MUScan" + d + "Entropy" + d + "PRScan" + d + "n this" + d + "n all" + d + "TP" + d + "TN" + d + "FP" + d + "FN" + d + "Accuracy" + d + "Precision" + d + "Sensitivity" + d + "Specificity" + d + "AUC" + d + "r2" + d + "Limits");
                foreach (double MUScan in MutantReads.Keys)
                {
                    foreach (NGS_Node_Set nSet in NodeSets.Values)
                    {
                        if (BW != null) BW.ReportProgress((int)(100 * (double)MCounter++ / (MutantReads.Keys.Count * NodeSets.Count)));
                        foreach (string ScoreName in _Master.Keys)
                        {
                            //Debug.Print(nSet.Name + "  " + ScoreName);
                            x = new List<double>(); y = new List<double>(); LinesToWrite = new List<string>(); TPprev = TNprev = FPprev = FNprev = -1; n = 0;
                            foreach (double PRScanVar in _Master.KVPs[ScoreName].Keys)
                            {
                                PRScan = PRScanVar; //if (nSet.IsControlSet) PRScan = 0.5;
                                CountNodes(nSet, MUScan, ScoreName, PRScan, out TP, out TN, out FP, out FN);
                                if (TP == TPprev && TN == TNprev && FP == FPprev && FN == FNprev) continue; //Don't record if it is exactly the same . . just chose PRscan that is too close together
                                n = TP + TN + FP + FN; Acc = (TP + TN) / n; Entropy = 2 * (0.5 - Math.Abs(((TP + FN) / n) - 0.5));
                                Prec = (TP + FP) == 0 ? 0 : TP / (TP + FP); Sen = TP / (TP + FN); Spec = TN / (TN + FP);
                                if (Entropy < Pa.EntropyMinimum) continue; if (double.IsNaN(Sen) || double.IsNaN(Spec)) continue; //Don't record if these things are not met
                                x.Add(1 - Spec); y.Add(Sen);
                                LinesToWrite.Add(nSet.Name + d + ScoreName + d + i++ + d + MUScan + d + Entropy + d + PRScan + d + n + d + nSet.Count + d + TP + d + TN + d + FP + d + FN + d + Acc + d + Prec + d + Sen + d + Spec);
                                TPprev = TP; TNprev = TN; FPprev = FP; FNprev = FN;
                            }
                            auc = -AUC(x, y);
                            r2 = nSet.Calculate_R2(ScoreName);
                            foreach (string Line in LinesToWrite) SW.WriteLine(Line + d + auc + d + r2 + d + (Pa.PR_LowerMax + "/" + Pa.PR_UpperMin));
                        }
                    }
                }
                SW.Close();
            }

            public static double AUC(List<double> x, List<double> y)
            {
                if (x.Count == 0) return -0.5;
                if (x[0] != 1 || y[0] != 1) { x.Insert(0, 1); y.Insert(0, 1); }
                if (x[x.Count - 1] != 0 || y[y.Count - 1] != 0) { x.Add(0); y.Add(0); }

                //Trapezoid rule
                double sm = 0; double h;
                for (int i = 1; i < x.Count; i++)
                {
                    h = x[i] - x[i - 1];
                    sm += h * (y[i - 1] + y[i]) / 2;
                }
                return sm;
            }

            private void CountNodes(NGS_Node_Set nSet, double mUScan, string ScoreNameKey, double pRScan, out double tP, out double tN, out double fP, out double fN)
            {
                tP = tN = fP = fN = 0; double prScore;
                foreach (NGSBI_Node node in nSet.Values)
                {
                    if (!node.PredictionScore.ContainsKey(ScoreNameKey)) continue; //If the score doesn't exist then you can't count this node
                    prScore = node.PredictionScore[ScoreNameKey].Val;
                    if (prScore > Pa.PR_LowerMax && prScore < Pa.PR_UpperMin) continue; //If the score is within the IGNORE region of the scores (usually the middle), then don't consider this and don't add it to the total
                    if (node.MutantReads >= mUScan)
                    {
                        if (prScore >= pRScan) tP++; else fN++;
                    }
                    else
                    {
                        if (prScore < pRScan) tN++; else fP++;
                    }
                }
            }

            public void AddToList(SortedList<double, int> SList, double Value, double Min, double Max)
            {
                int Mult = (int)Math.Pow(10, Pa.MUScan_DecimalPlaces);
                double RVer = Math.Round(Math.Round(Value * Mult, 0) / Mult, (int)Math.Ceiling(Pa.MUScan_DecimalPlaces + 1));
                if ((RVer < Min) || (RVer > Max)) return; //Only add it if it is in the specified range
                if (RVer > 1) Debugger.Break();
                if (RVer < 0) Debugger.Break();
                if (!SList.ContainsKey(RVer)) SList.Add(RVer, 0);
            }
        }
    }


    namespace TrackCells
    {
        public class CellMatcher
        {
            public static string Start(Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> pCI1, Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> pCI2, string ExportFolder)
            {
                //This only works on one plate . . 
                var S1 = pCI1.First().Value; string S1plate = pCI1.First().Key;
                var S2 = pCI2.First().Value; string S2plate = pCI2.First().Key;
                var BothScans = new List<Dictionary<string, HashSet<CellInfoS>>>() { S1, S2 };

                foreach (var wellField in S1.Keys)
                {
                    Debug.Print(wellField + " start . . ");
                    if (!S2.ContainsKey(wellField))
                    {
                        return "Fields and Wells must match up.";
                    }
                    OperateOnPairedFields(S1[wellField], S2[wellField]);
                    Debug.Print(wellField);
                }

                Debug.Print("Exporting . . ");
                var sB = new StringBuilder(); char d = '\t';
                foreach (var wellField in S1.Keys)
                {
                    for (int i = 0; i < BothScans.Count; i++)
                    {
                        foreach (var C in BothScans[i][wellField])
                        {
                            if (sB.Length < 1)
                                sB.Append("PlateID" + d + "WELL LABEL" + d + "FOV" + d + "Object ID" + d + "X" + d + "Y" + d + "ConnectID" + d + "ConnectDist" + "\r\n");
                            sB.Append(C.PlateID + d + C.WellLabel + d + C.FOV + d + C.ObjectID + d + C.CenterPoint.X + d + C.CenterPoint.Y + d + C.ConnectID + d + C.ConnectDist + "\r\n");
                        }
                    }
                }

                File.WriteAllText(ExportFolder, sB.ToString());
                return "";
            }

            public static void StartTest()
            {
                string pth = "R:\\five\\exp\\fiv608\\FIV608 sample data.txt";
                string[] arr = File.ReadAllLines(pth);

                var FullList = new List<CellInfoS>(arr.Length);
                int idx = 0; float Min = float.MaxValue, Max = float.MinValue;
                foreach (var line in arr)
                {
                    var data = line.Split(',');
                    var C = new CellInfoS("P", "A1", data[0], idx++.ToString(), data[1], data[2], "1", "1");
                    FullList.Add(C);
                    if (C.CenterPoint.X < Min) Min = C.CenterPoint.X;
                    if (C.CenterPoint.X > Max) Max = C.CenterPoint.X;
                }

                var listA = FullList.Where(x => x.FOV == "1");
                var listB = FullList.Where(x => x.FOV == "2");
                OperateOnPairedFields(listA, listB);
            }

            public static void AssignNearestNeighbors(IEnumerable<CellInfoS> listA, IEnumerable<CellInfoS> listB)
            {
                long ConnectID = -1;

                Debug.Print("Calculating Tree . . " + listA.Count());
                //KDTree tree = Testing1(listA);
                var tree = new KDTree(listA);
                Debug.Print("Assigning . . ");
                foreach (var item in listB)
                {
                    var nn = tree.FindNearest(item.CenterPoint);
                    item.NearestNeighbor = nn;
                    item.NNDist = KDTree.Distance(item.CenterPoint, nn.CenterPoint);
                    if (nn.NearestNeighbor == null)
                    {
                        nn.ConnectID = ++ConnectID;
                        nn.NearestNeighbor = item;
                        nn.NNDist = item.NNDist;
                    }
                    item.ConnectID = nn.ConnectID;
                    item.ConnectDist = item.NNDist;
                    item.PlateID += "B";
                }
            }

            private static KDTree Testing1(IEnumerable<CellInfoS> listA)
            {
                var subListA = listA.Take(12);

                KDTree tree = new KDTree(subListA);
                tree.PrintTree();
                var pointsFromTree = tree.Traverse();

                foreach (var item in subListA)
                {
                    var nearest = tree.FindNearest(item.CenterPoint);
                    Debug.Print("" + item.ObjectID + " " + (nearest.ObjectID == item.ObjectID));
                }

                int checker = 0;
                foreach (var item in listA)
                {
                    if (pointsFromTree.Contains(item)) checker++;
                }

                return tree;
            }

            public static void OperateOnPairedFields(IEnumerable<CellInfoS> listA, IEnumerable<CellInfoS> listB)
            {
                bool UseNew20231011 = true;

                if (UseNew20231011)
                {
                    CellInfoS.BruteForce_MatchB_to_A(listA, listB);

                    //AssignNearestNeighbors(listA, listB);
                    return;
                }

                float Min = float.MaxValue, Max = float.MinValue;
                foreach (var cell in listA)
                {
                    if (cell.CenterPoint.X > Max) Max = cell.CenterPoint.X;
                    else if (cell.CenterPoint.X < Min) Min = cell.CenterPoint.X;
                }

                int PixelsPerBin = 32;
                int StartRes = Math.Min(50, (int)((Max - Min) / PixelsPerBin));
                int Span = PixelsPerBin / 4;

                var tSearch = new List<int>(); for (int i = 0; i < 1 + (Span * 2); i++) tSearch.Add(i - Span);
                var Search = tSearch.ToArray();

                var Best = new SortedList<double, Tuple<int, int>>(); var Best2 = new SortedList<double, Tuple<int, int>>();
                var AlreadyTried = new HashSet<Tuple<int, int>>(); var AlreadyTried2 = new HashSet<Tuple<int, int>>();

                var HT = new CellMatcher(StartRes, listA, listB);
                HT.GridSearch(Search, Search, Best, AlreadyTried);

                int NextRes = StartRes / 3;

                //Setup a Hashset and a Sorted list that this will add to . . 
                var HT2 = new CellMatcher(NextRes, listA, listB);
                for (int i = 0; i < 4; i++)
                {
                    int XO = StartRes * Best.Values[i].Item1, YO = StartRes * Best.Values[i].Item2;
                    var S2x = ZoomInList(XO, StartRes, NextRes);
                    var S2y = ZoomInList(YO, StartRes, NextRes);

                    HT2.GridSearch(S2x, S2y, Best2, AlreadyTried2);
                }

                //Now we have the best shift, we can use it at the higher resolution to start matching up cells
                HT2.ApplyShift(Best2.First().Value);
                HT2.FindPartners();
            }

            public void Export(string v)
            {
                char d = '\t';
                var sB = new StringBuilder();
                long NextNotConnectID = -1;
                foreach (var KVP in PointsAll)
                {
                    foreach (var C in KVP.Value)
                    {
                        if (C.ConnectID >= 0) C.ConnectDist = C.NNDist;
                        else C.ConnectID = NextNotConnectID--;
                        if (sB.Length < 1) sB.Append("PlateID" + d + "WellLabel" + d + "FOV" + d + "ObjectID" + d + "X" + d + "Y" + d + "ConnectID" + d + "ConnectDist" + "\r\n");
                        else sB.Append(C.PlateID + d + C.WellLabel + d + C.FOV + d + C.ObjectID + d + C.CenterPoint.X + d + C.CenterPoint.Y + d + C.ConnectID + d + C.ConnectDist + "\r\n");
                    }
                }
                File.WriteAllText(v, sB.ToString());
            }

            private long NextConnectID = 0;

            public void FindPartners()
            {
                //Run this after finding and applying the shift

                //Lets go through different 'phases and see what matches
                //With Phase 0 we got 81 total to match (40 ish from each)
                var T0 = PointSets[0];
                for (int i = 0; i < Resolution; i++)
                {
                    var T1 = PointsTemp;
                    FindPartners_Phase(T0, T1);
                }
            }

            private void FindPartners_Phase(Dictionary<PointF, List<CellInfoS>> T0, Dictionary<PointF, List<CellInfoS>> T1)
            {
                var ToAddress = new HashSet<PointF>();
                var Matched = new HashSet<PointF>();
                foreach (var point in T0.Keys)
                {
                    if (T1.ContainsKey(point)) Matched.Add(point);
                    else ToAddress.Add(point);
                }

                foreach (var point in Matched)
                {
                    var T0a = T0[point];
                    var T1a = T1[point];
                    if (T0a.Count == T1a.Count)
                    {
                        if (T0a.Count == 1)
                        {
                            //These two are together in the Bin, so assign them
                            T0a[0].NearestNeighbor = T1a[0]; T0a[0].NNDist = 1; T0a[0].ConnectID = NextConnectID;
                            T1a[0].NearestNeighbor = T0a[0]; T1a[0].NNDist = 1; T1a[0].ConnectID = NextConnectID++;
                        }
                        else
                        {
                            //These have the same # in the Bin, so make sure they are in the right order and assign
                            var s0A = new SortedList<float, CellInfoS>();
                            var s1A = new SortedList<float, CellInfoS>();
                            for (int i = 0; i < T0a.Count; i++)
                            {
                                s0A.Add(T0a[i].CenterPoint.X + 10000 * T0a[i].CenterPoint.Y, T0a[i]); s1A.Add(T1a[i].CenterPoint.X + 10000 * T1a[i].CenterPoint.Y, T1a[i]);
                            }
                            for (int i = 0; i < T0a.Count; i++)
                            {
                                s0A.Values[i].NearestNeighbor = s1A.Values[i]; s0A.Values[i].NNDist = 0.8; s0A.Values[i].ConnectID = NextConnectID;
                                s1A.Values[i].NearestNeighbor = s0A.Values[i]; s1A.Values[i].NNDist = 0.8; s1A.Values[i].ConnectID = NextConnectID++;
                            }
                        }
                    }
                    else
                    {
                        //Different numbers, so assign a poorer score
                        var Fewer = T0a.Count < T1a.Count ? T0a : T1a;
                        var More = T0a.Count >= T1a.Count ? T0a : T1a;
                        if (Fewer.Count == 1)
                        {
                            for (int i = 0; i < More.Count; i++)
                            {
                                More[i].NearestNeighbor = Fewer[0]; More[i].NNDist = 0.5; More[i].ConnectID = NextConnectID;
                            }
                            Fewer[0].NearestNeighbor = More[0]; Fewer[0].NNDist = 0.4; Fewer[0].ConnectID = NextConnectID++;
                        }
                        else
                        {

                        }
                    }
                }
            }

            public void ApplyShift(Tuple<int, int> LocShift_ByRes)
            {
                var K = SumProd(LocShift_ByRes.Item1, LocShift_ByRes.Item2, true);
                //var T1 = PointsTemp;
                //var T2 = PointSets[1];
            }

            public static int[] ZoomInList(float BestPrevious, float PrevRes, float NewRes)
            {
                var T = new List<int>();
                for (float i = BestPrevious - PrevRes; i < BestPrevious + PrevRes; i += NewRes) T.Add((int)(i / NewRes));
                return T.ToArray();
            }

            public CellMatcher(float Resolution, IEnumerable<CellInfoS> listA, IEnumerable<CellInfoS> listB)
            {
                this.Min = float.MaxValue; this.Max = float.MinValue;
                this.Resolution = Resolution;
                ListSets = new List<IEnumerable<CellInfoS>>(2);
                ListSets.Add(listA); ListSets.Add(listB);

                PointsAll = new Dictionary<PointF, List<CellInfoS>>();
                PointSets = new List<Dictionary<PointF, List<CellInfoS>>>(ListSets.Count);
                for (int i = 0; i < ListSets.Count; i++)
                {
                    PointSets.Add(new Dictionary<PointF, List<CellInfoS>>());
                    foreach (var Cell in ListSets[i])
                    {
                        if (Cell.CenterPoint.X < Min) Min = Cell.CenterPoint.X;
                        if (Cell.CenterPoint.X > Max) Max = Cell.CenterPoint.X;
                        var Key = AtResolution(Cell.CenterPoint, Resolution);
                        if (!PointsAll.ContainsKey(Key)) PointsAll.Add(Key, new List<CellInfoS>());
                        if (!PointSets[i].ContainsKey(Key)) PointSets[i].Add(Key, new List<CellInfoS>());

                        PointsAll[Key].Add(Cell); PointSets[i][Key].Add(Cell);
                    }
                }
            }

            private Dictionary<PointF, List<CellInfoS>> PointsTemp;

            public double SumProd(int iShiftX, int iShiftY, bool SaveShifted = false)
            {
                //Shift the 2nd point set, and then do the sum product
                var PShift = PointSets[1];
                var PCheck = PointSets[0];
                //var Matched = new HashSet<PointF>();
                int Match = 0; int Denom = 0;

                if (SaveShifted) PointsTemp = new Dictionary<PointF, List<CellInfoS>>();
                foreach (var KVP in PShift)
                {
                    var Shifted = new PointF(KVP.Key.X + Resolution * iShiftX, KVP.Key.Y + Resolution * iShiftY);
                    if (SaveShifted) PointsTemp.Add(Shifted, KVP.Value);
                    if (Shifted.X < Min || Shifted.X > Max || Shifted.Y < Min || Shifted.Y > Max) continue;
                    if (PCheck.ContainsKey(Shifted))
                        Match += Math.Min(PCheck[Shifted].Count, KVP.Value.Count); //Matched.Add(Shifted);
                    Denom++;
                }
                if (Denom == 0) return 0;
                return 1000 * Match / Denom;
            }

            public void GridSearch(int[] XShifts, int[] YShifts, SortedList<double, Tuple<int, int>> SortedResults, HashSet<Tuple<int, int>> AlreadyTried)
            {
                var SL = SortedResults;
                foreach (var X in XShifts)
                {
                    foreach (var Y in YShifts)
                    {
                        var Loc = new Tuple<int, int>(X, Y);
                        if (AlreadyTried.Contains(Loc)) continue;
                        AlreadyTried.Add(Loc);
                        double Res = -SumProd(X, Y);
                        while (SL.ContainsKey(Res))
                            Res -= RND.ZeroOne() / 10000;
                        SL.Add(Res, Loc);
                    }
                }
            }

            public static PointF AtResolution(PointF Input, float Resolution)
            {
                var P = new PointF((float)(Resolution * Math.Round(Input.X / Resolution, 0)), (float)(Resolution * Math.Round(Input.Y / Resolution, 0)));
                return P;
            }

            public Dictionary<PointF, List<CellInfoS>> PointsAll;
            public List<Dictionary<PointF, List<CellInfoS>>> PointSets;

            public List<IEnumerable<CellInfoS>> ListSets;
            public float Resolution { get; set; }
            public float Min { get; set; }
            public float Max { get; set; }
        }

    }

}
