﻿using CMSImageTools;
using DocumentFormat.OpenXml.Drawing.Charts;
using FIVE.FOVtoRaftID;
using FIVE.ImageCheck;
using FIVE.InCellLibrary;
using FIVE.ML_IMG;
using FIVE.RaftCal;
using FIVE.TF;
using FIVE_IMG;
using FIVE_Tools_Main.Forms;
//using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Tensorflow.NumPy;

namespace FIVE
{
    public partial class FormMain : Form
    {
        public FIVE_Tools_Main.NGSAlleleFragmentsForm NGsAllelesForm = new FIVE_Tools_Main.NGSAlleleFragmentsForm();
        public FIVE_Tools_Main.Form_CombineModels Compiler_Form = new FIVE_Tools_Main.Form_CombineModels();
        public FIVE_Tools_Main.FormRegistration FormImageRegistration = new FIVE_Tools_Main.FormRegistration();
        public FIVE_Tools_Main.Form_PLInteract FormPLInteract = new FIVE_Tools_Main.Form_PLInteract();
        private FIVE_Tools_Main.Forms.Form_Layouts form_Layouts = new FIVE_Tools_Main.Forms.Form_Layouts();
        public INCELL_DB dB_Main;
        public List<string> InCell_Displays;
        public Dictionary<string, string> InCell_Folders;
        public Dictionary<string, INCELL_Folder> InCell_Object;
        private FileSystemWatcher FSW_InCarta, FSW_InCell;
        public FIVE_IMG.Store_Validation_Parameters NVP;
        public FormRaftCal PreviousCalCheck; //This is regenerated every time which Willie wrote is ideal in 2021 for some reason, but this gives you access to the most recent version

        public FormMain(FIVToolsMode startingMode = FIVToolsMode.Normal)
        {
            InitializeComponent();
            InCell_Displays = new List<string>();
            Load_Save_Params(false);
            SetupContextMenus();

            LogEvent("Open");

            btn_LoadFolders(this, new EventArgs());

            this.Text += " " + System.Windows.Forms.Application.ProductVersion;

            Mode = startingMode;
            if (Mode == FIVToolsMode.PlateID_WellList) //Leica FIVTools mode
            {
                if (NVP.RegParams.FullAuto) // Fully automated mode (set in XML settings)
                {

                    //MessageBox.Show("fullauto");
                    FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "getting barcode");
                    //FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "getting barcode" + "\r\n");
                    // File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "getting barcode" + "\r\n");

                    string filePath = "C:\\temp\\AILbase\\ini\\barcode.txt"; // replace with the file path barcode scanner writes to.
                    string barcode = File.ReadLines(filePath).FirstOrDefault();
                    FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "barcode - " + barcode);
                    // File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "barcode - " + barcode+ "\r\n");

                    if (string.IsNullOrEmpty(barcode))
                    {
                        GetFolderandReg("ruhroh");
                    }
                    var begOfName = NVP.RegParams.PlateNameBeginning;
                    string scanName = $"{begOfName}";
                    // scanName = "JOSH113_P1_PFAque";
                    if (Regex.IsMatch(barcode, @"^\d{10}$"))
                        scanName += $".{barcode.Substring(0, 2)}.{barcode.Substring(2, 2)}.{barcode.Substring(4, 2)}.{barcode.Substring(6, 2)}.{barcode.Substring(8, 2)}";
                    else
                        scanName += barcode;

                    // Inserting decimal points to format the string as 'YYYY.MM.DD.HH.MM.SS'
                    FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "barcode- " + scanName);
                    //File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "barcode- " + scanName + "\r\n");

                    GetFolderandReg(scanName);
                }
            }
        }

        private void SetupContextMenus()
        {
            ToolStripMenuItem mi = new ToolStripMenuItem("Copy XDCE Info to Clipboard");
            mi.Click += CopyXDCEToClip_Click;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Open Folder in Explorer");
            mi.Click += btn_OpenFolder_Click;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Delete Scan");
            mi.Click += DeleteSelectedScan;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Reconstruct as 5x");
            mi.Click += ReconstructScan;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Calibrate and Annotate");
            mi.Click += button_LoadRaftCalTest_Click;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Get Annotation Info");
            mi.Click += Click_GetScanAnnotationInfo;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Check Annotations");
            mi.Click += PrePickListCheck;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Export Raft Images (from list)");
            mi.Click += ExportRaftImages;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Export Cell Images (InCarta)");
            mi.Click += ExportCellImages;
            contextMenuStrip_Scans.Items.Add(mi);

            contextMenuStrip_Scans.Items.Add("-");

            mi = new ToolStripMenuItem("Rename/Normalize");
            mi.Click += RenameNormalizeClick;
            contextMenuStrip_Scans.Items.Add(mi);

            contextMenuStrip_Scans.Items.Add("-");

            mi = new ToolStripMenuItem("Export Mask Images (Photoactivation)");
            mi.Click += ExportMaskImages;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Pre Pick List Check");
            mi.Click += PrePickListCheck;
            contextMenuStrip_Scans.Items.Add(mi);

            mi = new ToolStripMenuItem("Export Pick List");
            mi.Click += GeneratedPickList;
            contextMenuStrip_Scans.Items.Add(mi);

            //-----------------------------------------

            mi = new ToolStripMenuItem("Copy first Abbrev to Selected");
            mi.Click += CopyFirstAbbrev_ToSelected;
            contextMenuStrip_Multi.Items.Add(mi);

            contextMenuStrip_Multi.Items.Add("-");

            mi = new ToolStripMenuItem("Class Definitions - View JSON");
            mi.Click += ClassDefinition_View;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Class Definitions - Load from JSON");
            mi.Click += ClassDefinition_Load;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Class Definitions - Load from Copied PlateMap");
            mi.Click += ClassDefinition_LoadFromPlateMap_Clipboard;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Class Definitions - Save Default");
            mi.Click += ClassDefinition_SaveDefault;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Class Definitions - Copy Spotfire Style");
            mi.Click += ClassDefinition_CopySpotfire;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Class Definitions - Copy PlateMap Style");
            mi.Click += ClassDefinition_CopyPlateMap;
            contextMenuStrip_Multi.Items.Add(mi);

            contextMenuStrip_Multi.Items.Add("-");

            mi = new ToolStripMenuItem("Classify and Measure");
            mi.Click += ML_Classify_Measure_Click;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Export all images (chop)");
            mi.Click += ChopUpExport;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Export Annotated Raft Images from Scan(s)");  //detects the Name
            mi.Click += MultiRaftExportImagesAnno;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Export All Raft Images from Scan(s)"); //detects the Name
            mi.Click += MultiRaftExportImagesAnno;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Export PrePickLists from Annotated Rafts");
            mi.Click += PrePickFromAnnotated;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("List Annotation Info");
            mi.Click += ListAnnotationInfo;
            contextMenuStrip_Multi.Items.Add(mi);

            mi = new ToolStripMenuItem("Export Fast PrePickList List ..");
            mi.Click += FastPrePick;
            contextMenuStrip_Multi.Items.Add(mi);

            //-----------------------------------------

            mi = new ToolStripMenuItem("Columns Reduce Dialog");
            mi.Click += Lbl_Columns_Reduce_Click;
            contextMenuStrip_CodeOnly.Items.Add(mi);

            mi = new ToolStripMenuItem("Image Zoom Rename");
            mi.Click += btn_ImageZoomRename_Click;
            contextMenuStrip_CodeOnly.Items.Add(mi);

            mi = new ToolStripMenuItem("Simulations");
            mi.Click += btnSimulations_Click;
            contextMenuStrip_CodeOnly.Items.Add(mi);

            mi = new ToolStripMenuItem("Spool Manager");
            mi.Click += btn_SpoolManagerStart;
            contextMenuStrip_CodeOnly.Items.Add(mi);

            mi = new ToolStripMenuItem("Align 20x to Lower Mag");
            mi.Click += AligntoLowerMag;
            contextMenuStrip_CodeOnly.Items.Add(mi);

            mi = new ToolStripMenuItem("Code Only");
            mi.Click += btn_CodeOnly_Sub;
            contextMenuStrip_CodeOnly.Items.Add(mi);
        }

        private void Click_GetScanAnnotationInfo(object sender, EventArgs e)
        {
            //Similar info to when we compile, we want to know about calibration, and about what is found in what fields
            //We can print this out and copy it to clipboard?
            INCELL_Folder FolderToPass;
            string plateID = listBox_Scans.SelectedItem == null ? "" : listBox_Scans.SelectedItem.ToString();
            if (plateID == "") FolderToPass = null;
            else FolderToPass = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            GetInfo_Cal_Annotations_Abbrev(FolderToPass);
        }

        private void AligntoLowerMag(object sender, EventArgs e)
        {
            INCELL_Folder FolderHigh, FolderLow;

            string plateID_HighMag = "FIV834P5_20x_1";
            string plateID_LowMag = "FIV834P5_10x_1";
            FolderHigh = dB_Main.Folders.Where(x => x.PlateID == plateID_HighMag).ToList()[0];
            FolderLow = dB_Main.Folders.Where(x => x.PlateID == plateID_LowMag).ToList()[0];

            INCELL_AlignToLowerMag.MannyTest01(FolderHigh, FolderLow);
        }


        private void ReconstructScan(object sender, EventArgs e)
        {
            INCELL_Folder FolderToPass;
            string plateID = listBox_Scans.SelectedItem == null ? "" : listBox_Scans.SelectedItem.ToString();
            if (plateID == "") return;
            else FolderToPass = dB_Main.Folders.Where(x => x.PlateID.ToUpper() == plateID.ToUpper()).ToList()[0];
            if (FolderToPass == null) return;

            var BWRS = new BackgroundWorker();
            BWRS.WorkerReportsProgress = true;
            BWRS.ProgressChanged += BWRS_ProgressChanged;
            BWRS.DoWork += BWRS_DoWork;

            var mOffsets = NVP.SavedOffsets; //Manny
            //var mOffsets = INCELL_MeasuredOffsets.Couch06_02();

            var MaskListFiles = Directory.GetFiles(FolderToPass.AdjustedPath, "*mask*.txt"); //Find Associates Mask List //Possibly other files we want to search for and reconstruct
            if (MaskListFiles.Length < 1)
            {
                var Ret = MessageBox.Show("Didn't find an associate mask file. You need this at the time of reconstruction to adjust the file as well. Please place one in the directory. OK to continue without one, otherwise cancel.", "Mask List", MessageBoxButtons.OKCancel);
                if (Ret == DialogResult.Cancel) return;
            }
            string AssociatedTabularFile = MaskListFiles.Length == 0 ? "" : MaskListFiles[0];
            var Args = (Folder: FolderToPass, MOffsets: mOffsets, Mag: NVP.RegParams.scanRecon_MagnificationNew,
                AssoFile: AssociatedTabularFile, fracFilled: NVP.RegParams.scanRecon_FractionFilled, append: NVP.RegParams.scanRecon_AppendPlateID, BW: BWRS);

            BWRS.RunWorkerAsync(Args);
        }

        private void BWRS_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = e.Argument as (INCELL_Folder Folder, INCELL_MeasuredOffsets MOffsets, float Mag, string AssoFile, float fracFilled, string append, BackgroundWorker BW)?;

            INCELL_Folder_Reconstruct.PerformReconstruction(Args.Value.Folder, Args.Value.MOffsets, Args.Value.Mag, Args.Value.AssoFile, Args.Value.fracFilled, Args.Value.append, Args.Value.BW);
        }

        private static string BWRS_PreviousText = "";

        private void BWRS_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage > 0)
            {
                txBx_Update.Text = PercentageToProgressString(e.ProgressPercentage) + "\r\n" + BWRS_PreviousText;
            }
            else
            {
                txBx_Update.Text = e.UserState.ToString() + "\r\n" + txBx_Update.Text;
                BWRS_PreviousText = txBx_Update.Text;
            }
        }

        public static string PercentageToProgressString(float percentage)
        {
            int totalDashes = 20;
            int equalsCount = 1 + (int)Math.Round(totalDashes * percentage / 100);
            return new String('|', equalsCount) + new String('-', totalDashes - equalsCount);
        }


        private void ExportCellImages(object sender, EventArgs e)
        {
            if (PreviousCalCheck == null)
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again.";
                return;
            }
            LogEvent("ExportCellImages");
            INCELL_Folder FolderToPass; DialogResult DR; string Filename;
            string Title = "Please select a .csv file with the list of cells (from InCarta). You need 'PlateID'~FIVXXXA1_3, 'WELL LABEL'~A - 1, 'FOV' and 'Object ID' and Nuclei Max Width, Height, Top, Left wv1. Also, the settings for this selected plate ID will be used for export.";
            OFDStd("Cell Images . . ", "2 MlModels", "csv files (*.csv)|*.csv|All files (*.*)|*.*", Title, out FolderToPass, out Filename, out DR);
            if (DR != DialogResult.OK) return;

            txBx_Update.Text = "Loading cells . . "; System.Windows.Forms.Application.DoEvents();
            var PlateID_WellField_CellInfo = CellInfoS.Plate_WellField_CellInfo(Filename, PreviousCalCheck.ExportCalForm.Settings.ColumnNameForAnnotation);
            if (PlateID_WellField_CellInfo == null)
            {
                txBx_Update.Text = "Problem loading or parsing the file (" + CellInfoS.ParseError + ").  Please see these instructions:\r\n" + Title;
                return;
            }
            //Build this into a BG worker
            var Args = new List<object>() { true, FolderToPass.InCell_Wavelength_Notes.DisplayParams, PreviousCalCheck.ExportCalForm.Settings, PreviousCalCheck, PlateID_WellField_CellInfo, NVP };
            bgWrk_RaftMultiExport.RunWorkerAsync(Args);
        }

        /// <summary>
        /// Exporting Annotated Rafts from Multiple Scans/Wells
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_MultiRaftExportImages_Click(object sender, EventArgs e)
        {
            contextMenuStrip_Multi.Show(Cursor.Position);
            contextMenuStrip_Multi.Visible = true;
        }

        private void ChopUpExport(object sender, EventArgs e)
        {
            Tuple<string, string, string, string, string> Res;
            var DR = MultiInputForm.ShowMulti("Usually, images are 2040x2040, and you keep it that size if you want to export the whole image (un-chopped).", out Res, "Export Folder", NVP.ChopSettings_ExportBaseFolder, "Orig Width (put 0 if you're not sure)", NVP.ChopSettings_OrigWidth.ToString(), "Output Width", NVP.ChopSettings_OutputWidth.ToString());
            if (DR == DialogResult.Cancel) return;
            NVP.ChopSettings_ExportBaseFolder = Res.Item1;
            NVP.ChopSettings_OrigWidth = int.Parse(Res.Item2);
            NVP.ChopSettings_OutputWidth = int.Parse(Res.Item3);

            Load_Save_Params(true);
            if (PreviousCalCheck == null)
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again."; return;
            }
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            var Folders = new List<INCELL_Folder>(plateIDs.Length);
            foreach (string id in plateIDs)
                Folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());

            var BW = new BackgroundWorker();
            BW.DoWork += BW_ChopExport_DoWork;
            BW.ProgressChanged += BW_ChopExport_ProgChanged; 
            BW.WorkerReportsProgress = true; BW.WorkerSupportsCancellation = true; 
            //BW.RunWorkerCompleted += 
            var Args = new Object[] { Folders, NVP, BW };
            BW.RunWorkerAsync(Args);
        }

        private void BW_ChopExport_ProgChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void BW_ChopExport_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (Object[])(e.Argument);
            var Folders = (List<INCELL_Folder>)Args[0];
            var NVP = (Store_Validation_Parameters)Args[1];
            var BW = (BackgroundWorker)Args[2];

            foreach (var Folder in Folders)
            {
                ChopUpExportPlate(Folder, NVP.ChopSettings_OrigWidth, NVP.ChopSettings_OutputWidth, NVP.ChopSettings_ExportBaseFolder, BW);
                if (BW.CancellationPending) break;
            }

            BW.ReportProgress(100,"Done Chopping (or aborted).");
        }

        private void ChopUpExportPlate(INCELL_Folder Folder, int OriginalCropWidth, int OutputCropWidth, string ExportBasePath, BackgroundWorker BW)
        {
            if (!Directory.Exists(ExportBasePath)) Directory.CreateDirectory(ExportBasePath);
            BW.ReportProgress(0, "Chopping from: " + Folder.PlateID + " ... Orig/Out: " + OriginalCropWidth + "/" + OutputCropWidth);
            BW.ReportProgress(0, ExportBasePath);
            var nSz = new System.Drawing.Size(OutputCropWidth, OutputCropWidth);
            foreach (var Well in Folder.XDCE.Wells.Values)
            {
                BW.ReportProgress(0, Well.NameAtLevel + ": " + Well.FOV_Max);
                for (int FOV = Well.FOV_Min; FOV < Well.FOV_Max; FOV++)
                {
                    if (BW.CancellationPending) return;
                    var Imgs = Well.GetFields(FOV);
                    var bmap = PreviousCalCheck.CombinedBMAP(Well, Imgs[0]);
                    if (OriginalCropWidth == 0)
                        OriginalCropWidth = bmap.Width;
                    
                    for (int i = 0; i < bmap.Width; i += OriginalCropWidth)
                    {
                        for (int j = 0; j < bmap.Height; j += OriginalCropWidth)
                        {
                            var rect = new Rectangle(i, j, OriginalCropWidth, OriginalCropWidth);
                            if ((rect.X + rect.Width) > bmap.Width || (rect.Y + rect.Height) > bmap.Height) continue;
                            var cropBmp = bmap.Clone(rect, bmap.PixelFormat);
                            var cropRsz = new Bitmap(cropBmp, nSz);
                            string szName = $"{Folder.PlateID}.{Well.NameAtLevel}_{FOV}_{i}_{j}.bmp";
                            cropRsz.Save(Path.Combine(ExportBasePath, szName));
                        }
                    }
                }
            }
        }

        private void ML_Classify_Measure_Click(object sender, EventArgs e)
        {
            SetupForGettingAnnotatedRafts(out List<INCELL_Folder> folders, out string FIVid, out _);
            var AnnoList = new List<string>();
            try
            {
                string AnnosStr = folders[0].XDCE.Wells.First().Value.ImageCheckInfo;
                AnnoList = AnnosStr.Split(',').Select(s => s.Trim().Split(':')[0]).ToList();
            }
            catch { }

            var FMLS = new Form_MLSettings(NVP);
            FMLS.ActiveAnnotationsList = AnnoList;
            var Ret = FMLS.ShowDialog();

            var MIS = FMLS.SVP.ModelInferSettings;
            NVP.ModelInferSettings = MIS;
            NVP.Save();

            if (Ret == DialogResult.OK)
            {
                //Save Run
                NVP.ModelInferSettings = MIS;
                ML_Classify_Measure(sender);
            }
            else if (Ret == DialogResult.Continue)
            {
                //Save Close
                NVP.ModelInferSettings = MIS;
            }
            else
            {
                //Do Nothing, Cancelled
            }
        }

        private void ML_Classify_Measure(object sender)
        {
            Load_Save_Params(true);

            if (PreviousCalCheck == null)
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again.";
                return;
            }
            //Check sender for which one
            var senderItem = (ToolStripItem)sender;
            string Type = senderItem.ToString();

            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            var Folders = new List<INCELL_Folder>(plateIDs.Length);
            foreach (string id in plateIDs)
                Folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());

            (double FractionCal, double FractionAnnos, double FractionAbbrev, string Message) = GetInfo_Cal_Anno_Abbrev(Folders);

            (string err, string note) = NVP.ModelInferSettings.PreCheck_ClassifyMeasure(FractionCal);
            if (err != "") { txBx_Update.Text = err + "\r\n" + note; return; }

            var BW_MLClassifyMeasure = new BackgroundWorker();

            BW_MLClassifyMeasure.WorkerReportsProgress = true;
            BW_MLClassifyMeasure.WorkerSupportsCancellation = true;
            BW_MLClassifyMeasure.DoWork += BW_MLClassifyMeasure_DoWork;
            BW_MLClassifyMeasure.ProgressChanged += BW_MLClassifyMeasure_ProgressChanged;

            var Args = (folders: Folders, type: Type, calCheck: PreviousCalCheck, bw: BW_MLClassifyMeasure, nvp: NVP, fractionCal: FractionCal);

            BW_MLClassifyMeasure.RunWorkerAsync(Args);
        }

        private void BW_MLClassifyMeasure_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState.ToString();
        }

        private void BW_MLClassifyMeasure_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = e.Argument as (List<INCELL_Folder> folders, string type, FormRaftCal calCheck, BackgroundWorker bw, Store_Validation_Parameters nvp, double fractionCal)?;
            Args.Value.bw.ReportProgress(0, "Starting . . ");

            var imgQueue = new ImageQueue();
            wvDisplayParams wvPs = Args.Value.calCheck.CurrentWVParams;
            ML_Context.DefaultBuildUpCount = 420;
            ML_Context.WholeRunTrack = "";
            string DTN = DateTime.Now.ToString("yyyyMMdd_hhmm");
            ML_Context.DefaultMeasureName = Args.Value.folders[0].FIViD + " mMeasures_" + DTN + ".txt";
            var MIS = NVP.ModelInferSettings;
            //MIS.svImageFolder = @"c:\temp\ImgRun_" + DTN;
            //var cls = MIS.SaveImagesWhileInferring_Classify; var ms = MIS.SaveImagesWhileInferring_Measure; var clsm = MIS.ModelPath_Classification;

            //Check if there are rafts or regions
            if (Args.Value.fractionCal > 0 || Args.Value.folders[0].XDCE.Rafts.Count > 0)
            {
                //Has Rafts / Regions
                bool TestingCase = false; if (TestingCase)
                {
                    MIS.ModelPaths_Measure_Multi = ModelInferSettings.Default_NeuriteMeasure_Paths;
                    MIS.ModelPath_Classification = @"S:\Raft\FIV827\001 SF Sort\M 20230403\ acc_0.7969 CNN_AC_R CNNs_7 kn_4 flt_18 iRt_1.5 ds__270_ nE_169 bSz_64 bNm_0 i_22\acc_0.7969 CNN_AC_R CNNs_7 kn_4.pb";
                }
            }
            else //No Rafts / Regions
            {
                
            }

            if (MIS.SaveMeasuresFolder == "") MIS.SaveMeasuresFolder = Args.Value.nvp.HCS_Image_DestinationFolder;
            if (!Directory.Exists(MIS.svImageFolder)) Directory.CreateDirectory(MIS.svImageFolder);
            //Start the preloading
            Args.Value.bw.ReportProgress(0, "PreLoading . . ");
            imgQueue.PreLoad(Args.Value.folders, wvPs);
            System.Threading.Thread.Sleep(3500);
            ML_Context.TotalWells = Args.Value.folders.Sum(x => x.XDCE.Wells.Count); ML_Context.FinishedWells = 0; ML_Context.TotalStart = DateTime.Now;
            
            foreach (var folder in Args.Value.folders)
            {
                foreach (var well in folder.XDCE.Wells.Values)
                {
                    if (MIS.cmPerformClassifications && MIS.cmDeleteExistingAnnotations) well.ImageCheck_PointsList = new ImageCheck_PointList() { Well = well.NameAtLevel };
                    var context = new ML_IMG.ML_Context(well, wvPs, imgQueue, MIS, NVP.CropSettings, ClassDef, null, Args.Value.bw, true, Args.Value.fractionCal > 0);
                    context.ML_ClassifyMeasure();
                    if (MIS.cmRecordBackAnnotations) well.ImageCheck_PointsList.SaveDefault(well.ParentFolder.FullPath);
                    ML_Context.WholeRunTrack = ((double)++ML_Context.FinishedWells / ML_Context.TotalWells).ToString("00.0%") + " " + well.PlateID + " " + well.NameAtLevel + ", " + context.TotalTimeElapsed + " min\r\n" + ML_Context.WholeRunTrack;
                }
            }
            //NVP.ModelInferSettings.SaveImagesWhileInferring_Classify = cls; NVP.ModelInferSettings.SaveImagesWhileInferring_Measure = ms; NVP.ModelInferSettings.ModelPath_Classification = clsm;
            File.WriteAllText(@"c:\temp\check_finish_measure.txt", "Finished! " + DateTime.Now.ToString());
        }

        private void MultiRaftExportImagesAnno(object sender, EventArgs e)
        {
            if (PreviousCalCheck == null)
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again.";
                return;
            }
            //Check sender for which one
            var senderItem = (ToolStripItem)sender;
            string Type = senderItem.ToString();

            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            var folders = new List<INCELL_Folder>(plateIDs.Length);
            foreach (string id in plateIDs)
                folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());

            //Build this into a BG worker
            var Args = new List<object>() { false, null, PreviousCalCheck.ExportCalForm.Settings, PreviousCalCheck, null, folders, Type == "Export Annotated Raft Images from Scan(s)" };
            bgWrk_RaftMultiExport.RunWorkerAsync(Args);
        }

        /// <summary>
        /// This is designed to export picked raft images (or any set of raft images) from a series of plates using the settings from the selected plate to adjust the brightness, etc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportRaftImages(object sender, EventArgs e)
        {
            if (PreviousCalCheck == null)
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again.";
                return;
            }
            LogEvent("ExportRaftImages");
            INCELL_Folder FolderToPass; DialogResult DR; string Filename;
            string Title = "Please select a .txt file with the list of rafts. You need 'PlateID'~FIVXXXA1_3, 'WELL LABEL'~A - 1, 'FOV' and 'RaftID'~D7F1. Also, the settings for this selected plate ID will be used for export. Uncheck all channels to get them individually.";
            OFDStd("Raft Images Requested . . ", "6 Images", "txt files (*.txt)|*.txt|All files (*.*)|*.*", Title, out FolderToPass, out Filename, out DR);
            if (DR != DialogResult.OK) return;

            var PlateIDs_FOV_RaftID = RaftInfoS.Parse_PlateID_RaftInfo(Filename);  //Parse_PlateID_Well_FOV_RaftID(Filename);
            if (PlateIDs_FOV_RaftID == null)
            {
                txBx_Update.Text = "Problem loading or parsing the file.  Please see these instructions:\r\n" + Title;
                return;
            }
            //Build this into a BG worker
            List<object> Args = new List<object>() { false, FolderToPass.InCell_Wavelength_Notes.DisplayParams, PreviousCalCheck.ExportCalForm.Settings, PreviousCalCheck, PlateIDs_FOV_RaftID };
            bgWrk_RaftMultiExport.RunWorkerAsync(Args);
        }

        private void bgWrk_RaftMultiExport_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (List<object>)e.Argument;
            var IsCell = (bool)Args[0];
            var DisplayParams = (wvDisplayParams)Args[1];
            var Settings = (RaftImage_Export_Settings)Args[2];
            var CalForm = (FormRaftCal)Args[3];
            //var iNVP = (FIVE_IMG.Store_Validation_Parameters)Args[5];

            if (IsCell)
            {
                //Settings.CellImageExpandPixels = 24; //You can adjust this here . . adjust from the export dialog
                var CellInfoSet = (Dictionary<string, Dictionary<string, HashSet<CellInfoS>>>)Args[4];
                PullOutInCartaImages(CellInfoSet, DisplayParams, Settings, CalForm, bgWrk_RaftMultiExport);
            }
            else
            {
                var PlateIDs_FOV_RaftID = (Dictionary<string, HashSet<RaftInfoS>>)Args[4];
                if (PlateIDs_FOV_RaftID != null)
                {
                    //Export by tabular List
                    MultiExport_RaftImages(PlateIDs_FOV_RaftID, DisplayParams, Settings, CalForm, bgWrk_RaftMultiExport);
                }
                else
                {
                    //Export from multi selection of PlateIDs
                    var Folders = (List<INCELL_Folder>)Args[5];
                    bool AnnotatedOnly = (bool)Args[6];
                    MultiExport_RaftsFromSelection(Folders, DisplayParams, Settings, CalForm, AnnotatedOnly, bgWrk_RaftMultiExport);
                }
            }
        }

        private void bgWrk_RaftMultiExport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + txBx_Update.Text;
            if (txBx_Update.Text.Length > 8000) txBx_Update.Text = txBx_Update.Text.Substring(0, 4000);
        }

        private void bgWrk_RaftMultiExport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        /// <summary>
        /// This is exporting rafts from a tabular file of RAFT IDs
        /// </summary>
        public string MultiExport_RaftImages(Dictionary<string, HashSet<RaftInfoS>> Plates_RaftInfos, wvDisplayParams wvParams, RaftImage_Export_Settings Settings, FormRaftCal CalForm, BackgroundWorker BW = null)
        {
            Settings.ExportFolderBase = Path.Combine(Settings.ExportFolderBase, Plates_RaftInfos.First().Key.Substring(0, 6)); //Consider altering this to an R:-based location (although would be slow)
            Settings.SubFolderByPlateID = false;
            string msg = "";
            foreach (var PlateIDSet in Plates_RaftInfos)
            {
                //if (PlateIDSet.Key.StartsWith("FIV831")) continue;
                var PreFolder = dB_Main.Folders.Where(x => x.PlateID == PlateIDSet.Key).ToArray();
                if (PreFolder.Length == 0)
                {
                    msg += "\r\n" + PlateIDSet.Key + " Missing in this dB.";
                    BW.ReportProgress(0, msg); continue;
                }
                var IC_Folder = PreFolder[0];
                if (BW != null) BW.ReportProgress(0, "\r\nLoading plate " + PlateIDSet.Key + " . . . ");
                //Package up the IDs so they can be exported (link them to images and points)
                var ICPL = ImageCheck_PointList.FromRafts(IC_Folder, PlateIDSet.Value);

                //Now call the export on this
                if (BW != null) BW.ReportProgress(0, "\r\nStarting Export of " + PlateIDSet.Value.Count + "/" + ICPL.Count + " Rafts . . . ");
                RaftCal.ExportCalForm.Trigger_Export(Settings, ICPL, IC_Folder, wvParams, CalForm, BW);
            }
            return msg;
        }

        /// <summary>
        /// Exporting raft images from a selection, and then using annotations
        /// </summary>
        private void MultiExport_RaftsFromSelection(List<INCELL_Folder> folders, wvDisplayParams wvParams, RaftImage_Export_Settings Settings, FormRaftCal CalForm, bool AnnotatedRaftsOnly, BackgroundWorker BW = null)
        {
            string FIVids = string.Join(",", folders.Select(x => x.FIViD).Distinct());
            Settings.ExportFolderBase = Path.Combine(Settings.ExportFolderBase, FIVids);
            Settings.SubFolderByPlateID = false;
            Settings.SubFolderBy_PlateID_Well = true;
            Settings.FolderName_ByAnnotation = false;
            Settings.FileName_ByAnnotation = true;

            Settings.TIF16Bit_Export = false; //For testing
            foreach (var folder in folders)
            {
                if (BW != null) BW.ReportProgress(0, "\r\nLoading plate " + folder.PlateID + " . . . ");
                //Have to go through the different wells . . 
                foreach (var well in folder.XDCE.Wells.Values)
                {
                    if (well.ImageCheck_PointsList == null && AnnotatedRaftsOnly) continue;
                    RaftCal.ExportCalForm.Trigger_Export(Settings, AnnotatedRaftsOnly ? well.ImageCheck_PointsList : null, folder, wvParams, CalForm, BW);
                }
            }
            if (BW != null) BW.ReportProgress(0, "\r\nDone mRafting!\r\n");
        }

        private void PrePickListCheck(object sender, EventArgs e)
        {
            bool PrePick = sender.ToString() != "Check Annotations";
            string plateID = listBox_Scans.SelectedItem == null ? "" : listBox_Scans.SelectedItem.ToString();
            if (PreviousCalCheck == null || plateID == "")
            {
                txBx_Update.Text = "Please go into Cal Check (double click) on a scan first to make sure all your display and export settings are correct, then try this again.";
                return;
            }
            INCELL_Folder FolderToPass; DialogResult DR; string Filename = "";
            int Max = NVP.PrePickCheck_Annotated_MaxToLoad;
            if (PrePick)
                OFDStd("Pick List Pre Flight . .", "3 PickRelease\\PreCheck", "txt files (*.txt)|*.txt|All files (*.*)|*.*", "Please select a .txt file with the pick list to check. Click cancel to view what is currently annotated (" + Max + " max).", out FolderToPass, out Filename, out DR);
            else
            {
                FolderToPass = dB_Main.Folders.Where(x => x.PlateID.ToUpper() == plateID.ToUpper()).ToList()[0];
                DR = DialogResult.Cancel;
            }
            Dictionary<string, HashSet<RaftInfoS>> PlateID_RaftInfo = null;
            if (DR == DialogResult.OK)
                PlateID_RaftInfo = RaftInfoS.Parse_PlateID_RaftInfo(Filename); //Open from File
            else
                PlateID_RaftInfo = RaftInfoS.FromAnnotations(FolderToPass, Max); //Open with annotations only
            txBx_Update.Text = RaftInfoS.ParseError;
            if (PlateID_RaftInfo == null) return;

            var FPC = new FIVE_Tools_Main.Forms.Form_PreCheckRaft(PreviousCalCheck, PlateID_RaftInfo, dB_Main, Path.GetFileNameWithoutExtension(Filename), Max);
            FPC.ShowDialog();
        }

        private void ListAnnotationInfo(object sender, EventArgs e)
        {
            List<INCELL_Folder> folders; string FIVid; Dictionary<string, HashSet<RaftInfoS>> AP;
            SetupForGettingAnnotatedRafts(out folders, out FIVid, out AP);

            txBx_Update.Text = "";
            foreach (var fldr in folders)
            {
                GetInfo_Cal_Annotations_Abbrev(fldr, false);
            }
            Clipboard.SetText(txBx_Update.Text);
            txBx_Update.Text = "Done, and Copied to Clipboard.\r\n\r\n" + txBx_Update.Text;
        }

        private void FastPrePick(object sender, EventArgs e)
        {
            List<INCELL_Folder> folders; string FIVid; Dictionary<string, HashSet<RaftInfoS>> AP;
            SetupForGettingAnnotatedRafts(out folders, out FIVid, out AP);

            //First get the list of annotations that we can choose from
            (_, var AnnoList, _) = GetAnnotatedRaftsFromFolder(folders, AP);

            var Pieces = MultiInputForm.ShowMultiMore("How would you like your PickList?", "Annotations to include", string.Join("|", AnnoList), "Length of List", "160");
            if (Pieces.Item1 == DialogResult.Cancel) return;
            var AnnosToInclude = Pieces.Item2[0].Split('|');
            var PickListLength = int.Parse(Pieces.Item2[1]);

            //Now use that to build a list which limits to those terms
            (StringBuilder sB, _, int Total) = GetAnnotatedRaftsFromFolder(folders, AP, PickListLength, AnnosToInclude, true, true);

            txBx_Update.Text = (Total < (PickListLength - 2) ? "Caution, Fewer than requested!! " : "") + Total + " rafts in exported list.";

            string Folder = GetAssumedFolder(@"3 PickRelease\PreCheckAuto\", folders[0]);
            string NewFileName = Path.Combine(Folder, FIVid + " FastPick A " + DateTime.Now.ToString("hhmmss") + ".txt");
            if (File.Exists(NewFileName)) NewFileName = Path.Combine(Folder, FIVid + " FastPick A " + DateTime.Now.ToString("hhmmsss") + ".txt");
            File.WriteAllText(NewFileName, sB.ToString());
            var startInfo = new ProcessStartInfo { Arguments = Folder, FileName = "explorer.exe" };
            Process.Start(startInfo);
        }


        /// <summary>
        /// Sets up the set of PickLists (pre-pick format) from the annotated rafts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrePickFromAnnotated(object sender, EventArgs e)
        {
            List<INCELL_Folder> folders; string FIVid; Dictionary<string, HashSet<RaftInfoS>> AP;
            SetupForGettingAnnotatedRafts(out folders, out FIVid, out AP);

            (StringBuilder sB, var AnnoList, _) = GetAnnotatedRaftsFromFolder(folders, AP);

            string Folder = GetAssumedFolder(@"3 PickRelease\PreCheckAuto\", folders[0]);
            string NewFileName = Path.Combine(Folder, FIVid + " PreCheck A " + DateTime.Now.ToString("hhmmss") + ".txt");
            if (File.Exists(NewFileName)) NewFileName = Path.Combine(Folder, FIVid + " PreCheck A " + DateTime.Now.ToString("hhmmsss") + ".txt");
            File.WriteAllText(NewFileName, sB.ToString());
            var startInfo = new ProcessStartInfo { Arguments = Folder, FileName = "explorer.exe" };
            Process.Start(startInfo);
        }

        private void SetupForGettingAnnotatedRafts(out List<INCELL_Folder> folders, out string FIVid, out Dictionary<string, HashSet<RaftInfoS>> AP)
        {
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            folders = new List<INCELL_Folder>(plateIDs.Length);
            foreach (string id in plateIDs) folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());

            FIVid = folders[0].FIViD;
            string AlreadyPickedPath = Path.Combine(GetAssumedFolder(@"3 PickRelease\", folders[0]), FIVid + " AlreadyPicked.txt");
            AP = null;
            if (File.Exists(AlreadyPickedPath)) AP = RaftInfoS.Parse_PlateID_RaftInfo(AlreadyPickedPath);
        }

        private (StringBuilder ResultRows, IEnumerable<string> AnnoList, int TotalFound) GetAnnotatedRaftsFromFolder(List<INCELL_Folder> folders, Dictionary<string, HashSet<RaftInfoS>> AlreadyPicked, int PickListLength = int.MaxValue, string[] AnnotationsToInclude = null, bool Exclude_Fiducials = true, bool Exclude_AlreadyPicked = false, char d = '\t')
        {
            var sB = new StringBuilder(); var APsub = new HashSet<RaftInfoS>(); var AnnoList = new HashSet<string>(); Random Rnd = new();
            int i; string Label = ""; int FiducialSkipped = 0; int Counter = 0;
            foreach (var ICFolder in folders)
            {
                APsub = null;
                if (AlreadyPicked != null) if (AlreadyPicked.ContainsKey(ICFolder.PlateID)) APsub = AlreadyPicked[ICFolder.PlateID];
                foreach (var Well in ICFolder.XDCE.Wells)
                {
                    string mlQuad = Well.Key switch
                    {
                        "A - 1" => "2", "A - 2" => "4",
                        "B - 1" => "1", "B - 2" => "3",
                        _ => "-1" 
                    };
                    i = 0;
                    if (ClassDef != null) Label = ClassDef.GetClassLabel(ICFolder.PlateIndex, Well.Key);
                    var ICPL = Well.Value.ImageCheck_PointsList;
                    if (ICPL == null) continue;
                    //Reorganize the rafts by their Annotation Score
                    var SortedRafts = new SortedList<double, ImageCheck_Point>();
                    foreach (var RaftID in ICPL.RaftIDs)
                    {
                        var PL = ICPL.FromDictionaryRaftID(RaftID);
                        float Score = PL[0].Annotations[0].Score;
                        float Key = float.IsNaN(Score) ? -1 : -Score;
                        while (SortedRafts.ContainsKey(Key))
                            Key += (float)(Rnd.NextDouble() / 10000);
                        SortedRafts.Add(Key, PL[0]);
                    }

                    //foreach (var RaftID in ICPL.RaftIDs)
                    foreach (var PL in SortedRafts.Values)
                    {
                        string RaftID = PL.RaftID;
                        if (ReturnRaft.IsRaftAnyFiducial(RaftID) && Exclude_Fiducials) { FiducialSkipped++; continue; }
                        //var PL = ICPL.FromDictionaryRaftID(RaftID);
                        string Anno = PL.Annotations[0].Value.ToString(); //= ICPL.FromDictionaryRaftID_Combined(RaftID);
                        float Score = PL.Annotations[0].Score;
                        string Model = PL.Annotations[0].Name;

                        //We may choose not to include these . . 
                        if (AnnotationsToInclude != null)
                        {
                            if (!AnnotationsToInclude.Contains(Anno)) continue;
                        }
                        AnnoList.Add(Anno);

                        string Status1 = "";
                        if (APsub != null)
                        {
                            var APrs = APsub.Where(x => x.RaftID == RaftID);
                            if (APrs.Count() > 0) Status1 = APrs.First().Line.Last();
                        }
                        if (Exclude_AlreadyPicked && Status1 != "") continue;

                        if (sB.Length < 10) //Setup Headers
                        {
                            sB.Append("mlPlateIndex" + d + "RaftID" + d + "mlQuad" + d + "mlClassLayout" + d + "PredictedClass" + d + "PredictedProbRaft" + d + "Rank" + d + "Cells per Raft" + d + "PlateID" + d + "WELL LABEL" + d + "FOV" + d + "PickSubPlate" + d + "ModelAtPick" + d + "AlreadyPickedNote" + "\r\n");
                        }
                        else
                        {
                            //PL.CellInfo
                            sB.Append(ICFolder.PlateIndex + d + RaftID + d + mlQuad + d + Label + d + Anno + d + Score + d + (i++) + d + "UNK" + d + ICFolder.PlateID + d + Well.Key + d + (PL.FOV + 1) + d + "-1" + d + Model + d + Status1 + "\r\n");
                            if (Counter++ > PickListLength) return (sB, AnnoList, Counter);
                        }
                    }
                }
            }
            return (sB, AnnoList, Counter);
        }

        #region Define Classes - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public void OpenInNotepadPlus(string FileToOpen)
        {
            string NotepadPath = @"C:\Program Files\Notepad++\notepad++.exe";
            if (!File.Exists(NotepadPath)) NotepadPath = @"C:\Program Files (x86)\Notepad++\notepad++.exe";
            if (!File.Exists(NotepadPath)) NotepadPath = @"notepad.exe";
            try
            {
                var Pr = Process.Start(NotepadPath, FileToOpen);
            }
            catch { }
            return;
        }

        private void ClassDefinition_View(object sender, EventArgs e)
        {
            string Filename; DialogResult DR; INCELL_Folder ICFolder;
            OFDStd("Choose a JSON ClassDefinition File (save it then Load it into memory)", "", "JSON files (*.json)|*.json|All files (*.*)|*.*", "Class Definition", out ICFolder, out Filename, out DR);

            if (DR == DialogResult.OK)
            {
                OpenInNotepadPlus(Filename);
                txBx_Update.Text += "\r\nEdit and save the Class Definitions, then press mRafts>Load to load them into memory before exporting anything.";
            }
        }

        public ClassDefinition ClassDef = null;

        private void ClassDefinition_LoadFromPlateMap_Clipboard(object sender, EventArgs e)
        {
            ClassDef = ClassDefinition.LoadfromPlatemap(Clipboard.GetText());
            NVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = ClassDef.WellLookUp_WellLabel_Comma_Folder_Semicolon;
            btn_MultiRaftExportImages.ForeColor = Color.Blue;
            txBx_Update.Text += "\r\nClass Definitions Loaded.";
        }

        private void ClassDefinition_Load(object sender, EventArgs e)
        {
            string Filename; DialogResult DR; INCELL_Folder ICFolder;
            OFDStd("Choose a JSON ClassDefinition File (This will Load it into memory)", "", "JSON files (*.json)|*.json|All files (*.*)|*.*", "Class Definition", out ICFolder, out Filename, out DR);
            if (DR != DialogResult.OK) return;
            ClassDef = ClassDefinition.Load(Filename);
            if (ClassDef.Dict_PlateIndex_WellLabel_to_ClassLayout.Count == 0)
            {
                txBx_Update.Text = "ERROR LOADING. Check Json formatting, remember you can't have a trailing comma.";
                return;
            }
            btn_MultiRaftExportImages.ForeColor = Color.Blue;
            NVP.ClassDef = ClassDef;
            NVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = ClassDef.WellLookUp_WellLabel_Comma_Folder_Semicolon;
            txBx_Update.Text += "\r\nClass Definitions Loaded.";
        }

        private void ClassDefinition_SaveDefault(object sender, EventArgs e)
        {
            var CD = new ClassDefinition();
            CD.Add("1", "A - 1", "G1");
            CD.Add("1", "A - 2", "Mix50");
            CD.Add("1", "B - 1", "G2");
            CD.Add("1", "B - 2", "G1");
            CD.Add("2", "A - 1", "G2");
            CD.Add("2", "A - 2", "G1");
            CD.Add("2", "B - 1", "Mix50");
            CD.Add("2", "B - 2", "G2");

            string Filename; DialogResult DR;
            var SFD = new SaveFileDialog(); SFD.Filter = "JSON Files|*.json";
            DR = SFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                Filename = SFD.FileName;
                CD.Save(Filename);
            }
        }

        private void ClassDefinition_CopySpotfire(object sender, EventArgs e)
        {
            if (ClassDef == null) MessageBox.Show("Please first click on mRafts, then LOAD a JSON class definition file before using this. To find the format of the file, click mRafts > Save_Default, then edit that file.");
            Clipboard.SetText(ClassDef.GetClassDefinitions_Style_Spotfire());
        }

        private void ClassDefinition_CopyPlateMap(object sender, EventArgs e)
        {
            if (ClassDef == null) MessageBox.Show("Please first click on mRafts, then LOAD a JSON class definition file before using this. To find the format of the file, click mRafts > Save_Default, then edit that file.");
            Clipboard.SetText(ClassDef.GetClassDefinitions_Style_PlateMap());
        }

        #endregion

        private string PullOutInCartaImages(Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> PlateID_WellFOV_CellInfoS, wvDisplayParams wvParams, RaftImage_Export_Settings Settings, FormRaftCal CalForm, BackgroundWorker BW = null)
        {
            Settings.ExportFolderBase = Path.Combine(@"S:\CellImages\", PlateID_WellFOV_CellInfoS.First().Key.Substring(0, 6), "iBased");
            Settings.SubFolderByPlateID = false;
            foreach (var PlateIDSet in PlateID_WellFOV_CellInfoS)
            {
                var PreFolder = dB_Main.Folders.Where(x => x.PlateID == PlateIDSet.Key).ToArray();
                if (PreFolder.Length == 0) PreFolder = dB_Main.Folders.Where(x => x.PlateID.Substring(0, PlateIDSet.Key.Length) == PlateIDSet.Key).ToArray();
                if (PreFolder.Length == 0) return PlateIDSet.Key + " couldn't be found in this dB. Try changing the spool location.";
                var IC_Folder = PreFolder[0];
                if (BW != null) BW.ReportProgress(0, "\r\nLoading plate " + PlateIDSet.Key + " . . . ");
                //Package up the IDs so they can be exported (link them to images and points)
                var ICPL = ImageCheck_PointList.FromWellFieldCells(IC_Folder, PlateIDSet.Value);

                //Now call the export on this
                if (BW != null) BW.ReportProgress(0, "\r\nStarting Export . . . ");
                RaftCal.ExportCalForm.Trigger_Export(Settings, ICPL, IC_Folder, wvParams, CalForm, BW);
            }
            if (BW != null) BW.ReportProgress(0, "\r\n\r\nDone and Done.");
            return "";
        }

        private void GeneratedPickList(object sender, EventArgs e)
        {
            INCELL_Folder FolderToPass; DialogResult DR; string Filename;
            OFDStd("Pick List Requested . .", "3 PickRelease", "txt files (*.txt)|*.txt|All files (*.*)|*.*", "Please select a .txt file with the pick list to make an excel for", out FolderToPass, out Filename, out DR);
            if (DR != DialogResult.OK) return;

            txBx_Update.Text += "\r\nStarting Background Excel (takes a few seconds) . . "; Application.DoEvents();
            GenerateMasksClass go_PickListExport = new GenerateMasksClass();
            if (go_PickListExport.TemplateLoaded)
            {
                string s = go_PickListExport.CSVFileCheck(Filename, FolderToPass.FIViD);
                if (s == "success")
                {
                    txBx_Update.Text = go_PickListExport.CreateCSVFiles() + " " + FolderToPass.FIViD;
                }
                else
                {
                    DialogResult dr = MessageBox.Show("error: " + s, "would you like to attempt to create CSV files anyways?", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        txBx_Update.Text = go_PickListExport.CreateCSVFiles();
                    }
                }
            }
            else
            {
                txBx_Update.Text = "Error loading template.";
            }
        }

        private void OFDStd(string UpdateMessage, string Subfolder, string Filter, string Title, out INCELL_Folder FolderToPass, out string Filename, out DialogResult DR)
        {
            string plateID = listBox_Scans.SelectedItem == null ? "" : listBox_Scans.SelectedItem.ToString();
            if (plateID == "") FolderToPass = null;
            else FolderToPass = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            txBx_Update.Text = UpdateMessage; Application.DoEvents();

            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = GetAssumedFolder(Subfolder, FolderToPass);
            OFD.Filter = Filter; // "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = Title;  // "Please select a .txt file with the pick list to make an excel for";
            //OFD.RestoreDirectory = true;
            DR = OFD.ShowDialog();
            Filename = OFD.FileName;
        }

        public static string GetAssumedFolder(string Subfolder, INCELL_Folder FolderToPass)
        {
            string PreFolder = Path.Combine(@"R:\FIVE\EXP", FolderToPass == null ? "" : FolderToPass.FIViD);
            if (!Directory.Exists("R:"))
            {
                PreFolder = "c:\\temp\\";
            }
            if (!Directory.Exists(PreFolder)) Directory.CreateDirectory(PreFolder);
            string Fldr = Path.Combine(PreFolder, Subfolder);
            if (!Directory.Exists(Fldr)) Directory.CreateDirectory(Fldr);
            return Fldr;
        }


        #region Rename Normalize - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void RenameNormalizeClick(object sender, EventArgs e)
        {
            string plateID = listBox_Scans.SelectedItem.ToString();
            INCELL_Folder FolderToPass = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            INCELL_WV_Notes WVNotes = FolderToPass.InCell_Wavelength_Notes;
            //WVNotes.List.RemoveAt(4);
            if (!WVNotes.HasAllAbbreviations)
            {
                txBx_Update.Text = "Abbreviations weren't filled out. Click open and edit the WavelengthNotes.xml file to put in the proper abbreviations.\r\n\r\n" +
                    "Hoechst - H\r\n" +
                    "MitoTracker - M\r\n" +
                    "Lysotracker - L\r\n" +
                    "mtSox - SXM\r\n" +
                    "ER - ER\r\n" +
                    "Golgi - GO\r\n" +
                    "TMRM - T\r\n" +
                    "GFP - GFP\r\n" +
                    "CellMask - C\r\n" +
                    "TubulinTracker - U\r\n" +
                    "Brightfield - B\r\n";
                return;
            }

            OpenFileDialog OFD = new OpenFileDialog(); OFD.InitialDirectory = GetAssumedFolder("2 MLModels", FolderToPass);
            OFD.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = "Please select the exported file from this plate to normalize and rename. *** Using mlPlateIndex to get averages per Plate/Array.";
            DialogResult DR = OFD.ShowDialog();
            if (DR != DialogResult.OK) return;

            List<object> Args = new List<object>() { FolderToPass, WVNotes, OFD.FileName, NVP.NormDivisorColumn };
            //Args = new List<object> { null, WVNotes, @"R:\five\exp\fiv574\1 Data\FIV574_Plate1_VC_2 compiled_RN1.csv", NVP.NormDivisorColumn };
            bgWrk_RenameNormalize.RunWorkerAsync(Args);
        }

        private void bgWrk_RenameNormalize_DoWork(object sender, DoWorkEventArgs e)
        {
            bool Rename_Wavelengths = true;

            List<object> Args = (List<object>)e.Argument;
            INCELL_Folder FolderToPass = (INCELL_Folder)Args[0]; INCELL_WV_Notes WVNotes = (INCELL_WV_Notes)Args[1];
            string FullPathOfPlainFile = (string)Args[2]; //@"R:\five\exp\fiv424\2 MLModels\FIV424 20210614 simp.csv";
            string NormDivisorColumn = (string)Args[3]; //Decide what you want to normalize to (the divisor) "mlPlateIndex"
            string FullPathNormFile = Path.Combine(Path.GetDirectoryName(FullPathOfPlainFile), Path.GetFileNameWithoutExtension(FullPathOfPlainFile) + "_rnnorm.txt");
            char delimRead = Path.GetExtension(FullPathOfPlainFile).ToUpper() == ".CSV" ? ',' : '\t';
            char delimWrite = '\t';

            StreamReader SR = new StreamReader(FullPathOfPlainFile);
            //First figure out which columns we want
            string[] headers = SR.ReadLine().Split(delimRead);
            if (!headers.Contains(NormDivisorColumn)) { bgWrk_RenameNormalize.ReportProgress(0, "Couldn't find the Normalization Divisor Column"); SR.Close(); return; }
            int NormDivisorColIdx = 0; for (int i = 0; i < headers.Length; i++) { if (headers[i] == NormDivisorColumn) { NormDivisorColIdx = i; break; } }
            HashSet<string> HS = new HashSet<string>(headers.Where(x => x.Contains(" wv")));

            List<int> ColsConsider = headers.Select((a, i) => new { Value = a, Index = i }).Where(b => b.Value.Contains(" wv")).Select(c => c.Index).ToList();
            if (ColsConsider.Count == 0) { ColsConsider = headers.Select((a, i) => new { Value = a, Index = i }).Where(b => b.Value.Contains(" WV")).Select(c => c.Index).ToList(); Rename_Wavelengths = false; }
            //Now get ready to get the statistics
            Dictionary<Tuple<string, int>, QStats> TrackStats = new Dictionary<Tuple<string, int>, QStats>();
            string[] cols; int colUse; Tuple<string, int> Key; string NormDivisorVal; int Counter = 0;

            void InsideReadLoop()
            {
                cols = SR.ReadLine().Split(delimRead);
                for (int c = 0; c < ColsConsider.Count; c++)
                {
                    colUse = ColsConsider[c]; NormDivisorVal = cols[NormDivisorColIdx];
                    Key = new Tuple<string, int>(NormDivisorVal, colUse);
                    if (!TrackStats.ContainsKey(Key)) TrackStats.Add(Key, new QStats(headers[colUse], colUse));
                    TrackStats[Key].AddValue(cols[colUse]);
                }
            }

            bgWrk_RenameNormalize.ReportProgress(0, "Reading initial . . ");
            for (int i = 0; i < 2000; i++) { InsideReadLoop(); if (SR.EndOfStream) break; } //Lets check the first few 1000 values slowly to root out any errors
            IEnumerable<int> ToRemove = TrackStats.Where(x => (x.Value.UniqueValues.Count < 10) || ((x.Value.Errors / x.Value.Count) > 0.8)).Select(x => x.Key.Item2); //Now remove any where there are less than 10 unique values or the vast majority are errors
            foreach (int IDX2Remo in ToRemove) ColsConsider.Remove(IDX2Remo);
            bgWrk_RenameNormalize.ReportProgress(0, "Reading rest . . ");
            while (!SR.EndOfStream) InsideReadLoop(); //Now continue with the rest
            SR.Close();

            //Now rename things
            bgWrk_RenameNormalize.ReportProgress(0, "Reconstructing . . ");
            StreamWriter SW = new StreamWriter(FullPathNormFile);
            SR = new StreamReader(FullPathOfPlainFile); StringBuilder SB;
            if (Rename_Wavelengths || delimWrite != delimRead)
            {
                cols = SR.ReadLine().Split(delimRead); SB = new StringBuilder();
                for (int c = 0; c < cols.Length; c++) SB.Append(WVNotes.Rename(cols[c]) + delimWrite);
                SW.WriteLine(SB);
            }
            else
            {
                SW.WriteLine(SR.ReadLine());
            }
            //Write out the normalized version of everything
            while (!SR.EndOfStream)
            {
                if (++Counter % 1000 == 0) bgWrk_RenameNormalize.ReportProgress(0, Counter);
                cols = SR.ReadLine().Split(delimRead); SB = new StringBuilder();
                NormDivisorVal = cols[NormDivisorColIdx];
                for (int c = 0; c < cols.Length; c++)
                {
                    if (ColsConsider.Contains(c))
                    {
                        Key = new Tuple<string, int>(NormDivisorVal, c);
                        SB.Append(TrackStats[Key].Norm(cols[c]));
                    }
                    else
                    {
                        SB.Append(cols[c]);
                    }
                    SB.Append(delimWrite);
                }
                SW.WriteLine(SB);
            }
            SR.Close();
            SW.Close();
        }

        private void bgWrk_RenameNormalize_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void bgWrk_RenameNormalize_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = "Finished Norm";
        }

        #endregion

        #region Export Mask Images - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        private void ExportMaskImages(object sender, EventArgs e)
        {
            bool temp = false;
            if (temp)
            {
                //1/6/2023 Used this code to map the 20x to 10x pattern
                string tPath = @"R:\five\exp\fiv810\Full 12 Well Scan 20x.xfpf";
                var sCor = sCoordinateSet.Load(tPath);
                Clipboard.SetText(sCor.ToText());
            }

            LogEvent("ExportMaskImages");
            txBx_Update.Text = ""; DialogResult DR;
            //First look in a default location, then if not there, prompt or export everything
            string plateID = listBox_Scans.SelectedItem.ToString();
            INCELL_Folder FolderToPass = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            DirectoryInfo DI = new DirectoryInfo(FolderToPass.MaskFolder);
            if (DI.Exists)
            {
                //Rename this assuming they want a new one
                DR = MessageBox.Show("We found an existing Mask folder. Press OK to backup the existing folder and continue.", "Generate Mask Images", MessageBoxButtons.OKCancel);
                if (DR == DialogResult.Cancel) return;
                string Rename; DirectoryInfo DI2; int c = 1;
                do
                {
                    Rename = Path.Combine(DI.Parent.FullName, DI.Name + c++);
                    DI2 = new DirectoryInfo(Rename);
                } while (DI2.Exists);
                DI.MoveTo(Rename);
                DI = new DirectoryInfo(FolderToPass.MaskFolder); //5/2022 Added this back in since it may get removed
            }
            //Ask for the mask list and proceed
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = "Please select a .txt file with the cell list to make masks for. Mask will be expanded by " + NVP.RegParams.Mask_PixelsExpand + " pixels. " + (NVP.RegParams.Mask_20x_to_10x ? "[20x>10x]" : "");
            OFD.RestoreDirectory = true;
            DR = OFD.ShowDialog(); if (DR != DialogResult.OK) return;

            DI.Create();
            FileInfo FI = new FileInfo(OFD.FileName);
            FI.CopyTo(FolderToPass.MaskList_FullPath);

            txBx_Update.Text = "Exporting Masks . . ";
            bgWrk_ExportMasks.RunWorkerAsync(new List<object> { FolderToPass, NVP.RegParams });
        }

        private void bgWrk_ExportMasks_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (List<object>)e.Argument;
            var Folder = (INCELL_Folder)Args[0];
            var SVRP = (FIVE_IMG.SVP_Registration_Params)Args[1];

            string msg = FIVE_IMG.MasksHelper.ExportAll(Folder, SVRP, bgWrk_ExportMasks);
            e.Result = msg;
        }

        private void bgWrk_ExportMasks_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = (string)e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void bgWrk_ExportMasks_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string msg = (string)e.Result;
            if (msg != "")
            {
                MessageBox.Show(msg);
            }
            else
            {
                txBx_Update.Text = "Exported Masks.";
            }
        }

        #endregion

        private void MultiExportXDCE(object sender, EventArgs e)
        {
            txBx_Update.Text = "Starting . . "; Application.DoEvents();
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            List<INCELL_Folder> folders = new List<INCELL_Folder>(plateIDs.Length);
            List<XDCE> XDCEs = new List<XDCE>(plateIDs.Length);
            foreach (string id in plateIDs) folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());
            foreach (INCELL_Folder folder in folders) XDCEs.Add(folder.XDCE);

            string PathToExp = Path.Combine(txBx_HCS_Image_DestFolder.Text, "XDCECompile " + DateTime.Now.ToString("hhmmss") + ".txt");
            InCellLibrary.XDCE.Compile(XDCEs, PathToExp);
            txBx_Update.Text = "Done exporting XDCEs\r\n" + PathToExp;
        }

        private void DeleteSelectedScan(object sender, EventArgs e)
        {
            string plateID = listBox_Scans.SelectedItem.ToString();
            INCELL_Folder Folder = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            DialogResult DR = MessageBox.Show("Are you sure you want to delete " + Folder.AdjustedPath, "Delete Check List", MessageBoxButtons.OKCancel);
            if (DR == DialogResult.OK)
            {
                File.Delete(Folder.AdjustedPath);
            }
        }

        private void CopyXDCEToClip_Click(object sender, EventArgs e)
        {
            string plateID = listBox_Scans.SelectedItem.ToString();
            INCELL_Folder Folder = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            StringBuilder sB = new StringBuilder();
            Dictionary<string, object> KVPs = Folder.XDCE.KVPs;
            foreach (KeyValuePair<string, object> KVP in KVPs) sB.Append(KVP.Key + "\t" + KVP.Value + "\r\n");
            Clipboard.SetText(sB.ToString());
            txBx_Update.Text = "Copied to Clipboard. Paste in Excel.";
        }

        /// <summary>
        /// Use this to split up a file to a set of smaller files
        /// </summary>
        /// <param name="FullPath">Path of the File to Split</param>
        /// <param name="MaxSizeBytes">Size in Bytes, for example 4Mb = 4000KBytes</param>
        private void SplitFileBySize(string FullPath, double MaxSizeKBytes)
        {
            StreamReader SR = new StreamReader(FullPath);
            StreamWriter SW = null;
            string t;
            double CurrentSize = MaxSizeKBytes + 1;
            int FileIndex = 0;
            while (!SR.EndOfStream)
            {
                if (CurrentSize > MaxSizeKBytes)
                {
                    CurrentSize = 0;
                    if (SW != null) SW.Close();
                    SW = new StreamWriter(FullPath + FileIndex++);
                }
                t = SR.ReadLine();
                SW.WriteLine(t);
                CurrentSize += (1.5 * (double)t.Length / 1024);
            }
            SR.Close();
            SW.Close();
        }

        public void ToggleButtonStates(bool StateRunning)
        {
            btn_Cancel.Enabled = StateRunning;
            //btn_Zoom_Rename_Images.Enabled = !StateRunning;
            btn_LoadDB.Enabled = !StateRunning;
            //btn_XCDE_Stitch.Enabled = !StateRunning;
        }

        public void Load_Save_Params(bool Save)
        {
            if (Save)
            {
                if (NVP == null) NVP = new FIVE_IMG.Store_Validation_Parameters();

                NVP.HCS_Image_SourceFolder = comboBox_Folder.Text;
                NVP.HCS_Image_DestinationFolder = txBx_HCS_Image_DestFolder.Text;
                NVP.HCS_Image_PlateName = txBx_HCS_Image_PlateName.Text;
                NVP.HCS_Image_CropSquare = chkBx_OnlyAnalyses.Checked;
                NVP.SearchPattern = txBx_Restrict_List.Text;

                NVP.FIVTools_Version = Application.ProductVersion;
                NVP.Save();
            }
            else
            {
                if (NVP == null) NVP = FIVE_IMG.Store_Validation_Parameters.Load();
                if (NVP == null)
                {
                    NVP = new FIVE_IMG.Store_Validation_Parameters();
                }
                else
                {
                    comboBox_Folder.Text = NVP.HCS_Image_SourceFolder;
                    if (NVP.MRU_FolderPaths != null)
                    {
                        comboBox_Folder.Items.Clear();
                        foreach (var item in NVP.MRU_FolderPaths) comboBox_Folder.Items.Add(item);
                    }

                    txBx_HCS_Image_DestFolder.Text = NVP.HCS_Image_DestinationFolder;
                    txBx_HCS_Image_PlateName.Text = NVP.HCS_Image_PlateName;
                    chkBx_OnlyAnalyses.Checked = NVP.HCS_Image_CropSquare;
                    txBx_Restrict_List.Text = NVP.SearchPattern;

                    //labelSequence_SetMain.Label_Sequence_Dictionary = NVP.Sequence_Interest_Dict;
                }
            }
        }

        public FIVE_IMG.Store_Validation_Parameters PreRun()
        {
            if (NVP == null) NVP = new FIVE_IMG.Store_Validation_Parameters();
            Load_Save_Params(true);
            ToggleButtonStates(true);
            return NVP;
        }

        public void UpdateProgress(string Message)
        {
            if (Message.StartsWith("^"))
            {
                Message = Message.Substring(1);
                //InCell_Displays.Add(Message);
                //RestrictList_Update();
            }
            txBx_Update.Text = Message + "\r\n" + txBx_Update.Text;
            if (txBx_Update.Text.Length > 1000000)
                txBx_Update.Text = txBx_Update.Text.Substring(0, 1000000);
        }

        private void btn_Refresh_MouseClick(object sender, MouseEventArgs e)
        {
            btn_Refresh_Click(sender, null);
        }

        private void btn_Refresh_MouseUp(object sender, MouseEventArgs e)
        {
            btn_Refresh_Click(sender, e);
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            LogEvent("Refresh");
            txBx_Restrict_List.Text = txBx_Restrict_List.Text.ToUpper().Trim();
            //TODO: Make this smarter
            //File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "refreshClick: restrict_list- " + txBx_Restrict_List.Text + "\r\n");
            string NewFolderPath = FolderPath_AndPrep(); if (NewFolderPath == "") return;
            //File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "refreshClick: newFolderPath- " + NewFolderPath + "\r\n");

            NVP.SearchPattern = ""; //Temporarily turn this off
            try
            {
                MouseEventArgs MEA = (MouseEventArgs)e;
                //if (MEA.Button == MouseButtons.Right)
                {
                    NVP.SearchPattern = txBx_Restrict_List.Text;
                    txBx_Update.Text += "Restricting based on search . .";
                }
            }
            catch 
            {
       
            }
            bgWrk_CheckFolders.RunWorkerAsync(NVP
                );
        }

        private void btn_LoadFolders(object sender, EventArgs e)
        {
            try
            {
                string NewFolderPath = FolderPath_AndPrep(); if (NewFolderPath == "") return;

                dB_Main = InCellLibrary.INCELL_DB.Load_From_BasePath(NewFolderPath);
                if (dB_Main == null) txBx_Update.Text = "ERROR " + InCellLibrary.INCELL_DB.RecentError;
                else FinishUp();
            }
            catch
            {
                txBx_Update.Text = "Error trying to Load folder, try changing path. ." + InCellLibrary.INCELL_DB.RecentError;
            }
        }

        public string FolderPath_AndPrep()
        {
            //First check if the source folder exists
            string NewFolderPath = null;
            NewFolderPath = comboBox_Folder.Text;
            if (File.Exists(NewFolderPath)) { UpdateProgress("Source Path not Found."); return ""; }

            //Now attempt to create the destination folder
            try
            {
                string DestLocation = txBx_HCS_Image_DestFolder.Text;
                if (!Directory.Exists(Path.GetPathRoot(DestLocation)))
                {
                    DestLocation = "c:\\temp\\";
                    txBx_HCS_Image_DestFolder.Text = DestLocation;
                }
                DirectoryInfo DI2 = new DirectoryInfo(DestLocation);
                if (!DI2.Exists) { DI2.Create(); }
            }
            catch { UpdateProgress("Trouble creating the destination folder, check your path"); return ""; }
            try
            {
                if (NVP.RegenMRUs(NewFolderPath)) comboBox_Folder.Items.Add(NewFolderPath);
            }
            catch
            { UpdateProgress("Error with MRU Regen."); return ""; }
            PreRun();
            return NewFolderPath;
        }

        private void comboBox_Folder_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            btn_LoadFolders(sender, e); //Trigger Load Folder
        }

        private void comboBox_Folder_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //This is too soon
        }

        private void bgWrk_CheckFolders_DoWork(object sender, DoWorkEventArgs e)
        {
            FIVE_IMG.Store_Validation_Parameters NVPi = (FIVE_IMG.Store_Validation_Parameters)e.Argument;
            if (dB_Main == null) { dB_Main = new INCELL_DB(); }
            dB_Main.BasePath = NVPi.HCS_Image_SourceFolder;
            dB_Main.Refresh(bgWrk_CheckFolders, NVPi.SearchPattern);
        }

        private void bgWrk_CheckFolders_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.UserState.ToString());
        }

        private void bgWrk_CheckFolders_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateProgress("Finished!");
            FinishUp();
        }

        private void FinishUp()
        {
            ToggleButtonStates(false);
            labl_dbUpdated.Text = "dB Updated: " + dB_Main.LastUpdated.ToShortDateString() + " " + dB_Main.LastUpdated.ToShortTimeString();
            InCell_Displays = dB_Main.Folders.Select(x => x.ToString()).ToList();
            InCell_Folders = dB_Main.Folders
                .GroupBy(p => p.ToString())
                .ToDictionary(x => x.Key, x => x.Last().FullPath);
            InCell_Object = dB_Main.Folders
                .GroupBy(p => p.ToString())
                .ToDictionary(x => x.Key, x => x.Last());
            RestrictList_Update();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            bgWrk_CheckFolders.CancelAsync();
        }

        private void btn_ImageZoomRename_Click(object sender, EventArgs e)
        {
            FIVE_IMG.CellZoom.ZoomTest();
            txBx_Update.Text += "\r\nFinished Zooming";
        }

        public void FIVE_Simulations()
        {
            bool First = true;
            StreamWriter SW = new StreamWriter(@"c:\temp\track_sims.txt", false); //decide whether to overwrite or not

            //Figure out how to represent this (instead of enrichment)
            //Maybe enrichment should ust be the position of 4 random guides in the bkgd (or 8, etc), instead of all the others (which makes the average much less)
            //Also put in the probability of survival for background and for the positive control
            Debugger.Break();

            List<double> FractionPerturb_ToHarvest = new List<double>() { 1, 1 };
            int LibrarySize = 1600;
            fPhenotype.ProbOfLifeNegative = 0.05;

            for (int CellsPergRNA = 200; CellsPergRNA <= 1200; CellsPergRNA = (int)(CellsPergRNA * Math.Sqrt(2)))
            {
                for (double PerCellCuttingProb = 0.05; PerCellCuttingProb <= 0.4; PerCellCuttingProb *= 2)
                {
                    for (double FractionPositiveHasEffect = 0.5; FractionPositiveHasEffect <= 0.9; FractionPositiveHasEffect += 0.1)
                    {
                        int CellsTotal = LibrarySize * CellsPergRNA;
                        fConditions c = new fConditions(LibrarySize, (double)8 / LibrarySize, 0.002, CellsTotal, 0.4, 0, 0, FractionPerturb_ToHarvest, 1, 0.1);
                        c.PerCellChanceOfCutting = PerCellCuttingProb;
                        fPhenotype.ProbOfLifePositive = FractionPositiveHasEffect;

                        fTrialSet ts = new fTrialSet(c, 1);
                        ts.Export(SW, First); First = false;
                        Debug.Print(CellsPergRNA + " " + PerCellCuttingProb);
                    }
                }
            }

            SW.Close();
            Debug.Print("Done Sim");

            bool skip = true;
            if (!skip)
            {
                FIVE_Sumulations_20190326();
            }
        }

        private static void FIVE_Sumulations_20190326()
        {
            //FIVE_Tools_Main.MLServices.Program.Main(null);
            //
            //  3/26/2019 TO DO : The phenotypes currently aren't included in the conditions, but there is a placeholder for them and it just has to be implemented (doesn't export with them yet either)
            //                    Probably also want this inside of a Background worker, since when doing the replicates it hits the break thing
            //
            Debugger.Break();
            bool First = true;
            List<double> FractionPerturb_ToHarvest = new List<double>() { 0.0024, 0.01 };
            StreamWriter SW = new StreamWriter(@"c:\temp\track_sims.txt", false); //decide whether to overwrite or not
            for (int i = 0; i < 6; i++) //Repeat everything 6 times
            {
                for (int LibrarySize = 150; LibrarySize <= 1500; LibrarySize = (int)(LibrarySize * 1.3))
                {
                    fConditions c = new fConditions(LibrarySize, 0.01, 0.05, 45000, 0.4, 40000, 0.05, FractionPerturb_ToHarvest, 2, 0.1);
                    fTrialSet ts = new fTrialSet(c, 1);
                    ts.Export(SW, First); First = false;
                    Debug.Print(LibrarySize.ToString());
                }
            }
            SW.Close();
        }

        private void btn_HCS_Analyses_Click(object sender, EventArgs e)
        {
            LogEvent("Compilation");
            //This starts the analysis on any set of selected Plate IDs
            txBx_Update.Text = "\r\nStarting Compilation . . \r\n";
            List<INCARTA_Analysis_Folder> AFs = SelectedResults();

            //Check if everything is calibrated and image-checked and has wv-notes, and abbreviations
            bool RenameWvs; bool OkToContinue;
            OkToContinue = GetInfo_Cal_Annotations_Abbrev(AFs, true, false, out RenameWvs);
            if (!OkToContinue) return;

            //Start the process
            var Args = new List<object>() { dB_Main, txBx_HCS_Image_DestFolder.Text, CellsPerFieldMin, CellsPerFieldMax, AFs, RenameWvs, bgWrk_CompileCells };
            bgWrk_CompileCells.RunWorkerAsync(Args);
        }

        private void bgWrk_CompileCells_DoWork(object sender, DoWorkEventArgs e)
        {
            List<object> Args = (List<object>)e.Argument;
            INCELL_Helper.CompileAnalyses((INCELL_DB)Args[0], (string)Args[1], (int)Args[2], (int)Args[3], (List<INCARTA_Analysis_Folder>)Args[4], (bool)Args[5], (BackgroundWorker)Args[6]);
        }

        private void bgWrk_CompileCells_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string ud = e.UserState.ToString();
            txBx_Update.Text = ud + (ud.Length < 6 ? ", " : "\r\n") + txBx_Update.Text;
        }

        private void bgWrk_CompileCells_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = "Done!" + "\r\n" + txBx_Update.Text;
        }

        private void CopyFirstAbbrev_ToSelected(object sender, EventArgs e)
        {
            if (listBox_Scans.SelectedItems.Count < 2)
            {
                txBx_Update.Text = "Select at least 2 scans, the first should have abbreviations, and those will be copied to the rest of the selected scans."; return;
            }

            //Get the list
            var plateIDs = listBox_Scans.SelectedItems.Cast<string>();
            var folders = new List<INCELL_Folder>(plateIDs.Count());
            foreach (string id in plateIDs) folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());

            //Make sure the first one has abbrev
            var wvNotes = folders[0].InCell_Wavelength_Notes;
            if (!wvNotes.HasAllAbbreviations)
            {
                txBx_Update.Text = "First selection needs abbreviations to copy to the rest. Please open CalCheck and add abbreviations for each wv."; return;
            }

            //Now assign it to the rest
            string Note = ""; bool skip = false;
            for (int i = 1; i < folders.Count; i++)
            {
                skip = false;
                var wvNoteDest = folders[i].InCell_Wavelength_Notes;
                if (wvNoteDest.HasAllAbbreviations) { Note += folders[i] + " has abbreviations, skipping assignment.\r\n"; skip = true; }
                if (wvNoteDest.Count != wvNoteDest.Count) { Note += folders[i] + " wavelengths do not match, skipping assignment.\r\n"; skip = true; }
                if (!skip)
                {
                    for (int w = 0; w < wvNoteDest.Count; w++)
                        wvNoteDest.List[w].Abbrev = wvNotes.List[w].Abbrev;
                    //TODO: Save these abbrev once they are assigned
                    Note += folders[i] + " assigned abbrevs.\r\n";
                }
            }
            txBx_Update.Text = Note;
        }

        private bool GetInfo_Cal_Annotations_Abbrev(INCELL_Folder Scan, bool clearOutput = true)
        {
            INCARTA_Analysis_Folder AF;
            if (Scan.AnalysisFolders.Count > 0) AF = Scan.AnalysisFolders[0];
            else { AF = new INCARTA_Analysis_Folder(); AF.Parent = Scan; }
            bool nothing;
            return GetInfo_Cal_Annotations_Abbrev(new List<INCARTA_Analysis_Folder>() { AF }, false, true, out nothing, clearOutput);
        }

        private bool GetInfo_Cal_Annotations_Abbrev(List<INCARTA_Analysis_Folder> AFs, bool NotifyIfMissing, bool AnnotationDetails, out bool RenameWvs, bool clearOutput = true)
        {
            if (clearOutput) txBx_Update.Text = "";
            double FractionWithAbbrevs; string MissingAbbrev = "";
            int CountTotal_Cal = 0; int CountCal = 0; int CountChk = 0; int CountAbbrevs = 0; string Missing = "";
            foreach (var AF in AFs)
            {
                if (AF.RelatedXDCE.Wells.Count <= 999) //Rafts so far only have 4 wells max, changed to 999 12/2023
                {
                    foreach (var Well in AF.RelatedXDCE.Wells)
                    {
                        CountTotal_Cal++;
                        string THead = Well.Value.PlateID + "." + Well.Key;
                        if (Well.Value.HasRaftCalibration) CountCal++; else Missing += THead + " NoCal, ";
                        if (Well.Value.HasImageCheck) CountChk++; else Missing += THead + " NoChk, ";
                        if (AnnotationDetails)
                        {
                            txBx_Update.Text += THead + " " + Well.Value.ImageCheckInfo + "\r\n";
                        }
                    }
                }
                if (AF.Parent.InCell_Wavelength_Notes.HasAllAbbreviations) CountAbbrevs++; else MissingAbbrev += AF.Parent.PlateID + ", ";
            }
            txBx_Update.Text += "Found " + CountTotal_Cal + " Wells Total . . \r\n";
            FractionWithAbbrevs = (float)CountAbbrevs / AFs.Count;
            txBx_Update.Text += FractionWithAbbrevs.ToString("0%") + " of Scans have wv Abbreviations \r\n";
            if (CountCal > 0) txBx_Update.Text += ((float)CountCal / CountTotal_Cal).ToString("0%") + " of Wells have Calibration \r\n";
            txBx_Update.Text += ((float)CountChk / CountTotal_Cal).ToString("0%") + " Wells have Annotations \r\n";
            txBx_Update.Text += Missing + "\r\n";
            Application.DoEvents();

            //Setup the WV renames
            RenameWvs = FractionWithAbbrevs == 1 ? NVP.RenameColumns_DuringInCartaCompile : false;
            if (RenameWvs != NVP.RenameColumns_DuringInCartaCompile && NotifyIfMissing)
            {
                var res = MessageBox.Show("In the XML Settings, 'RenameColumns_DuringInCartaCompile' is turned on, but not all scans have abbreviations. Either click 'Cancel' and save the abbreviations before compiling, or click 'OK' to continue WITHOUT renaming the columns.\r\n" + MissingAbbrev, "Rename Columns?", MessageBoxButtons.OKCancel);
                if (res != DialogResult.OK) return false;
            }
            return true;
        }

        public (double FractionCal, double FractionAnnos, double FractionAbbrev, string Message) GetInfo_Cal_Anno_Abbrev(IEnumerable<INCELL_Folder> Folders)
        {
            int CountTotal = 0; int CountCal = 0; int CountChk = 0; int CountAbbrevs = 0; string Missing = ""; string MissingAbbrev = "";
            foreach (var F in Folders)
            {
                foreach (var Well in F.XDCE.Wells)
                {
                    CountTotal++;
                    string THead = Well.Value.PlateID + "." + Well.Key;
                    if (Well.Value.HasRaftCalibration) CountCal++; else Missing += THead + " NoCal, ";
                    if (Well.Value.HasImageCheck) CountChk++; else Missing += THead + " NoChk, ";
                }

                if (F.InCell_Wavelength_Notes.HasAllAbbreviations) CountAbbrevs++; else MissingAbbrev += F.PlateID + ", ";
            }
            return (((float)CountCal / CountTotal), (float)CountChk / CountTotal, (float)CountAbbrevs / Folders.Count(), Missing + "\r\n" + MissingAbbrev);
        }

        public int CellsPerFieldMin
        {
            get
            {
                TextBox tB = txBx_CellsPerField_Min; int output = 0;
                if (!int.TryParse(tB.Text, out output))
                {
                    tB.Text = "0";
                }
                return output;
            }
        }

        public int CellsPerFieldMax
        {
            get
            {
                TextBox tB = txBx_CellsPerField_Max; int output = 1000;
                if (!int.TryParse(tB.Text, out output))
                {
                    tB.Text = "1000";
                }
                return output;
            }
        }

        private void btnSimulations_Click(object sender, EventArgs e)
        {
            Debugger.Break();
            FIVE_Simulations();
            bool BC = false;
            if (BC)
            {
                FIVE_Tools_Main.BarcodeMutantSim.main();
            }
        }

        private void btn_SpoolManagerStart(object sender, EventArgs e)
        {
            var SForm = new _5SpoolManager.SpoolManagerSettingsForm();
            SForm.Show();
        }

        private void button_LoadRaftCalTest_Click(object sender, EventArgs e)
        {
            Load_Save_Params(true);

            this.TopMost = false;
            LogEvent("CalCheck");
            var CalCheck = new FormRaftCal(NVP); //TODO - Currently this is ideal, since it doesn't do well if you try and reload from within
            PreviousCalCheck = CalCheck;

            string plateID = listBox_Scans.SelectedItem.ToString();
            if (true)
            {
                INCELL_Folder Folder = dB_Main.Folders.Where(x => x.PlateID.ToUpper() == plateID.ToUpper()).ToList()[0];
                var SR = SelectedResults();
                CalCheck.AnalysisFolder = SR.Count == 0 ? null : SR.First();
                CalCheck.Folder = Folder.AdjustedPath;
                CalCheck.ParentMode = Mode;
                CalCheck.IC_Folder = Folder;
                if (!Folder.HasMasks && Mode == FIVToolsMode.PlateID_WellList)
                {
                    txBx_Update.Text = "Please generate the mask images first.";
                    return;
                }
                DialogResult DR = CalCheck.ShowDialog();
                if (DR == DialogResult.Abort && Mode == FIVToolsMode.PlateID_WellList)
                {
                    LeicaSetupPlate_Settings(CalCheck);
                }

                //Now update the database for this one automatically
                int FolderIDX = dB_Main.Folders.IndexOf(Folder);
                CalCheck.IC_Folder.BasePath_Current = Folder.BasePath_Current; CalCheck.IC_Folder.BasePath_Saved = Folder.BasePath_Saved;  //Somehow this gets lost
                dB_Main.Folders[FolderIDX] = CalCheck.IC_Folder;

                foreach (var AF in CalCheck.IC_Folder.AnalysisFolders)
                {
                    var T = _DictLookupAFs.Where(x => x.Value.ResultsID == AF.ResultsID);
                    if (T.Any())
                        _DictLookupAFs[T.First().Key] = AF;
                }
            }
            else
            {

            }
        }

        private void GetFolderandReg(string barcode)
        {
            int time = Int32.Parse(DateTime.Now.ToString("HH"));
            if (barcode == "ruhroh")
            {
                // Don't even try!
                File.WriteAllText("C:\\temp\\AILBase\\INI\\PlateReg.ini", INI_Files.barcodeReaderWorked(false));
                if (time >= 6 && time < 23)
                    MessageBox.Show("this will cause an error, Josh gets email, and he will reset");
            }
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "getfolder&reg" + "\r\n");
            Load_Save_Params(true);
            this.TopMost = false;
            LogEvent("CalCheck");
            var CalCheck = new FormRaftCal(NVP); //TODO - Currently this is ideal, since it doesn't do well if you try and reload from within
            PreviousCalCheck = CalCheck;
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "madeCalCheck" + "\r\n");
            comboBox_Folder.Text = "F:\\Images\\Josh";
            txBx_Restrict_List.Text = barcode;
            btn_Refresh_Click(null, null);

            System.Threading.Thread.Sleep(4000);

            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "sleep/wokeup" + "\r\n");
            //string plateID = listBox_Scans.Items[0].ToString();
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "plateID - " + barcode + "\r\n");

            INCELL_Folder Folder = dB_Main.Folders.FirstOrDefault(x => x.PlateID.ToUpper() == barcode.ToUpper());
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "made folder" + "\r\n");


            CalCheck.Folder = Folder.AdjustedPath;
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"FOLDERPATH: {CalCheck.Folder}" + "\r\n");

            CalCheck.ParentMode = Mode;
            var sb = new StringBuilder();
            foreach (var wellName in Folder.XDCE.MaskWells)
            {
                sb.Append(wellName + ", ");
            }
            string wellList = sb.ToString();
            string l = wellList.Substring(0, wellList.Length - 2);
            CalCheck.Return_WellList = l;
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "welllist- " + l + "\r\n");

            if (!Folder.HasMasks)
            {
                File.WriteAllText("C:\\temp\\AILBase\\INI\\PlateReg.ini", INI_Files.barcodeReaderWorked(false));
                if (time >= 6 && time < 23)
                    MessageBox.Show("this will cause an error, Josh gets email, and he will reset");
            }

            else
            {
                File.WriteAllText("C:\\temp\\AILBase\\INI\\PlateReg.ini", INI_Files.barcodeReaderWorked(true));
                LeicaSetupPlate_Settings(CalCheck);
            }


            int FolderIDX = dB_Main.Folders.IndexOf(Folder);
            CalCheck.IC_Folder.BasePath_Current = Folder.BasePath_Current; CalCheck.IC_Folder.BasePath_Saved = Folder.BasePath_Saved;  //Somehow this gets lost
            dB_Main.Folders[FolderIDX] = CalCheck.IC_Folder;

            foreach (var AF in CalCheck.IC_Folder.AnalysisFolders)
            {
                var T = _DictLookupAFs.Where(x => x.Value.ResultsID == AF.ResultsID);
                if (T.Any())
                    _DictLookupAFs[T.First().Key] = AF;
            }
        }

        private void txBx_Restrict_List_KeyUp(object sender, KeyEventArgs e) { RestrictList_Update(); }

        private void txBx_Restrict_List_TextChanged(object sender, EventArgs e) { RestrictList_Update(); }

        /// <summary>
        /// Allows for a space to search for multiple names
        /// </summary>
        public void RestrictList_Update()
        {
            List<string> Res;
            string search = txBx_Restrict_List.Text.ToUpper().Trim();
            if (search == "") Res = InCell_Displays;
            else
            {
                string[] searchParts = search.Split(' ');
                Res = searchParts.SelectMany(part => InCell_Displays.Where(x => x.ToUpper().Contains(part))).Distinct().ToList();
            }

            if (chkBx_OnlyAnalyses.Checked)
            { //Remove any that don't have analyses
                List<string> RemoveList = new List<string>();
                foreach (string PlateID in Res) if (InCell_Object[PlateID].AnalysisFolders.Count == 0) RemoveList.Add(PlateID);
                Res = Res.Except(RemoveList).ToList();
            }
            if (chkBx_Rafts.Checked)
            { //Remove any that don't have analyses
                List<string> RemoveList = new List<string>();
                var HS = new HashSet<string>();
                foreach (string PlateID in Res)
                {
                    string AQ = InCell_Object[PlateID].AQP.ToUpper();
                    //if (!InCell_Object[PlateID].XDCE.Plate_Name.Contains("RAFT")) 
                    if (AQ.Contains("QUAD") || AQ.Contains("SINGLE")) { }
                    else
                    {
                        RemoveList.Add(PlateID);
                        HS.Add(AQ);
                    }
                }
                Res = Res.Except(RemoveList).ToList();
            }
            if (chkBx_Today.Checked)
            { //Remove any that don't have analyses
                List<string> RemoveList = new List<string>();
                foreach (string PlateID in Res) if (InCell_Object[PlateID].LatestEvent < DateTime.Now.AddHours(-24)) RemoveList.Add(PlateID);
                Res = Res.Except(RemoveList).ToList();
            }

            listBox_Scans.Items.Clear();
            listBox_Scans.Items.AddRange(Res.Cast<object>().ToArray());
            ResultsList_Update();
        }

        public void ResultsList_Update()
        {
            listBox_Results.Items.Clear(); _DictLookupAFs = new Dictionary<string, INCARTA_Analysis_Folder>();
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            INCELL_Folder folder;
            foreach (string id in plateIDs)
            {
                var fPre = dB_Main.Folders.Where(x => x.PlateID == id).ToList();
                if (fPre.Count < 1) return;
                folder = fPre[0];
                foreach (INCARTA_Analysis_Folder AF in folder.AnalysisFolders) listBox_Results.Items.Add(ListBoxResult_Text(AF));
            }
            listBox_Results_SelectedIndexChanged(null, null);
        }

        Dictionary<string, INCARTA_Analysis_Folder> _DictLookupAFs;
        public string ListBoxResult_Text(INCARTA_Analysis_Folder AF)
        {
            string ID = _DictLookupAFs.Count.ToString();
            _DictLookupAFs.Add(ID, AF);
            return AF.INCARTA_ProtocolName + "\t" + AF.ResultsID + "\t" + AF.PlateID + "\t" + ID;
        }

        public List<INCARTA_Analysis_Folder> SelectedResults()
        {
            string[] SelectedLines = listBox_Results.SelectedItems.Cast<string>().ToArray();
            var Folders = new List<INCARTA_Analysis_Folder>();
            int k; string ID;
            foreach (string fullLine in SelectedLines)
            {
                k = fullLine.LastIndexOf("\t"); ID = fullLine.Substring(k + 1);
                Folders.Add(_DictLookupAFs[ID]);
            }
            return Folders;
        }

        private void btn_DeleteAnalyses_Click(object sender, EventArgs e)
        {
            List<INCARTA_Analysis_Folder> Folders = SelectedResults();
            if (Folders.Count == 0) return;
            txBx_Update.Text = "Deleting, please wait . . . ";
            MessageBox.Show("Click OK to delete " + Folders.Count + " analysis folder(s).", "Confirm deletion", MessageBoxButtons.OKCancel);
            foreach (INCARTA_Analysis_Folder AF in Folders)
            {
                txBx_Update.Text += AF.ResultsID;
                Application.DoEvents();
                AF.Delete();
            }

            txBx_Update.Text = "Finished Deleting";
            ResultsList_Update();
            dB_Main.Save();
        }

        private void listBox_Scans_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_RaftCal.Enabled = btn_OpenFolder.Enabled = listBox_Scans.SelectedIndices.Count == 1;
            btn_XCDE_Stitch.Enabled = listBox_Scans.SelectedIndices.Count > 0;
            btn_CompareScans.Enabled = listBox_Scans.SelectedIndices.Count > 1;
            btn_MultiExportXDCE.Enabled = listBox_Scans.SelectedIndices.Count > 0;
            btn_MultiRaftExportImages.Enabled = listBox_Scans.SelectedIndices.Count > 0;
            ResultsList_Update();
        }

        private void listBox_Scans_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            var index = listBox_Scans.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                listBox_Scans.SelectedItems.Clear();
                listBox_Scans.SelectedIndex = index;
                //_selectedMenuItem = listBoxCollectionRounds.Items[index].ToString();
                contextMenuStrip_Scans.Show(Cursor.Position);
                contextMenuStrip_Scans.Visible = true;
            }
            else
            {
                contextMenuStrip_Scans.Visible = false;
            }
        }

        private void listBox_Scans_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            button_LoadRaftCalTest_Click(sender, e);
        }

        private void listBox_Results_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_DeleteAnalyses.Enabled = btn_HCS_Analyses.Enabled = btn_ExportTraced.Enabled = listBox_Results.SelectedIndices.Count > 0;
        }

        private void chkBx_OnlyAnalyses_CheckedChanged(object sender, EventArgs e)
        {
            RestrictList_Update();
        }

        private void btn_OpenFolder_Click(object sender, EventArgs e)
        {
            string plateID = listBox_Scans.SelectedItem.ToString();
            INCELL_Folder Folder = dB_Main.Folders.Where(x => x.PlateID == plateID).ToList()[0];
            string folderPath = Folder.FullPath;
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe" //Testing
                };
                Process.Start(startInfo);
            }
        }

        private void btn_CompareScans_Click(object sender, EventArgs e)
        {
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            List<INCELL_Folder> folders = new List<INCELL_Folder>(plateIDs.Length);
            foreach (string id in plateIDs)
            {
                folders.AddRange(dB_Main.Folders.Where(x => x.PlateID == id).ToList());
            }
            StringBuilder sB = new StringBuilder();
            XDCE First = folders[0].XDCE;
            XDCE Other;
            for (int i = 1; i < folders.Count; i++)
            {
                Other = folders[i].XDCE;
                sB.Append(First.Compare(Other));
            }
            txBx_Update.Text = sB.ToString();
        }

        private void ExportTracedImages(object sender, EventArgs e)
        {
            List<INCARTA_Analysis_Folder> Folders = SelectedResults();
            foreach (INCARTA_Analysis_Folder folder in Folders)
            {
                folder.ExportTracedImages();
            }
        }

        private void ImageStitchingFunc(BackgroundWorker bw, IEnumerable<string> folders)
        {
            XDCE_ImageStitcher GEStitch;

            string directoryName = System.IO.Path.GetDirectoryName(folders.ToList<string>()[0].ToUpper()).Split(System.IO.Path.DirectorySeparatorChar)[^1];

            // Regex pattern to match the directory name followed by an underscore and any characters after
            string pattern = $@"\b{Regex.Escape(directoryName)}_(.*)$";

            // Perform the regex search to find the specific pattern
            Match match = Regex.Match(folders.ToList<string>()[0].ToUpper(), pattern, RegexOptions.IgnoreCase);
            var result = match.Groups[1].Value;
            INCELL_Folder Folder;
            if (result == "")
            {
                Folder = dB_Main.Folders.Where(x =>
                    folders.Any(folder =>
                        x.PlateID.Substring(x.PlateID.LastIndexOf('\\') + 1).ToUpper() ==
                        folder.Substring(folder.LastIndexOf('\\') + 1).ToUpper()
                    )
                ).ToList().FirstOrDefault();
            }
            else
                Folder = dB_Main.Folders.Where(x => x.PlateID.ToUpper() == result).ToList()[0];

            if (Folder != null)
            {
                GEStitch = new XDCE_ImageStitcher(Folder);
                GEStitch.StitchAll();
            }
            else
            {
                foreach (string folder in folders)
                {
                    GEStitch = new XDCE_ImageStitcher(folder);
                    GEStitch.StitchAll();
                }
            }
        }

        private void bgWrk_Stitcher_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            //btn_XCDE_Stitch.Enabled = false;
            ImageStitchingFunc(bw, e.Argument as List<string>);
        }

        private void StitchImages_click(object sender, EventArgs e)
        {
            string[] plateIDs = listBox_Scans.SelectedItems.Cast<string>().ToArray();
            var folders = new List<string>(plateIDs.Length);
            foreach (string id in plateIDs)
            {
                folders.Add(dB_Main.Folders.Where(x => x.PlateID == id).ToList()[0].FullPath);
            }
            this.bgWrk_Stitcher.RunWorkerAsync(folders);
        }

        private void FixPython()
        {
            string root = @"\\genstorage.wustl.edu\smlab\CORE PROJECTS\Projects in progress\";
            DirectoryInfo DI = new DirectoryInfo(root);
            foreach (FileInfo fi in DI.GetFiles("proposed*.py", SearchOption.AllDirectories))
            {
                string t = File.ReadAllText(fi.FullName);
                t = t.Replace("NGS_one_main_program", "NGS_CF");
                File.WriteAllText(fi.FullName, t);
            }
        }

        private void btn_CMSTools_click(object sender, EventArgs e)
        {
            var form = new CMSImageTools.CMSForm();
            form.Show();
        }

        public void Compile_Parse_VCF()
        {
            //Use this for - - - GATK vcf Hapolotype Caller Funcutat Funcotat
            string tPath = @"R:\FIVE\EXP\FIV526\526_499_funcotations\output_combined"; //@"R:\FIVE\EXP\FIV499\funcotations\export_control"; // @"R:\FIVE\EXP\FIV526\NGS2\funcotations\export\";
            string szStart = "#CHROM";
            string szStart2 = "FUNCOTATION=[";
            string szEnd2 = "]";
            StringBuilder sB = new StringBuilder();
            string Prefix;
            bool First = true;
            char d1 = '\t';
            char d2 = '|';
            char l1 = '\n';
            foreach (string FileN in Directory.GetFiles(tPath))
            {
                Prefix = Path.GetFileNameWithoutExtension(FileN);
                string tFull = File.ReadAllText(FileN);
                tFull = tFull.Substring(tFull.IndexOf(szStart));
                string[] lines = tFull.Split(l1);
                if (First)
                {
                    sB.Append("Source" + d1 + lines[0] + d1 + "Funcotations" + l1); First = false;
                }
                for (int i = 1; i < lines.Length; i++)
                {
                    if (lines[i] != "")
                    {
                        if (lines[i].Contains(szStart2))
                        {
                            string func = lines[i].Substring(lines[i].IndexOf(szStart2) + szStart2.Length);
                            func = func.Substring(0, func.IndexOf(szEnd2));
                            func = func.Replace(d2, d1);
                            sB.Append(Prefix + d1 + lines[i] + d1 + func + l1);
                        }
                        else
                        {
                            sB.Append(Prefix + d1 + lines[i] + l1);
                        }
                    }
                }
            }
            File.WriteAllText(Path.Combine(tPath, "compileVCF03.txt"), sB.ToString());
        }

        public void MoveAroundFiles_ForTraining()
        {
            string pth = @"S:\Phys\NeuroMito2e\";
            float Train_Test_Split = 0.8f;
            string ext = ".BMP";

            var NewFolder = new Dictionary<Tuple<bool, string>, string>() {
                {new Tuple<bool,string>(false,"L") , "image\\train" },
                {new Tuple<bool,string>(false,"R"), "segment\\train" },
                {new Tuple<bool,string>(true,"L"), "image\\val" },
                {new Tuple<bool,string>(true,"R"), "segment\\val" }
            };

            int FileNum;
            string FileEnd;
            string name;
            string newPath; string tempFolder;
            var Files = Directory.GetFiles(pth, "*" + ext);
            int Count = 0; int Total = Files.Length; int ValSwap = (int)(Total * Train_Test_Split);
            foreach (string file in Files)
            {
                name = Path.GetFileName(file);
                FileNum = int.Parse(name.Split('_')[0]);
                FileEnd = name.Split('_')[1][0].ToString();
                tempFolder = Path.Combine(pth, NewFolder[new Tuple<bool, string>(Count > ValSwap, FileEnd)]);
                newPath = Path.Combine(tempFolder, FileNum + ext);
                if (!Directory.Exists(tempFolder)) Directory.CreateDirectory(tempFolder);
                //Also have to remove everything but the number
                File.Move(file, newPath);
                Count++;
                if (Count % 100 == 0) Debug.Print(Count.ToString() + " " + ((double)Count / Total).ToString("0.0%"));
            }
            Debug.Print("Finished MoveForTraining");
        }

        public void Extra_Utilities()
        {
            HashSet<string> WellsToDo = new HashSet<string>(); HashSet<string> ToDelete = new HashSet<string>();
            WellsToDo.Add("C - 2"); WellsToDo.Add("B - 3"); WellsToDo.Add("A - 4"); //WellsToDo.Add("C - 1"); WellsToDo.Add("B - 2"); WellsToDo.Add("A - 3");
            string Well; string NewName; int Counter = 0;
            foreach (var file in Directory.GetFiles(@"K:\Training MFN2\"))
            {
                Counter++;
                Well = file.Substring(28, 5);
                if (WellsToDo.Contains(Well))
                    NewName = @"K:\Training MFN2\G1\" + Path.GetFileName(file);
                else
                    NewName = @"K:\Training MFN2\G2\" + Path.GetFileName(file);
                File.Move(file, NewName);
            }

            //FixPython();
            foreach (var file in ToDelete)
            {
                File.Delete(file);
            }

            bool skip = true;
            if (!skip)
            {
                FIVE_IMG.ImageAlign_Return.ReadFromFolder(@"R:\FIVE\EXP\FIV538\6 Images\Scan 3 Overlays 1");
            }
        }

        private void bgWrk_Stitcher_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) txBx_Update.Text += e.Error.Message;
        }

        private void txBx_HCS_Image_SourceFolder_TextChanged(object sender, EventArgs e)
        {
            btn_LoadDB.Enabled = true;
        }

        private void button_NGSAssist_Click(object sender, EventArgs e)
        {
            LogEvent("NGSAssist");
            FIVE_Tools_Main.NGS_Assist_Form NGSAF = new FIVE_Tools_Main.NGS_Assist_Form();
            NGSAF.ShowDialog();
        }

        private void btn_AlleleFragments_Click(object sender, EventArgs e)
        {
            NGsAllelesForm.ShowDialog();
        }

        private void btnLibraryAligner_Click(object sender, EventArgs e)
        {
            LogEvent("LibAligner");
            //This will hopefully make it quicker to restart
            var NGS_LA_Form = new FIVE_Tools_Main.NGS_LibraryAligner_Form();
            NGS_LA_Form.ShowDialog();
        }

        private void lbl_Combine_Az_Scores_Click(object sender, EventArgs e)
        {
            string PlateID = listBox_Scans.SelectedItem == null ? "" : listBox_Scans.SelectedItem.ToString();
            string PlateToUse = "";
            if (PlateID == "") PlateID = txBx_Restrict_List.Text;
            if (PlateID == "UNK") PlateID = new DirectoryInfo(Name).Name;
            try
            {
                INCELL_Folder FolderToPass = dB_Main.Folders.Where(x => x.PlateID.Contains(PlateID)).ToList()[0];
                PlateToUse = FolderToPass.FIViD;
            }
            catch
            {
                PlateToUse = PlateID;
            }
            Compiler_Form.SelectedFIVID = PlateToUse;
            Compiler_Form.ShowDialog();
        }

        #region File System Watching - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void lbl_Notifications_Click(object sender, EventArgs e)
        {
            bool Leave = true;
            if (Leave) return; //Not sure about this yet . . 

            //InCarta >
            //I:\SingleArray_QuadWell_4Color_JCB\SingleArray_QuadWell_4Color_JCB_FIV480A1_1\Results\FIV482a_2021-Mar-18-11-58-42
            //summary_metadata.csv  <<Ideal
            //result_metadata.csv
            //I:\SingleArray_QuadWell_4Color_JCB\SingleArray_QuadWell_4Color_JCB_FIV480A1_1\Results\FIV482a_2021-Mar-18-11-58-42\Misc
            //version_1-11_1_success.json

            FSW_InCarta = new FileSystemWatcher(@"i:\");
            FSW_InCarta.NotifyFilter = NotifyFilters.FileName;
            FSW_InCarta.Filter = "result_metadata.csv";
            FSW_InCarta.IncludeSubdirectories = true;
            FSW_InCarta.Created += FSW_InCarta_Created;
            FSW_InCarta.EnableRaisingEvents = true;

            //InCell >
            //I:\SingleArray_QuadWell_4Color_JCB\SingleArray_QuadWell_4Color_JCB_FIV480A1_1
            //SingleArray_QuadWell_4Color_JCB_FIV480A1_1.xdce

            FSW_InCell = new FileSystemWatcher(@"i:\");
            FSW_InCell.NotifyFilter = NotifyFilters.FileName;
            FSW_InCell.Filter = "*.xdce";
            FSW_InCell.IncludeSubdirectories = true;
            FSW_InCell.Created += FSW_InCell_Created; ;
            FSW_InCell.EnableRaisingEvents = true;

            //CMS > 
            //R:\dB\CMS_Machine\CMSData\FIV484A1P4\AirDB_FIV484A1P4.sqlite

            this.Text = "FIVE Tools (monitoring)";
        }

        /// <summary>
        /// Gives the number of minutes between when the provided file was created and when it's parent directory was created.
        /// (This is used to distinguish short events and ignore them)
        /// </summary>
        public Double AgeOfParentFolder_CompareToFile_Minutes(string PathOfFile)
        {
            FileInfo FI = new FileInfo(PathOfFile);
            return AgeOfParentFolder_CompareToFile_Minutes(FI);
        }

        public Double AgeOfParentFolder_CompareToFile_Minutes(FileInfo FI)
        {
            return (FI.CreationTime - FI.Directory.CreationTime).TotalMinutes;
        }

        /// <summary>
        /// Check to see if this a known "test" scan or analysis type
        /// </summary>
        /// <param name="FI"></param>
        /// <returns></returns>
        public bool Confirm_Scan_InCarta(FileInfo FI)
        {
            if (FI.Directory.Name == "Temp") return false;
            if (FI.Directory.Name == "__MiniScan__") return false;
            return true;
        }

        public bool PostMessage_SomethingFinished(string NewFileName, int Type)
        {
            FileInfo FI = new FileInfo(NewFileName);
            if (Confirm_Scan_InCarta(FI))
            {
                Double TimeElapsed = AgeOfParentFolder_CompareToFile_Minutes(NewFileName);
                if (TimeElapsed < 4) return false; //Don't notify if the scan was less than x minutes

                string PlateID = Type == 1 ? FI.Name : FI.Directory.Parent.Name;
                switch (Type)
                {
                    case 1: //InCell
                        PostMessage_Actual("InCell Scan " + PlateID + " finished.");
                        break;
                    case 2: //InCarta
                        PostMessage_Actual("InCarta Analysis " + PlateID + " finished.");
                        break;
                    default:
                        break;
                }
                return true;
            }
            return false;
        }

        public void PostMessage_Actual(string message)
        {
            //https://stackoverflow.com/questions/57050321/how-do-i-post-a-message-to-microsoft-team-from-other-application
            string Webhook = "https://gowustl.webhook.office.com/webhookb2/083d93be-e406-4ba6-8b58-7397a5441aaa@4ccca3b5-71cd-4e6d-974b-4d9beb96c6d6/IncomingWebhook/3ecf9b1f43d9446b982dc823b4581d01/d3831c69-676c-4d3a-be35-57c471c56f01";

            //curl.exe -H "Content-Type:application/json" -d "{'text':'Servers x is started.'}" https://example.webhook.office.com/webhookb2/4dee1c26-036c-4bd2-af75-eb1abd901d18@3c69a296-d747-4ef3-9cc5-e94ee78db030/IncomingWebhook/87557542b42d8d3b04453c4a606f2b92/b852b3d0-84b6-4d98-a547-ae5f53452235

            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "curl.exe";
            startInfo.Arguments = "-H \"Content-Type:application/json\" -d \"{'text':'" + message + "'}\" " + Webhook;
            process.StartInfo = startInfo;
            process.Start();
        }

        //Email(message);
        //1160084f.gowustl.onmicrosoft.com@amer.teams.ms
        public static string Email(string htmlString)
        {
            //This isn't quite working
            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress("1160084f.gowustl.onmicrosoft.com@amer.teams.ms", "Teams"));
            msg.From = new MailAddress("wbuchser@wustl.edu", "Monitor");
            msg.Subject = "InCell Update";
            msg.Body = htmlString;
            msg.IsBodyHtml = true;

            //William.Buchser.0@gmail.com
            //

            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("wbuchser@wustl.edu", "");
            client.Port = 25; // You can use Port 25 if 587 is blocked (mine is!)
            client.Host = "smtp.office365.com";
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            try
            {
                client.Send(msg);
                //lblText.Text = "Message Sent Succesfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
                //lblText.Text = ex.ToString();
            }

            //MailMessage message = new MailMessage();
            //SmtpClient smtp = new SmtpClient();
            //message.From = new MailAddress("AquilaTheEagle@gmail.com");
            //message.To.Add(new MailAddress("1160084f.gowustl.onmicrosoft.com@amer.teams.ms"));
            //message.Subject = "InCell Update";
            //message.IsBodyHtml = true; //to make message body as html  
            //message.Body = htmlString;
            //smtp.Port = 587;
            //smtp.Host = "smtp.gmail.com"; //for gmail host  
            //smtp.EnableSsl = true;
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new NetworkCredential("AquilaTheEagle@gmail.com", "");
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtp.Send(message);
            return "";
        }

        private void FSW_InCell_Created(object sender, FileSystemEventArgs e) { PostMessage_SomethingFinished(e.FullPath, 1); }

        private void FSW_InCarta_Created(object sender, FileSystemEventArgs e) { PostMessage_SomethingFinished(e.FullPath, 2); }

        #endregion


        public static string LogFileLocation => @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\Software\FIVE_Tools\Settings\Log.txt";

        public static void LogEvent(string Note)
        {
            try
            {
                char delim = '\t';
                if (Environment.UserName.ToUpper() == "WBUCHSER" || Environment.UserName.ToUpper() == "JDMSO") return; //Ignore Willie/Josh's since it is usually just developing
                if (!Directory.Exists(Path.GetPathRoot(LogFileLocation))) return;

                string statusWrite = DateTime.Now.ToString("yyyy/MM/dd HHH:mm:ss") + delim + Environment.UserName + delim + Environment.MachineName + delim + Note + "\r\n";
                File.AppendAllText(LogFileLocation, statusWrite);
            }
            catch
            {
                //Folder probably didn't exist or wasn't accessible
            }
        }

        private void Lbl_Columns_Reduce_Click(object sender, EventArgs e)
        {
            var FC = new FIVE_Tools_Main.FormCompile01();
            FC.ShowDialog();
        }

        private void btn_Align2Scans_Click(object sender, EventArgs e)
        {
            INCELL_Folder FolderToPass; DialogResult DR; string Filename1, Filename2; string Title;

            Title = "Scan # 1. Please select a .csv file with the list of cells (from InCarta). You need 'PlateID'~FIVXXXA1_3, 'WELL LABEL'~A - 1, 'FOV' and 'Object ID' and Nuclei Max Width, Height, Top, Left wv1.";
            OFDStd("Aligning Scans . . ", "1 Data", "csv files (*.csv)|*.csv|All files (*.*)|*.*", Title, out FolderToPass, out Filename1, out DR);
            if (DR != DialogResult.OK) return;
            txBx_Update.Text = "Loading first . . "; Application.DoEvents();
            var PCI1 = CellInfoS.Plate_WellField_CellInfo(Filename1);
            if (PCI1 == null) { txBx_Update.Text = "Problem loading or parsing the file.  Please see these instructions:\r\n" + Title; return; }


            Title = "Scan # 2. Please select a .csv file with the list of cells (from InCarta). You need 'PlateID'~FIVXXXA1_3, 'WELL LABEL'~A - 1, 'FOV' and 'Object ID' and Nuclei Max Width, Height, Top, Left wv1.";
            OFDStd("Aligning Scans . . ", "1 Data", "csv files (*.csv)|*.csv|All files (*.*)|*.*", Title, out FolderToPass, out Filename2, out DR);
            if (DR != DialogResult.OK) return;
            txBx_Update.Text = "Loading second . . "; Application.DoEvents();
            var PCI2 = CellInfoS.Plate_WellField_CellInfo(Filename2);
            if (PCI2 == null) { txBx_Update.Text = "Problem loading or parsing the file.  Please see these instructions:\r\n" + Title; return; }

            TrackCells.CellMatcher.Start(PCI1, PCI2, Filename1 + "_AlignTable.txt");
        }

        private void btn_NearestNeighbors_Click(object sender, EventArgs e)
        {
            bool cont = false;
            if (!cont) return;
            LogEvent("Nearest Neighbors");
            INCELL_Folder FolderToPass; DialogResult DR; string Filename;
            string Title = "Please select a .csv file with the list of cells (from InCarta). You need 'PlateID'~FIVXXXA1_3, 'WELL LABEL'~A - 1, 'FOV' and 'Object ID' and Nuclei Max Width, Height, Top, Left wv1. Also, the settings for this selected plate ID will be used for export.";
            OFDStd("Nearest Neighbors . . ", "1 Data", "csv files (*.csv)|*.csv|All files (*.*)|*.*", Title, out FolderToPass, out Filename, out DR);
            if (DR != DialogResult.OK) return;

            txBx_Update.Text = "Loading cells . . "; Application.DoEvents();
            Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> PlateID_WellField_CellInfo = CellInfoS.Plate_WellField_CellInfo(Filename);
            if (PlateID_WellField_CellInfo == null)
            {
                txBx_Update.Text = "Problem loading or parsing the file.  Please see these instructions:\r\n" + Title;
                return;
            }
            //Build this into a BG worker
            List<object> Args = new List<object>() { PlateID_WellField_CellInfo, NVP, Filename };
            bgWork_NearestNeighbor.RunWorkerAsync(Args);
        }


        private void bgWork_NearestNeighbor_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (List<object>)e.Argument;
            var PlateID_WellField_CellInfo = (Dictionary<string, Dictionary<string, HashSet<CellInfoS>>>)Args[0];
            var NVPi = (FIVE_IMG.Store_Validation_Parameters)Args[1];
            string FilePathLoad = (string)Args[2];

            //Try this out for each Plate, Well, FOV
            var R = new Random(); var sB = new StringBuilder(); double MaxDist = -1; var NeedsMax = new HashSet<CellInfoS>();
            foreach (var Plate in PlateID_WellField_CellInfo)
            {
                bgWork_NearestNeighbor.ReportProgress(0, "Calculating . . " + Plate.Key);
                foreach (var WellField in Plate.Value)
                {
                    CellInfoS cellA, cellB;
                    double KeyDist;
                    var tArr = WellField.Value.ToArray();
                    if (tArr.Length == 1) { NeedsMax.Add(tArr[0]); continue; } //If there is only 1 cell, it needs the max distance, but we don't know it yet
                    for (int i = 0; i < tArr.Length; i++)
                    {
                        cellA = tArr[i];
                        if (cellA.NearestNeighbor != null) continue;
                        var tSort = new SortedDictionary<double, CellInfoS>();
                        for (int j = 0; j < tArr.Length; j++)
                        {
                            if (i == j) continue;
                            cellB = tArr[j];
                            KeyDist = cellA.DistanceTo(cellB);
                            while (tSort.ContainsKey(KeyDist)) KeyDist += (R.NextDouble() / 100000);
                            tSort.Add(KeyDist, cellB);
                        }
                        KeyDist = tSort.First().Key; cellB = tSort[KeyDist];
                        cellA.NearestNeighbor = cellB; cellA.NNDist = KeyDist; if (KeyDist > MaxDist) MaxDist = KeyDist;
                        //cellB.NearestNeighbor = cellA; cellB.NNDist = KeyDist; //Tried to grab the other one, but it may have an even closer neighbor
                        if (sB.Length == 0) sB.Append(cellA.ExportNN(true) + "\r\n");
                        sB.Append(cellA.ExportNN() + "\r\n");
                    }
                    if (R.Next(0, 15) == 5) bgWork_NearestNeighbor.ReportProgress(0, WellField.Key);
                }
            }
            //Now go back to any singles and export those . . if there are only singles in the whole dataset, then we won't get a header set
            foreach (var cellA in NeedsMax)
            {
                cellA.NearestNeighbor = cellA; cellA.NNDist = 100 * Math.Round(4 * MaxDist / 100, 0);
                sB.Append(cellA.ExportNN() + "\r\n");
            }
            bgWork_NearestNeighbor.ReportProgress(0, "Exporting Table . . ");
            File.WriteAllText(FilePathLoad + "_NearNeighb.txt", sB.ToString());
            bgWork_NearestNeighbor.ReportProgress(0, "Done with Nearest Neighbors");
        }

        private void bgWork_NearestNeighbor_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void bgWork_NearestNeighbor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }


        private void lbl_XMLSettings_Click(object sender, EventArgs e)
        {
            string XMLPath = FIVE_IMG.Store_Validation_Parameters.DefaultPath;
            OpenInNotepadPlus(XMLPath);
        }

        private void btn_MetadataEntry_Click(object sender, EventArgs e)
        {
            FIV_IMG_CLib.DataLineageForm DLF = new FIV_IMG_CLib.DataLineageForm();
            DLF.ShowDialog();
        }

        #region Bootstrap - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void btn_Bootstrap_Click(object sender, EventArgs e)
        {
            FIVE_Tools_Main.FormBootstrap FBS = new FIVE_Tools_Main.FormBootstrap();
            FBS.ShowDialog();
        }

        #endregion

        private void lbl_PerceptiLabs_CSV_Click(object sender, EventArgs e)
        {
            //PL_CSV PL CSV TF Deploy
            FormPLInteract.Show();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Load_Save_Params(true);

            if (FSW_InCarta != null) FSW_InCarta.EnableRaisingEvents = false;
            if (FSW_InCarta != null) FSW_InCell.EnableRaisingEvents = false;
        }

        private void btn_Layouts_Click(object sender, EventArgs e)
        {
            form_Layouts.Show();
        }

        public void PPTGrid()
        {
            //Powerpoint Grid
            //string[] Files =  Directory.GetFiles(@"R:\five\exp\FIV521\6 Images\FIV521A1_1\Renamed","*.BMP");
            //string[] Files = Directory.GetFiles(@"R:\five\exp\FIV532\6 Images\ReName", "*.BMP");
            //string[] names = Files.Select(x => Path.GetFileNameWithoutExtension(x)).Select(x => x.Substring(0, x.Length - 5)).ToArray();
            int rows = 4; int cols = 8; int Pages = 6; List<string> tList = new List<string>();
            string[] Files = Directory.GetFiles(@"S:\RaftExport\FIV661\Raft TubMito\", "*.jpg");
            for (int i = 0; i < Pages; i++)
            {
                var G1 = GetRandSubset(Files, 6, 2, "G1", rows * cols / 2);
                var G2 = GetRandSubset(Files, 6, 2, "G2", rows * cols / 2);
                tList.AddRange(G1); tList.AddRange(G2);
            }
            string[] names = tList.Select(x => Path.GetFileNameWithoutExtension(x)).Select(x => x.Substring(x.Length - 9)).ToArray(); //decide on the names
            PPT_Wrap.MakePPTGrid("FIV661 iPSC, G2=MFN2 (Dox at iPSC)", tList.ToArray(), names, cols, rows);
        }

        public IEnumerable<string> GetRandSubset(IEnumerable<string> Files, int EndMinus, int CharCount, string Compare, int TotalToTake)
        {
            var T = Files.Where(x => x.Substring(x.Length - EndMinus, CharCount) == Compare);
            T = T.OrderBy(x => Guid.NewGuid()).Take(TotalToTake);
            return T;
        }

        public FormFSdB FSdB;
        private void label_FSdB_Click(object sender, EventArgs e)
        {
            if (FSdB == null) FSdB = new FormFSdB(NVP);
            FSdB.Show();
        }

        private void btn_CodeOnly_Click(object sender, EventArgs e)
        {
            contextMenuStrip_CodeOnly.Show(Cursor.Position);
            contextMenuStrip_CodeOnly.Visible = true;
        }

        private void btn_CodeOnly_Sub(object sender, EventArgs e)
        {
            Debugger.Break();
            bool skip = true;
            if (!skip)
            {
                MoveAroundFiles_ForTraining();
                Debugger.Break();

                FIVE.TF.TF_Image_Model.AAATest();
                Debugger.Break();

                ImageSumPixelIntensities();
                Debugger.Break();

                HMM_Reconstruct();
                Debugger.Break();

                Compile_Parse_VCF();
                Debugger.Break();

                Extra_Utilities();
                Debugger.Break();

                FIVE.Name_Link.RenameTest();
                Debugger.Break();

                CompileText.Run(); //Various things here
                Debugger.Break();

                FIVE_IMG.PL.PLDeployModule.AA_TestRun();
                Debugger.Break();

                PPTGrid();
                Debugger.Break();
            }
        }

        private void ImageSumPixelIntensities()
        {
            Debugger.Break();
            string PathA = @"S:\CellImages\Model_Images\NeuroMito_FIV768,73\MitoInTub-Nuc\G1 G2\G1";
            string PathLow = Path.Combine(PathA, "Low\\");
            string PathMid = Path.Combine(PathA, "Mid\\");
            string PathHigh = Path.Combine(PathA, "High\\");
            Directory.CreateDirectory(PathLow); Directory.CreateDirectory(PathMid);
            Directory.CreateDirectory(PathHigh);
            Bitmap BM; var sB = new StringBuilder(); int i = 0;
            foreach (var file in Directory.GetFiles(PathA))
            {
                i++;
                if (i % 10 == 0) Debug.Print(i.ToString());
                if (Path.GetFileName(file) == "Thumbs.db") continue;
                BM = new Bitmap(file);
                long Sum = 0;
                for (int x = 0; x < BM.Width; x++)
                {
                    for (int y = 0; y < BM.Height; y++)
                    {
                        Sum += BM.GetPixel(x, y).G;
                    }
                }
                sB.Append(file + "\t" + Sum + "\r\n");
                BM.Dispose();
                if (Sum < 1000) File.Move(file, Path.Combine(PathLow, Path.GetFileName(file)));
                else if (Sum < 3000) File.Move(file, Path.Combine(PathMid, Path.GetFileName(file)));
                else if (Sum < 5000) File.Move(file, Path.Combine(PathHigh, Path.GetFileName(file)));
            }
            Clipboard.SetText(sB.ToString());
        }

        private void HMM_Reconstruct()
        {
            Dictionary<string, string> MakeFromLines(string[] lines)
            {
                var dc = new Dictionary<string, string>();
                for (int i = 0; i < lines.Length; i += 2)
                {
                    dc.Add(lines[i], lines[i + 1]);
                }
                return dc;
            }

            string[] lines = File.ReadAllLines(@"R:\dB\DomainCentric\Restore\TIR_Regions\Files for Creation\output_new_20191031 0948_Trimmed.txt");
            var TStore = new List<Dictionary<string, string>>();
            for (int i = 0; i <= 5; i++)
            {
                TStore.Add(MakeFromLines(File.ReadAllLines(
                    @"R:\dB\DomainCentric\Restore\TIR_Regions\Files for Creation\Out2\TIR_neighbors[f" + i + "].faa")));
            }

            var sB = new StringBuilder();
            int countbad = 0;
            for (int i = 1; i < lines.Length; i++)
            {
                string[] cols = lines[i].Split('\t');
                int frame = int.Parse(cols[6]);
                if (frame > 0)
                {

                }
                string Key = ">" + cols[0] + "_" + cols[1] + "_" + cols[2] + "[f" + frame + "][l3332]";
                if (TStore[frame].ContainsKey(Key))
                {
                    sB.Append(lines[i] + '\t');
                    sB.Append(TStore[frame][Key] + "\r\n");
                }
                else countbad++;
            }

            File.WriteAllText(@"e:\temp\TIROut0081.txt", sB.ToString());
        }

    }



}
