﻿using FIVE.InCellLibrary;
using FIVE.RaftCal;
using FIVE_IMG;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Windows.Forms;

namespace FIVE
{

    public partial class FormMain : Form
    {

        private FIVToolsMode _Mode;
        public FIVToolsMode Mode
        {
            get { return _Mode; }
            set
            {
                _Mode = value;
                switch (_Mode)
                {
                    case FIVToolsMode.Normal:
                        panel1.Enabled = btn_XCDE_Stitch.Enabled = btn_MultiExportXDCE.Enabled = btnLibraryAligner.Enabled = btn_AlleleFragments.Enabled = btnNGSAUC.Enabled = btn_HCS_Analyses.Enabled = btn_DeleteAnalyses.Enabled = btn_ExportTraced.Enabled = lbl_Combine_Az_Scores.Enabled = true;
                        txBx_Update.Text = "";
                        break;
                    case FIVToolsMode.PlateID:
                        break;
                    case FIVToolsMode.PlateID_WellList:
                        panel1.Enabled = btn_XCDE_Stitch.Enabled = btn_MultiExportXDCE.Enabled = btnLibraryAligner.Enabled = btn_AlleleFragments.Enabled = btnNGSAUC.Enabled = btn_HCS_Analyses.Enabled = btn_DeleteAnalyses.Enabled = btn_ExportTraced.Enabled = lbl_Combine_Az_Scores.Enabled = false;
                        txBx_Update.Text = "Select a Plate, then open 'Cal Check'. Adjust a single calibration channel then press 'Finish'.";
                        break;
                    case FIVToolsMode.WellCalibrate:
                        //Turn off almost everything
                        panel1.Enabled = btn_XCDE_Stitch.Enabled = btn_MultiExportXDCE.Enabled = btnLibraryAligner.Enabled = btn_AlleleFragments.Enabled = btnNGSAUC.Enabled = btn_HCS_Analyses.Enabled = btn_DeleteAnalyses.Enabled = btn_ExportTraced.Enabled = lbl_Combine_Az_Scores.Enabled = false;
                        txBx_Update.Text = "Well Calibrate. Probably silent.";
                        break;
                    case FIVToolsMode.FOVRefine:
                        txBx_Update.Text = "FOV Refine. Probably silent.";
                        break;
                    case FIVToolsMode.Silent:
                        break;
                    case FIVToolsMode.Listening:
                        File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "listening, shh " + "\r\n");
                        ListenForLeica();
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// This allows the user to choose the plate they are interested in
        /// </summary>
        /// <param name="CalCheck"></param>
        internal void LeicaSetupPlate_Settings(FormRaftCal CalCheck)
        {
            //First - Setup the Well Start
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt",  DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "starting 001" + "\r\n");
            FIVE_IMG.InCell_To_Leica.Leica001_WellStart(CalCheck.IC_Folder, CalCheck.Return_WellList, CalCheck.Return_StartField, CalCheck.Return_CheckWellFields, CalCheck.Wavelength, CalCheck.Brightness, CalCheck.CurrentWVParams, NVP.RegParams);
            FIVE_MSG.PipeClass.Server_ReadyClose();
            this.TopMost = false;
            ListenForLeica();
        }

        /// <summary>
        /// This makes the UI inoperable and instead only listens for Pipe commands coming thru the helper program from Leica's Metamorph
        /// </summary>
        internal void ListenForLeica()
        {
            string MSG; string[] arr;
            //this.Hide();
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "in LfL" + "\r\n");
            while (true)
            {
                txBx_Update.Text += "\r\nListening Mode";
                Application.DoEvents(); Application.DoEvents();

                MSG = FIVE_MSG.PipeClass.Server_Start_WaitRead(); //Don't let it hit this if your debugging
                txBx_Update.Text = MSG;
                arr = MSG.Split(' ');
                File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "switch case: " + arr[0] + "\r\n");
                switch (arr[0])
                {
                    case "1":
                        PrepForSetupMode(); //Allows user to open up the scan they want to run (RaftCal)
                        //Go to LeicaSetupPlate_Settings() after this
                        //LeicaSetupPlate_Settings(PreviousCalCheck); // Just for Testing
                        return;
                    case "2":
                        txBx_Update.Text += "\r\n" +
                            (InCell_To_Leica.retryReg ? FIVE_IMG.InCell_To_Leica.Leica002_RetryWell() : 
                                FIVE_IMG.InCell_To_Leica.Leica002_NextWell()); //Save the info for this well in the Ini file (go get the initial fields for registration)
                        File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"002 done. retry?- {InCell_To_Leica.retryReg}" + "\r\n");
                        break;
                    case "3":
                        FormImageRegistration.RegParams = NVP.RegParams; //Get the latest parameters
                        if (InCell_To_Leica.SR.automateRegistration)
                        {
                            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "automateReg- true" + "\r\n");
                            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"max retry- retrycount = {InCell_To_Leica.SR.maxRetry - InCell_To_Leica.DetermineRetryCount()}" + "\r\n");
                            if (InCell_To_Leica.SR.maxRetry - InCell_To_Leica.DetermineRetryCount() >= 0)
                            {
                                (bool passed, string result) = FIVE_IMG.InCell_To_Leica.Leica003b_RegisterWell();
                                File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"passed?- {passed}" + "\r\n");
                                if (passed)
                                {
                                    FormImageRegistration.continueCondition = FIVE_Tools_Main.FormRegResult.Continue;
                                }
                                else if (InCell_To_Leica.SR.maxRetry - InCell_To_Leica.DetermineRetryCount() <= 0)
                                {
                                    // Skip this well!
                                    InCell_To_Leica.SR.skipWell = true;
                                    // Do not retry!
                                    FormImageRegistration.continueCondition = FIVE_Tools_Main.FormRegResult.SkipWell;
                                }
                                else
                                {
                                    FormImageRegistration.continueCondition = FIVE_Tools_Main.FormRegResult.Retry;
                                }
                            }
                            else
                            {
                                // Skip this well!
                                InCell_To_Leica.SR.skipWell = true;
                                // Do not retry!
                                FormImageRegistration.continueCondition = FIVE_Tools_Main.FormRegResult.SkipWell;
                            }
                            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"(0 is continue, 1 is exit, 2 is retry, 3 is skipwell) condition: {FormImageRegistration.continueCondition}" + "\r\n");

                        }
                        else
                        {
                            FormImageRegistration.ShowDialog();//Show user registration results, let them pick additional parameters
                        }
                        switch (FormImageRegistration.continueCondition)
                        {
                            case FIVE_Tools_Main.FormRegResult.Continue:
                                //Just keep going
                                string reg_ini_pass = INI_Files.Get_Reg_Ini(true);
                                File.WriteAllText(InCell_To_Leica.SR.FullPath_RegIni, reg_ini_pass);
                                break;
                            case FIVE_Tools_Main.FormRegResult.Exit:
                                string reg_ini_exit = INI_Files.Get_Reg_Ini(true);
                                File.WriteAllText(InCell_To_Leica.SR.FullPath_RegIni, reg_ini_exit);
                                InCell_To_Leica.SR.CancelRequested = true; //This will write out a cancel flag in the "NextFOV"
                                break;
                            case FIVE_Tools_Main.FormRegResult.Retry:
                                string reg_ini_fail = INI_Files.Get_Reg_Ini(false);
                                File.WriteAllText(InCell_To_Leica.SR.FullPath_RegIni, reg_ini_fail);
                                break;
                            case FIVE_Tools_Main.FormRegResult.SkipWell:
                                string reg_ini_SkipWell = INI_Files.Get_Reg_Ini(true);
                                File.WriteAllText(InCell_To_Leica.SR.FullPath_RegIni, reg_ini_SkipWell);
                                InCell_To_Leica.SR.CancelRequested = true; //This will write out a cancel flag in the "NextFOV"
                                break;
                            default:
                                break;
                        }
                        txBx_Update.Text += "\r\n" + FormImageRegistration.ResultMessage;
                        System.Threading.Thread.Sleep(20); // pause, maybe writing out interrupts reading by metamorph?
                        break;
                    case "4":
                        txBx_Update.Text += "\r\n" +
                            InCell_To_Leica.Leica004_NextFOV(); //Get the coordinates for the next field for the .ini file
                        break;
                    case "5":
                        txBx_Update.Text += "\r\n" +
                            InCell_To_Leica.Leica005_RefineFOV(NVP.RegParams.InitialErrorDivisor); //Actually refine the position (do registration)
                        Application.DoEvents(); Application.DoEvents(); System.Threading.Thread.Sleep(500);//Wait a Sec so we can see any errors
                        break;
                    case "6":
                        txBx_Update.Text += "\r\n" +
                            InCell_To_Leica.Leica005_RefineFOV(11, 2); //Refine registration (only used for testing)
                        break;
                    case "7":
                        txBx_Update.Text += "\r\n" +
                            InCell_To_Leica.Leica007_SaveCropped(); //Outputs a cropped version of the mask that is aligned to the motor position
                        break;
                    case "8":
                        txBx_Update.Text += "\r\nLeaving Listening Mode.";
                        return;
                    case "9":
                        InCell_To_Leica.Leica009_WrapUp();
                        txBx_Update.Text += "\r\nExiting Program . . .";
                        Application.DoEvents();
                        Application.Exit();
                        return; //0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                    case "20":
                        //This lets you select the folder and adjust the settings -  TensorFlow on-the-fly
                        PrepForSetupMode();
                        lbl_CreateXDCE_Click(null, null);
                        return;
                    case "21":
                        //This looks for the images just saved, moves them to processed
                        LeicaOnTheFly.NextField();
                        break;
                    case "22":
                        while (true)
                        {
                            InCell_To_Leica.Leica004_NextFOV(); //Get the coordinates for the next field for the .ini file
                            Debug.Print(InCell_To_Leica.SR.CurrentFOV + " " + FIVE_IMG.InCell_To_Leica.Leica005_RefineFOV(NVP.RegParams.InitialErrorDivisor));
                            Application.DoEvents(); Application.DoEvents(); System.Threading.Thread.Sleep(500);//Wait a Sec so we can see any errors
                        }
                        break;
                    default:
                        txBx_Update.Text = MSG;
                        return;
                }
                FIVE_MSG.PipeClass.Server_ReadyClose();
                Application.DoEvents();
                System.Threading.Thread.Sleep(5);
                Application.DoEvents();
            }
        }

        private void PrepForSetupMode()
        {
            File.AppendAllText("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "in prep4setupmode" + "\r\n");
            Mode = FIVToolsMode.PlateID_WellList;
            Application.DoEvents();
            if (!NVP.RegParams.FullAuto)
            {
                this.WindowState = FormWindowState.Normal;
                this.BringToFront();
                this.TopMost = true;
                this.Focus();
                Application.DoEvents();
            }
        }

        private void Test_20x_5x()
        {
            //Where is the center of the Leica Image, when corrected to ge coordinates (in Plate um)


            // - - Setup the 20X Scan InCell Folder (ICF_20x)
            //var ICF_20x = new INCELL_Folder(@"R:\People\Manny Gerbi\20230809\20x_96W_3Ch_MG\20x_96W_3Ch_MG_20x_96well_2Color_MG_Leica0809_1");
            var ICF_20x = new INCELL_Folder(@"R:\\People\\Manny Gerbi\\20240208\\20x");
            //var ICF = new INCELL_Folder(@"R:\People\Josh Langmade\12 well scan CellVis Josh Fixed\12 well scan CellVis Josh Fixed_JOSH063_P1_20xTO5x_1_Recon5x\");

            // - - Setup the fake 5X Scan InCell Folder (ICF_5x)
            var Recon = new INCELL_Folder_Reconstruct(ICF_20x, null, 5, 0.4F, "", true); Recon.Reconstruct_Scan();
            var ICF_5x = Recon.ICFolder_New;

            // - - The next command sets everything up for us so the context is correct. It is most of what is inside > InCell_To_Leica.Leica001_WellStart(ICF, , "21", "", 0, 25, NVP.RegParams);
            InCell_To_Leica.SR = new SharedResource(ICF_20x, "A - 2", 2, 1, 0, 25, @"R:\People\Manny Gerbi\AIL_Base", NVP.RegParams, Environment.MachineName, false, ICF_5x, true);
            InCell_To_Leica.SR.TestingMode20x_5x_LiveRecon = true;
            InCell_To_Leica.Leica003b_RegisterWell();
            // - - Now go to the next well, pretend the image was taken, and Register
            InCell_To_Leica.Leica002_NextWell(); //Because test mode is on it won't do random fields
            InCell_To_Leica.Leica003b_RegisterWell(); //For Initial Testing //TODO:Willie working on improving this for every fov within

            //TODO: View the registered well as before - lower priority
            Post20x_5x_WellCheck();

            while (true)
            {
                InCell_To_Leica.Leica004_NextFOV();
                InCell_To_Leica.Leica005_RefineFOV(NVP.RegParams.InitialErrorDivisor);
            }
            //Leica000_CheckAgainstAll(@"C:\Temp\AILBase\FIV538_2\20210505_100224-C");
        }

        private void lbl_Test_01_Click(object sender, EventArgs e)
        {
            if (false)
            {
                string Fldr = @"R:\People\Josh Langmade\20220624_Testing\Overlays\";
                FIVE_IMG.ImageAlign_Return.ReadFromFolder(Fldr);
            }
            Mode = FIVToolsMode.PlateID_WellList;
        }

        private void lbl_Test_02_Click(object sender, EventArgs e)
        {
            if (true)
            {
                Test_20x_5x();
                return;
            }
            ListenForLeica();

            bool SomethingElse = false;
            if (SomethingElse)
            {
                Debugger.Break();
                FIVE_IMG.PL.PLDeployModule.AA_TestRun();


                Debugger.Break();
                //Powerpoint Grid
                //string[] Files =  Directory.GetFiles(@"R:\five\exp\FIV521\6 Images\FIV521A1_1\Renamed","*.BMP");
                string[] Files = Directory.GetFiles(@"R:\five\exp\FIV532\6 Images\ReName", "*.BMP");
                string[] names = Files.Select(x => Path.GetFileNameWithoutExtension(x)).Select(x => x.Substring(0, x.Length - 5)).ToArray();

                PPT_Wrap.MakePPTGrid("FIV532 Images", Files, names);
            }
        }

        private void Post20x_5x_WellCheck()
        {
            FormImageRegistration.RegParams = NVP.RegParams; //Get the latest parameters
            FormImageRegistration.ShowDialog();              //Show user registration results, let them pick additional parameters
            switch (FormImageRegistration.continueCondition)
            {
                case FIVE_Tools_Main.FormRegResult.Continue:
                    //Just keep going
                    break;
                case FIVE_Tools_Main.FormRegResult.Exit:
                    InCell_To_Leica.SR.CancelRequested = true; //This will write out a cancel flag in the "NextFOV"
                    break;
                case FIVE_Tools_Main.FormRegResult.Retry:
                    //Should go back to do another field in this well . . maybe we still take multiple field pictures, but we only try it on the first one
                    //TODO > make this better
                    break;
                default:
                    break;
            }
            txBx_Update.Text += "\r\n" + FormImageRegistration.ResultMessage;
        }

        private void lbl_CreateXDCE_Click(object sender, EventArgs e)
        {
            //Use the current folder to create and XDCE, for now use this . . 
            string FolderPath_or_Name; DialogResult DR;
            if (Mode != FIVToolsMode.PlateID_WellList)
            {
                var OFD = new OpenFileDialog();
                OFD.Filter = "All files (*.*)|*.*";
                OFD.Title = "Please select any file to indicate which Folder we should Open.";
                OFD.RestoreDirectory = true;
                DR = OFD.ShowDialog();
                FolderPath_or_Name = OFD.FileName;
                if (DR != DialogResult.OK) return;
            }
            else { FolderPath_or_Name = @"R:\FIVE\EXP\fiv567\Willie\"; } //@"C:\temp\OTFBase\Waiting"; }

            //restarting the whole thing seems to be better
            var CalCheck = new FormRaftCal(NVP); CalCheck.ParentMode = Mode;
            this.TopMost = false;
            CalCheck.chkBx_SquareMode.Checked = false;
            CalCheck.Folder = Path.GetDirectoryName(FolderPath_or_Name);
            DR = CalCheck.ShowDialog();
            if (DR == DialogResult.Abort && Mode == FIVToolsMode.PlateID_WellList)
            {
                //First - Setup the Well Start
                FIVE_IMG.LeicaOnTheFly.InitStuff(CalCheck.IC_Folder, NVP.CropSettings, NVP.MLSettings_PL, "Test01");
                FIVE_MSG.PipeClass.Server_ReadyClose();
                //this.TopMost = false;
                ListenForLeica();
            }
        }

    }
}