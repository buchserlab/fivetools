﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FIVE_Tools_Main
{
    public partial class NGSAlleleFragmentsForm : Form
    {
        public NGS_Alleles_Parameters Params;

        public NGSAlleleFragmentsForm()
        {
            InitializeComponent();
            Params = NGS_Alleles_Parameters.GetDefaults();
            LoadParams();
        }

        public void LoadParams()
        {
            txBx_Amplicon.Text = Params.WT_Amplicon;
            txBx_AnchorShift.Text = Params.Anchor_DistanceShift.ToString();
            txBx_AnchorSize.Text = Params.Anchor_PieceSize.ToString();
            txBx_MaxAmpSearchSize.Text = Params.AnchorSpan_MaxSize.ToString();
            txBx_AlleleSize.Text = Params.AlleleFrag_PieceSize.ToString();
            txBx_FractionCovered.Text = Params.Export_CummulativeFraction.ToString();
            txBx_FastQPath.Text = Params.PathSeqs;
            txBx_gRNA.Text = Params.gRNA;
            txBx_Name.Text = Params.Name;

            chkBx_RevComp.Checked = Params.RevComp;
        }

        public bool SaveParams()
        {
            try
            {
                Params.WT_Amplicon = txBx_Amplicon.Text;
                Params.Anchor_DistanceShift = int.Parse(txBx_AnchorShift.Text);
                Params.Anchor_PieceSize = int.Parse(txBx_AnchorSize.Text);
                Params.AnchorSpan_MaxSize = int.Parse(txBx_MaxAmpSearchSize.Text);
                Params.AlleleFrag_PieceSize = int.Parse(txBx_AlleleSize.Text);
                Params.Export_CummulativeFraction = double.Parse(txBx_FractionCovered.Text);
                Params.PathSeqs = txBx_FastQPath.Text;
                Params.gRNA = txBx_gRNA.Text;
                Params.Name = txBx_Name.Text;

                Params.RevComp = chkBx_RevComp.Checked;
                return true;
            }
            catch
            { return false; }
        }

        private void btn_FindAlleleFragments_Click(object sender, EventArgs e)
        {
            if (SaveParams())
            {
                bgWorker_Find.RunWorkerAsync(Params);
            }
            else
            {
                txBx_Update.Text = "Problem with one of the parameters (probably NaN).";
            }
        }

        private void bgWorker_Find_DoWork(object sender, DoWorkEventArgs e)
        {
            NGS_Alleles_Parameters Pa = (NGS_Alleles_Parameters)e.Argument;
            NGS_Alleles_Fragments Frags = new NGS_Alleles_Fragments(Pa, bgWorker_Find);
            e.Result = Frags;
        }

        private void bgWorker_Find_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void bgWorker_Find_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            NGS_Alleles_Fragments Frags = (NGS_Alleles_Fragments)e.Result;
            txBx_Update.Text = Frags.Pa.ExportPath_List + "\r\n\r\n" + Frags.Report_Percent;
        }
    }

    public class NGS_Alleles_SeqRecord
    {
        public static Dictionary<string, int> SourceName_Dict = new Dictionary<string, int>();
        public int GetSourceHash(string SourceName) { return SourceName_Dict[SourceName.ToUpper()]; }
        public Dictionary<string, int> SourceKVPs = new Dictionary<string, int>();
        public string Fragment = "";
        public string PrimaryPiece = "";

        private int _TotalCount = -1;
        public int TotalCount
        {
            get
            {
                if (_TotalCount < 0) Regenerate();
                return _TotalCount;
            }
        }

        private string _Names = "-1";
        public string Names
        {
            get
            {
                if (_Names == "-1") Regenerate();
                return _Names;
            }
        }

        public override string ToString()
        {
            return TotalCount.ToString();
        }

        private void Regenerate()
        {
            _Names = ""; _TotalCount = 0;
            foreach (KeyValuePair<string, int> KVP in SourceKVPs)
            {
                _Names += GetSourceHash(KVP.Key) + " ";
                _TotalCount += KVP.Value;
            }
        }

        public NGS_Alleles_SeqRecord(string name, string frag = "", int StartingCount = -1)
        {
            AddCheckSource(name);
            if (StartingCount > -1) SourceKVPs[name.ToUpper()] = StartingCount;
            Fragment = frag;
        }

        public NGS_Alleles_SeqRecord(string Piece, NGS_Alleles_SeqRecord ExistingRecord)
        {
            PrimaryPiece = Piece;
            Fragment = ExistingRecord.Fragment;
            AddSources(ExistingRecord);
        }

        internal void AddCheckSource(string name, int amountToAdd = 1)
        {
            name = name.ToUpper();
            if (!SourceKVPs.ContainsKey(name)) SourceKVPs.Add(name, 0);
            SourceKVPs[name] += amountToAdd;
            if (!SourceName_Dict.ContainsKey(name)) SourceName_Dict.Add(name, SourceName_Dict.Count);
        }

        internal void AddSources(NGS_Alleles_SeqRecord PreviousSources)
        {
            foreach (KeyValuePair<string, int> KVP in PreviousSources.SourceKVPs)
            {
                if (!SourceKVPs.ContainsKey(KVP.Key)) SourceKVPs.Add(KVP.Key, 0);
                SourceKVPs[KVP.Key] += KVP.Value;
            }
            _TotalCount = -1; _Names = "-1"; //Reset everytime it is added
        }
    }

    public class NGS_Alleles_Parameters
    {
        public string WT_Amplicon;
        public string Name;
        public string gRNA;
        public int AnchorSpan_MaxSize;
        public int Anchor_DistanceShift;
        public int Anchor_PieceSize;
        public int AlleleFrag_PieceSize;
        public string PathSeqs;

        public bool RevComp;
        public int SequenceID_Length;
        public double Export_CummulativeFraction;

        public string ExportPath_List { get => Path.Combine(Path.GetDirectoryName(PathSeqs), "SeqFrags_List_" + Name + ".txt"); }
        public string ExportPath_Matrix { get => Path.Combine(Path.GetDirectoryName(PathSeqs), "SeqFrags_Matrix_" + Name + ".txt"); }

        public NGS_Alleles_Parameters() 
        {
            RevComp = false;
        }

        public void CheckClean()
        {
            WT_Amplicon = WT_Amplicon.Trim().ToUpper();
            gRNA = gRNA.Trim().ToUpper();
            AnchorSpan_MaxSize = Math.Max(10, Math.Min(AnchorSpan_MaxSize, 1000));
            Anchor_DistanceShift = Math.Max(1, Math.Min(Anchor_DistanceShift, 1000));
            Anchor_PieceSize = Math.Max(3, Math.Min(Anchor_PieceSize, 1000));
            AlleleFrag_PieceSize = Math.Max(3, Math.Min(AlleleFrag_PieceSize, 1000));
            SequenceID_Length = Math.Max(1, Math.Min(SequenceID_Length, 50));
            Export_CummulativeFraction = Math.Max(0, Math.Min(Export_CummulativeFraction, 1));
        }

        public static NGS_Alleles_Parameters GetDefaults()
        {
            NGS_Alleles_Parameters pa = new NGS_Alleles_Parameters();
            bool Nup160 = true;
            if (Nup160)
            {
                //NUP160 --
                pa.Name = "NUP160";
                pa.WT_Amplicon = "CCTGTCATTTCTCAGAAGACGCGTGTGCAGGCTGACCACGTTCATTTTGCAGGGACTGCAAATGCCGTGGCTGGCGCCGTAAAATACAGTGAAAGCGCGGGAGGCTTTTACTACGTGGAGAGTGGCAAGTTGTTCTCCGTAACCAGAAACAGG";
                pa.gRNA = "TGGCTGGCGCCGTAAAATAC";
            }
            else
            {
                //NUP98 --
                pa.Name = "NUP98";
                pa.WT_Amplicon = "ACTGTCCCAACACCTTCCAGATACAGCAAAAATAATTTACTCCCGGCTGAGTGAAATAATATCTTAAAAAATTAATTACACCTAGCTTTGCTTAATCCCTTAATATATACAGGCAAAAAATTACATTCTGCACATTCAGTTTCTCTTTCTTTGATATGATATTTTACAGAAGTATCCTTAAATTTACCTCTTCCTTCTTCTTAGGGTCTGACATCGGATTCCGGAAGAGAGGAGAGTCTCCAAAAGGTGAGTATGTTAGACTATTGATGTGCTGCTGGAGAACAGCCTGCTGGGCAGCAGAAGCATTTGGATCT";
                pa.gRNA = "AAGAGAGGAGAGTCTCCAAA";
            }

            pa.PathSeqs = @"E:\Temp\NGS\NGS\G2 R2\";
            pa.AnchorSpan_MaxSize = 76;
            pa.Anchor_DistanceShift = 6;
            pa.Anchor_PieceSize = 17;
            pa.AlleleFrag_PieceSize = 20;
            pa.SequenceID_Length = 3;
            pa.Export_CummulativeFraction = 0.95;
            pa.RevComp = true;

            return pa;
        }
    }

    public class NGS_Alleles_Fragments
    {
        public NGS_Alleles_Parameters Pa;
        public List<HashSet<string>> AmpFragments;

        public Dictionary<string, NGS_Alleles_SeqRecord> Seqs_WT = new Dictionary<string, NGS_Alleles_SeqRecord>();
        public Dictionary<string, NGS_Alleles_SeqRecord> Seqs_MU = new Dictionary<string, NGS_Alleles_SeqRecord>();
        public Dictionary<string, int> Frag_WT = new Dictionary<string, int>();
        public Dictionary<string, NGS_Alleles_SeqRecord> Frag_MU = new Dictionary<string, NGS_Alleles_SeqRecord>();

        public int nSeq_WT = 0;
        public int nSeq_MU = 0;
        public int nSeq_Total = 0;

        public string Report_Reads { get => "WT\t" + nSeq_WT + "\r\nMU\t" + nSeq_MU + "\r\nTotal\t" + nSeq_Total; }
        public string Report_Percent { get => "%WT\t" + ((double)nSeq_WT / nSeq_Total).ToString("0.0%") + "\r\n%MU\t" + ((double)nSeq_MU / nSeq_Total).ToString("0.0%") + "\r\nRest\t" + ((double)(nSeq_Total - (nSeq_WT + nSeq_MU)) / nSeq_Total).ToString("00.0%"); }

        public NGS_Alleles_Fragments(NGS_Alleles_Parameters Params, BackgroundWorker BW = null)
        {
            Pa = Params; Pa.CheckClean();

            AmpFragments = BuildAmpFragments(Pa.WT_Amplicon, Pa.gRNA, Pa.AnchorSpan_MaxSize, Pa.Anchor_DistanceShift, Pa.Anchor_PieceSize);
            LoadSequences(Pa.PathSeqs, BW);
            DefineMutantFragments();
        }

        private void DefineMutantFragments()
        {
            //1st Examine Each of the Mutant Sequences, specifically, the small Fragment that we cut out. Chop the fragment up into 
            //pieces that are the size we want to use at the end (usually 20mers).  Assign those fragments either to WT or MU Dictionaries, and keep track
            //of where they came from
            string piece; string frag; Random RND = new Random();
            foreach (KeyValuePair<string, NGS_Alleles_SeqRecord> fragPre in Seqs_MU)
            {
                frag = fragPre.Value.Fragment;
                //if (frag.Length < Pa.AlleleFrag_PieceSize) //Shouldn't be possible since we add to the sequence fragment before we get here 3/3/2020

                for (int i = 0; i <= frag.Length - Pa.AlleleFrag_PieceSize; i++)
                {
                    piece = frag.Substring(i, Pa.AlleleFrag_PieceSize); //Make the 20mer
                    if (Frag_WT.ContainsKey(piece)) { Frag_WT[piece] += fragPre.Value.TotalCount; continue; } //If we have already seen it, add it to this bin
                    if (Frag_MU.ContainsKey(piece)) { Frag_MU[piece].AddSources(fragPre.Value); continue; } //If we have already seen it, add it to this bin
                    if (Pa.WT_Amplicon.Contains(piece)) { Frag_WT.Add(piece, fragPre.Value.TotalCount); continue; } //If it is WT, then make a new WT bin
                    Frag_MU.Add(piece, new NGS_Alleles_SeqRecord(piece, fragPre.Value)); //If we haven't seen it, it is a new mutant allele fragment
                }
            }

            //Sort the fragments by frequency
            SortedList<double, NGS_Alleles_SeqRecord> Sorted = new SortedList<double, NGS_Alleles_SeqRecord>();
            double ValKey; double sum = 0;
            foreach (KeyValuePair<string, NGS_Alleles_SeqRecord> KVP in Frag_MU)
            {
                if (KVP.Value.TotalCount <= 1) continue;
                sum += KVP.Value.TotalCount;
                ValKey = -KVP.Value.TotalCount;
                while (Sorted.ContainsKey(ValKey)) ValKey -= RND.NextDouble() / 1000;
                Sorted.Add(ValKey, KVP.Value);
            }

            //Now export the sequence list which can be used directly by LibraryAligner
            Export_SeqFragList(Sorted, sum);

            //Now export a Matrix, so we can potentially run PCA / clustering on the fragments and see which ones are orthogonal
            Export_SeqMatrix(Sorted, sum);
        }

        private void Export_SeqMatrix(SortedList<double, NGS_Alleles_SeqRecord> Sorted, double divisor)
        {
            //First setup the columns
            //Dictionary<string, int> Cols = new Dictionary<string, int>();

            double cummulative_fraction = 0; int nIndexes = NGS_Alleles_SeqRecord.SourceName_Dict.Count;
            StringBuilder ToExport = new StringBuilder();
            List<double> Cols = new List<double>();
            for (int i = 0; i < nIndexes; i++) Cols.Add(Cols.Count);
            ToExport.Append("Idxs" + "\t" + "Reads" + "\t" + "Seq" + "\tidx" + String.Join("\tidx", Cols) + "\r\n");
            foreach (NGS_Alleles_SeqRecord SeqRec in Sorted.Values)
            {
                cummulative_fraction += (SeqRec.TotalCount / divisor);
                if (cummulative_fraction >= Pa.Export_CummulativeFraction) break;
                Cols = Enumerable.Repeat((double)0, nIndexes).ToList();
                foreach (KeyValuePair<string, int> kvp in SeqRec.SourceKVPs)
                {
                    Cols[SeqRec.GetSourceHash(kvp.Key)] = (double)kvp.Value / SeqRec.TotalCount;
                }
                ToExport.Append(SeqRec.SourceKVPs.Count + "\t" + SeqRec.TotalCount + "\t" + SeqRec.PrimaryPiece + "\t" + String.Join("\t", Cols) + "\r\n");
            }
            File.WriteAllText(Pa.ExportPath_Matrix, ToExport.ToString());
        }

        private void Export_SeqFragList(SortedList<double, NGS_Alleles_SeqRecord> Sorted, double divisor)
        {
            string NameUse = Pa.Name; double cummulative_fraction = 0;
            string SeqID; _SeqIDs = new HashSet<string>();

            StringBuilder ToExport = new StringBuilder();
            ToExport.Append(NameUse + "_WT" + "\t" + Pa.gRNA + "\t0\r\n");
            foreach (NGS_Alleles_SeqRecord SeqRec in Sorted.Values)
            {
                cummulative_fraction += (SeqRec.TotalCount / divisor); //This divisor isn't correct, we need to do this per read, not per fragment, but that is a slightly different architecture
                SeqID = CreateID(SeqRec.PrimaryPiece); //This makes a unique name that is consistent for that sequence
                ToExport.Append(NameUse + "_" + SeqID + "\t" + SeqRec.PrimaryPiece + "\t" + cummulative_fraction + "\r\n");
                if (cummulative_fraction >= Pa.Export_CummulativeFraction) break;
            }
            File.WriteAllText(Pa.ExportPath_List, ToExport.ToString());
        }

        private HashSet<string> _SeqIDs;
        private string CreateID(string Sequence)
        {
            string tID = HashSeq(Sequence, Pa.SequenceID_Length);
            if (_SeqIDs.Contains(tID))
            {
                tID = HashSeq(Sequence, Pa.SequenceID_Length + 1);
                if (_SeqIDs.Contains(tID))
                {
                    int append = 1; string tIDP = tID;
                    while (_SeqIDs.Contains(tID)) tID = tIDP + append++;
                }
            }
            _SeqIDs.Add(tID);
            return tID;
        }

        public static string HashSeq(string Sequence, int Length)
        {
            string sh = Convert.ToBase64String(BitConverter.GetBytes(Sequence.GetHashCode()));
            //return sh.Substring(sh.Length - Length);
            return sh.Substring(0, Length);
        }

        private void CheckAddSeq(string Seq, string name)
        {
            nSeq_Total++;
            if (Seqs_WT.ContainsKey(Seq)) { Seqs_WT[Seq].AddCheckSource(name); nSeq_WT++; return; }
            if (Seqs_MU.ContainsKey(Seq)) { Seqs_MU[Seq].AddCheckSource(name); nSeq_MU++; return; }
            if (Seq.Contains(Pa.gRNA)) { Seqs_WT.Add(Seq, new NGS_Alleles_SeqRecord(name)); nSeq_WT++; return; }
            string newFrag = CheckSequenceOnFragments(Seq);
            if (newFrag == "") return;
            nSeq_MU++;
            Seqs_MU.Add(Seq, new NGS_Alleles_SeqRecord(name, newFrag));
        }

        private string CheckSequenceOnFragments(string Seq)
        {
            //Just needs to match two of the fragments (one from each side) to keep
            int pair = 0; List<int> idx = new List<int>(2); int l;
            for (int i = 0; i < AmpFragments.Count; i++)
            {
                foreach (string frag in AmpFragments[i])
                {
                    l = Seq.IndexOf(frag);
                    if (l >= 0)
                    {
                        idx.Add(i == 0 ? l + (2 * frag.Length / 3) : l + (1 * frag.Length / 3));
                        pair++; break;
                    }
                }
                if (pair < 1) return ""; //no reason to check the other side if this failed
            }
            if (pair < 2) return "";
            int len = Math.Max(idx[1] - idx[0], Pa.AlleleFrag_PieceSize);
            return Seq.Substring(idx[0], len);
        }

        private void LoadSequences(string pathSeqs, BackgroundWorker BW = null)
        {
            //As sequences are loaded, check that they are from the right amplicon, and whether they have the gRNA
            DirectoryInfo DI = new DirectoryInfo(pathSeqs);
            FileInfo[] Files = DI.GetFiles();
            int i = 0;
            foreach (FileInfo fi in Files)
            {
                if (BW != null && ++i % 5 == 0) BW.ReportProgress(0, i + "/" + Files.Length + " " + fi.Name);
                if (Path.GetExtension(fi.FullName).ToUpper() == ".FASTQ") LoadSequencesFile(fi);
            }
        }

        private void LoadSequencesFile(FileInfo fi)
        {
            string t;
            StreamReader SR = new StreamReader(fi.FullName);
            while (!SR.EndOfStream)
            {
                t = SR.ReadLine();
                if (t.StartsWith("@"))
                {
                    t = SR.ReadLine();
                    if (Pa.RevComp) t = SeqHelper.ReverseComplement(t);
                    CheckAddSeq(t, fi.Name);
                }
            }
            SR.Close();
        }

        public static List<HashSet<string>> BuildAmpFragments(string amplicon, string gRNA, int maxGoodSize, int ampFrag_DistanceShift, int Frag_Size)
        {
            int l_gRNA = amplicon.IndexOf(gRNA);
            int l_m1 = Math.Max(0, l_gRNA - (maxGoodSize - gRNA.Length) / 2);
            int l_m2 = l_gRNA + 20 + (maxGoodSize - gRNA.Length) / 2;
            string maxAmp = amplicon.Substring(l_m1, maxGoodSize);

            //Now make the fragments
            l_gRNA = maxAmp.IndexOf(gRNA);
            HashSet<string> FragsLeft =
                BuildAmpFragsSub(maxAmp, l_gRNA - gRNA.Length / 2, -ampFrag_DistanceShift, Frag_Size);
            HashSet<string> FragsRigh =
                BuildAmpFragsSub(maxAmp, l_gRNA + gRNA.Length / 2, +ampFrag_DistanceShift, Frag_Size);
            return new List<HashSet<string>>(2) { FragsLeft, FragsRigh };
        }

        private static HashSet<string> BuildAmpFragsSub(string maxAmp, int Start, int Shift, int Size)
        {
            HashSet<string> t = new HashSet<string>();
            int p = Start;
            while (true)
            {
                if (p < 0 || p + Size > maxAmp.Length) break;
                t.Add(maxAmp.Substring(p, Size));
                p += Shift;
            }
            return t;
        }

    }

    public static class SeqHelper
    {
        public static Dictionary<char, char> HashComplement = new Dictionary<char, char>(4) { { 'C', 'G' }, { 'G', 'C' }, { 'A', 'T' }, { 'T', 'A' } };

        public static string ReverseComplement(string Sequence)
        {
            StringBuilder sb = new StringBuilder();
            char c;
            char[] Seq = Sequence.ToUpper().Trim().ToCharArray();
            for (int i = Seq.Length - 1; i >= 0; i--)
            {
                c = Seq[i];
                if (HashComplement.ContainsKey(c))
                    sb.Append(HashComplement[c]);
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }
    }
}
