﻿using Library_Aligner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using Microsoft.Office.Core;
using FIVE_Tools_Main.Forms;

namespace FIVE_Tools_Main
{
    public partial class NGS_LibraryAligner_Form : Form
    {
        public Store Store;
        public OpenFileDialog OFD = new OpenFileDialog();

        public NGS_LibraryAligner_Form()
        {
            InitializeComponent();
            Store = Store.Load(); //Loads the previous version
            RefreshViewer_Post();

            string GenotypingPrefix = @"R:\FIVE\Genotyping\";
            txBx_DestFolder.Text = GenotypingPrefix + DateTime.Now.ToString("yyyyMMdd");
        }

        private void RefreshViewer_Pre()
        {
            bool SpecModeCutting = Store._SpecModeCutting;
            Store.UpdateFrom((IEnumerable<KVP_PI>)dataGridViewM.DataSource);
            Store._SpecModeCutting = SpecModeCutting;
            Store.Save(); //To the default Region
        }

        private void RefreshViewer_Post()
        {
            List<KVP_PI> TList = Store.GetList().ToList();
            dataGridViewM.DataSource = TList;
        }

        private void btn_SaveParams_Click(object sender, EventArgs e)
        {
            RefreshViewer_Pre();
            Store.Save(System.IO.Path.Combine(Store.PathFastQToSearch, "LA_Settings.xml"));
            txBx_Update.Text += "Settings saved: 'LA_Settings.xml'.\r\n";
        }

        private void NGS_LibraryAligner_Form_Load(object sender, EventArgs e)
        {

        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            //https://stackoverflow.com/questions/35627212/how-to-display-the-progress-of-the-operation-at-the-taskbar-icon-in-c-sharp-wind
            if (sender != null) Store._SpecModeCutting = false; //If we don't do this it thinks everyone is cutting
            CheckRep_Initial = Store.PathFastQToSearch;
            RefreshViewer_Pre(); //Saves things
            bgWrk_LA.RunWorkerAsync(Store);
        }

        private void btn_Cutting_Click(object sender, EventArgs e)
        {
            Store._SpecModeCutting = true;
            btn_Start_Click(null, e);
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            bgWrk_LA.CancelAsync();
        }

        private void bgWrk_LA_DoWork(object sender, DoWorkEventArgs e)
        {
            Store sUse = (Store)e.Argument;
            Library_Aligner.Library_Aligner.Start_Library_Aligner(sUse, bgWrk_LA, false);

            if (sUse._SpecModeCutting && sUse._SpecModeCuttingText!="")
            {
                bgWrk_LA.ReportProgress(0, "Making cutting figures . . ");
                var stringSavePath = Path.Combine(sUse.PathFastQToSearch, "Cutting_Plot.jpg");
                LA_Cutting_Graphics.CuttingPlot(Store._SpecModeCuttingData, stringSavePath);

                var startInfo = new ProcessStartInfo { Arguments = stringSavePath, FileName = "explorer.exe" };
                Process.Start(startInfo);
            }
        }

        private void bgWrk_LA_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (txBx_Update.Text.Length > 20000) txBx_Update.Text.Substring(1, 10000);
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
        }

        private void bgWrk_LA_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = "FINISHED\r\n\r\n" + txBx_Update.Text;
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public string LoadRef_Initial = @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\Sequences\gRNA Pools";
        public string LoadFastQ_Initial = @"\\storage1.ris.wustl.edu\wbuchser\Active\FIVE\Genotyping\";
        public string CheckRep_Initial = @"\\storage1.ris.wustl.edu\wbuchser\Active\dB\Sequences\gRNA Pools\zRefLongs";

        private void btn_LoadFromFile_Click(object sender, EventArgs e)
        {
            OFD.Filter = "xml settings (*.xml)|*.xml|All files (*.*)|*.*";
            DialogResult DR = OFD.ShowDialog();
            if (DR == DialogResult.Cancel) return;

            Store = Store.Load(OFD.FileName);
            RefreshViewer_Post();
            txBx_Update.Text += "Loaded Settings.\r\n";
        }

        private void btn_LoadDefaults_Click(object sender, EventArgs e)
        {
            Store = Store.GetDefaults();
            RefreshViewer_Post();
        }

        /// <summary>
        /// This button is designed to download from a particular website format then unzip the files for work with the Library Aligner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Download_UnZip_Click(object sender, EventArgs e)
        {
            //Should automatically update the FASTQ folder based on this download
            LoadFastQ_Initial = txBx_DestFolder.Text;
            txBx_Update.Text = "Opening Link . . ";

            List<string> Args = new List<string>(2) { txBx_DownloadLink.Text, txBx_DestFolder.Text };
            bgWrk_Download.RunWorkerAsync(Args);
        }

        public string Download_Unzip(string URL, string DestFolder, BackgroundWorker BW = null)
        {
            using (var WC = new System.Net.WebClient())
            {
                string WebText = "";
                try
                {
                    WebText = WC.DownloadString(URL);
                }
                catch
                {
                    if (BW != null) BW.ReportProgress(0, "URL Not Found, check the link.\r\n");
                    return "";
                }
                //Get the links from the website
                string Pattern = @"href=\""(.*R._001.fastq.gz)\""";
                Regex R = new Regex(Pattern);
                MatchCollection MC = R.Matches(WebText);
                List<string> tList = MC.Cast<System.Text.RegularExpressions.Match>().Select(x => x.Groups[1].Value).ToList();
                if (tList.Count > 0)
                {
                    if (BW != null) BW.ReportProgress(0, "Found " + tList.Count + " Files to Download.\r\n");
                }
                else
                {
                    if (BW != null) BW.ReportProgress(0, "Found no files with the format " + Pattern + "\r\n");
                    return "";
                }

                //Setup the destination Folder
                var DI = new DirectoryInfo(DestFolder);
                if (!DI.Exists) DI.Create();
                var DI2 = DI.CreateSubdirectory("FastQ_gz");
                var DI3 = DI.CreateSubdirectory("FastQ");

                //Actually start downloading
                int counter = 0; if (!URL.EndsWith("/")) URL += "/";
                string downloadLoc;
                if (BW != null) BW.ReportProgress(0, "\r\nDownloading . . \r\n");

                foreach (string FileURL in tList)
                {
                    string fURL = FileURL.Replace("./","");
                    downloadLoc = Path.Combine(DI2.FullName, fURL);
                    WC.DownloadFile(URL + fURL, downloadLoc);
                    if (counter++ % 5 == 0) if (BW != null) BW.ReportProgress(0, counter + "  ");
                }
                System.Threading.Thread.Sleep(500);

                //Now UnZip - - - - - - - - - - - - 
                if (BW != null) BW.ReportProgress(0, "/r/nUnZipping . . ./r/n");
                try
                {
                    string srcGz = DI2.FullName + (DI2.FullName.EndsWith("\\") ? "" : "\\");
                    string cmdText = "e " + srcGz + "*.gz -o" + DI3.FullName + " -y";
                    System.Diagnostics.Process.Start(@"C:\Program Files\7-Zip\7z.exe", cmdText);
                }
                catch
                {
                    if (BW != null) BW.ReportProgress(0, "/r/nProblem with UnZipping.  Make sure 7-zip is installed./r/n");
                }

                //12/2020 This also didn't work, didn't fully decompress the files, just peaked into them a little
                //foreach (string toUZ in UnZipList) Decompress(new FileInfo(toUZ), DI3);
                //ZipFile.ExtractToDirectory(downloadLoc, DI3.FullName); //Doesn't work for GZip
                return DI3.FullName;
            }
        }

        public static void Decompress(FileInfo fileToDecompress, DirectoryInfo folderDest)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName =
                    Path.Combine(folderDest.FullName, fileToDecompress.Name.Remove(fileToDecompress.Name.Length - fileToDecompress.Extension.Length));

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }

        private void bgWrk_Download_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> Args = (List<string>)e.Argument;
            e.Result = Download_Unzip(Args[0], Args[1], bgWrk_Download);
        }

        private void bgWrk_Download_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + txBx_Update.Text;
        }

        private void bgWrk_Download_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //If it is succesful, report it out, and change the value of the FastQ folder. . ?
            if (e.Result.ToString() == "") return;
            txBx_Update.Text = "Finished Downloading and currently UnZipping (to two separate folders). Delete the FastQ version to save space once LA is finished." +
                "\r\n\r\n" + e.Result.ToString();

        }

        private void txBx_DestFolder_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_LoadRef_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.Filter = "Text files (*.txt)|*.txt";
            OFD.Title = "Please select a reference file. 1st Column sould be gRNA ID '1_BRCA1..' where 1 is subpool and BRCA1 is gene. They must be unique. Column 2 is gRNA sequence.";
            OFD.InitialDirectory = LoadRef_Initial;
            var DR = OFD.ShowDialog();
            if (DR != DialogResult.OK) return;
            Store.PathReferenceList = Path.GetDirectoryName(OFD.FileName);
            LoadRef_Initial = Store.PathReferenceList;
            RefreshViewer_Post();
        }

        private void btn_LoadFASTQ_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.Filter = "FastQ sequence files (*.fa)|*.fa*";
            OFD.Title = "Please select on FastQ file. Library Aligner (LA) will run on all the FastQ files in the folder.";
            OFD.InitialDirectory = LoadFastQ_Initial;
            var DR = OFD.ShowDialog();
            if (DR != DialogResult.OK) return;
            Store.PathFastQToSearch = Path.GetDirectoryName(OFD.FileName);
            LoadFastQ_Initial = Store.PathFastQToSearch;
            RefreshViewer_Post();
        }

        private void btn_CheckRep_Click(object sender, EventArgs e)
        {
            //Save Check Rep Settings
            var SCR = CheckRepSettingsUpdate();
            if (SCR.HasError) { txBx_Update.Text = "Check entries, there may be a typo"; return; }

            //Will automatically make the contamination check and the reference check plots
            var OFD = new OpenFileDialog();
            OFD.Filter = "RefLong result file (*.txt)|*.txt";
            OFD.Title = "Please select a RefLong result file. This will then generate 2 .png files and save them with the same name in the same folder as output.";
            OFD.InitialDirectory = CheckRep_Initial;
            var DR = OFD.ShowDialog();
            if (DR != DialogResult.OK) return;

            txBx_Update.Text += "\r\n" + "Loading Data . ."; Application.DoEvents();

            //Load the RefLong Data
            var rlTable = RefLongTable.LoadRefLong(OFD.FileName, MergePools: SCR.MergePools);
            string Folder = Path.GetDirectoryName(OFD.FileName); CheckRep_Initial = Folder;
            string NameFront = Path.GetFileNameWithoutExtension(OFD.FileName);

            txBx_Update.Text += "\r\n" + "Contamination Plot . ."; Application.DoEvents();

            //Make the contamination plot
            LA_QC_Graphics.QCPlot_Contamination(rlTable, Path.Combine(Folder, NameFront + "_Contamination.png"));

            txBx_Update.Text += "\r\n" + "Representation Plots . ."; Application.DoEvents();

            //Make the representation plot(s)
            LA_QC_Graphics.QCPlot_Representation(rlTable, Path.Combine(Folder, NameFront + "_Represent"), SCR);

            var startInfo = new ProcessStartInfo { Arguments = Folder, FileName = "explorer.exe" };
            Process.Start(startInfo);

            txBx_Update.Text += "\r\n" + "Done!"; Application.DoEvents();
        }

        public Settings_CheckRep CheckRepSettingsUpdate()
        {
            var SCR = new Settings_CheckRep() { HasError = true };
            try
            {
                SCR.MergePools = chkBx_MergePools.Checked;
                SCR.CellFactor = float.Parse(txBx_CellFactor.Text);
                SCR.MOI = float.Parse(txBx_MOI.Text);
                SCR.Titer_IU_ML = float.Parse(txBx_TiterIUoML.Text);
                SCR.CellCoverage = 1000;
                SCR.HasError = false;
            }
            catch { }
            return SCR;
        }
    }

    public class Settings_CheckRep
    {
        public bool MergePools = false;
        public float MOI { get; set; }
        public double FDC => Math.Exp(-MOI);  //Fraction Dead Cells after Selection = e - MOI(if MOI = 0.1, e - 0.1 = 0.904, if MOI = 1, e - 1 = 0.367)
        public float Titer_IU_ML { get; set; }
        public float CellFactor { get; set; }
        public float CellCoverage { get; set; }
        public bool HasError = false;

        internal string rF_Desc(double rF)
        {
            if (rF < 2) return "Amazing";
            if (rF < 3) return "Great";
            if (rF < 4) return "Good";
            if (rF < 5) return "OK";
            if (rF < 6) return "Poor";
            return "Bad";
        }
    }

    public static class LA_QC_Graphics
    {
        public static List<Brush> tBrushToChoose = new List<Brush>() { Brushes.Blue, Brushes.Red, Brushes.Yellow, Brushes.Green, Brushes.Black, Brushes.Purple, Brushes.Brown, Brushes.Pink, Brushes.DarkCyan, Brushes.LightCoral, Brushes.LightGoldenrodYellow, Brushes.DarkKhaki, Brushes.Brown, Brushes.DeepPink, Brushes.DarkSlateGray, Brushes.DarkSeaGreen, Brushes.Salmon, Brushes.RosyBrown, Brushes.LightGray };
        public static Dictionary<string, Brush> Subpool_Brush = new Dictionary<string, Brush>();
        public static double ExcludeIndex_LessThanThisFraction = 0.05;
        private static Random _Rand = new Random(119);

        internal static void QCPlot_Contamination(RefLongTable rlTable, string ExportFileName)
        {
            QCPlot_Contamination(rlTable, ExportFileName, Size.Empty);
        }

        internal static void QCPlot_Contamination(RefLongTable rlTable, string ExportFileName, Size FinalSize)
        {
            //Create the set of indices to examine
            Dictionary<string, Dictionary<string, double>> PrepForDrawing = new Dictionary<string, Dictionary<string, double>>();
            foreach (var KVP in rlTable.Indexes_MappedReads)
            {
                if (KVP.Value < (rlTable.MaxReads_InIndex * ExcludeIndex_LessThanThisFraction)) continue;
                PrepForDrawing.Add(KVP.Key, new Dictionary<string, double>());
            }
            //See which subpools go in each index
            foreach (var Line in rlTable.List)
            {
                if (!rlTable.Subpools_rNames.ContainsKey(Line.rSubpool)) continue;
                if (!PrepForDrawing.ContainsKey(Line.Index)) continue;
                if (!PrepForDrawing[Line.Index].ContainsKey(Line.rSubpool)) PrepForDrawing[Line.Index].Add(Line.rSubpool, 0);
                PrepForDrawing[Line.Index][Line.rSubpool] += Line.Reads;
            }

            if (FinalSize == Size.Empty) FinalSize = new Size(600, 400);
            int NextBrush = 0; Pen tPen = new Pen(Brushes.Black, 2);
            Brush tBrush;
            Bitmap BM = new Bitmap(FinalSize.Width, FinalSize.Height);
            int Column = -1; float LastAmount; float WidthMult = (float)FinalSize.Width / (PrepForDrawing.Count + 1);
            using (Graphics g = Graphics.FromImage(BM))
            {
                var Fullrect = new Rectangle(0, 0, BM.Width, BM.Height);
                g.FillRectangle(Brushes.White, Fullrect);
                foreach (var Index in PrepForDrawing)
                {
                    LastAmount = 0; Column++;
                    foreach (var SubPool in Index.Value)
                    {
                        if (!Subpool_Brush.ContainsKey(SubPool.Key))
                            Subpool_Brush.Add(SubPool.Key, BrushToChoose(NextBrush++));
                        tBrush = Subpool_Brush[SubPool.Key];
                        float d = (float)(SubPool.Value / rlTable.Indexes_TotalReads[Index.Key]);
                        var rect = new RectangleF(Column * WidthMult, /*FinalSize.Height -*/ (LastAmount * FinalSize.Height), WidthMult * 0.95F, d * FinalSize.Height);
                        LastAmount += d;
                        g.FillRectangle(tBrush, rect);
                    }
                }
                g.DrawRectangle(tPen, Fullrect);
            }
            BM.RotateFlip(RotateFlipType.RotateNoneFlipY);
            using (Graphics g = Graphics.FromImage(BM))
            {
                LastAmount = 0; Column++; float tLeft = (WidthMult * Column) + (WidthMult * 0.1F); WidthMult *= 0.7F;
                WidthMult /= 2;
                Font tFont = new Font("Arial", 15, FontStyle.Bold);
                foreach (var SpBr in Subpool_Brush)
                {
                    var rect = new RectangleF(tLeft, LastAmount, WidthMult, WidthMult);
                    g.FillRectangle(SpBr.Value, rect);
                    g.DrawString(SpBr.Key, tFont, Brushes.White, tLeft + 5, LastAmount + 10);
                    LastAmount += WidthMult * 1.1F;
                }
            }

            BM.Save(ExportFileName);
        }

        private static Brush BrushToChoose(int BrushIndex)
        {
            int i = BrushIndex >= tBrushToChoose.Count ? BrushIndex % tBrushToChoose.Count : BrushIndex;
            return tBrushToChoose[i];
        }

        internal static void QCPlot_Representation(RefLongTable rlTable, string ExportFileName, Settings_CheckRep Settings)
        {
            QCPlot_Representation(rlTable, ExportFileName, Size.Empty, Settings);
        }

        internal static void QCPlot_Representation(RefLongTable rlTable, string ExportFileNameNoExt, Size FinalSize, Settings_CheckRep Settings)
        {
            //Create the set of indices to examine
            var PD = new Dictionary<string, Dictionary<string, SortedDictionary<double, string>>>();
            foreach (var KVP in rlTable.Indexes_MappedReads)
            {
                if (KVP.Value < (rlTable.MaxReads_InIndex * ExcludeIndex_LessThanThisFraction)) continue;
                PD.Add(KVP.Key, new Dictionary<string, SortedDictionary<double, string>>());
            }
            //See which rName go in each index
            foreach (var Line in rlTable.List)
            {
                if (!PD.ContainsKey(Line.Index)) continue;
                if (!rlTable.Subpools_rNames.ContainsKey(Line.rSubpool)) continue;
                if (!PD[Line.Index].ContainsKey(Line.rSubpool)) PD[Line.Index].Add(Line.rSubpool, new SortedDictionary<double, string>());
                var Spot = PD[Line.Index][Line.rSubpool];
                double key = Line.LogNormReads;
                while (Spot.ContainsKey(key)) key += (1E-10 * _Rand.NextDouble());
                Spot.Add(key, Line.rName);
            }
            //Now fill up with the gRNAs that weren't present . .  not necessary since it is accounted for down below . . 


            //OK, now draw all of these (seperate for each index)
            string FirstImagePath = "";
            foreach (var Index in PD)
            {
                string t = SaveRepForIndex(rlTable, Index.Key, Index.Value, ExportFileNameNoExt + "", FinalSize, Settings); //All subpools considered
                if (FirstImagePath == "") FirstImagePath = t;
                //SaveRepForIndex(rlTable, Index.Key, Index.Value, ExportFileNameNoExt + "_C", FinalSize, true); //considered to be 1 large subpool
            }

            //Open the first one of these
            //var startInfo = new System.Diagnostics.ProcessStartInfo("mspaint.exe " +FirstImagePath);
            //startInfo.Verb = "Edit";
            //System.Diagnostics.Process.Start(startInfo);
        }

        private static string SaveRepForIndex(RefLongTable rlTable, string IndexName, Dictionary<string, SortedDictionary<double, string>> SubPoolDict, string exportFileNameNoExt, Size FinalSize, Settings_CheckRep Settings)
        {
            //Remember to add in the empty space for rNames that aren't on the list (but are in this subpool)
            if (FinalSize == Size.Empty) FinalSize = new Size(640, 640);
            float StartX = FinalSize.Width / 18F; float StartY = StartX * 0.2F;
            float SizeX = FinalSize.Width - (StartX * 1.2F); float SizeY = FinalSize.Height - ((StartX * 1.2F) + 10);
            Pen tPen; Brush tBrush; float x, y;
            Pen gPen = new Pen(Brushes.LightGray, 1);
            Bitmap BM = new Bitmap(FinalSize.Width, FinalSize.Height);
            int yGridNum = 10; int xGridNum = 8;

            float LegendLastAmount = 3; float LegendWidthMult = SizeX / 13; float LegendLeft = FinalSize.Width - LegendWidthMult;
            Font FontLegend = new Font("Arial", SizeX / 30, FontStyle.Bold); Font FontGrid = new Font("Arial", SizeX / 40, FontStyle.Regular);

            using (Graphics g = Graphics.FromImage(BM))
            {
                //Draw White and Background Grid
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.FillRectangle(Brushes.White, new Rectangle(new Point(0, 0), BM.Size));
                for (int i = 0; i <= yGridNum; i++) { y = StartY + (SizeY * i / yGridNum); g.DrawLine(gPen, StartX, y, StartX + SizeX, y); g.DrawString(((float)i / yGridNum).ToString("0.0"), FontGrid, Brushes.Gray, 1, y - (SizeX / 60)); }

                //Go through each Subpool, but ignore subpools that really aren't in this index
                var sortedSubpools = new SortedDictionary<float, KeyValuePair<string, SortedDictionary<double, string>>>();
                foreach (var SP in SubPoolDict)
                {
                    float Key = -(float)SP.Value.Count / rlTable.Subpools_rNames[SP.Key].Count; //Fraction of indices picked up (not the perfect way . . we have to test)
                    while (sortedSubpools.ContainsKey(Key)) Key -= 0.000001F; //avoids collisions
                    sortedSubpools.Add(Key, SP);
                }
                //Now draw the curves for each subpool (of the strongest subpools in this index)
                bool First = true;
                foreach (var sSP in sortedSubpools)
                {
                    if (!First) if (sSP.Key > -0.05) continue; //Skip it if it has less than 5% max unless it is the only one (always show at least 1)
                    var SP = sSP.Value;
                    tBrush = Subpool_Brush[SP.Key]; tPen = new Pen(tBrush, First ? (SizeX / 100) : 2); tPen.StartCap = tPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                    float Max = (float)SP.Value.Max(r => r.Key);
                    float Min = (float)SP.Value.Min(r => r.Key);
                    int rNames = rlTable.Subpools_rNames[SP.Key].Count;
                    int idx = rNames - SP.Value.Count; //There might be empties, so this makes up for those
                    if (First) //Has to be done here so we can get the real values for the first one
                    {
                        for (int i = 0; i <= xGridNum; i++)
                        {
                            x = StartX + (SizeX * i / xGridNum); g.DrawLine(gPen, x, StartY, x, StartY + SizeY);
                            float xGridVal = Min + ((Max - Min) * i / xGridNum);
                            g.DrawString(xGridVal.ToString("0.00"), FontGrid, Brushes.Gray, x - (SizeX / 30), SizeY + StartY + 4);
                        }
                        g.DrawString("Log2 Norm Reads", FontGrid, Brushes.Gray, SizeX * 0.38F, SizeY + StartY + 4 + (SizeX / 36));
                        g.DrawString(GetStatsForIndex(IndexName, SP, rNames, idx, rlTable.Indexes_TotalReads[IndexName], Settings), FontGrid, Brushes.Black, StartX + SizeX * 0.51F, 10F);
                    }

                    //Draw the curve
                    float lastx = StartX + (SizeX * 0); float lasty = StartY + (SizeY * idx / rNames);
                    foreach (var KVP in SP.Value)
                    {
                        x = StartX + (SizeX * (float)KVP.Key / Max);
                        y = StartY + (SizeY * ++idx / rNames);
                        g.DrawLine(tPen, lastx, lasty, x, y);
                        lastx = x; lasty = y;
                    }

                    //Draw the Legend
                    var rect = new RectangleF(LegendLeft, LegendLastAmount, LegendWidthMult, LegendWidthMult);
                    g.FillRectangle(tBrush, rect);
                    g.DrawString(SP.Key, FontLegend, Brushes.White, LegendLeft + 5, LegendLastAmount + 10);
                    LegendLastAmount += LegendWidthMult * 1.1F;
                    First = false;
                }
            }
            string SaveName = exportFileNameNoExt + "_" + IndexName + ".png";
            BM.Save(SaveName);
            return SaveName;
        }

        private static string GetStatsForIndex(string IndexName, KeyValuePair<string, SortedDictionary<double, string>> SP, int rNames, int StartingIndex, double TotalReads, Settings_CheckRep Settings)
        {
            double GetReads(double MedLogNormRds) { return TotalReads * (Math.Pow(2, MedLogNormRds) - 1) / 10000; } //Local Function Def

            //Lets do all the stats from what is present (that way median won't be undefined)            
            var ValList = SP.Value.Keys.ToList(); int MiddleIndex = (ValList.Count / 2); double MedianReads = 0;
            double MedianLogNormReads = ValList[MiddleIndex];
            MedianReads = GetReads(MedianLogNormReads);
            int LowEnd = Math.Abs(ValList.BinarySearch(MedianLogNormReads - 0.5)); int HighEnd = Math.Abs(ValList.BinarySearch(MedianLogNormReads + 0.5));
            float Range = (float)(HighEnd - LowEnd) / ValList.Count;

            var sB = new StringBuilder();
            sB.Append(IndexName + "\r\n");
            sB.Append(rNames + " gRNAs in SP " + SP.Key + "\r\n");
            sB.Append(MedianReads.ToString("0") + " Median Reads (>200)" + "\r\n");
            sB.Append(((float)StartingIndex / rNames).ToString("0.0% / ") + StartingIndex.ToString("0") + " gRNAs Missing\r\n");
            //sB.Append(Range.ToString("0%") + " ½ Log Range (>50%)\r\n");

            //Top 5% - - - - - - - - - - - - - - - - - - - - - 
            double SumAll = 0, SumTop5P = 0, SumTop1P = 0, SumBot05P = 0, SumBot10P = 0;
            int Bot05P = (int)((float)ValList.Count * 0.05);
            int Bot10P = (int)((float)ValList.Count * 0.10);
            int Top05P = (int)((float)ValList.Count * 0.95);
            int Top01P = (int)((float)ValList.Count * 0.99);
            Top01P = Math.Min(ValList.Count - 1, Top01P);
            double vReads;
            for (int i = 0; i < ValList.Count; i++)
            {
                vReads = GetReads(ValList[i]);
                SumAll += vReads;
                if (i < Bot05P) SumBot05P += vReads;
                if (i < Bot10P) SumBot10P += vReads;
                if (i > Top05P) SumTop5P += vReads;
                if (i > Top01P) SumTop1P += vReads;
            }
            sB.Append((SumTop5P / SumAll).ToString("0.0%") + " Jackpot (5 pctl)" + "\r\n");
            //sB.Append((SumTop1P / SumAll).ToString("0.0%") + " Jackpot (1 pctl)" + "\r\n");
            sB.Append((SumBot10P / SumAll).ToString("0.0%") + " Lowest (10 pctl)" + "\r\n");
            //sB.Append((SumBot05P / SumAll).ToString("0.0%") + " Lowest 5pctl" + "\r\n");

            //Notes>
            double Level = 0.9;                             //But can't just change this since the Sum Bottom10Percent is currently hardcoded!
            int nG = (int)(ValList.Count * Level);          //Number of Guides (non-zero covered at the level)
            double COV = Settings.CellCoverage;             //Coverage
            double rF = (1 - Level) / (SumBot10P / SumAll); //Rep Factor
            double mF = Settings.CellFactor;                //Maintanence or Cell Fator

            double nCellsE = nG * COV * rF; //# Cells in Experiment 
            double nCellsM = nCellsE * mF; //# Cells Maintenance  
            double nCellsPS = nCellsM / (1 - Settings.FDC); //# Cells pre-Selection 
            double uL_Virus = 1000 * nCellsPS * Settings.MOI / Settings.Titer_IU_ML;

            sB.Append("\r\n");
            sB.Append("rF=" + rF.ToString("0.0") + " (" + Settings.rF_Desc(rF) + ")\r\n");
            sB.Append("" + uL_Virus.ToString("0.0") + " uL Virus if MOI=" + Settings.MOI.ToString("0.0") + "\r\n");
            sB.Append("90% of gRNAs (" + nG + ") @" + COV + "X:\r\n");
            sB.Append("" + (nCellsPS / 1000000).ToString("0.0M") + " PreSelection cells" + "\r\n");
            sB.Append("" + (nCellsM / 1000000).ToString("0.0M") + " Maintanence cells" + "\r\n");
            sB.Append("" + (nCellsE / 1000000).ToString("0.0M") + " Experiment cells" + "\r\n");

            return sB.ToString();
        }
    }

    public class RefLongTable
    {
        public List<RefLongEntry> List;
        public Dictionary<string, int> HeadersDict;
        public string[] Headers;
        public Dictionary<string, HashSet<string>> Subpools_rNames;
        public Dictionary<string, double> Subpools_MappedReads;
        public Dictionary<string, double> Indexes_MappedReads;
        public Dictionary<string, double> Indexes_TotalReads;
        public Dictionary<string, double> rNames_MappedReads;
        public double MaxReads_InIndex;
        private double TotalReads = 0;

        public RefLongTable()
        {
            List = new List<RefLongEntry>();
            Subpools_rNames = new Dictionary<string, HashSet<string>>();
            Subpools_MappedReads = new Dictionary<string, double>();
            Indexes_MappedReads = new Dictionary<string, double>();
            Indexes_TotalReads = new Dictionary<string, double>();
            rNames_MappedReads = new Dictionary<string, double>();
        }

        public static RefLongTable LoadRefLong(string Path, double MinFraction_rNAME = 0.0075, bool MergePools = false)
        {
            RefLongTable rlT = new RefLongTable();
            using (StreamReader SR = new StreamReader(Path))
            {
                string t;
                t = SR.ReadLine();
                rlT.Headers = t.Split('\t'); rlT.HeadersDict = new Dictionary<string, int>();
                for (int i = 0; i < rlT.Headers.Length; i++) rlT.HeadersDict.Add(rlT.Headers[i], i);
                while (!SR.EndOfStream)
                {
                    t = SR.ReadLine();
                    rlT.CheckAdd(new RefLongEntry(rlT, t), MergePools);
                }
                SR.Close();
            }
            //Setup Total Reads per Index
            rlT.Setup_TotalReads();
            //Setup LogNorm Reads
            rlT.Setup_LogNormReads();

            rlT.MaxReads_InIndex = 0;
            foreach (var IndexReads in rlT.Indexes_MappedReads.Values) if (rlT.MaxReads_InIndex < IndexReads) rlT.MaxReads_InIndex = IndexReads;

            var toRemove = new List<string>();
            foreach (var Subpool in rlT.Subpools_MappedReads) if ((Subpool.Value / rlT.TotalReads) < MinFraction_rNAME) toRemove.Add(Subpool.Key);
            foreach (var Subpool in toRemove) rlT.RemoveSubpool(Subpool);

            return rlT;
        }

        private void Setup_LogNormReads()
        {
            //Assumes TotalReads have been calculated
            double d;
            foreach (var rlEntry in List)
            {
                d = 10000 * rlEntry.Reads / Indexes_TotalReads[rlEntry.Index];
                d = Math.Log(d + 1, 2);
                rlEntry.LogNormReads = d;
            }
        }

        private void Setup_TotalReads()
        {
            foreach (var rlEntry in List)
            {
                if (!Indexes_TotalReads.ContainsKey(rlEntry.Index)) Indexes_TotalReads.Add(rlEntry.Index, 0);
                Indexes_TotalReads[rlEntry.Index] += rlEntry.Reads;
            }
        }

        private void RemoveSubpool(string SubpoolName)
        {
            foreach (var rName in Subpools_rNames[SubpoolName])
            {
                rNames_MappedReads.Remove(rName);
            }
            Subpools_rNames.Remove(SubpoolName);
            Subpools_MappedReads.Remove(SubpoolName);
        }

        private bool CheckAdd(RefLongEntry rlEntry, bool MergePools = false)
        {
            if (rlEntry.rName == "_Fake") return false;

            if (MergePools) rlEntry.rSubpool = "Mrg";

            if (!Subpools_rNames.ContainsKey(rlEntry.rSubpool)) Subpools_rNames.Add(rlEntry.rSubpool, new HashSet<string>());
            Subpools_rNames[rlEntry.rSubpool].Add(rlEntry.rName);

            if (!Indexes_MappedReads.ContainsKey(rlEntry.Index)) Indexes_MappedReads.Add(rlEntry.Index, 0);
            if (!rNames_MappedReads.ContainsKey(rlEntry.rName)) rNames_MappedReads.Add(rlEntry.rName, 0);
            if (!Subpools_MappedReads.ContainsKey(rlEntry.rSubpool)) Subpools_MappedReads.Add(rlEntry.rSubpool, 0);

            if (rlEntry.Reads > 0)
            {
                TotalReads += rlEntry.Reads;
                Indexes_MappedReads[rlEntry.Index] += rlEntry.Reads;
                rNames_MappedReads[rlEntry.rName] += rlEntry.Reads;
                Subpools_MappedReads[rlEntry.rSubpool] += rlEntry.Reads;
                //LogNormReads = Math.Log(1 + (10000 * Reads / TotalReads), 2);
                //double UNdo = rlEntry.TotalReads * (Math.Pow(2,rlEntry.LogNormReads) - 1) / 10000;
                //if (!Indexes_UnLogNorm_Factor.ContainsKey(rlEntry.Index + " " +rlEntry.rSubpool)) Indexes_UnLogNorm_Factor.Add(rlEntry.Index + " " + rlEntry.rSubpool, rlEntry.Reads / Math.Pow(rlEntry.LogNormReads,2)); 
                List.Add(rlEntry);
                return true;
            }
            return false;
        }
    }

    public class RefLongEntry
    {
        RefLongTable ParentTable;
        public string[] Line;

        public RefLongEntry(RefLongTable Parent, string Line)
        {
            ParentTable = Parent;
            this.Line = Line.Split('\t');
        }

        public string GetValue(string HeaderName)
        {
            int i = -1;
            if (ParentTable.HeadersDict.ContainsKey(HeaderName)) i = ParentTable.HeadersDict[HeaderName];
            if (i > -1 && i < Line.Length) return Line[i];
            return "";
        }

        public double Reads
        {
            get
            {
                string t = GetValue("Reads");
                if (t == "") return double.NaN;
                return double.Parse(t);
            }
        }

        /// <summary>
        /// Calculated from Reads, ignored if included in file
        /// </summary>
        public double LogNormReads { get; set; }

        private string _rSubpool = "";

        public string rSubpool
        {
            get
            {
                if (_rSubpool == "") _rSubpool = GetValue("rSubPool");
                return _rSubpool;
            }
            set
            {
                _rSubpool = value;
            }
        }

        public string rName
        {
            get
            {
                return GetValue("rName");
            }
        }

        public string Index
        {
            get
            {
                return GetValue("Index");
            }
        }
    }

    public static class LA_Cutting_Graphics
    {
        public static List<Brush> tBrushToChoose = new List<Brush>() { Brushes.Red, Brushes.Blue, Brushes.Green, Brushes.Purple, Brushes.Black, Brushes.Yellow, Brushes.Brown, Brushes.Pink, Brushes.DarkCyan, Brushes.LightCoral, Brushes.LightGoldenrodYellow, Brushes.DarkKhaki, Brushes.Brown, Brushes.DeepPink, Brushes.DarkSlateGray, Brushes.DarkSeaGreen, Brushes.Salmon, Brushes.RosyBrown, Brushes.LightGray };
        public static Dictionary<string, Brush> Subpool_Brush = new Dictionary<string, Brush>();
        public static double ExcludeIndex_LessThanThisFraction = 0.05;
        private static Random _Rand = new Random(119);

        internal static void CuttingPlot(Dictionary<string, Dictionary<string, object>> sData, string ExportFileName)
        {
            CuttingPlot(sData, ExportFileName, Size.Empty);
        }

        internal static void CuttingPlot(Dictionary<string, Dictionary<string, object>> sData, string ExportFileName, Size FinalSize)
        {
            //Create the set of indices to examine
            if (sData == null) return;
            if (FinalSize == Size.Empty) FinalSize = new Size(144 * (sData.Count + 1), 660); Subpool_Brush = new Dictionary<string, Brush>();
            int NextBrush = 0; Pen tPen = new Pen(Brushes.Black, 2); Brush tBrush;
            Font tFont = new Font("Arial", 15.8f, FontStyle.Bold); Font tFontSmall = new Font("Arial", 9f);
            Bitmap BM = new Bitmap(FinalSize.Width, FinalSize.Height);
            int HeighUse = (FinalSize.Height - 138); string tRest = "rest";
            int Column = -1; float LastAmount = 0; float WidthMult = (float)(FinalSize.Width / (sData.Count + 1.5));
            using (Graphics g = Graphics.FromImage(BM))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                var Fullrect = new Rectangle(0, 0, BM.Width, BM.Height);
                var Plotrect = new Rectangle(0, 0, BM.Width, HeighUse);
                g.FillRectangle(Brushes.White, Fullrect);
                foreach (var Index in sData)
                {
                    LastAmount = 0; Column++;
                    foreach (var sDatum in Index.Value)
                    {
                        if (sDatum.Key.StartsWith("_")) continue;
                        if (sDatum.Key.StartsWith("rest")) continue;
                        if (!Subpool_Brush.ContainsKey(sDatum.Key))
                            Subpool_Brush.Add(sDatum.Key, BrushToChoose(NextBrush++));
                        tBrush = Subpool_Brush[sDatum.Key];
                        float d = float.Parse(sDatum.Value.ToString());
                        var rect = new RectangleF(Column * WidthMult, (LastAmount * HeighUse), WidthMult * 0.95F, d * HeighUse);
                        LastAmount += d;
                        g.FillRectangle(tBrush, rect);
                        if (d>0.05)
                            g.DrawString(d.ToString("0.0%"), tFont, Brushes.White, Column * WidthMult, (LastAmount * HeighUse) - 26);
                    }
                    //Clear the lower space and write the info for each sample
                    g.FillRectangle(Brushes.White, Column * WidthMult - 9, (LastAmount * HeighUse), FinalSize.Width, HeighUse);
                    g.DrawString(Index.Key, tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 3);
                    g.DrawString(Index.Value["_grna"].ToString(), tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 25);
                    g.DrawString(Index.Value["_total"].ToString(), tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 48);
                    g.DrawString(float.Parse(Index.Value[tRest].ToString()).ToString("0.0%"), tFont, Brushes.Purple, Column * WidthMult, (LastAmount * HeighUse) + 71);
                    g.DrawString(Index.Value["_nearby"].ToString(), tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 94);
                }
                g.DrawRectangle(tPen, Plotrect);

                //Now do the definitions
                Column++;
                g.FillRectangle(Brushes.White, Column * WidthMult - 8, (LastAmount * HeighUse), FinalSize.Width, HeighUse);
                g.DrawString("index", tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 3);
                g.DrawString("gRNA", tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 25);
                g.DrawString("Total reads", tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 48);
                g.DrawString("%Non-informative", tFont, Brushes.Purple, Column * WidthMult, (LastAmount * HeighUse) + 71);
                g.DrawString("In/Out wNearby", tFont, Brushes.Black, Column * WidthMult, (LastAmount * HeighUse) + 94);
                g.DrawString(DateTime.Now + "    " + ExportFileName + "     " + "Non-informative are reads like primer dimer, In/Out wNearby are another way to calculate the %in/out-of-frame.", tFontSmall, Brushes.Black, 2, (LastAmount * HeighUse) + 121);
                LastAmount = 0; float tLeft = (WidthMult * Column) + (WidthMult * 0.1F); WidthMult *= 0.7F;
                WidthMult *= 1.2f;
                foreach (var SpBr in Subpool_Brush)
                {
                    var rect = new RectangleF(tLeft, LastAmount, WidthMult, WidthMult);
                    g.FillRectangle(SpBr.Value, rect);
                    g.DrawString(SpBr.Key, tFont, Brushes.White, tLeft + 15, LastAmount + 12);
                    LastAmount += WidthMult * 1.1F;
                }
            }

            BM.Save(ExportFileName);
        }

        private static Brush BrushToChoose(int BrushIndex)
        {
            int i = BrushIndex >= tBrushToChoose.Count ? BrushIndex % tBrushToChoose.Count : BrushIndex;
            return tBrushToChoose[i];
        }
    }
}