﻿namespace FIVE_Tools_Main
{
    partial class NGS_LibraryAligner_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NGS_LibraryAligner_Form));
            btn_Cancel = new System.Windows.Forms.Button();
            txBx_DownloadLink = new System.Windows.Forms.TextBox();
            txBx_DestFolder = new System.Windows.Forms.TextBox();
            txBx_Update = new System.Windows.Forms.TextBox();
            btn_Start = new System.Windows.Forms.Button();
            dataGridViewM = new System.Windows.Forms.DataGridView();
            bgWrk_LA = new System.ComponentModel.BackgroundWorker();
            btn_LoadDefaults = new System.Windows.Forms.Button();
            btn_LoadFromFile = new System.Windows.Forms.Button();
            btn_SaveParams = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            btn_Download_UnZip = new System.Windows.Forms.Button();
            bgWrk_Download = new System.ComponentModel.BackgroundWorker();
            btn_LoadRef = new System.Windows.Forms.Button();
            btn_LoadFASTQ = new System.Windows.Forms.Button();
            label4 = new System.Windows.Forms.Label();
            btn_CheckRep = new System.Windows.Forms.Button();
            label5 = new System.Windows.Forms.Label();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            chkBx_MergePools = new System.Windows.Forms.CheckBox();
            label6 = new System.Windows.Forms.Label();
            txBx_MOI = new System.Windows.Forms.TextBox();
            label8 = new System.Windows.Forms.Label();
            txBx_TiterIUoML = new System.Windows.Forms.TextBox();
            label9 = new System.Windows.Forms.Label();
            txBx_CellFactor = new System.Windows.Forms.TextBox();
            panel1 = new System.Windows.Forms.Panel();
            label7 = new System.Windows.Forms.Label();
            btn_Cutting = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)dataGridViewM).BeginInit();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // btn_Cancel
            // 
            btn_Cancel.Location = new System.Drawing.Point(455, 459);
            btn_Cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Cancel.Name = "btn_Cancel";
            btn_Cancel.Size = new System.Drawing.Size(88, 27);
            btn_Cancel.TabIndex = 6;
            btn_Cancel.Text = "Cancel";
            toolTip1.SetToolTip(btn_Cancel, "Cancel processing (wait a few minutes after clicking)");
            btn_Cancel.UseVisualStyleBackColor = true;
            btn_Cancel.Click += btn_Cancel_Click;
            // 
            // txBx_DownloadLink
            // 
            txBx_DownloadLink.Location = new System.Drawing.Point(621, 14);
            txBx_DownloadLink.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DownloadLink.Name = "txBx_DownloadLink";
            txBx_DownloadLink.Size = new System.Drawing.Size(204, 23);
            txBx_DownloadLink.TabIndex = 1;
            toolTip1.SetToolTip(txBx_DownloadLink, "Paste the link to the Apache file list to automatically download the R1 and R2 files");
            // 
            // txBx_DestFolder
            // 
            txBx_DestFolder.Location = new System.Drawing.Point(873, 14);
            txBx_DestFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DestFolder.Name = "txBx_DestFolder";
            txBx_DestFolder.Size = new System.Drawing.Size(165, 23);
            txBx_DestFolder.TabIndex = 2;
            toolTip1.SetToolTip(txBx_DestFolder, "Specifies the location to download the files");
            txBx_DestFolder.TextChanged += txBx_DestFolder_TextChanged;
            // 
            // txBx_Update
            // 
            txBx_Update.Location = new System.Drawing.Point(581, 44);
            txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_Update.Multiline = true;
            txBx_Update.Name = "txBx_Update";
            txBx_Update.Size = new System.Drawing.Size(572, 444);
            txBx_Update.TabIndex = 3;
            // 
            // btn_Start
            // 
            btn_Start.Location = new System.Drawing.Point(455, 426);
            btn_Start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Start.Name = "btn_Start";
            btn_Start.Size = new System.Drawing.Size(88, 27);
            btn_Start.TabIndex = 5;
            btn_Start.Text = "Start";
            toolTip1.SetToolTip(btn_Start, "Start processing FastQ files");
            btn_Start.UseVisualStyleBackColor = true;
            btn_Start.Click += btn_Start_Click;
            // 
            // dataGridViewM
            // 
            dataGridViewM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewM.Location = new System.Drawing.Point(14, 14);
            dataGridViewM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dataGridViewM.Name = "dataGridViewM";
            dataGridViewM.Size = new System.Drawing.Size(422, 530);
            dataGridViewM.TabIndex = 5;
            // 
            // bgWrk_LA
            // 
            bgWrk_LA.WorkerReportsProgress = true;
            bgWrk_LA.WorkerSupportsCancellation = true;
            bgWrk_LA.DoWork += bgWrk_LA_DoWork;
            bgWrk_LA.ProgressChanged += bgWrk_LA_ProgressChanged;
            bgWrk_LA.RunWorkerCompleted += bgWrk_LA_RunWorkerCompleted;
            // 
            // btn_LoadDefaults
            // 
            btn_LoadDefaults.Location = new System.Drawing.Point(454, 112);
            btn_LoadDefaults.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_LoadDefaults.Name = "btn_LoadDefaults";
            btn_LoadDefaults.Size = new System.Drawing.Size(88, 27);
            btn_LoadDefaults.TabIndex = 6;
            btn_LoadDefaults.Text = "Defaults";
            toolTip1.SetToolTip(btn_LoadDefaults, "Loads the default settings. This is a good place to start.");
            btn_LoadDefaults.UseVisualStyleBackColor = true;
            btn_LoadDefaults.Click += btn_LoadDefaults_Click;
            // 
            // btn_LoadFromFile
            // 
            btn_LoadFromFile.Location = new System.Drawing.Point(454, 145);
            btn_LoadFromFile.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_LoadFromFile.Name = "btn_LoadFromFile";
            btn_LoadFromFile.Size = new System.Drawing.Size(88, 27);
            btn_LoadFromFile.TabIndex = 7;
            btn_LoadFromFile.Text = "Load ..";
            toolTip1.SetToolTip(btn_LoadFromFile, "Loads previousy saved settings.");
            btn_LoadFromFile.UseVisualStyleBackColor = true;
            btn_LoadFromFile.Click += btn_LoadFromFile_Click;
            // 
            // btn_SaveParams
            // 
            btn_SaveParams.Location = new System.Drawing.Point(454, 179);
            btn_SaveParams.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_SaveParams.Name = "btn_SaveParams";
            btn_SaveParams.Size = new System.Drawing.Size(88, 27);
            btn_SaveParams.TabIndex = 8;
            btn_SaveParams.Text = "Save";
            toolTip1.SetToolTip(btn_SaveParams, "Save the current settings.");
            btn_SaveParams.UseVisualStyleBackColor = true;
            btn_SaveParams.Click += btn_SaveParams_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(459, 93);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(79, 15);
            label1.TabIndex = 9;
            label1.Text = "XML Settings:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(555, 17);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(60, 15);
            label2.TabIndex = 10;
            label2.Text = "HTCF Link";
            toolTip1.SetToolTip(label2, "Paste the link to the Apache file list to automatically download the R1 and R2 files");
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(838, 18);
            label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(30, 15);
            label3.TabIndex = 11;
            label3.Text = "Dest";
            toolTip1.SetToolTip(label3, "Specifies the location to download the files");
            // 
            // btn_Download_UnZip
            // 
            btn_Download_UnZip.Location = new System.Drawing.Point(1045, 12);
            btn_Download_UnZip.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Download_UnZip.Name = "btn_Download_UnZip";
            btn_Download_UnZip.Size = new System.Drawing.Size(108, 27);
            btn_Download_UnZip.TabIndex = 12;
            btn_Download_UnZip.Text = "Download UZ";
            toolTip1.SetToolTip(btn_Download_UnZip, "Downloads files from the provided link, then attempts to UnZip them. 7-Zip must be installed for the latter to work.");
            btn_Download_UnZip.UseVisualStyleBackColor = true;
            btn_Download_UnZip.Click += btn_Download_UnZip_Click;
            // 
            // bgWrk_Download
            // 
            bgWrk_Download.WorkerReportsProgress = true;
            bgWrk_Download.WorkerSupportsCancellation = true;
            bgWrk_Download.DoWork += bgWrk_Download_DoWork;
            bgWrk_Download.ProgressChanged += bgWrk_Download_ProgressChanged;
            bgWrk_Download.RunWorkerCompleted += bgWrk_Download_RunWorkerCompleted;
            // 
            // btn_LoadRef
            // 
            btn_LoadRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_LoadRef.Location = new System.Drawing.Point(455, 23);
            btn_LoadRef.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_LoadRef.Name = "btn_LoadRef";
            btn_LoadRef.Size = new System.Drawing.Size(86, 23);
            btn_LoadRef.TabIndex = 3;
            btn_LoadRef.Text = "browse ref";
            toolTip1.SetToolTip(btn_LoadRef, resources.GetString("btn_LoadRef.ToolTip"));
            btn_LoadRef.UseVisualStyleBackColor = true;
            btn_LoadRef.Click += btn_LoadRef_Click;
            // 
            // btn_LoadFASTQ
            // 
            btn_LoadFASTQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_LoadFASTQ.Location = new System.Drawing.Point(455, 49);
            btn_LoadFASTQ.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_LoadFASTQ.Name = "btn_LoadFASTQ";
            btn_LoadFASTQ.Size = new System.Drawing.Size(86, 23);
            btn_LoadFASTQ.TabIndex = 4;
            btn_LoadFASTQ.Text = "browse fa";
            toolTip1.SetToolTip(btn_LoadFASTQ, "Select any FastQ file and LA will run on all FastQ files in this folder");
            btn_LoadFASTQ.UseVisualStyleBackColor = true;
            btn_LoadFASTQ.Click += btn_LoadFASTQ_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label4.Location = new System.Drawing.Point(448, 212);
            label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label4.MaximumSize = new System.Drawing.Size(117, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(117, 156);
            label4.TabIndex = 15;
            label4.Text = resources.GetString("label4.Text");
            // 
            // btn_CheckRep
            // 
            btn_CheckRep.Location = new System.Drawing.Point(568, 6);
            btn_CheckRep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_CheckRep.Name = "btn_CheckRep";
            btn_CheckRep.Size = new System.Drawing.Size(126, 36);
            btn_CheckRep.TabIndex = 24;
            btn_CheckRep.Text = "Check Rep";
            toolTip1.SetToolTip(btn_CheckRep, "After running LA, use this to export standard plots to check the representation");
            btn_CheckRep.UseVisualStyleBackColor = true;
            btn_CheckRep.Click += btn_CheckRep_Click;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label5.Location = new System.Drawing.Point(9, 8);
            label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label5.MaximumSize = new System.Drawing.Size(105, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(74, 32);
            label5.TabIndex = 17;
            label5.Text = "Post Count Analysis";
            // 
            // chkBx_MergePools
            // 
            chkBx_MergePools.AutoSize = true;
            chkBx_MergePools.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            chkBx_MergePools.Location = new System.Drawing.Point(495, 16);
            chkBx_MergePools.Name = "chkBx_MergePools";
            chkBx_MergePools.Size = new System.Drawing.Size(61, 16);
            chkBx_MergePools.TabIndex = 23;
            chkBx_MergePools.Text = "Merge P";
            toolTip1.SetToolTip(chkBx_MergePools, "Merge the Subpools and libraries together");
            chkBx_MergePools.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label6.Location = new System.Drawing.Point(245, 4);
            label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label6.MaximumSize = new System.Drawing.Size(105, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(27, 13);
            label6.TabIndex = 21;
            label6.Text = "MOI";
            toolTip1.SetToolTip(label6, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // txBx_MOI
            // 
            txBx_MOI.Location = new System.Drawing.Point(245, 20);
            txBx_MOI.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_MOI.Name = "txBx_MOI";
            txBx_MOI.Size = new System.Drawing.Size(65, 23);
            txBx_MOI.TabIndex = 20;
            txBx_MOI.Text = "0.4";
            toolTip1.SetToolTip(txBx_MOI, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label8.Location = new System.Drawing.Point(404, 4);
            label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label8.MaximumSize = new System.Drawing.Size(105, 0);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(58, 13);
            label8.TabIndex = 23;
            label8.Text = "Titer (IU/ml)";
            toolTip1.SetToolTip(label8, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // txBx_TiterIUoML
            // 
            txBx_TiterIUoML.Location = new System.Drawing.Point(403, 20);
            txBx_TiterIUoML.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_TiterIUoML.Name = "txBx_TiterIUoML";
            txBx_TiterIUoML.Size = new System.Drawing.Size(83, 23);
            txBx_TiterIUoML.TabIndex = 22;
            txBx_TiterIUoML.Text = "4.5E7";
            toolTip1.SetToolTip(txBx_TiterIUoML, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label9.Location = new System.Drawing.Point(316, 4);
            label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label9.MaximumSize = new System.Drawing.Size(105, 0);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(79, 13);
            label9.TabIndex = 25;
            label9.Text = "mF (Cell Factor)";
            toolTip1.SetToolTip(label9, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // txBx_CellFactor
            // 
            txBx_CellFactor.Location = new System.Drawing.Point(315, 20);
            txBx_CellFactor.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_CellFactor.Name = "txBx_CellFactor";
            txBx_CellFactor.Size = new System.Drawing.Size(83, 23);
            txBx_CellFactor.TabIndex = 21;
            txBx_CellFactor.Text = "2";
            toolTip1.SetToolTip(txBx_CellFactor, "Write a set of MOIs that you want to perform calculations for. Usually \"0.1, 0.4, 1\".");
            // 
            // panel1
            // 
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(label9);
            panel1.Controls.Add(txBx_CellFactor);
            panel1.Controls.Add(label8);
            panel1.Controls.Add(txBx_TiterIUoML);
            panel1.Controls.Add(label6);
            panel1.Controls.Add(txBx_MOI);
            panel1.Controls.Add(label5);
            panel1.Controls.Add(btn_CheckRep);
            panel1.Controls.Add(chkBx_MergePools);
            panel1.Location = new System.Drawing.Point(444, 494);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(709, 50);
            panel1.TabIndex = 19;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(459, 408);
            label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(69, 15);
            label7.TabIndex = 20;
            label7.Text = "Get Counts:";
            // 
            // btn_Cutting
            // 
            btn_Cutting.Location = new System.Drawing.Point(455, 371);
            btn_Cutting.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Cutting.Name = "btn_Cutting";
            btn_Cutting.Size = new System.Drawing.Size(88, 27);
            btn_Cutting.TabIndex = 21;
            btn_Cutting.Text = "Cutting";
            toolTip1.SetToolTip(btn_Cutting, "Start processing FastQ files");
            btn_Cutting.UseVisualStyleBackColor = true;
            btn_Cutting.Click += btn_Cutting_Click;
            // 
            // NGS_LibraryAligner_Form
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1168, 549);
            Controls.Add(btn_Cutting);
            Controls.Add(label7);
            Controls.Add(panel1);
            Controls.Add(label4);
            Controls.Add(btn_LoadFASTQ);
            Controls.Add(btn_LoadRef);
            Controls.Add(btn_Download_UnZip);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btn_SaveParams);
            Controls.Add(btn_LoadFromFile);
            Controls.Add(btn_LoadDefaults);
            Controls.Add(dataGridViewM);
            Controls.Add(btn_Start);
            Controls.Add(txBx_Update);
            Controls.Add(txBx_DestFolder);
            Controls.Add(txBx_DownloadLink);
            Controls.Add(btn_Cancel);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "NGS_LibraryAligner_Form";
            Text = "Library Aligner";
            Load += NGS_LibraryAligner_Form_Load;
            ((System.ComponentModel.ISupportInitialize)dataGridViewM).EndInit();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.TextBox txBx_DownloadLink;
        private System.Windows.Forms.TextBox txBx_DestFolder;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.DataGridView dataGridViewM;
        private System.ComponentModel.BackgroundWorker bgWrk_LA;
        private System.Windows.Forms.Button btn_LoadDefaults;
        private System.Windows.Forms.Button btn_LoadFromFile;
        private System.Windows.Forms.Button btn_SaveParams;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Download_UnZip;
        private System.ComponentModel.BackgroundWorker bgWrk_Download;
        private System.Windows.Forms.Button btn_LoadRef;
        private System.Windows.Forms.Button btn_LoadFASTQ;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_CheckRep;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkBx_MergePools;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_MOI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txBx_CellFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txBx_TiterIUoML;
        private System.Windows.Forms.Button btn_Cutting;
    }
}