﻿namespace FIVE_Tools_Main
{
    partial class NGSAlleleFragmentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txBx_gRNA = new System.Windows.Forms.TextBox();
            this.btn_FindAlleleFragments = new System.Windows.Forms.Button();
            this.txBx_Amplicon = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txBx_Name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txBx_FastQPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txBx_AnchorSize = new System.Windows.Forms.TextBox();
            this.txBx_AnchorShift = new System.Windows.Forms.TextBox();
            this.label_AnchorStep = new System.Windows.Forms.Label();
            this.txBx_AlleleSize = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txBx_MaxAmpSearchSize = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.chkBx_RevComp = new System.Windows.Forms.CheckBox();
            this.bgWorker_Find = new System.ComponentModel.BackgroundWorker();
            this.txBx_FractionCovered = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "WT Amplicon";
            // 
            // txBx_gRNA
            // 
            this.txBx_gRNA.Location = new System.Drawing.Point(286, 24);
            this.txBx_gRNA.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_gRNA.Name = "txBx_gRNA";
            this.txBx_gRNA.Size = new System.Drawing.Size(285, 20);
            this.txBx_gRNA.TabIndex = 1;
            // 
            // btn_FindAlleleFragments
            // 
            this.btn_FindAlleleFragments.Location = new System.Drawing.Point(491, 189);
            this.btn_FindAlleleFragments.Margin = new System.Windows.Forms.Padding(2);
            this.btn_FindAlleleFragments.Name = "btn_FindAlleleFragments";
            this.btn_FindAlleleFragments.Size = new System.Drawing.Size(80, 54);
            this.btn_FindAlleleFragments.TabIndex = 2;
            this.btn_FindAlleleFragments.Text = "Find Allele Fragments";
            this.btn_FindAlleleFragments.UseVisualStyleBackColor = true;
            this.btn_FindAlleleFragments.Click += new System.EventHandler(this.btn_FindAlleleFragments_Click);
            // 
            // txBx_Amplicon
            // 
            this.txBx_Amplicon.Location = new System.Drawing.Point(11, 64);
            this.txBx_Amplicon.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Amplicon.Multiline = true;
            this.txBx_Amplicon.Name = "txBx_Amplicon";
            this.txBx_Amplicon.Size = new System.Drawing.Size(560, 62);
            this.txBx_Amplicon.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(283, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "gRNA";
            // 
            // txBx_Name
            // 
            this.txBx_Name.Location = new System.Drawing.Point(11, 24);
            this.txBx_Name.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Name.Name = "txBx_Name";
            this.txBx_Name.Size = new System.Drawing.Size(218, 20);
            this.txBx_Name.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Name";
            // 
            // txBx_FastQPath
            // 
            this.txBx_FastQPath.Location = new System.Drawing.Point(11, 150);
            this.txBx_FastQPath.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_FastQPath.Name = "txBx_FastQPath";
            this.txBx_FastQPath.Size = new System.Drawing.Size(560, 20);
            this.txBx_FastQPath.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "FastQ Path";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 180);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Anchor Size";
            // 
            // txBx_AnchorSize
            // 
            this.txBx_AnchorSize.Location = new System.Drawing.Point(11, 195);
            this.txBx_AnchorSize.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_AnchorSize.Name = "txBx_AnchorSize";
            this.txBx_AnchorSize.Size = new System.Drawing.Size(61, 20);
            this.txBx_AnchorSize.TabIndex = 10;
            // 
            // txBx_AnchorShift
            // 
            this.txBx_AnchorShift.Location = new System.Drawing.Point(94, 195);
            this.txBx_AnchorShift.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_AnchorShift.Name = "txBx_AnchorShift";
            this.txBx_AnchorShift.Size = new System.Drawing.Size(61, 20);
            this.txBx_AnchorShift.TabIndex = 12;
            // 
            // label_AnchorStep
            // 
            this.label_AnchorStep.AutoSize = true;
            this.label_AnchorStep.Location = new System.Drawing.Point(91, 180);
            this.label_AnchorStep.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_AnchorStep.Name = "label_AnchorStep";
            this.label_AnchorStep.Size = new System.Drawing.Size(65, 13);
            this.label_AnchorStep.TabIndex = 11;
            this.label_AnchorStep.Text = "Anchor Shift";
            // 
            // txBx_AlleleSize
            // 
            this.txBx_AlleleSize.Location = new System.Drawing.Point(181, 195);
            this.txBx_AlleleSize.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_AlleleSize.Name = "txBx_AlleleSize";
            this.txBx_AlleleSize.Size = new System.Drawing.Size(61, 20);
            this.txBx_AlleleSize.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(176, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Allele Frag Size";
            // 
            // txBx_MaxAmpSearchSize
            // 
            this.txBx_MaxAmpSearchSize.Location = new System.Drawing.Point(273, 195);
            this.txBx_MaxAmpSearchSize.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_MaxAmpSearchSize.Name = "txBx_MaxAmpSearchSize";
            this.txBx_MaxAmpSearchSize.Size = new System.Drawing.Size(61, 20);
            this.txBx_MaxAmpSearchSize.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(270, 180);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Max Amp Search";
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(586, 6);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(183, 257);
            this.txBx_Update.TabIndex = 17;
            // 
            // chkBx_RevComp
            // 
            this.chkBx_RevComp.AutoSize = true;
            this.chkBx_RevComp.Location = new System.Drawing.Point(11, 226);
            this.chkBx_RevComp.Name = "chkBx_RevComp";
            this.chkBx_RevComp.Size = new System.Drawing.Size(127, 17);
            this.chkBx_RevComp.TabIndex = 18;
            this.chkBx_RevComp.Text = "Reverse Complement";
            this.chkBx_RevComp.UseVisualStyleBackColor = true;
            // 
            // bgWorker_Find
            // 
            this.bgWorker_Find.WorkerReportsProgress = true;
            this.bgWorker_Find.WorkerSupportsCancellation = true;
            this.bgWorker_Find.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_Find_DoWork);
            this.bgWorker_Find.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_Find_ProgressChanged);
            this.bgWorker_Find.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_Find_RunWorkerCompleted);
            // 
            // txBx_FractionCovered
            // 
            this.txBx_FractionCovered.Location = new System.Drawing.Point(378, 195);
            this.txBx_FractionCovered.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_FractionCovered.Name = "txBx_FractionCovered";
            this.txBx_FractionCovered.Size = new System.Drawing.Size(61, 20);
            this.txBx_FractionCovered.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(375, 180);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Fraction Covered";
            // 
            // NGSAlleleFragmentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 285);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txBx_FractionCovered);
            this.Controls.Add(this.chkBx_RevComp);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.txBx_MaxAmpSearchSize);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txBx_AlleleSize);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txBx_AnchorShift);
            this.Controls.Add(this.label_AnchorStep);
            this.Controls.Add(this.txBx_AnchorSize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txBx_FastQPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txBx_Name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txBx_Amplicon);
            this.Controls.Add(this.btn_FindAlleleFragments);
            this.Controls.Add(this.txBx_gRNA);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NGSAlleleFragmentsForm";
            this.Text = "NGS Allele Fragments";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txBx_gRNA;
        private System.Windows.Forms.Button btn_FindAlleleFragments;
        private System.Windows.Forms.TextBox txBx_Amplicon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBx_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txBx_FastQPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txBx_AnchorSize;
        private System.Windows.Forms.TextBox txBx_AnchorShift;
        private System.Windows.Forms.Label label_AnchorStep;
        private System.Windows.Forms.TextBox txBx_AlleleSize;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txBx_MaxAmpSearchSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.CheckBox chkBx_RevComp;
        private System.ComponentModel.BackgroundWorker bgWorker_Find;
        private System.Windows.Forms.TextBox txBx_FractionCovered;
        private System.Windows.Forms.Label label6;
    }
}