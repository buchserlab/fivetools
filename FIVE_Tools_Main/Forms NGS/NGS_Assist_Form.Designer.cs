﻿using System.Drawing;

namespace FIVE_Tools_Main
{
   
    partial class NGS_Assist_Form
    {
     
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NGS_Assist_Form));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txBx_ReadsFile = new System.Windows.Forms.TextBox();
            this.txBx_ModelFile = new System.Windows.Forms.TextBox();
            this.btn_LoadReads = new System.Windows.Forms.Button();
            this.btn_LoadScores = new System.Windows.Forms.Button();
            this.btn_Export = new System.Windows.Forms.Button();
            this.txBx_ExportName = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_OpenFileDialog_Reads = new System.Windows.Forms.Button();
            this.btn_OpenFileDialog_Scores = new System.Windows.Forms.Button();
            this.bgWork_Export = new System.ComponentModel.BackgroundWorker();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btn_SaveParams = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_LoadSettings = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(298, 482);
            this.dataGridView1.TabIndex = 0;
            // 
            // txBx_ReadsFile
            // 
            this.txBx_ReadsFile.Location = new System.Drawing.Point(316, 118);
            this.txBx_ReadsFile.Name = "txBx_ReadsFile";
            this.txBx_ReadsFile.Size = new System.Drawing.Size(472, 20);
            this.txBx_ReadsFile.TabIndex = 1;
            // 
            // txBx_ModelFile
            // 
            this.txBx_ModelFile.Location = new System.Drawing.Point(316, 177);
            this.txBx_ModelFile.Name = "txBx_ModelFile";
            this.txBx_ModelFile.Size = new System.Drawing.Size(472, 20);
            this.txBx_ModelFile.TabIndex = 2;
            // 
            // btn_LoadReads
            // 
            this.btn_LoadReads.Location = new System.Drawing.Point(713, 92);
            this.btn_LoadReads.Name = "btn_LoadReads";
            this.btn_LoadReads.Size = new System.Drawing.Size(75, 23);
            this.btn_LoadReads.TabIndex = 3;
            this.btn_LoadReads.Text = "Load Reads File";
            this.btn_LoadReads.UseVisualStyleBackColor = true;
            this.btn_LoadReads.Click += new System.EventHandler(this.btn_LoadNodes);
            // 
            // btn_LoadScores
            // 
            this.btn_LoadScores.Location = new System.Drawing.Point(713, 151);
            this.btn_LoadScores.Name = "btn_LoadScores";
            this.btn_LoadScores.Size = new System.Drawing.Size(75, 23);
            this.btn_LoadScores.TabIndex = 4;
            this.btn_LoadScores.Text = "Load Scores File";
            this.btn_LoadScores.UseVisualStyleBackColor = true;
            this.btn_LoadScores.Click += new System.EventHandler(this.btn_LoadScores_Click);
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(525, 251);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.Size = new System.Drawing.Size(75, 23);
            this.btn_Export.TabIndex = 5;
            this.btn_Export.Text = "Export";
            this.btn_Export.UseVisualStyleBackColor = true;
            this.btn_Export.Click += new System.EventHandler(this.btn_Export_Click);
            // 
            // txBx_ExportName
            // 
            this.txBx_ExportName.Location = new System.Drawing.Point(378, 225);
            this.txBx_ExportName.Name = "txBx_ExportName";
            this.txBx_ExportName.Size = new System.Drawing.Size(410, 20);
            this.txBx_ExportName.TabIndex = 6;
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(561, 319);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(227, 157);
            this.txBx_Update.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(316, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Mutant Read Information (byIndex)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Model Score Data (MSR)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(316, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(452, 26);
            this.label3.TabIndex = 10;
            this.label3.Text = "Instructions : Use the exported file from Spotfire to get the %Mutants, as well a" +
    "s all the scores.  \r\nAdjust and values on the left, then hit export.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(561, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Progress";
            // 
            // btn_OpenFileDialog_Reads
            // 
            this.btn_OpenFileDialog_Reads.Location = new System.Drawing.Point(492, 95);
            this.btn_OpenFileDialog_Reads.Name = "btn_OpenFileDialog_Reads";
            this.btn_OpenFileDialog_Reads.Size = new System.Drawing.Size(34, 20);
            this.btn_OpenFileDialog_Reads.TabIndex = 12;
            this.btn_OpenFileDialog_Reads.Text = "...";
            this.btn_OpenFileDialog_Reads.UseVisualStyleBackColor = true;
            this.btn_OpenFileDialog_Reads.Click += new System.EventHandler(this.btn_OpenFileDialog_Click);
            // 
            // btn_OpenFileDialog_Scores
            // 
            this.btn_OpenFileDialog_Scores.Location = new System.Drawing.Point(492, 154);
            this.btn_OpenFileDialog_Scores.Name = "btn_OpenFileDialog_Scores";
            this.btn_OpenFileDialog_Scores.Size = new System.Drawing.Size(34, 20);
            this.btn_OpenFileDialog_Scores.TabIndex = 13;
            this.btn_OpenFileDialog_Scores.Text = "...";
            this.btn_OpenFileDialog_Scores.UseVisualStyleBackColor = true;
            this.btn_OpenFileDialog_Scores.Click += new System.EventHandler(this.btn_OpenFileDialog_Click);
            // 
            // bgWork_Export
            // 
            this.bgWork_Export.WorkerReportsProgress = true;
            this.bgWork_Export.WorkerSupportsCancellation = true;
            this.bgWork_Export.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork_Export_DoWork);
            this.bgWork_Export.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWork_Export_ProgressChanged);
            this.bgWork_Export.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWork_Export_RunWorkerCompleted);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(606, 251);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 14;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(674, 46);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(114, 23);
            this.btn_Reset.TabIndex = 15;
            this.btn_Reset.Text = "Reset";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btn_SaveParams
            // 
            this.btn_SaveParams.Location = new System.Drawing.Point(412, 46);
            this.btn_SaveParams.Name = "btn_SaveParams";
            this.btn_SaveParams.Size = new System.Drawing.Size(96, 23);
            this.btn_SaveParams.TabIndex = 16;
            this.btn_SaveParams.Text = "Save Params";
            this.btn_SaveParams.UseVisualStyleBackColor = true;
            this.btn_SaveParams.Click += new System.EventHandler(this.btn_SaveParams_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(381, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Export File Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(316, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(228, 91);
            this.label6.TabIndex = 18;
            this.label6.Text = resources.GetString("label6.Text");
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // btn_LoadSettings
            // 
            this.btn_LoadSettings.Location = new System.Drawing.Point(316, 46);
            this.btn_LoadSettings.Name = "btn_LoadSettings";
            this.btn_LoadSettings.Size = new System.Drawing.Size(90, 23);
            this.btn_LoadSettings.TabIndex = 19;
            this.btn_LoadSettings.Text = "Load Settings";
            this.btn_LoadSettings.UseVisualStyleBackColor = true;
            this.btn_LoadSettings.Click += new System.EventHandler(this.btn_LoadSettings_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = Color.FromKnownColor(KnownColor.ControlText);
            this.label7.Location = new System.Drawing.Point(316, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "8 Results\\";
            // 
            // NGS_Assist_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 488);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_LoadSettings);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_SaveParams);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OpenFileDialog_Scores);
            this.Controls.Add(this.btn_OpenFileDialog_Reads);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.txBx_ExportName);
            this.Controls.Add(this.btn_Export);
            this.Controls.Add(this.btn_LoadScores);
            this.Controls.Add(this.btn_LoadReads);
            this.Controls.Add(this.txBx_ModelFile);
            this.Controls.Add(this.txBx_ReadsFile);
            this.Controls.Add(this.dataGridView1);
            this.Name = "NGS_Assist_Form";
            this.Text = "NGS Assist and Model Testing";
            this.Load += new System.EventHandler(this.NGS_Assist_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txBx_ReadsFile;
        private System.Windows.Forms.TextBox txBx_ModelFile;
        private System.Windows.Forms.Button btn_LoadReads;
        private System.Windows.Forms.Button btn_LoadScores;
        private System.Windows.Forms.Button btn_Export;
        private System.Windows.Forms.TextBox txBx_ExportName;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_OpenFileDialog_Reads;
        private System.Windows.Forms.Button btn_OpenFileDialog_Scores;
        private System.ComponentModel.BackgroundWorker bgWork_Export;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btn_SaveParams;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Label label7;
    }
}