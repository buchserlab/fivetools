﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FIVE.NGS_Assist;

namespace FIVE_Tools_Main
{
    public partial class NGS_Assist_Form : Form
    {
        public NGS_Assist_Start NGSA;
        public NGS_Assist_Params Pa;
        public OpenFileDialog OFD = new OpenFileDialog();

        public NGS_Assist_Form()
        {
            Pa = NGS_Assist_Params.Load(); if (Pa == null) Pa = new NGS_Assist_Params();
            if (Pa._Col2PredScoresNames.Count > 3) Pa._Col2PredScoresNames.RemoveRange(3, Pa._Col2PredScoresNames.Count - 3);

            InitializeComponent();
            txBx_ReadsFile.Text = Pa._ReadsFile;
            txBx_ModelFile.Text = Pa._ModelFile;
            txBx_ExportName.Text = Pa._DefaultName;

            btn_Reset_Click(null, null);
        }

        private void NGS_Assist_Form_Load(object sender, EventArgs e)
        {
            RefreshViewer_Post();
        }

        private void btn_LoadNodes(object sender, EventArgs e)
        {
            RefreshViewer_Pre();
            FileInfo FI = new FileInfo(txBx_ReadsFile.Text); if (!FI.Exists)
            {
                txBx_Update.Text += "File Not Found\r\n";
                return;
            }
            if (Debugger.IsAttached)
            {
                LoadNodesActual();
            }
            else
            {
                try
                {
                    LoadNodesActual();
                }
                catch (Exception E)
                {
                    txBx_Update.Text += "Problem loading or with Column names : " + E.Message + "\r\n";
                }
            }
            btn_LoadScores.Enabled = true; btn_Export.Enabled = false;
        }

        private void LoadNodesActual()
        {
            NGSA = new NGS_Assist_Start(txBx_ReadsFile.Text, Pa);
            if (NGSA.ErrorLast == "" || NGSA.ErrorLast == null)
            {
                txBx_Update.Text += "Indexes loaded: " + NGSA.IndexCount + "\r\nKeys: " + NGSA.ClassKeys + "\r\n%MU Steps: " + NGSA.MutantSteps + "\r\n";
            }
            else
            {
                txBx_Update.Text += "Error, probably with a column name: " + NGSA.ErrorLast + "\r\n";
            }
            RefreshViewer_Post();
        }

        private void btn_LoadScores_Click(object sender, EventArgs e)
        {
            RefreshViewer_Pre();
            if (NGSA == null) { txBx_Update.Text += "Open the reads first.\r\n"; return; };
            if (txBx_ModelFile.Text.Trim() == "") { txBx_Update.Text += "Select a file first.\r\n"; return; };
            FileInfo FI = new FileInfo(txBx_ModelFile.Text); if (!FI.Exists)
            {
                txBx_Update.Text += "File Not Found\r\n";
                return;
            }
            string Error = NGSA.ImportScores(txBx_ModelFile.Text);
            if (Error != "") { txBx_Update.Text += Error + "\r\n"; return; }
            txBx_Update.Text += "Scores Loaded: " + NGSA.ScoresCount + "\r\nScores Steps: " + NGSA.ScoreSteps + "\r\nTotal Rows: " + NGSA.TotalCountKilo + "k\r\n";
            RefreshViewer_Post();
            if (NGSA.ScoresCount > 0)
            {
                //Make the controlled name for the output file
                string tName, tError;
                Pa.Build_AUCResults_Name_NoExt(out tName, out tError);
                if (tError == "") txBx_ExportName.Text = tName;
                else txBx_Update.Text += tError + "\r\n";

                btn_Export.Enabled = true; btn_SaveParams.Enabled = dataGridView1.Enabled = false;
            }
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            RefreshViewer_Pre();
            btn_Export.Enabled = btn_LoadReads.Enabled = btn_LoadScores.Enabled = false; btn_Cancel.Enabled = true;
            txBx_Update.Text += "Starting Export . . ." + "\r\n";
            string fldr = Pa.Local_Results_Folder;
            string exportPath = Path.Combine(fldr, txBx_ExportName.Text == "" ? "AUC_Results_001.txt" : Path.GetFileNameWithoutExtension(txBx_ExportName.Text));
            Pa.SaveCheckLocal(exportPath + ".xml");
            bgWork_Export.RunWorkerAsync(exportPath + ".txt");
        }

        private void RefreshViewer_Pre()
        {
            Pa._ReadsFile = txBx_ReadsFile.Text; Pa._ModelFile = txBx_ModelFile.Text; Pa._DefaultName = txBx_ExportName.Text;
            Pa.Save(); Pa.SaveCheckLocal();
        }

        private void RefreshViewer_Post()
        {
            dataGridView1.DataSource = Pa.GetList().ToList();
            txBx_ReadsFile.Text = Pa._ReadsFile; txBx_ModelFile.Text = Pa._ModelFile; txBx_ExportName.Text = Pa._DefaultName;
        }

        private void btn_SaveParams_Click(object sender, EventArgs e)
        {
            Pa.UpdateFrom((IEnumerable<KVP_Param_Item>)dataGridView1.DataSource);
            RefreshViewer_Pre();
            txBx_Update.Text += "Settings saved.\r\n";
        }

        private void btn_OpenFileDialog_Click(object sender, EventArgs e)
        {
            OFD.Filter = "tab-delimited text (*.txt)|*.txt|All files (*.*)|*.*";
            if (OFD.InitialDirectory == "" && txBx_ReadsFile.Text != "") OFD.InitialDirectory = Path.GetDirectoryName(txBx_ReadsFile.Text);
            DialogResult DR = OFD.ShowDialog();
            if (DR == DialogResult.Cancel) return;

            Button sndr = (Button)sender;
            if (sndr.Name == btn_OpenFileDialog_Reads.Name)
            {
                txBx_ReadsFile.Text = OFD.FileName;
            }
            else
            {
                txBx_ModelFile.Text = OFD.FileName;
            }
        }

        private void bgWork_Export_DoWork(object sender, DoWorkEventArgs e)
        {
            string exportPath = e.Argument.ToString();
            NGSA.Calculate_Export(exportPath, bgWork_Export);
        }

        private void bgWork_Export_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.ProgressPercentage.ToString() + "% " + (e.UserState == null ? "" : e.UserState.ToString()) + "\r\n" + txBx_Update.Text;
        }

        private static string ExportFinishedMessage = "DONE Exporting.";

        private void bgWork_Export_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = ExportFinishedMessage;
            btn_Reset_Click(null, null);
        }

        public class NGS_AF_Table
        {
            public string Key { get; }
            public string Value { get; set; }

            public NGS_AF_Table(string k, string v)
            {
                Key = k; Value = v;
            }
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            NGSA = null;
            btn_LoadReads.Enabled = dataGridView1.Enabled = btn_SaveParams.Enabled = true;
            btn_Cancel.Enabled = btn_Export.Enabled = btn_LoadScores.Enabled = false;
            if (txBx_Update.Text != ExportFinishedMessage) txBx_Update.Text = "";
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            bgWork_Export.CancelAsync();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btn_LoadSettings_Click(object sender, EventArgs e)
        {
            OFD.Filter = "xml settings (*.xml)|*.xml|All files (*.*)|*.*";
            DialogResult DR = OFD.ShowDialog();
            if (DR == DialogResult.Cancel) return;

            Pa = NGS_Assist_Params.Load(OFD.FileName);
            RefreshViewer_Post();
            txBx_Update.Text += "Loaded Settings.\r\n";
        }
    }
}