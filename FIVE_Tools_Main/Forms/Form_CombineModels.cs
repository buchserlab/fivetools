﻿using FIVE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main
{
    public partial class Form_CombineModels : Form
    {
        public string SelectedFIVID { get; set; }
        public Form_CombineModels()
        {
            SelectedFIVID = "";
            Init();
        }

        public void Init()
        {
            InitializeComponent();
        }

        private void btn_AzureModelCompile_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Debugger.Break();
            txBx_Update.Text = "Starting .  . ";
            var Args = new Tuple<string, string, string, int>(txBx_Folder.Text, txBx_Name.Text,"", 0);
            bgWrk01.RunWorkerAsync(Args);
        }

        private void Form_CombineModels_Shown(object sender, EventArgs e)
        {
            txBx_Folder.Text = Path.Combine(@"R:\FIVE\EXP\",SelectedFIVID,"2 MLModels\\Azure");
            txBx_Name.Text = SelectedFIVID;
        }

        private void bgWrk01_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (Tuple<string, string, string, int>)e.Argument;
            if (Args.Item4 == 0)
                e.Result = Compile_AzureML_xN_Results.CompileFolder(Args.Item1, Args.Item2, bgWrk01);
            else
                e.Result = Compile_TFml_xN_Results.CompileFolder(Args.Item1, Args.Item2, Args.Item3, bgWrk01);
        }

        private void bgWrk01_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n"+ txBx_Update.Text;
        }

        private void bgWrk01_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = e.Result.ToString();
        }

        private void btn_TF_Compile01_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Starting TF Search . . ";
            var Args = new Tuple<string, string,string, int>(txBx_Folder.Text, txBx_Name.Text, txbx_Folder_2.Text, 1);
            bgWrk01.RunWorkerAsync(Args);
        }

        private void txBx_Folder_TextChanged(object sender, EventArgs e)
        {
            if (txBx_Folder.Text == "") return;
            //Use this to create the name
            Regex R = new Regex(@"(FI\D\d\d\d)\D");
            Match M = R.Match(txBx_Folder.Text);
            if (M.Success) txBx_Name.Text = M.Groups[1].Value;
        }
    }
}
