﻿
namespace FIVE_Tools_Main.Forms
{
    partial class Form_Layouts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Layouts));
            label1 = new System.Windows.Forms.Label();
            txBx_ExpName = new System.Windows.Forms.TextBox();
            txBx_DestinationPlateCount = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            txBx_ExportFolder = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            dataGridViewM = new System.Windows.Forms.DataGridView();
            btn_FillPlates = new System.Windows.Forms.Button();
            pictureBox_Plate1 = new System.Windows.Forms.PictureBox();
            btn_ExportThis = new System.Windows.Forms.Button();
            txBx_RowFirst = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            txBx_ColFirst = new System.Windows.Forms.TextBox();
            label5 = new System.Windows.Forms.Label();
            txBx_ColLast = new System.Windows.Forms.TextBox();
            label6 = new System.Windows.Forms.Label();
            txBx_RowLast = new System.Windows.Forms.TextBox();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            pictureBox_Legend = new System.Windows.Forms.PictureBox();
            pictureBox_Plate2 = new System.Windows.Forms.PictureBox();
            txBx_RanEmpty = new System.Windows.Forms.TextBox();
            label11 = new System.Windows.Forms.Label();
            pictureBox_Plate3 = new System.Windows.Forms.PictureBox();
            txBx_WellsPerAspirate = new System.Windows.Forms.TextBox();
            label12 = new System.Windows.Forms.Label();
            radioButton_Randomize = new System.Windows.Forms.RadioButton();
            radioButton_Ordered = new System.Windows.Forms.RadioButton();
            txBx_FF = new System.Windows.Forms.TextBox();
            label13 = new System.Windows.Forms.Label();
            txBx_ActiveTips = new System.Windows.Forms.TextBox();
            label14 = new System.Windows.Forms.Label();
            txBx_DispenseVolume = new System.Windows.Forms.TextBox();
            label15 = new System.Windows.Forms.Label();
            lbl_96Mote = new System.Windows.Forms.Label();
            lbl_96Full = new System.Windows.Forms.Label();
            lbl_384Full = new System.Windows.Forms.Label();
            txBx_Update = new System.Windows.Forms.TextBox();
            txBx_DispenseVolRand = new System.Windows.Forms.TextBox();
            label16 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            chkBx_AddAntibodyPlate = new System.Windows.Forms.CheckBox();
            txBx_AbPlate_Reps = new System.Windows.Forms.TextBox();
            txBx_AbPlate_StartCol = new System.Windows.Forms.TextBox();
            label18 = new System.Windows.Forms.Label();
            label_Paste_Names = new System.Windows.Forms.Label();
            txBx_DestinationPlateStart = new System.Windows.Forms.TextBox();
            label19 = new System.Windows.Forms.Label();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            txBx_MinSourceVolumeUL = new System.Windows.Forms.TextBox();
            label20 = new System.Windows.Forms.Label();
            txBx_CellsPerWell = new System.Windows.Forms.TextBox();
            label21 = new System.Windows.Forms.Label();
            txBx_SourcePlate_Mapped_Cols = new System.Windows.Forms.TextBox();
            label22 = new System.Windows.Forms.Label();
            txBx_SourcePlate_Actual_Cols = new System.Windows.Forms.TextBox();
            label23 = new System.Windows.Forms.Label();
            txBx_SourcePlate_Mapped_Rows = new System.Windows.Forms.TextBox();
            label24 = new System.Windows.Forms.Label();
            txBx_SourcePlate_Actual_Rows = new System.Windows.Forms.TextBox();
            label25 = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            lbl_Source_48Well = new System.Windows.Forms.Label();
            lbl_Source_24Well = new System.Windows.Forms.Label();
            lbl_Source_12Well = new System.Windows.Forms.Label();
            label27 = new System.Windows.Forms.Label();
            panel1 = new System.Windows.Forms.Panel();
            label28 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)dataGridViewM).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Legend).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate3).BeginInit();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 7);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(61, 15);
            label1.TabIndex = 0;
            label1.Text = "Exp Name";
            // 
            // txBx_ExpName
            // 
            txBx_ExpName.Location = new System.Drawing.Point(15, 26);
            txBx_ExpName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_ExpName.Name = "txBx_ExpName";
            txBx_ExpName.Size = new System.Drawing.Size(65, 23);
            txBx_ExpName.TabIndex = 1;
            txBx_ExpName.Text = "FIV999";
            txBx_ExpName.Leave += txBx_ExpName_Leave;
            // 
            // txBx_DestinationPlateCount
            // 
            txBx_DestinationPlateCount.Location = new System.Drawing.Point(162, 26);
            txBx_DestinationPlateCount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DestinationPlateCount.Name = "txBx_DestinationPlateCount";
            txBx_DestinationPlateCount.Size = new System.Drawing.Size(43, 23);
            txBx_DestinationPlateCount.TabIndex = 3;
            txBx_DestinationPlateCount.Text = "2";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(162, 7);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(40, 15);
            label2.TabIndex = 2;
            label2.Text = "Count";
            toolTip1.SetToolTip(label2, "# of Plates to produce. Usually 2 or 3 replicate plates. The Biomek currently has a max of 4. This does NOT count the Antibody Plate (see below).");
            // 
            // txBx_ExportFolder
            // 
            txBx_ExportFolder.Location = new System.Drawing.Point(15, 102);
            txBx_ExportFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_ExportFolder.Name = "txBx_ExportFolder";
            txBx_ExportFolder.Size = new System.Drawing.Size(366, 23);
            txBx_ExportFolder.TabIndex = 5;
            txBx_ExportFolder.Text = "c:\\temp\\";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(15, 83);
            label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(77, 15);
            label3.TabIndex = 4;
            label3.Text = "Export Folder";
            // 
            // dataGridViewM
            // 
            dataGridViewM.AllowDrop = true;
            dataGridViewM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewM.Location = new System.Drawing.Point(504, 26);
            dataGridViewM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dataGridViewM.Name = "dataGridViewM";
            dataGridViewM.Size = new System.Drawing.Size(534, 607);
            dataGridViewM.TabIndex = 6;
            // 
            // btn_FillPlates
            // 
            btn_FillPlates.Location = new System.Drawing.Point(17, 223);
            btn_FillPlates.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_FillPlates.Name = "btn_FillPlates";
            btn_FillPlates.Size = new System.Drawing.Size(88, 27);
            btn_FillPlates.TabIndex = 7;
            btn_FillPlates.Text = "Fill Plates";
            toolTip1.SetToolTip(btn_FillPlates, "Randomize the sources into the destination wells.  You can press this multiple times until you like the arrangement.");
            btn_FillPlates.UseVisualStyleBackColor = true;
            btn_FillPlates.Click += btn_FillPlates_Click;
            // 
            // pictureBox_Plate1
            // 
            pictureBox_Plate1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pictureBox_Plate1.Location = new System.Drawing.Point(13, 258);
            pictureBox_Plate1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pictureBox_Plate1.Name = "pictureBox_Plate1";
            pictureBox_Plate1.Size = new System.Drawing.Size(208, 129);
            pictureBox_Plate1.TabIndex = 8;
            pictureBox_Plate1.TabStop = false;
            // 
            // btn_ExportThis
            // 
            btn_ExportThis.Location = new System.Drawing.Point(110, 223);
            btn_ExportThis.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExportThis.Name = "btn_ExportThis";
            btn_ExportThis.Size = new System.Drawing.Size(160, 27);
            btn_ExportThis.TabIndex = 9;
            btn_ExportThis.Text = "Export This Arrangement";
            toolTip1.SetToolTip(btn_ExportThis, "Saves out the arrangement by creating a plate map, plating setup, and biomek script.");
            btn_ExportThis.UseVisualStyleBackColor = true;
            btn_ExportThis.Click += btn_ExportThis_Click;
            // 
            // txBx_RowFirst
            // 
            txBx_RowFirst.ForeColor = System.Drawing.Color.DarkRed;
            txBx_RowFirst.Location = new System.Drawing.Point(239, 26);
            txBx_RowFirst.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_RowFirst.Name = "txBx_RowFirst";
            txBx_RowFirst.Size = new System.Drawing.Size(65, 23);
            txBx_RowFirst.TabIndex = 12;
            txBx_RowFirst.Text = "1";
            toolTip1.SetToolTip(txBx_RowFirst, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.ForeColor = System.Drawing.Color.Red;
            label4.Location = new System.Drawing.Point(236, 7);
            label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(55, 15);
            label4.TabIndex = 11;
            label4.Text = "First Row";
            toolTip1.SetToolTip(label4, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // txBx_ColFirst
            // 
            txBx_ColFirst.ForeColor = System.Drawing.Color.ForestGreen;
            txBx_ColFirst.Location = new System.Drawing.Point(316, 26);
            txBx_ColFirst.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_ColFirst.Name = "txBx_ColFirst";
            txBx_ColFirst.Size = new System.Drawing.Size(65, 23);
            txBx_ColFirst.TabIndex = 14;
            txBx_ColFirst.Text = "1";
            toolTip1.SetToolTip(txBx_ColFirst, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.ForeColor = System.Drawing.Color.ForestGreen;
            label5.Location = new System.Drawing.Point(318, 7);
            label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(75, 15);
            label5.TabIndex = 13;
            label5.Text = "First Column";
            toolTip1.SetToolTip(label5, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // txBx_ColLast
            // 
            txBx_ColLast.ForeColor = System.Drawing.Color.ForestGreen;
            txBx_ColLast.Location = new System.Drawing.Point(316, 71);
            txBx_ColLast.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_ColLast.Name = "txBx_ColLast";
            txBx_ColLast.Size = new System.Drawing.Size(65, 23);
            txBx_ColLast.TabIndex = 18;
            txBx_ColLast.Text = "12";
            toolTip1.SetToolTip(txBx_ColLast, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.ForeColor = System.Drawing.Color.ForestGreen;
            label6.Location = new System.Drawing.Point(318, 52);
            label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(74, 15);
            label6.TabIndex = 17;
            label6.Text = "Last Column";
            toolTip1.SetToolTip(label6, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // txBx_RowLast
            // 
            txBx_RowLast.ForeColor = System.Drawing.Color.DarkRed;
            txBx_RowLast.Location = new System.Drawing.Point(239, 71);
            txBx_RowLast.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_RowLast.Name = "txBx_RowLast";
            txBx_RowLast.Size = new System.Drawing.Size(65, 23);
            txBx_RowLast.TabIndex = 16;
            txBx_RowLast.Text = "8";
            toolTip1.SetToolTip(txBx_RowLast, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.ForeColor = System.Drawing.Color.Red;
            label7.Location = new System.Drawing.Point(236, 52);
            label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(54, 15);
            label7.TabIndex = 15;
            label7.Text = "Last Row";
            toolTip1.SetToolTip(label7, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(854, 7);
            label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(63, 15);
            label8.TabIndex = 19;
            label8.Text = "Templates:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.ForeColor = System.Drawing.Color.Blue;
            label9.Location = new System.Drawing.Point(921, 7);
            label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(62, 15);
            label9.TabIndex = 20;
            label9.Text = "FIVE Val 01";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.ForeColor = System.Drawing.Color.Blue;
            label10.Location = new System.Drawing.Point(989, 7);
            label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(40, 15);
            label10.TabIndex = 21;
            label10.Text = "595 01";
            // 
            // pictureBox_Legend
            // 
            pictureBox_Legend.Location = new System.Drawing.Point(232, 270);
            pictureBox_Legend.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pictureBox_Legend.Name = "pictureBox_Legend";
            pictureBox_Legend.Size = new System.Drawing.Size(156, 179);
            pictureBox_Legend.TabIndex = 22;
            pictureBox_Legend.TabStop = false;
            // 
            // pictureBox_Plate2
            // 
            pictureBox_Plate2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pictureBox_Plate2.Location = new System.Drawing.Point(13, 393);
            pictureBox_Plate2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pictureBox_Plate2.Name = "pictureBox_Plate2";
            pictureBox_Plate2.Size = new System.Drawing.Size(208, 124);
            pictureBox_Plate2.TabIndex = 23;
            pictureBox_Plate2.TabStop = false;
            // 
            // txBx_RanEmpty
            // 
            txBx_RanEmpty.Location = new System.Drawing.Point(155, 71);
            txBx_RanEmpty.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_RanEmpty.Name = "txBx_RanEmpty";
            txBx_RanEmpty.Size = new System.Drawing.Size(65, 23);
            txBx_RanEmpty.TabIndex = 25;
            txBx_RanEmpty.Text = "1";
            toolTip1.SetToolTip(txBx_RanEmpty, "Random Empties is used to \"fingerprint\" the plates so that their identity and orientation can be checked. Recommended to be set at 1.");
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(152, 52);
            label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(71, 15);
            label11.TabIndex = 24;
            label11.Text = "Rand Empty";
            toolTip1.SetToolTip(label11, "Random Empties is used to \"fingerprint\" the plates so that their identity and orientation can be checked. Recommended to be set at 1.");
            // 
            // pictureBox_Plate3
            // 
            pictureBox_Plate3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pictureBox_Plate3.Location = new System.Drawing.Point(13, 523);
            pictureBox_Plate3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pictureBox_Plate3.Name = "pictureBox_Plate3";
            pictureBox_Plate3.Size = new System.Drawing.Size(208, 129);
            pictureBox_Plate3.TabIndex = 26;
            pictureBox_Plate3.TabStop = false;
            // 
            // txBx_WellsPerAspirate
            // 
            txBx_WellsPerAspirate.Location = new System.Drawing.Point(169, 149);
            txBx_WellsPerAspirate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_WellsPerAspirate.Name = "txBx_WellsPerAspirate";
            txBx_WellsPerAspirate.Size = new System.Drawing.Size(65, 23);
            txBx_WellsPerAspirate.TabIndex = 28;
            txBx_WellsPerAspirate.Text = "2";
            toolTip1.SetToolTip(txBx_WellsPerAspirate, resources.GetString("txBx_WellsPerAspirate.ToolTip"));
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(162, 132);
            label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(83, 15);
            label12.TabIndex = 27;
            label12.Text = "Wells/Aspirate";
            toolTip1.SetToolTip(label12, resources.GetString("label12.ToolTip"));
            // 
            // radioButton_Randomize
            // 
            radioButton_Randomize.AutoSize = true;
            radioButton_Randomize.Checked = true;
            radioButton_Randomize.Location = new System.Drawing.Point(30, 134);
            radioButton_Randomize.Name = "radioButton_Randomize";
            radioButton_Randomize.Size = new System.Drawing.Size(84, 19);
            radioButton_Randomize.TabIndex = 29;
            radioButton_Randomize.TabStop = true;
            radioButton_Randomize.Text = "Randomize";
            radioButton_Randomize.UseVisualStyleBackColor = true;
            radioButton_Randomize.CheckedChanged += radioButton_Randomize_CheckedChanged;
            // 
            // radioButton_Ordered
            // 
            radioButton_Ordered.AutoSize = true;
            radioButton_Ordered.Location = new System.Drawing.Point(30, 159);
            radioButton_Ordered.Name = "radioButton_Ordered";
            radioButton_Ordered.Size = new System.Drawing.Size(68, 19);
            radioButton_Ordered.TabIndex = 30;
            radioButton_Ordered.Text = "Ordered";
            radioButton_Ordered.UseVisualStyleBackColor = true;
            radioButton_Ordered.CheckedChanged += radioButton_Ordered_CheckedChanged;
            // 
            // txBx_FF
            // 
            txBx_FF.Location = new System.Drawing.Point(255, 149);
            txBx_FF.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_FF.Name = "txBx_FF";
            txBx_FF.Size = new System.Drawing.Size(38, 23);
            txBx_FF.TabIndex = 32;
            txBx_FF.Text = "1.1";
            toolTip1.SetToolTip(txBx_FF, "Multiplier to determine how much extra cell mixture should go into each well");
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(253, 132);
            label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(46, 15);
            label13.TabIndex = 31;
            label13.Text = "FFactor";
            toolTip1.SetToolTip(label13, "Multiplier to determine how much extra cell mixture should go into each well");
            // 
            // txBx_ActiveTips
            // 
            txBx_ActiveTips.Location = new System.Drawing.Point(124, 149);
            txBx_ActiveTips.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_ActiveTips.Name = "txBx_ActiveTips";
            txBx_ActiveTips.Size = new System.Drawing.Size(37, 23);
            txBx_ActiveTips.TabIndex = 34;
            txBx_ActiveTips.Text = "2";
            toolTip1.SetToolTip(txBx_ActiveTips, "# of Tips the Biomek will use. 1 gives the most random arrangement and flexibility, but 2 is twice as fast. ");
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(120, 132);
            label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(28, 15);
            label14.TabIndex = 33;
            label14.Text = "Tips";
            toolTip1.SetToolTip(label14, "# of Tips the Biomek will use. 1 gives the most random arrangement and flexibility, but 2 is twice as fast. ");
            // 
            // txBx_DispenseVolume
            // 
            txBx_DispenseVolume.Location = new System.Drawing.Point(212, 194);
            txBx_DispenseVolume.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DispenseVolume.Name = "txBx_DispenseVolume";
            txBx_DispenseVolume.Size = new System.Drawing.Size(65, 23);
            txBx_DispenseVolume.TabIndex = 36;
            txBx_DispenseVolume.Text = "100";
            toolTip1.SetToolTip(txBx_DispenseVolume, "How much volume to dispense per well.");
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new System.Drawing.Point(199, 177);
            label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(94, 15);
            label15.TabIndex = 35;
            label15.Text = "Dispense Vol (ul)";
            toolTip1.SetToolTip(label15, "How much volume to dispense per well.");
            // 
            // lbl_96Mote
            // 
            lbl_96Mote.AutoSize = true;
            lbl_96Mote.ForeColor = System.Drawing.Color.Blue;
            lbl_96Mote.Location = new System.Drawing.Point(546, 8);
            lbl_96Mote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_96Mote.Name = "lbl_96Mote";
            lbl_96Mote.Size = new System.Drawing.Size(50, 15);
            lbl_96Mote.TabIndex = 37;
            lbl_96Mote.Text = "96 Mote";
            lbl_96Mote.Click += lbl_96Mote_Click;
            // 
            // lbl_96Full
            // 
            lbl_96Full.AutoSize = true;
            lbl_96Full.ForeColor = System.Drawing.Color.Blue;
            lbl_96Full.Location = new System.Drawing.Point(603, 8);
            lbl_96Full.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_96Full.Name = "lbl_96Full";
            lbl_96Full.Size = new System.Drawing.Size(41, 15);
            lbl_96Full.TabIndex = 38;
            lbl_96Full.Text = "96 Full";
            lbl_96Full.Click += lbl_96Full_Click;
            // 
            // lbl_384Full
            // 
            lbl_384Full.AutoSize = true;
            lbl_384Full.ForeColor = System.Drawing.Color.Blue;
            lbl_384Full.Location = new System.Drawing.Point(655, 8);
            lbl_384Full.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_384Full.Name = "lbl_384Full";
            lbl_384Full.Size = new System.Drawing.Size(47, 15);
            lbl_384Full.TabIndex = 39;
            lbl_384Full.Text = "384 Full";
            lbl_384Full.Click += lbl_384Full_Click;
            // 
            // txBx_Update
            // 
            txBx_Update.Location = new System.Drawing.Point(232, 455);
            txBx_Update.Multiline = true;
            txBx_Update.Name = "txBx_Update";
            txBx_Update.Size = new System.Drawing.Size(265, 197);
            txBx_Update.TabIndex = 40;
            // 
            // txBx_DispenseVolRand
            // 
            txBx_DispenseVolRand.Location = new System.Drawing.Point(301, 194);
            txBx_DispenseVolRand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DispenseVolRand.Name = "txBx_DispenseVolRand";
            txBx_DispenseVolRand.Size = new System.Drawing.Size(65, 23);
            txBx_DispenseVolRand.TabIndex = 42;
            txBx_DispenseVolRand.Text = "0";
            toolTip1.SetToolTip(txBx_DispenseVolRand, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(297, 177);
            label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(71, 15);
            label16.TabIndex = 41;
            label16.Text = "+/- Rand uL";
            toolTip1.SetToolTip(label16, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(504, 636);
            label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(297, 15);
            label17.TabIndex = 43;
            label17.Text = "Start Name with _ to only use that in the final (ab) plate";
            // 
            // chkBx_AddAntibodyPlate
            // 
            chkBx_AddAntibodyPlate.AutoSize = true;
            chkBx_AddAntibodyPlate.Location = new System.Drawing.Point(40, 196);
            chkBx_AddAntibodyPlate.Name = "chkBx_AddAntibodyPlate";
            chkBx_AddAntibodyPlate.Size = new System.Drawing.Size(70, 19);
            chkBx_AddAntibodyPlate.TabIndex = 44;
            chkBx_AddAntibodyPlate.Text = "Ab Plate";
            toolTip1.SetToolTip(chkBx_AddAntibodyPlate, resources.GetString("chkBx_AddAntibodyPlate.ToolTip"));
            chkBx_AddAntibodyPlate.UseVisualStyleBackColor = true;
            chkBx_AddAntibodyPlate.CheckedChanged += chkBx_AddAntibodyPlate_CheckedChanged;
            // 
            // txBx_AbPlate_Reps
            // 
            txBx_AbPlate_Reps.Enabled = false;
            txBx_AbPlate_Reps.Location = new System.Drawing.Point(117, 194);
            txBx_AbPlate_Reps.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_AbPlate_Reps.Name = "txBx_AbPlate_Reps";
            txBx_AbPlate_Reps.Size = new System.Drawing.Size(27, 23);
            txBx_AbPlate_Reps.TabIndex = 45;
            txBx_AbPlate_Reps.Text = "4";
            toolTip1.SetToolTip(txBx_AbPlate_Reps, resources.GetString("txBx_AbPlate_Reps.ToolTip"));
            // 
            // txBx_AbPlate_StartCol
            // 
            txBx_AbPlate_StartCol.Enabled = false;
            txBx_AbPlate_StartCol.Location = new System.Drawing.Point(152, 194);
            txBx_AbPlate_StartCol.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_AbPlate_StartCol.Name = "txBx_AbPlate_StartCol";
            txBx_AbPlate_StartCol.Size = new System.Drawing.Size(27, 23);
            txBx_AbPlate_StartCol.TabIndex = 46;
            txBx_AbPlate_StartCol.Text = "2";
            toolTip1.SetToolTip(txBx_AbPlate_StartCol, resources.GetString("txBx_AbPlate_StartCol.ToolTip"));
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(101, 176);
            label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(88, 15);
            label18.TabIndex = 47;
            label18.Text = "Ab Reps / 1 Col";
            toolTip1.SetToolTip(label18, resources.GetString("label18.ToolTip"));
            // 
            // label_Paste_Names
            // 
            label_Paste_Names.AutoSize = true;
            label_Paste_Names.ForeColor = System.Drawing.Color.BlueViolet;
            label_Paste_Names.Location = new System.Drawing.Point(747, 7);
            label_Paste_Names.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label_Paste_Names.Name = "label_Paste_Names";
            label_Paste_Names.Size = new System.Drawing.Size(79, 15);
            label_Paste_Names.TabIndex = 48;
            label_Paste_Names.Text = "PASTE Names";
            toolTip1.SetToolTip(label_Paste_Names, "Paste a list of names, and this will fill in, going across the columns first, then down the rows. Set the source Plate type first so it knows how many columns");
            label_Paste_Names.Click += label_Paste_Names_Click;
            // 
            // txBx_DestinationPlateStart
            // 
            txBx_DestinationPlateStart.Location = new System.Drawing.Point(110, 26);
            txBx_DestinationPlateStart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_DestinationPlateStart.Name = "txBx_DestinationPlateStart";
            txBx_DestinationPlateStart.Size = new System.Drawing.Size(37, 23);
            txBx_DestinationPlateStart.TabIndex = 49;
            txBx_DestinationPlateStart.Text = "1";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Location = new System.Drawing.Point(101, 7);
            label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(60, 15);
            label19.TabIndex = 50;
            label19.Text = "Plate Start";
            toolTip1.SetToolTip(label19, "This is the starting sub-plate number. If you enter FIV999 as Exp, and 1 as starting plate, the first plate made will be called FIV999P1.");
            // 
            // txBx_MinSourceVolumeUL
            // 
            txBx_MinSourceVolumeUL.Location = new System.Drawing.Point(301, 149);
            txBx_MinSourceVolumeUL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_MinSourceVolumeUL.Name = "txBx_MinSourceVolumeUL";
            txBx_MinSourceVolumeUL.Size = new System.Drawing.Size(67, 23);
            txBx_MinSourceVolumeUL.TabIndex = 52;
            txBx_MinSourceVolumeUL.Text = "1200";
            toolTip1.SetToolTip(txBx_MinSourceVolumeUL, "Minimum Volume in the source plate. This will automatically increase the amount so that the tips won't run dry.");
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(301, 132);
            label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(63, 15);
            label20.TabIndex = 51;
            label20.Text = "Min Src uL";
            toolTip1.SetToolTip(label20, "Minimum Volume in the source plate. This will automatically increase the amount so that the tips won't run dry.");
            // 
            // txBx_CellsPerWell
            // 
            txBx_CellsPerWell.Location = new System.Drawing.Point(301, 237);
            txBx_CellsPerWell.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_CellsPerWell.Name = "txBx_CellsPerWell";
            txBx_CellsPerWell.Size = new System.Drawing.Size(65, 23);
            txBx_CellsPerWell.TabIndex = 54;
            txBx_CellsPerWell.Text = "5000";
            toolTip1.SetToolTip(txBx_CellsPerWell, "Changes the # cells per well in the spreadsheet, but it can be changed more once the spreadsheet is opened.");
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new System.Drawing.Point(297, 220);
            label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(78, 15);
            label21.TabIndex = 53;
            label21.Text = "Cells per Well";
            toolTip1.SetToolTip(label21, "Makes the volume of dispense variable. Set to 0 to turn off.");
            // 
            // txBx_SourcePlate_Mapped_Cols
            // 
            txBx_SourcePlate_Mapped_Cols.ForeColor = System.Drawing.Color.ForestGreen;
            txBx_SourcePlate_Mapped_Cols.Location = new System.Drawing.Point(6, 316);
            txBx_SourcePlate_Mapped_Cols.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_SourcePlate_Mapped_Cols.Name = "txBx_SourcePlate_Mapped_Cols";
            txBx_SourcePlate_Mapped_Cols.Size = new System.Drawing.Size(65, 23);
            txBx_SourcePlate_Mapped_Cols.TabIndex = 62;
            txBx_SourcePlate_Mapped_Cols.Text = "12";
            toolTip1.SetToolTip(txBx_SourcePlate_Mapped_Cols, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.ForeColor = System.Drawing.Color.ForestGreen;
            label22.Location = new System.Drawing.Point(8, 297);
            label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(67, 15);
            label22.TabIndex = 61;
            label22.Text = "Virtual Cols";
            toolTip1.SetToolTip(label22, "Mapped within Biomek");
            // 
            // txBx_SourcePlate_Actual_Cols
            // 
            txBx_SourcePlate_Actual_Cols.ForeColor = System.Drawing.Color.DarkRed;
            txBx_SourcePlate_Actual_Cols.Location = new System.Drawing.Point(8, 213);
            txBx_SourcePlate_Actual_Cols.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_SourcePlate_Actual_Cols.Name = "txBx_SourcePlate_Actual_Cols";
            txBx_SourcePlate_Actual_Cols.Size = new System.Drawing.Size(65, 23);
            txBx_SourcePlate_Actual_Cols.TabIndex = 60;
            txBx_SourcePlate_Actual_Cols.Text = "4";
            toolTip1.SetToolTip(txBx_SourcePlate_Actual_Cols, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.ForeColor = System.Drawing.Color.Red;
            label23.Location = new System.Drawing.Point(5, 194);
            label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(76, 15);
            label23.TabIndex = 59;
            label23.Text = "Physical Cols";
            toolTip1.SetToolTip(label23, "Source # Cols");
            // 
            // txBx_SourcePlate_Mapped_Rows
            // 
            txBx_SourcePlate_Mapped_Rows.ForeColor = System.Drawing.Color.ForestGreen;
            txBx_SourcePlate_Mapped_Rows.Location = new System.Drawing.Point(6, 271);
            txBx_SourcePlate_Mapped_Rows.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_SourcePlate_Mapped_Rows.Name = "txBx_SourcePlate_Mapped_Rows";
            txBx_SourcePlate_Mapped_Rows.Size = new System.Drawing.Size(65, 23);
            txBx_SourcePlate_Mapped_Rows.TabIndex = 58;
            txBx_SourcePlate_Mapped_Rows.Text = "8";
            toolTip1.SetToolTip(txBx_SourcePlate_Mapped_Rows, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.ForeColor = System.Drawing.Color.ForestGreen;
            label24.Location = new System.Drawing.Point(8, 252);
            label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(72, 15);
            label24.TabIndex = 57;
            label24.Text = "Virtual Rows";
            toolTip1.SetToolTip(label24, "Mapped within BioMek");
            // 
            // txBx_SourcePlate_Actual_Rows
            // 
            txBx_SourcePlate_Actual_Rows.ForeColor = System.Drawing.Color.DarkRed;
            txBx_SourcePlate_Actual_Rows.Location = new System.Drawing.Point(8, 168);
            txBx_SourcePlate_Actual_Rows.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_SourcePlate_Actual_Rows.Name = "txBx_SourcePlate_Actual_Rows";
            txBx_SourcePlate_Actual_Rows.Size = new System.Drawing.Size(65, 23);
            txBx_SourcePlate_Actual_Rows.TabIndex = 56;
            txBx_SourcePlate_Actual_Rows.Text = "3";
            toolTip1.SetToolTip(txBx_SourcePlate_Actual_Rows, "Which wells to use in the plate. Usually the whole plate or a 1-well mote is used.");
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.ForeColor = System.Drawing.Color.Red;
            label25.Location = new System.Drawing.Point(5, 149);
            label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(81, 15);
            label25.TabIndex = 55;
            label25.Text = "Physical Rows";
            toolTip1.SetToolTip(label25, "Source # Rows");
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Location = new System.Drawing.Point(11, 3);
            label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(72, 30);
            label26.TabIndex = 63;
            label26.Text = "Source Plate\r\n(Advanced)";
            toolTip1.SetToolTip(label26, resources.GetString("label26.ToolTip"));
            // 
            // lbl_Source_48Well
            // 
            lbl_Source_48Well.AutoSize = true;
            lbl_Source_48Well.ForeColor = System.Drawing.Color.Blue;
            lbl_Source_48Well.Location = new System.Drawing.Point(40, 109);
            lbl_Source_48Well.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_Source_48Well.Name = "lbl_Source_48Well";
            lbl_Source_48Well.Size = new System.Drawing.Size(45, 15);
            lbl_Source_48Well.TabIndex = 66;
            lbl_Source_48Well.Text = "48 Well";
            toolTip1.SetToolTip(lbl_Source_48Well, "Set Source to this Plate");
            lbl_Source_48Well.Click += lbl_Source_PlateLink_Click;
            // 
            // lbl_Source_24Well
            // 
            lbl_Source_24Well.AutoSize = true;
            lbl_Source_24Well.ForeColor = System.Drawing.Color.Blue;
            lbl_Source_24Well.Location = new System.Drawing.Point(40, 83);
            lbl_Source_24Well.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_Source_24Well.Name = "lbl_Source_24Well";
            lbl_Source_24Well.Size = new System.Drawing.Size(45, 15);
            lbl_Source_24Well.TabIndex = 65;
            lbl_Source_24Well.Text = "24 Well";
            toolTip1.SetToolTip(lbl_Source_24Well, "Set Source to this Plate");
            lbl_Source_24Well.Click += lbl_Source_PlateLink_Click;
            // 
            // lbl_Source_12Well
            // 
            lbl_Source_12Well.AutoSize = true;
            lbl_Source_12Well.ForeColor = System.Drawing.Color.Blue;
            lbl_Source_12Well.Location = new System.Drawing.Point(40, 56);
            lbl_Source_12Well.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lbl_Source_12Well.Name = "lbl_Source_12Well";
            lbl_Source_12Well.Size = new System.Drawing.Size(45, 15);
            lbl_Source_12Well.TabIndex = 64;
            lbl_Source_12Well.Text = "12 Well";
            toolTip1.SetToolTip(lbl_Source_12Well, "Set Source to this Plate");
            lbl_Source_12Well.Click += lbl_Source_PlateLink_Click;
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label27.Location = new System.Drawing.Point(2, 368);
            label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label27.Name = "label27";
            label27.Size = new System.Drawing.Size(96, 60);
            label27.TabIndex = 67;
            label27.Text = "12 Well source can\r\nhold more volume \r\nand is treated intern-\r\nally (virtually) as a \r\n96-well plate.";
            toolTip1.SetToolTip(label27, resources.GetString("label27.ToolTip"));
            // 
            // panel1
            // 
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(label27);
            panel1.Controls.Add(lbl_Source_48Well);
            panel1.Controls.Add(lbl_Source_24Well);
            panel1.Controls.Add(lbl_Source_12Well);
            panel1.Controls.Add(label26);
            panel1.Controls.Add(txBx_SourcePlate_Actual_Rows);
            panel1.Controls.Add(txBx_SourcePlate_Mapped_Cols);
            panel1.Controls.Add(label25);
            panel1.Controls.Add(label22);
            panel1.Controls.Add(label24);
            panel1.Controls.Add(txBx_SourcePlate_Actual_Cols);
            panel1.Controls.Add(txBx_SourcePlate_Mapped_Rows);
            panel1.Controls.Add(label23);
            panel1.Location = new System.Drawing.Point(395, 7);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(102, 442);
            panel1.TabIndex = 63;
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Location = new System.Drawing.Point(510, 8);
            label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(33, 15);
            label28.TabIndex = 64;
            label28.Text = "Dest:";
            // 
            // Form_Layouts
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1047, 660);
            Controls.Add(label28);
            Controls.Add(panel1);
            Controls.Add(txBx_CellsPerWell);
            Controls.Add(label21);
            Controls.Add(txBx_MinSourceVolumeUL);
            Controls.Add(label20);
            Controls.Add(label19);
            Controls.Add(txBx_DestinationPlateStart);
            Controls.Add(label_Paste_Names);
            Controls.Add(label18);
            Controls.Add(txBx_AbPlate_StartCol);
            Controls.Add(txBx_AbPlate_Reps);
            Controls.Add(chkBx_AddAntibodyPlate);
            Controls.Add(label17);
            Controls.Add(txBx_DispenseVolRand);
            Controls.Add(label16);
            Controls.Add(txBx_Update);
            Controls.Add(lbl_384Full);
            Controls.Add(lbl_96Full);
            Controls.Add(lbl_96Mote);
            Controls.Add(txBx_DispenseVolume);
            Controls.Add(label15);
            Controls.Add(txBx_ActiveTips);
            Controls.Add(label14);
            Controls.Add(txBx_FF);
            Controls.Add(label13);
            Controls.Add(radioButton_Ordered);
            Controls.Add(radioButton_Randomize);
            Controls.Add(txBx_WellsPerAspirate);
            Controls.Add(label12);
            Controls.Add(pictureBox_Plate3);
            Controls.Add(txBx_RanEmpty);
            Controls.Add(label11);
            Controls.Add(pictureBox_Plate2);
            Controls.Add(pictureBox_Legend);
            Controls.Add(label10);
            Controls.Add(label9);
            Controls.Add(label8);
            Controls.Add(txBx_ColLast);
            Controls.Add(label6);
            Controls.Add(txBx_RowLast);
            Controls.Add(label7);
            Controls.Add(txBx_ColFirst);
            Controls.Add(label5);
            Controls.Add(txBx_RowFirst);
            Controls.Add(label4);
            Controls.Add(btn_ExportThis);
            Controls.Add(pictureBox_Plate1);
            Controls.Add(btn_FillPlates);
            Controls.Add(dataGridViewM);
            Controls.Add(txBx_ExportFolder);
            Controls.Add(label3);
            Controls.Add(txBx_DestinationPlateCount);
            Controls.Add(label2);
            Controls.Add(txBx_ExpName);
            Controls.Add(label1);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "Form_Layouts";
            Text = "FIV Layouts";
            FormClosing += Form_Layouts_FormClosing;
            ((System.ComponentModel.ISupportInitialize)dataGridViewM).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Legend).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Plate3).EndInit();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txBx_ExpName;
        private System.Windows.Forms.TextBox txBx_DestinationPlateCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBx_ExportFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridViewM;
        private System.Windows.Forms.Button btn_FillPlates;
        private System.Windows.Forms.PictureBox pictureBox_Plate1;
        private System.Windows.Forms.Button btn_ExportThis;
        private System.Windows.Forms.TextBox txBx_RowFirst;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txBx_ColFirst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txBx_ColLast;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_RowLast;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox_Legend;
        private System.Windows.Forms.PictureBox pictureBox_Plate2;
        private System.Windows.Forms.TextBox txBx_RanEmpty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox_Plate3;
        private System.Windows.Forms.TextBox txBx_WellsPerAspirate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton radioButton_Randomize;
        private System.Windows.Forms.RadioButton radioButton_Ordered;
        private System.Windows.Forms.TextBox txBx_FF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txBx_ActiveTips;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txBx_DispenseVolume;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbl_96Mote;
        private System.Windows.Forms.Label lbl_96Full;
        private System.Windows.Forms.Label lbl_384Full;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.TextBox txBx_DispenseVolRand;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkBx_AddAntibodyPlate;
        private System.Windows.Forms.TextBox txBx_AbPlate_Reps;
        private System.Windows.Forms.TextBox txBx_AbPlate_StartCol;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label_Paste_Names;
        private System.Windows.Forms.TextBox txBx_DestinationPlateStart;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txBx_MinSourceVolumeUL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txBx_CellsPerWell;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txBx_SourcePlate_Mapped_Cols;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txBx_SourcePlate_Actual_Cols;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txBx_SourcePlate_Mapped_Rows;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txBx_SourcePlate_Actual_Rows;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lbl_Source_48Well;
        private System.Windows.Forms.Label lbl_Source_24Well;
        private System.Windows.Forms.Label lbl_Source_12Well;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
    }
}