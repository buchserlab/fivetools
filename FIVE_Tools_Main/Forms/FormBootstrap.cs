﻿using FIVE.gRNA_Bootstrap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main
{
    public partial class FormBootstrap : Form
    {
        public FormBootstrap()
        {
            InitializeComponent();
        }

        private void FormBootstrap_Load(object sender, EventArgs e)
        {
            coBo_Function.Items.Clear();
            System.Reflection.MethodInfo[] props = typeof(GBS_Gene).GetMethods();
            IEnumerable<object> ret = props.Where(x => x.ReturnType.Name == "Double").Select(x => (object)x.Name);
            ret = ret.Where(x => (string)x != "get_Item");
            coBo_Function.Items.AddRange(ret.ToArray());
            coBo_Function.SelectedIndex = 0;
        }

        private void btn_LoadColumns_Click(object sender, EventArgs e)
        {
            var FI = new FileInfo(txBx_InputFile.Text);
            if (!FI.Exists)
            {
                txBx_Update.Text = "Couldn't find the file";
                return;
            }
            string ext = FI.Extension.ToUpper();
            char delim = ext == ".TXT" ? '\t' : ',';
            using (var SR = new StreamReader(FI.FullName))
            {
                string header = SR.ReadLine();
                string[] hArr = header.Split(delim);

                SetupComboAndDefault(coBo_Gene, hArr, new string[1] { "gene" });
                SetupComboAndDefault(coBo_gRNA, hArr, new string[2] { "rName", "gRNA" });
                SetupComboAndDefault(coBo_Filter1, hArr, new string[1] { "Min(Reads)" });
                SetupComboAndDefault(coBo_Value, hArr, new string[2] { "LFC Dox No Dox", "LFC Dox vs No Dox" });
                listBox_Splits.Items.Clear(); listBox_Splits.Items.AddRange(hArr);
            }
        }

        public void SetupComboAndDefault(ComboBox CB, string[] Items, string[] DefaultItems)
        {
            CB.Items.Clear();
            CB.Items.AddRange(Items);
            var Srch = new HashSet<string>(DefaultItems.Select(x => x.ToUpper()));
            for (int i = 0; i < CB.Items.Count; i++)
            {
                string Itm = CB.Items[i].ToString().ToUpper();
                if (Srch.Contains(Itm))
                {
                    CB.SelectedIndex = i;
                    return;
                }
            }
        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "txt files (*.txt)|*.txt|txt files (*.csv)|*.csv|All files (*.*)|*.*";
            OFD.Title = "Please select a .txt or .csv file to generate p-values for.";
            OFD.RestoreDirectory = true;
            DialogResult DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                txBx_InputFile.Text = OFD.FileName;
            }
        }

        private void bgWrk_BootstrapP_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] Args = (object[])e.Argument;
            gRNA_Bootstrap_Main.Bootstrap((string)Args[0], (double)Args[1], (int)Args[2], bgWrk_BootstrapP);
            bgWrk_BootstrapP.ReportProgress(100,"Exporting . . ");
            gRNA_Bootstrap_Main.Export();
        }

        private void bgWrk_BootstrapP_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState.ToString() + "\r\n" + txBx_Update.Text;
        }

        private void bgWrk_BootstrapP_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = "Finished";
        }

        private void btn_RunStats_Click(object sender, EventArgs e)
        {
            //First setup the columns
            var Columns = new GBS_Columns();
            Columns.Add(new GBS_Column(ColumnTypes.Gene, coBo_Gene));
            Columns.Add(new GBS_Column(ColumnTypes.gRNA, coBo_gRNA));
            Columns.Add(new GBS_Column(ColumnTypes.Value, coBo_Value));
            Columns.Add(new GBS_Column(ColumnTypes.Filter, coBo_Filter1, txBx_Filter1.Text));
            //Columns.Add(new GBS_Column(ColumnTypes.Filter, coBo_Filter2, txBx_Filter2.Text));
            GBS_Columns.SplitsFromList(listBox_Splits, Columns);

            string Note = gRNA_Bootstrap_Main.Load(Columns, txBx_InputFile.Text);
            if (Note != "")
            {
                txBx_Update.Text = "Error: " + Note; return;
            }
            txBx_Update.Text = gRNA_Bootstrap_Main.Sets.ErrorReport();

            double param = double.Parse(txBx_FunctionParam.Text); //Trying to get the best two
            int reps = int.Parse(txBx_BootstrapReps.Text);
            object[] Args = new object[3] { coBo_Function.Text, param, reps };
            bgWrk_BootstrapP.RunWorkerAsync(Args);
        }
    }
}
