﻿
namespace FIVE_Tools_Main.Forms
{
    partial class Form_PreCheckRaft
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_Poster = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txBx_ExportName = new System.Windows.Forms.TextBox();
            this.btn_All = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txBx_WellsCount = new System.Windows.Forms.TextBox();
            this.chkBx_AppendAlreadyPicked = new System.Windows.Forms.CheckBox();
            this.chkBx_AddRinseWell = new System.Windows.Forms.CheckBox();
            this.chkBx_Randomize = new System.Windows.Forms.CheckBox();
            this.txBx_List = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_First = new System.Windows.Forms.Button();
            this.flowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu01 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_Poster);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.txBx_ExportName);
            this.splitContainer1.Panel1.Controls.Add(this.btn_All);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.txBx_WellsCount);
            this.splitContainer1.Panel1.Controls.Add(this.chkBx_AppendAlreadyPicked);
            this.splitContainer1.Panel1.Controls.Add(this.chkBx_AddRinseWell);
            this.splitContainer1.Panel1.Controls.Add(this.chkBx_Randomize);
            this.splitContainer1.Panel1.Controls.Add(this.txBx_List);
            this.splitContainer1.Panel1.Controls.Add(this.txBx_Update);
            this.splitContainer1.Panel1.Controls.Add(this.btn_First);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flowLayout);
            this.splitContainer1.Size = new System.Drawing.Size(1409, 714);
            this.splitContainer1.SplitterDistance = 196;
            this.splitContainer1.TabIndex = 3;
            // 
            // btn_Poster
            // 
            this.btn_Poster.Location = new System.Drawing.Point(10, 679);
            this.btn_Poster.Name = "btn_Poster";
            this.btn_Poster.Size = new System.Drawing.Size(75, 23);
            this.btn_Poster.TabIndex = 13;
            this.btn_Poster.Text = "Poster";
            this.btn_Poster.UseVisualStyleBackColor = true;
            this.btn_Poster.Click += new System.EventHandler(this.btn_Poster_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 521);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "List Name";
            // 
            // txBx_ExportName
            // 
            this.txBx_ExportName.Location = new System.Drawing.Point(10, 537);
            this.txBx_ExportName.Name = "txBx_ExportName";
            this.txBx_ExportName.Size = new System.Drawing.Size(173, 20);
            this.txBx_ExportName.TabIndex = 11;
            // 
            // btn_All
            // 
            this.btn_All.Enabled = false;
            this.btn_All.Location = new System.Drawing.Point(108, 563);
            this.btn_All.Name = "btn_All";
            this.btn_All.Size = new System.Drawing.Size(75, 23);
            this.btn_All.TabIndex = 10;
            this.btn_All.Text = "All";
            this.btn_All.UseVisualStyleBackColor = true;
            this.btn_All.Click += new System.EventHandler(this.btn_All_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(121, 593);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Wells Pick";
            // 
            // txBx_WellsCount
            // 
            this.txBx_WellsCount.Location = new System.Drawing.Point(124, 612);
            this.txBx_WellsCount.Name = "txBx_WellsCount";
            this.txBx_WellsCount.Size = new System.Drawing.Size(49, 20);
            this.txBx_WellsCount.TabIndex = 7;
            this.txBx_WellsCount.TextChanged += new System.EventHandler(this.txBx_WellsCount_TextChanged);
            // 
            // chkBx_AppendAlreadyPicked
            // 
            this.chkBx_AppendAlreadyPicked.AutoSize = true;
            this.chkBx_AppendAlreadyPicked.Location = new System.Drawing.Point(10, 638);
            this.chkBx_AppendAlreadyPicked.Name = "chkBx_AppendAlreadyPicked";
            this.chkBx_AppendAlreadyPicked.Size = new System.Drawing.Size(137, 17);
            this.chkBx_AppendAlreadyPicked.TabIndex = 6;
            this.chkBx_AppendAlreadyPicked.Text = "Append Already Picked";
            this.chkBx_AppendAlreadyPicked.UseVisualStyleBackColor = true;
            this.chkBx_AppendAlreadyPicked.CheckedChanged += new System.EventHandler(this.chkBx_CheckedChanged);
            // 
            // chkBx_AddRinseWell
            // 
            this.chkBx_AddRinseWell.AutoSize = true;
            this.chkBx_AddRinseWell.Location = new System.Drawing.Point(10, 615);
            this.chkBx_AddRinseWell.Name = "chkBx_AddRinseWell";
            this.chkBx_AddRinseWell.Size = new System.Drawing.Size(77, 17);
            this.chkBx_AddRinseWell.TabIndex = 5;
            this.chkBx_AddRinseWell.Text = "Rinse Well";
            this.chkBx_AddRinseWell.UseVisualStyleBackColor = true;
            this.chkBx_AddRinseWell.CheckedChanged += new System.EventHandler(this.chkBx_CheckedChanged);
            // 
            // chkBx_Randomize
            // 
            this.chkBx_Randomize.AutoSize = true;
            this.chkBx_Randomize.Location = new System.Drawing.Point(10, 592);
            this.chkBx_Randomize.Name = "chkBx_Randomize";
            this.chkBx_Randomize.Size = new System.Drawing.Size(79, 17);
            this.chkBx_Randomize.TabIndex = 4;
            this.chkBx_Randomize.Text = "Randomize";
            this.chkBx_Randomize.UseVisualStyleBackColor = true;
            this.chkBx_Randomize.CheckedChanged += new System.EventHandler(this.chkBx_CheckedChanged);
            // 
            // txBx_List
            // 
            this.txBx_List.Location = new System.Drawing.Point(10, 77);
            this.txBx_List.Multiline = true;
            this.txBx_List.Name = "txBx_List";
            this.txBx_List.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txBx_List.Size = new System.Drawing.Size(173, 429);
            this.txBx_List.TabIndex = 2;
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(10, 12);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(173, 59);
            this.txBx_Update.TabIndex = 1;
            // 
            // btn_First
            // 
            this.btn_First.Location = new System.Drawing.Point(10, 563);
            this.btn_First.Name = "btn_First";
            this.btn_First.Size = new System.Drawing.Size(75, 23);
            this.btn_First.TabIndex = 0;
            this.btn_First.Text = "First";
            this.btn_First.UseVisualStyleBackColor = true;
            this.btn_First.Click += new System.EventHandler(this.btn_First_Click);
            // 
            // flowLayout
            // 
            this.flowLayout.AutoScroll = true;
            this.flowLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayout.Location = new System.Drawing.Point(0, 0);
            this.flowLayout.Name = "flowLayout";
            this.flowLayout.Size = new System.Drawing.Size(1209, 714);
            this.flowLayout.TabIndex = 3;
            // 
            // contextMenu01
            // 
            this.contextMenu01.Name = "contextMenu01";
            this.contextMenu01.Size = new System.Drawing.Size(61, 4);
            // 
            // Form_PreCheckRaft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 714);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_PreCheckRaft";
            this.Text = "PreCheckRaftForm";
            this.Load += new System.EventHandler(this.Form_PreCheckRaft_Load);
            this.Shown += new System.EventHandler(this.Form_PreCheckRaft_Shown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btn_First;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.TextBox txBx_List;
        private System.Windows.Forms.FlowLayoutPanel flowLayout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txBx_WellsCount;
        private System.Windows.Forms.CheckBox chkBx_AppendAlreadyPicked;
        private System.Windows.Forms.CheckBox chkBx_AddRinseWell;
        private System.Windows.Forms.CheckBox chkBx_Randomize;
        private System.Windows.Forms.Button btn_All;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBx_ExportName;
        private System.Windows.Forms.Button btn_Poster;
        private System.Windows.Forms.ContextMenuStrip contextMenu01;
    }
}