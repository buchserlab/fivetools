﻿using DocumentFormat.OpenXml.Drawing.Diagrams;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Wordprocessing;
using FIVE.TF;
using FIVE_IMG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main.Forms
{
    public partial class Form_MLSettings : Form
    {
        public Store_Validation_Parameters SVP;

        #region Init and Load 

        public Form_MLSettings()
        {
            InitializeComponent();
        }

        public Form_MLSettings(Store_Validation_Parameters SVPin)
        {
            SVP = SVPin.Copy();
            InitializeComponent();
            LoadSave();
        }

        public void LoadSave(bool Save = false)
        {
            if (!Save)
            {
                //Check to see if the settings are correct starting points
                if (SVP.ModelInferSettings.svImageExt == null || SVP.ModelInferSettings.svImageExt == "") SVP.ModelInferSettings.svImageExt = SVP.CropSettings.SaveTypeExtension_NoDot;
                if (SVP.ModelInferSettings.svImageFolder == null || SVP.ModelInferSettings.svImageFolder == "") SVP.ModelInferSettings.svImageFolder = SVP.CropSettings.SaveFolder;
                if (SVP.ModelInferSettings.SaveMeasuresFolder == null || SVP.ModelInferSettings.SaveMeasuresFolder == "") SVP.ModelInferSettings.SaveMeasuresFolder = SVP.HCS_Image_DestinationFolder;
                txBx_ClassificationModels.Text = "";
                txBx_MeasurementModels.Text = "";
            }

            var props = typeof(ModelInferSettings).GetProperties();
            foreach (var field in props)
            {
                string key = "mls_" + field.Name;
                if (this.Controls.ContainsKey(key))
                {
                    //Debug.Print(key);
                    //mls_SaveMeasuresFolder
                    try
                    {
                        object val = field.GetValue(SVP.ModelInferSettings);
                        string CtrlType = Controls[key].GetType().Name.ToUpper();
                        if (CtrlType == "CHECKBOX")
                        {
                            var checkBox = ((System.Windows.Forms.CheckBox)Controls[key]);
                            if (Save)
                                field.SetValue(SVP.ModelInferSettings, checkBox.Checked);
                            else
                                checkBox.Checked = (bool)val;
                        }
                        else if (CtrlType == "TEXTBOX")
                        {
                            if (Save)
                            {
                                string BoxVal = Controls[key].Text;
                                string fType = field.PropertyType.Name.ToUpper();
                                if (fType == "STRING")
                                    field.SetValue(SVP.ModelInferSettings, BoxVal);
                                else if (fType == "INT")
                                    field.SetValue(SVP.ModelInferSettings, int.Parse(BoxVal));
                                else if (fType == "BYTE")
                                    field.SetValue(SVP.ModelInferSettings, byte.Parse(BoxVal));
                                else
                                {

                                }
                            }
                            else
                            {
                                if (val == null) val = "";
                                Controls[key].Text = val.ToString();
                            }
                        }
                        else
                        {

                        }
                    }
                    catch
                    {

                    }
                }
            }

            if (Save)
            {
                SVP.ModelInferSettings.ModelPaths_Classification_Multi = List_Classify.Values.Select(x => x.ModelPathFull).ToList();
                SVP.ModelInferSettings.ModelPaths_Measure_Multi = List_Measure.Values.Select(x => x.ModelPathFull).ToList();
            }
            else
            {
                LoadModelList(SVP.ModelInferSettings.ModelPaths_Classification_Multi, 0);
                LoadModelList(SVP.ModelInferSettings.ModelPaths_Measure_Multi, 1);
            }

            mls_PerformClassifications_CheckedChanged(null, null);
            mls_PerformMeasures_CheckedChanged(null, null);
            txBx_Update.Text = Save ? "Saved." : "Loaded.";
        }

        private void btn_LoadSettings_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.Title = "Load ModelInferSettings"; OFD.Filter = "xml files (*.xml)|";
            var Res = OFD.ShowDialog();
            if (Res == DialogResult.OK)
            {
                var MIS = ModelInferSettings.Load(OFD.FileName);
            }
        }

        private void btn_SaveSettings_Click(object sender, EventArgs e)
        {
            //TODO: The list of models isn't saving since it is classes and not just text
            LoadSave(true);

            var SFD = new SaveFileDialog();
            SFD.Title = "Save ModelInferSettings As . ."; SFD.Filter = "XML Files (*.xml)|";
            var Res = SFD.ShowDialog();
            if (Res == DialogResult.OK)
            {
                SVP.ModelInferSettings.Save(SFD.FileName);
            }
            txBx_Update.Text = "Saved Settings";
        }

        #endregion

        #region Model Load Functions - ------------------------------------------

        private Dictionary<string, TF_Image_Model> List_Classify = new();
        private Dictionary<string, TF_Image_Model> List_Measure = new();
        private List<TF_Image_Model> SelectedModel = new List<TF_Image_Model>(2) { null, null };
        internal List<string> ActiveAnnotationsList;
        public const int ModelKeyNameLength = 42;

        private void UpdateModelLists()
        {
            txBx_ClassificationModels.Text = string.Join("\r\n", List_Classify.Keys);
            txBx_MeasurementModels.Text = string.Join("\r\n", List_Measure.Keys);
        }

        private void Update_Model_Detail(string ModelName, int List0_Classify_1_Measure)
        {
            var dict = List0_Classify_1_Measure == 0 ? List_Classify : List_Measure;
            var lbl = List0_Classify_1_Measure == 0 ? lbl_Detail_Class : lbl_Detail_Measure;
            if (dict.ContainsKey(ModelName))
            {
                var M = dict[ModelName];
                SelectedModel[List0_Classify_1_Measure] = M;
                lbl.Text = M.ModelName;
            }
        }

        private string AddModelToList(TF_Image_Model tFIM, int List0_Classify_1_Measure)
        {
            var dict = List0_Classify_1_Measure == 0 ? List_Classify : List_Measure;

            string mName = tFIM.ModelName;
            if (mName.Length > ModelKeyNameLength) mName = mName.Substring(0, ModelKeyNameLength);
            if (dict.ContainsKey(mName) && dict[mName].ModelPath == tFIM.ModelPath) return "Already added"; //Already added this one

            while (dict.ContainsKey(mName))
            {
                mName = tFIM.ModelName.Substring(0, mName.Length + 3);
                if (mName.Length >= tFIM.ModelName.Length) mName += "(2)";
            }
            if (!dict.ContainsKey(mName))
            {
                dict.Add(mName, tFIM);
            }
            UpdateModelLists();
            return "";
        }

        private string RemoveModelFromList(string ModelName, int List0_Classify_1_Measure)
        {
            var dict = List0_Classify_1_Measure == 0 ? List_Classify : List_Measure;
            if (dict.ContainsKey(ModelName))
            {
                dict.Remove(ModelName);
            }
            else return "Key not found";

            UpdateModelLists();
            return "";
        }

        private TF_Image_Model TryModelFromName(string PathToModel)
        {
            if (!File.Exists(PathToModel))
                (bool t, PathToModel) = TF_Image_Model.GetModelFile_FromFolder(PathToModel);

            if (!File.Exists(PathToModel)) return null;

            var TFIM = TF_Image_Model.LoadFromFrozen(PathToModel);
            return TFIM;
        }

        #endregion

        #region Model Preparation ---------------------------------------------------

        private void lbl_Classifications_ChooseModel_Click(object sender, EventArgs e)
        {
            string res = SelectModelFile("Select Classification File", "s:\\Raft\\"); if (res == "") return;
            mls_ModelPath_Classification.Text = res;
        }

        private void lbl_Measures_ChooseModel_Click(object sender, EventArgs e)
        {
            string res = SelectModelFile("Select Measurement File", "s:\\Phys\\"); if (res == "") return;
            mls_ModelPath_Measure.Text = res;
        }

        private string SelectModelFile(string Note, string InitDir)
        {
            var OFD = new OpenFileDialog();
            OFD.Title = Note;
            if (InitDir != "") OFD.InitialDirectory = InitDir;
            var res = OFD.ShowDialog();
            if (res == DialogResult.OK)
            {
                return OFD.FileName;
            }
            else
            {
                return "";
            }
        }

        private void btn_Classifications_AddSingle_Click(object sender, EventArgs e)
        {
            AddSingle(mls_ModelPath_Classification, 0);
        }


        private void btn_Measures_AddSingle_Click(object sender, EventArgs e)
        {
            AddSingle(mls_ModelPath_Measure, 1);
        }

        public void AddSingle(TextBox txInterest, int List0_Classify_1_Measure)
        {
            (bool success, string res, var TFIM) = LoadSingle(txInterest.Text, List0_Classify_1_Measure);

            if (!success)
            {
                txBx_Update.Text = res;
                return;
            }
            if (res == "") txInterest.Text = "";
            else txBx_Update.Text = res;
        }

        public void RemoveSingle(TextBox txMulti, string ModelName, int List0_Classify_1_Measure)
        {
            var Res = RemoveModelFromList(ModelName, List0_Classify_1_Measure);

            if (Res != "")
            {
                txBx_Update.Text = Res;
                return;
            }
        }

        public (bool Success, string Result, TF_Image_Model TFIM) LoadSingle(string LoadPath, int List0_Classify_1_Measure)
        {
            var TFIM = TryModelFromName(LoadPath);
            if (TFIM == null) return (false, "Problem loading this model: " + LoadPath, TFIM);
            var Res = AddModelToList(TFIM, List0_Classify_1_Measure);
            return (true, Res, TFIM);
        }

        public void LoadModelList(List<string> ModelPaths, int List0_Classify_1_Measure)
        {
            txBx_Update.Text = "Loading from Paths . . ";
            foreach (var path in ModelPaths)
            {
                (bool success, string res, var TFIM) = LoadSingle(path, List0_Classify_1_Measure);
                if (!success)
                {
                    txBx_Update.Text += "\r\n" + res;
                }
            }
        }

        private void btn_Classifications_AddFolder_Click(object sender, EventArgs e)
        {
            AddFolder(0);
        }

        private void btn_Measures_AddFolder_Click(object sender, EventArgs e)
        {
            AddFolder(1);
        }

        public void AddFolder(int List0_Classify_1_Measure)
        {
            var txFolderStart = List0_Classify_1_Measure == 0 ? mls_ModelPath_Classification : mls_ModelPath_Measure;
            string Fldr = txFolderStart.Text.ToUpper().Trim();
            if (Fldr == "")
            {
                Fldr = SelectModelFile("Select a model file, this will then search and pull up all sibling models.", "s:\\");
                return;
            }
            if (!Directory.Exists(Fldr) && !File.Exists(Fldr))
            {
                txBx_Update.Text = Fldr + " - couldn't find this . . ";
                return;
            }

            // Check to see whether Fldr contains a folder or a file. 
            if (File.Exists(Fldr))
            {
                var parentDir = Directory.GetParent(Fldr).Parent;
                if (parentDir != null)
                {
                    Fldr = parentDir.FullName;
                }
            }

            // Now, run through this folder, looking one directory deep for a .pb file, and make a list of those files
            var pbFiles = new List<string>();
            var directoryInfo = new DirectoryInfo(Fldr);
            //pbFiles.AddRange(directoryInfo.GetFiles("*.pb", SearchOption.TopDirectoryOnly).Select(f => f.FullName));

            foreach (var subDir in directoryInfo.GetDirectories())
            {
                var tFile = subDir.GetFiles("*.pb", SearchOption.TopDirectoryOnly).Select(f => f.FullName);
                if (tFile.Count() == 0)
                    tFile = subDir.GetDirectories().First().GetFiles("*.pb", SearchOption.TopDirectoryOnly).Select(f => f.FullName);
                pbFiles.AddRange(tFile);
            }

            //Now add those to the list
            foreach (var file in pbFiles)
            {
                var TFIM = TryModelFromName(file);
                if (TFIM == null) txBx_Update.Text += file + "/r/n";
                var Res = AddModelToList(TFIM, List0_Classify_1_Measure);
            }
        }

        #endregion

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Ignore;
            this.Hide();
        }

        private void btn_SaveClose_Click(object sender, EventArgs e)
        {
            LoadSave(true);
            DialogResult = DialogResult.Continue;
            this.Hide();
        }

        private void btn_SaveRun_Click(object sender, EventArgs e)
        {
            LoadSave(true);
            DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void mls_PerformClassifications_CheckedChanged(object sender, EventArgs e)
        {
            btn_Classifications_AddFolder.Enabled = btn_Classifications_AddSingle.Enabled = txBx_ClassificationModels.Enabled =
                mls_cmClassification_OnlyExistingByName.Enabled = mls_ExistingAnnotations_1All_2Existing_3Not_Classification.Enabled =
                mls_cmRecordBackAnnotations.Enabled = mls_ModelPath_Classification.Enabled = btn_Classifications_Remove.Enabled =
                mls_cmDeleteExistingAnnotations.Enabled =
                mls_cmPerformClassifications.Checked;
        }

        private void mls_PerformMeasures_CheckedChanged(object sender, EventArgs e)
        {
            btn_Measures_AddFolder.Enabled = btn_Measures_AddSingle.Enabled = txBx_MeasurementModels.Enabled =
                mls_Measure_OnlyExistingByName.Enabled = mls_ExistingAnnotations_1All_2Existing_3Not_Measure.Enabled =
                mls_SaveMeasuresFolder.Enabled = mls_ModelPath_Measure.Enabled = mls_cmSaveSegOutputImages.Enabled =
                btn_Measures_Remove.Enabled =
                mls_cmPerformMeasures.Checked;
        }

        private void txBx_ModelsMulti_MouseUp(object sender, MouseEventArgs e)
        {
            var txBx = sender as TextBox;

            if (txBx != null && txBx.Text != "")
            {
                int startLineIndex = txBx.GetLineFromCharIndex(txBx.SelectionStart);
                int endLineIndex = txBx.GetLineFromCharIndex(txBx.SelectionStart + txBx.SelectionLength);

                int startCharIndex = txBx.GetFirstCharIndexFromLine(startLineIndex);
                int endCharIndex = txBx.GetFirstCharIndexFromLine(endLineIndex) + txBx.Lines[endLineIndex].Length;

                txBx.Select(startCharIndex, endCharIndex - startCharIndex);

                Update_Model_Detail(startLineIndex == endLineIndex ? txBx.SelectedText : "", txBx.Name.Contains("Class") ? 0 : 1);
            }
        }

        private void btn_Multi_Remove(object sender, EventArgs e)
        {
            var btn = sender as Button;
            var txBx = btn.Name.Contains("Class") ? txBx_ClassificationModels : txBx_MeasurementModels;
            var tP = btn.Name.Contains("Class") ? 0 : 1;

            RemoveSingle(txBx, txBx.SelectedText, tP);
        }

        private void lbl_Detail_Model_Click(object sender, EventArgs e)
        {
            var lbl = sender as Label;
            var tP = lbl.Name.Contains("Class") ? 0 : 1;
            var Mdl = SelectedModel[tP];
            if (Mdl == null) return;
            string Folder = Directory.GetParent(Mdl.ModelPathFull).FullName;
            if (!Directory.Exists(Folder))
            {
                txBx_Update.Text = "Model folder may have changed";
                return;
            }
            var startInfo = new ProcessStartInfo { Arguments = "\"" + Folder + "\"", FileName = "explorer.exe" };
            Process.Start(startInfo);
        }

        private void ShowExistingAnnos_Click(object sender, EventArgs e)
        {
            var txBx = (((Label)sender).Name == "label12") ? mls_Measure_OnlyExistingByName : mls_cmClassification_OnlyExistingByName;
            txBx.Text = string.Join("; ", ActiveAnnotationsList);
        }
    }
}
