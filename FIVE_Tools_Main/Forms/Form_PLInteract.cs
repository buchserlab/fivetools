﻿using FIVE_IMG.PL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FIVE_Tools_Main
{
    public partial class Form_PLInteract : Form
    {
        MLSettings_PL Settings;
        Dictionary<string, DockerContainer> DockerContainers;
        DateTime DockerChecked = DateTime.MinValue;
        int DontRecheckLessThan_Seconds = 60 * 1;
        public string MRU_ModelPath_SavePath = Path.Combine(Path.GetTempPath(), "fiveTools_Models_MRU.txt");
        List<string> MRU_ModelPaths;

        public Form_PLInteract()
        {
            InitializeComponent();
            Load_MRUModelPaths();
        }

        private void Form_PLInteract_Load(object sender, EventArgs e)
        {

        }

        private void Form_PLInteract_Shown(object sender, EventArgs e)
        {
            DisableDeploy();
            RefreshDocker();
        }

        public void Load_MRUModelPaths()
        {
            combo_ModelPath.Items.Clear();
            if (File.Exists(MRU_ModelPath_SavePath))
            {
                MRU_ModelPaths = File.ReadAllLines(MRU_ModelPath_SavePath).ToList();
                combo_ModelPath.Items.AddRange(MRU_ModelPaths.ToArray());
            }
        }

        public void Save_MRUModelPaths()
        {
            if (!combo_ModelPath.Items.Contains(combo_ModelPath.Text)) combo_ModelPath.Items.Add(combo_ModelPath.Text);
            MRU_ModelPaths = new List<string>();
            for (int i = 0; i < combo_ModelPath.Items.Count; i++) MRU_ModelPaths.Add(combo_ModelPath.Items[i].ToString());
            File.WriteAllLines(MRU_ModelPath_SavePath, MRU_ModelPaths.ToArray());
        }

        public string SelectedModelPath
        {
            get
            {
                return combo_ModelPath.Text;
            }
            set
            {
                //First see if it is already in the list
                combo_ModelPath.Text = value;
            }
        }

        private void lbl_ClearList_Click(object sender, EventArgs e)
        {
            combo_ModelPath.Items.Clear();
        }

        private void btn_MakeCSV_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Generating CSV for Perceptilabs . . "; Application.DoEvents();
            string BasePath = txBx_ImageBaseFolder.Text;
            string Class;
            StringBuilder sB = new StringBuilder();
            sB.Append("Images,Labeles\r\n");
            List<string> Folder = Directory.GetDirectories(BasePath).ToList();
            if (Folder.Count == 0) Folder.Add(BasePath);
            int FileCount = 0;
            foreach (string folders in Folder)
            {
                Class = new DirectoryInfo(folders).Name;
                string[] files = Directory.GetFiles(folders, txBx_SearchString.Text);
                FileCount += files.Length;
                string t = string.Join("\r\n", files.Select(x => "" + Class + "/" + Path.GetFileName(x) + "," + Class));
                sB.Append(t + "\r\n");
            }
            File.WriteAllText(Path.Combine(BasePath, "ForPL001.csv"), sB.ToString());
            txBx_Update.Text = "Done " + FileCount + "files."; Application.DoEvents();
        }

        private void btn_StartServer_Click(object sender, EventArgs e)
        {
            Save_MRUModelPaths();
            DisableDeploy();
            //Check to see if this port is already in use
            RefreshDocker();
            var DC = GetFromListedPort();
            bool Outcome = false;
            if (DC != null)
            {
                //This port is already running. Check to see if the model paths match
                if (SelectedModelPath.ToUpper().Trim() == DC.ModelPath.ToUpper())
                {
                    //Paths match, so just attach to these settings and we are good
                    Settings = MLSettings_PL.SetupStartPort(SelectedModelPath, txBx_Port.Text, false);
                    txBx_Update.Text = "Connected on " + txBx_Port.Text;
                    Outcome = true;
                }
                else
                {
                    //Paths don't match, ask what to do
                    //var DR = MessageBox.Show("","",MessageBoxButtons.YesNoCancel);
                    txBx_Update.Text = "The Model Path listed in TF Path doesn't match the model source that is running on Port " + txBx_Port.Text + ". \r\n1: Use 'Get Model from Port' if you want to deploy to the model already running at that port \r\n2: Delete the container at that port and click 'Start or Connect' again which will load the listed model into the port.\r\n3: Change to a new port.";
                }
            }
            else
            {
                DirectoryInfo DI = new DirectoryInfo(SelectedModelPath);
                if (!DI.Exists) { txBx_Update.Text = "Model Path Incorrect"; return; }

                Settings = MLSettings_PL.SetupStartPort(SelectedModelPath, txBx_Port.Text, true);
                txBx_Update.Text = "Starting . . ";

                SpinSleep(3);
                RefreshDocker(true); //Check to see if it actually started
                if (!DockerContainers.ContainsKey(txBx_Port.Text))
                {
                    txBx_Update.Text = "Waiting . . ";
                    SpinSleep(5);
                    RefreshDocker(true);
                    if (!DockerContainers.ContainsKey(txBx_Port.Text))
                    {
                        txBx_Update.Text = "Problem starting this Container.";
                        return;
                    }
                }
                Outcome = true;
            }
            if (Outcome)
            {
                btn_DeployOnImages.Enabled = true;
            }
        }

        public void SpinSleep(int seconds)
        {
            var S = DateTime.Now;
            while ((DateTime.Now - S).TotalSeconds < seconds)
            {
                System.Threading.Thread.Sleep(25);
                Application.DoEvents();
            }
        }

        public void RefreshDocker(bool Force = false)
        {
            //See if necessary
            if (!Force) if ((DateTime.Now - DockerChecked).TotalSeconds < DontRecheckLessThan_Seconds) return;

            //First Check Running Containers
            txBx_Update.Text = "Checking running containers . . "; Application.DoEvents();
            DockerContainers = DockerContainer.GetAllContainers(); DockerChecked = DateTime.Now;
            if (DockerContainers.Count == 0) txBx_Update.Text = "No running containers. After installing docker, be sure to load this command (only needs to be run once) :\r\n\r\n" + "docker pull tensorflow/serving";
            else txBx_Update.Text = "Running Containers:\r\n" + string.Join("\r\n", DockerContainers.Select(x => x.Key + "\t" + x.Value.ModelPath));

            Application.DoEvents();
        }

        public DockerContainer GetFromListedPort()
        {
            if (DockerContainers.ContainsKey(txBx_Port.Text))
            {
                var DC = DockerContainers[txBx_Port.Text];
                return DC;
            }
            return null;
        }

        private void btn_GetFromPort_Click(object sender, EventArgs e)
        {
            var DC = GetFromListedPort();
            if (DC == null)
            {
                txBx_Update.Text = "Couldn't find a model on that port.";
                DisableDeploy();
            }
            else
            {
                SelectedModelPath = DC.ModelPath;
                txBx_Update.Text = "Updated.";
                btn_DeployOnImages.Enabled = true;
            }
        }

        private void btn_DeployOnImages_Click(object sender, EventArgs e)
        {
            if (Settings == null) { txBx_Update.Text = "Something is wrong, make sure to start or connect a model."; return; }

            DirectoryInfo DI = new DirectoryInfo(txBx_ImageBaseFolder.Text);
            if (!DI.Exists) { txBx_Update.Text = "Image Folder Path Incorrect"; return; }
            Settings.SearchString = txBx_SearchString.Text;
            Settings.MoveImageToSubfolder = chkBx_MoveImages.Checked;
            Settings.ImageFolder = txBx_ImageBaseFolder.Text;
            Settings.ResultFolder = txBx_ResultsFolder.Text;

            btn_Stop.Enabled = true;
            bgWorker_Main.RunWorkerAsync(Settings);
        }

        private void btn_BrowseImages_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Title = "Please select an example image from folder. ";
            OFD.RestoreDirectory = true;
            OFD.Filter = "jpeg|*.jpg;tif|*.tif,bmp|*.bmp";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                txBx_ImageBaseFolder.Text = Path.GetDirectoryName(OFD.FileName);
                txBx_Update.Text = "If you are using the image path to create a CSV, you probably have to remove the last directory since it wants to search for subfolders with images inside the subfolders.";
            }
        }

        private void btnBrowse_TFModelPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Title = "Please select the .pb file inside of the '1' folder of the model. Make sure you didn't rename the folder name.";
            OFD.RestoreDirectory = true;
            OFD.Filter = "TF Model|*.pb";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                string Fldr = Path.GetDirectoryName(OFD.FileName);
                Fldr = Fldr.Substring(0, Fldr.Length - 1);
                SelectedModelPath = Path.GetDirectoryName(Fldr);
                txBx_Update.Text = "";
                DisableDeploy();
            }
        }

        private void bgWorker_Main_DoWork(object sender, DoWorkEventArgs e)
        {
            var Settings = (MLSettings_PL)e.Argument;
            var Ret = PLDeployModule.PredictOnFolder(Settings.ImageFolder, Settings, bgWorker_Main);

            //Now package up the results
            string SavePath = Path.Combine(Settings.ResultFolder, Settings.ModelName + DateTime.Now.ToString("yyyyMMdd_HHmmss") + " Res.txt");
            e.Result = SavePath;
            if (Ret.Count == 0) return;
            bool first = true;
            var sB = new StringBuilder(); char d = '\t';
            int track = 0;
            foreach (var item in Ret)
            {
                if (first)
                {
                    //This will fail if the first one has no predictions . . 
                    sB.Append("FullName" + d + "Folder" + d + "Filename" + d + "Text" + d);
                    for (int i = 0; i < item.Value.numericRes.Length; i++) sB.Append("C" + i + d);
                    sB.Append("Class" + "\r\n");
                    first = false;
                }
                string Filename = item.Key; string FullName = Path.Combine(Settings.ImageFolder, Filename);
                sB.Append(FullName + d + Settings.ImageFolder + d + Filename + d);
                if (item.Value == null) { sB.Append("no prediction\r\n"); continue; }
                if (item.Value.textRes == null) { sB.Append("no prediction\r\n"); continue; }
                sB.Append(item.Value.textRes + d);

                //Figure out the 1 class
                double Max = double.MinValue; int indexMax = -1;
                for (int i = 0; i < item.Value.numericRes.Length; i++)
                {
                    double tVal = item.Value.numericRes[i];
                    if (tVal > Max) { Max = tVal; indexMax = i; }
                    sB.Append(tVal.ToString() + d);
                }
                sB.Append(indexMax + "\r\n");
                string ClassName = indexMax.ToString();

                //Move to folder
                if (Settings.MoveImageToSubfolder)
                {
                    if (track++ % 10 == 0) bgWorker_Main.ReportProgress(0,"Moving Images . . " + track);
                    var DI = new DirectoryInfo(Path.Combine(Settings.ImageFolder,ClassName));
                    if (!DI.Exists) DI.Create();
                    File.Move(FullName, Path.Combine(DI.FullName, Filename));
                }
            }
            File.WriteAllText(SavePath, sB.ToString());
        }

        private void bgWorker_Main_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
            if (txBx_Update.Text.Length > 12000) txBx_Update.Text = txBx_Update.Text.Substring(0, 4000);
        }

        private void bgWorker_Main_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txBx_Update.Text = e.Result.ToString();
            btn_Stop.Enabled = false;
        }

        public void DisableDeploy() { btn_DeployOnImages.Enabled = false; }


        private void btn_DeleteContainerAtThisPort_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Deleting . "; Application.DoEvents();

            RefreshDocker();
            var DC = GetFromListedPort();
            if (DC != null)
            {
                string resp = DC.Delete();
                DockerChecked = DateTime.MinValue; //Forces a recheck next time
                txBx_Update.Text = resp;
                DisableDeploy();

                txBx_Update.Text = "Deleting . . ";

                SpinSleep(2);
                RefreshDocker(true); //Check to see if it actually started
                if (DockerContainers.ContainsKey(DC.Port))
                {
                    txBx_Update.Text = "Waiting . . ";
                    SpinSleep(4);
                    RefreshDocker(true);
                }
            }
            else
            {
                txBx_Update.Text = "There is no Docker container running on that port.";
            }
        }

        private void lbl_RefreshPorts_Click(object sender, EventArgs e)
        {
            RefreshDocker(true);
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            bgWorker_Main.CancelAsync();
            txBx_Update.Text = "Stopping . . ";
        }

        private void Form_PLInteract_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
