﻿using FIVE_IMG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE_Tools_Main
{

    public enum FormRegResult
    {
        Continue = 0, Exit = 1, Retry = 2, SkipWell = 3
    }

    public partial class FormRegistration : Form
    {

        public FormRegistration()
        {
            InitializeComponent();
        }

        public FormRegResult continueCondition = FormRegResult.Exit;
        public string ResultMessage = "";
        public List<FIVE_IMG.SharedResource> SR_List;
        public int CurrentSR;
        public string RegisterWellResult;
        public FIVE_IMG.SVP_Registration_Params RegParams;

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                PointF tP;
                try
                {
                    tP = new PointF(float.Parse(txBx_Offset_X.Text), float.Parse(txBx_Offset_Y.Text));
                }
                catch { txBx_Update.Text = "Check that well offset points are numbers."; return; }
                InCell_To_Leica.SR.Offset_WellRefined = tP;
            } else if (radioButton2.Checked)
            {
                InCell_To_Leica.SR.Offset_WellRefined = SR.IAR.Offset;
            } else if (radioButton3.Checked)
            {
                PointF tP;
                try
                {
                    tP = new PointF(float.Parse(txBx_Cu_X.Text), float.Parse(txBx_Cu_Y.Text));
                }
                catch { txBx_Update.Text = "Check that custom points are numbers."; return; }
                InCell_To_Leica.SR.Offset_WellRefined = tP;
            }

            //Also update the RegParams that are inputted here
            float LastFOVMixRatio;
            if (!float.TryParse(txBx_LastFOVMixRatio.Text, out LastFOVMixRatio)) { txBx_Update.Text = "Check that the LastFOVMixRatio is between 0 and 1"; return; }
            RegParams.LastFOVOffset_over_WellOffset_MixRatio = LastFOVMixRatio;
            float x, y; bool bx, by;
            bx = float.TryParse(txBx_MaxDev_X.Text, out x);
            by = float.TryParse(txBx_MaxDev_Y.Text, out y);
            if (!(bx && by)) { txBx_Update.Text = "Check that the Max Dev X and Y are numeric."; return; }
            RegParams.FOVOffset_MaxDeviationFromWell = new PointF(x,y);

            continueCondition = FormRegResult.Continue;
            this.Hide();
        }

        private void btn_SR_Backward_Click(object sender, EventArgs e)
        {
            CurrentSR--; if (CurrentSR < 0)
                CurrentSR = SR_List.Count - 1;
            UpdateReport();
        }

        private void btn_SR_Forward_Click(object sender, EventArgs e)
        {
            CurrentSR++; if (CurrentSR >= SR_List.Count) CurrentSR = 0;
            UpdateReport();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            continueCondition = FormRegResult.Exit;
            this.Hide();
        }
        private void btn_Retry_Click(object sender, EventArgs e)
        {
            continueCondition = FormRegResult.Retry;
            this.Hide();
        }
        private void FormRegistration_Load(object sender, EventArgs e)
        {

            (bool passed, RegisterWellResult) = InCell_To_Leica.Leica003b_RegisterWell();
            SR_List = InCell_To_Leica.SRQ.ToList();

            if (SR_List.Count == 0)
            {
                txBx_Update.Text = "Couldn't find an overlay, probably means that there was an issue with the intensity of the Leica image being too low or too little contrast.";
                return;
            }
            else
            {
                txBx_Update.Text = RegisterWellResult;
            }

            CurrentSR = 0;

            CalculateDisplayWellOffset();
            lbl_MaxRangeReport.Text = "Max Range :" +
                InCell_To_Leica.SR.WellBased_MaxRange_LastSet.X.ToString("0.0") + ", " +
                InCell_To_Leica.SR.WellBased_MaxRange_LastSet.Y.ToString("0.0");  //Range of how far off the wells are
            if (InCell_To_Leica.SR.Offset_WellRefined.X != 0) radioButton1.Checked = true; else radioButton3.Checked = true;

            txBx_Cu_X.Text = InCell_To_Leica.SR.DefaultInitOffset.X.ToString("0");
            txBx_Cu_Y.Text = InCell_To_Leica.SR.DefaultInitOffset.Y.ToString("0");
            lbl_PlateWell.Text = SR.PlateID + "." + SR.CurrentWell;

            txBx_LastFOVMixRatio.Text = RegParams.LastFOVOffset_over_WellOffset_MixRatio.ToString();
            txBx_MaxDev_X.Text = RegParams.FOVOffset_MaxDeviationFromWell.X.ToString();
            txBx_MaxDev_Y.Text = RegParams.FOVOffset_MaxDeviationFromWell.Y.ToString();

            Application.DoEvents();
            this.Focus();
            this.BringToFront();
            this.TopMost = true;
            Application.DoEvents();

            UpdateReport();
        }

        private void CalculateDisplayWellOffset()
        {
            //Based on the includes . . 
            if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x20_Standard)
            {
                var Exists = SR_List.Where(x => x.IAR.RR != null);
                if (Exists.Count() == 0) { txBx_Update.Text = "SR List Empty"; return; }
                var Includes = Exists.Where(x => x.IAR.RR.Include);
                if (Includes.Count() == 0) { txBx_Update.Text = "Include List Empty"; return; }

                txBx_Offset_X.Text = Includes.Average(x => x.IAR.Offset.X).ToString("0.00");
                txBx_Offset_Y.Text = Includes.Average(x => x.IAR.Offset.Y).ToString("0.00");
            }
            else if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                txBx_Offset_X.Text = SR.avgOffset.X.ToString("0.00");
                txBx_Offset_Y.Text = SR.avgOffset.Y.ToString("0.00");
            }
            //txBx_Offset_X.Text = FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined.X.ToString(); //Whole well offset
            //txBx_Offset_Y.Text = FIVE_IMG.InCell_To_Leica.SR.Offset_WellRefined.Y.ToString();
        }

        public FIVE_IMG.SharedResource SR { get => SR_List[CurrentSR]; }

        public void UpdateReport()
        {
            Bitmap BMUse;
            bool testMode = InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile;

            string overlayPath = testMode ? SR.x5OverlayPath : SR.OverlayPath;

            if (!System.IO.File.Exists(overlayPath))
            {
                BMUse = new Bitmap(10, 10); using (var g = Graphics.FromImage(BMUse)) { g.Clear(Color.AliceBlue); }
            }
            else BMUse = new Bitmap(overlayPath);

            // make sure BMUse fits the screen... (not sure if we have to do this, the picture box can be set to stretch it down, may be useful to have the full version so we can zoom in?)  9/14/2023 comment
            BMUse = new Bitmap(BMUse, 381, 381);
            try
            {
                pictureBox_Main.Image = BMUse;
                if (!testMode) { 
                    label_ID.Text = SR.IAR.ExportName;
                    label_Info.Text = "Score F " + SR.IAR.RR.Score_Final.ToString("0.0");
                    lbl_Info2.Text = "Score R2 " + SR.IAR.RR.FinalR2.ToString("0.0");
                    chkBx_Include.Checked = SR.IAR.RR.Include;

                    txBx_FO_X.Text = SR.IAR.Offset.X.ToString();
                    txBx_FO_Y.Text = SR.IAR.Offset.Y.ToString();
                }
            }
            catch
            {
                pictureBox_Main.Image = null;
                txBx_Update.Text = "Some error occured, try another image.";
            }
        }

        private void chkBx_Include_CheckedChanged(object sender, EventArgs e)
        {
            SR.IAR.RR.Include = chkBx_Include.Checked;
            CalculateDisplayWellOffset();
        }

        private void chkBx_AutoReg_CheckedChanged(object sender, EventArgs e)
        {
            InCell_To_Leica.SR.automateRegistration = chkBx_autoReg.Checked;
            MessageBox.Show(InCell_To_Leica.SR.automateRegistration ? "All wells will run without additional input if this is checked" : "You'll need to verify registrations after each well");
        }
    }
}
