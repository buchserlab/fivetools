﻿
namespace FIVE_Tools_Main
{
    partial class Form_PLInteract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txBx_Port = new System.Windows.Forms.TextBox();
            this.btn_StartServer = new System.Windows.Forms.Button();
            this.btnBrowse_TFModelPath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txBx_ImageBaseFolder = new System.Windows.Forms.TextBox();
            this.txBx_SearchString = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_MakeCSV = new System.Windows.Forms.Button();
            this.btn_DeployOnImages = new System.Windows.Forms.Button();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_BrowseImages = new System.Windows.Forms.Button();
            this.bgWorker_Main = new System.ComponentModel.BackgroundWorker();
            this.btn_GetFromPort = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkBx_MoveImages = new System.Windows.Forms.CheckBox();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.lbl_RefreshPorts = new System.Windows.Forms.Label();
            this.lbl_ClearList = new System.Windows.Forms.Label();
            this.combo_ModelPath = new System.Windows.Forms.ComboBox();
            this.btn_DeleteContainerAtThisPort = new System.Windows.Forms.Button();
            this.txBx_ResultsFolder = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txBx_Port
            // 
            this.txBx_Port.Location = new System.Drawing.Point(433, 44);
            this.txBx_Port.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Port.Name = "txBx_Port";
            this.txBx_Port.Size = new System.Drawing.Size(90, 23);
            this.txBx_Port.TabIndex = 1;
            this.txBx_Port.Text = "8038";
            // 
            // btn_StartServer
            // 
            this.btn_StartServer.Location = new System.Drawing.Point(552, 14);
            this.btn_StartServer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_StartServer.Name = "btn_StartServer";
            this.btn_StartServer.Size = new System.Drawing.Size(110, 59);
            this.btn_StartServer.TabIndex = 2;
            this.btn_StartServer.Text = "Start or Connect to Server";
            this.btn_StartServer.UseVisualStyleBackColor = true;
            this.btn_StartServer.Click += new System.EventHandler(this.btn_StartServer_Click);
            // 
            // btnBrowse_TFModelPath
            // 
            this.btnBrowse_TFModelPath.Location = new System.Drawing.Point(144, 15);
            this.btnBrowse_TFModelPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBrowse_TFModelPath.Name = "btnBrowse_TFModelPath";
            this.btnBrowse_TFModelPath.Size = new System.Drawing.Size(56, 25);
            this.btnBrowse_TFModelPath.TabIndex = 3;
            this.btnBrowse_TFModelPath.Text = ". . . ";
            this.btnBrowse_TFModelPath.UseVisualStyleBackColor = true;
            this.btnBrowse_TFModelPath.Click += new System.EventHandler(this.btnBrowse_TFModelPath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "TF Model Path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(441, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Images Base Folder";
            // 
            // txBx_ImageBaseFolder
            // 
            this.txBx_ImageBaseFolder.Location = new System.Drawing.Point(27, 35);
            this.txBx_ImageBaseFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ImageBaseFolder.Name = "txBx_ImageBaseFolder";
            this.txBx_ImageBaseFolder.Size = new System.Drawing.Size(398, 23);
            this.txBx_ImageBaseFolder.TabIndex = 6;
            this.txBx_ImageBaseFolder.Text = "R:\\People\\Purva_Patel\\PLTest\\Images\\TestY";
            // 
            // txBx_SearchString
            // 
            this.txBx_SearchString.Location = new System.Drawing.Point(448, 35);
            this.txBx_SearchString.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_SearchString.Name = "txBx_SearchString";
            this.txBx_SearchString.Size = new System.Drawing.Size(89, 23);
            this.txBx_SearchString.TabIndex = 8;
            this.txBx_SearchString.Text = "*.jpg";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(456, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Search String";
            // 
            // btn_MakeCSV
            // 
            this.btn_MakeCSV.Location = new System.Drawing.Point(567, 31);
            this.btn_MakeCSV.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_MakeCSV.Name = "btn_MakeCSV";
            this.btn_MakeCSV.Size = new System.Drawing.Size(110, 27);
            this.btn_MakeCSV.TabIndex = 10;
            this.btn_MakeCSV.Text = "Make CSV";
            this.btn_MakeCSV.UseVisualStyleBackColor = true;
            this.btn_MakeCSV.Click += new System.EventHandler(this.btn_MakeCSV_Click);
            // 
            // btn_DeployOnImages
            // 
            this.btn_DeployOnImages.Enabled = false;
            this.btn_DeployOnImages.Location = new System.Drawing.Point(552, 108);
            this.btn_DeployOnImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_DeployOnImages.Name = "btn_DeployOnImages";
            this.btn_DeployOnImages.Size = new System.Drawing.Size(110, 37);
            this.btn_DeployOnImages.TabIndex = 11;
            this.btn_DeployOnImages.Text = "Infer on Images";
            this.btn_DeployOnImages.UseVisualStyleBackColor = true;
            this.btn_DeployOnImages.Click += new System.EventHandler(this.btn_DeployOnImages_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(12, 300);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(677, 176);
            this.txBx_Update.TabIndex = 12;
            // 
            // btn_BrowseImages
            // 
            this.btn_BrowseImages.Location = new System.Drawing.Point(159, 6);
            this.btn_BrowseImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_BrowseImages.Name = "btn_BrowseImages";
            this.btn_BrowseImages.Size = new System.Drawing.Size(56, 25);
            this.btn_BrowseImages.TabIndex = 13;
            this.btn_BrowseImages.Text = ". . . ";
            this.btn_BrowseImages.UseVisualStyleBackColor = true;
            this.btn_BrowseImages.Click += new System.EventHandler(this.btn_BrowseImages_Click);
            // 
            // bgWorker_Main
            // 
            this.bgWorker_Main.WorkerReportsProgress = true;
            this.bgWorker_Main.WorkerSupportsCancellation = true;
            this.bgWorker_Main.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_Main_DoWork);
            this.bgWorker_Main.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_Main_ProgressChanged);
            this.bgWorker_Main.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_Main_RunWorkerCompleted);
            // 
            // btn_GetFromPort
            // 
            this.btn_GetFromPort.Location = new System.Drawing.Point(206, 15);
            this.btn_GetFromPort.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_GetFromPort.Name = "btn_GetFromPort";
            this.btn_GetFromPort.Size = new System.Drawing.Size(144, 25);
            this.btn_GetFromPort.TabIndex = 14;
            this.btn_GetFromPort.Text = "Get Model from Port";
            this.btn_GetFromPort.UseVisualStyleBackColor = true;
            this.btn_GetFromPort.Click += new System.EventHandler(this.btn_GetFromPort_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(564, 61);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "(Only for Training)";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkBx_MoveImages);
            this.panel1.Controls.Add(this.btn_Stop);
            this.panel1.Controls.Add(this.lbl_RefreshPorts);
            this.panel1.Controls.Add(this.lbl_ClearList);
            this.panel1.Controls.Add(this.combo_ModelPath);
            this.panel1.Controls.Add(this.btn_DeleteContainerAtThisPort);
            this.panel1.Controls.Add(this.txBx_ResultsFolder);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnBrowse_TFModelPath);
            this.panel1.Controls.Add(this.btn_GetFromPort);
            this.panel1.Controls.Add(this.txBx_Port);
            this.panel1.Controls.Add(this.btn_DeployOnImages);
            this.panel1.Controls.Add(this.btn_StartServer);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(14, 97);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(678, 157);
            this.panel1.TabIndex = 16;
            // 
            // chkBx_MoveImages
            // 
            this.chkBx_MoveImages.AutoSize = true;
            this.chkBx_MoveImages.Location = new System.Drawing.Point(552, 86);
            this.chkBx_MoveImages.Name = "chkBx_MoveImages";
            this.chkBx_MoveImages.Size = new System.Drawing.Size(117, 19);
            this.chkBx_MoveImages.TabIndex = 22;
            this.chkBx_MoveImages.Text = "Move Img to Fldr";
            this.chkBx_MoveImages.UseVisualStyleBackColor = true;
            // 
            // btn_Stop
            // 
            this.btn_Stop.Enabled = false;
            this.btn_Stop.Location = new System.Drawing.Point(479, 108);
            this.btn_Stop.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(65, 37);
            this.btn_Stop.TabIndex = 21;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // lbl_RefreshPorts
            // 
            this.lbl_RefreshPorts.AutoSize = true;
            this.lbl_RefreshPorts.ForeColor = System.Drawing.Color.MediumBlue;
            this.lbl_RefreshPorts.Location = new System.Drawing.Point(441, 6);
            this.lbl_RefreshPorts.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_RefreshPorts.Name = "lbl_RefreshPorts";
            this.lbl_RefreshPorts.Size = new System.Drawing.Size(76, 15);
            this.lbl_RefreshPorts.TabIndex = 20;
            this.lbl_RefreshPorts.Text = "Refresh Ports";
            this.lbl_RefreshPorts.Click += new System.EventHandler(this.lbl_RefreshPorts_Click);
            // 
            // lbl_ClearList
            // 
            this.lbl_ClearList.AutoSize = true;
            this.lbl_ClearList.ForeColor = System.Drawing.Color.MediumBlue;
            this.lbl_ClearList.Location = new System.Drawing.Point(307, 74);
            this.lbl_ClearList.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ClearList.Name = "lbl_ClearList";
            this.lbl_ClearList.Size = new System.Drawing.Size(55, 15);
            this.lbl_ClearList.TabIndex = 19;
            this.lbl_ClearList.Text = "Clear List";
            this.lbl_ClearList.Click += new System.EventHandler(this.lbl_ClearList_Click);
            // 
            // combo_ModelPath
            // 
            this.combo_ModelPath.FormattingEnabled = true;
            this.combo_ModelPath.Location = new System.Drawing.Point(12, 44);
            this.combo_ModelPath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.combo_ModelPath.Name = "combo_ModelPath";
            this.combo_ModelPath.Size = new System.Drawing.Size(398, 23);
            this.combo_ModelPath.TabIndex = 18;
            // 
            // btn_DeleteContainerAtThisPort
            // 
            this.btn_DeleteContainerAtThisPort.Location = new System.Drawing.Point(433, 74);
            this.btn_DeleteContainerAtThisPort.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_DeleteContainerAtThisPort.Name = "btn_DeleteContainerAtThisPort";
            this.btn_DeleteContainerAtThisPort.Size = new System.Drawing.Size(91, 25);
            this.btn_DeleteContainerAtThisPort.TabIndex = 17;
            this.btn_DeleteContainerAtThisPort.Text = "Delete";
            this.btn_DeleteContainerAtThisPort.UseVisualStyleBackColor = true;
            this.btn_DeleteContainerAtThisPort.Click += new System.EventHandler(this.btn_DeleteContainerAtThisPort_Click);
            // 
            // txBx_ResultsFolder
            // 
            this.txBx_ResultsFolder.Location = new System.Drawing.Point(12, 108);
            this.txBx_ResultsFolder.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_ResultsFolder.Name = "txBx_ResultsFolder";
            this.txBx_ResultsFolder.Size = new System.Drawing.Size(296, 23);
            this.txBx_ResultsFolder.TabIndex = 15;
            this.txBx_ResultsFolder.Text = "c:\\temp\\";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 90);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "Results Folder";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 282);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "Updates:";
            // 
            // Form_PLInteract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 490);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_BrowseImages);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.btn_MakeCSV);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txBx_SearchString);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txBx_ImageBaseFolder);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form_PLInteract";
            this.Text = "Infer Deployed TF Model";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_PLInteract_FormClosing);
            this.Load += new System.EventHandler(this.Form_PLInteract_Load);
            this.Shown += new System.EventHandler(this.Form_PLInteract_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txBx_Port;
        private System.Windows.Forms.Button btn_StartServer;
        private System.Windows.Forms.Button btnBrowse_TFModelPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txBx_ImageBaseFolder;
        private System.Windows.Forms.TextBox txBx_SearchString;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_MakeCSV;
        private System.Windows.Forms.Button btn_DeployOnImages;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_BrowseImages;
        private System.ComponentModel.BackgroundWorker bgWorker_Main;
        private System.Windows.Forms.Button btn_GetFromPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txBx_ResultsFolder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_DeleteContainerAtThisPort;
        private System.Windows.Forms.Label lbl_ClearList;
        private System.Windows.Forms.ComboBox combo_ModelPath;
        private System.Windows.Forms.Label lbl_RefreshPorts;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.CheckBox chkBx_MoveImages;
    }
}