﻿
namespace FIVE_Tools_Main
{
    partial class FormBootstrap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txBx_InputFile = new System.Windows.Forms.TextBox();
            this.coBo_Gene = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.coBo_gRNA = new System.Windows.Forms.ComboBox();
            this.listBox_Splits = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.coBo_Filter1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.coBo_Value = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_Browse = new System.Windows.Forms.Button();
            this.btn_LoadColumns = new System.Windows.Forms.Button();
            this.txBx_Filter1 = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.bgWrk_BootstrapP = new System.ComponentModel.BackgroundWorker();
            this.btn_RunStats = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txBx_BootstrapReps = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txBx_FunctionParam = new System.Windows.Forms.TextBox();
            this.coBo_Function = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txBx_InputFile
            // 
            this.txBx_InputFile.Location = new System.Drawing.Point(12, 39);
            this.txBx_InputFile.Name = "txBx_InputFile";
            this.txBx_InputFile.Size = new System.Drawing.Size(651, 20);
            this.txBx_InputFile.TabIndex = 0;
            // 
            // coBo_Gene
            // 
            this.coBo_Gene.FormattingEnabled = true;
            this.coBo_Gene.Location = new System.Drawing.Point(12, 142);
            this.coBo_Gene.Name = "coBo_Gene";
            this.coBo_Gene.Size = new System.Drawing.Size(194, 21);
            this.coBo_Gene.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Gene";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "gRNA";
            // 
            // coBo_gRNA
            // 
            this.coBo_gRNA.FormattingEnabled = true;
            this.coBo_gRNA.Location = new System.Drawing.Point(12, 192);
            this.coBo_gRNA.Name = "coBo_gRNA";
            this.coBo_gRNA.Size = new System.Drawing.Size(194, 21);
            this.coBo_gRNA.TabIndex = 4;
            // 
            // listBox_Splits
            // 
            this.listBox_Splits.FormattingEnabled = true;
            this.listBox_Splits.Location = new System.Drawing.Point(222, 119);
            this.listBox_Splits.Name = "listBox_Splits";
            this.listBox_Splits.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_Splits.Size = new System.Drawing.Size(163, 225);
            this.listBox_Splits.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 270);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Filter 1";
            // 
            // coBo_Filter1
            // 
            this.coBo_Filter1.FormattingEnabled = true;
            this.coBo_Filter1.Location = new System.Drawing.Point(12, 289);
            this.coBo_Filter1.Name = "coBo_Filter1";
            this.coBo_Filter1.Size = new System.Drawing.Size(194, 21);
            this.coBo_Filter1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Value Column";
            // 
            // coBo_Value
            // 
            this.coBo_Value.FormattingEnabled = true;
            this.coBo_Value.Location = new System.Drawing.Point(12, 239);
            this.coBo_Value.Name = "coBo_Value";
            this.coBo_Value.Size = new System.Drawing.Size(194, 21);
            this.coBo_Value.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Input File";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(217, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "4. Select all Split Columns";
            // 
            // btn_Browse
            // 
            this.btn_Browse.Location = new System.Drawing.Point(75, 10);
            this.btn_Browse.Name = "btn_Browse";
            this.btn_Browse.Size = new System.Drawing.Size(75, 23);
            this.btn_Browse.TabIndex = 13;
            this.btn_Browse.Text = "1. Browse . .";
            this.btn_Browse.UseVisualStyleBackColor = true;
            this.btn_Browse.Click += new System.EventHandler(this.btn_Browse_Click);
            // 
            // btn_LoadColumns
            // 
            this.btn_LoadColumns.Location = new System.Drawing.Point(75, 65);
            this.btn_LoadColumns.Name = "btn_LoadColumns";
            this.btn_LoadColumns.Size = new System.Drawing.Size(111, 23);
            this.btn_LoadColumns.TabIndex = 14;
            this.btn_LoadColumns.Text = "2. Load Columns";
            this.btn_LoadColumns.UseVisualStyleBackColor = true;
            this.btn_LoadColumns.Click += new System.EventHandler(this.btn_LoadColumns_Click);
            // 
            // txBx_Filter1
            // 
            this.txBx_Filter1.Location = new System.Drawing.Point(31, 316);
            this.txBx_Filter1.Name = "txBx_Filter1";
            this.txBx_Filter1.Size = new System.Drawing.Size(175, 20);
            this.txBx_Filter1.TabIndex = 15;
            this.txBx_Filter1.Text = "-1";
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(456, 240);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(207, 113);
            this.txBx_Update.TabIndex = 16;
            // 
            // bgWrk_BootstrapP
            // 
            this.bgWrk_BootstrapP.WorkerReportsProgress = true;
            this.bgWrk_BootstrapP.WorkerSupportsCancellation = true;
            this.bgWrk_BootstrapP.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWrk_BootstrapP_DoWork);
            this.bgWrk_BootstrapP.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWrk_BootstrapP_ProgressChanged);
            this.bgWrk_BootstrapP.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWrk_BootstrapP_RunWorkerCompleted);
            // 
            // btn_RunStats
            // 
            this.btn_RunStats.Location = new System.Drawing.Point(403, 211);
            this.btn_RunStats.Name = "btn_RunStats";
            this.btn_RunStats.Size = new System.Drawing.Size(102, 23);
            this.btn_RunStats.TabIndex = 17;
            this.btn_RunStats.Text = "6. Run Bootstraps";
            this.btn_RunStats.UseVisualStyleBackColor = true;
            this.btn_RunStats.Click += new System.EventHandler(this.btn_RunStats_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 319);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = ">";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(400, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Updates:";
            // 
            // txBx_BootstrapReps
            // 
            this.txBx_BootstrapReps.Location = new System.Drawing.Point(403, 180);
            this.txBx_BootstrapReps.Name = "txBx_BootstrapReps";
            this.txBx_BootstrapReps.Size = new System.Drawing.Size(175, 20);
            this.txBx_BootstrapReps.TabIndex = 21;
            this.txBx_BootstrapReps.Text = "30000";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(400, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Bootstrap Replicates";
            // 
            // txBx_FunctionParam
            // 
            this.txBx_FunctionParam.Location = new System.Drawing.Point(403, 138);
            this.txBx_FunctionParam.Name = "txBx_FunctionParam";
            this.txBx_FunctionParam.Size = new System.Drawing.Size(175, 20);
            this.txBx_FunctionParam.TabIndex = 22;
            this.txBx_FunctionParam.Text = "0.63";
            // 
            // coBo_Function
            // 
            this.coBo_Function.FormattingEnabled = true;
            this.coBo_Function.Location = new System.Drawing.Point(403, 98);
            this.coBo_Function.Name = "coBo_Function";
            this.coBo_Function.Size = new System.Drawing.Size(175, 21);
            this.coBo_Function.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(400, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Bootstrap Replicates";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(400, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Function";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(186, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "3. Select corresponding column";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(400, 63);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "5. Adjust Settings";
            // 
            // FormBootstrap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 365);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.coBo_Function);
            this.Controls.Add(this.txBx_FunctionParam);
            this.Controls.Add(this.txBx_BootstrapReps);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_RunStats);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.txBx_Filter1);
            this.Controls.Add(this.btn_LoadColumns);
            this.Controls.Add(this.btn_Browse);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.coBo_Filter1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.coBo_Value);
            this.Controls.Add(this.listBox_Splits);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.coBo_gRNA);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.coBo_Gene);
            this.Controls.Add(this.txBx_InputFile);
            this.Name = "FormBootstrap";
            this.Text = "Generate p Values with Bootstrap";
            this.Load += new System.EventHandler(this.FormBootstrap_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txBx_InputFile;
        private System.Windows.Forms.ComboBox coBo_Gene;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox coBo_gRNA;
        private System.Windows.Forms.ListBox listBox_Splits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox coBo_Filter1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox coBo_Value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_Browse;
        private System.Windows.Forms.Button btn_LoadColumns;
        private System.Windows.Forms.TextBox txBx_Filter1;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.ComponentModel.BackgroundWorker bgWrk_BootstrapP;
        private System.Windows.Forms.Button btn_RunStats;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txBx_BootstrapReps;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txBx_FunctionParam;
        private System.Windows.Forms.ComboBox coBo_Function;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}