﻿namespace FIVE_Tools_Main
{
    partial class FormRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRegistration));
            this.pictureBox_Main = new System.Windows.Forms.PictureBox();
            this.btn_Continue = new System.Windows.Forms.Button();
            this.btn_Retry = new System.Windows.Forms.Button();
            this.btn_SR_Back = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label_ID = new System.Windows.Forms.Label();
            this.label_Info = new System.Windows.Forms.Label();
            this.txBx_Offset_X = new System.Windows.Forms.TextBox();
            this.txBx_Offset_Y = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.btn_SR_Forward = new System.Windows.Forms.Button();
            this.txBx_FO_Y = new System.Windows.Forms.TextBox();
            this.txBx_FO_X = new System.Windows.Forms.TextBox();
            this.txBx_Cu_Y = new System.Windows.Forms.TextBox();
            this.txBx_Cu_X = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Info2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_PlateWell = new System.Windows.Forms.Label();
            this.txBx_MaxDev_Y = new System.Windows.Forms.TextBox();
            this.txBx_MaxDev_X = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txBx_LastFOVMixRatio = new System.Windows.Forms.TextBox();
            this.lbl_MaxRangeReport = new System.Windows.Forms.Label();
            this.chkBx_Include = new System.Windows.Forms.CheckBox();
            this.chkBx_autoReg = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox_Main
            // 
            this.pictureBox_Main.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox_Main.InitialImage")));
            this.pictureBox_Main.Location = new System.Drawing.Point(12, 118);
            this.pictureBox_Main.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox_Main.Name = "pictureBox_Main";
            this.pictureBox_Main.Size = new System.Drawing.Size(342, 324);
            this.pictureBox_Main.TabIndex = 0;
            this.pictureBox_Main.TabStop = false;
            // 
            // btn_Continue
            // 
            this.btn_Continue.Location = new System.Drawing.Point(375, 465);
            this.btn_Continue.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Continue.Name = "btn_Continue";
            this.btn_Continue.Size = new System.Drawing.Size(90, 27);
            this.btn_Continue.TabIndex = 1;
            this.btn_Continue.Text = "Continue";
            this.btn_Continue.UseVisualStyleBackColor = true;
            this.btn_Continue.Click += new System.EventHandler(this.btnOK_Click);
            //
            // btn_Retry
            //
            this.btn_Retry.Location = new System.Drawing.Point(423, 500);
            this.btn_Retry.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Retry.Name = "btn_Retry";
            this.btn_Retry.Size = new System.Drawing.Size(80, 27);
            this.btn_Retry.TabIndex = 13;
            this.btn_Retry.Text = "Retry";
            this.btn_Retry.UseVisualStyleBackColor = true;
            this.btn_Retry.Click += new System.EventHandler(this.btn_Retry_Click);
            // 

            // btn_SR_Back
            // 
            this.btn_SR_Back.Location = new System.Drawing.Point(371, 67);
            this.btn_SR_Back.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SR_Back.Name = "btn_SR_Back";
            this.btn_SR_Back.Size = new System.Drawing.Size(42, 27);
            this.btn_SR_Back.TabIndex = 3;
            this.btn_SR_Back.Text = "<-";
            this.btn_SR_Back.UseVisualStyleBackColor = true;
            this.btn_SR_Back.Click += new System.EventHandler(this.btn_SR_Backward_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 479);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 5;
            // 
            // label_ID
            // 
            this.label_ID.AutoSize = true;
            this.label_ID.Location = new System.Drawing.Point(18, 16);
            this.label_ID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(72, 15);
            this.label_ID.TabIndex = 7;
            this.label_ID.Text = "PlateX A1.45";
            // 
            // label_Info
            // 
            this.label_Info.AutoSize = true;
            this.label_Info.Location = new System.Drawing.Point(18, 53);
            this.label_Info.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Info.Name = "label_Info";
            this.label_Info.Size = new System.Drawing.Size(66, 15);
            this.label_Info.TabIndex = 8;
            this.label_Info.Text = "Score Ratio";
            // 
            // txBx_Offset_X
            // 
            this.txBx_Offset_X.Enabled = false;
            this.txBx_Offset_X.Location = new System.Drawing.Point(384, 302);
            this.txBx_Offset_X.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Offset_X.Name = "txBx_Offset_X";
            this.txBx_Offset_X.Size = new System.Drawing.Size(76, 23);
            this.txBx_Offset_X.TabIndex = 9;
            // 
            // txBx_Offset_Y
            // 
            this.txBx_Offset_Y.Enabled = false;
            this.txBx_Offset_Y.Location = new System.Drawing.Point(468, 302);
            this.txBx_Offset_Y.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Offset_Y.Name = "txBx_Offset_Y";
            this.txBx_Offset_Y.Size = new System.Drawing.Size(76, 23);
            this.txBx_Offset_Y.TabIndex = 10;
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(13, 465);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(341, 29);
            this.txBx_Update.TabIndex = 12;
            // 
            // btn_Exit
            // 
            this.btn_Exit.Location = new System.Drawing.Point(471, 465);
            this.btn_Exit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(77, 27);
            this.btn_Exit.TabIndex = 13;
            this.btn_Exit.Text = "Exit";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_SR_Forward
            // 
            this.btn_SR_Forward.Location = new System.Drawing.Point(420, 67);
            this.btn_SR_Forward.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_SR_Forward.Name = "btn_SR_Forward";
            this.btn_SR_Forward.Size = new System.Drawing.Size(42, 27);
            this.btn_SR_Forward.TabIndex = 14;
            this.btn_SR_Forward.Text = "->";
            this.btn_SR_Forward.UseVisualStyleBackColor = true;
            this.btn_SR_Forward.Click += new System.EventHandler(this.btn_SR_Forward_Click);
            // 
            // txBx_FO_Y
            // 
            this.txBx_FO_Y.Enabled = false;
            this.txBx_FO_Y.Location = new System.Drawing.Point(468, 375);
            this.txBx_FO_Y.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FO_Y.Name = "txBx_FO_Y";
            this.txBx_FO_Y.Size = new System.Drawing.Size(76, 23);
            this.txBx_FO_Y.TabIndex = 16;
            // 
            // txBx_FO_X
            // 
            this.txBx_FO_X.Enabled = false;
            this.txBx_FO_X.Location = new System.Drawing.Point(384, 375);
            this.txBx_FO_X.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_FO_X.Name = "txBx_FO_X";
            this.txBx_FO_X.Size = new System.Drawing.Size(76, 23);
            this.txBx_FO_X.TabIndex = 15;
            // 
            // txBx_Cu_Y
            // 
            this.txBx_Cu_Y.Location = new System.Drawing.Point(468, 429);
            this.txBx_Cu_Y.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Cu_Y.Name = "txBx_Cu_Y";
            this.txBx_Cu_Y.Size = new System.Drawing.Size(76, 23);
            this.txBx_Cu_Y.TabIndex = 18;
            // 
            // txBx_Cu_X
            // 
            this.txBx_Cu_X.Location = new System.Drawing.Point(384, 429);
            this.txBx_Cu_X.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_Cu_X.Name = "txBx_Cu_X";
            this.txBx_Cu_X.Size = new System.Drawing.Size(76, 23);
            this.txBx_Cu_X.TabIndex = 17;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(371, 279);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(166, 19);
            this.radioButton1.TabIndex = 19;
            this.radioButton1.Text = "Well Mean Offset (include)";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(371, 352);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(114, 19);
            this.radioButton2.TabIndex = 20;
            this.radioButton2.Text = "This FOV\'s Offset";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(371, 406);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(102, 19);
            this.radioButton3.TabIndex = 21;
            this.radioButton3.Text = "Custom Offset";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_Info2);
            this.panel1.Controls.Add(this.label_ID);
            this.panel1.Controls.Add(this.label_Info);
            this.panel1.Location = new System.Drawing.Point(14, 14);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 80);
            this.panel1.TabIndex = 22;
            // 
            // lbl_Info2
            // 
            this.lbl_Info2.AutoSize = true;
            this.lbl_Info2.Location = new System.Drawing.Point(195, 53);
            this.lbl_Info2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Info2.Name = "lbl_Info2";
            this.lbl_Info2.Size = new System.Drawing.Size(66, 15);
            this.lbl_Info2.TabIndex = 9;
            this.lbl_Info2.Text = "Score Ratio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(368, 251);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Choose which offset to use:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 15);
            this.label3.TabIndex = 23;
            this.label3.Text = "Use the arrows to preview";
            // 
            // lbl_PlateWell
            // 
            this.lbl_PlateWell.AutoSize = true;
            this.lbl_PlateWell.Location = new System.Drawing.Point(368, 14);
            this.lbl_PlateWell.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_PlateWell.Name = "lbl_PlateWell";
            this.lbl_PlateWell.Size = new System.Drawing.Size(59, 15);
            this.lbl_PlateWell.TabIndex = 10;
            this.lbl_PlateWell.Text = "Plate.Well";
            // 
            // txBx_MaxDev_Y
            // 
            this.txBx_MaxDev_Y.Location = new System.Drawing.Point(459, 149);
            this.txBx_MaxDev_Y.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_MaxDev_Y.Name = "txBx_MaxDev_Y";
            this.txBx_MaxDev_Y.Size = new System.Drawing.Size(76, 23);
            this.txBx_MaxDev_Y.TabIndex = 25;
            // 
            // txBx_MaxDev_X
            // 
            this.txBx_MaxDev_X.Location = new System.Drawing.Point(375, 149);
            this.txBx_MaxDev_X.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_MaxDev_X.Name = "txBx_MaxDev_X";
            this.txBx_MaxDev_X.Size = new System.Drawing.Size(76, 23);
            this.txBx_MaxDev_X.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(375, 131);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 15);
            this.label4.TabIndex = 26;
            this.label4.Text = "FOV max deviation from Well";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(375, 180);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 15);
            this.label5.TabIndex = 28;
            this.label5.Text = "LastFOV/Well Mix Ratio";
            // 
            // txBx_LastFOVMixRatio
            // 
            this.txBx_LastFOVMixRatio.Location = new System.Drawing.Point(375, 198);
            this.txBx_LastFOVMixRatio.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txBx_LastFOVMixRatio.Name = "txBx_LastFOVMixRatio";
            this.txBx_LastFOVMixRatio.Size = new System.Drawing.Size(52, 23);
            this.txBx_LastFOVMixRatio.TabIndex = 27;
            // 
            // lbl_MaxRangeReport
            // 
            this.lbl_MaxRangeReport.AutoSize = true;
            this.lbl_MaxRangeReport.Location = new System.Drawing.Point(363, 328);
            this.lbl_MaxRangeReport.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_MaxRangeReport.Name = "lbl_MaxRangeReport";
            this.lbl_MaxRangeReport.Size = new System.Drawing.Size(69, 15);
            this.lbl_MaxRangeReport.TabIndex = 10;
            this.lbl_MaxRangeReport.Text = "Max Range:";
            // 
            // chkBx_Include
            // 
            this.chkBx_Include.AutoSize = true;
            this.chkBx_Include.Location = new System.Drawing.Point(471, 72);
            this.chkBx_Include.Name = "chkBx_Include";
            this.chkBx_Include.Size = new System.Drawing.Size(70, 19);
            this.chkBx_Include.TabIndex = 29;
            this.chkBx_Include.Text = "Include?";
            this.chkBx_Include.UseVisualStyleBackColor = true;
            this.chkBx_Include.CheckedChanged += new System.EventHandler(this.chkBx_Include_CheckedChanged);
            //
            // chkBx_Automate
            // 
            this.chkBx_autoReg.AutoSize = true;
            this.chkBx_autoReg.Location = new System.Drawing.Point(275, 500);
            this.chkBx_autoReg.Name = "chkBx_autoRegisterWells";
            this.chkBx_autoReg.Size = new System.Drawing.Size(70, 19);
            this.chkBx_autoReg.TabIndex = 29;
            this.chkBx_autoReg.Text = "Auto-Reg future wells?";
            this.chkBx_autoReg.UseVisualStyleBackColor = true;
            this.chkBx_autoReg.CheckedChanged += new System.EventHandler(this.chkBx_AutoReg_CheckedChanged);
            // 
            // FormRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 550);
            this.Controls.Add(this.chkBx_autoReg);
            this.Controls.Add(this.chkBx_Include);
            this.Controls.Add(this.lbl_MaxRangeReport);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txBx_LastFOVMixRatio);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txBx_MaxDev_Y);
            this.Controls.Add(this.txBx_MaxDev_X);
            this.Controls.Add(this.lbl_PlateWell);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.txBx_Cu_Y);
            this.Controls.Add(this.txBx_Cu_X);
            this.Controls.Add(this.txBx_FO_Y);
            this.Controls.Add(this.txBx_FO_X);
            this.Controls.Add(this.btn_SR_Forward);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.txBx_Offset_Y);
            this.Controls.Add(this.txBx_Offset_X);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_SR_Back);
            this.Controls.Add(this.btn_Retry);
            this.Controls.Add(this.btn_Continue);
            this.Controls.Add(this.pictureBox_Main);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FormRegistration";
            this.Text = "Registration QC";
            this.Load += new System.EventHandler(this.FormRegistration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Continue;
        private System.Windows.Forms.Button btn_Retry;
        public System.Windows.Forms.PictureBox pictureBox_Main;
        private System.Windows.Forms.Button btn_SR_Back;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.Label label_Info;
        private System.Windows.Forms.TextBox txBx_Offset_X;
        private System.Windows.Forms.TextBox txBx_Offset_Y;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_SR_Forward;
        private System.Windows.Forms.TextBox txBx_FO_Y;
        private System.Windows.Forms.TextBox txBx_FO_X;
        private System.Windows.Forms.TextBox txBx_Cu_Y;
        private System.Windows.Forms.TextBox txBx_Cu_X;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Info2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_PlateWell;
        private System.Windows.Forms.TextBox txBx_MaxDev_Y;
        private System.Windows.Forms.TextBox txBx_MaxDev_X;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txBx_LastFOVMixRatio;
        private System.Windows.Forms.Label lbl_MaxRangeReport;
        private System.Windows.Forms.CheckBox chkBx_Include;
        private System.Windows.Forms.CheckBox chkBx_autoReg;
    }
}