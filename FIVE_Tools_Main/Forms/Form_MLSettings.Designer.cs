﻿using System.Drawing;

namespace FIVE_Tools_Main.Forms
{

    partial class Form_MLSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            mls_ModelPath_Classification = new System.Windows.Forms.TextBox();
            txBx_ClassificationModels = new System.Windows.Forms.TextBox();
            txBx_MeasurementModels = new System.Windows.Forms.TextBox();
            mls_ModelPath_Measure = new System.Windows.Forms.TextBox();
            mls_cmSaveImages_Overall = new System.Windows.Forms.CheckBox();
            txBx_Update = new System.Windows.Forms.TextBox();
            mls_cmRecordBackAnnotations = new System.Windows.Forms.CheckBox();
            mls_svImageFolder = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            mls_SaveMeasuresFolder = new System.Windows.Forms.TextBox();
            lbl_Classifications_ChooseModel = new System.Windows.Forms.Label();
            lbl_Measures_ChooseModel = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            mls_cmPerformClassifications = new System.Windows.Forms.CheckBox();
            mls_cmPerformMeasures = new System.Windows.Forms.CheckBox();
            mls_ExistingAnnotations_1All_2Existing_3Not_Classification = new System.Windows.Forms.TextBox();
            mls_ExistingAnnotations_1All_2Existing_3Not_Measure = new System.Windows.Forms.TextBox();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            mls_cmClassification_OnlyExistingByName = new System.Windows.Forms.TextBox();
            label12 = new System.Windows.Forms.Label();
            mls_Measure_OnlyExistingByName = new System.Windows.Forms.TextBox();
            btn_Classifications_AddSingle = new System.Windows.Forms.Button();
            btn_Classifications_AddFolder = new System.Windows.Forms.Button();
            btn_Measures_AddFolder = new System.Windows.Forms.Button();
            btn_Measures_AddSingle = new System.Windows.Forms.Button();
            btn_LoadSettings = new System.Windows.Forms.Button();
            btn_SaveSettings = new System.Windows.Forms.Button();
            mls_cmRaftBased = new System.Windows.Forms.CheckBox();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            mls_cmSaveSegOutputImages = new System.Windows.Forms.CheckBox();
            btn_Classifications_Remove = new System.Windows.Forms.Button();
            btn_Measures_Remove = new System.Windows.Forms.Button();
            btn_Cancel = new System.Windows.Forms.Button();
            btn_SaveClose = new System.Windows.Forms.Button();
            btn_SaveRun = new System.Windows.Forms.Button();
            mls_svImageExt = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            lbl_Detail_Class = new System.Windows.Forms.Label();
            lbl_Detail_Measure = new System.Windows.Forms.Label();
            mls_cmDeleteExistingAnnotations = new System.Windows.Forms.CheckBox();
            SuspendLayout();
            // 
            // mls_ModelPath_Classification
            // 
            mls_ModelPath_Classification.Location = new System.Drawing.Point(12, 101);
            mls_ModelPath_Classification.Name = "mls_ModelPath_Classification";
            mls_ModelPath_Classification.Size = new System.Drawing.Size(287, 23);
            mls_ModelPath_Classification.TabIndex = 1;
            mls_ModelPath_Classification.DoubleClick += lbl_Classifications_ChooseModel_Click;
            // 
            // txBx_ClassificationModels
            // 
            txBx_ClassificationModels.Location = new System.Drawing.Point(12, 186);
            txBx_ClassificationModels.Multiline = true;
            txBx_ClassificationModels.Name = "txBx_ClassificationModels";
            txBx_ClassificationModels.Size = new System.Drawing.Size(287, 190);
            txBx_ClassificationModels.TabIndex = 2;
            txBx_ClassificationModels.MouseUp += txBx_ModelsMulti_MouseUp;
            // 
            // txBx_MeasurementModels
            // 
            txBx_MeasurementModels.Location = new System.Drawing.Point(326, 186);
            txBx_MeasurementModels.Multiline = true;
            txBx_MeasurementModels.Name = "txBx_MeasurementModels";
            txBx_MeasurementModels.Size = new System.Drawing.Size(297, 179);
            txBx_MeasurementModels.TabIndex = 3;
            txBx_MeasurementModels.MouseUp += txBx_ModelsMulti_MouseUp;
            // 
            // mls_ModelPath_Measure
            // 
            mls_ModelPath_Measure.Location = new System.Drawing.Point(326, 101);
            mls_ModelPath_Measure.Name = "mls_ModelPath_Measure";
            mls_ModelPath_Measure.Size = new System.Drawing.Size(297, 23);
            mls_ModelPath_Measure.TabIndex = 4;
            mls_ModelPath_Measure.DoubleClick += lbl_Measures_ChooseModel_Click;
            // 
            // mls_cmSaveImages_Overall
            // 
            mls_cmSaveImages_Overall.AutoSize = true;
            mls_cmSaveImages_Overall.Location = new System.Drawing.Point(649, 57);
            mls_cmSaveImages_Overall.Name = "mls_cmSaveImages_Overall";
            mls_cmSaveImages_Overall.Size = new System.Drawing.Size(96, 19);
            mls_cmSaveImages_Overall.TabIndex = 5;
            mls_cmSaveImages_Overall.Text = "Save Images?";
            toolTip1.SetToolTip(mls_cmSaveImages_Overall, "When the images are loaded for processing, should they be saved?");
            mls_cmSaveImages_Overall.UseVisualStyleBackColor = true;
            // 
            // txBx_Update
            // 
            txBx_Update.Location = new System.Drawing.Point(649, 164);
            txBx_Update.Multiline = true;
            txBx_Update.Name = "txBx_Update";
            txBx_Update.Size = new System.Drawing.Size(244, 172);
            txBx_Update.TabIndex = 6;
            // 
            // mls_cmRecordBackAnnotations
            // 
            mls_cmRecordBackAnnotations.AutoSize = true;
            mls_cmRecordBackAnnotations.Location = new System.Drawing.Point(12, 416);
            mls_cmRecordBackAnnotations.Name = "mls_cmRecordBackAnnotations";
            mls_cmRecordBackAnnotations.Size = new System.Drawing.Size(159, 19);
            mls_cmRecordBackAnnotations.TabIndex = 7;
            mls_cmRecordBackAnnotations.Text = "Record Back Annotations";
            mls_cmRecordBackAnnotations.UseVisualStyleBackColor = true;
            // 
            // mls_svImageFolder
            // 
            mls_svImageFolder.Location = new System.Drawing.Point(649, 107);
            mls_svImageFolder.Name = "mls_svImageFolder";
            mls_svImageFolder.Size = new System.Drawing.Size(205, 23);
            mls_svImageFolder.TabIndex = 8;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(649, 89);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(103, 15);
            label2.TabIndex = 9;
            label2.Text = "Save Image Folder";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(326, 398);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(120, 15);
            label3.TabIndex = 11;
            label3.Text = "Save Measures Folder";
            // 
            // mls_SaveMeasuresFolder
            // 
            mls_SaveMeasuresFolder.Location = new System.Drawing.Point(326, 416);
            mls_SaveMeasuresFolder.Name = "mls_SaveMeasuresFolder";
            mls_SaveMeasuresFolder.Size = new System.Drawing.Size(297, 23);
            mls_SaveMeasuresFolder.TabIndex = 10;
            // 
            // lbl_Classifications_ChooseModel
            // 
            lbl_Classifications_ChooseModel.AutoSize = true;
            lbl_Classifications_ChooseModel.Location = new System.Drawing.Point(12, 83);
            lbl_Classifications_ChooseModel.Name = "lbl_Classifications_ChooseModel";
            lbl_Classifications_ChooseModel.Size = new System.Drawing.Size(99, 15);
            lbl_Classifications_ChooseModel.TabIndex = 12;
            lbl_Classifications_ChooseModel.Text = "Choose Model . . ";
            toolTip1.SetToolTip(lbl_Classifications_ChooseModel, "Click here to open folder");
            lbl_Classifications_ChooseModel.Click += lbl_Classifications_ChooseModel_Click;
            // 
            // lbl_Measures_ChooseModel
            // 
            lbl_Measures_ChooseModel.AutoSize = true;
            lbl_Measures_ChooseModel.Location = new System.Drawing.Point(326, 83);
            lbl_Measures_ChooseModel.Name = "lbl_Measures_ChooseModel";
            lbl_Measures_ChooseModel.Size = new System.Drawing.Size(96, 15);
            lbl_Measures_ChooseModel.TabIndex = 13;
            lbl_Measures_ChooseModel.Text = "Choose Model . .";
            toolTip1.SetToolTip(lbl_Measures_ChooseModel, "Click here to Open Folder");
            lbl_Measures_ChooseModel.Click += lbl_Measures_ChooseModel_Click;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(12, 167);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(119, 15);
            label6.TabIndex = 14;
            label6.Text = "Classification Models";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(326, 167);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(122, 15);
            label7.TabIndex = 15;
            label7.Text = "Measurement Models";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(649, 146);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(101, 15);
            label8.TabIndex = 16;
            label8.Text = "More Information";
            // 
            // mls_cmPerformClassifications
            // 
            mls_cmPerformClassifications.AutoSize = true;
            mls_cmPerformClassifications.Location = new System.Drawing.Point(12, 9);
            mls_cmPerformClassifications.Name = "mls_cmPerformClassifications";
            mls_cmPerformClassifications.Size = new System.Drawing.Size(152, 19);
            mls_cmPerformClassifications.TabIndex = 17;
            mls_cmPerformClassifications.Text = "Perform Classifications?";
            mls_cmPerformClassifications.UseVisualStyleBackColor = true;
            mls_cmPerformClassifications.CheckedChanged += mls_PerformClassifications_CheckedChanged;
            // 
            // mls_cmPerformMeasures
            // 
            mls_cmPerformMeasures.AutoSize = true;
            mls_cmPerformMeasures.Location = new System.Drawing.Point(326, 9);
            mls_cmPerformMeasures.Name = "mls_cmPerformMeasures";
            mls_cmPerformMeasures.Size = new System.Drawing.Size(105, 19);
            mls_cmPerformMeasures.TabIndex = 18;
            mls_cmPerformMeasures.Text = "Run Measures?";
            mls_cmPerformMeasures.UseVisualStyleBackColor = true;
            mls_cmPerformMeasures.CheckedChanged += mls_PerformMeasures_CheckedChanged;
            // 
            // mls_ExistingAnnotations_1All_2Existing_3Not_Classification
            // 
            mls_ExistingAnnotations_1All_2Existing_3Not_Classification.Location = new System.Drawing.Point(12, 53);
            mls_ExistingAnnotations_1All_2Existing_3Not_Classification.Name = "mls_ExistingAnnotations_1All_2Existing_3Not_Classification";
            mls_ExistingAnnotations_1All_2Existing_3Not_Classification.Size = new System.Drawing.Size(59, 23);
            mls_ExistingAnnotations_1All_2Existing_3Not_Classification.TabIndex = 19;
            toolTip1.SetToolTip(mls_ExistingAnnotations_1All_2Existing_3Not_Classification, "Only perform new classifications on \"1 All, 2 with Existing Annos, 3 No annotations\"");
            // 
            // mls_ExistingAnnotations_1All_2Existing_3Not_Measure
            // 
            mls_ExistingAnnotations_1All_2Existing_3Not_Measure.Location = new System.Drawing.Point(326, 53);
            mls_ExistingAnnotations_1All_2Existing_3Not_Measure.Name = "mls_ExistingAnnotations_1All_2Existing_3Not_Measure";
            mls_ExistingAnnotations_1All_2Existing_3Not_Measure.Size = new System.Drawing.Size(71, 23);
            mls_ExistingAnnotations_1All_2Existing_3Not_Measure.TabIndex = 20;
            toolTip1.SetToolTip(mls_ExistingAnnotations_1All_2Existing_3Not_Measure, "Only run the measurements on \"1 All\", \"2 with existing annos\", \"3 with No annos\"");
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(326, 35);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(58, 15);
            label9.TabIndex = 22;
            label9.Text = "Add to . . ";
            toolTip1.SetToolTip(label9, "1=All, 2=Existing, 3=Empty");
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(12, 35);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(120, 15);
            label10.TabIndex = 21;
            label10.Text = "1 all, 2 existing, 3 non";
            toolTip1.SetToolTip(label10, "1=All, 2=Existing, 3=Empty");
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(212, 35);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(92, 15);
            label11.TabIndex = 24;
            label11.Text = "Only if Contains";
            toolTip1.SetToolTip(label11, "Enter a semicolon delimited list of annotations. Click this label to get them from the current scan.");
            label11.Click += ShowExistingAnnos_Click;
            // 
            // mls_cmClassification_OnlyExistingByName
            // 
            mls_cmClassification_OnlyExistingByName.Location = new System.Drawing.Point(128, 53);
            mls_cmClassification_OnlyExistingByName.Name = "mls_cmClassification_OnlyExistingByName";
            mls_cmClassification_OnlyExistingByName.Size = new System.Drawing.Size(171, 23);
            mls_cmClassification_OnlyExistingByName.TabIndex = 23;
            toolTip1.SetToolTip(mls_cmClassification_OnlyExistingByName, "Only classify if the annotation currently contains the following text");
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(531, 35);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(92, 15);
            label12.TabIndex = 26;
            label12.Text = "Only if Contains";
            toolTip1.SetToolTip(label12, "Enter a semicolon delimited list of annotations. Click this label to get them from the current scan.");
            label12.Click += ShowExistingAnnos_Click;
            // 
            // mls_Measure_OnlyExistingByName
            // 
            mls_Measure_OnlyExistingByName.Location = new System.Drawing.Point(452, 53);
            mls_Measure_OnlyExistingByName.Name = "mls_Measure_OnlyExistingByName";
            mls_Measure_OnlyExistingByName.Size = new System.Drawing.Size(171, 23);
            mls_Measure_OnlyExistingByName.TabIndex = 25;
            toolTip1.SetToolTip(mls_Measure_OnlyExistingByName, "Only run measurements if the annotation contains this text");
            // 
            // btn_Classifications_AddSingle
            // 
            btn_Classifications_AddSingle.Location = new System.Drawing.Point(12, 130);
            btn_Classifications_AddSingle.Name = "btn_Classifications_AddSingle";
            btn_Classifications_AddSingle.Size = new System.Drawing.Size(99, 23);
            btn_Classifications_AddSingle.TabIndex = 27;
            btn_Classifications_AddSingle.Text = "Add Single";
            btn_Classifications_AddSingle.UseVisualStyleBackColor = true;
            btn_Classifications_AddSingle.Click += btn_Classifications_AddSingle_Click;
            // 
            // btn_Classifications_AddFolder
            // 
            btn_Classifications_AddFolder.Location = new System.Drawing.Point(195, 130);
            btn_Classifications_AddFolder.Name = "btn_Classifications_AddFolder";
            btn_Classifications_AddFolder.Size = new System.Drawing.Size(104, 23);
            btn_Classifications_AddFolder.TabIndex = 28;
            btn_Classifications_AddFolder.Text = "Add Folder";
            toolTip1.SetToolTip(btn_Classifications_AddFolder, "First define a folder to search above. Will try to back out from a file that is already selected.");
            btn_Classifications_AddFolder.UseVisualStyleBackColor = true;
            btn_Classifications_AddFolder.Click += btn_Classifications_AddFolder_Click;
            // 
            // btn_Measures_AddFolder
            // 
            btn_Measures_AddFolder.Location = new System.Drawing.Point(519, 130);
            btn_Measures_AddFolder.Name = "btn_Measures_AddFolder";
            btn_Measures_AddFolder.Size = new System.Drawing.Size(104, 23);
            btn_Measures_AddFolder.TabIndex = 30;
            btn_Measures_AddFolder.Text = "Add Folder";
            toolTip1.SetToolTip(btn_Measures_AddFolder, "First define a folder to search above. Will try to back out from a file that is already selected.");
            btn_Measures_AddFolder.UseVisualStyleBackColor = true;
            btn_Measures_AddFolder.Click += btn_Measures_AddFolder_Click;
            // 
            // btn_Measures_AddSingle
            // 
            btn_Measures_AddSingle.Location = new System.Drawing.Point(326, 130);
            btn_Measures_AddSingle.Name = "btn_Measures_AddSingle";
            btn_Measures_AddSingle.Size = new System.Drawing.Size(99, 23);
            btn_Measures_AddSingle.TabIndex = 29;
            btn_Measures_AddSingle.Text = "Add Single";
            btn_Measures_AddSingle.UseVisualStyleBackColor = true;
            btn_Measures_AddSingle.Click += btn_Measures_AddSingle_Click;
            // 
            // btn_LoadSettings
            // 
            btn_LoadSettings.Location = new System.Drawing.Point(649, 342);
            btn_LoadSettings.Name = "btn_LoadSettings";
            btn_LoadSettings.Size = new System.Drawing.Size(99, 23);
            btn_LoadSettings.TabIndex = 31;
            btn_LoadSettings.Text = "Import Settings";
            btn_LoadSettings.UseVisualStyleBackColor = true;
            btn_LoadSettings.Click += btn_LoadSettings_Click;
            // 
            // btn_SaveSettings
            // 
            btn_SaveSettings.Location = new System.Drawing.Point(754, 342);
            btn_SaveSettings.Name = "btn_SaveSettings";
            btn_SaveSettings.Size = new System.Drawing.Size(99, 23);
            btn_SaveSettings.TabIndex = 32;
            btn_SaveSettings.Text = "Export Settings";
            btn_SaveSettings.UseVisualStyleBackColor = true;
            btn_SaveSettings.Click += btn_SaveSettings_Click;
            // 
            // mls_cmRaftBased
            // 
            mls_cmRaftBased.AutoSize = true;
            mls_cmRaftBased.Location = new System.Drawing.Point(649, 9);
            mls_cmRaftBased.Name = "mls_cmRaftBased";
            mls_cmRaftBased.Size = new System.Drawing.Size(86, 19);
            mls_cmRaftBased.TabIndex = 33;
            mls_cmRaftBased.Text = "Raft Based?";
            mls_cmRaftBased.UseVisualStyleBackColor = true;
            // 
            // mls_cmSaveSegOutputImages
            // 
            mls_cmSaveSegOutputImages.AutoSize = true;
            mls_cmSaveSegOutputImages.Location = new System.Drawing.Point(470, 391);
            mls_cmSaveSegOutputImages.Name = "mls_cmSaveSegOutputImages";
            mls_cmSaveSegOutputImages.Size = new System.Drawing.Size(154, 19);
            mls_cmSaveSegOutputImages.TabIndex = 37;
            mls_cmSaveSegOutputImages.Text = "Save Seg Output Images";
            toolTip1.SetToolTip(mls_cmSaveSegOutputImages, "If the measurement model generates output images, should we save those as well?");
            mls_cmSaveSegOutputImages.UseVisualStyleBackColor = true;
            // 
            // btn_Classifications_Remove
            // 
            btn_Classifications_Remove.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_Classifications_Remove.Location = new System.Drawing.Point(195, 157);
            btn_Classifications_Remove.Name = "btn_Classifications_Remove";
            btn_Classifications_Remove.Size = new System.Drawing.Size(104, 23);
            btn_Classifications_Remove.TabIndex = 40;
            btn_Classifications_Remove.Text = "Remove Selected";
            toolTip1.SetToolTip(btn_Classifications_Remove, "First define a folder to search above. Will try to back out from a file that is already selected.");
            btn_Classifications_Remove.UseVisualStyleBackColor = true;
            btn_Classifications_Remove.Click += btn_Multi_Remove;
            // 
            // btn_Measures_Remove
            // 
            btn_Measures_Remove.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_Measures_Remove.Location = new System.Drawing.Point(519, 157);
            btn_Measures_Remove.Name = "btn_Measures_Remove";
            btn_Measures_Remove.Size = new System.Drawing.Size(104, 23);
            btn_Measures_Remove.TabIndex = 41;
            btn_Measures_Remove.Text = "Remove Selected";
            toolTip1.SetToolTip(btn_Measures_Remove, "First define a folder to search above. Will try to back out from a file that is already selected.");
            btn_Measures_Remove.UseVisualStyleBackColor = true;
            btn_Measures_Remove.Click += btn_Multi_Remove;
            // 
            // btn_Cancel
            // 
            btn_Cancel.Location = new System.Drawing.Point(649, 418);
            btn_Cancel.Name = "btn_Cancel";
            btn_Cancel.Size = new System.Drawing.Size(77, 23);
            btn_Cancel.TabIndex = 34;
            btn_Cancel.Text = "Cancel";
            btn_Cancel.UseVisualStyleBackColor = true;
            btn_Cancel.Click += btn_Cancel_Click;
            // 
            // btn_SaveClose
            // 
            btn_SaveClose.Location = new System.Drawing.Point(732, 417);
            btn_SaveClose.Name = "btn_SaveClose";
            btn_SaveClose.Size = new System.Drawing.Size(77, 24);
            btn_SaveClose.TabIndex = 35;
            btn_SaveClose.Text = "Save Close";
            btn_SaveClose.UseVisualStyleBackColor = true;
            btn_SaveClose.Click += btn_SaveClose_Click;
            // 
            // btn_SaveRun
            // 
            btn_SaveRun.Location = new System.Drawing.Point(815, 417);
            btn_SaveRun.Name = "btn_SaveRun";
            btn_SaveRun.Size = new System.Drawing.Size(77, 24);
            btn_SaveRun.TabIndex = 36;
            btn_SaveRun.Text = "Save Run";
            btn_SaveRun.UseVisualStyleBackColor = true;
            btn_SaveRun.Click += btn_SaveRun_Click;
            // 
            // mls_svImageExt
            // 
            mls_svImageExt.Location = new System.Drawing.Point(857, 107);
            mls_svImageExt.Name = "mls_svImageExt";
            mls_svImageExt.Size = new System.Drawing.Size(35, 23);
            mls_svImageExt.TabIndex = 38;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(836, 89);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(58, 15);
            label1.TabIndex = 39;
            label1.Text = "extension";
            // 
            // lbl_Detail_Class
            // 
            lbl_Detail_Class.AutoSize = true;
            lbl_Detail_Class.ForeColor = Color.FromKnownColor(KnownColor.Highlight);
            lbl_Detail_Class.Location = new System.Drawing.Point(12, 379);
            lbl_Detail_Class.Name = "lbl_Detail_Class";
            lbl_Detail_Class.Size = new System.Drawing.Size(37, 15);
            lbl_Detail_Class.TabIndex = 42;
            lbl_Detail_Class.Text = "Detail";
            lbl_Detail_Class.Click += lbl_Detail_Model_Click;
            // 
            // lbl_Detail_Measure
            // 
            lbl_Detail_Measure.AutoSize = true;
            lbl_Detail_Measure.ForeColor = Color.FromKnownColor(KnownColor.Highlight);
            lbl_Detail_Measure.Location = new System.Drawing.Point(326, 367);
            lbl_Detail_Measure.Name = "lbl_Detail_Measure";
            lbl_Detail_Measure.Size = new System.Drawing.Size(37, 15);
            lbl_Detail_Measure.TabIndex = 43;
            lbl_Detail_Measure.Text = "Detail";
            lbl_Detail_Measure.Click += lbl_Detail_Model_Click;
            // 
            // mls_cmDeleteExistingAnnotations
            // 
            mls_cmDeleteExistingAnnotations.AutoSize = true;
            mls_cmDeleteExistingAnnotations.Location = new System.Drawing.Point(192, 416);
            mls_cmDeleteExistingAnnotations.Name = "mls_cmDeleteExistingAnnotations";
            mls_cmDeleteExistingAnnotations.Size = new System.Drawing.Size(103, 19);
            mls_cmDeleteExistingAnnotations.TabIndex = 44;
            mls_cmDeleteExistingAnnotations.Text = "Delete Existing";
            mls_cmDeleteExistingAnnotations.UseVisualStyleBackColor = true;
            // 
            // Form_MLSettings
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(910, 448);
            Controls.Add(mls_cmDeleteExistingAnnotations);
            Controls.Add(lbl_Detail_Measure);
            Controls.Add(lbl_Detail_Class);
            Controls.Add(btn_Measures_Remove);
            Controls.Add(btn_Classifications_Remove);
            Controls.Add(label1);
            Controls.Add(mls_svImageExt);
            Controls.Add(mls_cmSaveSegOutputImages);
            Controls.Add(btn_SaveRun);
            Controls.Add(btn_SaveClose);
            Controls.Add(btn_Cancel);
            Controls.Add(mls_cmRaftBased);
            Controls.Add(btn_SaveSettings);
            Controls.Add(btn_LoadSettings);
            Controls.Add(btn_Measures_AddFolder);
            Controls.Add(btn_Measures_AddSingle);
            Controls.Add(btn_Classifications_AddFolder);
            Controls.Add(btn_Classifications_AddSingle);
            Controls.Add(label12);
            Controls.Add(mls_Measure_OnlyExistingByName);
            Controls.Add(label11);
            Controls.Add(mls_cmClassification_OnlyExistingByName);
            Controls.Add(label9);
            Controls.Add(label10);
            Controls.Add(mls_ExistingAnnotations_1All_2Existing_3Not_Measure);
            Controls.Add(mls_ExistingAnnotations_1All_2Existing_3Not_Classification);
            Controls.Add(mls_cmPerformMeasures);
            Controls.Add(mls_cmPerformClassifications);
            Controls.Add(label8);
            Controls.Add(label7);
            Controls.Add(label6);
            Controls.Add(lbl_Measures_ChooseModel);
            Controls.Add(lbl_Classifications_ChooseModel);
            Controls.Add(label3);
            Controls.Add(mls_SaveMeasuresFolder);
            Controls.Add(label2);
            Controls.Add(mls_svImageFolder);
            Controls.Add(mls_cmRecordBackAnnotations);
            Controls.Add(txBx_Update);
            Controls.Add(mls_cmSaveImages_Overall);
            Controls.Add(mls_ModelPath_Measure);
            Controls.Add(txBx_MeasurementModels);
            Controls.Add(txBx_ClassificationModels);
            Controls.Add(mls_ModelPath_Classification);
            Name = "Form_MLSettings";
            Text = "ML Settings";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.TextBox mls_ModelPath_Classification;
        private System.Windows.Forms.TextBox txBx_ClassificationModels;
        private System.Windows.Forms.TextBox txBx_MeasurementModels;
        private System.Windows.Forms.TextBox mls_ModelPath_Measure;
        private System.Windows.Forms.CheckBox mls_cmSaveImages_Overall;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.CheckBox mls_cmRecordBackAnnotations;
        private System.Windows.Forms.TextBox mls_svImageFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox mls_SaveMeasuresFolder;
        private System.Windows.Forms.Label lbl_Classifications_ChooseModel;
        private System.Windows.Forms.Label lbl_Measures_ChooseModel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox mls_cmPerformClassifications;
        private System.Windows.Forms.CheckBox mls_cmPerformMeasures;
        private System.Windows.Forms.TextBox mls_ExistingAnnotations_1All_2Existing_3Not_Classification;
        private System.Windows.Forms.TextBox mls_ExistingAnnotations_1All_2Existing_3Not_Measure;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox mls_cmClassification_OnlyExistingByName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox mls_Measure_OnlyExistingByName;
        private System.Windows.Forms.Button btn_Classifications_AddSingle;
        private System.Windows.Forms.Button btn_Classifications_AddFolder;
        private System.Windows.Forms.Button btn_Measures_AddFolder;
        private System.Windows.Forms.Button btn_Measures_AddSingle;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.CheckBox mls_cmRaftBased;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_SaveClose;
        private System.Windows.Forms.Button btn_SaveRun;
        private System.Windows.Forms.CheckBox mls_cmSaveSegOutputImages;
        private System.Windows.Forms.TextBox mls_svImageExt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Classifications_Remove;
        private System.Windows.Forms.Button btn_Measures_Remove;
        private System.Windows.Forms.Label lbl_Detail_Class;
        private System.Windows.Forms.Label lbl_Detail_Measure;
        private System.Windows.Forms.CheckBox mls_cmDeleteExistingAnnotations;
    }
}