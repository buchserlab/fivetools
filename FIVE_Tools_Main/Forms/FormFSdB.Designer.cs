﻿namespace FIVE
{
    partial class FormFSdB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Browse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fs_Port = new System.Windows.Forms.TextBox();
            this.fs_Username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fs_Password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fs_Filetoload = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Import = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.bgWorkerImport = new System.ComponentModel.BackgroundWorker();
            this.fs_dbname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_DeleteTables = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Browse
            // 
            this.btn_Browse.Location = new System.Drawing.Point(180, 175);
            this.btn_Browse.Name = "btn_Browse";
            this.btn_Browse.Size = new System.Drawing.Size(75, 23);
            this.btn_Browse.TabIndex = 0;
            this.btn_Browse.Text = "browse";
            this.btn_Browse.UseVisualStyleBackColor = true;
            this.btn_Browse.Click += new System.EventHandler(this.btn_Browse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Port";
            // 
            // fs_Port
            // 
            this.fs_Port.Location = new System.Drawing.Point(12, 130);
            this.fs_Port.Name = "fs_Port";
            this.fs_Port.Size = new System.Drawing.Size(77, 23);
            this.fs_Port.TabIndex = 2;
            // 
            // fs_Username
            // 
            this.fs_Username.Location = new System.Drawing.Point(95, 130);
            this.fs_Username.Name = "fs_Username";
            this.fs_Username.Size = new System.Drawing.Size(77, 23);
            this.fs_Username.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Username";
            // 
            // fs_Password
            // 
            this.fs_Password.Location = new System.Drawing.Point(178, 130);
            this.fs_Password.Name = "fs_Password";
            this.fs_Password.PasswordChar = '*';
            this.fs_Password.Size = new System.Drawing.Size(77, 23);
            this.fs_Password.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Password";
            // 
            // fs_Filetoload
            // 
            this.fs_Filetoload.Location = new System.Drawing.Point(12, 204);
            this.fs_Filetoload.Name = "fs_Filetoload";
            this.fs_Filetoload.Size = new System.Drawing.Size(243, 23);
            this.fs_Filetoload.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "File to Load";
            // 
            // btn_Import
            // 
            this.btn_Import.Location = new System.Drawing.Point(117, 259);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.Size = new System.Drawing.Size(66, 23);
            this.btn_Import.TabIndex = 9;
            this.btn_Import.Text = "Import";
            this.btn_Import.UseVisualStyleBackColor = true;
            this.btn_Import.Click += new System.EventHandler(this.btn_Import_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(189, 259);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(66, 23);
            this.btn_Cancel.TabIndex = 10;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // txBx_Update
            // 
            this.txBx_Update.Location = new System.Drawing.Point(12, 288);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.Size = new System.Drawing.Size(243, 150);
            this.txBx_Update.TabIndex = 11;
            // 
            // bgWorkerImport
            // 
            this.bgWorkerImport.WorkerReportsProgress = true;
            this.bgWorkerImport.WorkerSupportsCancellation = true;
            this.bgWorkerImport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerImport_DoWork);
            this.bgWorkerImport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerImport_ProgressChanged);
            this.bgWorkerImport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerImport_RunWorkerCompleted);
            // 
            // fs_dbname
            // 
            this.fs_dbname.Location = new System.Drawing.Point(178, 75);
            this.fs_dbname.Name = "fs_dbname";
            this.fs_dbname.Size = new System.Drawing.Size(77, 23);
            this.fs_dbname.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "dbName";
            // 
            // btn_DeleteTables
            // 
            this.btn_DeleteTables.Location = new System.Drawing.Point(12, 244);
            this.btn_DeleteTables.Name = "btn_DeleteTables";
            this.btn_DeleteTables.Size = new System.Drawing.Size(66, 38);
            this.btn_DeleteTables.TabIndex = 14;
            this.btn_DeleteTables.Text = "Delete Tables";
            this.btn_DeleteTables.UseVisualStyleBackColor = true;
            this.btn_DeleteTables.Click += new System.EventHandler(this.btn_DeleteTables_Click);
            // 
            // FormFSdB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_DeleteTables);
            this.Controls.Add(this.fs_dbname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txBx_Update);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Import);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fs_Filetoload);
            this.Controls.Add(this.fs_Password);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fs_Username);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fs_Port);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Browse);
            this.Name = "FormFSdB";
            this.Text = "FIVE FS dB";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Browse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fs_Port;
        private System.Windows.Forms.TextBox fs_Username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fs_Password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fs_Filetoload;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Import;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.ComponentModel.BackgroundWorker bgWorkerImport;
        private System.Windows.Forms.TextBox fs_dbname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_DeleteTables;
    }
}