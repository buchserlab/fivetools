﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIVE_Tools_Main
{
    public class BMS_Barcode_Tracker
    {
        public double Mutants;
        public double Barcodes;
        public double RatioParticles;
        public double ActualParticles_Barcodes;

        public int Total_Barcodes = 0;
        public int NonEmpty_Barcodes = 0;
        public int Usable_Barcodes = 0;

        public double AvgBarcodes_All_PerMutant;
        public double AvgBarcodes_Safe_PerMutant;
        public double Fraction_Mutants_NoBarcode;

        public double Fraction_Barcodes_Empty { get => (double)(Total_Barcodes - NonEmpty_Barcodes) / Total_Barcodes; }
        public double Fraction_Barcodes_Collision { get => (double)(NonEmpty_Barcodes - Usable_Barcodes) / Total_Barcodes; }

        public HashSet<int> BarcodesCollision = new HashSet<int>();

        public BMS_Barcode_Tracker(int Total)
        {
            this.Total_Barcodes = Total;
        }

        public void AddNewNonEmpty()
        {
            NonEmpty_Barcodes++;
            Usable_Barcodes++;
        }

        public void AddCollision(int BarcodeID)
        {
            Usable_Barcodes--;
            BarcodesCollision.Add(BarcodeID);
        }

        internal string ExportLine(bool HeaderLine)
        {
            char d = '\t';
            if (HeaderLine)
            {
                return "Mutants" + d + "Barcodes" + d + "RatioParticles" + d + "ActualParticles_Barcodes" + d + "NonEmpty_Barcodes" + d + "Usable_Barcodes" + d + "AvgBarcodes_All_PerMutant" + d + "AvgBarcodes_Safe_PerMutant" + d + "Fraction_Mutants_NoBarcode" + d + "Fraction_Barcodes_Collision" + d + "Fraction_Barcodes_Empty" + "\r\n";
            }
            else
            {
                return "" + Mutants + d + Barcodes + d + RatioParticles + d + ActualParticles_Barcodes + d + NonEmpty_Barcodes + d + Usable_Barcodes + d + AvgBarcodes_All_PerMutant + d + AvgBarcodes_Safe_PerMutant + d + Fraction_Mutants_NoBarcode + d + Fraction_Barcodes_Collision + d + Fraction_Barcodes_Empty + "\r\n";
            }
        }
    }

    public class BMS_Barcode
    {
        public BMS_Barcode_Tracker Tracker;
        public Dictionary<int, int> Mutations;
        public int Count_Mutations { get => Mutations.Count; }
        public int TotalCount = 0;
        public int ID;

        public bool Usable
        {
            get
            {
                return true;
            }
        }

        public BMS_Barcode(int ID, int Mutation, BMS_Barcode_Tracker Tracker)
        {
            this.ID = ID;
            this.Tracker = Tracker;
            Mutations = new Dictionary<int, int>(1);
            AddMutation(Mutation);
        }

        public void AddMutation(int MutationID)
        {
            TotalCount++;
            if (Mutations.ContainsKey(MutationID))
            {
                Mutations[MutationID]++;
            }
            else
            {
                Mutations.Add(MutationID, 1);
            }
            if (TotalCount == 1)
            {
                Tracker.AddNewNonEmpty();
                
            }
            else
            {
                if (Mutations.Count == 2)
                {
                    Tracker.AddCollision(ID);
                }
            }
        }

    }

    public static class BarcodeMutantSim
    {
        private static Random Rand = new Random();

        public static void main()
        {
            BMS_Barcode_Tracker res;

            int Mutants = 8000;
            //int Barcodes = 100000;
            //double Ratio_BarcodesToMutants_Particles = 8;
            //res = SingleRun(Mutants, Barcodes, Ratio_BarcodesToMutants_Particles); //28 seconds

            StringBuilder SB = null;
            for (int bcs = Mutants*10; bcs <= Mutants * (10*3*3); bcs*=3)
            {
                for (int ratios = 1000; ratios <= (1000*5*5); ratios*=5)
                {
                    Debug.Print(bcs + " " + ratios);
                    res = SingleRun(Mutants, bcs, ratios);
                    if (SB == null)
                    {
                        SB = new StringBuilder();
                        SB.Append(res.ExportLine(true));
                    }
                    SB.Append(res.ExportLine(false));
                }
            }
            Debug.Print(". . Writing . . ");
            System.IO.File.WriteAllText(@"c:\temp\RSMC_Sim3.txt",SB.ToString());
            Debug.Print("Done!");
        }

        public static BMS_Barcode_Tracker SingleRun(double Mutants, double Barcodes, double Ratio_BarcodesToMutants_Particles)
        {
            double Mult_Barcodes = 10000; //With 8000 particles, 10X gives us Attomoles, 10000X gives us FEMTO Moles
            double Particles_Barcodes = Barcodes * Mult_Barcodes;
            double Particles_Mutants = Particles_Barcodes / Ratio_BarcodesToMutants_Particles;
            double Mult_Mutants = Particles_Mutants / Mutants;

            List<int> ListMutants = new List<int>((int)Particles_Mutants);
            for (int i = 0; i < Mutants; i++) for (int j = 0; j < Mult_Mutants; j++) ListMutants.Add(i);

            //Track the Mutations
            Dictionary<int, HashSet<int>> Mutations_Hash = new Dictionary<int, HashSet<int>>((int)Mutants);

            //Track the Barcodes
            Dictionary<int, BMS_Barcode> Barcode_Hash = new Dictionary<int, BMS_Barcode>();
            int Pick; int Barcode;
            BMS_Barcode_Tracker Tracker = new BMS_Barcode_Tracker((int)Barcodes);
            Tracker.Mutants = Mutants; Tracker.Barcodes = Barcodes; Tracker.RatioParticles = Ratio_BarcodesToMutants_Particles; Tracker.ActualParticles_Barcodes = Particles_Barcodes;
            for (int i = 0; i < ListMutants.Count; i++)
            {
                //See which Barcode is picked
                Pick = Rand.Next(0, (int)Particles_Barcodes);
                Barcode = (int)(Pick / Mult_Barcodes);
                //Assign this mutation to the Barcode
                if (!Barcode_Hash.ContainsKey(Barcode))
                {
                    Barcode_Hash.Add(Barcode, new BMS_Barcode(Barcode, ListMutants[i], Tracker));
                }
                else
                {
                    Barcode_Hash[Barcode].AddMutation(ListMutants[i]);
                }
                //Assign this Barcode to the Mutant
                if (!Mutations_Hash.ContainsKey(ListMutants[i])) Mutations_Hash.Add(ListMutants[i], new HashSet<int>());
                Mutations_Hash[ListMutants[i]].Add(Barcode);
            }

            //Now get stats per Mutation:
            double SumBarcodesAll = 0; double SumBarcodesSafe = 0;  int Empty = 0;
            foreach (KeyValuePair<int, HashSet<int>> KVP in Mutations_Hash)
            {
                if (KVP.Value != null)
                {
                    SumBarcodesAll += KVP.Value.Count;

                    //First remove any collision barcodes:
                    KVP.Value.ExceptWith(Tracker.BarcodesCollision);

                    //Now count stuff up
                    SumBarcodesSafe += KVP.Value.Count;
                    if (KVP.Value.Count == 0) Empty++;
                } else
                {
                    Empty++;
                }
            }

            Tracker.AvgBarcodes_All_PerMutant = SumBarcodesAll / Mutations_Hash.Count;
            Tracker.AvgBarcodes_Safe_PerMutant = SumBarcodesSafe / Mutations_Hash.Count;
            Tracker.Fraction_Mutants_NoBarcode = (double)Empty / Mutations_Hash.Count;
            Tracker.BarcodesCollision = null; //Just so we don't take up too much memory
            return Tracker;
        }
    }


}
