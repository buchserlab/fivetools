# Library Imports
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import toolbox # or whatever name of this python file is
    import inspect
    import numpy as np
    import pandas as pd
    import random
    import os
    import time
    from datetime import datetime
    import matplotlib.pyplot as plt
    from matplotlib.colors import ListedColormap, LinearSegmentedColormap
    from mpl_toolkits.mplot3d import Axes3D
    import seaborn as sns
    import plotly.express as px
    import plotly.graph_objects as go
    import plotly.figure_factory as ff
    from plotly.subplots import make_subplots
    from umap import UMAP
    from scipy import stats
    from scipy import cluster
    from scipy.spatial.distance import pdist, squareform
    import scikit_posthocs as sp
    import statsmodels
    import statsmodels.api as sm
    import statsmodels.formula.api as smf
    from statsmodels.stats.outliers_influence import variance_inflation_factor
    from statsmodels.tools.tools import add_constant
    import pylab as py
    from sklearn.base import BaseEstimator, TransformerMixin
    from sklearn.preprocessing import MinMaxScaler, StandardScaler
    from sklearn.impute import SimpleImputer
    from sklearn.utils import shuffle
    from sklearn.feature_selection import VarianceThreshold, SelectKBest, f_classif, SequentialFeatureSelector, RFECV, SelectFromModel
    from sklearn.compose import ColumnTransformer
    from sklearn.calibration import CalibratedClassifierCV
    from imblearn.pipeline import Pipeline
    from imblearn.over_sampling import SMOTE
    import harmonypy as hm
    from sklearn.linear_model import LogisticRegression, SGDClassifier, RidgeClassifier, SGDOneClassSVM
    from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, IsolationForest
    from sklearn.naive_bayes import GaussianNB, MultinomialNB
    from sklearn.svm import SVC, LinearSVC, OneClassSVM
    from sklearn.neighbors import LocalOutlierFactor
    from sklearn.decomposition import PCA
    from sklearn.model_selection import train_test_split, KFold, StratifiedKFold, cross_val_score, GridSearchCV, RandomizedSearchCV
    import sklearn.metrics as metrics
    from joblib import dump, load, Parallel, delayed


# User-Defined Functions
def import_df(exps, meta_data=[], features=[], drop_mismatches=False, file_name='_CellsUse', extension='.csv', sep=',', low_memory=False, dir='R:\\FIVE\\EXP\\', sub_dir='\\2 MLModels\\'):
    df_agg = pd.DataFrame()
    cols_agg = []
    for x in exps:
        PATH = dir+x+sub_dir+x+file_name+extension
        df = pd.read_csv(PATH, sep=sep, low_memory=low_memory)
        df.columns = [" ".join(c.split()) for c in df.columns.tolist()]
        df.columns = df.columns.str.upper()
        if len(features)==0:
            feat = [i for i in df.columns.tolist() if 'WV' in i]
        else:
            feat = features
        if len(meta_data)==0:
            meta = [i for i in df.columns.tolist() if 'WV' not in i]
        else:
            meta = meta_data
        cols = meta+feat
        cols_agg.append(cols)
        df = df[cols].reset_index(drop=True)
        df_agg = pd.concat((df_agg, df), axis=0).reset_index(drop=True)

    mismatched_features = {}
    for i, lst in enumerate(cols_agg):
        for value in lst:
            for j, other_list in enumerate(cols_agg):
                if j != i and value not in other_list:
                    if value not in mismatched_features:
                        mismatched_features[value] = [exps[j]]
                    else:
                        if exps[j] not in mismatched_features[value]:
                            mismatched_features[value].append(exps[j])
    if len(mismatched_features)>0:
        if drop_mismatches==True:
            df_agg = df_agg.drop(list(mismatched_features.keys()), axis=1)
        else:
            output = []
            for value, ids in mismatched_features.items():
                output.append(f"{value} is absent from {', '.join(ids)}")
            print('WARNING:')
            display(output)
    return(df_agg)

def wrangle(
    data, # dataframe
    condition, # the underlying condition which determines the label (i.e. 'NAME').
    label='MLCLASSLAYOUT', # for machine learning, usually 'MLCLASSLAYOUT'
    normalize=True, # boolean
    normalize_by='PLATE ID', # usally 'PLATE ID' but could be a level lower or higher
    relabel={}, # dictionary remapping a key condition to a value label. 
    training_labels=['G1', 'G2'], # which labels do you want to train on?
    label_map={'G1': 0, 'G2' : 1}, # dicitionary mapping each training label to an integer
    leaky_features=['BORDER', 'CG'], # Feature identifiers that leak the label to a model
    remove_wavelengths=['WVH', 'WVB'], # list of WV_'s that should be removed
    remove_organelles=['MITONETWORKS'], # list of NUCLEI, CELL, MITONETWORKS... that should be removed
    remove_bckg_features=True, # boolean, to remove any feature that measure background
    remove_features=[], # list of specific features to remove (whole name)
    include_features=[], # list of specific features to include (whole name)
    include_features_only=False, # if set to True, only the features listed in include_features will be kept
    exp='FIVNUMBER', # what feature should define the highest order meta data
    plate = 'PLATE ID', # what feature should define the plate likely PLATE ID, PLATEIDX, or PLATEID)
    remove_exps=[], # remove whole experiment if remove_plates and remove_conditions are empty. 
    remove_plates=[], # remove whole plate from remove_exps if remove_conditions is empty. 
    remove_conditions=[], # remove conditions from remove_plates or remove_exps. If remove_plates and remove_exps are empty, all occurences of the condition(s) will be removed. 
    keep_predict_label=False, # labels that are not used in training are left in the training dataframe if set to True
    remove_na_cols=False, # handle null values by removing whole features
    remove_na_rows=False, # handle null values by removing whole cells
    impute_na=False, # handle null values by imputing the median value
    impute_method='constant', # this will only apply to numerical columns, specifically the features that are specified by the criterion above. Same optinos as scikit-learn's Simple Imputer
    fill_value=0, # applies if impute_method is 'constant'
    show_arguments=False
    ):
    if show_arguments:
        current_locals = locals().copy()
        del current_locals['show_arguments']
        print(current_locals)
    # Columns
    if len(include_features) > 0:
        data_include = data[include_features]
    for lky in leaky_features:
        data = data.drop(data.filter(like=lky).columns, axis=1)
    for wv in remove_wavelengths:
        data = data.drop(data.filter(like=wv).columns, axis=1)
    for org in remove_organelles:
        data = data.drop(data.filter(like=org).columns, axis=1)
    for rmv in remove_features:
        data = data.drop(data.filter(like=rmv).columns, axis=1)
    if len(include_features) > 0 and include_features_only:
        data = data[include_features + [label]]
    if remove_bckg_features:
        data = data.drop(data.filter(like='BCKG').columns, axis=1)
    if len(include_features) > 0:
        data = pd.concat([data, data_include], axis=1)
    features = [i for i in data.columns.tolist() if 'WV' in i]
    # Rows
    if remove_exps and not (remove_plates or remove_conditions):
        data = data[~data[exp].isin(remove_exps)]
    elif remove_plates and not remove_conditions:
        data = data[~data[plate].isin(remove_plates)]
    elif remove_conditions and remove_plates and not remove_exps:
        data = data[~(data[condition].isin(remove_conditions) & data[plate].isin(remove_plates))]
    elif remove_conditions and remove_exps and not remove_plates:
        data = data[~(data[condition].isin(remove_conditions) & data[exp].isin(remove_exps))]
    elif remove_conditions and not (remove_exps or remove_plates):
        data = data[~data[condition].isin(remove_conditions)]
    # Normalize
    if normalize:
        scaler = StandardScaler()
        for f in features:
            data[f] = data.groupby(normalize_by)[f].transform(lambda x: pd.Series(scaler.fit_transform(x.values.reshape(-1, 1)).flatten(), index=x.index))
    # Imputate NaN
    if impute_na and features:
        imputer = SimpleImputer(missing_values=np.nan, strategy=impute_method)
        data[features] = imputer.fit_transform(data[features])
    # Remove NaN Rows
    if remove_na_rows==True:
        data = data.dropna(axis=0, how='any', subset=features)
    # Remove NaN Columns
    if remove_na_cols==True:
        data = data.dropna(axis=1, how='any', subset=features)
    # Remove non-training labels:
    if keep_predict_label==False:
        data = data[data[label].isin(training_labels)].reset_index(drop=True)
    # Dataframes for machine learning
    X = data[features]
    y = data[label].map(label_map)
    return(data, X, y)

def define_labels(data, condition, g1, g2=[], balance=False, test_size=0, gui=['FIVNUMBER', 'PLATE IDX', 'WELL IDX']):
    # Create GUI for well
    # level_list = [data[i].apply(lambda x: f'{x:05d}') for i in gui]
    level_list = [data[i].apply(lambda x: f'{int(x):05d}' if isinstance(x, (int, float)) else str(x)) for i in gui]
    data['LEVEL ID'] = pd.Series([''.join(items) for items in zip(*level_list)])
    # stratify_list = [data[i].apply(lambda x: f'{x:05d}') for i in gui[:-1]]
    stratify_list = [data[i].apply(lambda x: f'{int(x):05d}' if isinstance(x, (int, float)) else str(x)) for i in gui[:-1]]
    data['STRATIFY'] = pd.Series([''.join(items) for items in zip(*stratify_list)])
    data = data.drop([i for i in data.columns.tolist() if '_gui' in i], axis=1)
    # Group by well, pull condition and MLCLASSLAYOUT
    agg_cols = ['STRATIFY', condition]
    result = data.groupby('LEVEL ID').agg([list])[agg_cols].reset_index()
    for col in agg_cols:
        result[(col, 'list')] = result[(col, 'list')].apply(lambda x: list(set(x))[0] if len(set(x)) == 1 else None)
    result.columns = [col[0] if col[1] == '' else col[0] for col in result.columns.values]
    if None in result[condition].unique():
        print("WARNING: 'LEVEL ID' is not 1:1 with condition variable")
    # Class application
    mlclasslayout = []
    for i in result[condition]:
        if i in g1:
            mlclasslayout.append('G1')
        elif i in g2:
            mlclasslayout.append('G2')
        else:
            mlclasslayout.append('MixS')
    result['MLCLASSLAYOUT'] = mlclasslayout
    # Stratified sample and/or downsample
    train = result[result['MLCLASSLAYOUT']!='MixS']
    inf = result[result['MLCLASSLAYOUT']=='MixS']
    if test_size>0:
        train, test = train_test_split(train, test_size=test_size, random_state=42, stratify=train['STRATIFY']) # Take a stratified sample. Stratify by gui[:-1] (i.e. 'PLATE IDX' and 'FIVNUMBER)
    if balance==True:
        n = np.round(train['MLCLASSLAYOUT'].value_counts().min()) # Find the lowest represented class
        train = train.groupby('MLCLASSLAYOUT').apply(lambda x: x.sample(n, random_state=42)).reset_index(drop=True) # Downsample majority class to balance
    result = pd.concat((train, inf), axis=0).reset_index(drop=True) # New result (i.e. df where WELL = row). All inf + selected wells.
    # Apply new class layout to cell data
    data = data.merge(result, how='left', on='LEVEL ID')
    data[condition] = data[condition+'_y']
    data['MLCLASSLAYOUT'] = data['MLCLASSLAYOUT_y']
    data = data.drop([condition+'_x', condition+'_y', 'MLCLASSLAYOUT_x', 'MLCLASSLAYOUT_y', 'STRATIFY_x', 'STRATIFY_y'], axis=1)
    data = data.dropna(subset=['MLCLASSLAYOUT'])
    return(data)

def cph(source_df, rank_on, rank_by, categories=[], width=6.4, height=4.8, color_map={'MixS': 'green', 'G1': 'blue', 'G2': 'red'}):
    source_df['RANK'] = ""
    if len(categories)>0:
        pools = categories
        matches = []
        for i in range(0,len(categories)):
            matches.append(source_df[source_df[rank_by]==categories[i]])
        source_df = pd.concat(matches)
    else:
        pools = [p for p in source_df[rank_by].unique() if len(source_df[source_df[rank_by]==p]) > 1]
    fig, ax = plt.subplots(figsize=(width, height), layout='constrained')
    # plt.figure(figsize=(width, height))
    for p in pools:
        # Select feautre vector
        measurements = source_df[source_df[rank_by]==p][rank_on].sort_values(ascending=True)
        # Rank feature vector, add some randomness so no repeating values and normalize
        pool_rank = measurements.rank()+np.random.rand(len(source_df[source_df[rank_by]==p][rank_by]))/100000
        pool_rank_norm = pd.Series(1-(pool_rank-pool_rank.min())/(pool_rank.max()-pool_rank.min()), name='RANK')
        source_df.update(pool_rank_norm)
        plt.plot(measurements, pool_rank_norm, label=p, color=color_map[p])
    plt.legend(pools)
    plt.title(rank_on)
    plt.show()

def train(
    X,
    y,
    MODEL_PATH, 
    classifiers, 
    n_models, 
    test_size=0.3,
    random_state=42,
    cv=5,
    n_jobs=-1,
    scoring = ['roc_auc', 'f1'],
    refit='roc_auc',
    n_random_features=None, 
    scale=False, 
    smote=False, 
    var_threshold=None, # float between 0 and 1
    coln_threshold=None, # float between 0 and 1
    kbest=None, # integer
    l1_max_feat=None, # integer
    rfecv_min_feat=None, # integer
    rfecv_step=None, # integer
    calibrate_method=None, # i.e. 'isotonic'
    search_cv=None,
    hyperparameters={
        RandomForestClassifier : {
            'Classifier__n_estimators':[10, 50, 100, 200],
            'Classifier__max_depth': [None, 5, 10, 20],
            'Classifier__min_samples_split':[2, 5, 10],
            'Classifier__min_samples_leaf':[1, 2, 4],
        },
        LogisticRegression : {
            'Classifier__penalty': ['l1', 'l2', 'elasticnet'],
            'Classifier__C': [0.01, 0.1, 1, 10, 100],
            'Classifier__solver': ['saga'],
        },
        SVC : {
            'Classifier__C': [0.01, 0.1, 1, 10, 100],
            'Classifier__kernel': ['linear', 'rbf'],
            'Classifier__gamma': ['scale', 'auto'],
            'Classifier__probability': [True]
        },
        OneClassSVM : {
            'Classifier__nu': [0.01, 0.1, 0.5],
            'gamma': [0.01, 0.1, 1, 'scale', 'auto'],
            'kernel': ['linear', 'rbf', 'poly', 'sigmoid']
        },
        IsolationForest : {
            'Classifier__contamination': [0.5]    
        }
    }
    ):
    for i in range(1, n_models+1):
        sample_cols = X.sample(n=n_random_features, axis=1, replace=False).columns.tolist()
        classifier = random.choice(classifiers)
        steps=[]
        if n_random_features is not None:
            steps.append(('Random', ColumnTransformer([('Sample', 'passthrough', sample_cols)],remainder="drop")),)
        if scale==True:
            steps.append(('MinMaxScaler', MinMaxScaler()))
        if smote==True:
            steps.append(('SMOTE', SMOTE()))
        if var_threshold is not None:
            steps.append(('VarianceThreshold', VarianceThreshold(var_threshold)))
        if coln_threshold is not None:
            steps.append(('CorrelationThreshold', CollinearityFilter(threshold=coln_threshold)))
        if kbest is not None:
            steps.append(('KBest', SelectKBest(k=kbest)),)
        if l1_max_feat is not None:
            steps.append(('L1', SelectFromModel(LinearSVC(C=1.0, penalty='l1', dual=False), max_features=l1_max_feat)))
        if rfecv_min_feat and rfecv_step is not None:
            steps.append(('RFECV', RFECV(estimator=classifier(), step=rfecv_step, min_features_to_select=rfecv_min_feat, cv=cv, n_jobs=n_jobs)))
        if calibrate_method is not None:
            steps.append(('Calibrate', CalibratedClassifierCV(base_estimator=classifier(), method=calibrate_method, cv=cv, n_jobs=n_jobs, ensemble=True)))
        steps.append(('Classifier', classifier()))
        pipeline = Pipeline(steps)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
        if search_cv is not None:
            pipeline = search_cv(pipeline, hyperparameters[classifier], cv=cv, scoring=scoring, refit=refit, n_jobs=n_jobs)
        if y.nunique() > 1:
            pipeline.fit(X_train, y_train)
            y_test_pred = pipeline.predict(X_test)
            y_test_scores = pipeline.predict_proba(X_test)[:,1]
            mcc = metrics.matthews_corrcoef(y_test, y_test_pred)
            auc = metrics.roc_auc_score(y_test, y_test_scores)
            model_name = ''.join([char for char in str(classifier)])[[model for model in range(0,len(str(classifier))) if str(classifier)[model].isupper()][0]:-2]+' (MCC='+str(round(mcc,5))+', AUC='+str(round(auc,5))+')'
        else:
            pipeline.fit(X_train)
            y_test_pred = pipeline.predict(X_test)
            y_test_scores = pipeline.decision_function(X_test)
            model_name = ''.join([char for char in str(classifier)])[[model for model in range(0, len(str(classifier))) if str(classifier)[model].isupper()][0]:-2] + ' ' + str(int(random.random() * (10**15)))
        dump(pipeline, MODEL_PATH+model_name+'.pkl')

def infer(data, MODEL_PATH, n_classes, model_ext='.pkl', export=False, export_name='Cells_Inf', export_type='csv'):
    models = [f for f in os.listdir(MODEL_PATH) if f.endswith(model_ext)]
    results = pd.DataFrame(columns=models)
    count=0
    print("Inferring: ", end='', flush=True)
    for m in models:
        count+=1
        pipeline = load(MODEL_PATH+m)
        if n_classes > 1:
            results[m] = pipeline.predict_proba(data)[:,1]
        else:
            results[m] = pipeline.decision_function(data)
        percentage = np.round(count / len(models) * 100, 2)
        print(f'\r{percentage}% Inferred ', end='', flush=True)
    results = pd.concat((data, results), axis=1)
    if export==True:
        results.to_csv(MODEL_PATH+'\\'+export_name, index=False)
        print()
        print("Exporting")
        print("DONE")
    return(results)

# Dictionary: summary of import
modules = [val for name, val in toolbox.__dict__.items() if inspect.ismodule(val)]
classes = [obj for name, obj in inspect.getmembers(toolbox) if inspect.isclass(obj) and obj.__module__ != 'toolbox']
functions = [obj for name, obj in inspect.getmembers(toolbox) if inspect.isfunction(obj)]
toolbox_dict = {
    "modules": [m.__name__ for m in modules],
    "classes": [f"{c.__name__} (from {c.__module__})" for c in classes],
    "functions": [f"{f.__name__} (from {f.__module__})" for f in functions]
}