# - - - - - - - Imports
import matplotlib.pyplot as plt
import seaborn as sns

import os 
import time
from datetime import datetime
import json
from typing import Dict
import re #regular Expressions
import math
import csv
from random import randint
import random
from IPython.display import clear_output
from PIL import Image

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import class_weight
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, confusion_matrix
from scipy.ndimage import binary_dilation, shift
import matplotlib.lines as lines
import umap.umap_ as umap
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2
from tensorflow.keras.metrics import MeanAbsoluteError, MeanSquaredError, Mean
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras import backend as K
from tensorflow.keras.losses import binary_crossentropy
import tf2onnx

# - - - - - - - Function Definitions - - Standard set - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - 

# summarize history for accuracy, loss
def QPlot_Loss(hst, vS, log_scale=True):
    plt.figure()
    plt.plot(hst.history[vS])
    try:
        plt.plot(hst.history['val_'+vS]) #may not be a val version
    except KeyError:
        pass
    plt.title('model ' + vS)
    plt.ylabel(vS)
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid()
    #if log_scale:
    #    plt.yscale('log')
    plt.show()

def QPlot_Loss2(ax, hst, vS, log_scale=True, ScaleEpochsAfter=1):
    ax.plot(range(len(hst.history[vS])), hst.history[vS])
    try:
        ax.plot(range(len(hst.history['val_'+vS])), hst.history['val_'+vS]) 
    except KeyError:
        pass

    ax.set_title('model ' + vS)
    ax.set_ylabel(vS)
    ax.set_xlabel('epoch')
    ax.legend(['train', 'test'], loc='upper left')
    ax.grid()
    
    y_values = hst.history[vS] if ScaleEpochsAfter <= 0 else hst.history[vS][ScaleEpochsAfter:]
    y_values_test = hst.history.get('val_'+vS, len(y_values))
    y_all = y_values + y_values_test

    if log_scale and all(i > 0 for i in y_all):
        ax.set_yscale('log')

    y_lower, y_upper = np.percentile(y_all, [0, 100])
    ax.set_ylim(y_lower, y_upper)

def QGetBestEpoch_Val(hst, metric_check, mectric_report):
    try:
        min_v = min(hst.history[metric_check])
        min_idx = [i for i, j in enumerate(hst.history[metric_check]) if j == min_v]
        min_idx = min_idx[0]
        best_val_acc = hst.history[mectric_report][min_idx]
        return min_idx, best_val_acc, min_v
    except:
        return -1,0,0

def QWorstCase_Accuracy(hst, FinalEpochToAverage, NumberToAverage, metric):
    n = min(len(hst.history['loss']), FinalEpochToAverage)
    min_idx, best_val_acc, _ = QGetBestEpoch_Val(hst,'val_loss', 'val_' + metric)
    last_accuracy_L = last_accuracy_E = 0
    for i in range(0,NumberToAverage):
        last_accuracy_L += hst.history[metric][n-i-1]
        last_accuracy_E += hst.history['val_'+metric][n-i-1]
    return min(last_accuracy_E, last_accuracy_L) / NumberToAverage, n, best_val_acc

# - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - -  Save model stuff  - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - 

def save_metrics_to_csv_raft(hist, best_epoch, model_name, output_file_name):
    # Extract the metrics and their values at the best epoch
    headers = ['model_name'] + ['Epoch'] + list(hist.history.keys())
    row_data = [model_name] + [best_epoch] + [hist.history[key][best_epoch - 1] for key in hist.history.keys()]
    file_exists = os.path.isfile(output_file_name)
    # Open the file in append mode, write headers if the file is new, and append the row
    with open(output_file_name, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        if not file_exists:
            writer.writerow(headers)
        writer.writerow(row_data)

def MultiModelSave(saveModel, folderToSave, saveName, modelType = "", totalTraininCount = 0, classDict = None, classCounts = None, scalingFactors = None, featureNames = [""], additional_note=""):
    folderToSaveSub = os.path.join(folderToSave, saveName)
    saveModel.save(os.path.join(folderToSaveSub, "KSM")) 

    if (classDict!=None):
        SaveModelMetadata(classDict, classCounts, scalingFactors, totalTraininCount,  saveName, modelType, folderToSaveSub, "", featureNames,additional_note)


def PostRunTasks(folder_save, Name, ModelType, Settings, randomSettings, predSet, pSave, classDict, classCounts, scalingFactors, i, denseSizes, mModel, hst, metric, trainingCount=0):
    #Save the training/testing epoch images

    last_accuracy_Show, mEpochsActual, best_accuracy = QWorstCase_Accuracy(hst, Settings.mEpochs, 3, metric) 
    szName = "acc=" + f'{best_accuracy:.4f}' + " " + Name + " CNNs=" + str(randomSettings.conv_layers) + " kn=" + str(randomSettings.kernel_sz) + " flt=" + str(randomSettings.filter_nm) + " iRt=" + str(randomSettings.cnn_filter_increase_rate) + " ds=" + str(denseSizes) + " nE=" + str(mEpochsActual)+","+str(Settings.mEpochs) + " bSz=" + str(Settings.BatchSize) + " bNm=" + str(randomSettings.used_batch_norm_conv) + " i=" + str(i)
    szName = re.sub(r'[^\w\s\-_\.,]', '_', szName) #Regular expression to clean this Prefix from any unsafe characters
    
    #plt.figure(figsize=(14, 7)); ax = plt.subplot(1, 2, 2); QPlot_Loss(hst, metric); ax = plt.subplot(1, 2, 1); QPlot_Loss(hst, 'loss') #Late 2022 style
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 7)); QPlot_Loss2(ax1, hst, metric); QPlot_Loss2(ax2, hst, 'loss');  #6/2023 style

    plt.savefig(folder_save + "\\" + "Plts " + szName + ".jpg") ; plt.clf()
    save_metrics_to_csv_raft(hst, mEpochsActual, szName, os.path.join(folder_save,'metrics01.csv'))
    MultiModelSave(mModel, folder_save, szName, ModelType, trainingCount, classDict, classCounts, scalingFactors)

    if (predSet == None):
        return

    #Predict stuf with the new model
    nClasses = len(classDict)
    p = pd.DataFrame(np.squeeze(mModel.predict(predSet)))
    p.drop(columns = p.columns[0], inplace=True) #Get rid of the G1 so only have G2
    if (False): #Turn this on if we want a class prediction, otherwise we don't need it
        pMax = p.iloc[: , :nClasses] #This is the set of columns where we want to find the max
        p["Pred"] = pMax.idxmax(axis=1) #This is the prediction, if you do this later, it won't be numeric

    #Rename the new columns so the contain the original labels, and the prefix
    try:
        p.rename(columns=classDict, inplace=True); 
        p = p.add_prefix(szName + " ")
    except: 
        pass
    
    pSave = pSave.join(p)
    pSave.to_csv(folder_save + "\\" + "Res_Save_00.csv") #Save each time so we can stop anytime
    AppLog(szName)
    return pSave

def predict_class_label_number(dataset, classNames, mModel):
  """Runs inference and returns predictions as class label numbers."""
  #https://www.tensorflow.org/hub/tutorials/cropnet_on_device
  rev_label_names = {l: i for i, l in enumerate(classNames)}
  return [
    rev_label_names[o[0][0]]
    for o in mModel.predict(dataset, batch_size=128)
  ]

def show_confusion_matrix(cm, labels):
  plt.figure(figsize=(10, 8))
  sns.heatmap(cm, xticklabels=labels, yticklabels=labels, 
              annot=True, fmt='g')
  plt.xlabel('Prediction')
  plt.ylabel('Label')
  plt.show()


def OccasionalWait(Period, ShortAmountSeconds, LongAmountSeconds):
    if (randint(0,Period) == 1):
        print("taking a longer break for " + str(LongAmountSeconds) + " sec . . .")
        time.sleep(LongAmountSeconds) # Every so often, give this computer a 30 second break
    else:
        print("taking a quick break for " + str(ShortAmountSeconds) + " sec . . .")
        time.sleep(ShortAmountSeconds) # Always give it at least 2 seconds

def AppLog(Text):
    file1 = open(r"TF_NB_Log.txt", "a")  # append mode
    file1.write(str(datetime.now()) +"\t"+ str(Text) + "\n")
    file1.close()
    


class TrainingSettings:
    def __init__(self, img_height=256, img_width=256, ksize_Pool=2, nStrides_CNN=1, regulariz=1E-6,
                 mConvDropOut=0.0, mEarlyStopPatience=18, mPenultimateNeuronsCutOff=12, mPostConvDropOut=0.4,
                 nRuns=99999, mEpochs=180, batchSize=32, mTransferName="", numColumns=40, tf_seed= 13):
        self.img_height = img_height
        self.img_width = img_width
        self.ksize_Pool = ksize_Pool
        self.nStrides_CNN = nStrides_CNN
        self.regulariz = regulariz
        self.mConvDropOut = mConvDropOut
        self.mEarlyStopPatience = mEarlyStopPatience
        self.mPenultimateNeuronsCutOff = mPenultimateNeuronsCutOff
        self.mPostConvDropOut = mPostConvDropOut
        self.nRuns = nRuns
        self.mEpochs = mEpochs
        self.mTransferName = mTransferName
        self.BatchSize = batchSize
        self.numColumns = numColumns
        self.TF_seed = tf_seed
        

    def to_json(self, file_path):
        with open(file_path, 'w') as f:
            json.dump(self.__dict__, f)

    @classmethod
    def from_json(cls, file_path):
        with open(file_path, 'r') as f:
            data = json.load(f)
        return cls(**data)
    

class RandomizedSettings:
    def __init__(self, conv_max, conv_layers, filter_nm, cnn_filter_increase_rate, kernel_sz, used_batch_norm_conv, dense_dropouts, dense_batch_norm, cnn_residual_style, UseMultiLoss, dense_min=40, dense_max=80, denseSizesTabular = None):
        self.conv_max = conv_max
        self.conv_layers = conv_layers
        self.filter_nm = filter_nm
        self.cnn_filter_increase_rate = cnn_filter_increase_rate
        self.kernel_sz = kernel_sz
        self.used_batch_norm_conv = used_batch_norm_conv
        self.dense_dropouts = dense_dropouts
        self.dense_batch_norm = dense_batch_norm
        self.cnn_residual_style = cnn_residual_style
        self.use_multi_loss = UseMultiLoss
        self.dense_min = dense_min
        self.dense_max = dense_max
        self.denseSizesTabular = denseSizesTabular

    @classmethod
    def get_randomized_settings(cls, settings, filter_low=18, filter_high=36, dense_min=40, dense_max=80, denseSizesTabularInput = None):
        conv_max = max(1, int(math.log(settings.img_height, settings.ksize_Pool)))
        conv_layers = randint(max(1, conv_max - 2), conv_max)
        filter_nm = randint(filter_low, filter_high)
        cnn_filter_increase_rate = 1 + (randint(1, 3) / 4)
        kernel_sz = randint(2, 4)
        used_batch_norm_conv = randint(0, 1) * randint(0, 1) * randint(0, 1)
        dense_dropouts = randint(0, 3) / 6
        dense_batch_norm = randint(0, 1) * randint(0, 1)
        cnn_residual_style = randint(0,1) #2 is also an option, but turned off
        use_multi_loss = 1 - (randint(0,1) * randint(0,1)) #0 means don't, 1 means do - - more likely that we do
        if (denseSizesTabularInput == None):
            denseSizesTabularA = [randint(dense_min, dense_max), randint(dense_min, dense_max), randint(dense_min, dense_max)]

        return cls(conv_max, conv_layers, filter_nm, cnn_filter_increase_rate, kernel_sz, used_batch_norm_conv, dense_dropouts, dense_batch_norm, cnn_residual_style, use_multi_loss, denseSizesTabular=denseSizesTabularA)

# - - - - - - -  Model Metadata - - - - - - - - - - - - - - - - 

class TF_Model_Metadata:
    def __init__(self):
        self.Index2ClassInfo = {}
        self.ModelName = ""
        self.ModelPath = ""
        self.ModelType = ""
        self.ModelDate = ""
        self.ThresholdDefault = 0.7
        self.TrainingSize = 0
        self.ColumnsIncluded = ""
        self.AdditionalNote = ""

    def add_class(self, idx: int, name: str, training_count: int, rgb: bytes = None, comment: str = "", scalerMin: float = 0, scalerMax: float = 1):
        tfci = TF_Model_ClassInfo(idx, name, training_count, rgb, comment, scalerMin, scalerMax)
        self.Index2ClassInfo[idx] = tfci

    def to_dict(self) -> Dict:
        exclude = {'Index2ClassInfo'}
        result = {key: getattr(self, key) for key in self.__dict__ if key not in exclude}
        result["Index2ClassInfo"] = {idx: tfci.to_dict() for idx, tfci in self.Index2ClassInfo.items()}
        return result

    def to_json(self) -> str:
        def default(o):
            if isinstance(o, np.integer):
                return int(o)
            raise TypeError
        return json.dumps(self.to_dict(), indent=4, default=default)


class TF_Model_ClassInfo:
    def __init__(self, idx: int, name: str, training_count: int, rgb: bytes = None, comment: str = "", scalerMin: float = 0, scalerMax: float = 1):
        self.Index = idx
        self.Name = name
        self.TrainingCount = training_count
        self.RGB = list(rgb if not rgb==None else bytes([randint(0,255),randint(0,255),randint(0,255)]))
        self.Comment = comment
        self.Ignore = False
        self.ThresholdMultiplier = 1
        self.ScalerMin = scalerMin
        self.ScalerMax = scalerMax

    def to_dict(self) -> Dict:
        return {key: getattr(self, key) for key in self.__dict__}


def GetImageCountsPerClass(tfImageDataset, classNamesA=None):
    classCounts = {}
    
    # Try to get the built-in class names, use provided class names if not available
    classNames = getattr(tfImageDataset, 'class_names', classNamesA)
    
    for images, labels in tfImageDataset:
        for label in labels:
            class_name = classNames[label.numpy()] # Use numpy() to convert label to a Python scalar
            if class_name not in classCounts:
                classCounts[class_name] = 0
            classCounts[class_name] += 1
    return classCounts


def SaveModelMetadata(ClassDict, ClassCounts, ScalingValues, TotalTrainingSize, ModelName, ModelType, ModelPath, output_file = "", featureNames = [""], additional_note = ""):
    #print(output_file + " " + str(featureNames))
    tfmm = TF_Model_Metadata()
    tfmm.ModelDate = str(datetime.now())
    tfmm.ModelName = ModelName
    tfmm.ModelType = ModelType
    tfmm.ModelPath = ModelPath
    tfmm.TrainingSize = TotalTrainingSize
    tfmm.ColumnsIncluded = '|'.join(featureNames)
    tfmm.AdditionalNote = additional_note
    for key, value in ClassDict.items():
        tfmm.add_class(
            key, value, int(ClassCounts[value]) if not ClassCounts==None else -1, None, "", 
            ScalingValues[value]['min'] if not ScalingValues==None else 0,
            ScalingValues[value]['max'] if not ScalingValues==None else 1)
    json_str = tfmm.to_json()
    if (output_file == ""):
        #isFile = os.path.isfile(fpath)
        #isDirectory = os.path.isdir(fpath)
        #dirname, fname = os.path.split(fullpath)
        output_file = os.path.join(ModelPath, "FIVE_metadata.json")
    with open(output_file, 'w') as f:
        f.write(json_str)



# - - - - - - - Function Definitions - - Feature Set - - - - - - - - - - - - - - 



def LimitColumns(dataframe_X, Keep_ifInName_List, KeepOtherColumn = "", AddBackInAreaInt = False):
    Xp = dataframe_X
    X_cols_keep = [col for col in Xp.columns if [x for x in Keep_ifInName_List if x in col]] 

    #'SPACING', 'NEIGHBOR', 
    prohibited_substrings = ['CG', 'GLOBAL', 'BORDER', 'ProbPr', 'PrPro', '(SOI)', 'FAKE', 'X UM', 'Y UM', 'Well Idx', 'PlateIdx', 'DispenseIdx','Replicate','Tip']
    X_cols_throw = [col for col in Xp.columns if any(substring in col for substring in prohibited_substrings)]
    X_cols_keep = [x for x in X_cols_keep if x not in X_cols_throw]
    X = Xp[X_cols_keep].copy()
    #--The following line puts back the two main WVH features
    if (AddBackInAreaInt):
        X[['NUCLEI AREA WVH','NUCLEI INTENSITY WVH']] = dataframe_X[['NUCLEI AREA WVH','NUCLEI INTENSITY WVH']]
    if (KeepOtherColumn != ""):
        X[KeepOtherColumn] = Xp[KeepOtherColumn]
    return X

# Norm_SubsetBy is used to equalize all the plates for instance, generally good for intensity measures but not for other measures
def NormalizeF(dataframe_X, Norm_SubsetBy = ""):
    X = dataframe_X.fillna(0)

    if (Norm_SubsetBy != ""):
        groups = dataframe_X.groupby(Norm_SubsetBy)
        #mean, std = groups.transform("mean"), groups.transform("std") #Gets both StDev and Mean
        #df.groupby('indx').transform(lambda x: (x - x.mean()) / x.std()) Slower way
        mean = groups.transform("mean", numeric_only=True)
        X = dataframe_X[mean.columns] / mean
        #normalized = normalized.join(df3[Norm_SubsetBy])
        #normalized.to_csv(r"s:\Feature\FIV836\try.csv")
    
    X=(X-X.min())/(X.max()-X.min()) 
    return X

def NewG2From(specificClassForG2, ListOfClassesPerCell, classesUnique):
    Y = ListOfClassesPerCell
    for cl in classesUnique:
        if (cl==specificClassForG2):
            Y = Y.replace(cl,"G2")    
        else:
            Y = Y.replace(cl,"Mix")    
    return Y

def PrepXY(dataframe_X, dataframe_Y):
    #Remove non-trainable rows
    Y = dataframe_Y.replace('L76P HET A3','G2').replace('L76P C6','G2').replace('R94Q D4','G2')
    Y = Y.replace('R94Q E7','G2').replace('R94Q D10','G2').replace('WTC11 GEN2C','G1')
    rows_Remove = Y.index[Y.str.contains('MIX')].tolist()
    #pd.DataFrame(rows_Remove).to_clipboard()
    X = dataframe_X.drop(dataframe_X.index[rows_Remove]).fillna(0)
    Y = Y.drop(Y.index[rows_Remove])

    #UnComment below if you want to turn the columns numeric
    #Y = Y.replace('Pathogenic',1).replace('Benign',0) #turn the labels into numbers
    mClasses = Y.unique()
    mClassesCount = len(mClasses)
    values, counts = np.unique(Y, return_counts=True)
    RowsPerY = dict(zip(values, counts))

    #Y = pd.get_dummies(Y) #This hot-encodes the classes
    le = LabelEncoder(); le.fit(Y)
    Y = le.transform(Y) #This turns the labeled columns into numbers
    Y_Mapping = dict(zip(le.transform(le.classes_),le.classes_))
    #Y_Mapping = [le.classes_, le.transform(le.classes_)]
    #Y_Mapping = np.transpose(Y_Mapping)

    ## Get Labels and figure out balance (this sets the class weights)
    class_weights = class_weight.compute_class_weight('balanced', classes=np.unique(Y), y=Y) #https://stackoverflow.com/questions/44716150/how-can-i-assign-a-class-weight-in-keras-in-a-simple-way
    class_weights = {i : class_weights[i] for i in range(len(class_weights))} #https://stackoverflow.com/questions/61261907/on-colab-class-weight-is-causing-a-valueerror-the-truth-value-of-an-array-wit
    return X, Y, mClasses, RowsPerY, Y_Mapping, class_weights

def calculate_metrics_2class(y_true, y_pred, n_classes):
    is_binary = n_classes == 2
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred, average='weighted')
    recall = recall_score(y_true, y_pred, average='weighted')
    f1 = f1_score(y_true, y_pred, average='weighted')

    if is_binary:
        tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
        sensitivity = tp / (tp + fn)
        specificity = tn / (tn + fp)
        auc = roc_auc_score(y_true, y_pred)
    else:
        sensitivity, specificity, auc = None, None, None

    metrics = {
        'Accuracy': accuracy,
        'Precision': precision,
        'Sensitivity': sensitivity,
        'Specificity': specificity,
        'AUC': auc,
        'F1_score': f1
    }

    return metrics

def save_metrics_to_csv(Xp, predictions, Y_Class_Mapping, col_TrainingLayouts, col_GenotypedAs, ModelName, csv_file_path, featureNames):
    # Reverse the class mapping
    inv_Y_Class_Mapping = {v: k for k, v in Y_Class_Mapping.items()}
    metrics_dict = {}
    n_classes = len(Y_Class_Mapping)
    TrainingSets = Xp[col_TrainingLayouts].unique()

    # Calculate metrics for each subset
    calculate_metrics = True if col_GenotypedAs != "" else False
    if calculate_metrics:
        for value in TrainingSets:
            subset_indices = Xp[Xp[col_TrainingLayouts] == value].index

            # Filter out rows with class labels not present in inv_Y_Class_Mapping
            subset_true_labels = Xp.loc[subset_indices, col_GenotypedAs]
            valid_indices = subset_true_labels.isin(inv_Y_Class_Mapping.keys())
            valid_subset_indices = subset_indices[valid_indices]
            true_labels = subset_true_labels[valid_indices].map(inv_Y_Class_Mapping).values

            # Get the predictions for the current subset
            subset_predictions = predictions.loc[valid_subset_indices, :]
            subset_pred_labels = np.argmax(subset_predictions.values, axis=-1)

            # Calculate and store the metrics
            metrics_dict[value] = calculate_metrics_2class(true_labels, subset_pred_labels, n_classes)

    # Write metrics to CSV
    file_exists = os.path.isfile(csv_file_path)
    with open(csv_file_path, 'a', newline='') as f:
        writer = csv.writer(f)
        if not file_exists:
            # Write headers if the file doesn't exist
            headers = ['ModelName', 'Subset', 'Features']
            if calculate_metrics:
                first_item_key = next(iter(metrics_dict))
                headers += list(metrics_dict[first_item_key].keys())
            writer.writerow(headers)
        for key, metrics in metrics_dict.items():
            row = [ModelName, key, '|'.join(featureNames)]
            if calculate_metrics:
                row += list(metrics.values())
            writer.writerow(row)



# - - - - - - - - Tabular NAS Run  - - - - - - - 

def NAS_Run(randomized_settings, training_settings, dfSaveRes, FolderSave, Prefix, Input_X, Input_Y, PreDrop_X, Y_Class_Mapping, rowsPerClass, dctClassWeigts, Xp_Full, 
            col_Label, col_TrainingLayouts="", col_GenotypedAs="", CalcUMAP=True, saveModels=True, TF_seed=13):
    
    Prefix = re.sub(r'[^\w\s\-_\.,]', '_', Prefix)  # Regular expression to clean this Prefix from any unsafe characters
    ClassesCount = len(Y_Class_Mapping); tf.random.set_seed(training_settings.TF_seed)
    callBack = keras.callbacks.EarlyStopping(monitor="val_loss", patience=training_settings.mEarlyStopPatience, baseline=True)
    for featureRS in range(training_settings.nRuns):
        denseSizes = randomized_settings.denseSizesTabular
        print("denses=" + str(denseSizes))

        ### --- Extract features and get train/test split
        Xs = Input_X.sample(n=min(Input_X.shape[1], training_settings.numColumns), axis='columns', random_state=featureRS)
        column_headers = Xs.columns.tolist(); print("Cols sampled: " + str(column_headers))
        X_train, X_test, Y_train, Y_test = train_test_split(Xs, Input_Y, test_size=0.3, random_state=119)
        print("TrainTest sizes = " + str(X_train.shape) + " " + str(X_test.shape))

        ### --- Define Model
        mInput = keras.Input(shape=(Xs.shape[1])); mNext = mInput
        for dS in denseSizes:
            mNext = keras.layers.Dense(units=dS, kernel_regularizer=keras.regularizers.L2(training_settings.regulariz), activation='relu')(mNext)
            mNext = keras.layers.Dropout(training_settings.mPostConvDropOut)(mNext)
        mOut = keras.layers.Dense(ClassesCount, activation='softmax')(mNext)
        mModel = keras.Model(mInput, mOut)
        mModel.compile(loss=tf.losses.SparseCategoricalCrossentropy(), optimizer=keras.optimizers.Adam(), metrics=["accuracy"])

        hist = mModel.fit(X_train, Y_train, validation_data=(X_test, Y_test), epochs=training_settings.mEpochs, batch_size=training_settings.BatchSize, verbose=2, class_weight=dctClassWeigts, callbacks=[callBack])
        last_accuracy_Show, mEpochsActual, bestAcc = QWorstCase_Accuracy(hist, training_settings.mEpochs, 4, "accuracy")

        szName = Prefix + " acc=" + f'{bestAcc:.4f}' + " nC=" + str(training_settings.numColumns) + " Feat_rs=" + str(featureRS) + " nE=" + str(mEpochsActual) + "," + str(training_settings.mEpochs) + " lay=" + str(denseSizes) + " BS=" + str(training_settings.BatchSize) + " TF_rs=" + str(TF_seed)

        plt.figure(figsize=(12, 5))
        ax = plt.subplot(1, 2, 2); QPlot_Loss(hist, 'accuracy'); ax = plt.subplot(1, 2, 1); QPlot_Loss(hist, 'loss')
        plt.savefig(os.path.join(FolderSave,szName + ".jpg")); plt.clf()

        #Predict with the new model
        fullrows_X = pd.DataFrame(PreDrop_X[column_headers]) #Xs.columns
        pred = mModel.predict(fullrows_X)
        p = pd.DataFrame(np.squeeze(pred)); 
        #y2 = np.expand_dims(predLabels,1)
        p = p.rename(columns=Y_Class_Mapping) ; p = p.add_prefix(szName + " ")
        inv_Y_Class_Mapping = {v: k for k, v in Y_Class_Mapping.items()}
        if (saveModels):
            MultiModelSave(mModel, FolderSave, szName + "M", classDict=inv_Y_Class_Mapping, featureNames=column_headers) 
        dfSaveRes = dfSaveRes.join(p)
        save_metrics_to_csv(Xp_Full, p, Y_Class_Mapping, col_TrainingLayouts, col_GenotypedAs , szName, os.path.join(FolderSave,"evalMetrics01.csv"), featureNames = column_headers)
        #save_metrics_to_csv(Xp_Full, p, Y_Class_Mapping, col_TrainingLayouts, col_GenotypedAs if col_GenotypedAs != "" else col_Label, szName, os.path.join(FolderSave,"evalMetrics01.csv"), featureNames = column_headers)

        if (CalcUMAP):
            print("Calculating UMAP . .")
            toUMAP = fullrows_X.join(p)
            uMAPembed = pd.DataFrame(umap.UMAP().fit_transform(toUMAP)).add_prefix(szName + " ")
            dfSaveRes = dfSaveRes.join(uMAPembed)
        
        print("Saving ResSave00 . . ")
        dfSaveRes.to_csv(os.path.join(FolderSave,"ResSave00.csv")) #Save each time for safety
        OccasionalWait(10 if CalcUMAP else 3, 2, 20)
        clear_output(wait=True)
    return dfSaveRes, mModel, Xs.columns
