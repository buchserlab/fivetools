﻿namespace CMSImageTools
{
    partial class CMSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputCMSRootFolder = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.OutFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.projectBox = new System.Windows.Forms.ListBox();
            this.scanBox = new System.Windows.Forms.ListBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnStitcher = new System.Windows.Forms.Button();
            this.btnPickedRafts = new System.Windows.Forms.Button();
            this.updateBox = new System.Windows.Forms.TextBox();
            this.workerStitcher = new System.ComponentModel.BackgroundWorker();
            this.workerSQLite = new System.ComponentModel.BackgroundWorker();
            this.btnLoadRafts = new System.Windows.Forms.Button();
            this.btnStitcherCustom = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAllRafts = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.ExtensionImage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputCMSRootFolder
            // 
            this.inputCMSRootFolder.Location = new System.Drawing.Point(24, 27);
            this.inputCMSRootFolder.Name = "inputCMSRootFolder";
            this.inputCMSRootFolder.Size = new System.Drawing.Size(386, 20);
            this.inputCMSRootFolder.TabIndex = 0;
            this.inputCMSRootFolder.Text = "\\\\storage1.ris.wustl.edu\\wbuchser\\Active\\dB\\CMS_Machine\\CMSData";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ExtensionImage);
            this.panel1.Controls.Add(this.OutFolder);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.inputCMSRootFolder);
            this.panel1.Location = new System.Drawing.Point(32, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 119);
            this.panel1.TabIndex = 1;
            // 
            // OutFolder
            // 
            this.OutFolder.Location = new System.Drawing.Point(24, 76);
            this.OutFolder.Name = "OutFolder";
            this.OutFolder.Size = new System.Drawing.Size(351, 20);
            this.OutFolder.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output Folder (Leave empty if you want to save in scan folder)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "CMS Results Root Folder";
            // 
            // projectBox
            // 
            this.projectBox.FormattingEnabled = true;
            this.projectBox.Location = new System.Drawing.Point(32, 166);
            this.projectBox.Name = "projectBox";
            this.projectBox.Size = new System.Drawing.Size(212, 329);
            this.projectBox.TabIndex = 2;
            this.projectBox.SelectedValueChanged += new System.EventHandler(this.GetScans);
            this.projectBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.projectBox_MouseDoubleClick);
            // 
            // scanBox
            // 
            this.scanBox.FormattingEnabled = true;
            this.scanBox.Location = new System.Drawing.Point(283, 166);
            this.scanBox.Name = "scanBox";
            this.scanBox.Size = new System.Drawing.Size(212, 121);
            this.scanBox.TabIndex = 3;
            this.scanBox.SelectedValueChanged += new System.EventHandler(this.SelectScan);
            this.scanBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.scanBox_MouseDoubleClick);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(32, 137);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(98, 23);
            this.btnLoad.TabIndex = 5;
            this.btnLoad.Text = "Load Folders";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.LoadDB);
            // 
            // btnStitcher
            // 
            this.btnStitcher.Enabled = false;
            this.btnStitcher.Location = new System.Drawing.Point(283, 303);
            this.btnStitcher.Name = "btnStitcher";
            this.btnStitcher.Size = new System.Drawing.Size(93, 23);
            this.btnStitcher.TabIndex = 6;
            this.btnStitcher.Text = "Stitch Markers";
            this.btnStitcher.UseVisualStyleBackColor = true;
            this.btnStitcher.Click += new System.EventHandler(this.clickStitcher);
            // 
            // btnPickedRafts
            // 
            this.btnPickedRafts.Enabled = false;
            this.btnPickedRafts.Location = new System.Drawing.Point(386, 303);
            this.btnPickedRafts.Name = "btnPickedRafts";
            this.btnPickedRafts.Size = new System.Drawing.Size(109, 23);
            this.btnPickedRafts.TabIndex = 7;
            this.btnPickedRafts.Text = "Get Picked Rafts";
            this.btnPickedRafts.UseVisualStyleBackColor = true;
            this.btnPickedRafts.Click += new System.EventHandler(this.clickSQLite);
            // 
            // updateBox
            // 
            this.updateBox.Location = new System.Drawing.Point(283, 390);
            this.updateBox.Multiline = true;
            this.updateBox.Name = "updateBox";
            this.updateBox.ReadOnly = true;
            this.updateBox.Size = new System.Drawing.Size(212, 104);
            this.updateBox.TabIndex = 8;
            // 
            // workerStitcher
            // 
            this.workerStitcher.WorkerSupportsCancellation = true;
            this.workerStitcher.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerStitcher_DoWork);
            this.workerStitcher.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerStitcher_Completed);
            // 
            // workerSQLite
            // 
            this.workerSQLite.WorkerSupportsCancellation = true;
            this.workerSQLite.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerSQLite_DoWork);
            this.workerSQLite.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerSQLite_Completed);
            // 
            // btnLoadRafts
            // 
            this.btnLoadRafts.Location = new System.Drawing.Point(283, 137);
            this.btnLoadRafts.Name = "btnLoadRafts";
            this.btnLoadRafts.Size = new System.Drawing.Size(105, 23);
            this.btnLoadRafts.TabIndex = 9;
            this.btnLoadRafts.Text = "Load Custom Rafts";
            this.btnLoadRafts.UseVisualStyleBackColor = true;
            this.btnLoadRafts.Click += new System.EventHandler(this.clickLoadRafts);
            // 
            // btnStitcherCustom
            // 
            this.btnStitcherCustom.Enabled = false;
            this.btnStitcherCustom.Location = new System.Drawing.Point(283, 332);
            this.btnStitcherCustom.Name = "btnStitcherCustom";
            this.btnStitcherCustom.Size = new System.Drawing.Size(114, 23);
            this.btnStitcherCustom.TabIndex = 4;
            this.btnStitcherCustom.Text = "Stitch Custom Rafts";
            this.btnStitcherCustom.UseVisualStyleBackColor = true;
            this.btnStitcherCustom.Click += new System.EventHandler(this.clickStitcherCustom);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(146, 137);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(98, 23);
            this.btnRefresh.TabIndex = 10;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.RefreshDB);
            // 
            // btnAllRafts
            // 
            this.btnAllRafts.Enabled = false;
            this.btnAllRafts.Location = new System.Drawing.Point(403, 332);
            this.btnAllRafts.Name = "btnAllRafts";
            this.btnAllRafts.Size = new System.Drawing.Size(92, 23);
            this.btnAllRafts.TabIndex = 11;
            this.btnAllRafts.Text = "Get All Rafts";
            this.btnAllRafts.UseVisualStyleBackColor = true;
            this.btnAllRafts.Click += new System.EventHandler(this.clickAllRafts);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(333, 361);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(110, 23);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "Stop Everything";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.clickStop);
            // 
            // ExtensionImage
            // 
            this.ExtensionImage.Location = new System.Drawing.Point(381, 76);
            this.ExtensionImage.Name = "ExtensionImage";
            this.ExtensionImage.Size = new System.Drawing.Size(60, 20);
            this.ExtensionImage.TabIndex = 4;
            this.ExtensionImage.Text = ".tif";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(388, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Extension";
            // 
            // CMSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 506);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnAllRafts);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnStitcherCustom);
            this.Controls.Add(this.btnLoadRafts);
            this.Controls.Add(this.updateBox);
            this.Controls.Add(this.btnPickedRafts);
            this.Controls.Add(this.btnStitcher);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.scanBox);
            this.Controls.Add(this.projectBox);
            this.Controls.Add(this.panel1);
            this.Name = "CMSForm";
            this.Text = "CMS Image Tools";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputCMSRootFolder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OutFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox projectBox;
        private System.Windows.Forms.ListBox scanBox;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnStitcher;
        private System.Windows.Forms.Button btnPickedRafts;
        private System.Windows.Forms.TextBox updateBox;
        private System.ComponentModel.BackgroundWorker workerStitcher;
        private System.ComponentModel.BackgroundWorker workerSQLite;
        private System.Windows.Forms.Button btnLoadRafts;
        private System.Windows.Forms.Button btnStitcherCustom;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnAllRafts;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ExtensionImage;
    }
}