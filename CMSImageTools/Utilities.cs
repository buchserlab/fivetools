﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


/// <summary>
/// All CMSImageTools
/// NOTE: x and y refer to coordinates within an image, while i and j refer to raft coordinates and refer to row number
/// find the column number (j), you must use the X coordinate, which might be the opposite of what you think at first.
/// </summary>
namespace CMSImageTools
{
    /// <summary>
    /// A struct containing all the parameters for CMS image analysis
    /// </summary>    
    public struct CMSParams {

        public int CORNER_X, CORNER_Y;      // The X and Y coordinates of the top left raft in each tiff
        public int WIDTH, HEIGHT;           // The pixel width and height of each raft
        public int MOVE_X, MOVE_Y;          // The pixel distances from one raft to another (including width and height)
        public int CMS_OVER_i, CMS_OVER_j;  // The number or rows and columns, respectively the machine moves over for each tiff 
        public int NUM_MARKERS;             // The number of fiduciary markers in each row/column

        public CMSParams(int cx, int cy, int w, int h, int mx, int my, int cmsoi, int cmsoj, int nm)
        {
            CORNER_X    = cx;     CORNER_Y    = cy;
            WIDTH       = w;      HEIGHT      = h;
            MOVE_X      = mx;     MOVE_Y      = my;
            CMS_OVER_i  = cmsoi;  CMS_OVER_j  = cmsoj;
            NUM_MARKERS = nm;
        }
    }

    /// <summary>
    /// A static class containing important methods & constants
    /// </summary>
    public static class Tools {

        public static CMSParams R100Quad = new CMSParams(cx: 110, cy: 60, w: 200, h: 200, mx: 208, my: 208, cmsoi: 10, cmsoj: 12, nm: 20); // For 100x100 and quad
        public static CMSParams R200     = new CMSParams(cx: 148, cy: 81, w: 400, h: 400, mx: 401, my: 401, cmsoi:  5, cmsoj:  6, nm: 10); // For 200x200

        /// <summary>
        /// A regex that matches if there is a correctly formatted raft
        /// </summary>
        public static Regex RaftMatch = new Regex(@"[A-Za-z]\d[A-Za-z]\d");
        /// <summary>
        /// Gets a row column coordinate tuple for a raft id
        /// </summary>
        /// <param name="fname">A string containing the raft id in the first four characters</param>
        /// <returns>A tuple with the row and column information</returns>
        /// <example>GetArrayCoords("D0J0.tiff") => (40,100)</example>
        public static (int i, int j) GetArrayCoords (string fname)
        {
            return (GetCoord(fname.Substring(0, 2)), GetCoord(fname.Substring(2, 2)));
        }

        /// <param name="fi">The FileInfo object that corresponds to a raft image</param>
        /// <see cref="GetArrayCoords(string)"/>
        public static (int i, int j) GetArrayCoords(FileInfo fi)
        {
            return GetArrayCoords(fi.Name);
        }

        /// <summary>
        /// Converts a raft position to a number
        /// </summary>
        /// <example>GetCoord("J0") => 90</example>
        public static int GetCoord(string s)
        {
            return 10 * (s[0] - 65) + (s[1] - 48);
        }

        /// <summary>
        /// Converts a number to a raft position
        /// </summary>
        /// <example>GetRaftPos(90) => "J0"</example>
        public static string GetRaftPos(int x)
        {
            char letter = (char)(x / 10 + 65);
            char number = (char)(x % 10 + 48);
            return new string(new char[] { letter, number });
        }

        /// <summary>
        /// True if raft is a fiduciary marker with letters and numbers
        /// </summary>
        /// <example>IsFiducLetter("J0") => true</example>
        /// <example>IsFiducLetter("A5") => false</example>
        public static bool IsFiducLetter(string raftname)
        {
            return (raftname[1] == '0' && raftname[3] == '0');
        }

        /// <summary>
        /// True if raft is a fiduciary marker with letters and numbers
        /// </summary>
        /// <example>IsFiducLetter("J0") => false</example>
        /// <example>IsFiducLetter("A5") => true</example>
        public static bool IsFiducDot(string raftname)
        {
            if (IsFiducLetter(raftname)) return false;

            bool Row05 = (raftname[1] == '0' || raftname[1] == '5');
            bool Col05 = (raftname[3] == '0' || raftname[3] == '5');
   
            if (!(Row05 && Col05)) return false;
            return true;
        }

        /// <summary>
        /// Gets a list of all the rafts (as 4-character raft ids) in a folder
        /// </summary>
        public static List<string> RaftsInFolder(string Folder)
        {
            HashSet<string> Rafts = new HashSet<string>();
            foreach (FileInfo ImageFile in (new DirectoryInfo(Folder)).GetFiles())
            {
                Rafts.Add(ImageFile.Name.Substring(0, 4));
            }
            return Rafts.ToList();
        }

        /// <summary>
        /// A method for finding the optimal dimensions of an image (in terms of rows and columns) with a given number of rafts
        /// </summary>
        /// <param name="length">The number of images</param>
        /// <returns>A tuple with the number of rows and columns</returns>
        public static (int nrow, int ncol) OptimalDims(int length)
        {
            int ncol = (int)Math.Ceiling(Math.Sqrt(length));
            int nrow = (length - 1) / ncol + 1;
            if (nrow * ncol < length) throw new Exception($"You dummy, {ncol} * {nrow} < {length}");
            return (nrow, ncol);
        }
    }
    
    public class CMSStitcher
    {
        public enum ArType { Norm100, Quad100, Norm200, Quad200 };

        #region Instance Variables
        /// <summary>
        /// The struct containing the parameters for this specific scan
        /// </summary>
        public CMSParams ScanPs;              
        /// <summary>
        /// The Folder containing all the tiffs
        /// </summary>
        public DirectoryInfo Folder;          
        /// <summary>
        /// The folder that the final image should go in
        /// </summary>
        public string OutFolder;              
        /// <summary>
        /// A 2-D list containing all the images, laid out intuitively
        /// </summary>
        private List<List<FileInfo>> Images;
        /// <summary>
        /// A list containing all the fiduciary markers
        /// </summary>
        private List<string> FiduciaryMarkers;
        /// <summary>
        /// A list containing all the rafts that are wanted
        /// </summary>
        public List<string> Rafts;
        /// <summary>
        /// A boolean indicating whether 
        /// </summary>
        public readonly bool UseFiduc;
        public string EndString => UseFiduc ? "stitch" : "stitchcustom";
        /// <summary>
        /// True if the raft is a quad
        /// </summary>
        public readonly ArType ArrayType;
        /// <summary>
        /// A number indicating the buffer around the final cropped image
        /// </summary>
        public int BufferSize;
        /// <summary>
        /// The actual width of each buffered image;
        /// </summary>
        public int BufferWidth => 2 * BufferSize + 1;
        /// <summary>
        /// Whether or not to draw a grid on the final image
        /// </summary>
        public bool DrawGrid = false;
        /// <summary>
        /// The background worker that might be working in this class
        /// </summary>
        private readonly BackgroundWorker BW;
        /// <summary>
        /// This object contains the mapping from raft id to three values: the file containing that raft, the number of rows offset
        /// from the top left corner of that image, and the number of columns offset from the top left corner of that image
        /// </summary>
        private Dictionary<string, (FileInfo fi, int offset_i, int offset_j)> Mapping;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder_path"></param>
        /// <param name="outPath"></param>
        /// <param name="raftsWanted"></param>
        /// <param name="croppedBuffer"></param>
        /// <param name="useAll"></param>
        public CMSStitcher(string folder_path, string outPath = null, List<string> raftsWanted = null, int croppedBuffer = -1, bool useAll = false, BackgroundWorker bw = null)
        {
            BW = bw;
            Mapping = new Dictionary<string, (FileInfo fi, int offset_i, int offset_j)>();

            // Creates the 2-D Image List
            Folder = new DirectoryInfo(folder_path);
            Images = new List<List<FileInfo>>();
            List<FileInfo> Row = new List<FileInfo>();
            string rowString = "A0";
            FileInfo[] ImageFiles = Folder.GetFiles();
            var OrderedFiles = ImageFiles.OrderBy(f => f.Name);
            foreach (FileInfo fi in OrderedFiles)
            {
                string filename = fi.Name;
                if (fi.Name[4] == 'F') // If the image is focused (better for id-ing numbers)
                {
                    if (fi.Name.Substring(0, 2) != rowString) // If it's moved on to a new row
                    {
                        Images.Add(Row);
                        Row = new List<FileInfo>();
                        rowString = fi.Name.Substring(0, 2);
                    }
                    Row.Add(fi);
                }
            }
            Images.Add(Row);

            // Checks the number of rafts that the images are offset by in each direction then adjusts the image
            // search properties (important for distinguishing between 100x100/Quads and 200x200)
            FileInfo TopLeft = Images[0][0];
            FileInfo TopishLeft = Images[1][0];
            if (Tools.GetArrayCoords(TopishLeft).i - Tools.GetArrayCoords(TopLeft).i >= 10)
            {
                ScanPs = Tools.R100Quad;
                ArrayType = ArType.Norm100;
            }
            else
            {
                ScanPs = Tools.R200;
                ArrayType = ArType.Norm200;
            }

            int nummarkers = ScanPs.NUM_MARKERS;
            // Checks to see if the array is a quad (AOIOF.tiff should only exist for quads)
            if (Images[0].Select(f => f.Name).Contains("A0I0F.tiff")) {
                ArrayType = ArType.Quad100;
                nummarkers = 16;
            }
            if (Images[0].Select(f => f.Name).Contains("A0E0F.tiff"))
            {
                ArrayType = ArType.Quad200;
                nummarkers = 8;
            }

            // Adds all the fiduciary marker coordinates to FiduciaryMarkers
            string alphabet = "ABCDEFGHIJLKMNOPQRSTUVWXYZ".Substring(0, nummarkers);
            FiduciaryMarkers = new List<string>(200);
            foreach (char c in alphabet)
            {
                foreach (char d in alphabet)
                {
                    FiduciaryMarkers.Add($"{c}0{d}0");
                }
            }

            // If no specific rafts are wanted, the program will look for the fiduciary markers
            if (useAll)
            {
                Rafts = new List<string>(nummarkers * nummarkers * 10 * 10);
                foreach (char c in alphabet)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        foreach (char d in alphabet)
                        {
                            for (int j = 0; j < 10; j++)
                            {
                                Rafts.Add($"{c}{i}{d}{j}");
                            }
                        }
                    }
                }
                UseFiduc = false;
            }
            else if (raftsWanted == null)
            {
                Rafts = FiduciaryMarkers;
                UseFiduc = true;
            }
            else
            {
                Rafts = raftsWanted;
                UseFiduc = false;
            }

            // If no specific path is given, the program will save the stitched image to the parent of the image folder
            OutFolder = outPath ?? Folder.Parent.FullName;
            // If croppedBuffer is not given, there will be no buffer
            BufferSize = croppedBuffer > -1 ? croppedBuffer : 0;
            if (BufferSize > 0) DrawGrid = true;
            if (BufferSize > 2)
            {
                Console.WriteLine("You're not gonna do that buffer size, mister");
                System.Threading.Thread.Sleep(1000 * 60);
                throw new ArgumentException("Buffer too big");
            }
        }

        /// <summary>
        /// Generates every mapping
        /// </summary>
        /// <see cref="GenerateMap(string)"/>
        public void GenerateAllMaps()
        {
            foreach (string raft in Rafts)
            {
                if (BW.CancellationPending) return;
                GenerateMap(raft);
            }
        }

        /// <summary>
        /// Generates the mapping for a given raft by first finding the corresponding file then calculates the offset values
        /// </summary>
        /// <param name="raft">A string with the raft id, e.g. "A0A0"</param>
        public void GenerateMap(string raft)
        {
            FileInfo fi = GetImageFile(raft);
            if (fi == null) return;
            (int i, int j) ImageCoords = Tools.GetArrayCoords(fi.Name);
            (int i, int j) RaftCoords = Tools.GetArrayCoords(raft);
            int offset_i = RaftCoords.i - ImageCoords.i;
            int offset_j = RaftCoords.j - ImageCoords.j;
            Mapping.Add(raft, (fi, offset_i, offset_j));
        }

        /// <summary>
        /// Finds the image file for a given raft
        /// </summary>
        /// <param name="raft">A string with the raft id, e.g. "A0A0"</param>
        /// <returns>The FileInfo corresponding to the needed image</returns>
        public FileInfo GetImageFile(string raft)
        {
            var RaftCoords = Tools.GetArrayCoords(raft);
            foreach (List<FileInfo> Row in Images)
            {
                foreach (FileInfo file in Row)
                {
                    var ImageCoords = Tools.GetArrayCoords(file);
                    if (RaftCoords.i - ImageCoords.i >= ScanPs.CMS_OVER_i) // If the raft is much farther down than the image
                    {
                        break;
                    }
                    if (RaftCoords.j - ImageCoords.j < ScanPs.CMS_OVER_j) // If the raft is within the column range of the tiff
                    {
                        // This snippet accounts for the vertical line that happens before the I column in quads
                        // It basically just shifts the image over by one for rafts like "__I0"
                        if (ArrayType == ArType.Quad100 && raft[2] == 'I' && file.Name[2] != 'I') continue;
                        if (ArrayType == ArType.Quad200 && raft[2] == 'E' && file.Name[2] != 'E') continue;
                        return file;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Separates all the rafts into separate images and saves them
        /// </summary>
        /// <param name="openAtEnd">true to open the directory containing the files at the end</param>
        public void SeparateRaftImages(bool openAtEnd = true, string ext = ".tiff")
        {
            string ImageFolder = Path.Combine(OutFolder, "RaftImages");
            Directory.CreateDirectory(ImageFolder);
            Bitmap raftimage;
            foreach (KeyValuePair<string, (FileInfo fi, int offset_i, int offset_j)> kvp in Mapping)
            {
                if (BW.CancellationPending) return;
                raftimage = CroppedRaft(kvp.Value.fi, kvp.Value.offset_i, kvp.Value.offset_j);
                raftimage.Save(Path.Combine(ImageFolder, $"{kvp.Key}{ext}"));
            }
            if (openAtEnd)
            {
                var PSI = new ProcessStartInfo(ImageFolder); PSI.UseShellExecute = true;
                Process.Start(PSI);
            }
        }
        /// <summary>
        /// Stitches all the fiduciary markers together by placing each cropped raft in a corresponding place in a new image
        /// </summary>
        public void Stitch(bool openAtEnd = true)
        {
            int ncol = ScanPs.NUM_MARKERS;
            int nrow = ScanPs.NUM_MARKERS;
            if (!UseFiduc) (nrow, ncol) = Tools.OptimalDims(Rafts.Count);

            Bitmap FinalImage = new Bitmap(ncol * ScanPs.WIDTH * BufferWidth, nrow * ScanPs.HEIGHT * BufferWidth);

            using (Graphics g = Graphics.FromImage(FinalImage))
            {
                int rowPos;
                int colPos;
                int imageCounter = 0;
                Bitmap CroppedImage;
                int buffered_i;
                int buffered_j;
                int buff_width;
                int buff_height;
                Point drawPoint;
                Pen BufferPen = new Pen(Color.FromArgb(220, 40, 40, 40), ScanPs.HEIGHT);
                foreach (KeyValuePair<string, (FileInfo, int, int)> kvp in Mapping)
                {
                    if (BW.CancellationPending) return;
                    string raftID = kvp.Key;
                    (FileInfo fi, int offset_i, int offset_j) = kvp.Value;
                    var RaftCoords = Tools.GetArrayCoords(raftID);


                    rowPos = RaftCoords.i / 10;
                    colPos = RaftCoords.j / 10;
                    if (!UseFiduc)
                    {
                        rowPos = imageCounter / ncol;
                        colPos = imageCounter % ncol;
                    }

                    if (BufferSize == 0)
                    {
                        CroppedImage = CroppedRaft(fi, offset_i, offset_j);
                        drawPoint = new Point(ScanPs.WIDTH * colPos, ScanPs.HEIGHT * rowPos);
                    }
                    else
                    {
                        buffered_i = (offset_i - BufferSize) < 0 ? (offset_i) : (offset_i - BufferSize);
                        buffered_j = (offset_j - BufferSize) < 0 ? (offset_j) : (offset_j - BufferSize);
                        buff_width = (buffered_j + BufferWidth) > ScanPs.CMS_OVER_j ? (BufferWidth - 1) : (BufferWidth);
                        buff_height = (buffered_i + BufferWidth) > ScanPs.CMS_OVER_i ? (BufferWidth - 1) : (BufferWidth);

                        CroppedImage = CroppedRaft(fi, buffered_i, buffered_j, buff_width * ScanPs.WIDTH, buff_height * ScanPs.HEIGHT);
                        drawPoint = new Point(ScanPs.WIDTH * (colPos * BufferWidth + (buffered_j + BufferSize - offset_j)), ScanPs.HEIGHT * (rowPos * BufferWidth + (buffered_i + BufferSize - offset_i)));
                    }

                    // The division by 10 happens to convert fiduciary marker position to stitched image position e.g. D0J0 => 3, 9
                    g.DrawImage(CroppedImage, drawPoint);
                    if (BufferSize > 0)
                    {
                        g.DrawRectangle(BufferPen, (int) (ScanPs.WIDTH * (colPos * BufferWidth + 0.5)), (int) (ScanPs.HEIGHT * (rowPos * BufferWidth + 0.5)), 2 * ScanPs.WIDTH, 2 * ScanPs.HEIGHT);
                    }
                    if (!UseFiduc) g.DrawString(raftID, new Font("Sans-serif", 130), new SolidBrush(Color.Orchid), new Point(ScanPs.WIDTH * colPos * BufferWidth + 50, ScanPs.HEIGHT * rowPos * BufferWidth + 50));
                    CroppedImage.Dispose();
                    imageCounter++;
                }

                if (DrawGrid)
                {
                    Pen pen = new Pen(Color.MidnightBlue, 50);
                    for (int i = 1; i < ncol; i++)
                    {
                        g.DrawLine(pen, new Point(i * ScanPs.WIDTH * BufferWidth, 0), new Point(i * ScanPs.WIDTH * BufferWidth, FinalImage.Height));
                    }
                    for (int i = 1; i < nrow; i++)
                    {
                        g.DrawLine(pen, new Point(0, i * ScanPs.HEIGHT * BufferWidth), new Point(FinalImage.Width, i * ScanPs.HEIGHT * BufferWidth));
                    }
                }
            }
            
            // This is just so that you don't save over another file
            string name = Path.Combine(OutFolder, $"{Folder.Name}{EndString}" + "{0}.png");
            int counter = 0;
            string fname = String.Format(name, counter);
            while (File.Exists(fname))
            {
                counter++;
                fname = String.Format(name, counter);
            }

            FinalImage.Save(fname);
            FinalImage.Dispose();
            if (openAtEnd) {
                var PSI = new ProcessStartInfo(fname); PSI.UseShellExecute = true;
                Process.Start(PSI);
            }
        }

        /// <summary>
        /// Crops a large tiff to a specified raft
        /// </summary>
        /// <param name="fi">The FileInfo object corresponding to the large tiff</param>
        /// <param name="offset_i">The row offset from the top left</param>
        /// <param name="offset_j">The column offset from the top left</param>
        /// <returns>A Bitmap containing the cropped raft</returns>
        public Bitmap CroppedRaft(FileInfo fi, int offset_i, int offset_j, int width = -1, int height = -1) 
        {
            if (width < 0) width = ScanPs.WIDTH;
            if (height < 0) height = ScanPs.HEIGHT;
            Bitmap FullImage = (Bitmap)Image.FromFile(fi.FullName, true);

            Rectangle Crop = new Rectangle(new Point(ScanPs.CORNER_X + offset_j * ScanPs.MOVE_X, 
                                                     ScanPs.CORNER_Y + offset_i * ScanPs.MOVE_Y), 
                                           new Size(width, height));
            Bitmap CroppedImage = FullImage.Clone(Crop, FullImage.PixelFormat);
            FullImage.Dispose();
            return CroppedImage;
        }
    }

    public class CMSSQLite {

        public static string SaveFolder = "PickedRafts";
        /// <summary>
        /// Extracts the before-pick raft images from the sqlite file (much of this code is copied from the internet)
        /// </summary>
        /// <param name="SQLiteFile">The sqlite file path</param>
        public static void GetImages(string SQLiteFile, BackgroundWorker bw = null)
        {
            string OutFolder = Path.Combine((new FileInfo(SQLiteFile)).DirectoryName, SaveFolder);
            Directory.CreateDirectory(OutFolder);
            // Opens sqllite
            var conn = new SQLiteConnection($"Data Source={SQLiteFile};Version=3;", true);
            conn.Open();
            // Creates list of Raft IDs
            SQLiteCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT Collection_Wells.Raft, Collection_Images.Image " +
                              "FROM Collection_Wells " +
                              "INNER JOIN Collection_Images ON Collection_Wells.Id=Collection_Images.Collection_Well_Id";
            

            SQLiteDataReader Reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess);
            FileStream stream;  
            BinaryWriter writer;
            int bufferSize = 100;  
            byte[] outByte = new byte[bufferSize];
            long retval;
            long startIndex = 0;
            string FileName;

            while (Reader.Read())
            {
                if (bw.CancellationPending)
                {
                    Reader.Close();
                    conn.Close();
                    return;
                }
                int raft = Reader.GetInt32(0);
                string RaftID = $"{Tools.GetRaftPos(raft / 1000)}{Tools.GetRaftPos(raft % 1000)}";

                FileName = Path.Combine(OutFolder, $"{RaftID}F.tiff");
                if (File.Exists(FileName))
                {
                    FileName = Path.Combine(OutFolder, $"{RaftID}X.tiff");
                }
                stream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write);
                writer = new BinaryWriter(stream);

                startIndex = 0;

                retval = Reader.GetBytes(1, startIndex, outByte, 0, bufferSize);

                while (retval == bufferSize)
                {
                    writer.Write(outByte);
                    writer.Flush();
                    startIndex += bufferSize;
                    retval = Reader.GetBytes(1, startIndex, outByte, 0, bufferSize);
                }

                writer.Write(outByte, 0, (int)retval);
                writer.Flush();
                writer.Close();
                stream.Close();
            }
            Reader.Close();
            conn.Close();
        }

        /// <summary>
        /// Splits individual raft pictures based on their fiduciary markers
        /// </summary>
        /// <param name="ImageFolder">The folder containing the raft images</param>
        public static void SplitImages(string ImageFolder)
        {
            // Generates the list of raft image files
            DirectoryInfo di = new DirectoryInfo(ImageFolder);
            if (di.Name != SaveFolder) di = new DirectoryInfo(Path.Combine(di.FullName, SaveFolder));
            List<FileInfo> RaftFiles = di.GetFiles().Where(f => f.Name[4] == 'F').ToList();

            // Figures out what size the rafts are
            Bitmap First = (Bitmap)Image.FromFile(RaftFiles.First().FullName, true);
            int width, height;
            if (First.Width < 200)
            {
                width  = Tools.R100Quad.WIDTH;
                height = Tools.R100Quad.HEIGHT;
            }
            else
            {
                width  = Tools.R200.WIDTH;
                height = Tools.R200.HEIGHT;
            }

            // Sorts the rafts based on their fiduciary marker
            List<FileInfo> LetterRafts = new List<FileInfo>();
            List<FileInfo> DotRafts    = new List<FileInfo>();
            List<FileInfo> Others      = new List<FileInfo>();

            foreach (FileInfo Raft in RaftFiles)
            {
                if (Tools.IsFiducLetter(Raft.Name))
                {
                    LetterRafts.Add(Raft);
                }
                else if (Tools.IsFiducDot(Raft.Name))
                {
                    DotRafts.Add(Raft);
                }
                else
                {
                    Others.Add(Raft);
                }
            }

            // Saves a compilation image of each different group of rafts
            string OutFolder = di.Parent.FullName;
            if (LetterRafts.Count > 0) SaveRaftComp(LetterRafts, Path.Combine(OutFolder, "Letters.jpeg"), width, height);
            if (DotRafts.Count    > 0) SaveRaftComp(DotRafts,    Path.Combine(OutFolder, "Dots.jpeg"),    width, height);
            if (Others.Count      > 0) SaveRaftComp(Others,      Path.Combine(OutFolder, "Others.jpeg"),  width, height);
        }

        /// <summary>
        /// Saves a compilation image of a given list of raft images
        /// </summary>
        /// <param name="RaftFiles">A list containing the raft image files</param>
        /// <param name="outfile">The compilation image file path</param>
        /// <param name="width">The width (px) of a raft</param>
        /// <param name="height">The height (px) of a raft</param>
        private static void SaveRaftComp(List<FileInfo> RaftFiles, string outfile, int width = -1, int height = -1)
        {
            // Finds width and height if they are not given
            if (width == -1 || height == -1)
            {
                Bitmap First = (Bitmap)Image.FromFile(RaftFiles.First().FullName, true);
                if (First.Width < 200)
                {
                    width  = Tools.R100Quad.WIDTH;
                    height = Tools.R100Quad.HEIGHT;
                }
                else
                {
                    width  = Tools.R200.WIDTH;
                    height = Tools.R200.HEIGHT;
                }
            }

            // Figures out the optimal dimensions for the compilation image
            (int nrow, int ncol) = Tools.OptimalDims(RaftFiles.Count);

            // Creates the compilation image by saving each image sequentially by row
            Bitmap FinalImage = new Bitmap(ncol * width, nrow * height);
            int row_counter = 0;
            int col_counter = 0;
            
            using (Graphics g = Graphics.FromImage(FinalImage))
            {
                foreach (FileInfo raftfile in RaftFiles)
                {
                    Bitmap Raft = (Bitmap)Image.FromFile(raftfile.FullName, true);
                    g.DrawImage(Raft, col_counter * width, row_counter * height);
                    col_counter++;
                    if (col_counter >= ncol)
                    {
                        col_counter = 0;
                        row_counter++;
                    }
                }
            }
            FinalImage.Save(outfile);
            Process.Start(outfile);
        }
    }

    public static class FormTools
    {
        public static void SaveDB(string rootFolderName, List<string> projects, Dictionary<string, List<string>> projectScans)
        {
            using (var writer = new StreamWriter(Path.Combine(rootFolderName, ".cmsdb")))
            {
                foreach (string project in projects)
                {
                    writer.WriteLine($"{project}:{String.Join(",", projectScans[project])}");
                }
            }
        }

        public static (List<string> Projects, Dictionary<string, List<string>> ProjectScans) LoadDB(string dbfile)
        {
            var projects = new List<string>();
            var projectscans = new Dictionary<string, List<string>>();
            using (var reader = new StreamReader(dbfile))
            {
                string line;
                string[] split = new string[2];
                while ((line = reader.ReadLine()) != null)
                {
                    split = line.Split(':');
                    projects.Add(split[0]);
                    projectscans.Add(split[0], split[1].Split(',').ToList());
                }
            }
            return (projects, projectscans);
        }
    }
}
