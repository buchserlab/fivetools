﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using ImageMagick;
using OpenCvSharp;
using FIVE.InCellLibrary;
using FIVE.FOVtoRaftID;
using FIVE.ImageCheck;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.ComponentModel;
using FIVE_IMG;
using FIVE_IMG.Seg;
using System.Data;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;
using System.Timers;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;


namespace FIVE.RaftCal
{

    using static FIVE_IMG.Utilities.ColorMatrix;


    public partial class FormRaftCal : Form
    {
        public class OpenCVBox : Control
        {

            private Mat _MatImage;

            // Property to set the ImageMagick image
            public Mat MagickImage
            {

                get => _MatImage;
                set
                {
                    _MatImage?.Dispose(); // Dispose the old image
                    _MatImage = value;
                    Invalidate(); // Trigger repaint
                }
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);

                if (_MatImage != null)
                {
                    using (Bitmap bitmap = new Bitmap(_MatImage.ToMemoryStream()))
                    {
                        e.Graphics.DrawImage(bitmap, new Rectangle(0, 0, Width, Height));
                    }
                }
                else
                {
                    e.Graphics.Clear(BackColor);
                    TextRenderer.DrawText(e.Graphics, "No Image", Font, ClientRectangle, ForeColor, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
                }
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    _MatImage?.Dispose();
                }
                base.Dispose(disposing);
            }
        }
        public class MagickPictureBox : Control
        {
            private MagickImage _magickImage;

            // Property to set the ImageMagick image
            public MagickImage MagickImage
            {
                get => _magickImage;
                set
                {
                    _magickImage?.Dispose(); // Dispose the old image
                    _magickImage = value;
                    Invalidate(); // Trigger repaint
                }
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);

                if (_magickImage != null)
                {
                    using (Bitmap bitmap = _magickImage.ToBitmap())
                    {
                        e.Graphics.DrawImage(bitmap, new Rectangle(0, 0, Width, Height));
                    }
                }
                else
                {
                    e.Graphics.Clear(BackColor);
                    TextRenderer.DrawText(e.Graphics, "No Image", Font, ClientRectangle, ForeColor, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
                }
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    _magickImage?.Dispose();
                }
                base.Dispose(disposing);
            }
        }
        private DirectoryInfo _DI;
        private List<PictureBox> _PictureBoxes;

        private List<TextBox> txBx_Wv_Abbrev;
        private List<TextBox> txBx_Wv_Name;
        private List<TextBox> txBx_Wv_Thresh;
        private List<TextBox> txBx_Wv_Idx;
        private List<TextBox> txBx_Wv_Color;
        private List<TextBox> txBx_Wv_BrightnessMultiplier;
        private List<CheckBox> chkBx_ObjDetect;
        private List<CheckBox> chkBx_OnWV;
        private List<CheckBox> chkBx_DI;

        private List<XDCE_Image> _XDCEs;
        public INCELL_Folder IC_Folder;
        private XDCE_ImageGroup _CurrentWell;
        private CalibrationRafts CalRafts = new CalibrationRafts();
        private List<KnownRaft> KnownRaftsTemp;
        private double FullX, FullY;
        public FIVE_IMG.Store_Validation_Parameters NVP;
        public BackgroundWorker BWModelRun;

        public ImageCheck.ImageCheck_PointList ImageChk_List
        {
            get; set;
        }
        private static Brush RedBrush = new SolidBrush(Color.Red);
        private static Brush GreenBrush = new SolidBrush(Color.LightGreen);
        private static Pen MainPen = new Pen(Color.Red, 2);
        private static Pen CalPen = new Pen(Color.LightGreen, 3);
        private static Pen GridPen = new Pen(Color.Gray, 1);
        private static Font CalFont = new Font("Arial", 13, FontStyle.Bold);
        public FIVE_IMG.ImageQueue ImgQueue = new FIVE_IMG.ImageQueue(); //This is about a 4.3 Gigabyte of storage
        public ExportCalForm ExportCalForm;

        public List<Control> RaftOnly;
        public List<Control> FieldOnly;

        #region Public Properties ------------------------------------------------------------------------------------------------------------------------

        public DirectoryInfo DI
        {
            get
            {
                if (_DI == null)
                {
                    if (txBx_FolderPath.Text == "") txBx_FolderPath.Text = IC_Folder.FullPath;
                    _DI = new DirectoryInfo(txBx_FolderPath.Text);
                }
                return _DI;
            }
        }

        public bool RaftMode
        {
            get
            {
                return chkBx_SquareMode.Checked;
            }
            set
            {
                chkBx_SquareMode.Checked = value;
            }
        }

        public int Row
        {
            get
            {
                int nRow;
                if (!int.TryParse(txBx_Row.Text, out nRow)) nRow = 6;
                if (nRow < 0) nRow = 0;
                if (nRow > CurrentWell.Row_Count - 2) nRow = CurrentWell.Row_Count - 2;
                txBx_Row.Text = nRow.ToString();
                return nRow;
            }
            set
            {
                txBx_Row.Text = value.ToString();
            }
        }

        public int Col
        {
            get
            {
                int nCol;
                if (!int.TryParse(txBx_Col.Text, out nCol)) nCol = 6;
                if (nCol < 0) nCol = 0;
                if (nCol > CurrentWell.Col_Count - 2) nCol = CurrentWell.Col_Count - 2;
                txBx_Col.Text = nCol.ToString();
                return nCol;
            }
            set
            {
                txBx_Col.Text = value.ToString();
            }
        }

        private string PreviousWell = "--1--";
        public string WellLabel
        {
            get
            {
                return comboBox_Well.Text;
            }
            set
            {
                if (!IC_Folder.XDCE.Wells.ContainsKey(value)) return;
                comboBox_Well.Text = value;
            }
        }

        private void SetupWellList()
        {
            comboBox_Well.Items.Clear();
            IC_Folder.XDCE.CheckLoad();
            comboBox_Well.Items.AddRange(IC_Folder.XDCE.Wells.Keys.ToArray());
            if (comboBox_Well.Items.Count > 0) comboBox_Well.SelectedIndex = 0;
            PreviousWell = comboBox_Well.Text;
            WellLabel = comboBox_Well.Text;
        }

        private void SetupOverlayList()
        {
            comboBox_Overlay.Items.Clear();
            comboBox_Overlay.Items.AddRange(AnalysisFolder.GenImages_AppendList);
            if (comboBox_Overlay.Items.Count > 0) comboBox_Overlay.SelectedIndex = 0;
        }

        public XDCE_ImageGroup CurrentWell
        {
            get
            {
                //CancelBackground();
                if (IC_Folder == null) return null;
                if (IC_Folder.XDCE.Wells.Count == 0) return null;
                if (_CurrentWell != null && WellLabel == PreviousWell) return _CurrentWell;
                if (_CurrentWell != null && WellLabel != PreviousWell)
                {
                    if (!SaveCalibration(false))
                    {
                        WellLabel = _CurrentWell.NameAtLevel;
                        return _CurrentWell;
                    }
                    btn_SaveCheck_Click(null, null);
                }
                //We should only get here if we are making a new well . . (or swapping)
                if (WellLabel == "" || !IC_Folder.XDCE.Wells.ContainsKey(WellLabel)) _CurrentWell = IC_Folder.XDCE.Wells.First().Value;
                else _CurrentWell = IC_Folder.XDCE.Wells[WellLabel];
                PreviousWell = WellLabel;
                Row = 20; Col = 0; txBx_SavedList.Text = "";
                txBx_FOV.Text = _CurrentWell.FOV_Min.ToString(); // New 8/29/2024
                if (_CurrentWell.HasRaftCalibration)
                {
                    CalRafts = _CurrentWell.CalibrationRaftSettings;
                    KnownRaftsTemp = CalRafts.KnownRafts;
                    label_Warning.Text = CalRafts.CalibrationVersion + " Calibration:\r\n" + CalRafts.DateCalibrated + "\r\n"; // Click around the raft to check it.\r\nClick here to start a new one.";
                    radioButton_TestCal.Enabled = true;
                }
                else
                {
                    label_Warning.Text = "Click to Calibrate.";
                    CalRafts = new FOVtoRaftID.CalibrationRafts();
                    KnownRaftsTemp = new List<FOVtoRaftID.KnownRaft>(4);
                }
                if (_CurrentWell.HasImageCheck)
                {
                    ImageChk_List = _CurrentWell.ImageCheck_PointsList;
                    lblImageCheck.Text = "Loaded " + ImageChk_List.Count + " points.";
                }
                else
                {
                    ImageChk_List = new ImageCheck.ImageCheck_PointList(); //Update this to be part of the XDCE class
                    ImageChk_List.Name = Folder;
                    ImageChk_List.Well = WellLabel;
                    lblImageCheck.Text = "Start Image Check";
                }

                return _CurrentWell;
            }
            //set
            //{
            //   WellLabel = value.NameAtLevel;
            //   XDCE_ImageGroup Temp = CurrentWell; //Refreshes things
            //}
        }

        public short Wavelength
        {
            get
            {
                short nWav;
                if (!short.TryParse(txBx_Wavelength1.Text, out nWav)) nWav = 0;
                if (nWav < 0) nWav = 0;
                if (nWav > IC_Folder.XDCE.Wavelength_Count - 1) nWav = (short)Math.Max(0, (IC_Folder.XDCE.Wavelength_Count - 1));
                txBx_Wavelength1.Text = nWav.ToString();
                return nWav;
            }
            set
            {
                txBx_Wavelength1.Text = value.ToString();
            }
        }

        public float Brightness
        {
            get
            {
                float nCol;
                if (!float.TryParse(txBx_BrightnessMultiplier1.Text, out nCol)) nCol = 10;
                if (nCol < 0.00001) nCol = 0.00001F;
                if (nCol > 1000000) nCol = 1000000F;
                txBx_BrightnessMultiplier1.Text = nCol.ToString();
                return nCol;
            }
            set
            {
                txBx_BrightnessMultiplier1.Text = value.ToString();
            }
        }

        public void wvDP_Assign(int wvIdx0, wvDisplayParam wvP, bool DirectionToForm)
        {
            if (DirectionToForm)
            {
                txBx_Wv_Idx[wvIdx0].Text = wvP.wvIdx_Txt;
                txBx_Wv_BrightnessMultiplier[wvIdx0].Text = wvP.Brightness_Txt;
                txBx_Wv_Color[wvIdx0].Text = wvP.Color;
                txBx_Wv_Thresh[wvIdx0].Text = wvP.Threshold.ToString();

                chkBx_OnWV[wvIdx0].Checked = wvP.Active;
                chkBx_ObjDetect[wvIdx0].Checked = wvP.ObjectAnalysis;
                chkBx_DI[wvIdx0].Checked = wvP.DI_Analysis;
            }
            else
            {
                wvP.wvIdx_Txt = txBx_Wv_Idx[wvIdx0].Text;
                wvP.Brightness_Txt = txBx_Wv_BrightnessMultiplier[wvIdx0].Text;
                wvP.Color = txBx_Wv_Color[wvIdx0].Text;
                wvP.Threshold = txBx_Wv_Thresh[wvIdx0].Text == "" ? 0 : float.Parse(txBx_Wv_Thresh[wvIdx0].Text);

                wvP.Active = chkBx_OnWV[wvIdx0].Checked;
                wvP.ObjectAnalysis = chkBx_ObjDetect[wvIdx0].Checked;
                wvP.DI_Analysis = chkBx_DI[wvIdx0].Checked;
            }
        }

        public wvDisplayParams CurrentWVParams
        {
            set
            {
                wvDisplayParams wvPs = value;
                //Puts the info stored into the Form
                for (int i = 0; i < 5; i++) if (wvPs.Count > i) wvDP_Assign(i, wvPs[i], true);
            }
            get
            {
                try
                {
                    //Saves the info from the Form back into this variable
                    var wvPs = new wvDisplayParams();
                    wvPs.ColorCalcTypeStr = comboBox_ColorMerge.SelectedItem.ToString();
                    wvPs.UseClipping = chk_Bx_ObjClipsVis.Checked;
                    txBx_ClipExpand_Pos.Enabled = txBx_ClipExpand_Neg.Enabled = wvPs.UseClipping;
                    wvPs.ClippingExpand_positive = int.Parse(txBx_ClipExpand_Pos.Text);
                    wvPs.ClippingExpand_negative = int.Parse(txBx_ClipExpand_Neg.Text);
                    wvPs.UseSquareMode = chkBx_SquareMode.Checked;
                    wvDisplayParam wvP;
                    for (int i = 0; i < 5; i++)
                    {
                        wvP = new wvDisplayParam();
                        wvDP_Assign(i, wvP, false);
                        wvPs.Add(wvP);
                    }
                    if (wvPs.Actives.Count == 0) wvPs[0].Active = true;
                    return wvPs;
                }
                catch { return new wvDisplayParams(); }
            }
        }

        public int CurrentFOV
        {
            get
            {
                int d = 0;
                int.TryParse(txBx_FOV.Text.Trim(), out d);
                return d;
            }
            set
            {
                txBx_FOV.Text = value.ToString();
            }
        }

        private INCARTA_Analysis_Folder _AnalysisFolder;
        public INCARTA_Analysis_Folder AnalysisFolder
        {
            get { return _AnalysisFolder; }
            set
            {
                _AnalysisFolder = value;
                if (_AnalysisFolder != null)
                {
                    _AnalysisFolder.Refresh_GenList();
                    SetupOverlayList();
                }
                chkBx_ShowOverlay.Enabled = (_AnalysisFolder != null);
            }
        }

        public bool OpenWithDialog(string FolderToOpen, out INCELL_Folder ICFolder)
        {
            ICFolder = null;
            Tuple<string, string, string, string, string> Results;
            short ch = 0; double widthum = 0; int pixelsAcross = 0; string WellLabel = "";
            while (true)
            {
                var DR = MultiInputForm.ShowMulti("Settings for a non-XDCE image set.", out Results, "Channels", "2", "Pixel Width um", "0.325", "Image Width Pixels", "2048", "Well Label", "A - 1 ");
                if (DR == DialogResult.Cancel) return false;
                try
                {
                    ch = short.Parse(Results.Item1);
                    widthum = double.Parse(Results.Item2);
                    pixelsAcross = int.Parse(Results.Item3);
                    WellLabel = Results.Item4.ToUpper().Trim();
                    break;
                }
                catch { }
            }
            ICFolder = INCELL_Folder.CreateWithNoXDCE(FolderToOpen, ch, WellLabel, widthum, pixelsAcross);
            return true;
        }

        public bool OpenWithNoXDCE(string FolderToOpen)
        {
            //Check if there is a wavelength file
            INCELL_WV_Notes wvNotes = INCELL_WV_Notes.Load_Folder(FolderToOpen);
            //If not, open a box to ask some questions
            if (wvNotes != null)
            {
                if (wvNotes.NoXDCE_ChannelCount >= 1)
                {
                    IC_Folder = INCELL_Folder.CreateWithNoXDCE(FolderToOpen, wvNotes); return true;
                }
            }

            if (!OpenWithDialog(FolderToOpen, out IC_Folder)) return false;
            return true;
        }


        public string Folder
        {
            get
            {
                return txBx_FolderPath.Text;
            }
            set
            {
                CancelBackground();
                txBx_FolderPath.Text = value; //@"D:\Temp\Imaging\RaftArray_JCB_01\RaftArray_JCB_01_200ArrayTest_2\";

                var XDCEpath = XDCE.GetXDCEPath(value);
                if (XDCEpath == "")
                {
                    bool response = OpenWithNoXDCE(value);
                    if (response == false) Close();
                }
                else IC_Folder = new INCELL_Folder(XDCEpath, txBx_FolderPath.Text);

                if (IC_Folder.XDCE == null) { txBx_FolderPath.Text = "Try Refreshing."; return; }
                //AnalysisFolder = null;

                //Try to guess Raft vs. Non Raft
                chkBx_SquareMode.Checked = (value.ToUpper().Contains("QUAD") || value.ToUpper().Contains("SINGLE") || value.ToUpper().Contains("RAFT"));

                //Check to make sure that someone else isn't working on this . . 
                string CheckedOutStatus = Folder_AttemptCheckOut(IC_Folder.FullPath);
                if (CheckedOutStatus != "")
                {
                    DialogResult Res = MessageBox.Show("Another user (" + CheckedOutStatus + ") has this open, any changes are not recommended.\r\n OK = continue, Cancel = clear the check (usually avoid this).", "Check Out", MessageBoxButtons.OKCancel);
                    if (Res == DialogResult.Cancel)
                    {
                        CheckedOutAlready = false;
                        Folder_AttemptCheckOut(IC_Folder.FullPath, true);
                    }
                }
                //TODO : Stop or message the user

                SetupWellList();
                UpdateAll();
                AddWVNotes();
            }
        }

        /// <summary>
        /// This just makes the various Form Controls array accessible
        /// </summary>
        public void SetupBoxArrays()
        {
            txBx_Wv_Abbrev = new List<TextBox>() { txBx_Wv_Abbrev1, txBx_Wv_Abbrev2, txBx_Wv_Abbrev3, txBx_Wv_Abbrev4, txBx_Wv_Abbrev5 };
            txBx_Wv_Name = new List<TextBox>() { txBx_Wv_Name1, txBx_Wv_Name2, txBx_Wv_Name3, txBx_Wv_Name4, txBx_Wv_Name5 };
            txBx_Wv_Thresh = new List<TextBox>() { txBx_Wv_Thresh1, txBx_Wv_Thresh2, txBx_Wv_Thresh3, txBx_Wv_Thresh4, txBx_Wv_Thresh5 };
            txBx_Wv_Color = new List<TextBox>() { txBx_Color1, txBx_Color2, txBx_Color3, txBx_Color4, txBx_Color5 };
            txBx_Wv_Idx = new List<TextBox>() { txBx_Wavelength1, txBx_Wavelength2, txBx_Wavelength3, txBx_Wavelength4, txBx_Wavelength5 };
            txBx_Wv_BrightnessMultiplier = new List<TextBox>() { txBx_BrightnessMultiplier1, txBx_BrightnessMultiplier2, txBx_BrightnessMultiplier3, txBx_BrightnessMultiplier4, txBx_BrightnessMultiplier5 };

            chkBx_OnWV = new List<CheckBox>() { chkBx_Onwv1, chkBx_Onwv2, chkBx_Onwv3, chkBx_Onwv4, chkBx_Onwv5 };
            chkBx_ObjDetect = new List<CheckBox>() { chkBx_ObjDetect1, chkBx_ObjDetect2, chkBx_ObjDetect3, chkBx_ObjDetect4, chkBx_ObjDetect5 };
            chkBx_DI = new List<CheckBox>() { chkBx_DI_1, chkBx_DI_2, chkBx_DI_3, chkBx_DI_4, chkBx_DI_5 };
        }

        private void AddWVNotes()
        {
            //The wvParams are just linked to the user, and give the defaults, but if there are WVNotes, then those should override the wvParams
            var N = IC_Folder.InCell_Wavelength_Notes;
            N.ParentFolder = IC_Folder.FullPath;
            if (IC_Folder.Type_NoXDCE && N.NoXDCE_ChannelCount == 0)
            {
                N.NoXDCE_ChannelCount = IC_Folder.NoXDCE_Params.Item1; N.NoXDCE_PixelWidthMicrons = IC_Folder.NoXDCE_Params.Item2;
                N.NoXDCE_ImageWidthPixels = IC_Folder.NoXDCE_Params.Item3; N.NoXDCE_WellLabel = IC_Folder.NoXDCE_Params.Item4;
            }

            N.CheckSave(); //This saves out the wavelength file if it isn't already saved

            txBx_WavelengthExtended.Text = N.AQPNote;
            toolTip1.SetToolTip(txBx_WavelengthExtended, N.AQPNote);

            //Here is where we actually add the info to the form
            for (int i = 0; i < 5; i++) AddWVNotes_WV(N, i);
            //By default turn on the first one (but let a different pattern be saved)
            chkBx_OnWV[0].Checked = true;

            bool LoadDispParams = false;
            if (N.DisplayParams == null) N.DisplayParams = CurrentWVParams;
            else { if (N.DisplayParams.Count == 0) N.DisplayParams = CurrentWVParams; else LoadDispParams = true; }

            if (LoadDispParams) CurrentWVParams = N.DisplayParams;
        }

        public void WVNoteBox_EnableStatus(int wvIdx0Idx, bool Activate)
        {
            chkBx_ObjDetect[wvIdx0Idx].Enabled = chkBx_OnWV[wvIdx0Idx].Enabled = chkBx_DI[wvIdx0Idx].Enabled = Activate;
            if (!Activate)
            {
                //Only uncheck this if we are turning off, don't automatically check it
                chkBx_ObjDetect[wvIdx0Idx].Checked = chkBx_OnWV[wvIdx0Idx].Checked = chkBx_DI[wvIdx0Idx].Checked = Activate;
            }
            txBx_Wv_Abbrev[wvIdx0Idx].Enabled = txBx_Wv_Name[wvIdx0Idx].Enabled = txBx_Wv_Thresh[wvIdx0Idx].Enabled = Activate;
        }

        private void AddWVNotes_WV(INCELL_WV_Notes N, int wvIdx0Idx)
        {
            //First disable everything
            WVNoteBox_EnableStatus(wvIdx0Idx, false);

            //If this is a real position, enable it
            if (N.Count > wvIdx0Idx)
            {
                txBx_Wv_Name[wvIdx0Idx].Text = N.List[wvIdx0Idx].QuickName;
                txBx_Wv_Abbrev[wvIdx0Idx].Text = N.List[wvIdx0Idx].Abbrev;
                if (N.List[wvIdx0Idx].Abbrev == "-")
                {
                    switch (N.List[wvIdx0Idx].QuickName.ToUpper())
                    {
                        case string s when s.StartsWith("HOE"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "H";
                            break;
                        case string s when s.StartsWith("SPY650"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "H";
                            break;
                        case string s when s.StartsWith("DAPI"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "H";
                            break;
                        case string s when s.StartsWith("H2AX"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "2AX";
                            break;
                        case string s when s.StartsWith("BRIGHT"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "B";
                            break;
                        case string s when s.StartsWith("TL"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "B";
                            break;
                        case string s when s.StartsWith("MTSOX"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "SXM";
                            break;
                        case string s when s.StartsWith("MITOSOX"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "SXM";
                            break;
                        case string s when s.StartsWith("MITO"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "M";
                            break;
                        case string s when s.StartsWith("TMR"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "T";
                            break;
                        case string s when s.StartsWith("TDP43"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "P43";
                            break;
                        case string s when s.StartsWith("TDP-43"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "P43";
                            break;
                        case string s when s.StartsWith("CELLMASK"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "C";
                            break;
                        case string s when s.StartsWith("CELL MASK"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "C";
                            break;
                        case string s when s.StartsWith("FITC"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "L";
                            break;
                        case string s when s.StartsWith("MEMBRA"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "C";
                            break;
                        case string s when s.StartsWith("TUBULIN"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "U";
                            break;
                        case string s when s.StartsWith("LYSO"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "L";
                            break;
                        case string s when s.StartsWith("ER"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "ER";
                            break;
                        case string s when s.StartsWith("GOLG"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "GO";
                            break;
                        case string s when s.StartsWith("GFP"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "GFP";
                            break;
                        case string s when s.Contains("NUCLEUS"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "H";
                            break;
                        case string s when s.StartsWith("TRITC"):
                            txBx_Wv_Abbrev[wvIdx0Idx].Text = "A";
                            break;
                        default:
                            break;
                    }
                }
                WVNoteBox_EnableStatus(wvIdx0Idx, true);
                txBx_Wv_Thresh[wvIdx0Idx].Text = N.List[wvIdx0Idx].Threshold;
            }
        }

        #endregion

        #region Initiation and Setup ----------------------------------------------------------------------------------------------------------------


        public FormRaftCal(FIVE_IMG.Store_Validation_Parameters NVP)
        {
            InitializeComponent();
            Setup_ToMoveWhen_ErrorFixed();

            this.NVP = NVP;
            _PictureBoxes = new List<PictureBox>(4) { pictureBox1, pictureBox2, pictureBox3, pictureBox4 };
            _XDCEs = new List<XDCE_Image>(4) { null, null, null, null };
            SetupBoxArrays();
            foreach (PictureBox pBox in _PictureBoxes)
            {
                pBox.SizeMode = PictureBoxSizeMode.StretchImage;
                pBox.MouseClick += this.pictureBox_MouseClick;
                pBox.MouseMove += this.pictureBox_MouseMove;
                pBox.MouseLeave += this.pictureBox_MouseLeave;
            }

            RaftOnly = new List<Control>(); RaftOnly.Add(btn_U5); RaftOnly.Add(btn_UL); RaftOnly.Add(btn_UR); RaftOnly.Add(btn_Up); RaftOnly.Add(btn_D5); RaftOnly.Add(btn_Down); RaftOnly.Add(btn_L5); RaftOnly.Add(btn_Left); RaftOnly.Add(btn_R5); RaftOnly.Add(btn_Right); RaftOnly.Add(btn_BL); RaftOnly.Add(btn_BR); RaftOnly.Add(btn_RaftNext); RaftOnly.Add(btn_RaftPrev);
            RaftOnly.Add(txBx_Col); RaftOnly.Add(txBx_Row); /*RaftOnly.Add(txBx_SavedList);*/ RaftOnly.Add(txBx_RaftID); RaftOnly.Add(btnResetSaveCal); RaftOnly.Add(btn_SaveCalibration); RaftOnly.Add(radioButton_TestCal);

            FieldOnly = new List<Control>(); FieldOnly.Add(btnFieldBack); FieldOnly.Add(btnFieldNext); FieldOnly.Add(txBx_FOV);

            Row = 28; Col = 2;

            //Loads the saved settings
            var tWavParams = wvDisplayParams.Load();

            foreach (string CCtype in Enum.GetNames<ColorCalculationType>())
                comboBox_ColorMerge.Items.Add(CCtype);
            comboBox_ColorMerge.SelectedIndex = 1;
            if (tWavParams.ColorCalcTypeStr == null) tWavParams.ColorCalcTypeStr = "Add";
            comboBox_ColorMerge.Text = tWavParams.ColorCalcTypeStr;
            chk_Bx_ObjClipsVis.Checked = tWavParams.UseClipping;
            txBx_ClipExpand_Pos.Text = tWavParams.ClippingExpand_positive.ToString();
            txBx_ClipExpand_Neg.Text = tWavParams.ClippingExpand_negative.ToString();
            chkBx_SquareMode.Checked = tWavParams.UseSquareMode;

            CurrentWVParams = tWavParams;
            ExportCalForm = new ExportCalForm(this);

            if (IC_Folder != null) UpdateAll();
        }

        private void Setup_ToMoveWhen_ErrorFixed() //Can be moved to Initialize components once it is working again 3/2023
        {
            btnRunSegmentation.Text = "1: Segmentation";
            //string S = toolTip1.GetToolTip(btnRunSegmentation);

            Leica_Label.Text = "2: Classify";
            toolTip1.SetToolTip(Leica_Label, "Classify: Left=Run on All, Right=Settings");

            lbl_SegTest.Text = "3: Measure";
            lbl_SegTest.Click += Lbl_SegTest_Click;
            toolTip1.SetToolTip(lbl_SegTest, "Run InCarta-like model (Right Click=Settings)");

            txBx_RaftID.DoubleClick += DoubleClick_RaftIDBox;

            BWModelRun = new BackgroundWorker();
            //BWModelRun.WorkerSupportsCancellation = true;
            BWModelRun.WorkerReportsProgress = true;
            BWModelRun.DoWork += BWModelRun_DoWork;
            BWModelRun.ProgressChanged += BWModelRun_ProgressChanged;
            BWModelRun.RunWorkerCompleted += BWModelRun_RunWorkerCompleted;


        }

        private void FormRaftCal_Load(object sender, EventArgs e)
        {

        }

        private void FormRaftCal_Shown(object sender, EventArgs e)
        {
            //Update_Display(sender, e);  //The other one seems to work . . haven't tried this one
            UpdateAll();
            if (IC_Folder != null && IC_Folder.XDCE.Images.Any(t => t.IsTimeSeries == true))
            {
                btn_TimeSeriesBck.Visible = btn_TimeSeriesFwd.Visible = true;
                btn_TimeSeriesBck.Enabled = btn_TimeSeriesFwd.Enabled = true;
                btn_TimeSeriesFwd.Visible = true;
                btn_TimeSeriesFwd.Enabled = true;
                btn_SlideShow.Visible = true;
                btn_SlideShow.Enabled = true;
                txBx_MoveToTimePoint.Visible = true;
                txBx_MoveToTimePoint.Enabled = true;
                TimeLabel.Visible = true;
                TimeLabel.Enabled = true;

            }
        }

        public void CancelBackground()
        {
            bgWorker_ImageLoad.CancelAsync();
            bgWorker_Analysis1.CancelAsync();
            Spin(10);
        }

        private void FormRaftCal_FormClosing(object sender, FormClosingEventArgs e)
        {
            CancelBackground();
            CurrentWVParams.Save();
            if (!SaveCalibration(false)) e.Cancel = true;
            btn_SaveCheck_Click(null, null);
            Folder_AttemptCheckOut(Folder, true);
        }

        #endregion

        #region CheckIn System ----------------------------------------------------------------------------------------------------------------------

        private static string CheckedOutName = "FIVTools_CheckedOutStatus.txt";
        private bool CheckedOutAlready = false;

        private string Folder_AttemptCheckOut(string folderPath, bool Return = false)
        {
            string statusWrite = "";
            if (Return)
            {
                //This means we are returning it in (no longer checked out)
                if (CheckedOutAlready) return "";
            }
            else
            {
                //This means we are trying to check it out
                string CheckedOutStatus = FolderCheckedOutStatus(folderPath);
                if (CheckedOutStatus != "")
                {
                    if (CheckedOutStatus.StartsWith(Environment.MachineName))
                    {
                        CheckedOutAlready = false;
                        return "";
                    }
                    else
                    {
                        CheckedOutAlready = true;
                        return CheckedOutStatus;
                    }
                }
                statusWrite = Environment.MachineName + DateTime.Now.ToString(" M/dd HHH:mm");
            }
            File.WriteAllText(Path.Combine(folderPath, CheckedOutName), statusWrite);
            CheckedOutAlready = false;
            return "";
        }

        private string FolderCheckedOutStatus(string folderPath)
        {
            string CheckedOutStatus = "";
            FileInfo FI = new FileInfo(Path.Combine(folderPath, CheckedOutName));
            if (FI.Exists)
            {
                CheckedOutStatus = File.ReadAllText(FI.FullName);
            }
            return CheckedOutStatus;
        }

        #endregion

        #region Refresh, UpdateBox, UpdateAll ------------------------------------------------------------------------------------------------

        public void UpdateAll()
        {
            if (!this.Visible) return;
            //if (bgWorker_ImageLoad.IsBusy) bgWorker_ImageLoad.CancelAsync(); //Not sure the best here . . 
            Update_Display(null, null);
        }

        private void UpdateEnableds()
        {
            foreach (Control item in RaftOnly) item.Enabled = RaftMode;
            foreach (Control item in FieldOnly) item.Enabled = !RaftMode;
            //TODO: wv index should disabled on the parent mode setting before here
            btnRunSegmentation.Enabled = (chkBx_ObjDetect1.Checked || chkBx_ObjDetect2.Checked || chkBx_ObjDetect3.Checked || chkBx_ObjDetect4.Checked || chkBx_ObjDetect5.Checked || chkBx_DI_1.Checked || chkBx_DI_2.Checked || chkBx_DI_3.Checked || chkBx_DI_4.Checked || chkBx_DI_5.Checked);
            CurrentWVParams.Save();

            radioButton_RecordFocus.Enabled = true;
            radioButton_TestCal.Enabled = CalRafts.Calibrated;
            btnJumpNextCalPoint.Enabled = radioButton_TestCal.Checked;
        }

        protected void Update_Display(object sender, EventArgs e)
        {
            if (!this.Visible) return;
            UpdateEnableds();
            if (CurrentWell == null) return;
            XDCE_Image xI = null;
            if (RaftMode)
            {
                xI = CurrentWell.GetImage(Row, Col, Wavelength);
                if (xI != null)
                {
                    if (CalRafts.Calibrated) HideAllDragPoints(); //I think this is to erase them if you moved around
                    if (txBx_FOV.Text != xI.FOV.ToString())
                    {
                        txBx_FOV.Text = xI.FOV.ToString();
                        txBx_RaftID.Text = "";
                    }
                }
            }
            else
            {
                if (CurrentFOV < CurrentWell.FOV_Min)
                {
                    if (IC_Folder.XDCE.Wells.Count == 1) CurrentFOV = CurrentWell.FOV_Max;
                    else
                    {
                        CurrentFOV = CurrentWell.FOV_Max - 3; //CurrentWell = CurrentWell.WellPrevious(IC_Folder.XDCE);
                        WellLabel = CurrentWell.WellPrevious(IC_Folder.XDCE, IC_Folder.PlateID).NameAtLevel;
                        return; //since this already triggers it . . 
                    }
                }
                if (CurrentFOV + 0 > CurrentWell.FOV_Max)
                {
                    CurrentFOV = CurrentWell.FOV_Min;
                    string NewWell = CurrentWell.WellNext(IC_Folder.XDCE, IC_Folder.PlateID).NameAtLevel;
                    comboBox_Well.Text = NewWell;
                    return; //since this already triggers it . . 
                }
                xI = CurrentWell.GetField(CurrentFOV, Wavelength);
                txBx_FOV.Text = CurrentFOV.ToString();
            }
            if (xI == null) { txBx_ImageName.Text = "Doesn't Exist"; return; }

            txBx_ImageName.Text = xI.FileName;
            txBx_LocationInfo.Text = xI.PlateX_um.ToString("0.00") + ", " + xI.PlateY_um.ToString("0.00");

            int BxIdx = 0; int dim = (CurrentWell.FOV_Max - CurrentWell.FOV_Min >= 3) ? 2 : 1;
            for (int r = 0; r < dim; r++)
                for (int c = 0; c < dim; c++)
                {

                    var tXI = UpdateBox(BxIdx++, r + Row, c + Col);
                    //FIVE_IMG.MasksHelper.ExportShiftedMask(tXI, NVP.RegParams); //Just for testing mask stuff
                    if (tXI != null) { if (r == 0 && c == 1) txBx_FOV_UR.Text = tXI.FOV.ToString(); if (r == 1 && c == 1) txBx_FOV_LR.Text = tXI.FOV.ToString(); if (r == 1 && c == 0) txBx_FOV_LL.Text = tXI.FOV.ToString(); }  //Fill out the FOVs
                }
            //radioButton_RecordFocus.Enabled = radioButton_TestCal.Enabled = (CalRafts.KnownRafts.Count == 4); //Turned off 5/24/2022, all in update enabled now
            btn_SaveCheck.Enabled = true; // btnResetImageCheck.Enabled = ImageChk_List.Count > 0; //SOmetimes you want to save it as nothing 3/2023
            lblImageCheck.Text = ImageChk_List.Count + " Points";

            BackgroundLoadNext(); //Load the next set of images in the background (on another thread)
        }

        wvDisplayParams LastUsedWVPs;

        public Bitmap CombinedBMAP(XDCE_ImageGroup Welli, XDCE_Image xI, bool IsBackgroundProcess = false)
        {
            wvDisplayParams wvPs;
            if (IsBackgroundProcess) wvPs = LastUsedWVPs;
            else { wvPs = CurrentWVParams; LastUsedWVPs = wvPs; }


            var (bmp, _) = ImgQueue.CombinedBMAP(Welli, xI, wvPs, IsBackgroundProcess);
            return bmp;
        }

        private XDCE_Image UpdateBox(int BxIdx, int r, int c, XDCE_Image theXi = null)
        {
            var PB = _PictureBoxes[BxIdx];  //This picture box


            var xI = theXi == null ? GetCurrentXI(CurrentWell, BxIdx, r, c, Wavelength) : theXi;


            if (xI == null)
            {
                var bmp = new Bitmap(60, 60);
                using (var g = Graphics.FromImage(bmp)) { g.Clear(Color.Blue); }
                PB.Image = bmp;
                txBx_ImageName.Text = "Doesn't Exist"; return xI;
            }
            else { /*txBx_ImageName.Text = xI.FileName;*/ }
            _XDCEs[BxIdx] = xI;

            var bmap = CombinedBMAP(CurrentWell, xI);
            if (bmap == null) return null;

            //Add Overlay - - - - - - - 
            if (this.AnalysisFolder != null && chkBx_ShowOverlay.Checked)
            {
                object selectedItem = comboBox_Overlay.SelectedItem;
                if (selectedItem != null)
                {
                    string[] parts = AnalysisFolder.GenImages_Dict[selectedItem.ToString()];
                    string t2 = parts[0] + xI.FileName_GenImageBaseWV(int.Parse(parts[1])) + parts[2] + ".tif";
                    string OName = Path.Combine(this.AnalysisFolder.FolderPath_GenImages, t2);

                    var mI = new MagickImage(OName); mI.AutoLevel();
                    bmap = mI.ToBitmap();
                }
            }

            //Actually show the image
            if (PB.Image != null) PB.Image.Dispose();
            PB.Image = bmap.Clone(new Rectangle(0, 0, bmap.Width, bmap.Height), PixelFormat.DontCare);
            txBx_ImageName.Text = xI.FileName;
            PB.Refresh();
            if (radioButton_RecordFocus.Checked) ReDraw_CheckPoints(xI, PB, ImageChk_List);  //Draw the annotatoin points
            if (radioButton_TestCal.Checked) ReDraw_CalPoints(xI, PB);                       //Add Calibration Pointsc
            else if (chkBx_ShowCalGrid.Checked) ReDraw_CalPoints(xI, PB, true);
            return xI;
        }
        private void ClearBox(int boxIndex)
        {
            var PB = _PictureBoxes[boxIndex];
            if (PB.Image != null) PB.Image.Dispose();
            var blank = new Bitmap(60, 60);
            using (var g = Graphics.FromImage(blank)) { g.Clear(Color.Blue); }
            PB.Image = blank;
            PB.Refresh();
        }
        private XDCE_Image GetCurrentXI(XDCE_ImageGroup Well, int BxIdx, int r, int c, short wv)
        {
            XDCE_Image xI = null;
            if (RaftMode) //Raft mode vs Field mode
            {
                xI = Well.GetImage(r, c, wv);
            }
            else
            {
                xI = Well.GetField(CurrentFOV + BxIdx, wv);
            }

            return xI;
        }

        private System.Drawing.Bitmap GetAdjustedImage(XDCE_Image xI, float BrightVal = -1)
        {
            return FIVE_IMG.Utilities.DrawingUtils.GetAdjustedImageSt(DI.FullName, xI, BrightVal < 0 ? Brightness : BrightVal);
        }

        private void DrawRaftBounds(PictureBox pB, XDCE_Image xI, ReturnRaft rR, bool HiLite, bool ShowRaftID, bool GridMode = false)
        {
            if (rR == null) return;
            List<PointF> Points = ReturnRaft.PointsFromCornersOnImage(pB.Size, xI, rR);
            Graphics g = pB.CreateGraphics();
            g.DrawLines(GridMode ? GridPen : (HiLite ? CalPen : MainPen), Points.ToArray());
            if (ShowRaftID)
            {
                g.DrawString(" " + rR.RaftID, CalFont, Brushes.White, Points[0]);
                g.DrawString(" " + rR.RaftID, CalFont, HiLite ? GreenBrush : RedBrush, new PointF(Points[0].X + 1, Points[0].Y + 1));
                //g.DrawString(" " + rR.RaftID, CalFont, Brushes.Black, new PointF(Points[0].X + 2, Points[0].Y + 2));
            }
        }

        #endregion

        #region Background Load ------------------------------------------------------------------------------------------------------------------

        public void BackgroundLoadNext()
        {
            txBx_SavedList.Text = "Background loading more images . .";
            //string Note = "Not Busy"; //These notes were to see more of the details
            if (bgWorker_ImageLoad.IsBusy)
            {
                bgWorker_ImageLoad.CancelAsync();
                //int counter = 0;
                while (bgWorker_ImageLoad.IsBusy) { Spin(10); /*counter++;*/ } //We have a new set of variables, so we should spin and then retry
                                                                               //Note = "Busy " + (counter*10);
            }
            var Args = new Tuple<XDCE_ImageGroup, int, int, short>(CurrentWell, Row, Col, Wavelength);
            bgWorker_ImageLoad.RunWorkerAsync(Args);
        }


        public void Spin(int msec)
        {
            DateTime Start = DateTime.Now;
            while ((DateTime.Now - Start).TotalMilliseconds < msec)
            {
                Application.DoEvents();
            }
        }

        public static List<Tuple<int, int>> LoadInSpiral(int totalCount)
        {
            // Pre-allocate the result list
            var result = new List<Tuple<int, int>>(totalCount);

            int[] rowOffsets = { 0, -1, 0, 1 }; // Y-axis movement
            int[] colOffsets = { 1, 0, -1, 0 }; // X-axis movement

            int currentRow = 0;
            int currentCol = 0;

            int direction = 0;
            int stepsInCurrentDirection = 1;
            int stepsTakenInCurrentDirection = 0;
            int changesInDirection = 0;

            for (int i = 0; i < totalCount; i++)
            {
                // Add current position to the result
                result.Add(Tuple.Create(currentRow, currentCol));

                // Move in the current direction
                currentRow += rowOffsets[direction];
                currentCol += colOffsets[direction];
                stepsTakenInCurrentDirection++;

                // If steps in the current direction are complete
                if (stepsTakenInCurrentDirection == stepsInCurrentDirection)
                {
                    // Change direction
                    direction = (direction + 1) % 4;
                    stepsTakenInCurrentDirection = 0;
                    changesInDirection++;

                    // Increase step count every two direction changes
                    if (changesInDirection % 2 == 0)
                    {
                        stepsInCurrentDirection++;
                    }
                }
            }

            return result;
        }

        //Cross pattern (not including the center) - it does this list in order, always trying to get the closest fields first
        //r:\dB\Software\FIVE_Tools\Settings\BackgroundLoadOrder.xlsx
        private List<Tuple<int, int>> LoadList = LoadInSpiral(1250);

        //56 version:
        //new Tuple<int, int>(0, 2), new Tuple<int, int>(1, 2), new Tuple<int, int>(0, 3), new Tuple<int, int>(1, 3), new Tuple<int, int>(0, -1), new Tuple<int, int>(1, -1), new Tuple<int, int>(0, -2), new Tuple<int, int>(1, -2), new Tuple<int, int>(0, 4), new Tuple<int, int>(1, 4), new Tuple<int, int>(0, 5), new Tuple<int, int>(1, 5), new Tuple<int, int>(2, 0), new Tuple<int, int>(2, 1), new Tuple<int, int>(3, 0), new Tuple<int, int>(3, 1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1), new Tuple<int, int>(-2, 0), new Tuple<int, int>(-2, 1), new Tuple<int, int>(0, -3), new Tuple<int, int>(1, -3), new Tuple<int, int>(0, -4), new Tuple<int, int>(1, -4), new Tuple<int, int>(0, 6), new Tuple<int, int>(1, 6), new Tuple<int, int>(0, 7), new Tuple<int, int>(1, 7), new Tuple<int, int>(0, 8), new Tuple<int, int>(1, 8), new Tuple<int, int>(0, 9), new Tuple<int, int>(1, 9), new Tuple<int, int>(2, 2), new Tuple<int, int>(3, 2), new Tuple<int, int>(2, 3), new Tuple<int, int>(3, 3), new Tuple<int, int>(2, -1), new Tuple<int, int>(3, -1), new Tuple<int, int>(2, -2), new Tuple<int, int>(3, -2), new Tuple<int, int>(-1, 2), new Tuple<int, int>(-2, 2), new Tuple<int, int>(-1, 3), new Tuple<int, int>(-2, 3), new Tuple<int, int>(-1, -1), new Tuple<int, int>(-2, -1), new Tuple<int, int>(-1, -2), new Tuple<int, int>(-2, -2),
        //128 version:
        //new Tuple<int, int>(0, 2), new Tuple<int, int>(1, 2), new Tuple<int, int>(0, 3), new Tuple<int, int>(1, 3), new Tuple<int, int>(0, -1), new Tuple<int, int>(1, -1), new Tuple<int, int>(0, -2), new Tuple<int, int>(1, -2), new Tuple<int, int>(0, 4), new Tuple<int, int>(1, 4), new Tuple<int, int>(0, 5), new Tuple<int, int>(1, 5), new Tuple<int, int>(0, -3), new Tuple<int, int>(1, -3), new Tuple<int, int>(0, -4), new Tuple<int, int>(1, -4), new Tuple<int, int>(2, 0), new Tuple<int, int>(2, 1), new Tuple<int, int>(3, 0), new Tuple<int, int>(3, 1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1), new Tuple<int, int>(-2, 0), new Tuple<int, int>(-2, 1), new Tuple<int, int>(0, 6), new Tuple<int, int>(1, 6), new Tuple<int, int>(0, 7), new Tuple<int, int>(1, 7), new Tuple<int, int>(0, -5), new Tuple<int, int>(1, -5), new Tuple<int, int>(0, -6), new Tuple<int, int>(1, -6), new Tuple<int, int>(0, 8), new Tuple<int, int>(1, 8), new Tuple<int, int>(0, 9), new Tuple<int, int>(1, 9), new Tuple<int, int>(0, -7), new Tuple<int, int>(1, -7), new Tuple<int, int>(0, -8), new Tuple<int, int>(1, -8), new Tuple<int, int>(2, 2), new Tuple<int, int>(3, 2), new Tuple<int, int>(2, 3), new Tuple<int, int>(3, 3), new Tuple<int, int>(2, -1), new Tuple<int, int>(3, -1), new Tuple<int, int>(2, -2), new Tuple<int, int>(3, -2), new Tuple<int, int>(-1, 2), new Tuple<int, int>(-2, 2), new Tuple<int, int>(-1, 3), new Tuple<int, int>(-2, 3), new Tuple<int, int>(-1, -1), new Tuple<int, int>(-2, -1), new Tuple<int, int>(-1, -2), new Tuple<int, int>(-2, -2), new Tuple<int, int>(2, 4), new Tuple<int, int>(3, 4), new Tuple<int, int>(2, 5), new Tuple<int, int>(3, 5), new Tuple<int, int>(2, -3), new Tuple<int, int>(3, -3), new Tuple<int, int>(2, -4), new Tuple<int, int>(3, -4), new Tuple<int, int>(-1, 4), new Tuple<int, int>(-2, 4), new Tuple<int, int>(-1, 5), new Tuple<int, int>(-2, 5), new Tuple<int, int>(-1, -3), new Tuple<int, int>(-2, -3), new Tuple<int, int>(-1, -4), new Tuple<int, int>(-2, -4), new Tuple<int, int>(2, 6), new Tuple<int, int>(3, 6), new Tuple<int, int>(2, 7), new Tuple<int, int>(3, 7), new Tuple<int, int>(2, -5), new Tuple<int, int>(3, -5), new Tuple<int, int>(2, -6), new Tuple<int, int>(3, -6), new Tuple<int, int>(-1, 6), new Tuple<int, int>(-2, 6), new Tuple<int, int>(-1, 7), new Tuple<int, int>(-2, 7), new Tuple<int, int>(-1, -5), new Tuple<int, int>(-2, -5), new Tuple<int, int>(-1, -6), new Tuple<int, int>(-2, -6), new Tuple<int, int>(4, 2), new Tuple<int, int>(5, 2), new Tuple<int, int>(4, 3), new Tuple<int, int>(5, 3), new Tuple<int, int>(4, -1), new Tuple<int, int>(5, -1), new Tuple<int, int>(4, -2), new Tuple<int, int>(5, -2), new Tuple<int, int>(-3, 2), new Tuple<int, int>(-4, 2), new Tuple<int, int>(-3, 3), new Tuple<int, int>(-4, 3), new Tuple<int, int>(-3, -1), new Tuple<int, int>(-4, -1), new Tuple<int, int>(-3, -2), new Tuple<int, int>(-4, -2), new Tuple<int, int>(4, 4), new Tuple<int, int>(5, 4), new Tuple<int, int>(4, 5), new Tuple<int, int>(5, 5), new Tuple<int, int>(4, -3), new Tuple<int, int>(5, -3), new Tuple<int, int>(4, -4), new Tuple<int, int>(5, -4), new Tuple<int, int>(-3, 4), new Tuple<int, int>(-4, 4), new Tuple<int, int>(-3, 5), new Tuple<int, int>(-4, 5), new Tuple<int, int>(-3, -3), new Tuple<int, int>(-4, -3), new Tuple<int, int>(-3, -4), new Tuple<int, int>(-4, -4),

        private void bgWorker_ImageLoad_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var Args = (Tuple<XDCE_ImageGroup, int, int, short>)e.Argument;

            int BxIdx = 0; int result_count = 0;
            foreach (Tuple<int, int> LL in LoadList)
            {
                if (bgWorker_ImageLoad.CancellationPending) return;
                XDCE_Image xI = GetCurrentXI(Args.Item1, BxIdx++, LL.Item1 + Args.Item2, LL.Item2 + Args.Item3, Args.Item4);

                if (bgWorker_ImageLoad.CancellationPending) return;
                if (xI != null)
                {
                    CombinedBMAP(Args.Item1, xI, true);
                    result_count++;
                }
            }
            e.Result = result_count.ToString();
        }
        /*
                private void bgWorker_ImageLoad_DoWork_Parallel(object sender, System.ComponentModel.DoWorkEventArgs e)
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    var Args = (Tuple<XDCE_ImageGroup, int, int, short>)e.Argument;
                    var imageGroup = Args.Item1;

                    int resultCount = 0;
                    try
                    {
                        Parallel.ForEach(LoadList, new ParallelOptions { MaxDegreeOfParallelism = 4 }, X =>
                        {
                            // Check for cancellation (if needed)
                            if (bgWorker_ImageLoad.CancellationPending) return;
                            var arg2 = Args.Item2;
                            var arg3 = Args.Item3;
                            var arg4 = Args.Item4;

                            // Process the image
                            int argument2 = Interlocked.Add(ref arg2, X.Item1);
                            int argument3 = Interlocked.Add(ref arg3, X.Item2);
                            XDCE_Image xI = GetCurrentXI(imageGroup, Interlocked.Increment(ref resultCount), argument2, argument3, arg4);
                            if (xI != null)
                            {
                                CombinedBMAP(imageGroup, xI, true);
                            }
                        });

                        Debug.WriteLine($"Processing completed. Total processed: {resultCount}");
                    }
                    catch (OperationCanceledException)
                    {
                        Trace.WriteLine("Processing was canceled.");
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine($"Error during processing: {ex.Message}");
                    }
                    Debug.WriteLine("Total Time to Load 1,250 Images: " +sw.ElapsedMilliseconds);
                    e.Result = resultCount.ToString();
                }
        */

        private void bgWorker_ImageLoad_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            txBx_SavedList.Text = ""; //DateTime.Now + "\r\n" + (string)e.Result;
        }

        #endregion

        #region Navigation Events ----------------------------------------------------------------------------------------------------------------

        private void btn_Up_Click(object sender, EventArgs e) { Row--; UpdateAll(); }

        private void btn_Left_Click(object sender, EventArgs e) { Col--; UpdateAll(); LastDirection = -1; }

        private void btn_Right_Click(object sender, EventArgs e) { Col++; UpdateAll(); LastDirection = 1; }

        private void btn_RaftPrev_Click(object sender, EventArgs e) { Col -= 2; UpdateAll(); LastDirection = -1; }

        private void btn_RaftNext_Click(object sender, EventArgs e) { Col += 2; UpdateAll(); LastDirection = 1; }

        private void btnFieldSnake_Click(object sender, EventArgs e) { MoveFieldSnakePattern(); }

        private int LastDirection = 0;
        public void MoveFieldSnakePattern()
        {
            if (RaftMode == true)
            {
                if (CurrentWell.FOVCount == 4)
                {
                    CurrentFOV += 4;
                }
                else
                {
                    if (LastDirection == 0)
                        LastDirection = 1;
                    int LastCol = Col;
                    Col += LastDirection * 2;
                    if (Col == LastCol)
                    {
                        //Means it didn't move, so we have to go up
                        Row -= 2;
                        LastDirection *= -1;
                    }
                }
            }
            else
            {
                CurrentFOV += 4;
            }
            UpdateAll();
        }

        private void btn_Down_Click(object sender, EventArgs e) { Row++; UpdateAll(); }

        private void btn_U5_Click(object sender, EventArgs e) { Row -= 5; UpdateAll(); }

        private void btn_R5_Click(object sender, EventArgs e) { Col += 5; UpdateAll(); }

        private void btn_D5_Click(object sender, EventArgs e) { Row += 5; UpdateAll(); }

        private void btn_L5_Click(object sender, EventArgs e) { Col -= 5; UpdateAll(); }

        private void btn_UL_Click(object sender, EventArgs e) { Col = Row = 0; UpdateAll(); }

        private void btn_BR_Click(object sender, EventArgs e) { Col = Row = 1000; UpdateAll(); }

        private void btn_BL_Click(object sender, EventArgs e) { Col = 0; Row = 1000; UpdateAll(); }

        private void btn_UR_Click(object sender, EventArgs e) { Col = 1000; Row = 0; UpdateAll(); }

        private void btnFieldBack_Click(object sender, EventArgs e) { CurrentFOV -= 4; UpdateAll(); }

        private void btnFieldNext_Click(object sender, EventArgs e) { CurrentFOV += 4; UpdateAll(); }

        #endregion

        #region More Events ---------------------------------------------------------------------------------

        private void btn_SaveCheck_Click(object sender, EventArgs e)
        {
            var MEA = (MouseEventArgs)e; if (MEA == null) MEA = new MouseEventArgs(MouseButtons.Left, 1, 50, 50, 0);

            if (MEA.X > 5)
            {
                if (!ImageChk_List.Dirty_Saved)
                {
                    //if (ImageChk_List.Count == 0) return; //Prevents overwriting, but no longer best option now that we have a SAved_Dirty track 3/2022
                    txBx_SavedList.Text = "Didn't save since it is clean. Click on the far left of the button to force.";
                    return; //Don't save if this is clean
                }
            }
            ImageChk_List.SaveDefault(Folder);

            CurrentWell.ImageCheck_PointsList = ImageChk_List;

            lblImageCheck.Text = "Saved " + ImageChk_List.Count;
            //Now push this out to the IC_folder so it can be remembered
        }

        private void btn_SaveCalibration_Click(object sender, EventArgs e)
        {
            var e2 = (MouseEventArgs)e;

            //-----------------------------------

            SaveCalibration(true);
        }

        private int KnownRaftIndex = -1;

        private void btnJumpNextCalPoint_Click(object sender, EventArgs e)
        {
            if (CalRafts == null) return;
            KnownRaftIndex++; if (KnownRaftIndex >= CalRafts.KnownRafts.Count) KnownRaftIndex = 0;

            var KR = CalRafts.KnownRafts[KnownRaftIndex];

            CurrentWell.Refresh_Wells_Rafts(11); //Slow - but aster with a smaller number here . . ~8 is the smallest

            if (CurrentWell.Rafts.ContainsKey(KR.RaftID))
            {
                var xIG = CurrentWell.Rafts[KR.RaftID].Images[0];
                Col = CurrentWell.X_to_Index[xIG.PlateX_um];
                Row = CurrentWell.Y_to_Index[xIG.PlateY_um];
                btnJumpNextCalPoint.Text = "cal " + (KnownRaftIndex + 1).ToString();
                UpdateAll();
            }
        }


        private void comboBox_Well_SelectedIndexChanged(object sender, EventArgs e) { UpdateAll(); }

        private void btnResetCalibration(object sender, EventArgs e)
        {
            label_Warning.Text = "Cal Reset";
            txBx_LocationInfo.Text = "";
            txBx_SavedList.Text = "";
            HideAllDragPoints();
            CalRafts = new FOVtoRaftID.CalibrationRafts();
            KnownRaftsTemp = new List<FOVtoRaftID.KnownRaft>();
        }

        private void lblImageCheck_Click(object sender, EventArgs e)
        {
            txBx_SavedList.Text = CurrentWell.ImageCheckInfo;
        }

        private void btnResetImageCheck_Click(object sender, EventArgs e)
        {
            DialogResult DR = MessageBox.Show("Are you sure you want to delete the Check List?", "Delete Check List", MessageBoxButtons.OKCancel);
            if (DR == DialogResult.OK)
            {
                lblImageCheck.Text = "Reset.";
                ImageChk_List = new ImageCheck_PointList();
                ImageChk_List.Well = WellLabel;
                ImageChk_List.Name = Folder;
                UpdateAll();
            }
        }

        /// <summary>
        /// Takes away the last point that was just added
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUndoImageCheck_Click(object sender, EventArgs e)
        {
            ImageChk_List.RemoveLast();
            UpdateAll();
        }

        private void chkBx_RaftToggle_CheckedChanged(object sender, EventArgs e)
        {
            UpdateAll();
        }

        private void btn_FullSize_Click(object sender, EventArgs e)
        {
            Button sButton = (Button)sender;
            string newSizeSz = sButton.Text;
            int oldSize = pictureBox1.Width;
            int newSize = int.Parse(newSizeSz); int edge = 6; int extra = 6;

            this.Width = this.Width - (oldSize * 2) + newSize * 2;
            this.Height = this.Height - (oldSize * 2) + newSize * 2;

            pictureBox1.Width = pictureBox1.Height = pictureBox2.Width = pictureBox2.Height =
                pictureBox3.Width = pictureBox3.Height = pictureBox4.Width = pictureBox4.Height = newSize;

            pictureBox1.Left = edge; pictureBox1.Top = edge;
            pictureBox2.Left = edge + newSize + extra; pictureBox2.Top = edge;
            pictureBox3.Left = edge; pictureBox3.Top = edge + newSize + extra;
            pictureBox4.Left = edge + newSize + extra; pictureBox4.Top = edge + newSize + extra;

            panelControls.Left = panelControls.Left - (oldSize * 2) + (newSize * 2);

            UpdateAll();
        }

        private void chkBx_ShowOverlay_CheckedChanged(object sender, EventArgs e)
        {
            Update_Display(sender, e);
        }

        private void comboBox_Overlay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update_Display(sender, e);
        }

        #endregion

        #region AnnotateFromTrainedModel ------------------------------------------------------------------------------------


        private void Model_Run_Classification_Go(object sender, EventArgs e)
        {
            ModelGenericGo(true, ((MouseEventArgs)e));
        }

        private void Lbl_SegTest_Click(object sender, EventArgs e)
        {
            ModelGenericGo(false, (MouseEventArgs)e);
        }

        public void ModelGenericGo(bool ModelTypeClassify, MouseEventArgs MEA)
        {
            if (MEA.Button == MouseButtons.Right)
                UpdateModelNVP(ModelTypeClassify);

            //NVP.ModelInferSettings. L00K
            NVP.ModelInferSettings.cmPerformClassifications = true;
            NVP.ModelInferSettings.cmRecordBackAnnotations = true;
            NVP.ModelInferSettings.ActiveType_Classify = ModelTypeClassify;
            var contex = new ML_IMG.ML_Context(CurrentWell, CurrentWVParams, ImgQueue, NVP.ModelInferSettings, NVP.CropSettings, NVP.ClassDef, ImageChk_List, BWModelRun);
            if (contex.TFIM == null)
            {
                txBx_SavedList.Text = "Problem with the model selected. Right click and change it.\r\n" + contex.RequestedModelPath;
                return;
            }

            if (MEA.Button == MouseButtons.Left)
            {
                txBx_SavedList.Text = "ML Run Starting . . ";
                BWModelRun.RunWorkerAsync(contex); //MEA.Button == MouseButtons.Left ? true : false
            }
        }

        private void BWModelRun_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txBx_SavedList.Text = e.UserState.ToString();
        }

        private void BWModelRun_DoWork(object sender, DoWorkEventArgs e)
        {
            var contex = ((ML_IMG.ML_Context)e.Argument);

            contex.ML_AnnotateRafts(true);
        }

        private void BWModelRun_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (NVP.ModelInferSettings.ActiveType_Classify)
            {
                UpdateAll();  //ImageChk_List.Dirty_Saved
                txBx_SavedList.Text = "Done adding annotations (probably hit save).";
            }
            else
            {
                Process.Start(new ProcessStartInfo(Folder) { UseShellExecute = true });
                txBx_SavedList.Text = "Measurements appended and folder opened.";
            }
        }

        public void UpdateModelNVP(bool ModelTypeClassify)
        {
            if (ModelTypeClassify)
            {
                var DR = MultiInputForm.ShowMulti("Settings for Classification image-based model run.", out var Results,
                        "Model Path", NVP.ModelInferSettings.ModelPath_Classification, "Main Threshold", NVP.ModelInferSettings.Classification_KeepThreshold.ToString("0.000"),
                        "Which images? 1=All 2=Only Images with Annotations 3=Without Annotations", NVP.ModelInferSettings.ExistingAnnotations_1All_2Existing_3Not_Classification.ToString(),
                        "Save the images while it goes?", NVP.ModelInferSettings.SaveImagesWhileInferring_Classify.ToString());
                if (DR == DialogResult.Cancel) return;
                try
                {
                    NVP.ModelInferSettings.ModelPath_Classification = Results.Item1;
                    NVP.ModelInferSettings.Classification_KeepThreshold = float.Parse(Results.Item2);
                    NVP.ModelInferSettings.ExistingAnnotations_1All_2Existing_3Not_Classification = byte.Parse(Results.Item3);
                    NVP.ModelInferSettings.SaveImagesWhileInferring_Classify = bool.Parse(Results.Item4);
                }
                catch (Exception err)
                {
                    txBx_SavedList.Text = "Error parsing what you typed, try again.\r\n" + err.ToString();
                    return;
                }
            }
            else
            {
                var DR = MultiInputForm.ShowMulti("Settings for image-based Measurement Model.", out var Results,
                    "Measure Model Path", NVP.ModelInferSettings.ModelPath_Measure,
                    "Which images? 1=All 2=Only Images with Annotations 3=Without Annotations", NVP.ModelInferSettings.ExistingAnnotations_1All_2Existing_3Not_Measure.ToString(),
                    "Limit to particular annotation?", NVP.ModelInferSettings.Measure_OnlyExistingByName,
                    "Save the images while it goes?", NVP.ModelInferSettings.SaveImagesWhileInferring_Measure.ToString());
                if (DR == DialogResult.Cancel) return;
                try
                {
                    NVP.ModelInferSettings.ModelPath_Measure = Results.Item1;
                    NVP.ModelInferSettings.ExistingAnnotations_1All_2Existing_3Not_Measure = byte.Parse(Results.Item2);
                    NVP.ModelInferSettings.Measure_OnlyExistingByName = Results.Item3;
                    NVP.ModelInferSettings.SaveImagesWhileInferring_Measure = bool.Parse(Results.Item4);
                }
                catch (Exception err)
                {
                    txBx_SavedList.Text = "Error parsing what you typed, try again.\r\n" + err.ToString();
                    return;
                }
            }
            NVP.Save();
            txBx_SavedList.Text = "ML Settings adjusted and saved. Click again to run.";
        }



        #endregion

        #region Save Point and Draw Image Check Points -----------------------------------------------------------------------------------------

        private void pictureBox_MouseLeave(object sender, EventArgs e)
        {
            ZoomImage = null;
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            var PB = (PictureBox)sender;
            int PB_IDx = _PictureBoxes.IndexOf(PB);
            var xI = _XDCEs[PB_IDx];
            xI.PlateCoordinates_FromFraction(((double)e.X / PB.Width), ((double)e.Y / PB.Height), out FullX, out FullY);
            txBx_LocationInfo.Text = FullX + ", " + FullY;
            btn_SavePoint_Click(sender, e);
        }

        private PictureBox CurrentPB;
        private PointF ZoomPoint;
        private Image ZoomImage;
        private Bitmap ZoomLatestCrop;

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                CurrentPB = (PictureBox)sender;
                if (e.Location.X > CurrentPB.Width) return; if (e.Location.Y > CurrentPB.Height) return;
                ZoomImage = CurrentPB.Image;
                if (ZoomImage == null) return;
                ZoomPoint = new PointF((float)e.Location.X / CurrentPB.Width, (float)e.Location.Y / CurrentPB.Height);
                int tX = (int)(ZoomPoint.X * ZoomImage.Width);
                int tY = (int)(ZoomPoint.Y * ZoomImage.Height);
                lbl_xyi.Text = tX + "," + tY + "\r\n" + ((Bitmap)(ZoomImage)).GetPixel(tX, tY).R.ToString();
                pictureBox_Zoom.Invalidate();
            }
            catch
            {

            }
        }

        private static Pen ZoomCrossHairs = new Pen(new SolidBrush(Color.FromArgb(128, Color.Yellow))) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };
        private void pictureBox_Zoom_Paint(object sender, PaintEventArgs e)
        {
            if (ZoomImage == null) return;
            Bitmap tZ;
            try
            {
                tZ = (Bitmap)ZoomImage.Clone();
            }
            catch { return; }
            if (tZ == null) return; //The reason we are doing it like this is because there are multi threads and sometimes it tries to draw after the image is disposed throwing an error
            float HalfWidth = pictureBox_Zoom.Width / 2; float HalfHeight = pictureBox_Zoom.Height / 2;
            var p = new PointF(ZoomPoint.X * -tZ.Width, ZoomPoint.Y * -tZ.Height);
            p.X += HalfWidth; p.Y += HalfHeight;

            e.Graphics.DrawImage(tZ, p);
            e.Graphics.DrawLine(ZoomCrossHairs, HalfWidth, 1, HalfWidth, pictureBox_Zoom.Height);
            e.Graphics.DrawLine(ZoomCrossHairs, 1, HalfHeight, pictureBox_Zoom.Width, HalfHeight);

            //Save this for later
            ZoomLatestCrop = new Bitmap(pictureBox_Zoom.Width, pictureBox_Zoom.Height);
            using (var g = Graphics.FromImage(ZoomLatestCrop)) { g.DrawImage(tZ, p); }
        }

        private void btn_SavePoint_Click(object sender, MouseEventArgs e)
        {
            if (txBx_RaftID.Text == "")
            {
                var PB = (PictureBox)sender;
                int PB_IDx = _PictureBoxes.IndexOf(PB);
                var xI = _XDCEs[PB_IDx];
                FOVtoRaftID.ReturnRaft RR = null;

                if (CalRafts.Calibrated) //Ignore unless it is already calibrated
                {
                    RR = CalRafts.FindRaftID_RR(FullX, FullY);
                    if (radioButton_TestCal.Checked)
                    {
                        txBx_SavedList.Text = RR.RaftID + "  " + RR.MinDistToEdge.ToString("0.0");
                        //DrawRaftBounds(PB, xI, RR, false, false); //Before 5/2022 we only showed this one raft, now we show all of them
                    }
                }
                else if (xI.Rafts.Count > 0)
                {   //Special Case with Regions
                    foreach (var Region in xI.Rafts.Values)
                    {
                        if (Region.ContainsPoint(FullX, FullY))
                        {
                            RR = Region.RR; break;
                        }
                    }
                }
                if (radioButton_RecordFocus.Checked)
                {
                    if (RaftMode && !CalRafts.Calibrated) txBx_SavedList.Text = "WARNING . . RAFT NOT CALIBRATED";
                    AddImageCheckPoint(FullX, FullY, PB, xI, RR, e);
                }
            }
            else
            {
                string RaftID = txBx_RaftID.Text.ToUpper().Trim(); txBx_RaftID.Text = "";
                if (KnownRaftsTemp.Count == 1)
                {
                    if (!CalibrationRafts.OnSameVertAxis(KnownRaftsTemp[0].RaftID, RaftID))
                    {
                        MessageBox.Show("Please select a raft above the first on the same vertical raft axis"); return;
                    }
                }
                if (KnownRaftsTemp.Count == 2)
                {
                    if (!CalibrationRafts.OnSameHorAxis(KnownRaftsTemp[1].RaftID, RaftID))
                    {
                        MessageBox.Show("Please select a raft to the right of the previous on the same horizontal raft axis"); return;
                    }
                }
                if (KnownRaftsTemp.Count == 3) // vinay added this 4/28/22
                {
                    if (!CalibrationRafts.OnSameVertAxis(KnownRaftsTemp[2].RaftID, RaftID))
                    {
                        MessageBox.Show("Please select a raft below the previous on the same vertical raft axis"); return;
                    }
                    if (!CalibrationRafts.OnSameHorAxis(KnownRaftsTemp[0].RaftID, RaftID)) // extra logic added on 5/18/22
                    {
                        MessageBox.Show("Please select a raft below the previous on the same vertical raft axis"); return;
                    }
                }

                var KR = new KnownRaft(RaftID, FullX, FullY);
                KnownRaftsTemp.Add(KR); CalRafts.Dirty = true;
                label_Warning.Text = KnownRaftsTemp.Count + " Click here to Reset Calibration";

                string newInfo = RaftID + "," + txBx_LocationInfo.Text;
                txBx_SavedList.Text = txBx_SavedList.Text == "" ? newInfo : txBx_SavedList.Text + "\r\n" + newInfo;

                if (KnownRaftsTemp.Count >= 3) //Ready to calibrate 
                {
                    if (KnownRaftsTemp.Count == 3)
                    {
                        KnownRaftsTemp.Add(CalibrationRafts.ExtrapolateFourthRaft(KnownRaftsTemp[0], KnownRaftsTemp[1], KnownRaftsTemp[2])); KnownRaftIndex = 2;
                    }
                    CalRafts = new CalibrationRafts(KnownRaftsTemp);
                    CalRafts.Dirty = true; //Not sure why this is here
                    CurrentWell.CalibrationRaftSettings = CalRafts;
                    btn_SaveCalibration.Enabled = true;
                    label_Warning.Text = "Calibrated.\r\nClick RESET Start Over.\r\nSAVE when ready.";
                    radioButton_TestCal.Checked = radioButton_TestCal.Enabled = true;
                    UpdateAll();
                }
            }
        }

        internal string AppendImageCheckPoint(string PlateID, string WellLabel, string FOV, string RaftID, string Annotation_Name, string Annotation_Value, ImageCheck_PointList ICPL)
        {
            if (PlateID != IC_Folder.PlateID) return "Wrong PlateID";
            if (Annotation_Value.ToUpper() == "UNTAGGED" || Annotation_Value == "") return "UnTagged";
            var IList = ICPL.FromDictionarySpecific(WellLabel, FOV, RaftID);
            if (IList.Count < 1) return "RaftID Missing";
            if (IList.Count > 1)
            {
                //Look for the one with the highest val . .
            }

            var ICA = new ImageCheck_Annotation();
            ICA.Name = Annotation_Name; ICA.Value = Annotation_Value;
            var ICARef = ImageCheck_Annotation_Schema.ActiveSchema.FromValue(Annotation_Value);
            ICA.Color = ICARef == null ? Color.AliceBlue : ICARef.Color;

            foreach (var Pt in IList)
            {
                Pt.Add(ICA);
                ImageChk_List.Add(Pt);
            }

            return "Appended";
        }

        internal void AddImageCheckPoint(double fullX, double fullY, PictureBox pB, XDCE_Image xI, ReturnRaft rR, MouseEventArgs mouseEA)
        {
            var Pt = new ImageCheck.ImageCheck_Point(); ImageCheck_Annotation ICA;
            Pt.PixelX_Fraction = (float)mouseEA.X / pB.Width; Pt.PixelY_Fraction = (float)mouseEA.Y / pB.Height;
            Pt.Plate_Xum = fullX; Pt.Plate_Yum = fullY;
            Pt.WaveLength = xI.wavelength;
            Pt.Well_Row = xI.Well_Row; Pt.Well_Col = xI.Well_Column.ToString();
            Pt.FOV = xI.FOV;
            if (rR != null) Pt.RaftID = rR.RaftID;

            //Decide what time of Annotation to add (based on whether the user pressed a key while hovering over the image)
            if (Mouse_Modifier_Ignore)
            {
                bool Bad = true;
                switch (mouseEA.Button)
                {
                    case MouseButtons.Left:
                        Bad = false;
                        break;
                    case MouseButtons.Right:
                        Bad = true;
                        break;
                    default:
                        break;
                }
                ICA = Bad ? ICA = ImageCheck_Annotation.Bad : ImageCheck_Annotation.Good;
            }
            else
            {
                ICA = ImageCheck_Annotation_Schema.ActiveSchema.FromModifier(Mouse_Modifier_Key);
            }
            Pt.Add(ICA);
            ImageChk_List.Add(Pt);
            lblImageCheck.Text = ImageChk_List.Count + " Points";
            DrawPoint(pB.CreateGraphics(), ICA, mouseEA.X, mouseEA.Y);
            btn_SaveCheck.Enabled = btnResetImageCheck.Enabled = true;
        }

        private void DrawPoint(Graphics g, bool Bad, float X, float Y)
        {
            g.FillEllipse(Bad ? RedBrush : GreenBrush, X, Y, 9, 9);
        }

        private void DrawPoint(Graphics g, Color Clr, float X, float Y)
        {
            g.FillEllipse(new SolidBrush(Clr), X, Y, 9, 9);
        }

        private void DrawPoint(Graphics g, ImageCheck_Annotation ICA, float X, float Y)
        {
            if (ICA.Color.IsEmpty)
                ICA.Color = ImageCheck_Annotation_Schema.ActiveSchema.FromModifier(ICA.Default_Key).Color;
            g.FillEllipse(new SolidBrush(ICA.Color), X, Y, 9, 9);
        }

        private void ReDraw_CheckPoints(XDCE_Image xI, PictureBox pB, ImageCheck_PointList PList)
        {
            List<ImageCheck_Point> Points = PList.FromDictionary(xI.Well_Row, xI.Well_Column, xI.FOV, 1);
            if (Points.Count < 1) return;
            using Graphics g = pB.CreateGraphics();

            PointF p;
            RectangleF[] rectangles = new RectangleF[Points.Count];
            SolidBrush[] colors = new SolidBrush[Points.Count];
            for (int i = 0; i < Points.Count; i++)
            {
                ImageCheck_Point pt = Points[i];
                p = ReturnRaft.Point_from_ImageCheck(pt, pB.Size);
                colors[i] = new SolidBrush(pt.Annotations[0].Color);
                rectangles[i] = new RectangleF(p.X, p.Y, 9, 9);
            }

            DrawPoints(g, rectangles, colors);

        }
        private void DrawPoints(Graphics g, RectangleF[] Rectangles, SolidBrush[] colors)
        {
            // Iterate over each color and its associated rectangles
            for (int i = 0; i < Rectangles.Length; i++)
            {
                g.FillEllipse(colors[i], Rectangles[i]);
            }
            g.Dispose();

        }

        private void ReDraw_CalPoints(XDCE_Image xI, PictureBox PB, bool GridMode = false)
        {
            //Special Case for non-raft regions
            if (GridMode && !CalRafts.Calibrated && xI.Rafts.Count > 0)
            {
                foreach (var Raft in xI.Rafts)
                {
                    DrawRaftBounds(PB, xI, Raft.Value.RR, false, false, true);
                }
            }

            if (!CalRafts.Calibrated) return;

            //The rafts are saved in the image itself, but are lazily loaded
            if (xI.RaftCalCode != CalRafts.CalCode) xI.Rafts.Clear(); //This is if we changed the calibration, it will have a different fingerprint
            if (xI.Rafts.Count == 0) xI.RefreshRafts(CurrentWell, null);

            var KRs = CalRafts.KnownRaftsContained(xI.Rafts.Keys);

            foreach (var Raft in xI.Rafts)
            {
                bool CalRaft = CalRafts.KnownRaftSet.ContainsKey(Raft.Key);
                if (GridMode)
                {
                    DrawRaftBounds(PB, xI, Raft.Value.RR, false, false, true);
                }
                else
                {
                    if (CalRaft) continue; //skip these till afterwards so they are drawn on top
                    DrawRaftBounds(PB, xI, Raft.Value.RR, false, true); // Raft.Value.RR.IsFiducial);
                }
            }
            if (GridMode) return;
            foreach (var KR in KRs) //Just does the calibration rafts
            {
                DrawRaftBounds(PB, xI, KR.ReturnRaft, true, true);
                if (KR.DragPoint == null) SetupDragPoint(KR);
                //btnDragPoint.Visible = true;
                var tP = Point.Round(ReturnRaft.PointCenterOnImage(PB.Size, xI, KR.ReturnRaft));
                if (tP.IsEmpty) continue;
                if (tP.X < 0 || tP.Y < 0) continue;
                Button DP = (Button)KR.DragPoint;
                DP.Location = new Point(tP.X + PB.Left - (DP.Width / 2), tP.Y + (PB.Top - DP.Height) / 2);
                DP.Visible = true;
            }
        }


        private List<Button> _AllDragPoints;

        private void HideAllDragPoints()
        {
            if (_AllDragPoints == null) _AllDragPoints = new List<Button>();
            foreach (var DP in _AllDragPoints) DP.Visible = false;
        }

        private void SetupDragPoint(KnownRaft kR)
        {
            Button btn = new Button();
            btn.Tag = kR;
            btn.Name = "";
            if (_AllDragPoints == null) _AllDragPoints = new List<Button>();
            _AllDragPoints.Add(btn); //Need a better way to delete these or re-use them
            kR.DragPoint = btn;
            btn.Size = new Size(15, 15);
            btn.TabStop = false;

            this.Controls.Add(btn);
            btn.FlatAppearance.BorderSize = 1;
            btn.FlatAppearance.BorderColor = Color.Yellow;

            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = Color.FromArgb(255, 0, 0, 0);

            btn.MouseDown += btnDragPoint_MouseDown;
            btn.MouseUp += btnDragPoint_MouseUp;
            btn.MouseMove += btnDragPoint_MouseMove;

            btn.Visible = true;
            btn.BringToFront();
        }

        #endregion

        #region Utilities -----------------------------------------------------------------------------------------------------------------



        /// <summary>
        /// True means Continue, False means cancel
        /// </summary>
        /// <param name="Force"></param>
        /// <returns></returns>
        public bool SaveCalibration(bool Force)
        {
            bool Ask = false;
            bool Save = false;
            switch (Force)
            {
                case true:
                    Ask = false; Save = true;
                    break;
                case false when CalRafts.Dirty:
                    Ask = true; Save = false;
                    break;
                case false when !CalRafts.Dirty:
                    Ask = false; Save = false;
                    break;
                default:
                    Ask = true; Save = false;
                    break;
            }
            if (Ask)
            {
                DialogResult DR = MessageBox.Show("Do you want to save (or overwrite) the Calibration?", "Save Calibration?", MessageBoxButtons.YesNoCancel);
                switch (DR)
                {
                    case DialogResult.Cancel:
                        return false;
                    case DialogResult.Yes:
                        Save = true; break;
                    case DialogResult.No:
                        Save = false; break;
                    default:
                        return false;
                }
            }
            if (Save)
            {
                CalRafts.Well = PreviousWell;
                CalRafts.SaveDefault(Folder);
                label_Warning.Text = "Saved.";
                CurrentWell.CalibrationRaftSettings = CalRafts;
                //FOVtoRaftID.CalibrationRafts CR = FOVtoRaftID.CalibrationRafts.Load(p);
            }
            return true;
        }

        #endregion

        #region Annotation Schema ---------------------------------------------------------------------------------------------

        private void DoubleClick_RaftIDBox(object sender, EventArgs e)
        {
            if (CalRafts == null) return;
            string rID = txBx_RaftID.Text.Trim().ToUpper();
            if (rID == "") { txBx_SavedList.Text = "Please enter a raft ID"; return; }

            CurrentWell.Refresh_Wells_Rafts(11); //Slow - but faster with a smaller number here . . ~8 is the smallest

            if (CurrentWell.Rafts.ContainsKey(rID))
            {
                var xIG = CurrentWell.Rafts[rID].Images[0];
                Col = CurrentWell.X_to_Index[xIG.PlateX_um];
                Row = CurrentWell.Y_to_Index[xIG.PlateY_um];
                UpdateAll();
            }
            else
            {
                txBx_SavedList.Text = rID + " not found here. Example: " + CurrentWell.Rafts.First().Key + "," + CurrentWell.Rafts.Last().Key;
            }
        }

        private bool MouseIsOverControl(Panel btn) => btn.ClientRectangle.Contains(btn.PointToClient(Cursor.Position));

        private void FormRaftCal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.Handled = e.SuppressKeyPress = true;
                Schema_Modifier(-1);
                return;
            }
            if (e.KeyCode == Keys.Tab)
            {
                //Just to deactivate
                e.Handled = e.SuppressKeyPress = true;
                return;
            }
            if (e.KeyValue == 192)
            {
                //Tilda key - goes through a snake pattern
                e.Handled = e.SuppressKeyPress = true;
                MoveFieldSnakePattern();
                return;
            }
            if (MouseIsOverControl(panelControls))
            {
                e.Handled = e.SuppressKeyPress = false;
                Mouse_Modifier_Ignore = true;
            }
            else
            {
                if (e.KeyCode == Keys.C && e.Control)
                {
                    e.Handled = e.SuppressKeyPress = true;
                    var bM = ZoomLatestCrop;
                    Clipboard.SetImage(bM);
                    txBx_SavedList.Text = "Copied crop to Clipboard . . ";
                    Application.DoEvents();
                    return;
                }
                if (e.KeyCode == Keys.R && e.Control)
                {
                    //Doesn't work for some reason
                    e.Handled = e.SuppressKeyPress = true;
                    txBx_SavedList.Text = "";
                    Application.DoEvents();
                    return;
                }
                if (e.KeyCode == Keys.X || e.KeyCode == Keys.Z)
                {
                    //Since it is the form that captures this and not the picture box, we need to get the locataion
                    //This didn't work great . . 
                    //var P = Cursor.Position;
                    //var Ch = this.GetChildAtPoint(P);
                    //if (Ch == null) foreach (PictureBox pictureBox in _PictureBoxes) if (pictureBox.ClientRectangle.Contains(pictureBox.PointToClient(P))) { Ch = pictureBox; break; }
                    //if (Ch is PictureBox) { }
                    //Special Case, add points over the whole picture box that you were hovering over, or remove all
                    e.Handled = e.SuppressKeyPress = true;
                    AnnotateFullField(CurrentPB, e.KeyCode == Keys.X);
                    return;
                }
                //Could be over a picture box, so store it
                e.Handled = e.SuppressKeyPress = true;
                Schema_Modifier(e.KeyValue);
            }
        }

        private bool Mouse_Modifier_Ignore = true;
        private char Mouse_Modifier_Key;

        /// <summary>
        /// Tells the system that there is a modifier being pressed while hovering over an image, this allows for fancy
        /// </summary>
        /// <param name="keyValue"></param>
        private void Schema_Modifier(int keyValue)
        {
            if (keyValue > -1 && radioButton_RecordFocus.Checked)
            {
                Mouse_Modifier_Ignore = false;
                Mouse_Modifier_Key = (char)keyValue;
                txBx_SavedList.Text = ImageCheck_Annotation_Schema.ActiveSchema.FromModifier(Mouse_Modifier_Key).Report();
            }
            else
            {
                Mouse_Modifier_Ignore = true;
                if (radioButton_RecordFocus.Checked) txBx_SavedList.Text = "";
            }
        }

        private void AnnotateFullField(PictureBox PB, bool AnnotateAllBad)
        {
            double fraction = (double)1 / 8;
            int PB_IDx = _PictureBoxes.IndexOf(PB);
            XDCE_Image xI = _XDCEs[PB_IDx]; //double FullX, FullY; //No, it has to be the global ones
            if (AnnotateAllBad)
            {
                Mouse_Modifier_Ignore = true; double tx, ty;
                for (double x = 0; x < 1; x += fraction)
                {
                    for (double y = 0; y < 1; y += fraction)
                    {
                        tx = x + fraction / 2; ty = y + fraction / 2;
                        xI.PlateCoordinates_FromFraction(tx, ty, out FullX, out FullY);
                        var ME = new MouseEventArgs(MouseButtons.Right, 1, (int)(PB.Width * tx), (int)(PB.Height * ty), 0);
                        btn_SavePoint_Click(PB, ME);
                    }
                }
            }
            else
            {
                List<ImageCheck_Point> Points = ImageChk_List.FromDictionary(xI.Well_Row, xI.Well_Column, xI.FOV, 1);
                ImageChk_List.RemovePoints(Points);
                UpdateAll();
            }
        }

        #endregion

        #region Export Images List or Markings (now mostly in its own form) ---------------------------------------------------------------------

        private void btn_ExportFromList_Click(object sender, EventArgs e)
        {
            ExportCalForm.Show();
            return;
        }

        private void btn_ExportRaftImages_MouseUp(object sender, MouseEventArgs e)
        {
            //To delete
        }

        private void btn_ExportAllRaftImages_MouseUp(object sender, MouseEventArgs e)
        {
            //To delete
        }

        private void btn_ExportCurrentImageView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var RIES = RaftImage_Export_Settings.Load();

                string Exported = ExportStitchCurrentImageSet(RIES.ExportFolderBase, IC_Folder.PlateID, Path.GetFileNameWithoutExtension(txBx_ImageName.Text), RIES.ImageExtension);
                txBx_SavedList.Text = Exported;
            }
            if (e.Button == MouseButtons.Right)
            {
                var tBMP = CurrentImageSetStitched();
                Clipboard.SetDataObject(tBMP);
                txBx_SavedList.Text = "Copied to Clipboard";
            }
            if (e.Button == MouseButtons.Middle)
            {
                string Folder = Path.Combine(NVP.HCS_Image_DestinationFolder, "FIVImgExport");
                Directory.CreateDirectory(Folder);
                for (int i = 0; i < _XDCEs.Count; i++)
                {
                    var xI = _XDCEs[i];
                    if (xI == null) continue;
                    var PB = _PictureBoxes[i];
                    PB.Image.Save(Path.Combine(Folder, Path.GetFileNameWithoutExtension(xI.FileName) + ".png"));
                }
                txBx_SavedList.Text = Folder;
            }
        }

        public Bitmap CurrentImageSetStitched()
        {
            Bitmap UL = pictureBox1.Image != null ? new Bitmap(pictureBox1.Image) : null;
            Bitmap UR = pictureBox2.Image != null ? new Bitmap(pictureBox2.Image) : null;
            Bitmap LL = pictureBox3.Image != null ? new Bitmap(pictureBox3.Image) : null;
            Bitmap LR = pictureBox4.Image != null ? new Bitmap(pictureBox4.Image) : null;

            int nonNullCount = 0; Bitmap NN = null;
            if (UL != null) { nonNullCount++; NN = UL; }
            if (UR != null) { nonNullCount++; NN = UR; }
            if (LL != null) { nonNullCount++; NN = LL; }
            if (LR != null) { nonNullCount++; NN = LR; }
            if (nonNullCount == 1) return NN;

            var destBitmap = new System.Drawing.Bitmap(UL.Width * 2, UL.Height * 2);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                Rectangle destRegion;
                Rectangle ImageRect_SRC = new Rectangle(0, 0, UL.Width, UL.Height);

                if (UL != null) { destRegion = new Rectangle(new Point(0, 0), UL.Size); g.DrawImage(UL, destRegion, ImageRect_SRC, GraphicsUnit.Pixel); }
                if (UR != null) { destRegion = new Rectangle(new Point(UL.Width, 0), UL.Size); g.DrawImage(UR, destRegion, ImageRect_SRC, GraphicsUnit.Pixel); }
                if (LL != null) { destRegion = new Rectangle(new Point(0, UL.Height), UL.Size); g.DrawImage(LL, destRegion, ImageRect_SRC, GraphicsUnit.Pixel); }
                if (LR != null) { destRegion = new Rectangle(new Point(UL.Width, UL.Height), UL.Size); g.DrawImage(LR, destRegion, ImageRect_SRC, GraphicsUnit.Pixel); }
            }
            return destBitmap;
        }

        public string ExportStitchCurrentImageSet(string ExportFolder, string PlateID, string FileName, string Extension, float resizeMultiplier = 1, int CropToMax = -1)
        {
            //resizeMultiplier = 0.033f; CropToMax = 100;
            Bitmap destBitmap = CurrentImageSetStitched();
            int oWidth = destBitmap.Width; int oHeight = destBitmap.Height;

            Bitmap finalBitmap = new Bitmap(destBitmap, (int)(oWidth * resizeMultiplier), (int)(oHeight * resizeMultiplier)); //resize
            oWidth = finalBitmap.Width; oHeight = finalBitmap.Height;

            if (CropToMax < 2) CropToMax = Math.Min(oWidth, oHeight); else CropToMax = Math.Min(CropToMax, Math.Min(oWidth, oHeight)); //Account for default and don't allow this to be greater than the resized size
            int Left = (oWidth - CropToMax) / 2; int Top = (oHeight - CropToMax) / 2; //first corner
            Rectangle r = new Rectangle(Left, Top, CropToMax, CropToMax); //crop rectangle
            finalBitmap = finalBitmap.Clone(r, PixelFormat.Format24bppRgb); //crop

            string ExportBase = Path.Combine(ExportFolder, PlateID);
            DirectoryInfo DI = new DirectoryInfo(ExportBase); if (!DI.Exists) DI.Create();
            string ExportedName = Path.Combine(ExportBase, FileName + "." + Extension);
            finalBitmap.Save(ExportedName);
            return ExportedName;
        }

        public static byte[] BitmapToByteArray(System.Drawing.Bitmap bitmap)
        {

            BitmapData bmpdata = null;

            try
            {
                bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
                int numbytes = bmpdata.Stride * bitmap.Height;
                byte[] bytedata = new byte[numbytes];
                IntPtr ptr = bmpdata.Scan0;

                Marshal.Copy(ptr, bytedata, 0, numbytes);

                if (bitmap.PixelFormat == PixelFormat.Format32bppArgb)
                {
                    byte[] b2 = new byte[bytedata.Length / 4];
                    for (int i = 0; i < bytedata.Length; i += 4)
                    {
                        b2[i / 4] = bytedata[i];
                    }
                    return b2;
                }

                return bytedata;
            }
            finally
            {
                if (bmpdata != null)
                    bitmap.UnlockBits(bmpdata);
            }

        }

        #endregion

        #region Image Analysis and Segmentation - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void btnRunSegmentation_MouseUp(object sender, MouseEventArgs e)
        {
            if (bgWorker_Analysis1.IsBusy) //Cancel Segmentation
            {
                btnRunSegmentation.Text = "Cancelling . . ";
                bgWorker_Analysis1.CancelAsync();
            }
            else
            {
                if (e.Button == MouseButtons.Left) //Run Segmentation
                {
                    //Check if Right or Left Click
                    btnRunSegmentation.Text = "Cancel Seg";
                    CancelBackground(); //May not be necessary
                    btn_SaveWavelengthInfo_Click(sender, e); //Make sure to save the settings for InCellWVNotes
                    if (CurrentWVParams.Tracings.Count < 1)
                    {
                        txBx_SavedList.Text = "Check at least one 'Obj' and set the tresholds.";
                        return;
                    }
                    txBx_SavedList.Text = "Starting segmentation analysis . . \r\n";
                    bgWorker_Analysis1.RunWorkerAsync((NVP.CropSettings, ImgQueue));
                }
                if (e.Button == MouseButtons.Right) //Check Settings
                {
                    var (Res, Arr) = MultiInputForm.ShowMultiMore("Segmentation Settings",
                        "Well Dictionary", NVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon,
                        "Crop Width", NVP.CropSettings.CropSize.Width.ToString(),
                        "Extension", NVP.CropSettings.SaveTypeExtension_NoDot,
                        "Min Area", NVP.CropSettings.MinAreaPixels.ToString(),
                        "Max Area", NVP.CropSettings.MaxAreaPixels.ToString(),
                        "Max Overlap %", NVP.CropSettings.MaxOverlapAllowed.ToString(),
                        "DI_Fragmented_CompactnessThresh", NVP.CropSettings.DI_Fragmented_CompactnessThresh.ToString(),
                        "DI_Max_Area_Bounding", NVP.CropSettings.DI_Max_Area_Bounding.ToString(),
                        "DI_MinPerimeterDensity", NVP.CropSettings.DI_MinPerimeterDensity.ToString(),
                        "DI_Slice_ModulusFactor_PreBin", NVP.CropSettings.DI_Slice_ModulusFactor_PreBin.ToString(),
                        "Only Region Assignment (type y)", NVP.CropSettings.OnlyForRegionAssignment ? "y" : ""
                        );
                    if (Res == DialogResult.OK)
                    {
                        //Save these new settings
                        NVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = Arr[0];
                        NVP.CropSettings.WellDictionary_Refresh_FromString(); //Updates the dictionary
                        int Sz = int.Parse(Arr[1]);
                        NVP.CropSettings.CropSize = new Size(Sz, Sz);
                        NVP.CropSettings.SaveTypeExtension_NoDot = Arr[2];
                        NVP.CropSettings.MinAreaPixels = int.Parse(Arr[3]);
                        NVP.CropSettings.MaxAreaPixels = int.Parse(Arr[4]);
                        NVP.CropSettings.MaxOverlapAllowed = float.Parse(Arr[5]);
                        NVP.CropSettings.DI_Fragmented_CompactnessThresh = double.Parse(Arr[6]);
                        NVP.CropSettings.DI_Max_Area_Bounding = int.Parse(Arr[7]);
                        NVP.CropSettings.DI_MinPerimeterDensity = double.Parse(Arr[8]);
                        NVP.CropSettings.DI_Slice_ModulusFactor_PreBin = int.Parse(Arr[9]);
                        Arr[10] += ' '; //To make sure following doesn't fail
                        NVP.CropSettings.OnlyForRegionAssignment = Arr[10].ToUpper()[0] == 'Y';
                        NVP.Save();
                    }
                }
            }
        }

        protected void bgWorker_Analysis1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //int MaxToProcess = 16; //Only for non-parallel
            bool UseRafts = IC_Folder.XDCE.Wells.First().Value.HasRaftCalibration; var Start = DateTime.Now;
            var ImageParts = new System.Collections.Concurrent.ConcurrentDictionary<string, XDCE_ImageGroup>();
            if (UseRafts) ImageParts = IC_Folder.Rafts; //IEnumerable<string> WellsFields = IC_Folder.XDCE.WellFOV_Set;
            if (ImageParts == null || ImageParts.Count == 0) ImageParts = IC_Folder.XDCE.WellFOV_Dict;

            (var CropSettings, var imgQueuePass) = ((FIVE_IMG.Seg.CropImageSettings, ImageQueue))e.Argument;
            CropSettings.WellDictionary_Refresh_FromString(); //Refreshes
            //CropSettings.ParallelizeSegmentation = true;
            CropSettings.SaveFolder = @"E:\Temp\";

            CropSettings.SaveCroppedImages = false;

            //CropSettings.StoreResults = false; //Does this take less memory? - Perhaps the xIs are what is taking memory somehow?
            //Debugger.Break(); //Always break if running this from Visual Studio so we can check it out
            if (false)
            {   //FIV574 
                //CropSettings.SaveFolder = @"e:\temp\RaftExport\FIV574B\";
                //CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = "B - 3,a1A04;C - 3,a1F08;D - 3,a2A10;E - 3,a2B06;F - 3,a2D03;G - 3,a4F01;B - 4,a1A04;C - 4,a1F08;D - 4,a2A10;E - 4,a2B06;F - 4,a2D03;G - 4,a4F01;B - 5,a5G06;C - 5,a6E04;D - 5,a8A09;E - 5,a4G08;F - 5,a5A05;G - 5,a5E02;B - 6,a5G06;C - 6,a6E04;D - 6,a8A09;E - 6,a4G08;F - 6,a5A05;G - 6,a5E02;B - 7,a2B06;C - 7,a2D03;D - 7,a4F01;E - 7,a1A04;F - 7,a1F08;G - 7,a2A10;B - 8,a2B06;C - 8,a2D03;D - 8,a4F01;E - 8,a1A04;F - 8,a1F08;G - 8,a2A10;B - 9,a4G08;C - 9,a5A05;D - 9,a5E02;E - 9,a5G06;F - 9,a6E04;G - 9,a8A09;B - 10,a4G08;C - 10,a5A05;D - 10,a5E02;E - 10,a5G06;F - 10,a6E04;G - 10,a8A09";
                //CropSettings.RandomizeImageIntensity_Min = 0.2f;
                //SARM version>
                //CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = "C - 3,SARM 226-232;C - 4,SARM KO;C - 5,SARM KO;C - 7,SARM WT;C - 8,SARM KO;C - 9,SARM 226-232;D - 3,SARM WT;D - 4,SARM WT;D - 5,SARM WT;D - 7,SARM WT;D - 8,SARM KO;D - 9,SARM 226-232;E - 3,SARM KO;E - 4,SARM 226-232;E - 7,SARM WT";

                //FIV617
                CropSettings.CropSize = new Size(128, 128);
                CropSettings.SaveFolder = @"S:\Cells\FIV617\";
                CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = "E - 13,UNK"; //Not relevant for this one, but we should import the mRaft Class information
                CropSettings.SaveTypeExtension_NoDot = "bmp";
                CropSettings.MinAreaPixels = 250; //Since the threshold image is binned, these are slightly different, multiply by 2 then 0.325 to turn it into the sizes we are used to in Spotfire (I think)
                CropSettings.MaxAreaPixels = 820;
                CropSettings.RandomizeImageIntensity_Min = 1; //1 turns off 0 means it can be extreme differences
            }
            var Res = FIVE_IMG.Seg.Segmentation.RunSegmentation(UseRafts, Start, ImageParts, CropSettings, IC_Folder.InCell_Wavelength_Notes, imgQueuePass, bgWorker_Analysis1); //Actually Segmentation Part of it

            bgWorker_Analysis1.ReportProgress(0, Res.ProcessedCount.ToString() + " files in " + (DateTime.Now - Start).TotalMinutes.ToString("0.0") + " minutes.");

            if (CropSettings.OnlyForRegionAssignment) return;

            string ExportFolder = Path.Combine(IC_Folder.FullPath, "Seg");
            Directory.CreateDirectory(ExportFolder);
            string ExportFile; int counter = 1;
            do
            {
                ExportFile = Path.Combine(ExportFolder, "Segmentation_" + counter++.ToString("000") + ".txt");
            } while (File.Exists(ExportFile));

            Res.ExportObjects(ExportFile);
            Res.ExportAggregations(ExportFile.Replace(".txt", "_Agg.txt"));
            Res.ExportDIs(ExportFile.Replace("_Agg.txt", "_DI.txt"));
        }

        private void bgWorker_Analysis1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            txBx_SavedList.Text = (string)e.UserState;
        }

        private void bgWorker_Analysis1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            btnRunSegmentation.Text = "Segmentation";
        }


        #endregion

        #region Leica Mapping - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private FIVToolsMode _ParentMode;
        public FIVToolsMode ParentMode
        {
            get
            {
                return _ParentMode;
            }
            set
            {
                _ParentMode = value;
                switch (_ParentMode)
                {
                    case FIVToolsMode.Normal:
                        btn_Finish.Visible = false;
                        break;
                    case FIVToolsMode.PlateID:
                        break;
                    case FIVToolsMode.PlateID_WellList:
                        btn_Finish.Visible = true; chkBx_Onwv1.Checked = true;
                        btn_ExportFromList.Enabled = btn_SaveCalibration.Enabled = btn_SaveCheck.Enabled = btnUndoImageCheck.Enabled = btnResetImageCheck.Enabled = false;
                        chkBx_Onwv2.Checked = chkBx_Onwv3.Checked = chkBx_Onwv4.Checked = chkBx_Onwv5.Checked = txBx_Wavelength2.Enabled = txBx_Wavelength3.Enabled = txBx_Wavelength4.Enabled = txBx_Wavelength5.Enabled = false;
                        chkBx_SquareMode.Checked = false;
                        txBx_SavedList.Text = "Setup the first wavelength to reflect the single channel you are interested in calibrating. Make sure to adjust the intensity to get a lot of bright pixels. The click finish.";
                        break;
                    case FIVToolsMode.Silent:
                        break;
                    default:
                        break;
                }
            }
        }

        public string Return_WellList;
        public string Return_StartField;
        public string Return_CheckWellFields;

        private void btn_Finish_Click(object sender, EventArgs e)
        {
            //txBx_SavedList.Text = IC_Folder.XDCE.MaskFields.Count.ToString(); Spin(10);

            bgWorker_Analysis1.CancelAsync(); bgWorker_ImageLoad.CancelAsync(); Spin(10);
            btn_SaveWavelengthInfo_Click(sender, e); //Make sure to save the settings for InCellWVNotes
            //This button isn't usually visible. Depending on mode we will decide what to do next
            if (ParentMode == FIVToolsMode.PlateID_WellList)
            {
                if (!IC_Folder.Type_NoXDCE)
                {
                    //This is registration pipeline specific
                    string WellList = string.Join(",", IC_Folder.XDCE.MaskWells);
                    Tuple<string, string, string, string, string> Results;
                    var DR = MultiInputForm.ShowMulti("Enter the list of wells you would like to use. Only wells with masks will be considered. Below is all the wells > ",
                                                        out Results, "Well list", WellList,
                                                        "Enter starting Field (usually 1)", "1",
                                                        "Fields to check at the start of the well", "9");
                    if (DR != DialogResult.OK) return;
                    Return_WellList = Results.Item1;
                    Return_StartField = Results.Item2;
                    Return_CheckWellFields = Results.Item3;
                }
                else
                {

                    //Anything On-The-Fly specific should go here
                }
                Close();
                DialogResult = DialogResult.Abort;
            }
        }

        private void btn_SaveWavelengthInfo_Click(object sender, EventArgs e)
        {
            InCellLibrary.INCELL_WV_Notes N = IC_Folder.InCell_Wavelength_Notes;
            if (N.Count > 0) { N.List[0].Abbrev = txBx_Wv_Abbrev1.Text.Trim().ToUpper(); N.List[0].Staining = txBx_Wv_Name1.Text.Trim(); N.List[0].Threshold = txBx_Wv_Thresh1.Text.Trim(); }
            if (N.Count > 1) { N.List[1].Abbrev = txBx_Wv_Abbrev2.Text.Trim().ToUpper(); N.List[1].Staining = txBx_Wv_Name2.Text.Trim(); N.List[1].Threshold = txBx_Wv_Thresh2.Text.Trim(); }
            if (N.Count > 2) { N.List[2].Abbrev = txBx_Wv_Abbrev3.Text.Trim().ToUpper(); N.List[2].Staining = txBx_Wv_Name3.Text.Trim(); N.List[2].Threshold = txBx_Wv_Thresh3.Text.Trim(); }
            if (N.Count > 3) { N.List[3].Abbrev = txBx_Wv_Abbrev4.Text.Trim().ToUpper(); N.List[3].Staining = txBx_Wv_Name4.Text.Trim(); N.List[3].Threshold = txBx_Wv_Thresh4.Text.Trim(); }
            if (N.Count > 4) { N.List[4].Abbrev = txBx_Wv_Abbrev5.Text.Trim().ToUpper(); N.List[4].Staining = txBx_Wv_Name5.Text.Trim(); N.List[4].Threshold = txBx_Wv_Thresh5.Text.Trim(); }
            N.DisplayParams = CurrentWVParams;
            N.Save();
            //Push this out to the IC_Folder so it can be remembered
        }

        //txBx_Wv_Thresh2

        private void txBx_Wv_Thresh_DoubleClick(object sender, EventArgs e)
        {
            int bin = 4; double threshTemp;

            foreach (var thBox in txBx_Wv_Thresh) { if (!double.TryParse(thBox.Text, out threshTemp)) { txBx_SavedList.Text = "Enter a valid threshold before proceeding"; } }

            // Get the necessary parameters
            var pre = CurrentWVParams.List.Where(x => x.ObjectAnalysis);
            var obj = pre.Count() > 0 ? pre.First() : CurrentWVParams[0];
            var xi = CurrentWell.GetField(_XDCEs[0].FOV, obj.wvIdx);
            TextBox tx = (TextBox)sender;

            // Call the AnalyzeImage method
            var OnlyRegionPre = NVP.CropSettings.OnlyForRegionAssignment; NVP.CropSettings.OnlyForRegionAssignment = false;
            var result = BasicSegmentatioResults.BasicSegmentation(NVP.CropSettings, CurrentWVParams, xi, ImgQueue, bin, true, true, true);
            NVP.CropSettings.OnlyForRegionAssignment = OnlyRegionPre;
            if (result == null) { txBx_SavedList.Text = "Error, Probably no objects found"; return; }

            // Set PictureBox images
            pictureBox1.Image = BasicSegmentatioResults.AddLabel(result.CombinedBMAP, "1 Original");
            pictureBox3.Image = BasicSegmentatioResults.AddLabel(result.PostThreshBitmap, "2 Thresholded", bin);
            pictureBox2.Image = BasicSegmentatioResults.AddLabel(result.FilteredObjectsBitmap, "3 Filtered Objects = " + result.FilterObjs.Count().ToString());
            pictureBox4.Image = BasicSegmentatioResults.AddLabel(result.DeOverlappedObjectsBitmap, "4 DeOverlapped Regions = " + result.DeOverlap.Count().ToString());

            if (false) //Make the DeOverlaps into Rafts
            {
                var tRafts = _XDCEs[0].Rafts;
                foreach (var Region in result.DeOverlap) Region.AddasRaftInfo(tRafts, _XDCEs[0], NVP.CropSettings.CropSize, bin);
            }

            // Report out the findings
            string report = "No Objects, threshold is too high or there are no white/red pixels.";
            if (result.DeOverlap.Count() > 0)
            {
                report = $"{result.Objs.Count},{result.FilterObjs.Count()},{result.DeOverlap.Count()} # total, filt, deovrlp. " +
                    $"Area Min={result.DeOverlap.Min(x => x.m_Area_Pixels):0}, Avg={result.DeOverlap.Average(x => x.m_Area_Pixels):0}, Max={result.DeOverlap.Max(x => x.m_Area_Pixels):0}\r\n" +
                    $"Thresh msec={result.ThreshTime:0}, Obj Detect msec={result.ObjTime:0}, Regions msec={result.RegionsTime:0}";
            }
            txBx_SavedList.Text = report;
            Application.DoEvents();
        }

        private void label_DegenIndex_Click(object sender, EventArgs e)
        {
            int bin = 2;

            // Get the necessary parameters
            var pre = CurrentWVParams.List.Where(x => x.DI_Analysis);
            var di = pre.Count() > 0 ? pre.First() : CurrentWVParams[0];
            var xI = CurrentWell.GetField(_XDCEs[0].FOV, di.wvIdx);

            // Call the AnalyzeImage method
            var (CombinedBMAP, avail) = ImgQueue.CombinedBMAP(xI.Parent, xI, CurrentWVParams);
            var bmpPreThresh = FIVE_IMG.Utilities.DrawingUtils.GetAdjustedImageSt(xI.FullPath, xI, di.Brightness);
            var result = BasicSegmentatioResults.DegenerationIndex(NVP.CropSettings, di.Threshold, xI, bmpPreThresh, CombinedBMAP, bin, true, true, true);

            // Set PictureBox images
            pictureBox1.Image = BasicSegmentatioResults.AddLabel(result.CombinedBMAP, "1 Original");
            pictureBox3.Image = BasicSegmentatioResults.AddLabel(result.PostThreshBitmap, "2 Thresholded", bin);
            pictureBox2.Image = BasicSegmentatioResults.AddLabel(result.FilteredObjectsOutline, "3 Outlines = " + result.FilterObjs.Count().ToString());
            pictureBox4.Image = BasicSegmentatioResults.AddLabel(result.FragmentPreviewBitmap, "4 Degen = " + result.DI.ToString("F2"));

            // Report out the findings
            string report = "No Objects, threshold is too high or there are no white/red pixels.";
            if (result.FilterObjs.Count() > 0)
            {
                report = $"{result.Objs.Count},{result.FilterObjs.Count()},{-1} # total, filt, deovrlp. " +
                    $"Area Min={result.FilterObjs.Min(x => x.m_Area_Pixels):0}, Avg={result.FilterObjs.Average(x => x.m_Area_Pixels):0}, Max={result.FilterObjs.Max(x => x.m_Area_Pixels):0}\r\n" +
                    $"Thresh msec={result.ThreshTime:0}, Obj Detect msec={result.ObjTime:0}, Regions msec={result.RegionsTime:0}";
            }
            txBx_SavedList.Text = report;
            Application.DoEvents();
        }

        private void txBx_SavedList_DoubleClick(object sender, EventArgs e)
        {
            txBx_SavedList.Text = INCELL_Folder.ErrorLog;
        }

        private void txBx_FolderPath_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = false;

            var OFD = new OpenFileDialog();
            OFD.Filter = "All files (*.*)|*.*";
            OFD.Title = "Please select any file to indicate which Folder we should Open.";
            OFD.RestoreDirectory = true;
            DialogResult DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                //For some reason this doesn't really work . . restarting the whole thing seems to be better
                Folder = Path.GetDirectoryName(OFD.FileName);
            }

            this.Visible = true;
        }

        private void btnDragPoint_MouseDown(object sender, MouseEventArgs e)
        {
            var btn = (Button)sender;
            if (Control.ModifierKeys == Keys.Shift) btn.Name = "DRAG"; //Only do this if shift is pressed
            else txBx_SavedList.Text = "Hold Shift to Adjust Calibration . . ";
        }

        public static float CalDrag_SlowFactor = 1; //Doesn't work the way I wanted . . 

        private void btnDragPoint_MouseMove(object sender, MouseEventArgs e)
        {
            var btnDragPoint = (Button)sender;
            if ((string)btnDragPoint.Name == "DRAG")
            {
                //Drag from the middle
                btnDragPoint.Location =
                   new Point((int)(btnDragPoint.Location.X - (btnDragPoint.Size.Width / 2) + (e.X / CalDrag_SlowFactor)), (int)(btnDragPoint.Location.Y - (btnDragPoint.Size.Height / 2) + (e.Y / CalDrag_SlowFactor)));
            }
        }

        private void btnDragPoint_MouseUp(object sender, MouseEventArgs e)
        {
            var btnDragPoint = (Button)sender;
            if (btnDragPoint.Name != "DRAG") return;
            btnDragPoint.Name = "";
            Point P = new Point(btnDragPoint.Location.X + btnDragPoint.Size.Width / 2, btnDragPoint.Location.Y + btnDragPoint.Size.Height / 2);

            if (Control.ModifierKeys == Keys.Shift)
            {
                txBx_SavedList.Text = "Confirmed move";
                var KR = (KnownRaft)btnDragPoint.Tag;

                int PB_IDx; PictureBox PB = null;
                for (PB_IDx = 0; PB_IDx < _PictureBoxes.Count; PB_IDx++)
                    if (_PictureBoxes[PB_IDx].Bounds.Contains(P)) { PB = _PictureBoxes[PB_IDx]; break; }

                if (PB != null)
                {  /*= _PictureBoxes.IndexOf(PB);*/
                    var xI = _XDCEs[PB_IDx];
                    xI.PlateCoordinates_FromFraction(((double)(P.X - PB.Left) / PB.Width), ((double)(P.Y - PB.Top) / PB.Height), out FullX, out FullY);

                    KR.X = FullX; KR.Y = FullY;
                    KR.ReturnRaft = null;

                    CalibrationRafts.RecordDebug = true;
                    var RR = KR.ReturnRaft;
                    if (RR.RaftID != KR.RaftID)
                    {
                        Debug.Print("Error");
                    }
                    CalibrationRafts.RecordDebug = false;

                    ////This is what we used to do before 9/2022 >
                    //CalRafts = new CalibrationRafts(CalRafts.KnownRafts); CurrentWell.CalibrationRaftSettings = CalRafts;
                    //--New version 9/2022
                    CurrentWell.CalibrationRaftSettings.CalCodeShift(); //This should trigger a re-drawing of relevant rafts
                }
            }
            else
                txBx_SavedList.Text = "Cancelled move";
            UpdateAll();
        }

        private void radioButton_TestCal_CheckedChanged(object sender, EventArgs e)
        {
            //btnDragPoint.Visible = false;
            UpdateAll();
        }

        private void chkBx_ShowCalGrid_CheckedChanged(object sender, EventArgs e)
        {
            UpdateAll();
        }

        private void chkBx_Onwv5_CheckedChanged(object sender, EventArgs e)
        {
            //UpdateAll();
        }

        private void chkBx_Onwv4_CheckedChanged(object sender, EventArgs e)
        {
            //UpdateAll();
        }

        private void chkBx_Onwv3_CheckedChanged(object sender, EventArgs e)
        {
            //UpdateAll();
        }

        private void chkBx_Onwv2_CheckedChanged(object sender, EventArgs e)
        {
            //UpdateAll();
        }

        private void chkBx_Onwv1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txBx_Color_TextChanged(object sender, EventArgs e)
        {
            var txBx = (TextBox)sender;
            var C = wvDisplayParam.FromStringToColor(txBx.Text.ToUpper().Trim());
            txBx.BackColor = C;
            txBx.ForeColor = Color.FromArgb(255 - C.R, 255 - C.G, 255 - C.B);
        }

        private int LabelColorToggle = 0;

        private void label_Color_Click(object sender, EventArgs e)
        {
            LabelColorToggle++; if (LabelColorToggle > 3) LabelColorToggle = 0;
            if (LabelColorToggle == 0)
            {
                txBx_Color1.Text = "0000FF";
                txBx_Color2.Text = "00FF00";
                txBx_Color3.Text = "FF0000";
                txBx_Color4.Text = "FFFFFF";
                txBx_Color5.Text = "FFFFFF";
                txBx_Color_Overlay.Text = "FF00FF";
            }
            if (LabelColorToggle == 1)
            {
                txBx_Color1.Text = "FF0000";
                txBx_Color2.Text = "0000FF";
                txBx_Color3.Text = "00FF00";
                txBx_Color4.Text = "FFFFFF";
                txBx_Color5.Text = "FFFFFF";
                txBx_Color_Overlay.Text = "FF00FF";
            }
            if (LabelColorToggle == 2)
            {
                txBx_Color1.Text = "8000FF";
                txBx_Color2.Text = "FFFFFF";
                txBx_Color3.Text = "00FFFF";
                txBx_Color4.Text = "FFFFFF";
                txBx_Color5.Text = "FFFFFF";
                txBx_Color_Overlay.Text = "FF00FF";
            }
            if (LabelColorToggle == 3)
            {
                txBx_Color1.Text = "FFFFFF";
                txBx_Color2.Text = "FFFFFF";
                txBx_Color3.Text = "FFFFFF";
                txBx_Color4.Text = "FFFFFF";
                txBx_Color5.Text = "FFFFFF";
                txBx_Color_Overlay.Text = "FF00FF";
            }
        }


        bool addedElapsedHandler = false;
        private System.Timers.Timer slideshowTimer = new System.Timers.Timer();
        private static int currentTimePoint; // Class-level variable to track the current slide

        private void btn_SlideShow_click(object sender, EventArgs e)
        {
            // Disable the button to prevent multiple slideshow starts
            btn_SlideShow.Enabled = false;

            // Validate time-series data
            if (IC_Folder.XDCE.Images == null || !IC_Folder.XDCE.Images.Any())
            {
                MessageBox.Show("No images available for slideshow.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btn_SlideShow.Enabled = true;
                return;
            }

            // Reset currentTimePoint globally to the first time point
            currentTimePoint = 0; // Start at the first time point

            // Configure the timer
            slideshowTimer.Interval = 400; // 500 milliseconds
            slideshowTimer.AutoReset = false; // Allow manual control of restarting
            slideshowTimer.Stop(); // Ensure the timer is stopped before starting

            // Remove existing handlers and add the handler again (optional but ensures correctness)
            slideshowTimer.Elapsed -= SlideshowTimerHandler; // Remove any previous handlers
            slideshowTimer.Elapsed += SlideshowTimerHandler; // Add the handler


            // Start the slideshow
            slideshowTimer.Start();
        }

        private void SlideshowTimerHandler(object sender, ElapsedEventArgs args)
        {
            this.Invoke((MethodInvoker)(() =>
            {
                int maxTimePoints = IC_Folder.XDCE.Images.Max(img => img.TimeSeriesOrder);

                // Check if we reached the end of the slideshow
                if (currentTimePoint >= maxTimePoints)
                {
                    slideshowTimer.Stop();
                    btn_SlideShow.Enabled = true; // Re-enable the button when slideshow finishes
                    MessageBox.Show("Slideshow has reached the end of the time series data.", "End of Slideshow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // Validate before advancing
                int nextTimePoint = currentTimePoint + 1;
                if (nextTimePoint > maxTimePoints)
                {
                    slideshowTimer.Stop();
                    btn_SlideShow.Enabled = true; // Re-enable the button when slideshow finishes
                    MessageBox.Show("Slideshow has completed.", "End of Slideshow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // Advance the slideshow
                currentTimePoint = nextTimePoint; // Increment current time point
                UpdateImagesForTimePoint(currentTimePoint);

                // Restart the timer if there are more slides
                if (currentTimePoint < maxTimePoints)
                {
                    slideshowTimer.Start();
                }
                else
                {
                    slideshowTimer.Stop();
                    btn_SlideShow.Enabled = true; // Re-enable the button when slideshow finishes
                    MessageBox.Show("Slideshow has completed.", "End of Slideshow", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }));
        }
        private void txBx_MoveToTimePoint_TextChanged(object sender, EventArgs e)
        {
            // Parse the entered value to determine the time point
            if (int.TryParse(txBx_MoveToTimePoint.Text, out int requestedTimePoint))
            {
                // Validate that the requested time point exists
                int currentTimePoint = _XDCEs.FirstOrDefault()?.TimeSeriesOrder ?? 1; // Get current time point
                if (currentTimePoint == requestedTimePoint)
                {
                    return;
                }

                int maxTimePoint = IC_Folder.XDCE.Images.Max(t => t.TimeSeriesOrder);

                if (requestedTimePoint >= 0 && requestedTimePoint <= maxTimePoint)
                {
                    if (requestedTimePoint ==0)
                        requestedTimePoint = 1;
                    // Move to the requested time point
                    MoveToTimePoint(requestedTimePoint);
                }
                else
                {
                    MessageBox.Show("Invalid time point. Please enter a valid time point within the range.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Please enter a numeric value for the time point.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private (int? fov1, int? fov2, int? fov3, int? fov4) GetFOVValues()
        {
            int? fov1 = int.TryParse(txBx_FOV_LR.Text, out int lr) ? lr : null;
            int? fov2 = int.TryParse(txBx_FOV_LL.Text, out int ll) ? ll : null;
            int? fov3 = int.TryParse(txBx_FOV_UR.Text, out int ur) ? ur : null;
            int? fov4 = int.TryParse(txBx_FOV.Text, out int ul) ? ul : null;

            return (fov1, fov2, fov3, fov4);
        }
        // Method to move to the requested time point

        private void UpdateImagesForTimePoint(int timeSeriesOrder)
        {
            var (fov1, fov2, fov3, fov4) = GetFOVValues();
            int maxOrder = IC_Folder.XDCE.Images.Max(t => t.TimeSeriesOrder);

            // Validate the requested time series order
            if (timeSeriesOrder < 1 || timeSeriesOrder > maxOrder)
            {
                // Stop the timer in case of an invalid point during the slideshow
                slideshowTimer.Stop();

                MessageBox.Show($"Invalid time point. Please select a time point between 1 and {maxOrder}.",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                timeSeriesOrder = 1;
                return;
            }

            // Retrieve images at the specified FOVs and time series order
            var fovsAtTimePoint = IC_Folder.XDCE.Images
                .Where(t => new int?[] { fov1, fov2, fov3, fov4 }.Contains(t.FOV) && t.TimeSeriesOrder == timeSeriesOrder)
                .ToList();

            if (fovsAtTimePoint.Count == 0)
            {
                MessageBox.Show("No images found for the requested time point.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Organize images based on their FOVs for consistent ordering
            XDCE_Image[] newImages = new XDCE_Image[4];
            newImages[3] = fovsAtTimePoint.FirstOrDefault(t => t.FOV == fov1);
            newImages[2] = fovsAtTimePoint.FirstOrDefault(t => t.FOV == fov2);
            newImages[1] = fovsAtTimePoint.FirstOrDefault(t => t.FOV == fov3);
            newImages[0] = fovsAtTimePoint.FirstOrDefault(t => t.FOV == fov4);

            // Update each picture box with the corresponding image or leave blank if no image
            for (int i = 0; i < newImages.Length; i++)
            {
                if (newImages[i] != null)
                {
                    UpdateBox(i, newImages[i].RowNumber, newImages[i].ColNumber, newImages[i]);
                }
                else
                {
                    ClearBox(i);
                }
            }

            // Update the current time point label or text box
            txBx_MoveToTimePoint.Text = timeSeriesOrder.ToString();
        }
        private void TimeSeriesBck_Click(object sender, EventArgs e)
        {
            if (_XDCEs == null || !_XDCEs.Any())
            {
                MessageBox.Show("No time series data available.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            // Calculate the previous time point
            int currentTimePoint = _XDCEs.FirstOrDefault()?.TimeSeriesOrder ?? 1;
            int previousTimePoint = currentTimePoint - 1;

            if (previousTimePoint < 1)
            {
                MessageBox.Show("You have reached the beginning of the time series data.", "No earlier data exists.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Update images for the new time point
            UpdateImagesForTimePoint(previousTimePoint);
        }
        private void TimeSeriesFwd_Click(object sender, EventArgs e)
        {
            if (_XDCEs == null || !_XDCEs.Any())
            {
                MessageBox.Show("No time series data available.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int currentTimePoint = _XDCEs.FirstOrDefault()?.TimeSeriesOrder ?? 1;
            int maxTimePoints = IC_Folder.XDCE.Images.Max(img => img.TimeSeriesOrder);

            // Validate that the next time point is within range
            if (currentTimePoint + 1 > maxTimePoints)
            {
                slideshowTimer.Stop(); // Stop the slideshow timer
                btn_SlideShow.Enabled = true; // Re-enable the button
                currentTimePoint = 0;
                MessageBox.Show("You have reached the end of the time series data.", "End of Time Series", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Update images for the next time point
            UpdateImagesForTimePoint(currentTimePoint + 1);
        }
        private void MoveToTimePoint(int requestedTimePoint)
        {
            if (_XDCEs == null || !_XDCEs.Any())
            {
                MessageBox.Show("No time series data available.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            UpdateImagesForTimePoint(requestedTimePoint);
        }

        private void chk_Bx_ObjClipsVis_CheckedChanged(object sender, EventArgs e)
        {
            txBx_ClipExpand_Neg.Enabled = txBx_ClipExpand_Pos.Enabled = chk_Bx_ObjClipsVis.Checked;
        }


        //This was just a way of testing some stuff . . 
        private void Leica_Label_Click(object sender, EventArgs e)
        {
            Model_Run_Classification_Go(sender, e);

            // - - - - - - - - - - - - - - - - - - - - - - - - - - To test out Leica stuff

            bool dubay = false;
            if (dubay)
            {
                FIVE_IMG.Associate_InCell_Leica.GenerateMemoryList(IC_Folder, "C - 2", @"c:\temp\C2b.stg", 2000, NVP.RegParams);

                FIVE_IMG.Associate_InCell_Leica.CompareAllLeicaSessionImages(IC_Folder, Wavelength, Brightness, @"R:\five\exp\fiv497\PlateDB", NVP.RegParams, true);

                txBx_SavedList.Text = "";
            }
        }

        #endregion


        private void TimeLabel_Click(object sender, EventArgs e)
        {

        }

    }
}

public static class Prompt
{
    public static string ShowDialog(string text, string caption, string defaultText)
    {
        Form prompt = new Form()
        {
            Width = 500,
            Height = 250,
            FormBorderStyle = FormBorderStyle.FixedDialog,
            Text = caption,
            StartPosition = FormStartPosition.CenterScreen
        };
        Label textLabel = new Label() { Left = 50, Top = 20, Width = 200, Text = text };
        TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400, Text = defaultText, Multiline = true };
        Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
        confirmation.Click += (sender, e) => { prompt.Close(); };
        prompt.Controls.Add(textBox);
        prompt.Controls.Add(confirmation);
        prompt.Controls.Add(textLabel);
        prompt.AcceptButton = confirmation;

        return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
    }
}