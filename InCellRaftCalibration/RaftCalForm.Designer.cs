﻿using System;

namespace FIVE.RaftCal
{
    partial class FormRaftCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components=new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRaftCal));
            pictureBox1=new System.Windows.Forms.PictureBox();
            txBx_FolderPath=new System.Windows.Forms.TextBox();
            btn_Update=new System.Windows.Forms.Button();
            txBx_ImageName=new System.Windows.Forms.TextBox();
            txBx_FOV=new System.Windows.Forms.TextBox();
            txBx_BrightnessMultiplier1=new System.Windows.Forms.TextBox();
            pictureBox2=new System.Windows.Forms.PictureBox();
            pictureBox3=new System.Windows.Forms.PictureBox();
            pictureBox4=new System.Windows.Forms.PictureBox();
            label1=new System.Windows.Forms.Label();
            label2=new System.Windows.Forms.Label();
            label3=new System.Windows.Forms.Label();
            txBx_Row=new System.Windows.Forms.TextBox();
            txBx_Col=new System.Windows.Forms.TextBox();
            btn_Up=new System.Windows.Forms.Button();
            btn_Down=new System.Windows.Forms.Button();
            btn_Left=new System.Windows.Forms.Button();
            btn_Right=new System.Windows.Forms.Button();
            label4=new System.Windows.Forms.Label();
            label5=new System.Windows.Forms.Label();
            label6=new System.Windows.Forms.Label();
            txBx_Wavelength1=new System.Windows.Forms.TextBox();
            btn_U5=new System.Windows.Forms.Button();
            btn_D5=new System.Windows.Forms.Button();
            btn_L5=new System.Windows.Forms.Button();
            btn_R5=new System.Windows.Forms.Button();
            labl_ImageName=new System.Windows.Forms.Label();
            txBx_SavedList=new System.Windows.Forms.TextBox();
            txBx_RaftID=new System.Windows.Forms.TextBox();
            label7=new System.Windows.Forms.Label();
            label9=new System.Windows.Forms.Label();
            txBx_LocationInfo=new System.Windows.Forms.TextBox();
            label_Notify=new System.Windows.Forms.Label();
            panel1=new System.Windows.Forms.Panel();
            btnFieldSnake=new System.Windows.Forms.Button();
            btn_RaftNext=new System.Windows.Forms.Button();
            btn_RaftPrev=new System.Windows.Forms.Button();
            btnFieldNext=new System.Windows.Forms.Button();
            btnFieldBack=new System.Windows.Forms.Button();
            btn_BL=new System.Windows.Forms.Button();
            btn_UR=new System.Windows.Forms.Button();
            btn_BR=new System.Windows.Forms.Button();
            btn_UL=new System.Windows.Forms.Button();
            btn_SaveCalibration=new System.Windows.Forms.Button();
            label_Warning=new System.Windows.Forms.Label();
            radioButton_TestCal=new System.Windows.Forms.RadioButton();
            radioButton_RecordFocus=new System.Windows.Forms.RadioButton();
            label11=new System.Windows.Forms.Label();
            btn_SaveCheck=new System.Windows.Forms.Button();
            comboBox_Well=new System.Windows.Forms.ComboBox();
            panel2=new System.Windows.Forms.Panel();
            label12=new System.Windows.Forms.Label();
            lblImageCheck=new System.Windows.Forms.Label();
            btnResetImageCheck=new System.Windows.Forms.Button();
            btnResetSaveCal=new System.Windows.Forms.Button();
            chkBx_SquareMode=new System.Windows.Forms.CheckBox();
            label13=new System.Windows.Forms.Label();
            btn_Size512=new System.Windows.Forms.Button();
            panelControls=new System.Windows.Forms.Panel();
            txBx_MoveToTimePoint=new System.Windows.Forms.TextBox();
            btn_SlideShow=new System.Windows.Forms.Button();
            TimeLabel=new System.Windows.Forms.Label();
            btn_TimeSeriesBck=new System.Windows.Forms.Button();
            btn_TimeSeriesFwd=new System.Windows.Forms.Button();
            txBx_ClipExpand_Neg=new System.Windows.Forms.TextBox();
            txBx_ClipExpand_Pos=new System.Windows.Forms.TextBox();
            chk_Bx_ObjClipsVis=new System.Windows.Forms.CheckBox();
            txBx_Color_Overlay=new System.Windows.Forms.TextBox();
            comboBox_ColorMerge=new System.Windows.Forms.ComboBox();
            chkBx_ShowCalGrid=new System.Windows.Forms.CheckBox();
            btnJumpNextCalPoint=new System.Windows.Forms.Button();
            label19=new System.Windows.Forms.Label();
            label18=new System.Windows.Forms.Label();
            chkBx_DI_5=new System.Windows.Forms.CheckBox();
            chkBx_DI_4=new System.Windows.Forms.CheckBox();
            chkBx_DI_3=new System.Windows.Forms.CheckBox();
            chkBx_DI_2=new System.Windows.Forms.CheckBox();
            chkBx_DI_1=new System.Windows.Forms.CheckBox();
            btnRunSegmentation=new System.Windows.Forms.Button();
            txBx_Wv_Thresh5=new System.Windows.Forms.TextBox();
            txBx_Wv_Thresh4=new System.Windows.Forms.TextBox();
            txBx_Wv_Thresh3=new System.Windows.Forms.TextBox();
            txBx_Wv_Thresh2=new System.Windows.Forms.TextBox();
            txBx_Wv_Thresh1=new System.Windows.Forms.TextBox();
            label17=new System.Windows.Forms.Label();
            btn_SaveWavelengthInfo=new System.Windows.Forms.Button();
            txBx_WavelengthExtended=new System.Windows.Forms.TextBox();
            label16=new System.Windows.Forms.Label();
            label15=new System.Windows.Forms.Label();
            txBx_Wv_Abbrev5=new System.Windows.Forms.TextBox();
            txBx_Wv_Name5=new System.Windows.Forms.TextBox();
            txBx_Wv_Abbrev4=new System.Windows.Forms.TextBox();
            txBx_Wv_Name4=new System.Windows.Forms.TextBox();
            txBx_Wv_Abbrev3=new System.Windows.Forms.TextBox();
            txBx_Wv_Name3=new System.Windows.Forms.TextBox();
            txBx_Wv_Abbrev2=new System.Windows.Forms.TextBox();
            txBx_Wv_Name2=new System.Windows.Forms.TextBox();
            txBx_Wv_Abbrev1=new System.Windows.Forms.TextBox();
            txBx_Wv_Name1=new System.Windows.Forms.TextBox();
            chkBx_ObjDetect5=new System.Windows.Forms.CheckBox();
            chkBx_Onwv5=new System.Windows.Forms.CheckBox();
            txBx_Color5=new System.Windows.Forms.TextBox();
            txBx_BrightnessMultiplier5=new System.Windows.Forms.TextBox();
            txBx_Wavelength5=new System.Windows.Forms.TextBox();
            label14=new System.Windows.Forms.Label();
            label10=new System.Windows.Forms.Label();
            chkBx_ObjDetect4=new System.Windows.Forms.CheckBox();
            chkBx_ObjDetect3=new System.Windows.Forms.CheckBox();
            chkBx_ObjDetect2=new System.Windows.Forms.CheckBox();
            chkBx_ObjDetect1=new System.Windows.Forms.CheckBox();
            lbl_xyi=new System.Windows.Forms.Label();
            lbl_SegTest=new System.Windows.Forms.Label();
            pictureBox_Zoom=new System.Windows.Forms.PictureBox();
            btn_ExportCurrentImageView=new System.Windows.Forms.Button();
            txBx_FOV_LR=new System.Windows.Forms.TextBox();
            txBx_FOV_UR=new System.Windows.Forms.TextBox();
            txBx_FOV_LL=new System.Windows.Forms.TextBox();
            Leica_Label=new System.Windows.Forms.Label();
            label8=new System.Windows.Forms.Label();
            chkBx_Onwv4=new System.Windows.Forms.CheckBox();
            chkBx_Onwv3=new System.Windows.Forms.CheckBox();
            chkBx_Onwv2=new System.Windows.Forms.CheckBox();
            txBx_Color4=new System.Windows.Forms.TextBox();
            txBx_Color3=new System.Windows.Forms.TextBox();
            txBx_Color2=new System.Windows.Forms.TextBox();
            txBx_Color1=new System.Windows.Forms.TextBox();
            chkBx_Onwv1=new System.Windows.Forms.CheckBox();
            txBx_BrightnessMultiplier4=new System.Windows.Forms.TextBox();
            txBx_Wavelength4=new System.Windows.Forms.TextBox();
            txBx_BrightnessMultiplier3=new System.Windows.Forms.TextBox();
            txBx_Wavelength3=new System.Windows.Forms.TextBox();
            txBx_BrightnessMultiplier2=new System.Windows.Forms.TextBox();
            txBx_Wavelength2=new System.Windows.Forms.TextBox();
            btn_ExportFromList=new System.Windows.Forms.Button();
            btnUndoImageCheck=new System.Windows.Forms.Button();
            comboBox_Overlay=new System.Windows.Forms.ComboBox();
            chkBx_ShowOverlay=new System.Windows.Forms.CheckBox();
            btnSize300=new System.Windows.Forms.Button();
            btnSize700=new System.Windows.Forms.Button();
            toolTip1=new System.Windows.Forms.ToolTip(components);
            btn_Finish=new System.Windows.Forms.Button();
            bgWorker_ImageLoad=new System.ComponentModel.BackgroundWorker();
            bgWorker_Analysis1=new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            panel1.SuspendLayout();
            panelControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Zoom).BeginInit();
            SuspendLayout();
            // 
            // pictureBox1
            // 
            pictureBox1.Cursor=System.Windows.Forms.Cursors.Cross;
            pictureBox1.Location=new System.Drawing.Point(8, 9);
            pictureBox1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            pictureBox1.Name="pictureBox1";
            pictureBox1.Size=new System.Drawing.Size(400, 461);
            pictureBox1.TabIndex=0;
            pictureBox1.TabStop=false;
            // 
            // txBx_FolderPath
            // 
            txBx_FolderPath.Location=new System.Drawing.Point(2, 41);
            txBx_FolderPath.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_FolderPath.Name="txBx_FolderPath";
            txBx_FolderPath.ReadOnly=true;
            txBx_FolderPath.Size=new System.Drawing.Size(509, 27);
            txBx_FolderPath.TabIndex=1;
            txBx_FolderPath.DoubleClick+=txBx_FolderPath_DoubleClick;
            // 
            // btn_Update
            // 
            btn_Update.Location=new System.Drawing.Point(402, 304);
            btn_Update.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Update.Name="btn_Update";
            btn_Update.Size=new System.Drawing.Size(109, 44);
            btn_Update.TabIndex=2;
            btn_Update.Text="Update";
            toolTip1.SetToolTip(btn_Update, "Reload the images with current settings");
            btn_Update.UseVisualStyleBackColor=true;
            btn_Update.Click+=Update_Display;
            // 
            // txBx_ImageName
            // 
            txBx_ImageName.Enabled=false;
            txBx_ImageName.Location=new System.Drawing.Point(102, 987);
            txBx_ImageName.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_ImageName.Name="txBx_ImageName";
            txBx_ImageName.Size=new System.Drawing.Size(274, 27);
            txBx_ImageName.TabIndex=3;
            // 
            // txBx_FOV
            // 
            txBx_FOV.Enabled=false;
            txBx_FOV.Font=new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            txBx_FOV.Location=new System.Drawing.Point(10, 947);
            txBx_FOV.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_FOV.Name="txBx_FOV";
            txBx_FOV.Size=new System.Drawing.Size(37, 23);
            txBx_FOV.TabIndex=4;
            // 
            // txBx_BrightnessMultiplier1
            // 
            txBx_BrightnessMultiplier1.Location=new System.Drawing.Point(274, 104);
            txBx_BrightnessMultiplier1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_BrightnessMultiplier1.Name="txBx_BrightnessMultiplier1";
            txBx_BrightnessMultiplier1.Size=new System.Drawing.Size(52, 27);
            txBx_BrightnessMultiplier1.TabIndex=5;
            // 
            // pictureBox2
            // 
            pictureBox2.Cursor=System.Windows.Forms.Cursors.Cross;
            pictureBox2.Location=new System.Drawing.Point(416, 9);
            pictureBox2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            pictureBox2.Name="pictureBox2";
            pictureBox2.Size=new System.Drawing.Size(400, 461);
            pictureBox2.TabIndex=7;
            pictureBox2.TabStop=false;
            // 
            // pictureBox3
            // 
            pictureBox3.Cursor=System.Windows.Forms.Cursors.Cross;
            pictureBox3.Location=new System.Drawing.Point(8, 480);
            pictureBox3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            pictureBox3.Name="pictureBox3";
            pictureBox3.Size=new System.Drawing.Size(400, 461);
            pictureBox3.TabIndex=9;
            pictureBox3.TabStop=false;
            // 
            // pictureBox4
            // 
            pictureBox4.Cursor=System.Windows.Forms.Cursors.Cross;
            pictureBox4.Location=new System.Drawing.Point(416, 480);
            pictureBox4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            pictureBox4.Name="pictureBox4";
            pictureBox4.Size=new System.Drawing.Size(400, 461);
            pictureBox4.TabIndex=8;
            pictureBox4.TabStop=false;
            // 
            // label1
            // 
            label1.AutoSize=true;
            label1.Location=new System.Drawing.Point(10, 921);
            label1.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label1.Name="label1";
            label1.Size=new System.Drawing.Size(36, 20);
            label1.TabIndex=10;
            label1.Text="FOV";
            // 
            // label2
            // 
            label2.AutoSize=true;
            label2.Location=new System.Drawing.Point(280, 80);
            label2.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label2.Name="label2";
            label2.Size=new System.Drawing.Size(49, 20);
            label2.TabIndex=11;
            label2.Text="Bright";
            toolTip1.SetToolTip(label2, "A multiplier (decimal) >=1. Add a slash \"/\" and put the absolute value for the black level. Default is 10 (if no slash). ");
            // 
            // label3
            // 
            label3.AutoSize=true;
            label3.Location=new System.Drawing.Point(9, 13);
            label3.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label3.Name="label3";
            label3.Size=new System.Drawing.Size(92, 20);
            label3.TabIndex=12;
            label3.Text="InCell Folder";
            // 
            // txBx_Row
            // 
            txBx_Row.Location=new System.Drawing.Point(178, 373);
            txBx_Row.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Row.Name="txBx_Row";
            txBx_Row.Size=new System.Drawing.Size(67, 27);
            txBx_Row.TabIndex=13;
            // 
            // txBx_Col
            // 
            txBx_Col.Location=new System.Drawing.Point(254, 373);
            txBx_Col.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Col.Name="txBx_Col";
            txBx_Col.Size=new System.Drawing.Size(67, 27);
            txBx_Col.TabIndex=14;
            // 
            // btn_Up
            // 
            btn_Up.Location=new System.Drawing.Point(79, 40);
            btn_Up.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Up.Name="btn_Up";
            btn_Up.Size=new System.Drawing.Size(64, 31);
            btn_Up.TabIndex=15;
            btn_Up.Text="Up";
            btn_Up.UseVisualStyleBackColor=true;
            btn_Up.Click+=btn_Up_Click;
            // 
            // btn_Down
            // 
            btn_Down.Location=new System.Drawing.Point(79, 113);
            btn_Down.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Down.Name="btn_Down";
            btn_Down.Size=new System.Drawing.Size(64, 31);
            btn_Down.TabIndex=16;
            btn_Down.Text="Down";
            btn_Down.UseVisualStyleBackColor=true;
            btn_Down.Click+=btn_Down_Click;
            // 
            // btn_Left
            // 
            btn_Left.Location=new System.Drawing.Point(55, 77);
            btn_Left.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Left.Name="btn_Left";
            btn_Left.Size=new System.Drawing.Size(53, 31);
            btn_Left.TabIndex=17;
            btn_Left.Text="Left";
            btn_Left.UseVisualStyleBackColor=true;
            btn_Left.Click+=btn_Left_Click;
            // 
            // btn_Right
            // 
            btn_Right.Location=new System.Drawing.Point(107, 77);
            btn_Right.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Right.Name="btn_Right";
            btn_Right.Size=new System.Drawing.Size(64, 31);
            btn_Right.TabIndex=18;
            btn_Right.Text="Right";
            btn_Right.UseVisualStyleBackColor=true;
            btn_Right.Click+=btn_Right_Click;
            // 
            // label4
            // 
            label4.AutoSize=true;
            label4.Location=new System.Drawing.Point(178, 349);
            label4.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label4.Name="label4";
            label4.Size=new System.Drawing.Size(38, 20);
            label4.TabIndex=19;
            label4.Text="Row";
            // 
            // label5
            // 
            label5.AutoSize=true;
            label5.Location=new System.Drawing.Point(256, 349);
            label5.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label5.Name="label5";
            label5.Size=new System.Drawing.Size(60, 20);
            label5.TabIndex=20;
            label5.Text="Column";
            // 
            // label6
            // 
            label6.AutoSize=true;
            label6.Location=new System.Drawing.Point(239, 80);
            label6.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label6.Name="label6";
            label6.Size=new System.Drawing.Size(27, 20);
            label6.TabIndex=22;
            label6.Text="wv";
            toolTip1.SetToolTip(label6, "This is 0-indexed");
            // 
            // txBx_Wavelength1
            // 
            txBx_Wavelength1.Location=new System.Drawing.Point(235, 104);
            txBx_Wavelength1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wavelength1.Name="txBx_Wavelength1";
            txBx_Wavelength1.Size=new System.Drawing.Size(31, 27);
            txBx_Wavelength1.TabIndex=21;
            // 
            // btn_U5
            // 
            btn_U5.Location=new System.Drawing.Point(89, 3);
            btn_U5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_U5.Name="btn_U5";
            btn_U5.Size=new System.Drawing.Size(42, 31);
            btn_U5.TabIndex=23;
            btn_U5.Text="U5";
            btn_U5.UseVisualStyleBackColor=true;
            btn_U5.Click+=btn_U5_Click;
            // 
            // btn_D5
            // 
            btn_D5.Location=new System.Drawing.Point(89, 149);
            btn_D5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_D5.Name="btn_D5";
            btn_D5.Size=new System.Drawing.Size(42, 31);
            btn_D5.TabIndex=24;
            btn_D5.Text="D5";
            btn_D5.UseVisualStyleBackColor=true;
            btn_D5.Click+=btn_D5_Click;
            // 
            // btn_L5
            // 
            btn_L5.Location=new System.Drawing.Point(5, 77);
            btn_L5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_L5.Name="btn_L5";
            btn_L5.Size=new System.Drawing.Size(42, 31);
            btn_L5.TabIndex=25;
            btn_L5.Text="L5";
            btn_L5.UseVisualStyleBackColor=true;
            btn_L5.Click+=btn_L5_Click;
            // 
            // btn_R5
            // 
            btn_R5.Location=new System.Drawing.Point(178, 77);
            btn_R5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_R5.Name="btn_R5";
            btn_R5.Size=new System.Drawing.Size(42, 31);
            btn_R5.TabIndex=26;
            btn_R5.Text="R5";
            btn_R5.UseVisualStyleBackColor=true;
            btn_R5.Click+=btn_R5_Click;
            // 
            // labl_ImageName
            // 
            labl_ImageName.AutoSize=true;
            labl_ImageName.Location=new System.Drawing.Point(104, 961);
            labl_ImageName.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            labl_ImageName.Name="labl_ImageName";
            labl_ImageName.Size=new System.Drawing.Size(95, 20);
            labl_ImageName.TabIndex=27;
            labl_ImageName.Text="Image Name";
            // 
            // txBx_SavedList
            // 
            txBx_SavedList.Location=new System.Drawing.Point(120, 691);
            txBx_SavedList.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_SavedList.Multiline=true;
            txBx_SavedList.Name="txBx_SavedList";
            txBx_SavedList.Size=new System.Drawing.Size(388, 72);
            txBx_SavedList.TabIndex=28;
            txBx_SavedList.DoubleClick+=txBx_SavedList_DoubleClick;
            // 
            // txBx_RaftID
            // 
            txBx_RaftID.Location=new System.Drawing.Point(5, 691);
            txBx_RaftID.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_RaftID.Name="txBx_RaftID";
            txBx_RaftID.Size=new System.Drawing.Size(91, 27);
            txBx_RaftID.TabIndex=29;
            toolTip1.SetToolTip(txBx_RaftID, resources.GetString("txBx_RaftID.ToolTip"));
            // 
            // label7
            // 
            label7.AutoSize=true;
            label7.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label7.Location=new System.Drawing.Point(7, 724);
            label7.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label7.Name="label7";
            label7.Size=new System.Drawing.Size(97, 26);
            label7.TabIndex=30;
            label7.Text="Type the RaftID,\r\nthen Click to Save.";
            // 
            // label9
            // 
            label9.AutoSize=true;
            label9.Location=new System.Drawing.Point(376, 961);
            label9.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label9.Name="label9";
            label9.Size=new System.Drawing.Size(138, 20);
            label9.TabIndex=27;
            label9.Text="Plate Location (um)";
            // 
            // txBx_LocationInfo
            // 
            txBx_LocationInfo.Enabled=false;
            txBx_LocationInfo.Location=new System.Drawing.Point(384, 987);
            txBx_LocationInfo.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_LocationInfo.Name="txBx_LocationInfo";
            txBx_LocationInfo.Size=new System.Drawing.Size(127, 27);
            txBx_LocationInfo.TabIndex=32;
            // 
            // label_Notify
            // 
            label_Notify.AutoSize=true;
            label_Notify.Location=new System.Drawing.Point(278, 12);
            label_Notify.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label_Notify.Name="label_Notify";
            label_Notify.Size=new System.Drawing.Size(53, 20);
            label_Notify.TabIndex=33;
            label_Notify.Text="( - - - )";
            // 
            // panel1
            // 
            panel1.Controls.Add(btnFieldSnake);
            panel1.Controls.Add(btn_RaftNext);
            panel1.Controls.Add(btn_RaftPrev);
            panel1.Controls.Add(btnFieldNext);
            panel1.Controls.Add(btnFieldBack);
            panel1.Controls.Add(btn_BL);
            panel1.Controls.Add(btn_UR);
            panel1.Controls.Add(btn_BR);
            panel1.Controls.Add(btn_UL);
            panel1.Controls.Add(btn_Right);
            panel1.Controls.Add(btn_Up);
            panel1.Controls.Add(btn_Down);
            panel1.Controls.Add(btn_Left);
            panel1.Controls.Add(btn_U5);
            panel1.Controls.Add(btn_D5);
            panel1.Controls.Add(btn_L5);
            panel1.Controls.Add(btn_R5);
            panel1.Location=new System.Drawing.Point(281, 503);
            panel1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            panel1.Name="panel1";
            panel1.Size=new System.Drawing.Size(225, 183);
            panel1.TabIndex=34;
            // 
            // btnFieldSnake
            // 
            btnFieldSnake.Location=new System.Drawing.Point(139, 149);
            btnFieldSnake.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnFieldSnake.Name="btnFieldSnake";
            btnFieldSnake.Size=new System.Drawing.Size(30, 31);
            btnFieldSnake.TabIndex=35;
            btnFieldSnake.Text="~";
            btnFieldSnake.UseVisualStyleBackColor=true;
            btnFieldSnake.Click+=btnFieldSnake_Click;
            // 
            // btn_RaftNext
            // 
            btn_RaftNext.Location=new System.Drawing.Point(151, 40);
            btn_RaftNext.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_RaftNext.Name="btn_RaftNext";
            btn_RaftNext.Size=new System.Drawing.Size(42, 31);
            btn_RaftNext.TabIndex=34;
            btn_RaftNext.Text=">";
            btn_RaftNext.UseVisualStyleBackColor=true;
            btn_RaftNext.Click+=btn_RaftNext_Click;
            // 
            // btn_RaftPrev
            // 
            btn_RaftPrev.Location=new System.Drawing.Point(27, 40);
            btn_RaftPrev.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_RaftPrev.Name="btn_RaftPrev";
            btn_RaftPrev.Size=new System.Drawing.Size(42, 31);
            btn_RaftPrev.TabIndex=33;
            btn_RaftPrev.Text="<";
            btn_RaftPrev.UseVisualStyleBackColor=true;
            btn_RaftPrev.Click+=btn_RaftPrev_Click;
            // 
            // btnFieldNext
            // 
            btnFieldNext.Location=new System.Drawing.Point(151, 113);
            btnFieldNext.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnFieldNext.Name="btnFieldNext";
            btnFieldNext.Size=new System.Drawing.Size(42, 31);
            btnFieldNext.TabIndex=32;
            btnFieldNext.Text=">";
            btnFieldNext.UseVisualStyleBackColor=true;
            btnFieldNext.Click+=btnFieldNext_Click;
            // 
            // btnFieldBack
            // 
            btnFieldBack.Location=new System.Drawing.Point(27, 113);
            btnFieldBack.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnFieldBack.Name="btnFieldBack";
            btnFieldBack.Size=new System.Drawing.Size(42, 31);
            btnFieldBack.TabIndex=31;
            btnFieldBack.Text="<";
            btnFieldBack.UseVisualStyleBackColor=true;
            btnFieldBack.Click+=btnFieldBack_Click;
            // 
            // btn_BL
            // 
            btn_BL.Location=new System.Drawing.Point(5, 149);
            btn_BL.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_BL.Name="btn_BL";
            btn_BL.Size=new System.Drawing.Size(42, 31);
            btn_BL.TabIndex=30;
            btn_BL.Text="BL";
            btn_BL.UseVisualStyleBackColor=true;
            btn_BL.Click+=btn_BL_Click;
            // 
            // btn_UR
            // 
            btn_UR.Location=new System.Drawing.Point(178, 3);
            btn_UR.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_UR.Name="btn_UR";
            btn_UR.Size=new System.Drawing.Size(42, 31);
            btn_UR.TabIndex=29;
            btn_UR.Text="UR";
            btn_UR.UseVisualStyleBackColor=true;
            btn_UR.Click+=btn_UR_Click;
            // 
            // btn_BR
            // 
            btn_BR.Location=new System.Drawing.Point(178, 149);
            btn_BR.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_BR.Name="btn_BR";
            btn_BR.Size=new System.Drawing.Size(42, 31);
            btn_BR.TabIndex=28;
            btn_BR.Text="BR";
            btn_BR.UseVisualStyleBackColor=true;
            btn_BR.Click+=btn_BR_Click;
            // 
            // btn_UL
            // 
            btn_UL.Location=new System.Drawing.Point(5, 3);
            btn_UL.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_UL.Name="btn_UL";
            btn_UL.Size=new System.Drawing.Size(42, 31);
            btn_UL.TabIndex=27;
            btn_UL.Text="UL";
            btn_UL.UseVisualStyleBackColor=true;
            btn_UL.Click+=btn_UL_Click;
            // 
            // btn_SaveCalibration
            // 
            btn_SaveCalibration.Enabled=false;
            btn_SaveCalibration.Location=new System.Drawing.Point(153, 856);
            btn_SaveCalibration.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_SaveCalibration.Name="btn_SaveCalibration";
            btn_SaveCalibration.Size=new System.Drawing.Size(101, 69);
            btn_SaveCalibration.TabIndex=35;
            btn_SaveCalibration.Text="Save Calibration";
            btn_SaveCalibration.UseVisualStyleBackColor=true;
            btn_SaveCalibration.Click+=btn_SaveCalibration_Click;
            // 
            // label_Warning
            // 
            label_Warning.AutoSize=true;
            label_Warning.ForeColor=System.Drawing.Color.Green;
            label_Warning.Location=new System.Drawing.Point(23, 796);
            label_Warning.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label_Warning.Name="label_Warning";
            label_Warning.Size=new System.Drawing.Size(122, 20);
            label_Warning.TabIndex=36;
            label_Warning.Text="Reset Calibration";
            // 
            // radioButton_TestCal
            // 
            radioButton_TestCal.AutoSize=true;
            radioButton_TestCal.Enabled=false;
            radioButton_TestCal.Location=new System.Drawing.Point(22, 856);
            radioButton_TestCal.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            radioButton_TestCal.Name="radioButton_TestCal";
            radioButton_TestCal.Size=new System.Drawing.Size(131, 24);
            radioButton_TestCal.TabIndex=37;
            radioButton_TestCal.Text="See Calibration";
            radioButton_TestCal.UseVisualStyleBackColor=true;
            radioButton_TestCal.CheckedChanged+=radioButton_TestCal_CheckedChanged;
            // 
            // radioButton_RecordFocus
            // 
            radioButton_RecordFocus.AutoSize=true;
            radioButton_RecordFocus.Checked=true;
            radioButton_RecordFocus.Enabled=false;
            radioButton_RecordFocus.Location=new System.Drawing.Point(363, 824);
            radioButton_RecordFocus.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            radioButton_RecordFocus.Name="radioButton_RecordFocus";
            radioButton_RecordFocus.Size=new System.Drawing.Size(91, 24);
            radioButton_RecordFocus.TabIndex=38;
            radioButton_RecordFocus.TabStop=true;
            radioButton_RecordFocus.Text="Annotate";
            toolTip1.SetToolTip(radioButton_RecordFocus, "Hover over the images, then press a number key to set an annotation. Then Click. Use the \"Exports\" option above to edit the schema");
            radioButton_RecordFocus.UseVisualStyleBackColor=true;
            // 
            // label11
            // 
            label11.AutoSize=true;
            label11.Location=new System.Drawing.Point(289, 767);
            label11.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label11.Name="label11";
            label11.Size=new System.Drawing.Size(210, 20);
            label11.TabIndex=39;
            label11.Text="Left Click = Good, Right = Bad";
            // 
            // btn_SaveCheck
            // 
            btn_SaveCheck.Location=new System.Drawing.Point(359, 856);
            btn_SaveCheck.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_SaveCheck.Name="btn_SaveCheck";
            btn_SaveCheck.Size=new System.Drawing.Size(151, 69);
            btn_SaveCheck.TabIndex=40;
            btn_SaveCheck.Text="Save Image Check";
            btn_SaveCheck.UseVisualStyleBackColor=true;
            btn_SaveCheck.Click+=btn_SaveCheck_Click;
            // 
            // comboBox_Well
            // 
            comboBox_Well.DropDownStyle=System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox_Well.FormattingEnabled=true;
            comboBox_Well.Location=new System.Drawing.Point(7, 373);
            comboBox_Well.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            comboBox_Well.Name="comboBox_Well";
            comboBox_Well.Size=new System.Drawing.Size(159, 28);
            comboBox_Well.TabIndex=41;
            comboBox_Well.SelectedIndexChanged+=comboBox_Well_SelectedIndexChanged;
            // 
            // panel2
            // 
            panel2.BackColor=System.Drawing.Color.Black;
            panel2.Location=new System.Drawing.Point(262, 776);
            panel2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            panel2.Name="panel2";
            panel2.Size=new System.Drawing.Size(14, 200);
            panel2.TabIndex=42;
            // 
            // label12
            // 
            label12.AutoSize=true;
            label12.Location=new System.Drawing.Point(23, 767);
            label12.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label12.Name="label12";
            label12.Size=new System.Drawing.Size(150, 20);
            label12.TabIndex=43;
            label12.Text="Raft Calibration Only:";
            // 
            // lblImageCheck
            // 
            lblImageCheck.AutoSize=true;
            lblImageCheck.ForeColor=System.Drawing.Color.Green;
            lblImageCheck.Location=new System.Drawing.Point(288, 796);
            lblImageCheck.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            lblImageCheck.Name="lblImageCheck";
            lblImageCheck.Size=new System.Drawing.Size(134, 20);
            lblImageCheck.TabIndex=44;
            lblImageCheck.Text="Reset Image Check";
            lblImageCheck.Click+=lblImageCheck_Click;
            // 
            // btnResetImageCheck
            // 
            btnResetImageCheck.Location=new System.Drawing.Point(294, 891);
            btnResetImageCheck.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnResetImageCheck.Name="btnResetImageCheck";
            btnResetImageCheck.Size=new System.Drawing.Size(57, 33);
            btnResetImageCheck.TabIndex=45;
            btnResetImageCheck.Text="reset";
            btnResetImageCheck.UseVisualStyleBackColor=true;
            btnResetImageCheck.Click+=btnResetImageCheck_Click;
            // 
            // btnResetSaveCal
            // 
            btnResetSaveCal.Location=new System.Drawing.Point(90, 884);
            btnResetSaveCal.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnResetSaveCal.Name="btnResetSaveCal";
            btnResetSaveCal.Size=new System.Drawing.Size(57, 33);
            btnResetSaveCal.TabIndex=46;
            btnResetSaveCal.Text="reset";
            btnResetSaveCal.UseVisualStyleBackColor=true;
            btnResetSaveCal.Click+=btnResetCalibration;
            // 
            // chkBx_SquareMode
            // 
            chkBx_SquareMode.AutoSize=true;
            chkBx_SquareMode.Checked=true;
            chkBx_SquareMode.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_SquareMode.Location=new System.Drawing.Point(328, 379);
            chkBx_SquareMode.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_SquareMode.Name="chkBx_SquareMode";
            chkBx_SquareMode.Size=new System.Drawing.Size(120, 24);
            chkBx_SquareMode.TabIndex=47;
            chkBx_SquareMode.Text="Square Mode";
            toolTip1.SetToolTip(chkBx_SquareMode, "Square Mode - Assumes that the layout of the fields is in a uniform (usually rectangular) pattern");
            chkBx_SquareMode.UseVisualStyleBackColor=true;
            chkBx_SquareMode.CheckedChanged+=chkBx_RaftToggle_CheckedChanged;
            // 
            // label13
            // 
            label13.AutoSize=true;
            label13.Location=new System.Drawing.Point(7, 349);
            label13.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label13.Name="label13";
            label13.Size=new System.Drawing.Size(41, 20);
            label13.TabIndex=48;
            label13.Text="Well:";
            // 
            // btn_Size512
            // 
            btn_Size512.Font=new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_Size512.Location=new System.Drawing.Point(158, 7);
            btn_Size512.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Size512.Name="btn_Size512";
            btn_Size512.Size=new System.Drawing.Size(57, 33);
            btn_Size512.TabIndex=49;
            btn_Size512.Text="512";
            toolTip1.SetToolTip(btn_Size512, "Single Image Size 512x512");
            btn_Size512.UseVisualStyleBackColor=true;
            btn_Size512.Click+=btn_FullSize_Click;
            // 
            // panelControls
            // 
            panelControls.Controls.Add(txBx_MoveToTimePoint);
            panelControls.Controls.Add(btn_SlideShow);
            panelControls.Controls.Add(TimeLabel);
            panelControls.Controls.Add(btn_TimeSeriesBck);
            panelControls.Controls.Add(btn_TimeSeriesFwd);
            panelControls.Controls.Add(txBx_ClipExpand_Neg);
            panelControls.Controls.Add(txBx_ClipExpand_Pos);
            panelControls.Controls.Add(chk_Bx_ObjClipsVis);
            panelControls.Controls.Add(txBx_Color_Overlay);
            panelControls.Controls.Add(comboBox_ColorMerge);
            panelControls.Controls.Add(chkBx_ShowCalGrid);
            panelControls.Controls.Add(btnJumpNextCalPoint);
            panelControls.Controls.Add(label19);
            panelControls.Controls.Add(label18);
            panelControls.Controls.Add(chkBx_DI_5);
            panelControls.Controls.Add(chkBx_DI_4);
            panelControls.Controls.Add(chkBx_DI_3);
            panelControls.Controls.Add(chkBx_DI_2);
            panelControls.Controls.Add(chkBx_DI_1);
            panelControls.Controls.Add(btnRunSegmentation);
            panelControls.Controls.Add(txBx_Wv_Thresh5);
            panelControls.Controls.Add(txBx_Wv_Thresh4);
            panelControls.Controls.Add(txBx_Wv_Thresh3);
            panelControls.Controls.Add(txBx_Wv_Thresh2);
            panelControls.Controls.Add(txBx_Wv_Thresh1);
            panelControls.Controls.Add(label17);
            panelControls.Controls.Add(btn_SaveWavelengthInfo);
            panelControls.Controls.Add(txBx_WavelengthExtended);
            panelControls.Controls.Add(label16);
            panelControls.Controls.Add(label15);
            panelControls.Controls.Add(txBx_Wv_Abbrev5);
            panelControls.Controls.Add(txBx_Wv_Name5);
            panelControls.Controls.Add(txBx_Wv_Abbrev4);
            panelControls.Controls.Add(txBx_Wv_Name4);
            panelControls.Controls.Add(txBx_Wv_Abbrev3);
            panelControls.Controls.Add(txBx_Wv_Name3);
            panelControls.Controls.Add(txBx_Wv_Abbrev2);
            panelControls.Controls.Add(txBx_Wv_Name2);
            panelControls.Controls.Add(txBx_Wv_Abbrev1);
            panelControls.Controls.Add(txBx_Wv_Name1);
            panelControls.Controls.Add(chkBx_ObjDetect5);
            panelControls.Controls.Add(chkBx_Onwv5);
            panelControls.Controls.Add(txBx_Color5);
            panelControls.Controls.Add(txBx_BrightnessMultiplier5);
            panelControls.Controls.Add(txBx_Wavelength5);
            panelControls.Controls.Add(label14);
            panelControls.Controls.Add(label10);
            panelControls.Controls.Add(chkBx_ObjDetect4);
            panelControls.Controls.Add(chkBx_ObjDetect3);
            panelControls.Controls.Add(chkBx_ObjDetect2);
            panelControls.Controls.Add(chkBx_ObjDetect1);
            panelControls.Controls.Add(lbl_xyi);
            panelControls.Controls.Add(lbl_SegTest);
            panelControls.Controls.Add(pictureBox_Zoom);
            panelControls.Controls.Add(btn_ExportCurrentImageView);
            panelControls.Controls.Add(txBx_FOV_LR);
            panelControls.Controls.Add(txBx_FOV_UR);
            panelControls.Controls.Add(txBx_FOV_LL);
            panelControls.Controls.Add(Leica_Label);
            panelControls.Controls.Add(label8);
            panelControls.Controls.Add(chkBx_Onwv4);
            panelControls.Controls.Add(chkBx_Onwv3);
            panelControls.Controls.Add(chkBx_Onwv2);
            panelControls.Controls.Add(txBx_Color4);
            panelControls.Controls.Add(txBx_Color3);
            panelControls.Controls.Add(txBx_Color2);
            panelControls.Controls.Add(txBx_Color1);
            panelControls.Controls.Add(chkBx_Onwv1);
            panelControls.Controls.Add(txBx_BrightnessMultiplier4);
            panelControls.Controls.Add(txBx_Wavelength4);
            panelControls.Controls.Add(txBx_BrightnessMultiplier3);
            panelControls.Controls.Add(txBx_Wavelength3);
            panelControls.Controls.Add(txBx_BrightnessMultiplier2);
            panelControls.Controls.Add(txBx_Wavelength2);
            panelControls.Controls.Add(btn_ExportFromList);
            panelControls.Controls.Add(btnUndoImageCheck);
            panelControls.Controls.Add(comboBox_Overlay);
            panelControls.Controls.Add(chkBx_ShowOverlay);
            panelControls.Controls.Add(btnSize300);
            panelControls.Controls.Add(btnSize700);
            panelControls.Controls.Add(label3);
            panelControls.Controls.Add(btn_Size512);
            panelControls.Controls.Add(txBx_FolderPath);
            panelControls.Controls.Add(label13);
            panelControls.Controls.Add(btn_Update);
            panelControls.Controls.Add(chkBx_SquareMode);
            panelControls.Controls.Add(txBx_ImageName);
            panelControls.Controls.Add(btnResetSaveCal);
            panelControls.Controls.Add(txBx_FOV);
            panelControls.Controls.Add(btnResetImageCheck);
            panelControls.Controls.Add(txBx_BrightnessMultiplier1);
            panelControls.Controls.Add(lblImageCheck);
            panelControls.Controls.Add(label1);
            panelControls.Controls.Add(label12);
            panelControls.Controls.Add(label2);
            panelControls.Controls.Add(panel2);
            panelControls.Controls.Add(txBx_Row);
            panelControls.Controls.Add(comboBox_Well);
            panelControls.Controls.Add(txBx_Col);
            panelControls.Controls.Add(btn_SaveCheck);
            panelControls.Controls.Add(label4);
            panelControls.Controls.Add(label11);
            panelControls.Controls.Add(label5);
            panelControls.Controls.Add(radioButton_RecordFocus);
            panelControls.Controls.Add(txBx_Wavelength1);
            panelControls.Controls.Add(radioButton_TestCal);
            panelControls.Controls.Add(label6);
            panelControls.Controls.Add(label_Warning);
            panelControls.Controls.Add(labl_ImageName);
            panelControls.Controls.Add(label9);
            panelControls.Controls.Add(btn_SaveCalibration);
            panelControls.Controls.Add(txBx_SavedList);
            panelControls.Controls.Add(panel1);
            panelControls.Controls.Add(txBx_RaftID);
            panelControls.Controls.Add(label_Notify);
            panelControls.Controls.Add(label7);
            panelControls.Controls.Add(txBx_LocationInfo);
            panelControls.Location=new System.Drawing.Point(821, 1);
            panelControls.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            panelControls.Name="panelControls";
            panelControls.Size=new System.Drawing.Size(519, 1028);
            panelControls.TabIndex=50;
            // 
            // txBx_MoveToTimePoint
            // 
            txBx_MoveToTimePoint.Enabled=false;
            txBx_MoveToTimePoint.Location=new System.Drawing.Point(384, 422);
            txBx_MoveToTimePoint.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_MoveToTimePoint.Name="txBx_MoveToTimePoint";
            txBx_MoveToTimePoint.Size=new System.Drawing.Size(27, 27);
            txBx_MoveToTimePoint.TabIndex=129;
            txBx_MoveToTimePoint.Text="1";
            txBx_MoveToTimePoint.Visible=false;
            txBx_MoveToTimePoint.TextChanged+=txBx_MoveToTimePoint_TextChanged;
            // 
            // btn_SlideShow
            // 
            btn_SlideShow.Enabled=false;
            btn_SlideShow.Location=new System.Drawing.Point(419, 419);
            btn_SlideShow.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_SlideShow.Name="btn_SlideShow";
            btn_SlideShow.Size=new System.Drawing.Size(61, 31);
            btn_SlideShow.TabIndex=128;
            btn_SlideShow.Text="SShow";
            btn_SlideShow.UseVisualStyleBackColor=true;
            btn_SlideShow.Visible=false;
            btn_SlideShow.Click+=btn_SlideShow_click;
            // 
            // TimeLabel
            // 
            TimeLabel.AutoSize=true;
            TimeLabel.Enabled=false;
            TimeLabel.Visible=false;
            TimeLabel.Font=new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            TimeLabel.Location=new System.Drawing.Point(278, 432);
            TimeLabel.Margin=new System.Windows.Forms.Padding(0);
            TimeLabel.Name="TimeLabel";
            TimeLabel.Size=new System.Drawing.Size(105, 19);
            TimeLabel.TabIndex=127;
            TimeLabel.Text="Time Nav Series";
            TimeLabel.Click+=TimeLabel_Click;
            // 
            // btn_TimeSeriesBck
            // 
            btn_TimeSeriesBck.Enabled=false;
            btn_TimeSeriesBck.Location=new System.Drawing.Point(282, 467);
            btn_TimeSeriesBck.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_TimeSeriesBck.Name="btn_TimeSeriesBck";
            btn_TimeSeriesBck.Size=new System.Drawing.Size(42, 31);
            btn_TimeSeriesBck.TabIndex=36;
            btn_TimeSeriesBck.Text="<";
            btn_TimeSeriesBck.UseVisualStyleBackColor=true;
            btn_TimeSeriesBck.Visible=false;
            btn_TimeSeriesBck.Click+=TimeSeriesBck_Click;
            // 
            // btn_TimeSeriesFwd
            // 
            btn_TimeSeriesFwd.Enabled=false;
            btn_TimeSeriesFwd.Location=new System.Drawing.Point(326, 467);
            btn_TimeSeriesFwd.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_TimeSeriesFwd.Name="btn_TimeSeriesFwd";
            btn_TimeSeriesFwd.Size=new System.Drawing.Size(40, 31);
            btn_TimeSeriesFwd.TabIndex=37;
            btn_TimeSeriesFwd.Text=">";
            btn_TimeSeriesFwd.UseVisualStyleBackColor=true;
            btn_TimeSeriesFwd.Visible=false;
            btn_TimeSeriesFwd.Click+=TimeSeriesFwd_Click;
            // 
            // txBx_ClipExpand_Neg
            // 
            txBx_ClipExpand_Neg.Enabled=false;
            txBx_ClipExpand_Neg.Location=new System.Drawing.Point(465, 349);
            txBx_ClipExpand_Neg.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_ClipExpand_Neg.Name="txBx_ClipExpand_Neg";
            txBx_ClipExpand_Neg.Size=new System.Drawing.Size(31, 27);
            txBx_ClipExpand_Neg.TabIndex=126;
            txBx_ClipExpand_Neg.Text="0";
            toolTip1.SetToolTip(txBx_ClipExpand_Neg, "When Clip is turned on, this sets how far to expand the thresholded area before applying the clip");
            // 
            // txBx_ClipExpand_Pos
            // 
            txBx_ClipExpand_Pos.Enabled=false;
            txBx_ClipExpand_Pos.Location=new System.Drawing.Point(429, 349);
            txBx_ClipExpand_Pos.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_ClipExpand_Pos.Name="txBx_ClipExpand_Pos";
            txBx_ClipExpand_Pos.Size=new System.Drawing.Size(31, 27);
            txBx_ClipExpand_Pos.TabIndex=125;
            txBx_ClipExpand_Pos.Text="0";
            toolTip1.SetToolTip(txBx_ClipExpand_Pos, "When Clip is turned on, this sets how far to expand the thresholded area before applying the clip");
            // 
            // chk_Bx_ObjClipsVis
            // 
            chk_Bx_ObjClipsVis.AutoSize=true;
            chk_Bx_ObjClipsVis.Location=new System.Drawing.Point(328, 352);
            chk_Bx_ObjClipsVis.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chk_Bx_ObjClipsVis.Name="chk_Bx_ObjClipsVis";
            chk_Bx_ObjClipsVis.Size=new System.Drawing.Size(106, 24);
            chk_Bx_ObjClipsVis.TabIndex=124;
            chk_Bx_ObjClipsVis.Text="Obj clip Vis";
            toolTip1.SetToolTip(chk_Bx_ObjClipsVis, resources.GetString("chk_Bx_ObjClipsVis.ToolTip"));
            chk_Bx_ObjClipsVis.UseVisualStyleBackColor=true;
            chk_Bx_ObjClipsVis.CheckedChanged+=chk_Bx_ObjClipsVis_CheckedChanged;
            // 
            // txBx_Color_Overlay
            // 
            txBx_Color_Overlay.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color_Overlay.Location=new System.Drawing.Point(272, 315);
            txBx_Color_Overlay.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color_Overlay.Name="txBx_Color_Overlay";
            txBx_Color_Overlay.Size=new System.Drawing.Size(57, 20);
            txBx_Color_Overlay.TabIndex=123;
            txBx_Color_Overlay.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color_Overlay, "Use this to define how this channel's color looks. ");
            txBx_Color_Overlay.TextChanged+=txBx_Color_TextChanged;
            // 
            // comboBox_ColorMerge
            // 
            comboBox_ColorMerge.DropDownStyle=System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox_ColorMerge.Font=new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            comboBox_ColorMerge.FormattingEnabled=true;
            comboBox_ColorMerge.Location=new System.Drawing.Point(337, 313);
            comboBox_ColorMerge.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            comboBox_ColorMerge.Name="comboBox_ColorMerge";
            comboBox_ColorMerge.Size=new System.Drawing.Size(57, 20);
            comboBox_ColorMerge.TabIndex=122;
            toolTip1.SetToolTip(comboBox_ColorMerge, "These options define how the colors above are merged together.  Add is the default.  Difference is the older way we did this.");
            // 
            // chkBx_ShowCalGrid
            // 
            chkBx_ShowCalGrid.AutoSize=true;
            chkBx_ShowCalGrid.Location=new System.Drawing.Point(294, 825);
            chkBx_ShowCalGrid.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ShowCalGrid.Name="chkBx_ShowCalGrid";
            chkBx_ShowCalGrid.Size=new System.Drawing.Size(59, 24);
            chkBx_ShowCalGrid.TabIndex=121;
            chkBx_ShowCalGrid.Text="Grid";
            toolTip1.SetToolTip(chkBx_ShowCalGrid, "Square Mode - Assumes that the layout of the fields is in a uniform (usually rectangular) pattern");
            chkBx_ShowCalGrid.UseVisualStyleBackColor=true;
            chkBx_ShowCalGrid.CheckedChanged+=chkBx_ShowCalGrid_CheckedChanged;
            // 
            // btnJumpNextCalPoint
            // 
            btnJumpNextCalPoint.Location=new System.Drawing.Point(13, 884);
            btnJumpNextCalPoint.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnJumpNextCalPoint.Name="btnJumpNextCalPoint";
            btnJumpNextCalPoint.Size=new System.Drawing.Size(73, 33);
            btnJumpNextCalPoint.TabIndex=120;
            btnJumpNextCalPoint.Text="next Cal";
            btnJumpNextCalPoint.UseVisualStyleBackColor=true;
            btnJumpNextCalPoint.Click+=btnJumpNextCalPoint_Click;
            // 
            // label19
            // 
            label19.AutoSize=true;
            label19.Location=new System.Drawing.Point(422, 792);
            label19.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label19.Name="label19";
            label19.Size=new System.Drawing.Size(100, 40);
            label19.TabIndex=119;
            label19.Text="x = field bad\r\nz = reset field";
            // 
            // label18
            // 
            label18.AutoSize=true;
            label18.Location=new System.Drawing.Point(440, 80);
            label18.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label18.Name="label18";
            label18.Size=new System.Drawing.Size(24, 20);
            label18.TabIndex=118;
            label18.Text="DI";
            toolTip1.SetToolTip(label18, "Click here to preview the degeneration index (DI) settings. Make sure just one of the DI boxes is checked and the \"Obj clip Vis\" below is unchecked.");
            label18.Click+=label_DegenIndex_Click;
            // 
            // chkBx_DI_5
            // 
            chkBx_DI_5.AutoSize=true;
            chkBx_DI_5.Location=new System.Drawing.Point(443, 268);
            chkBx_DI_5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_DI_5.Name="chkBx_DI_5";
            chkBx_DI_5.Size=new System.Drawing.Size(18, 17);
            chkBx_DI_5.TabIndex=117;
            chkBx_DI_5.UseVisualStyleBackColor=true;
            // 
            // chkBx_DI_4
            // 
            chkBx_DI_4.AutoSize=true;
            chkBx_DI_4.Location=new System.Drawing.Point(443, 228);
            chkBx_DI_4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_DI_4.Name="chkBx_DI_4";
            chkBx_DI_4.Size=new System.Drawing.Size(18, 17);
            chkBx_DI_4.TabIndex=116;
            chkBx_DI_4.UseVisualStyleBackColor=true;
            // 
            // chkBx_DI_3
            // 
            chkBx_DI_3.AutoSize=true;
            chkBx_DI_3.Location=new System.Drawing.Point(443, 188);
            chkBx_DI_3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_DI_3.Name="chkBx_DI_3";
            chkBx_DI_3.Size=new System.Drawing.Size(18, 17);
            chkBx_DI_3.TabIndex=115;
            chkBx_DI_3.UseVisualStyleBackColor=true;
            // 
            // chkBx_DI_2
            // 
            chkBx_DI_2.AutoSize=true;
            chkBx_DI_2.Location=new System.Drawing.Point(443, 148);
            chkBx_DI_2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_DI_2.Name="chkBx_DI_2";
            chkBx_DI_2.Size=new System.Drawing.Size(18, 17);
            chkBx_DI_2.TabIndex=114;
            chkBx_DI_2.UseVisualStyleBackColor=true;
            // 
            // chkBx_DI_1
            // 
            chkBx_DI_1.AutoSize=true;
            chkBx_DI_1.Location=new System.Drawing.Point(443, 109);
            chkBx_DI_1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_DI_1.Name="chkBx_DI_1";
            chkBx_DI_1.Size=new System.Drawing.Size(18, 17);
            chkBx_DI_1.TabIndex=113;
            chkBx_DI_1.UseVisualStyleBackColor=true;
            // 
            // btnRunSegmentation
            // 
            btnRunSegmentation.Location=new System.Drawing.Point(376, 452);
            btnRunSegmentation.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnRunSegmentation.Name="btnRunSegmentation";
            btnRunSegmentation.Size=new System.Drawing.Size(129, 44);
            btnRunSegmentation.TabIndex=112;
            btnRunSegmentation.Text="Segmentation";
            toolTip1.SetToolTip(btnRunSegmentation, "Right click for settings, Left click to Run Segmentation and Export images.  Double click Thresh first to test your object detection and get object area ranges.");
            btnRunSegmentation.UseVisualStyleBackColor=true;
            btnRunSegmentation.MouseUp+=btnRunSegmentation_MouseUp;
            // 
            // txBx_Wv_Thresh5
            // 
            txBx_Wv_Thresh5.Location=new System.Drawing.Point(465, 264);
            txBx_Wv_Thresh5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Thresh5.Name="txBx_Wv_Thresh5";
            txBx_Wv_Thresh5.Size=new System.Drawing.Size(45, 27);
            txBx_Wv_Thresh5.TabIndex=111;
            toolTip1.SetToolTip(txBx_Wv_Thresh5, "Double click to preview");
            txBx_Wv_Thresh5.DoubleClick+=txBx_Wv_Thresh_DoubleClick;
            // 
            // txBx_Wv_Thresh4
            // 
            txBx_Wv_Thresh4.Location=new System.Drawing.Point(465, 224);
            txBx_Wv_Thresh4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Thresh4.Name="txBx_Wv_Thresh4";
            txBx_Wv_Thresh4.Size=new System.Drawing.Size(45, 27);
            txBx_Wv_Thresh4.TabIndex=110;
            toolTip1.SetToolTip(txBx_Wv_Thresh4, "Double click to preview");
            txBx_Wv_Thresh4.DoubleClick+=txBx_Wv_Thresh_DoubleClick;
            // 
            // txBx_Wv_Thresh3
            // 
            txBx_Wv_Thresh3.Location=new System.Drawing.Point(465, 184);
            txBx_Wv_Thresh3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Thresh3.Name="txBx_Wv_Thresh3";
            txBx_Wv_Thresh3.Size=new System.Drawing.Size(45, 27);
            txBx_Wv_Thresh3.TabIndex=109;
            toolTip1.SetToolTip(txBx_Wv_Thresh3, "Double click to preview");
            txBx_Wv_Thresh3.DoubleClick+=txBx_Wv_Thresh_DoubleClick;
            // 
            // txBx_Wv_Thresh2
            // 
            txBx_Wv_Thresh2.Location=new System.Drawing.Point(465, 144);
            txBx_Wv_Thresh2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Thresh2.Name="txBx_Wv_Thresh2";
            txBx_Wv_Thresh2.Size=new System.Drawing.Size(45, 27);
            txBx_Wv_Thresh2.TabIndex=108;
            toolTip1.SetToolTip(txBx_Wv_Thresh2, "Double click to preview");
            txBx_Wv_Thresh2.DoubleClick+=txBx_Wv_Thresh_DoubleClick;
            // 
            // txBx_Wv_Thresh1
            // 
            txBx_Wv_Thresh1.Location=new System.Drawing.Point(465, 104);
            txBx_Wv_Thresh1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Thresh1.Name="txBx_Wv_Thresh1";
            txBx_Wv_Thresh1.Size=new System.Drawing.Size(45, 27);
            txBx_Wv_Thresh1.TabIndex=106;
            toolTip1.SetToolTip(txBx_Wv_Thresh1, "Double click to preview");
            txBx_Wv_Thresh1.DoubleClick+=txBx_Wv_Thresh_DoubleClick;
            // 
            // label17
            // 
            label17.AutoSize=true;
            label17.Location=new System.Drawing.Point(459, 80);
            label17.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label17.Name="label17";
            label17.Size=new System.Drawing.Size(52, 20);
            label17.TabIndex=107;
            label17.Text="Thresh";
            toolTip1.SetToolTip(label17, "Only relevant for the specific setting you have on brightness, double click to preview");
            // 
            // btn_SaveWavelengthInfo
            // 
            btn_SaveWavelengthInfo.Font=new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_SaveWavelengthInfo.Location=new System.Drawing.Point(5, 264);
            btn_SaveWavelengthInfo.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_SaveWavelengthInfo.Name="btn_SaveWavelengthInfo";
            btn_SaveWavelengthInfo.Size=new System.Drawing.Size(80, 33);
            btn_SaveWavelengthInfo.TabIndex=105;
            btn_SaveWavelengthInfo.Text="Save";
            toolTip1.SetToolTip(btn_SaveWavelengthInfo, "Save the newly entered Wavelength Name and Abbreviations");
            btn_SaveWavelengthInfo.UseVisualStyleBackColor=true;
            btn_SaveWavelengthInfo.Click+=btn_SaveWavelengthInfo_Click;
            // 
            // txBx_WavelengthExtended
            // 
            txBx_WavelengthExtended.Font=new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_WavelengthExtended.Location=new System.Drawing.Point(5, 80);
            txBx_WavelengthExtended.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_WavelengthExtended.Multiline=true;
            txBx_WavelengthExtended.Name="txBx_WavelengthExtended";
            txBx_WavelengthExtended.Size=new System.Drawing.Size(78, 173);
            txBx_WavelengthExtended.TabIndex=104;
            // 
            // label16
            // 
            label16.AutoSize=true;
            label16.Location=new System.Drawing.Point(87, 80);
            label16.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label16.Name="label16";
            label16.Size=new System.Drawing.Size(57, 20);
            label16.TabIndex=103;
            label16.Text="Abbrev";
            toolTip1.SetToolTip(label16, "This must be set to use Rename Norm");
            // 
            // label15
            // 
            label15.AutoSize=true;
            label15.Location=new System.Drawing.Point(150, 80);
            label15.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label15.Name="label15";
            label15.Size=new System.Drawing.Size(49, 20);
            label15.TabIndex=102;
            label15.Text="Name";
            // 
            // txBx_Wv_Abbrev5
            // 
            txBx_Wv_Abbrev5.Location=new System.Drawing.Point(89, 264);
            txBx_Wv_Abbrev5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Abbrev5.Name="txBx_Wv_Abbrev5";
            txBx_Wv_Abbrev5.Size=new System.Drawing.Size(31, 27);
            txBx_Wv_Abbrev5.TabIndex=101;
            // 
            // txBx_Wv_Name5
            // 
            txBx_Wv_Name5.Location=new System.Drawing.Point(127, 264);
            txBx_Wv_Name5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Name5.Name="txBx_Wv_Name5";
            txBx_Wv_Name5.Size=new System.Drawing.Size(105, 27);
            txBx_Wv_Name5.TabIndex=100;
            // 
            // txBx_Wv_Abbrev4
            // 
            txBx_Wv_Abbrev4.Location=new System.Drawing.Point(89, 224);
            txBx_Wv_Abbrev4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Abbrev4.Name="txBx_Wv_Abbrev4";
            txBx_Wv_Abbrev4.Size=new System.Drawing.Size(31, 27);
            txBx_Wv_Abbrev4.TabIndex=99;
            // 
            // txBx_Wv_Name4
            // 
            txBx_Wv_Name4.Location=new System.Drawing.Point(127, 224);
            txBx_Wv_Name4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Name4.Name="txBx_Wv_Name4";
            txBx_Wv_Name4.Size=new System.Drawing.Size(105, 27);
            txBx_Wv_Name4.TabIndex=98;
            // 
            // txBx_Wv_Abbrev3
            // 
            txBx_Wv_Abbrev3.Location=new System.Drawing.Point(89, 184);
            txBx_Wv_Abbrev3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Abbrev3.Name="txBx_Wv_Abbrev3";
            txBx_Wv_Abbrev3.Size=new System.Drawing.Size(31, 27);
            txBx_Wv_Abbrev3.TabIndex=97;
            // 
            // txBx_Wv_Name3
            // 
            txBx_Wv_Name3.Location=new System.Drawing.Point(127, 184);
            txBx_Wv_Name3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Name3.Name="txBx_Wv_Name3";
            txBx_Wv_Name3.Size=new System.Drawing.Size(105, 27);
            txBx_Wv_Name3.TabIndex=96;
            // 
            // txBx_Wv_Abbrev2
            // 
            txBx_Wv_Abbrev2.Location=new System.Drawing.Point(89, 144);
            txBx_Wv_Abbrev2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Abbrev2.Name="txBx_Wv_Abbrev2";
            txBx_Wv_Abbrev2.Size=new System.Drawing.Size(31, 27);
            txBx_Wv_Abbrev2.TabIndex=95;
            // 
            // txBx_Wv_Name2
            // 
            txBx_Wv_Name2.Location=new System.Drawing.Point(127, 144);
            txBx_Wv_Name2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Name2.Name="txBx_Wv_Name2";
            txBx_Wv_Name2.Size=new System.Drawing.Size(105, 27);
            txBx_Wv_Name2.TabIndex=94;
            // 
            // txBx_Wv_Abbrev1
            // 
            txBx_Wv_Abbrev1.Location=new System.Drawing.Point(89, 104);
            txBx_Wv_Abbrev1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Abbrev1.Name="txBx_Wv_Abbrev1";
            txBx_Wv_Abbrev1.Size=new System.Drawing.Size(31, 27);
            txBx_Wv_Abbrev1.TabIndex=93;
            // 
            // txBx_Wv_Name1
            // 
            txBx_Wv_Name1.Location=new System.Drawing.Point(127, 104);
            txBx_Wv_Name1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wv_Name1.Name="txBx_Wv_Name1";
            txBx_Wv_Name1.Size=new System.Drawing.Size(105, 27);
            txBx_Wv_Name1.TabIndex=92;
            toolTip1.SetToolTip(txBx_Wv_Name1, resources.GetString("txBx_Wv_Name1.ToolTip"));
            // 
            // chkBx_ObjDetect5
            // 
            chkBx_ObjDetect5.AutoSize=true;
            chkBx_ObjDetect5.Location=new System.Drawing.Point(421, 268);
            chkBx_ObjDetect5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ObjDetect5.Name="chkBx_ObjDetect5";
            chkBx_ObjDetect5.Size=new System.Drawing.Size(18, 17);
            chkBx_ObjDetect5.TabIndex=91;
            chkBx_ObjDetect5.UseVisualStyleBackColor=true;
            // 
            // chkBx_Onwv5
            // 
            chkBx_Onwv5.AutoSize=true;
            chkBx_Onwv5.Checked=true;
            chkBx_Onwv5.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_Onwv5.Location=new System.Drawing.Point(395, 268);
            chkBx_Onwv5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_Onwv5.Name="chkBx_Onwv5";
            chkBx_Onwv5.Size=new System.Drawing.Size(18, 17);
            chkBx_Onwv5.TabIndex=90;
            chkBx_Onwv5.UseVisualStyleBackColor=true;
            chkBx_Onwv5.CheckedChanged+=chkBx_Onwv5_CheckedChanged;
            // 
            // txBx_Color5
            // 
            txBx_Color5.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color5.Location=new System.Drawing.Point(334, 267);
            txBx_Color5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color5.Name="txBx_Color5";
            txBx_Color5.Size=new System.Drawing.Size(57, 20);
            txBx_Color5.TabIndex=89;
            txBx_Color5.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color5, "Use this to define how this channel's color looks. ");
            txBx_Color5.TextChanged+=txBx_Color_TextChanged;
            // 
            // txBx_BrightnessMultiplier5
            // 
            txBx_BrightnessMultiplier5.Location=new System.Drawing.Point(274, 264);
            txBx_BrightnessMultiplier5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_BrightnessMultiplier5.Name="txBx_BrightnessMultiplier5";
            txBx_BrightnessMultiplier5.Size=new System.Drawing.Size(52, 27);
            txBx_BrightnessMultiplier5.TabIndex=87;
            // 
            // txBx_Wavelength5
            // 
            txBx_Wavelength5.Location=new System.Drawing.Point(235, 264);
            txBx_Wavelength5.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wavelength5.Name="txBx_Wavelength5";
            txBx_Wavelength5.Size=new System.Drawing.Size(31, 27);
            txBx_Wavelength5.TabIndex=88;
            // 
            // label14
            // 
            label14.AutoSize=true;
            label14.Location=new System.Drawing.Point(415, 80);
            label14.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label14.Name="label14";
            label14.Size=new System.Drawing.Size(33, 20);
            label14.TabIndex=86;
            label14.Text="Obj";
            // 
            // label10
            // 
            label10.AutoSize=true;
            label10.Location=new System.Drawing.Point(389, 80);
            label10.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label10.Name="label10";
            label10.Size=new System.Drawing.Size(28, 20);
            label10.TabIndex=85;
            label10.Text="Vis";
            // 
            // chkBx_ObjDetect4
            // 
            chkBx_ObjDetect4.AutoSize=true;
            chkBx_ObjDetect4.Location=new System.Drawing.Point(421, 228);
            chkBx_ObjDetect4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ObjDetect4.Name="chkBx_ObjDetect4";
            chkBx_ObjDetect4.Size=new System.Drawing.Size(18, 17);
            chkBx_ObjDetect4.TabIndex=84;
            chkBx_ObjDetect4.UseVisualStyleBackColor=true;
            // 
            // chkBx_ObjDetect3
            // 
            chkBx_ObjDetect3.AutoSize=true;
            chkBx_ObjDetect3.Location=new System.Drawing.Point(421, 188);
            chkBx_ObjDetect3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ObjDetect3.Name="chkBx_ObjDetect3";
            chkBx_ObjDetect3.Size=new System.Drawing.Size(18, 17);
            chkBx_ObjDetect3.TabIndex=83;
            chkBx_ObjDetect3.UseVisualStyleBackColor=true;
            // 
            // chkBx_ObjDetect2
            // 
            chkBx_ObjDetect2.AutoSize=true;
            chkBx_ObjDetect2.Location=new System.Drawing.Point(421, 148);
            chkBx_ObjDetect2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ObjDetect2.Name="chkBx_ObjDetect2";
            chkBx_ObjDetect2.Size=new System.Drawing.Size(18, 17);
            chkBx_ObjDetect2.TabIndex=82;
            chkBx_ObjDetect2.UseVisualStyleBackColor=true;
            // 
            // chkBx_ObjDetect1
            // 
            chkBx_ObjDetect1.AutoSize=true;
            chkBx_ObjDetect1.Location=new System.Drawing.Point(421, 109);
            chkBx_ObjDetect1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ObjDetect1.Name="chkBx_ObjDetect1";
            chkBx_ObjDetect1.Size=new System.Drawing.Size(18, 17);
            chkBx_ObjDetect1.TabIndex=81;
            chkBx_ObjDetect1.UseVisualStyleBackColor=true;
            // 
            // lbl_xyi
            // 
            lbl_xyi.AutoSize=true;
            lbl_xyi.Location=new System.Drawing.Point(285, 411);
            lbl_xyi.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            lbl_xyi.Name="lbl_xyi";
            lbl_xyi.Size=new System.Drawing.Size(33, 20);
            lbl_xyi.TabIndex=80;
            lbl_xyi.Text="x,y,i";
            // 
            // lbl_SegTest
            // 
            lbl_SegTest.AutoSize=true;
            lbl_SegTest.ForeColor=System.Drawing.Color.RoyalBlue;
            lbl_SegTest.Location=new System.Drawing.Point(418, 936);
            lbl_SegTest.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            lbl_SegTest.Name="lbl_SegTest";
            lbl_SegTest.Size=new System.Drawing.Size(65, 20);
            lbl_SegTest.TabIndex=79;
            lbl_SegTest.Text="Dev Test";
            // 
            // pictureBox_Zoom
            // 
            pictureBox_Zoom.Location=new System.Drawing.Point(5, 411);
            pictureBox_Zoom.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            pictureBox_Zoom.Name="pictureBox_Zoom";
            pictureBox_Zoom.Size=new System.Drawing.Size(273, 276);
            pictureBox_Zoom.TabIndex=78;
            pictureBox_Zoom.TabStop=false;
            pictureBox_Zoom.Paint+=pictureBox_Zoom_Paint;
            // 
            // btn_ExportCurrentImageView
            // 
            btn_ExportCurrentImageView.Font=new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_ExportCurrentImageView.Location=new System.Drawing.Point(326, 7);
            btn_ExportCurrentImageView.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_ExportCurrentImageView.Name="btn_ExportCurrentImageView";
            btn_ExportCurrentImageView.Size=new System.Drawing.Size(102, 33);
            btn_ExportCurrentImageView.TabIndex=77;
            btn_ExportCurrentImageView.Text="Save IMGs*";
            toolTip1.SetToolTip(btn_ExportCurrentImageView, "Export (stitched) the set of images. It will save in the default location and show that location below after saving. ");
            btn_ExportCurrentImageView.UseVisualStyleBackColor=true;
            btn_ExportCurrentImageView.MouseUp+=btn_ExportCurrentImageView_MouseUp;
            // 
            // txBx_FOV_LR
            // 
            txBx_FOV_LR.Enabled=false;
            txBx_FOV_LR.Location=new System.Drawing.Point(55, 987);
            txBx_FOV_LR.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_FOV_LR.Name="txBx_FOV_LR";
            txBx_FOV_LR.Size=new System.Drawing.Size(37, 27);
            txBx_FOV_LR.TabIndex=76;
            // 
            // txBx_FOV_UR
            // 
            txBx_FOV_UR.Enabled=false;
            txBx_FOV_UR.Location=new System.Drawing.Point(55, 947);
            txBx_FOV_UR.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_FOV_UR.Name="txBx_FOV_UR";
            txBx_FOV_UR.Size=new System.Drawing.Size(37, 27);
            txBx_FOV_UR.TabIndex=75;
            // 
            // txBx_FOV_LL
            // 
            txBx_FOV_LL.Enabled=false;
            txBx_FOV_LL.Location=new System.Drawing.Point(10, 987);
            txBx_FOV_LL.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_FOV_LL.Name="txBx_FOV_LL";
            txBx_FOV_LL.Size=new System.Drawing.Size(37, 27);
            txBx_FOV_LL.TabIndex=74;
            // 
            // Leica_Label
            // 
            Leica_Label.AutoSize=true;
            Leica_Label.ForeColor=System.Drawing.Color.RoyalBlue;
            Leica_Label.Location=new System.Drawing.Point(295, 936);
            Leica_Label.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            Leica_Label.Name="Leica_Label";
            Leica_Label.Size=new System.Drawing.Size(69, 20);
            Leica_Label.TabIndex=73;
            Leica_Label.Text="Dev Only";
            Leica_Label.Click+=Leica_Label_Click;
            // 
            // label8
            // 
            label8.AutoSize=true;
            label8.Location=new System.Drawing.Point(341, 80);
            label8.Margin=new System.Windows.Forms.Padding(5, 0, 5, 0);
            label8.Name="label8";
            label8.Size=new System.Drawing.Size(45, 20);
            label8.TabIndex=72;
            label8.Text="Color";
            toolTip1.SetToolTip(label8, "Use this to define how this channel's color looks. \r\nThis is a Hex RGB value. \r\nWhite = FFFFFF\r\nRed = FF0000\r\nGreen = 00FF00\r\nBlue = 0000FF\r\nYellow = FFFF00\r\nCyan =  00FFFF\r\nMagenta = FF00FF");
            label8.Click+=label_Color_Click;
            // 
            // chkBx_Onwv4
            // 
            chkBx_Onwv4.AutoSize=true;
            chkBx_Onwv4.Checked=true;
            chkBx_Onwv4.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_Onwv4.Location=new System.Drawing.Point(395, 228);
            chkBx_Onwv4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_Onwv4.Name="chkBx_Onwv4";
            chkBx_Onwv4.Size=new System.Drawing.Size(18, 17);
            chkBx_Onwv4.TabIndex=71;
            chkBx_Onwv4.UseVisualStyleBackColor=true;
            chkBx_Onwv4.CheckedChanged+=chkBx_Onwv4_CheckedChanged;
            // 
            // chkBx_Onwv3
            // 
            chkBx_Onwv3.AutoSize=true;
            chkBx_Onwv3.Checked=true;
            chkBx_Onwv3.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_Onwv3.Location=new System.Drawing.Point(395, 188);
            chkBx_Onwv3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_Onwv3.Name="chkBx_Onwv3";
            chkBx_Onwv3.Size=new System.Drawing.Size(18, 17);
            chkBx_Onwv3.TabIndex=70;
            chkBx_Onwv3.UseVisualStyleBackColor=true;
            chkBx_Onwv3.CheckedChanged+=chkBx_Onwv3_CheckedChanged;
            // 
            // chkBx_Onwv2
            // 
            chkBx_Onwv2.AutoSize=true;
            chkBx_Onwv2.Checked=true;
            chkBx_Onwv2.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_Onwv2.Location=new System.Drawing.Point(395, 148);
            chkBx_Onwv2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_Onwv2.Name="chkBx_Onwv2";
            chkBx_Onwv2.Size=new System.Drawing.Size(18, 17);
            chkBx_Onwv2.TabIndex=69;
            chkBx_Onwv2.UseVisualStyleBackColor=true;
            chkBx_Onwv2.CheckedChanged+=chkBx_Onwv2_CheckedChanged;
            // 
            // txBx_Color4
            // 
            txBx_Color4.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color4.Location=new System.Drawing.Point(334, 227);
            txBx_Color4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color4.Name="txBx_Color4";
            txBx_Color4.Size=new System.Drawing.Size(57, 20);
            txBx_Color4.TabIndex=68;
            txBx_Color4.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color4, "Use this to define how this channel's color looks. ");
            txBx_Color4.TextChanged+=txBx_Color_TextChanged;
            // 
            // txBx_Color3
            // 
            txBx_Color3.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color3.Location=new System.Drawing.Point(334, 187);
            txBx_Color3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color3.Name="txBx_Color3";
            txBx_Color3.Size=new System.Drawing.Size(57, 20);
            txBx_Color3.TabIndex=67;
            txBx_Color3.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color3, "Use this to define how this channel's color looks. ");
            txBx_Color3.TextChanged+=txBx_Color_TextChanged;
            // 
            // txBx_Color2
            // 
            txBx_Color2.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color2.Location=new System.Drawing.Point(334, 147);
            txBx_Color2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color2.Name="txBx_Color2";
            txBx_Color2.Size=new System.Drawing.Size(57, 20);
            txBx_Color2.TabIndex=66;
            txBx_Color2.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color2, "Use this to define how this channel's color looks. ");
            txBx_Color2.TextChanged+=txBx_Color_TextChanged;
            // 
            // txBx_Color1
            // 
            txBx_Color1.Font=new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            txBx_Color1.Location=new System.Drawing.Point(334, 107);
            txBx_Color1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Color1.Name="txBx_Color1";
            txBx_Color1.Size=new System.Drawing.Size(57, 20);
            txBx_Color1.TabIndex=65;
            txBx_Color1.Text="FFFFFF";
            toolTip1.SetToolTip(txBx_Color1, "Use this to define how this channel's color looks. ");
            txBx_Color1.TextChanged+=txBx_Color_TextChanged;
            // 
            // chkBx_Onwv1
            // 
            chkBx_Onwv1.AutoSize=true;
            chkBx_Onwv1.Checked=true;
            chkBx_Onwv1.CheckState=System.Windows.Forms.CheckState.Checked;
            chkBx_Onwv1.Location=new System.Drawing.Point(395, 109);
            chkBx_Onwv1.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_Onwv1.Name="chkBx_Onwv1";
            chkBx_Onwv1.Size=new System.Drawing.Size(18, 17);
            chkBx_Onwv1.TabIndex=64;
            chkBx_Onwv1.UseVisualStyleBackColor=true;
            chkBx_Onwv1.CheckedChanged+=chkBx_Onwv1_CheckedChanged;
            // 
            // txBx_BrightnessMultiplier4
            // 
            txBx_BrightnessMultiplier4.Location=new System.Drawing.Point(274, 224);
            txBx_BrightnessMultiplier4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_BrightnessMultiplier4.Name="txBx_BrightnessMultiplier4";
            txBx_BrightnessMultiplier4.Size=new System.Drawing.Size(52, 27);
            txBx_BrightnessMultiplier4.TabIndex=62;
            // 
            // txBx_Wavelength4
            // 
            txBx_Wavelength4.Location=new System.Drawing.Point(235, 224);
            txBx_Wavelength4.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wavelength4.Name="txBx_Wavelength4";
            txBx_Wavelength4.Size=new System.Drawing.Size(31, 27);
            txBx_Wavelength4.TabIndex=63;
            // 
            // txBx_BrightnessMultiplier3
            // 
            txBx_BrightnessMultiplier3.Location=new System.Drawing.Point(274, 184);
            txBx_BrightnessMultiplier3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_BrightnessMultiplier3.Name="txBx_BrightnessMultiplier3";
            txBx_BrightnessMultiplier3.Size=new System.Drawing.Size(52, 27);
            txBx_BrightnessMultiplier3.TabIndex=60;
            // 
            // txBx_Wavelength3
            // 
            txBx_Wavelength3.Location=new System.Drawing.Point(235, 184);
            txBx_Wavelength3.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wavelength3.Name="txBx_Wavelength3";
            txBx_Wavelength3.Size=new System.Drawing.Size(31, 27);
            txBx_Wavelength3.TabIndex=61;
            // 
            // txBx_BrightnessMultiplier2
            // 
            txBx_BrightnessMultiplier2.Location=new System.Drawing.Point(274, 144);
            txBx_BrightnessMultiplier2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_BrightnessMultiplier2.Name="txBx_BrightnessMultiplier2";
            txBx_BrightnessMultiplier2.Size=new System.Drawing.Size(52, 27);
            txBx_BrightnessMultiplier2.TabIndex=58;
            // 
            // txBx_Wavelength2
            // 
            txBx_Wavelength2.Location=new System.Drawing.Point(235, 144);
            txBx_Wavelength2.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            txBx_Wavelength2.Name="txBx_Wavelength2";
            txBx_Wavelength2.Size=new System.Drawing.Size(31, 27);
            txBx_Wavelength2.TabIndex=59;
            // 
            // btn_ExportFromList
            // 
            btn_ExportFromList.Location=new System.Drawing.Point(433, 7);
            btn_ExportFromList.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_ExportFromList.Name="btn_ExportFromList";
            btn_ExportFromList.Size=new System.Drawing.Size(80, 33);
            btn_ExportFromList.TabIndex=57;
            btn_ExportFromList.Text="Exports..";
            toolTip1.SetToolTip(btn_ExportFromList, resources.GetString("btn_ExportFromList.ToolTip"));
            btn_ExportFromList.UseVisualStyleBackColor=true;
            btn_ExportFromList.Click+=btn_ExportFromList_Click;
            // 
            // btnUndoImageCheck
            // 
            btnUndoImageCheck.Location=new System.Drawing.Point(294, 856);
            btnUndoImageCheck.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnUndoImageCheck.Name="btnUndoImageCheck";
            btnUndoImageCheck.Size=new System.Drawing.Size(57, 33);
            btnUndoImageCheck.TabIndex=56;
            btnUndoImageCheck.Text="undo";
            btnUndoImageCheck.UseVisualStyleBackColor=true;
            btnUndoImageCheck.Click+=btnUndoImageCheck_Click;
            // 
            // comboBox_Overlay
            // 
            comboBox_Overlay.DropDownStyle=System.Windows.Forms.ComboBoxStyle.DropDownList;
            comboBox_Overlay.FormattingEnabled=true;
            comboBox_Overlay.Location=new System.Drawing.Point(78, 311);
            comboBox_Overlay.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            comboBox_Overlay.Name="comboBox_Overlay";
            comboBox_Overlay.Size=new System.Drawing.Size(189, 28);
            comboBox_Overlay.TabIndex=54;
            comboBox_Overlay.SelectedIndexChanged+=comboBox_Overlay_SelectedIndexChanged;
            // 
            // chkBx_ShowOverlay
            // 
            chkBx_ShowOverlay.AutoSize=true;
            chkBx_ShowOverlay.Enabled=false;
            chkBx_ShowOverlay.Location=new System.Drawing.Point(5, 315);
            chkBx_ShowOverlay.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            chkBx_ShowOverlay.Name="chkBx_ShowOverlay";
            chkBx_ShowOverlay.Size=new System.Drawing.Size(81, 24);
            chkBx_ShowOverlay.TabIndex=53;
            chkBx_ShowOverlay.Text="Overlay";
            chkBx_ShowOverlay.UseVisualStyleBackColor=true;
            chkBx_ShowOverlay.CheckedChanged+=chkBx_ShowOverlay_CheckedChanged;
            // 
            // btnSize300
            // 
            btnSize300.Font=new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnSize300.Location=new System.Drawing.Point(97, 7);
            btnSize300.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnSize300.Name="btnSize300";
            btnSize300.Size=new System.Drawing.Size(57, 33);
            btnSize300.TabIndex=51;
            btnSize300.Text="300";
            toolTip1.SetToolTip(btnSize300, "Single Image Size 300x300");
            btnSize300.UseVisualStyleBackColor=true;
            btnSize300.Click+=btn_FullSize_Click;
            // 
            // btnSize700
            // 
            btnSize700.Font=new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btnSize700.Location=new System.Drawing.Point(217, 7);
            btnSize700.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btnSize700.Name="btnSize700";
            btnSize700.Size=new System.Drawing.Size(57, 33);
            btnSize700.TabIndex=50;
            btnSize700.Text="700";
            toolTip1.SetToolTip(btnSize700, "Single Image Size 700x700");
            btnSize700.UseVisualStyleBackColor=true;
            btnSize700.Click+=btn_FullSize_Click;
            // 
            // btn_Finish
            // 
            btn_Finish.BackColor=System.Drawing.Color.FromArgb(255, 128, 128);
            btn_Finish.Font=new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            btn_Finish.Location=new System.Drawing.Point(647, 812);
            btn_Finish.Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            btn_Finish.Name="btn_Finish";
            btn_Finish.Size=new System.Drawing.Size(142, 100);
            btn_Finish.TabIndex=77;
            btn_Finish.Text="Finish";
            btn_Finish.UseVisualStyleBackColor=false;
            btn_Finish.Visible=false;
            btn_Finish.Click+=btn_Finish_Click;
            // 
            // bgWorker_ImageLoad
            // 
            bgWorker_ImageLoad.WorkerSupportsCancellation=true;
            bgWorker_ImageLoad.DoWork+=bgWorker_ImageLoad_DoWork;
            bgWorker_ImageLoad.RunWorkerCompleted+=bgWorker_ImageLoad_RunWorkerCompleted;
            // 
            // bgWorker_Analysis1
            // 
            bgWorker_Analysis1.WorkerReportsProgress=true;
            bgWorker_Analysis1.WorkerSupportsCancellation=true;
            bgWorker_Analysis1.DoWork+=bgWorker_Analysis1_DoWork;
            bgWorker_Analysis1.ProgressChanged+=bgWorker_Analysis1_ProgressChanged;
            bgWorker_Analysis1.RunWorkerCompleted+=bgWorker_Analysis1_RunWorkerCompleted;
            // 
            // FormRaftCal
            // 
            AutoScaleDimensions=new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode=System.Windows.Forms.AutoScaleMode.Font;
            ClientSize=new System.Drawing.Size(1342, 1023);
            Controls.Add(btn_Finish);
            Controls.Add(panelControls);
            Controls.Add(pictureBox3);
            Controls.Add(pictureBox4);
            Controls.Add(pictureBox2);
            Controls.Add(pictureBox1);
            KeyPreview=true;
            Margin=new System.Windows.Forms.Padding(5, 4, 5, 4);
            Name="FormRaftCal";
            Text="InCell Image Check (FIV Tools)";
            FormClosing+=FormRaftCal_FormClosing;
            Load+=FormRaftCal_Load;
            Shown+=FormRaftCal_Shown;
            KeyDown+=FormRaftCal_KeyDown;
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            panel1.ResumeLayout(false);
            panelControls.ResumeLayout(false);
            panelControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox_Zoom).EndInit();
            ResumeLayout(false);
        }



        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txBx_FolderPath;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.TextBox txBx_FOV;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txBx_Row;
        private System.Windows.Forms.TextBox txBx_Col;
        private System.Windows.Forms.Button btn_Up;
        private System.Windows.Forms.Button btn_Down;
        private System.Windows.Forms.Button btn_Left;
        private System.Windows.Forms.Button btn_Right;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_Wavelength1;
        private System.Windows.Forms.Button btn_U5;
        private System.Windows.Forms.Button btn_D5;
        private System.Windows.Forms.Button btn_L5;
        private System.Windows.Forms.Button btn_R5;
        private System.Windows.Forms.Label labl_ImageName;
        private System.Windows.Forms.TextBox txBx_SavedList;
        private System.Windows.Forms.TextBox txBx_RaftID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txBx_LocationInfo;
        private System.Windows.Forms.Label label_Notify;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_BR;
        private System.Windows.Forms.Button btn_UL;
        private System.Windows.Forms.Button btn_BL;
        private System.Windows.Forms.Button btn_UR;
        private System.Windows.Forms.Button btn_SaveCalibration;
        private System.Windows.Forms.Label label_Warning;
        private System.Windows.Forms.RadioButton radioButton_TestCal;
        private System.Windows.Forms.RadioButton radioButton_RecordFocus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_SaveCheck;
        private System.Windows.Forms.ComboBox comboBox_Well;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblImageCheck;
        private System.Windows.Forms.Button btnResetImageCheck;
        private System.Windows.Forms.Button btnResetSaveCal;
        private System.Windows.Forms.Button btnFieldBack;
        private System.Windows.Forms.Button btnFieldNext;
        private System.Windows.Forms.Button btn_RaftNext;
        private System.Windows.Forms.Button btn_RaftPrev;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_Size512;
        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.Button btnSize700;
        private System.Windows.Forms.Button btnSize300;
        private System.Windows.Forms.CheckBox chkBx_ShowOverlay;
        private System.Windows.Forms.ComboBox comboBox_Overlay;
        private System.Windows.Forms.Button btnUndoImageCheck;
        private System.Windows.Forms.Button btn_ExportFromList;
        private System.Windows.Forms.Button btnFieldSnake;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkBx_Onwv4;
        private System.Windows.Forms.CheckBox chkBx_Onwv3;
        private System.Windows.Forms.CheckBox chkBx_Onwv2;
        private System.Windows.Forms.TextBox txBx_Color4;
        private System.Windows.Forms.TextBox txBx_Color3;
        private System.Windows.Forms.TextBox txBx_Color2;
        private System.Windows.Forms.TextBox txBx_Color1;
        private System.Windows.Forms.CheckBox chkBx_Onwv1;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier4;
        private System.Windows.Forms.TextBox txBx_Wavelength4;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier3;
        private System.Windows.Forms.TextBox txBx_Wavelength3;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier2;
        private System.Windows.Forms.TextBox txBx_Wavelength2;
        private System.Windows.Forms.Label Leica_Label;
        private System.Windows.Forms.TextBox txBx_FOV_LR;
        private System.Windows.Forms.TextBox txBx_FOV_UR;
        private System.Windows.Forms.TextBox txBx_FOV_LL;
        private System.Windows.Forms.Button btn_Finish;
        private System.Windows.Forms.Button btn_ExportCurrentImageView;
        private System.Windows.Forms.PictureBox pictureBox_Zoom;
        private System.Windows.Forms.Label lbl_SegTest;
        private System.Windows.Forms.Label lbl_xyi;
        private System.ComponentModel.BackgroundWorker bgWorker_ImageLoad;
        private System.ComponentModel.BackgroundWorker bgWorker_Analysis1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect4;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect3;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect2;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect1;
        private System.Windows.Forms.CheckBox chkBx_ObjDetect5;
        private System.Windows.Forms.CheckBox chkBx_Onwv5;
        private System.Windows.Forms.TextBox txBx_Color5;
        private System.Windows.Forms.TextBox txBx_BrightnessMultiplier5;
        private System.Windows.Forms.TextBox txBx_Wavelength5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev5;
        private System.Windows.Forms.TextBox txBx_Wv_Name5;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev4;
        private System.Windows.Forms.TextBox txBx_Wv_Name4;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev3;
        private System.Windows.Forms.TextBox txBx_Wv_Name3;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev2;
        private System.Windows.Forms.TextBox txBx_Wv_Name2;
        private System.Windows.Forms.TextBox txBx_Wv_Abbrev1;
        private System.Windows.Forms.TextBox txBx_Wv_Name1;
        private System.Windows.Forms.Button btn_SaveWavelengthInfo;
        private System.Windows.Forms.TextBox txBx_WavelengthExtended;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh5;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh4;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh3;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh2;
        private System.Windows.Forms.TextBox txBx_Wv_Thresh1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnRunSegmentation;
        public System.Windows.Forms.CheckBox chkBx_SquareMode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkBx_DI_5;
        private System.Windows.Forms.CheckBox chkBx_DI_4;
        private System.Windows.Forms.CheckBox chkBx_DI_3;
        private System.Windows.Forms.CheckBox chkBx_DI_2;
        private System.Windows.Forms.CheckBox chkBx_DI_1;
        public System.Windows.Forms.TextBox txBx_ImageName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnJumpNextCalPoint;
        public System.Windows.Forms.CheckBox chkBx_ShowCalGrid;
        private System.Windows.Forms.ComboBox comboBox_ColorMerge;
        private System.Windows.Forms.TextBox txBx_Color_Overlay;
        public System.Windows.Forms.CheckBox chk_Bx_ObjClipsVis;
        private System.Windows.Forms.TextBox txBx_ClipExpand_Neg;
        private System.Windows.Forms.TextBox txBx_ClipExpand_Pos;
        private System.Windows.Forms.Label TimeLabel;
        private System.Windows.Forms.Button btn_TimeSeriesBck;
        private System.Windows.Forms.Button btn_TimeSeriesFwd;
        private System.Windows.Forms.Button btn_SlideShow;
        private System.Windows.Forms.TextBox txBx_MoveToTimePoint;
    }
}