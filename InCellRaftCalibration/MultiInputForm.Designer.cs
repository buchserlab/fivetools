﻿
namespace FIVE.RaftCal
{
    partial class MultiInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txBx1 = new System.Windows.Forms.TextBox();
            txBx2 = new System.Windows.Forms.TextBox();
            txBx3 = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            button_OK = new System.Windows.Forms.Button();
            button_Cancel = new System.Windows.Forms.Button();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            txBx4 = new System.Windows.Forms.TextBox();
            txBx5 = new System.Windows.Forms.TextBox();
            label7 = new System.Windows.Forms.Label();
            txBx7 = new System.Windows.Forms.TextBox();
            label6 = new System.Windows.Forms.Label();
            txBx6 = new System.Windows.Forms.TextBox();
            label8 = new System.Windows.Forms.Label();
            txBx8 = new System.Windows.Forms.TextBox();
            label9 = new System.Windows.Forms.Label();
            txBx9 = new System.Windows.Forms.TextBox();
            label10 = new System.Windows.Forms.Label();
            txBx10 = new System.Windows.Forms.TextBox();
            label11 = new System.Windows.Forms.Label();
            txBx11 = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // txBx1
            // 
            txBx1.Location = new System.Drawing.Point(14, 32);
            txBx1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx1.Name = "txBx1";
            txBx1.Size = new System.Drawing.Size(401, 23);
            txBx1.TabIndex = 0;
            // 
            // txBx2
            // 
            txBx2.Location = new System.Drawing.Point(10, 80);
            txBx2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx2.Name = "txBx2";
            txBx2.Size = new System.Drawing.Size(168, 23);
            txBx2.TabIndex = 1;
            // 
            // txBx3
            // 
            txBx3.Location = new System.Drawing.Point(10, 131);
            txBx3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx3.Name = "txBx3";
            txBx3.Size = new System.Drawing.Size(168, 23);
            txBx3.TabIndex = 2;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(14, 10);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(38, 15);
            label1.TabIndex = 3;
            label1.Text = "label1";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(10, 60);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(38, 15);
            label2.TabIndex = 4;
            label2.Text = "label2";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(10, 110);
            label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(38, 15);
            label3.TabIndex = 5;
            label3.Text = "label3";
            // 
            // button_OK
            // 
            button_OK.Location = new System.Drawing.Point(44, 305);
            button_OK.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            button_OK.Name = "button_OK";
            button_OK.Size = new System.Drawing.Size(88, 27);
            button_OK.TabIndex = 6;
            button_OK.Text = "OK";
            button_OK.UseVisualStyleBackColor = true;
            button_OK.Click += button_OK_Click;
            // 
            // button_Cancel
            // 
            button_Cancel.Location = new System.Drawing.Point(289, 305);
            button_Cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            button_Cancel.Name = "button_Cancel";
            button_Cancel.Size = new System.Drawing.Size(88, 27);
            button_Cancel.TabIndex = 7;
            button_Cancel.Text = "Cancel";
            button_Cancel.UseVisualStyleBackColor = true;
            button_Cancel.Click += button_Cancel_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(10, 159);
            label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(38, 15);
            label4.TabIndex = 11;
            label4.Text = "label4";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(10, 209);
            label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(38, 15);
            label5.TabIndex = 10;
            label5.Text = "label5";
            // 
            // txBx4
            // 
            txBx4.Location = new System.Drawing.Point(10, 179);
            txBx4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx4.Name = "txBx4";
            txBx4.Size = new System.Drawing.Size(168, 23);
            txBx4.TabIndex = 9;
            // 
            // txBx5
            // 
            txBx5.Location = new System.Drawing.Point(10, 230);
            txBx5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx5.Name = "txBx5";
            txBx5.Size = new System.Drawing.Size(168, 23);
            txBx5.TabIndex = 8;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(249, 59);
            label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(38, 15);
            label7.TabIndex = 15;
            label7.Text = "label7";
            // 
            // txBx7
            // 
            txBx7.Location = new System.Drawing.Point(245, 80);
            txBx7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx7.Name = "txBx7";
            txBx7.Size = new System.Drawing.Size(168, 23);
            txBx7.TabIndex = 14;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(10, 255);
            label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(38, 15);
            label6.TabIndex = 13;
            label6.Text = "label6";
            // 
            // txBx6
            // 
            txBx6.Location = new System.Drawing.Point(10, 276);
            txBx6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx6.Name = "txBx6";
            txBx6.Size = new System.Drawing.Size(168, 23);
            txBx6.TabIndex = 12;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(249, 110);
            label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(38, 15);
            label8.TabIndex = 19;
            label8.Text = "label8";
            // 
            // txBx8
            // 
            txBx8.Location = new System.Drawing.Point(245, 131);
            txBx8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx8.Name = "txBx8";
            txBx8.Size = new System.Drawing.Size(168, 23);
            txBx8.TabIndex = 18;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(249, 158);
            label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(38, 15);
            label9.TabIndex = 17;
            label9.Text = "label9";
            // 
            // txBx9
            // 
            txBx9.Location = new System.Drawing.Point(245, 179);
            txBx9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx9.Name = "txBx9";
            txBx9.Size = new System.Drawing.Size(168, 23);
            txBx9.TabIndex = 16;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(249, 209);
            label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(44, 15);
            label10.TabIndex = 23;
            label10.Text = "label10";
            // 
            // txBx10
            // 
            txBx10.Location = new System.Drawing.Point(245, 230);
            txBx10.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx10.Name = "txBx10";
            txBx10.Size = new System.Drawing.Size(168, 23);
            txBx10.TabIndex = 22;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(249, 255);
            label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(44, 15);
            label11.TabIndex = 21;
            label11.Text = "label11";
            // 
            // txBx11
            // 
            txBx11.Location = new System.Drawing.Point(245, 276);
            txBx11.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx11.Name = "txBx11";
            txBx11.Size = new System.Drawing.Size(168, 23);
            txBx11.TabIndex = 20;
            // 
            // MultiInputForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(426, 344);
            Controls.Add(label10);
            Controls.Add(txBx10);
            Controls.Add(label11);
            Controls.Add(txBx11);
            Controls.Add(label8);
            Controls.Add(txBx8);
            Controls.Add(label9);
            Controls.Add(txBx9);
            Controls.Add(label7);
            Controls.Add(txBx7);
            Controls.Add(label6);
            Controls.Add(txBx6);
            Controls.Add(label4);
            Controls.Add(label5);
            Controls.Add(txBx4);
            Controls.Add(txBx5);
            Controls.Add(button_Cancel);
            Controls.Add(button_OK);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(txBx3);
            Controls.Add(txBx2);
            Controls.Add(txBx1);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "MultiInputForm";
            Text = "MultiInputForm";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        public System.Windows.Forms.TextBox txBx1;
        public System.Windows.Forms.TextBox txBx2;
        public System.Windows.Forms.TextBox txBx3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Button button_Cancel;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txBx4;
        public System.Windows.Forms.TextBox txBx5;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txBx7;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txBx6;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txBx8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txBx9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txBx10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txBx11;
    }
}