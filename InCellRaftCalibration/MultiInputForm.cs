﻿using FIVE_IMG.Seg;
using Protobuf.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIVE.RaftCal
{
    public partial class MultiInputForm : Form
    {
        public MultiInputForm()
        {
            InitializeComponent();
            label1.Click += Label1_Click;

            //var tB6 = new TextBox(); var tB7 = new TextBox();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.Title = label1.Text;
            OFD.InitialDirectory = txBx1.Text;
            var res = OFD.ShowDialog();
            if (res == DialogResult.OK)
            {
                txBx1.Text = OFD.FileName;
            }
        }

        public static DialogResult ShowMulti(string Title, out Tuple<string, string, string, string, string> Results, string Caption1, string D1, string Caption2 = "", string D2 = "", string Caption3 = "", string D3 = "", string Caption4 = "", string D4 = "", string Caption5 = "", string D5 = "")
        {
            var th = new MultiInputForm();
            th.Text = Title;
            th.label1.Text = Caption1; th.label2.Text = Caption2; th.label3.Text = Caption3; th.label4.Text = Caption4; th.label5.Text = Caption5;
            th.txBx1.Text = D1; th.txBx2.Text = D2; th.txBx3.Text = D3; th.txBx4.Text = D4; th.txBx5.Text = D5;

            th.label3.Visible = th.txBx3.Visible = (Caption3 != "");
            th.label4.Visible = th.txBx4.Visible = (Caption4 != "");
            th.label5.Visible = th.txBx5.Visible = (Caption5 != "");
            th.label6.Visible = th.txBx6.Visible = false;
            th.label7.Visible = th.txBx7.Visible = false;
            th.label8.Visible = th.txBx8.Visible = false;
            th.label9.Visible = th.txBx9.Visible = false;
            th.label10.Visible = th.txBx10.Visible = false;
            th.label11.Visible = th.txBx11.Visible = false;

            DialogResult Dr = th.ShowDialog();
            Results = new Tuple<string, string, string, string, string>(th.txBx1.Text, th.txBx2.Text, th.txBx3.Text, th.txBx4.Text, th.txBx5.Text);
            return Dr;
        }

        public static (DialogResult, string[]) ShowMultiMore(string Title, string C1, string D1, string C2 = "", string D2 = "", string C3 = "", string D3 = "", string C4 = "", string D4 = "", string C5 = "", string D5 = "", string C6 = "", string D6 = "", string C7 = "", string D7 = "", string C8 = "", string D8 = "", string C9 = "", string D9 = "", string C10 = "", string D10 = "", string C11 = "", string D11 = "")
        {
            var th = new MultiInputForm();
            th.Text = Title;
            th.label1.Text = C1; th.label2.Text = C2; th.label3.Text = C3; th.label4.Text = C4; th.label5.Text = C5; th.label6.Text = C6; th.label7.Text = C7; th.label8.Text = C8; th.label9.Text = C9; th.label10.Text = C10; th.label11.Text = C11;
            th.txBx1.Text = D1; th.txBx2.Text = D2; th.txBx3.Text = D3; th.txBx4.Text = D4; th.txBx5.Text = D5; th.txBx6.Text = D6; th.txBx7.Text = D7; th.txBx8.Text = D8; th.txBx9.Text = D9; th.txBx10.Text = D10; th.txBx11.Text = D11;

            th.label3.Visible = th.txBx3.Visible = (C3 != "");
            th.label4.Visible = th.txBx4.Visible = (C4 != "");
            th.label5.Visible = th.txBx5.Visible = (C5 != "");
            th.label6.Visible = th.txBx6.Visible = (C6 != "");
            th.label7.Visible = th.txBx7.Visible = (C7 != "");
            th.label8.Visible = th.txBx8.Visible = (C8 != "");
            th.label9.Visible = th.txBx9.Visible = (C9 != "");
            th.label10.Visible = th.txBx10.Visible = (C10 != "");
            th.label11.Visible = th.txBx11.Visible = (C11 != "");

            DialogResult Dr = th.ShowDialog();
            string[] resarr = new string[] { th.txBx1.Text, th.txBx2.Text, th.txBx3.Text, th.txBx4.Text, th.txBx5.Text, th.txBx6.Text, th.txBx7.Text, th.txBx8.Text, th.txBx9.Text, th.txBx10.Text, th.txBx11.Text };
            return (Dr, resarr);
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
