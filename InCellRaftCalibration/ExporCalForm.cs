﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using FIVE.ImageCheck;
using FIVE.FOVtoRaftID;
using FIVE.InCellLibrary;
using FIVE_IMG;
using System.Diagnostics;

namespace FIVE.RaftCal
{
    public partial class ExportCalForm : Form
    {
        public FormRaftCal ParentCalForm;
        public RaftImage_Export_Settings Settings;

        #region FormWide - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        public ExportCalForm(FormRaftCal Parent = null)
        {
            ParentCalForm = Parent;
            InitializeComponent();
            SaveLoad_Export_Settings(true);
            Enable_Save_Settings(true);
            SaveLoad_Annotation_Settings(true);
        }

        private void ExporCalForm_Load(object sender, EventArgs e)
        {

        }

        private void ExporCalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        #endregion

        #region Export Settings - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        //This was a function in the old version that got the default path copied
        private void LoadLoc()
        {
            RaftImage_Export_Settings.Load();
            Clipboard.SetText(RaftImage_Export_Settings.DefaultPath);
            MessageBox.Show("Settings for this export and also list-based export are saved here: " + RaftImage_Export_Settings.DefaultPath + "\r\n\r\n (and copied to the clipboard)  You can edit that file first (XML), then click on an export button again (it checks for the file every time you click). Those settings will become the new defaults. Delete the file to 'reset' back to factory defaults.");
        }

        public void SaveLoad_Export_Settings(bool LoadIntoForm = true)
        {
            if (Settings == null) Settings = RaftImage_Export_Settings.Load();
            Type RIES = Settings.GetType();
            PropertyInfo[] Fields = RIES.GetProperties();
            Control C;
            foreach (PropertyInfo field in Fields)
            {
                if (Controls.Find(field.Name, true).Count() == 0) continue;
                C = Controls.Find(field.Name, true).First();
                if (field.PropertyType.Name.Contains("String"))
                {
                    TextBox CB = (TextBox)C;
                    if (LoadIntoForm) CB.Text = (string)field.GetValue(Settings);
                    else field.SetValue(Settings, CB.Text);
                }
                else
                if (field.PropertyType.Name.Contains("Boolean"))
                {
                    CheckBox CB = (CheckBox)C;
                    if (LoadIntoForm) CB.Checked = (bool)field.GetValue(Settings);
                    else field.SetValue(Settings, CB.Checked);
                }
                else
                {
                    TextBox CB = (TextBox)C;
                    if (LoadIntoForm)
                        CB.Text = field.GetValue(Settings).ToString();
                    else
                    {
                        if (field.PropertyType.Name.Contains("Single") || field.PropertyType.Name.Contains("Double"))
                        {
                            field.SetValue(Settings, float.Parse(CB.Text));
                        }
                        else
                        {
                            field.SetValue(Settings, int.Parse(CB.Text));
                        }
                    }
                }
            }
        }

        private void btn_SaveSettings_Click(object sender, EventArgs e)
        {
            SaveLoad_Export_Settings(false);
            Enable_Save_Settings(true);
            //Actually save it . . 
            Settings.Save();
        }

        private void btn_DefaultSettings_Click(object sender, EventArgs e)
        {
            Settings = RaftImage_Export_Settings.Defaults();
            SaveLoad_Export_Settings(true);
        }

        public string Default_ExportSettingsFolder = @"R:\dB\Software\FIVE_Tools\Settings\";

        private void btn_SaveAsSettings_Click(object sender, EventArgs e)
        {
            btn_SaveSettings_Click(sender, e); //Do this first

            var SFD = new SaveFileDialog();
            SFD.InitialDirectory = Default_ExportSettingsFolder;
            SFD.Filter = "xml file|*.xml";
            SFD.Title = "Enter a location to Save the settings";
            var DR = SFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                Settings.Save(SFD.FileName);
                txBx_Update.Text = "Saved Settings to \r\n" + SFD.FileName;
            }
        }

        private void btn_LoadSettings_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Default_ExportSettingsFolder;
            OFD.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            OFD.Title = "Please select the settings XML file to Load.";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                Settings = RaftImage_Export_Settings.Load(OFD.FileName); //Makes it Active
                SaveLoad_Export_Settings(true);
            }
        }

        private void Form_Settings_Changed(object sender, EventArgs e)
        {
            Enable_Save_Settings(false);
        }

        public void Enable_Save_Settings(bool TurnOnActions)
        {
            btn_SaveSettings.Enabled = !TurnOnActions;
            btn_ExportAllRaftImages.Enabled = btn_ExportFromList.Enabled = btn_ExportRaftImages.Enabled = btn_ExportCropReg_Objects.Enabled= TurnOnActions;
        }

        #endregion

        #region Export Methods - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void ExportFromList()
        {
            if (ParentCalForm.CurrentWell.CalibrationRaftSettings == null)
            {
                MessageBox.Show("Currently only possible for calibrated rafts.");
                return;
            }

            //Ask user for a file that has RaftIDs
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "This will look for ANY and ALL Raft IDs in the file, make sure that only ones related to this plate are in the selected file.";
            ofd.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true;
            DialogResult DR = ofd.ShowDialog();
            if (DR == DialogResult.Cancel) return;
            string textContents = File.ReadAllText(ofd.FileName);
            System.Text.RegularExpressions.Regex Regexp = new System.Text.RegularExpressions.Regex("[A-Z]\\d[A-Z]\\d");
            System.Text.RegularExpressions.MatchCollection MC = Regexp.Matches(textContents);

            //Get the unique list of IDs
            HashSet<string> HS = new HashSet<string>();
            foreach (System.Text.RegularExpressions.Match match in MC) HS.Add(match.Value);

            //Package up the IDs so they can be exported (link them to images and points)
            ImageCheck_PointList ICPL = ImageCheck_PointList.FromRafts(ParentCalForm.IC_Folder, HS);

            //Now call the export on this
            Trigger_Export_Intern(Settings, ICPL);

            //Go back and get remaining RaftIDs
            txBx_Update.Text += ". . " + (HS.Count - ICPL.Count) + " remaining rafts which weren't exported.";
        }

        private void btn_ExportRaftImages_Click(object sender, EventArgs e)
        {
            //Use the loaded list of points to go through with the current image settings and crop out the rafts that are marked.  We can name them with prefix or suffix that indicates which marking they got
            Trigger_Export_Intern(Settings, ParentCalForm.ImageChk_List);
        }

        private void btn_ExportAllRaftImages_Click(object sender, EventArgs e)
        {
            //Export all
            Trigger_Export_Intern(Settings, new ImageCheck_PointList());
        }

        private void btn_ExportFromList_Click(object sender, EventArgs e)
        {
            //Export from List
            ExportFromList();
        }

        public void Trigger_Export_Intern(RaftImage_Export_Settings SettingsToUse, ImageCheck_PointList PLToUse)
        {
            Trigger_Export(SettingsToUse, PLToUse, ParentCalForm.IC_Folder, ParentCalForm.CurrentWVParams, ParentCalForm, bgMain);
        }

        public static void Trigger_Export(RaftImage_Export_Settings SettingsToUse, ImageCheck_PointList PLToUse, INCELL_Folder IC_Folder, wvDisplayParams wvParams, FormRaftCal CalForm, BackgroundWorker BW = null)
        {
            if (!Directory.Exists(SettingsToUse.ExportFolderBase))
            {
                Directory.CreateDirectory(SettingsToUse.ExportFolderBase);
                //SettingsToUse.ExportFolderBase = @"e:\temp\cellimages\iBased\";
            }
            string PName = SettingsToUse.SubFolderByPlateID ? Path.Combine(SettingsToUse.ExportFolderBase, IC_Folder.PlateID) : SettingsToUse.ExportFolderBase;
            var DI = new DirectoryInfo(PName);
            if (DI.Exists && SettingsToUse.SubFolderByPlateID)
            {
                DialogResult DR = MessageBox.Show("That subfolder appears to already exist. Do you want to continue (may overwrite previous exports)?", "Export Folder Check", MessageBoxButtons.OKCancel);
                if (DR == DialogResult.Cancel) return;
            }


            if (BW != null)
            {
                if (BW.IsBusy)
                {
                    ExportImages(SettingsToUse, PLToUse, IC_Folder, wvParams, CalForm, BW);
                }
                else
                {
                    var Args = new Tuple<RaftImage_Export_Settings, ImageCheck_PointList, INCELL_Folder, wvDisplayParams, FormRaftCal>(
                                 SettingsToUse, PLToUse, IC_Folder, wvParams, CalForm);
                    BW.RunWorkerAsync(Args);
                }
            }
        }

        public static Font FontPrePick = new Font("Arial", 11, FontStyle.Regular);
        private static Random _Rand = new Random();

        public static (Bitmap Final, Bitmap NoImprint) GetImage(RaftInfoS RaftInfo, INCELL_Folder ICFolder, RaftImage_Export_Settings Settings, wvDisplayParams WVParams, FormRaftCal CalForm, string Imprint = "", BackgroundWorker BW = null)
        {
            var PointList = new ImageCheck_PointList();
            ImageCheck_PointList.FromRaft(ICFolder, RaftInfo, PointList, true);
            Bitmap BM = null; string status = "";
            if (PointList.Count == 0)
            {
                var Pt = new ImageCheck_Point();
                Pt.RaftID = RaftInfo.RaftID;
                Pt.PlateID = RaftInfo.PlateID;
                Pt.Well_Row = RaftInfo.WellLabel == "" ? "" : RaftInfo.WellLabel.Substring(0, 1);
                Pt.Well_Col = RaftInfo.WellLabel == "" ? "" : RaftInfo.WellLabel.Substring(4, 1);
                Pt.Annotations = new List<ImageCheck_Annotation>();
                Pt.Annotations.Add(new ImageCheck_Annotation() { Value = RaftInfo.Note });

                if (Pt.RaftID.StartsWith("nr"))
                {
                    Pt.IsCell = true;
                    var Arr = Pt.RaftID.Substring(2).Split(','); int cx = int.Parse(Arr[0]); int cy = int.Parse(Arr[1]); int cr = int.Parse(Arr[2]);
                    var r = new Rectangle(cx - cr / 2, cy - cr / 2, cr, cr);
                    Pt.CellInfo = new CellInfoS() { RaftID = Pt.RaftID, ObjectID = Pt.RaftID, PlateID = Pt.PlateID, WellLabel = Pt.Well_Label, FOV = Pt.FOV.ToString(), R = r };
                }

                PointList.Add(Pt);
                //BM = null now
            }
            if (PointList.Count > 0)
            {
                if (PointList.Count > 4)
                {

                }
                (BM, status) = ExportImages(Settings, PointList, ICFolder, WVParams, CalForm, BW, false);
            }
            if (BM == null)
            {
                BM = new Bitmap(150, 150);
                using (Graphics g = Graphics.FromImage(BM)) { g.Clear(Color.DarkBlue); g.DrawString(status, FontPrePick, Brushes.Cyan, 10, 110); }
            }
            bool AddLabels = true;
            var Pre = new Bitmap(BM);
            if (AddLabels)
            {
                using (Graphics e = Graphics.FromImage(BM))
                {
                    e.DrawString(RaftInfo.RaftID, FontPrePick, Brushes.White, new Point(2, 2));
                    if (Imprint != "") e.DrawString(Imprint, FontPrePick, Brushes.White, new Point(BM.Width - 35, 2));
                }
            }
            return (BM, Pre);
        }

        public static (Bitmap BMP, string Status) ExportImages(RaftImage_Export_Settings Settings, ImageCheck_PointList PointList, INCELL_Folder ICFolder, wvDisplayParams WVParams, FormRaftCal CalForm, BackgroundWorker BW, bool Save = true)
        {
            XDCE_ImageGroup Welli; string FName; string ExportFolder = ""; string lastStatus = "Good";

            string Plate = ICFolder.PlateID;
            if (Save)
            {
                var DI = new DirectoryInfo(Settings.ExportFolderBase); if (!DI.Exists) DI.Create();
                if (Settings.SubFolderBy_PlateID_Well) Settings.SubFolderByPlateID = false;
                if (Settings.SubFolderByPlateID) { DI = new DirectoryInfo(Path.Combine(Settings.ExportFolderBase, Plate)); if (!DI.Exists) DI.Create(); }
                ExportFolder = DI.FullName;
            }
            if (BW != null) if (BW.CancellationPending) return (null, "Cancelled");

            var RaftsExported = new HashSet<string>(); //Ensures that we don't export a raft more than once
            ReturnRaft RR; RectangleF r; Bitmap nb; Bitmap resized; float SquareRatio; float SmallerDim;
            List<ImageMagick.MagickImage> mI = null, mISub = null;
            XDCE_Image xI; Bitmap bmap = null;
            Bitmap bmapMult = null; string Annotation; byte[] TE1; string ExportSubFolder = ExportFolder; Bitmap bmapLatest = null;
            int PointsExported = 0; int PointsTotal = 0; int NonSquare = 0; int Fiducial = 0; int BitMapIssue = 0; int NotFullSize = 0; int counter = 0; int NotIncludeAnno = 0; bool IgnoreBadRafts;
            if (PointList == null) PointList = new ImageCheck_PointList();
            ImageCheck_PointList PointListTemp;

            if (PointList.FOVs.Count == 0)
            {
                foreach (var well in ICFolder.XDCE.Wells)
                {
                    if (well.Value.HasRaftCalibration || well.Value.Rafts.Count > 0) // Before 4/2024 this was set to "Has Calibration", but adjusted so regions could work
                    { } //Good
                    else
                    {
                        BW.ReportProgress(0, "!!! Warning, Well " + well.Value.NameAtLevel + " isn't calibrated");
                        return (null, "Not Cal");
                    }
                }
                if (BW != null) BW.ReportProgress(0, "Exporting ALL RAFTS . . ");
                PointListTemp = ImageCheck_PointList.FromRafts(ICFolder);
                IgnoreBadRafts = true;
            }
            else
            {
                if (BW != null) BW.ReportProgress(0, "Exporting only SPECIFIED RAFTS . . ");
                PointListTemp = PointList;
                IgnoreBadRafts = false;
            }

            //TODO FIX THIS UP TO THE RIGHT PLACE, USE ANOTHER VARIABLE THAT WE ALREADY BUILT FOR THIS
            var ReplaceMeDict = new Dictionary<string, string>() { { "1.A - 1", "G1" }, { "1.A - 2", "Mix50" }, { "1.B - 1", "G2" }, { "1.B - 2", "G1" }, { "2.A - 1", "G2" }, { "2.A - 2", "G1" }, { "2.B - 1", "Mix50" }, { "2.B - 2", "G2" } };
            var SkipWells = new HashSet<string>(); //Skips problem wells
            var FolderCreated = new Dictionary<string, string>();
            //Now go through and actually export
            if (PointListTemp.Count == 0) { BW.ReportProgress(0, "!!!! Nothing to export, could be because Wells are not Calibrated. !!"); return (null, "No Points"); }
            if (Save) Settings.SaveCurrent(ICFolder, WVParams, Path.Combine(ExportFolder, "ExportSettings.xml")); //Write a file out that has the settings in it
            foreach (string Well_Field in PointListTemp.WellFields)
            {
                List<ImageCheck_Point> points = PointListTemp.FromDictionaryWellField(Well_Field); if (points.Count == 0) { lastStatus = "339"; continue; }
                Welli = ICFolder.XDCE.Wells[points[0].Well_Label];
                if (Save && Settings.SubFolderBy_PlateID_Well)
                {
                    string tKey = Plate.Substring(7, 1) + "." + Welli.NameAtLevel; string subT = "NJ"; // ReplaceMeDict.conta ReplaceMeDict[tKey];
                    var DI = new DirectoryInfo(Path.Combine(Settings.ExportFolderBase, subT)); if (!DI.Exists) DI.Create(); ExportSubFolder = DI.FullName;
                }
                if (SkipWells.Contains(Welli.NameAtLevel)) { lastStatus = "346"; continue; }
                //if (!Welli.HasRaftCalibration && !points[0].IsCell) { if (BW != null) BW.ReportProgress(0, "!!! Warning, Well " + Welli.NameAtLevel + " isn't calibrated"); SkipWells.Add(Welli.NameAtLevel); continue; }
                xI = Welli.GetField(points[0].FOV, 0); // Instead of 0, used to be Wavelength
                if (!Settings.TIF16Bit_Export)
                {
                    bmap = CalForm.CombinedBMAP(Welli, xI); //Pulls from or adds to the Image Queue
                }
                else
                {
                    mI = mIQueue.GetMImageSet(xI, false);
                    bmap = new Bitmap(mI[0].Width, mI[0].Height);
                }
                if (bmap.Width <= 10) { BitMapIssue++; continue; }
                if (BW != null) { if (++counter % 5 == 0) BW.ReportProgress(1, counter.ToString("00 ")); if (BW.CancellationPending) { BW.ReportProgress(0, "User Cancelled"); return (null, "Cancelled"); } }
                if (false && points[0].IsCell) //This draws the boxes on the image itself (for cells)
                {
                    using (var g = Graphics.FromImage(bmap))
                    {  //batch this
                       Rectangle[] rectangles = new Rectangle[points.Count];
                       for(int i = 0; i < points.Count; i++)
                        { 
                            Rectangle Rectcoordinates = points[i].CellCoordinates(xI, 2);
                            rectangles[i] = Rectcoordinates;
                        }
                        g.DrawRectangles(Pens.Green, rectangles);
                    }
                    bmap.Save(Path.Combine(ExportSubFolder, Well_Field.Replace(":", "_") + "." + Settings.ImageExtension));
                }
                foreach (ImageCheck_Point point in points)
                {
                    PointsTotal++;
                    if (point.IsCell)
                    {
                        if (_Rand.NextDouble() > Settings.DownsampleFraction) continue; //Skips images randomly so we don't get too many

                        bool TryPhySeg = false; //12/2022 - use this for Physics-based Segmentation FIV791
                        if (TryPhySeg)
                        {
                            int size1 = 41; // 58;
                            r = new Rectangle((int)point.CellInfo.CenterPoint.X - size1, (int)point.CellInfo.CenterPoint.Y - size1, size1, size1);
                            if (r.X < 0 || r.Y < 0 || r.X + r.Width > bmap.Width || r.Y + r.Height > bmap.Height) continue;
                            bmapMult = bmap.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                            bmapMult = new Bitmap(bmapMult, new Size(256, 256));
                        }
                        else
                        {
                            //We shouldn't be expanding by cellImageExpandpixels if this is a semi-raft generated one
                            r = point.CellCoordinates(xI, Settings.CellImageExpandPixels);
                            bmapMult = bmap.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        }
                        bmapLatest = bmapMult;
                        if (Save) //Actually save
                        {
                            FName = Plate + "." + point.Well_Label + "." + point.FOV + "_" + point.CellInfo.ObjectID + ".";
                            string ExpSFolderT = ExportSubFolder;
                            if (ExpSFolderT == "") ExpSFolderT = "c:\\temp";
                            if (Settings.ColumnNameForAnnotation != "")
                            {
                                if (FolderCreated.ContainsKey(point.CellInfo.ClassLabel))
                                    ExpSFolderT = FolderCreated[point.CellInfo.ClassLabel];
                                else
                                {
                                    ExpSFolderT = Path.Combine(ExportSubFolder, point.CellInfo.ClassLabel);
                                    var DI = new DirectoryInfo(ExpSFolderT); DI.Create();
                                    FolderCreated.Add(point.CellInfo.ClassLabel, ExpSFolderT); //That way we don't have to use IO
                                }
                            }
                            bmapMult.Save(Path.Combine(ExpSFolderT, FName) + Settings.ImageExtension);
                        }
                    }
                    else
                    {
                        if (Welli.HasRaftCalibration) RR = Welli.CalibrationRaftSettings.FindRaftID_RR(point.Plate_Xum, point.Plate_Yum);
                        else
                        {
                            if (point.RaftID == null) continue;
                            var tFOV = Welli.GetFields(point.FOV).First();
                            if (!tFOV.Rafts.ContainsKey(point.RaftID)) continue;
                            RR = tFOV.Rafts[point.RaftID].RR;
                        }
                        if (RR == null) { if (BW != null) BW.ReportProgress(0, "!!! Warning, Well " + Welli.NameAtLevel + " isn't calibrated"); SkipWells.Add(Welli.NameAtLevel); continue; }
                        if (!Settings.Include_Fiducial_Rafts && RR.IsFiducial) { Fiducial++; lastStatus = "Fiducial"; continue; }
                        if (RaftsExported.Contains(RR.RaftID)) continue; //Seems necessary, but I had taken it out for some reason. . 
                        if (!RaftInfoS.IsRaftIDValid(RR.RaftID)) continue;
                        r = ReturnRaft.RectFromCorners(xI, RR, bmap.Size);

                        SmallerDim = Math.Min(r.Width, r.Height); SquareRatio = (float)(Math.Round(SmallerDim, 0) / Math.Round(Math.Max(r.Width, r.Height), 0)); //if (PixelsExpand > 0) r = new RectangleF(r.Left - PixelsExpand, r.Top + PixelsExpand, r.Width + PixelsExpand, r.Height + PixelsExpand); //Implementation of Pixels expand . . but isn't quite right
                        if (Settings.OnlyExportSquare)
                        {
                            if (SquareRatio < Settings.Min_AspectRatio_Keep)
                            {
                                NonSquare++; continue;
                            } //Skip the export if this isn't mostly square

                            //This new style we actually crop out the region of interest first, resize it, then do the final crop (trimming)
                            r = new RectangleF(r.Left, r.Top, SmallerDim, SmallerDim);
                            try
                            {
                                if (!Settings.TIF16Bit_Export)
                                    bmapMult = bmap.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                                else
                                {
                                    bmapMult = new Bitmap((int)r.Width, (int)r.Height);
                                    mISub = mIQueue.CropSet(mI, r);
                                }
                            }
                            catch { continue; }
                            if (Settings.ResizeMultiplier != 1 && !Settings.TIF16Bit_Export)
                            {
                                int newSize = (int)(SmallerDim * Settings.ResizeMultiplier) + 1;
                                if ((Settings.CropToMaxSize - newSize) < 10) newSize = (int)Settings.CropToMaxSize;
                                bmapMult = new Bitmap(bmapMult, new Size(newSize, newSize));
                            }
                        } //NotSquare is not implemented
                          //Trying to restrict the final size
                        float CropToMax = Math.Min(Settings.CropToMaxSize, Math.Min(bmapMult.Size.Height, bmapMult.Size.Width));
                        if (Settings.CropToMaxSize >= 1 && CropToMax <= bmapMult.Width)
                        {
                            // This is the code before 7/2021 when we already have the cropped out piece that we need that is resized
                            //float CropToMax = Math.Min(Settings.CropToMaxSize, Math.Min(r.Size.Height, r.Size.Width));
                            //SizeF FinalSize = new SizeF(CropToMax, CropToMax); PointF Shift2 = new PointF((r.Size.Width - FinalSize.Width) / 2, (r.Size.Height - FinalSize.Height) / 2);
                            //r = new RectangleF(new PointF(r.Left + Shift2.X, r.Top + Shift2.Y), FinalSize); //r = new RectangleF(Math.Min(bmap.Width, Math.Max(0, r.Left)), Math.Min(bmap.Height, Math.Max(0, r.Top)), Math.Min(bmap.Width, Math.Max(0, r.Width)), Math.Min(bmap.Height, Math.Max(0, r.Height))); //Old one but the math isn't right
                            SizeF FinalSize = new SizeF(CropToMax, CropToMax);
                            PointF Shift2 = new PointF((bmapMult.Size.Width - FinalSize.Width) / 2, (bmapMult.Size.Height - FinalSize.Height) / 2);
                            r = new RectangleF(new PointF(Shift2.X, Shift2.Y), FinalSize);
                        }
                        else
                        { r = new RectangleF(new PointF(0, 0), (SizeF)bmapMult.Size); }
                        if (Settings.CropToMaxSize >= 1 && Settings.OnlyExportFullSize && r.Size.Width < Settings.CropToMaxSize) { NotFullSize++; continue; }
                        if (float.IsNaN(r.Width) || float.IsNaN(r.Height) || SmallerDim < 0)
                        {
                            if (!IgnoreBadRafts && BW != null) BW.ReportProgress(1, point.RaftID); //Problem with the calibration, couldn't figure out the rafts . . 
                        }
                        else
                        {
                            try
                            {
                                //For Jack's program, we need to export as RGB, not ARGB
                                nb = bmapMult.Clone(r, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                            }
                            catch { continue; }
                            Annotation = PointList.FromDictionaryRaftID_Combined(point.RaftID).Replace("/", "_");
                            if (Settings.OnlyExportAnnotations.Count > 0)
                            {
                                if (!Settings.OnlyExportAnnotations.Contains(Annotation.Trim().ToUpper()))
                                {
                                    NotIncludeAnno++; continue;
                                }
                            }
                            if (Annotation == "")
                            {
                                Annotation = "A";  //if (Annotation.StartsWith("No")) continue; //Use this to only export one type of annotation
                            }
                            FName = (Settings.FileName_ByAnnotation ? Annotation + "-" : "") + Plate + "." + point.Well_Label + "." + point.FOV + "." + point.RaftID + "." + (!RaftsExported.Contains(point.RaftID) ? "0" : "1"); //The .0 will always have that one, but may have a second one that is .1 or .2, etc
                            FName = FName.Replace("  ", " ").Trim();
                            if (Settings.Resize_And_MLExport) //This is for direct pixel data export
                            {
                                resized = new System.Drawing.Bitmap(nb, new Size(Settings.PixelList_ExportSize, Settings.PixelList_ExportSize));
                                TE1 = FormRaftCal.BitmapToByteArray(resized);
                                //SW.WriteLine(Annotation + "\t" + Plate + "\t" + point.Well_Label + "\t" + point.RaftID + "\t" + String.Join("\t", TE1)); resized.Save(Path.Combine(ExportFolder, FName) + "_s.bmp"); //To export the actual resized image
                            }
                            if (Settings.FolderName_ByAnnotation && Save)
                            {
                                ExportSubFolder = Path.Combine(ExportFolder, Annotation);
                                DirectoryInfo DISF = new DirectoryInfo(ExportSubFolder); if (!DISF.Exists) DISF.Create();
                            }
                            bmapLatest = nb;
                            if (Save) //Actually save
                            {
                                if (!Settings.TIF16Bit_Export)
                                {
                                    nb.Save(Path.Combine(ExportSubFolder, FName) + "." + Settings.ImageExtension);
                                    if (Settings.Export_4Rotations)
                                    {
                                        for (int i = 0; i < 3; i++)
                                        {
                                            nb.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                            nb.Save(Path.Combine(ExportSubFolder, FName) + "-" + i + "." + Settings.ImageExtension);
                                        }
                                    }
                                }
                                else
                                {
                                    for (int wv = 0; wv < mISub.Count; wv++) { mISub[wv].Write(Path.Combine(ExportSubFolder, FName) + " wv" + wv + ".tif"); mISub[wv].Dispose(); }
                                }
                                bmapMult.Dispose();
                                nb.Dispose(); //Except we are using the last one for bmapLatest?
                            }
                            RaftsExported.Add(point.RaftID); PointsExported++;
                        }
                    }
                }
                //bmap.Dispose(); //Since this is managed now by the ImageQueue system, we don't have to do this.. 
                if (mI != null)
                    foreach (var img in mI) img.Dispose();
            }
            //SW.Close();
            if (BW != null) BW.ReportProgress(0, "\r\nExported " + RaftsExported.Count + " raft Images to " + ExportFolder + " from " + PointsExported + " points. " + NonSquare + " not square enough, " + Fiducial + "  fiducial, " + BitMapIssue + " BMap issues, " + (Settings.OnlyExportAnnotations.Count == 0 ? "" : (NotIncludeAnno + " not in Anno list to include, ")) + NotFullSize + " not full size." + ".\r\n");
            return (bmapLatest, lastStatus);
        }

        #endregion

        #region RunExport - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            bgMain.CancelAsync();
        }

        private void bgMain_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = (Tuple<RaftImage_Export_Settings, ImageCheck_PointList, INCELL_Folder, wvDisplayParams, FormRaftCal>)e.Argument;
            ExportImages(Args.Item1, Args.Item2, Args.Item3, Args.Item4, Args.Item5, bgMain);
        }

        private void bgMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0) txBx_Update.Text = e.UserState + "\r\n" + txBx_Update.Text;
            else txBx_Update.Text = e.UserState + ", " + txBx_Update.Text;
        }

        private void bgMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        #endregion

        #region Random - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        private void btn_PlateMetadata(object sender, EventArgs e)
        {
            //Saves a file with all the annotations for this whole plate
            Clipboard.SetText(ImageCheck_GetTable(ParentCalForm.ImageChk_List));
            txBx_Update.Text = "Copied RaftIDs to Clipboard.";
        }
        public static string ImageCheck_GetTable(ImageCheck_PointList PointList)
        {
            System.Text.StringBuilder SB = new System.Text.StringBuilder();
            char delim = '\t';
            foreach (ImageCheck_Point Point in PointList.Points)
            {
                SB.Append(Point.Well_Label + delim + Point.FOV + delim + Point.RaftID + delim + delim + string.Join("|", Point.Annotations.Select(x => x.Value)) + "\r\n");
            }
            return SB.ToString();
        }

        #endregion

        #region Annotations - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public ImageCheck_Annotation_Schema ActiveSchema { get => ImageCheck_Annotation_Schema.ActiveSchema; set { ImageCheck_Annotation_Schema.ActiveSchema = value; } }

        public string Default_AnnotationSchemaFolder = @"R:\dB\Software\FIVE_Tools\Schemas\";

        private void SaveLoad_Annotation_Settings(bool v)
        {
            txBx_AnnotationsInfo.Text = ActiveSchema.GetAsText();
        }

        public void LoadSchema(string Path)
        {
            ActiveSchema = ImageCheck_Annotation_Schema.Load(Path);
            txBx_AnnotationsInfo.Text = ActiveSchema.GetAsText();
            txBx_AnnotationsCurrentSet.Text = Path;
        }

        private void btn_Annotations_Load_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Default_AnnotationSchemaFolder;
            OFD.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            OFD.Title = "Please select the Annotation Schema XML file to Load.";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                LoadSchema(OFD.FileName); //Makes it Active
            }
        }

        private void btn_Annotations_New_Click(object sender, EventArgs e)
        {
            txBx_Update.Text = "Not Implemented";
        }

        private void btn_Annotations_SaveAs_Click(object sender, EventArgs e)
        {
            var SFD = new SaveFileDialog();
            SFD.InitialDirectory = Default_AnnotationSchemaFolder;
            SFD.Filter = "xml file|*.xml";
            SFD.Title = "Enter a location to Save this annotation Schema";
            var DR = SFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                ActiveSchema.Save(SFD.FileName);
                txBx_AnnotationsCurrentSet.Text = SFD.FileName;
            }
        }

        private void btn_Annotations_Open_Click(object sender, EventArgs e)
        {
            //Opens the current one for editing
            bool cont = true; FileInfo FI = null;
            if (txBx_AnnotationsCurrentSet.Text == "") cont = false;
            if (cont) { FI = new FileInfo(txBx_AnnotationsCurrentSet.Text); if (!FI.Exists) cont = false; }
            if (!cont)
            {
                txBx_Update.Text = "Please load an annotation set first (or Save As)";
                return;
            }
            Process Pr = new Process();
            try
            {
                Pr = Process.Start("notepad++.exe", FI.FullName);
            }
            catch
            {
                Pr = Process.Start("notepad.exe", FI.FullName);
            }
        }

        private void btn_Import_Annotations_Click(object sender, EventArgs e)
        {
            var OFD = new OpenFileDialog();
            OFD.InitialDirectory = Path.Combine(@"R:\FIVE\EXP", ParentCalForm.IC_Folder.FIViD);
            OFD.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            OFD.Title = "Please select at TXT file with Columns = PlateID, RaftID, Name, Value, Well Label, FOV";
            var DR = OFD.ShowDialog();
            if (DR == DialogResult.OK)
            {
                List<string[]> Table1 = LoadAnnotationImportTable(OFD.FileName);
                if (Table1 == null)
                {
                    txBx_Update.Text = "Problem importing the table, check the headings (& use tab delimited text).\r\n" + OFD.Title;
                    return;
                }
                txBx_Update.Text = AppendAnnotationsFromFile(Table1);
                ParentCalForm.UpdateAll();
            }
        }

        private List<string[]> LoadAnnotationImportTable(string fileName)
        {
            var table1 = new List<string[]>(); string[] tLine;
            using (var SR = new StreamReader(fileName))
            {
                tLine = SR.ReadLine().Split('\t');
                if (tLine.Length < 4) return null;
                while (!SR.EndOfStream)
                {
                    tLine = SR.ReadLine().Split('\t');
                    table1.Add(tLine);
                }
                SR.Close();
            }
            return table1;
        }

        private string AppendAnnotationsFromFile(List<string[]> table1)
        {
            //We are assuming the following column order, the headers have already been removed
            int iPlate = 0;
            int iRaftID = 1;
            int iAName = 2;
            int iAVal = 3;
            int iWell = 4;
            int iFOV = 5;
            string response;
            Debug.Print(ParentCalForm.ImageChk_List.Count.ToString());
            var UpdateTrack = new Dictionary<string, int>();
            var ICPL = ImageCheck_PointList.FromRafts(ParentCalForm.IC_Folder, table1.Select(x => x[1]));
            foreach (string[] Row in table1)
            {
                response = ParentCalForm.AppendImageCheckPoint(Row[iPlate], Row.Length > iWell ? Row[iWell] : "", Row.Length > iFOV ? Row[iFOV] : "", Row[iRaftID], Row[iAName], Row[iAVal], ICPL);
                if (!UpdateTrack.ContainsKey(response)) UpdateTrack.Add(response, 0);
                UpdateTrack[response]++;
            }
            return string.Join("\r\n", UpdateTrack.Select(x => x.Key + "\t" + x.Value));
        }


        #endregion

        #region Crop Around Objects - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        private void btn_ExportCropRegions_Objects_Click(object sender, EventArgs e)
        {
            //for now just do this through segmentation

        }
        #endregion

        private void btn_ExportStitchFOV(object sender, EventArgs e)
        {
            //Josh M's new function for Purva 12/16/2021
            var RIES = RaftImage_Export_Settings.Load();
            foreach (var well in ParentCalForm.IC_Folder.XDCE.Wells.Values)
            {
                ParentCalForm.WellLabel = well.NameAtLevel;
                //Set Currentwell to this well
                for (int FOV = well.FOV_Min; FOV < well.FOV_Max; FOV += 4)
                {
                    //RIES.CropToMaxSize = 1024;
                    //set current field to this field
                    ParentCalForm.CurrentFOV = FOV;
                    ParentCalForm.UpdateAll();
                    string Exported = ParentCalForm.ExportStitchCurrentImageSet(
                        RIES.ExportFolderBase, ParentCalForm.IC_Folder.PlateID,
                        Path.GetFileNameWithoutExtension(ParentCalForm.txBx_ImageName.Text), RIES.ImageExtension, RIES.ResizeMultiplier, (int)RIES.CropToMaxSize);

                    txBx_Update.Text = Exported + "\r\n" + txBx_Update.Text;
                    Application.DoEvents();
                }
            }
            txBx_Update.Text = "Done exporting";
        }

        private void CellImageExpandPixels_TextChanged(object sender, EventArgs e)
        {
            Enable_Save_Settings(false);
        }

        private void DownsampleFraction_TextChanged(object sender, EventArgs e)
        {
            Enable_Save_Settings(false);
        }

        private void ColumnNameForAnnotation_TextChanged(object sender, EventArgs e)
        {
            Enable_Save_Settings(false);
        }

        private void txBx_AnnotationsInfo_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

public static class ExportHelper
{
    public delegate int MYTestDel(int x, int y);

    public static void AccessImages_Parallel(RaftImage_Export_Settings Settings, wvDisplayParams WVParams, INCELL_WV_Notes WVNotes, ImageCheck_PointList PointList, INCELL_Folder ICFolder, MYTestDel Del, BackgroundWorker BW)
    {
        //Raft List, Annotated Rafts, All Rafts, Objects, Fields

        //Assumes you already setup Pointlist with ALL Rafts, or a designation that you want all cells to be analyzed here
        foreach (var Well_Field in PointList.WellFields)
        {
            AccessImages_Internal(Well_Field, Settings, WVParams, WVNotes, PointList, ICFolder, Del, BW);
        }
    }

    public static void AccessImages_Internal(string Well_Field, RaftImage_Export_Settings Settings, wvDisplayParams WVParams, INCELL_WV_Notes WVNotes, ImageCheck_PointList PointList, INCELL_Folder ICFolder, MYTestDel Del, BackgroundWorker BW)
    {

    }
}
