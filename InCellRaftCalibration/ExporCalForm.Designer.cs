﻿
namespace FIVE.RaftCal
{
    partial class ExportCalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            ExportFolderBase = new System.Windows.Forms.TextBox();
            FileName_ByAnnotation = new System.Windows.Forms.CheckBox();
            btn_ExportAllRaftImages = new System.Windows.Forms.Button();
            btn_ExportFromList = new System.Windows.Forms.Button();
            btn_ExportRaftImages = new System.Windows.Forms.Button();
            txBx_Update = new System.Windows.Forms.TextBox();
            FolderName_ByAnnotation = new System.Windows.Forms.CheckBox();
            btn_RaftIDsAnnotated = new System.Windows.Forms.Button();
            ImageExtension = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            Resize_And_MLExport = new System.Windows.Forms.CheckBox();
            Export_4Rotations = new System.Windows.Forms.CheckBox();
            OnlyExportSquare = new System.Windows.Forms.CheckBox();
            PixelList_ExportSize = new System.Windows.Forms.TextBox();
            ResizeMultiplier = new System.Windows.Forms.TextBox();
            Min_AspectRatio_Keep = new System.Windows.Forms.TextBox();
            PixelsExpand = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            btn_SaveSettings = new System.Windows.Forms.Button();
            panelExportSettings = new System.Windows.Forms.Panel();
            label12 = new System.Windows.Forms.Label();
            OnlyExportAnnotationPipeSep = new System.Windows.Forms.TextBox();
            label11 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            FractionUnAnnotated = new System.Windows.Forms.TextBox();
            ColumnNameForAnnotation = new System.Windows.Forms.TextBox();
            label9 = new System.Windows.Forms.Label();
            DownsampleFraction = new System.Windows.Forms.TextBox();
            label8 = new System.Windows.Forms.Label();
            CellImageExpandPixels = new System.Windows.Forms.TextBox();
            OnlyExportFullSize = new System.Windows.Forms.CheckBox();
            label7 = new System.Windows.Forms.Label();
            CropToMaxSize = new System.Windows.Forms.TextBox();
            Include_Fiducial_Rafts = new System.Windows.Forms.CheckBox();
            btn_LoadSettings = new System.Windows.Forms.Button();
            btn_SaveAsSettings = new System.Windows.Forms.Button();
            btn_DefaultSettings = new System.Windows.Forms.Button();
            btn_Cancel = new System.Windows.Forms.Button();
            bgMain = new System.ComponentModel.BackgroundWorker();
            panel2 = new System.Windows.Forms.Panel();
            dataGridView1 = new System.Windows.Forms.DataGridView();
            btn_Annotations_Load = new System.Windows.Forms.Button();
            btn_Annotations_New = new System.Windows.Forms.Button();
            btn_Annotations_SaveAs = new System.Windows.Forms.Button();
            txBx_AnnotationsCurrentSet = new System.Windows.Forms.TextBox();
            txBx_AnnotationsInfo = new System.Windows.Forms.TextBox();
            panel_Annotations = new System.Windows.Forms.Panel();
            btn_Annotations_Open = new System.Windows.Forms.Button();
            toolTip1 = new System.Windows.Forms.ToolTip(components);
            btn_Import_Annotations = new System.Windows.Forms.Button();
            btn_ExportCropReg_Objects = new System.Windows.Forms.Button();
            btn_ExpStitchFOV = new System.Windows.Forms.Button();
            txBx_MoveToTimePoint = new System.Windows.Forms.TextBox();
            panelExportSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            panel_Annotations.SuspendLayout();
            SuspendLayout();
            // 
            // ExportFolderBase
            // 
            ExportFolderBase.Location = new System.Drawing.Point(12, 23);
            ExportFolderBase.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            ExportFolderBase.Name = "ExportFolderBase";
            ExportFolderBase.Size = new System.Drawing.Size(474, 23);
            ExportFolderBase.TabIndex = 0;
            ExportFolderBase.TextChanged += Form_Settings_Changed;
            // 
            // FileName_ByAnnotation
            // 
            FileName_ByAnnotation.AutoSize = true;
            FileName_ByAnnotation.Location = new System.Drawing.Point(250, 81);
            FileName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            FileName_ByAnnotation.Name = "FileName_ByAnnotation";
            FileName_ByAnnotation.Size = new System.Drawing.Size(154, 19);
            FileName_ByAnnotation.TabIndex = 1;
            FileName_ByAnnotation.Text = "FileName_ByAnnotation";
            FileName_ByAnnotation.UseVisualStyleBackColor = true;
            FileName_ByAnnotation.CheckedChanged += Form_Settings_Changed;
            // 
            // btn_ExportAllRaftImages
            // 
            btn_ExportAllRaftImages.Location = new System.Drawing.Point(369, 430);
            btn_ExportAllRaftImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExportAllRaftImages.Name = "btn_ExportAllRaftImages";
            btn_ExportAllRaftImages.Size = new System.Drawing.Size(160, 28);
            btn_ExportAllRaftImages.TabIndex = 95;
            btn_ExportAllRaftImages.Text = "Export All Raft Images";
            btn_ExportAllRaftImages.UseVisualStyleBackColor = true;
            btn_ExportAllRaftImages.Click += btn_ExportAllRaftImages_Click;
            // 
            // btn_ExportFromList
            // 
            btn_ExportFromList.Location = new System.Drawing.Point(369, 396);
            btn_ExportFromList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExportFromList.Name = "btn_ExportFromList";
            btn_ExportFromList.Size = new System.Drawing.Size(160, 28);
            btn_ExportFromList.TabIndex = 94;
            btn_ExportFromList.Text = "Export From List . . ";
            btn_ExportFromList.UseVisualStyleBackColor = true;
            btn_ExportFromList.Click += btn_ExportFromList_Click;
            // 
            // btn_ExportRaftImages
            // 
            btn_ExportRaftImages.Location = new System.Drawing.Point(369, 500);
            btn_ExportRaftImages.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExportRaftImages.Name = "btn_ExportRaftImages";
            btn_ExportRaftImages.Size = new System.Drawing.Size(160, 28);
            btn_ExportRaftImages.TabIndex = 93;
            btn_ExportRaftImages.Text = "Export Annotated Rafts";
            btn_ExportRaftImages.UseVisualStyleBackColor = true;
            btn_ExportRaftImages.Click += btn_ExportRaftImages_Click;
            // 
            // txBx_Update
            // 
            txBx_Update.Location = new System.Drawing.Point(8, 396);
            txBx_Update.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_Update.Multiline = true;
            txBx_Update.Name = "txBx_Update";
            txBx_Update.Size = new System.Drawing.Size(353, 320);
            txBx_Update.TabIndex = 96;
            // 
            // FolderName_ByAnnotation
            // 
            FolderName_ByAnnotation.AutoSize = true;
            FolderName_ByAnnotation.Location = new System.Drawing.Point(250, 57);
            FolderName_ByAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            FolderName_ByAnnotation.Name = "FolderName_ByAnnotation";
            FolderName_ByAnnotation.Size = new System.Drawing.Size(169, 19);
            FolderName_ByAnnotation.TabIndex = 97;
            FolderName_ByAnnotation.Text = "FolderName_ByAnnotation";
            FolderName_ByAnnotation.UseVisualStyleBackColor = true;
            FolderName_ByAnnotation.CheckedChanged += Form_Settings_Changed;
            // 
            // btn_RaftIDsAnnotated
            // 
            btn_RaftIDsAnnotated.Location = new System.Drawing.Point(369, 687);
            btn_RaftIDsAnnotated.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_RaftIDsAnnotated.Name = "btn_RaftIDsAnnotated";
            btn_RaftIDsAnnotated.Size = new System.Drawing.Size(160, 28);
            btn_RaftIDsAnnotated.TabIndex = 98;
            btn_RaftIDsAnnotated.Text = "Annotations to Clipboard";
            btn_RaftIDsAnnotated.UseVisualStyleBackColor = true;
            btn_RaftIDsAnnotated.Click += btn_PlateMetadata;
            // 
            // ImageExtension
            // 
            ImageExtension.Location = new System.Drawing.Point(12, 70);
            ImageExtension.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            ImageExtension.Name = "ImageExtension";
            ImageExtension.Size = new System.Drawing.Size(170, 23);
            ImageExtension.TabIndex = 99;
            ImageExtension.TextChanged += Form_Settings_Changed;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(8, 5);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(104, 15);
            label1.TabIndex = 100;
            label1.Text = "Export Folder Base";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(8, 52);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(67, 15);
            label2.TabIndex = 101;
            label2.Text = "Image Type";
            // 
            // Resize_And_MLExport
            // 
            Resize_And_MLExport.AutoSize = true;
            Resize_And_MLExport.Enabled = false;
            Resize_And_MLExport.Location = new System.Drawing.Point(250, 118);
            Resize_And_MLExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Resize_And_MLExport.Name = "Resize_And_MLExport";
            Resize_And_MLExport.Size = new System.Drawing.Size(141, 19);
            Resize_And_MLExport.TabIndex = 103;
            Resize_And_MLExport.Text = "Resize_And_MLExport";
            Resize_And_MLExport.UseVisualStyleBackColor = true;
            Resize_And_MLExport.CheckedChanged += Form_Settings_Changed;
            // 
            // Export_4Rotations
            // 
            Export_4Rotations.AutoSize = true;
            Export_4Rotations.Location = new System.Drawing.Point(250, 191);
            Export_4Rotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Export_4Rotations.Name = "Export_4Rotations";
            Export_4Rotations.Size = new System.Drawing.Size(121, 19);
            Export_4Rotations.TabIndex = 102;
            Export_4Rotations.Text = "Export_4Rotations";
            Export_4Rotations.UseVisualStyleBackColor = true;
            Export_4Rotations.CheckedChanged += Form_Settings_Changed;
            // 
            // OnlyExportSquare
            // 
            OnlyExportSquare.AutoSize = true;
            OnlyExportSquare.Enabled = false;
            OnlyExportSquare.Location = new System.Drawing.Point(250, 143);
            OnlyExportSquare.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            OnlyExportSquare.Name = "OnlyExportSquare";
            OnlyExportSquare.Size = new System.Drawing.Size(121, 19);
            OnlyExportSquare.TabIndex = 104;
            OnlyExportSquare.Text = "OnlyExportSquare";
            OnlyExportSquare.UseVisualStyleBackColor = true;
            OnlyExportSquare.CheckedChanged += Form_Settings_Changed;
            // 
            // PixelList_ExportSize
            // 
            PixelList_ExportSize.Enabled = false;
            PixelList_ExportSize.Location = new System.Drawing.Point(12, 103);
            PixelList_ExportSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            PixelList_ExportSize.Name = "PixelList_ExportSize";
            PixelList_ExportSize.Size = new System.Drawing.Size(88, 23);
            PixelList_ExportSize.TabIndex = 105;
            PixelList_ExportSize.TextChanged += Form_Settings_Changed;
            // 
            // ResizeMultiplier
            // 
            ResizeMultiplier.Location = new System.Drawing.Point(12, 186);
            ResizeMultiplier.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            ResizeMultiplier.Name = "ResizeMultiplier";
            ResizeMultiplier.Size = new System.Drawing.Size(88, 23);
            ResizeMultiplier.TabIndex = 106;
            ResizeMultiplier.Text = "11";
            ResizeMultiplier.TextChanged += Form_Settings_Changed;
            // 
            // Min_AspectRatio_Keep
            // 
            Min_AspectRatio_Keep.Location = new System.Drawing.Point(12, 158);
            Min_AspectRatio_Keep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Min_AspectRatio_Keep.Name = "Min_AspectRatio_Keep";
            Min_AspectRatio_Keep.Size = new System.Drawing.Size(88, 23);
            Min_AspectRatio_Keep.TabIndex = 107;
            Min_AspectRatio_Keep.TextChanged += Form_Settings_Changed;
            // 
            // PixelsExpand
            // 
            PixelsExpand.Enabled = false;
            PixelsExpand.Location = new System.Drawing.Point(12, 130);
            PixelsExpand.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            PixelsExpand.Name = "PixelsExpand";
            PixelsExpand.Size = new System.Drawing.Size(88, 23);
            PixelsExpand.TabIndex = 108;
            PixelsExpand.TextChanged += Form_Settings_Changed;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Enabled = false;
            label3.Location = new System.Drawing.Point(107, 106);
            label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(109, 15);
            label3.TabIndex = 109;
            label3.Text = "PixelList_ExportSize";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Enabled = false;
            label4.Location = new System.Drawing.Point(107, 134);
            label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(76, 15);
            label4.TabIndex = 110;
            label4.Text = "PixelsExpand";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(107, 162);
            label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(127, 15);
            label5.TabIndex = 111;
            label5.Text = "Min_AspectRatio_Keep";
            toolTip1.SetToolTip(label5, "If the aspect ratio is worse than this number (lower), it is not exported. 1 = perfect square, 0 = straight line");
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(107, 189);
            label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(93, 15);
            label6.TabIndex = 112;
            label6.Text = "Resize Multiplier";
            toolTip1.SetToolTip(label6, "If you enter 0.5, the new image will be half the size of the original. If you enter 2, it is like zooming in if you use the cropping below. Resize is first, crop is second");
            // 
            // btn_SaveSettings
            // 
            btn_SaveSettings.Location = new System.Drawing.Point(12, 270);
            btn_SaveSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_SaveSettings.Name = "btn_SaveSettings";
            btn_SaveSettings.Size = new System.Drawing.Size(126, 28);
            btn_SaveSettings.TabIndex = 113;
            btn_SaveSettings.Text = "Save Settings";
            toolTip1.SetToolTip(btn_SaveSettings, "Locks in the setting typed above so they can be applied to this export");
            btn_SaveSettings.UseVisualStyleBackColor = true;
            btn_SaveSettings.Click += btn_SaveSettings_Click;
            // 
            // panelExportSettings
            // 
            panelExportSettings.Controls.Add(label12);
            panelExportSettings.Controls.Add(OnlyExportAnnotationPipeSep);
            panelExportSettings.Controls.Add(label11);
            panelExportSettings.Controls.Add(label10);
            panelExportSettings.Controls.Add(FractionUnAnnotated);
            panelExportSettings.Controls.Add(ColumnNameForAnnotation);
            panelExportSettings.Controls.Add(label9);
            panelExportSettings.Controls.Add(DownsampleFraction);
            panelExportSettings.Controls.Add(label8);
            panelExportSettings.Controls.Add(CellImageExpandPixels);
            panelExportSettings.Controls.Add(OnlyExportFullSize);
            panelExportSettings.Controls.Add(label7);
            panelExportSettings.Controls.Add(CropToMaxSize);
            panelExportSettings.Controls.Add(Include_Fiducial_Rafts);
            panelExportSettings.Controls.Add(btn_LoadSettings);
            panelExportSettings.Controls.Add(btn_SaveAsSettings);
            panelExportSettings.Controls.Add(btn_DefaultSettings);
            panelExportSettings.Controls.Add(PixelList_ExportSize);
            panelExportSettings.Controls.Add(btn_SaveSettings);
            panelExportSettings.Controls.Add(ResizeMultiplier);
            panelExportSettings.Controls.Add(OnlyExportSquare);
            panelExportSettings.Controls.Add(label6);
            panelExportSettings.Controls.Add(Resize_And_MLExport);
            panelExportSettings.Controls.Add(Min_AspectRatio_Keep);
            panelExportSettings.Controls.Add(Export_4Rotations);
            panelExportSettings.Controls.Add(label5);
            panelExportSettings.Controls.Add(FolderName_ByAnnotation);
            panelExportSettings.Controls.Add(label2);
            panelExportSettings.Controls.Add(PixelsExpand);
            panelExportSettings.Controls.Add(label1);
            panelExportSettings.Controls.Add(label4);
            panelExportSettings.Controls.Add(ImageExtension);
            panelExportSettings.Controls.Add(FileName_ByAnnotation);
            panelExportSettings.Controls.Add(label3);
            panelExportSettings.Controls.Add(ExportFolderBase);
            panelExportSettings.Location = new System.Drawing.Point(8, 87);
            panelExportSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            panelExportSettings.Name = "panelExportSettings";
            panelExportSettings.Size = new System.Drawing.Size(520, 302);
            panelExportSettings.TabIndex = 114;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(449, 96);
            label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(64, 15);
            label12.TabIndex = 130;
            label12.Text = "Only Anno";
            // 
            // OnlyExportAnnotationPipeSep
            // 
            OnlyExportAnnotationPipeSep.Location = new System.Drawing.Point(416, 114);
            OnlyExportAnnotationPipeSep.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            OnlyExportAnnotationPipeSep.Name = "OnlyExportAnnotationPipeSep";
            OnlyExportAnnotationPipeSep.Size = new System.Drawing.Size(99, 23);
            OnlyExportAnnotationPipeSep.TabIndex = 129;
            toolTip1.SetToolTip(OnlyExportAnnotationPipeSep, "Pipe-separated list of annotations to export, all other annotations will be ignored");
            OnlyExportAnnotationPipeSep.TextChanged += Form_Settings_Changed;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(449, 52);
            label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(57, 15);
            label11.TabIndex = 128;
            label11.Text = "Anno Col";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(428, 172);
            label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(78, 30);
            label10.TabIndex = 127;
            label10.Text = "Fraction\r\nUnAnnotated";
            toolTip1.SetToolTip(label10, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are this number in pixels");
            // 
            // FractionUnAnnotated
            // 
            FractionUnAnnotated.Location = new System.Drawing.Point(416, 205);
            FractionUnAnnotated.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            FractionUnAnnotated.Name = "FractionUnAnnotated";
            FractionUnAnnotated.Size = new System.Drawing.Size(98, 23);
            FractionUnAnnotated.TabIndex = 126;
            // 
            // ColumnNameForAnnotation
            // 
            ColumnNameForAnnotation.Location = new System.Drawing.Point(416, 68);
            ColumnNameForAnnotation.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            ColumnNameForAnnotation.Name = "ColumnNameForAnnotation";
            ColumnNameForAnnotation.Size = new System.Drawing.Size(99, 23);
            ColumnNameForAnnotation.TabIndex = 125;
            toolTip1.SetToolTip(ColumnNameForAnnotation, "Annotation Column to Use");
            ColumnNameForAnnotation.TextChanged += ColumnNameForAnnotation_TextChanged;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(361, 246);
            label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(142, 15);
            label9.TabIndex = 124;
            label9.Text = "Cell Downsampling (1 all)";
            toolTip1.SetToolTip(label9, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are this number in pixels");
            // 
            // DownsampleFraction
            // 
            DownsampleFraction.Location = new System.Drawing.Point(265, 242);
            DownsampleFraction.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            DownsampleFraction.Name = "DownsampleFraction";
            DownsampleFraction.Size = new System.Drawing.Size(88, 23);
            DownsampleFraction.TabIndex = 123;
            DownsampleFraction.TextChanged += DownsampleFraction_TextChanged;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(108, 245);
            label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(135, 15);
            label8.TabIndex = 122;
            label8.Text = "CellImage Expand Pixels";
            toolTip1.SetToolTip(label8, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are this number in pixels");
            // 
            // CellImageExpandPixels
            // 
            CellImageExpandPixels.Location = new System.Drawing.Point(12, 241);
            CellImageExpandPixels.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            CellImageExpandPixels.Name = "CellImageExpandPixels";
            CellImageExpandPixels.Size = new System.Drawing.Size(88, 23);
            CellImageExpandPixels.TabIndex = 121;
            CellImageExpandPixels.TextChanged += CellImageExpandPixels_TextChanged;
            // 
            // OnlyExportFullSize
            // 
            OnlyExportFullSize.AutoSize = true;
            OnlyExportFullSize.Location = new System.Drawing.Point(250, 167);
            OnlyExportFullSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            OnlyExportFullSize.Name = "OnlyExportFullSize";
            OnlyExportFullSize.Size = new System.Drawing.Size(124, 19);
            OnlyExportFullSize.TabIndex = 120;
            OnlyExportFullSize.Text = "OnlyExportFullSize";
            OnlyExportFullSize.UseVisualStyleBackColor = true;
            OnlyExportFullSize.CheckedChanged += Form_Settings_Changed;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(108, 217);
            label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(88, 15);
            label7.TabIndex = 119;
            label7.Text = "CropToMaxSize";
            toolTip1.SetToolTip(label7, "Enter -1 to disable, will crop the image (only smaller) so the dimensions are this number in pixels");
            // 
            // CropToMaxSize
            // 
            CropToMaxSize.Location = new System.Drawing.Point(12, 213);
            CropToMaxSize.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            CropToMaxSize.Name = "CropToMaxSize";
            CropToMaxSize.Size = new System.Drawing.Size(88, 23);
            CropToMaxSize.TabIndex = 118;
            CropToMaxSize.TextChanged += Form_Settings_Changed;
            // 
            // Include_Fiducial_Rafts
            // 
            Include_Fiducial_Rafts.AutoSize = true;
            Include_Fiducial_Rafts.Location = new System.Drawing.Point(250, 215);
            Include_Fiducial_Rafts.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Include_Fiducial_Rafts.Name = "Include_Fiducial_Rafts";
            Include_Fiducial_Rafts.Size = new System.Drawing.Size(142, 19);
            Include_Fiducial_Rafts.TabIndex = 117;
            Include_Fiducial_Rafts.Text = "Include_Fiducial_Rafts";
            Include_Fiducial_Rafts.UseVisualStyleBackColor = true;
            Include_Fiducial_Rafts.CheckedChanged += Form_Settings_Changed;
            // 
            // btn_LoadSettings
            // 
            btn_LoadSettings.Location = new System.Drawing.Point(416, 270);
            btn_LoadSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_LoadSettings.Name = "btn_LoadSettings";
            btn_LoadSettings.Size = new System.Drawing.Size(82, 28);
            btn_LoadSettings.TabIndex = 116;
            btn_LoadSettings.Text = "Load . .";
            btn_LoadSettings.UseVisualStyleBackColor = true;
            btn_LoadSettings.Click += btn_LoadSettings_Click;
            // 
            // btn_SaveAsSettings
            // 
            btn_SaveAsSettings.Location = new System.Drawing.Point(328, 270);
            btn_SaveAsSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_SaveAsSettings.Name = "btn_SaveAsSettings";
            btn_SaveAsSettings.Size = new System.Drawing.Size(82, 28);
            btn_SaveAsSettings.TabIndex = 115;
            btn_SaveAsSettings.Text = "Save As . .";
            btn_SaveAsSettings.UseVisualStyleBackColor = true;
            btn_SaveAsSettings.Click += btn_SaveAsSettings_Click;
            // 
            // btn_DefaultSettings
            // 
            btn_DefaultSettings.Location = new System.Drawing.Point(250, 270);
            btn_DefaultSettings.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_DefaultSettings.Name = "btn_DefaultSettings";
            btn_DefaultSettings.Size = new System.Drawing.Size(71, 28);
            btn_DefaultSettings.TabIndex = 114;
            btn_DefaultSettings.Text = "Defaults";
            btn_DefaultSettings.UseVisualStyleBackColor = true;
            btn_DefaultSettings.Click += btn_DefaultSettings_Click;
            // 
            // btn_Cancel
            // 
            btn_Cancel.Location = new System.Drawing.Point(369, 625);
            btn_Cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Cancel.Name = "btn_Cancel";
            btn_Cancel.Size = new System.Drawing.Size(94, 28);
            btn_Cancel.TabIndex = 115;
            btn_Cancel.Text = "Stop / Cancel";
            btn_Cancel.UseVisualStyleBackColor = true;
            btn_Cancel.Click += btn_Cancel_Click;
            // 
            // bgMain
            // 
            bgMain.WorkerReportsProgress = true;
            bgMain.WorkerSupportsCancellation = true;
            bgMain.DoWork += bgMain_DoWork;
            bgMain.ProgressChanged += bgMain_ProgressChanged;
            bgMain.RunWorkerCompleted += bgMain_RunWorkerCompleted;
            // 
            // panel2
            // 
            panel2.Location = new System.Drawing.Point(8, 7);
            panel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(520, 73);
            panel2.TabIndex = 116;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new System.Drawing.Point(4, 3);
            dataGridView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new System.Drawing.Size(468, 181);
            dataGridView1.TabIndex = 0;
            // 
            // btn_Annotations_Load
            // 
            btn_Annotations_Load.Location = new System.Drawing.Point(92, 192);
            btn_Annotations_Load.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Annotations_Load.Name = "btn_Annotations_Load";
            btn_Annotations_Load.Size = new System.Drawing.Size(82, 28);
            btn_Annotations_Load.TabIndex = 119;
            btn_Annotations_Load.Text = "Load . .";
            btn_Annotations_Load.UseVisualStyleBackColor = true;
            btn_Annotations_Load.Click += btn_Annotations_Load_Click;
            // 
            // btn_Annotations_New
            // 
            btn_Annotations_New.Enabled = false;
            btn_Annotations_New.Location = new System.Drawing.Point(4, 192);
            btn_Annotations_New.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Annotations_New.Name = "btn_Annotations_New";
            btn_Annotations_New.Size = new System.Drawing.Size(82, 28);
            btn_Annotations_New.TabIndex = 118;
            btn_Annotations_New.Text = "New";
            btn_Annotations_New.UseVisualStyleBackColor = true;
            btn_Annotations_New.Click += btn_Annotations_New_Click;
            // 
            // btn_Annotations_SaveAs
            // 
            btn_Annotations_SaveAs.Location = new System.Drawing.Point(181, 192);
            btn_Annotations_SaveAs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Annotations_SaveAs.Name = "btn_Annotations_SaveAs";
            btn_Annotations_SaveAs.Size = new System.Drawing.Size(82, 28);
            btn_Annotations_SaveAs.TabIndex = 120;
            btn_Annotations_SaveAs.Text = "Save As . .";
            btn_Annotations_SaveAs.UseVisualStyleBackColor = true;
            btn_Annotations_SaveAs.Click += btn_Annotations_SaveAs_Click;
            // 
            // txBx_AnnotationsCurrentSet
            // 
            txBx_AnnotationsCurrentSet.Location = new System.Drawing.Point(4, 226);
            txBx_AnnotationsCurrentSet.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_AnnotationsCurrentSet.Name = "txBx_AnnotationsCurrentSet";
            txBx_AnnotationsCurrentSet.ReadOnly = true;
            txBx_AnnotationsCurrentSet.Size = new System.Drawing.Size(467, 23);
            txBx_AnnotationsCurrentSet.TabIndex = 121;
            // 
            // txBx_AnnotationsInfo
            // 
            txBx_AnnotationsInfo.Location = new System.Drawing.Point(4, 256);
            txBx_AnnotationsInfo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_AnnotationsInfo.Multiline = true;
            txBx_AnnotationsInfo.Name = "txBx_AnnotationsInfo";
            txBx_AnnotationsInfo.Size = new System.Drawing.Size(467, 337);
            txBx_AnnotationsInfo.TabIndex = 122;
            // 
            // panel_Annotations
            // 
            panel_Annotations.Controls.Add(btn_Annotations_Open);
            panel_Annotations.Controls.Add(dataGridView1);
            panel_Annotations.Controls.Add(btn_Annotations_New);
            panel_Annotations.Controls.Add(txBx_AnnotationsInfo);
            panel_Annotations.Controls.Add(btn_Annotations_Load);
            panel_Annotations.Controls.Add(txBx_AnnotationsCurrentSet);
            panel_Annotations.Controls.Add(btn_Annotations_SaveAs);
            panel_Annotations.Location = new System.Drawing.Point(536, 7);
            panel_Annotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            panel_Annotations.Name = "panel_Annotations";
            panel_Annotations.Size = new System.Drawing.Size(478, 600);
            panel_Annotations.TabIndex = 118;
            // 
            // btn_Annotations_Open
            // 
            btn_Annotations_Open.Location = new System.Drawing.Point(270, 192);
            btn_Annotations_Open.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Annotations_Open.Name = "btn_Annotations_Open";
            btn_Annotations_Open.Size = new System.Drawing.Size(88, 28);
            btn_Annotations_Open.TabIndex = 123;
            btn_Annotations_Open.Text = "Open/Edit";
            btn_Annotations_Open.UseVisualStyleBackColor = true;
            btn_Annotations_Open.Click += btn_Annotations_Open_Click;
            // 
            // btn_Import_Annotations
            // 
            btn_Import_Annotations.Location = new System.Drawing.Point(369, 655);
            btn_Import_Annotations.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_Import_Annotations.Name = "btn_Import_Annotations";
            btn_Import_Annotations.Size = new System.Drawing.Size(126, 28);
            btn_Import_Annotations.TabIndex = 119;
            btn_Import_Annotations.Text = "Import Annotations";
            toolTip1.SetToolTip(btn_Import_Annotations, "This will append the annotations you select from a file into this PlateID. Select a .txt file that has 6 columns = PlateID, RaftID, Name, Value, Well Label, FOV");
            btn_Import_Annotations.UseVisualStyleBackColor = true;
            btn_Import_Annotations.Click += btn_Import_Annotations_Click;
            // 
            // btn_ExportCropReg_Objects
            // 
            btn_ExportCropReg_Objects.Location = new System.Drawing.Point(369, 465);
            btn_ExportCropReg_Objects.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExportCropReg_Objects.Name = "btn_ExportCropReg_Objects";
            btn_ExportCropReg_Objects.Size = new System.Drawing.Size(160, 28);
            btn_ExportCropReg_Objects.TabIndex = 120;
            btn_ExportCropReg_Objects.Text = "Export All Seg Images";
            toolTip1.SetToolTip(btn_ExportCropReg_Objects, "Uses the Segmentation Settings and \"OBJ\" selection to crop out around a single nuclei or object");
            btn_ExportCropReg_Objects.UseVisualStyleBackColor = true;
            btn_ExportCropReg_Objects.Click += btn_ExportCropRegions_Objects_Click;
            // 
            // btn_ExpStitchFOV
            // 
            btn_ExpStitchFOV.Location = new System.Drawing.Point(422, 534);
            btn_ExpStitchFOV.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            btn_ExpStitchFOV.Name = "btn_ExpStitchFOV";
            btn_ExpStitchFOV.Size = new System.Drawing.Size(106, 28);
            btn_ExpStitchFOV.TabIndex = 121;
            btn_ExpStitchFOV.Text = "Exp Stitch Fov";
            btn_ExpStitchFOV.UseVisualStyleBackColor = true;
            btn_ExpStitchFOV.Click += btn_ExportStitchFOV;
            // 
            // txBx_MoveToTimePoint
            // 
            txBx_MoveToTimePoint.Location = new System.Drawing.Point(369, 539);
            txBx_MoveToTimePoint.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txBx_MoveToTimePoint.Name = "txBx_MoveToTimePoint";
            txBx_MoveToTimePoint.Size = new System.Drawing.Size(46, 23);
            txBx_MoveToTimePoint.TabIndex = 121;
            txBx_MoveToTimePoint.Text = "4";
            // 
            // ExportCalForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1022, 764);
            Controls.Add(txBx_MoveToTimePoint);
            Controls.Add(btn_ExpStitchFOV);
            Controls.Add(btn_ExportCropReg_Objects);
            Controls.Add(btn_Import_Annotations);
            Controls.Add(panel_Annotations);
            Controls.Add(panel2);
            Controls.Add(btn_Cancel);
            Controls.Add(panelExportSettings);
            Controls.Add(btn_RaftIDsAnnotated);
            Controls.Add(txBx_Update);
            Controls.Add(btn_ExportAllRaftImages);
            Controls.Add(btn_ExportFromList);
            Controls.Add(btn_ExportRaftImages);
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "ExportCalForm";
            Text = "ExporCalForm";
            FormClosing += ExporCalForm_FormClosing;
            Load += ExporCalForm_Load;
            panelExportSettings.ResumeLayout(false);
            panelExportSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            panel_Annotations.ResumeLayout(false);
            panel_Annotations.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox ExportFolderBase;
        private System.Windows.Forms.CheckBox FileName_ByAnnotation;
        private System.Windows.Forms.Button btn_ExportAllRaftImages;
        private System.Windows.Forms.Button btn_ExportFromList;
        private System.Windows.Forms.Button btn_ExportRaftImages;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.CheckBox FolderName_ByAnnotation;
        private System.Windows.Forms.Button btn_RaftIDsAnnotated;
        private System.Windows.Forms.TextBox ImageExtension;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox Resize_And_MLExport;
        private System.Windows.Forms.CheckBox Export_4Rotations;
        private System.Windows.Forms.CheckBox OnlyExportSquare;
        private System.Windows.Forms.TextBox PixelList_ExportSize;
        private System.Windows.Forms.TextBox ResizeMultiplier;
        private System.Windows.Forms.TextBox Min_AspectRatio_Keep;
        private System.Windows.Forms.TextBox PixelsExpand;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.Panel panelExportSettings;
        private System.Windows.Forms.Button btn_Cancel;
        private System.ComponentModel.BackgroundWorker bgMain;
        private System.Windows.Forms.Button btn_LoadSettings;
        private System.Windows.Forms.Button btn_SaveAsSettings;
        private System.Windows.Forms.Button btn_DefaultSettings;
        private System.Windows.Forms.CheckBox Include_Fiducial_Rafts;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Annotations_New;
        private System.Windows.Forms.Button btn_Annotations_Load;
        private System.Windows.Forms.Button btn_Annotations_SaveAs;
        private System.Windows.Forms.TextBox txBx_AnnotationsCurrentSet;
        private System.Windows.Forms.TextBox txBx_AnnotationsInfo;
        private System.Windows.Forms.Panel panel_Annotations;
        private System.Windows.Forms.Button btn_Annotations_Open;
        private System.Windows.Forms.TextBox CropToMaxSize;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox OnlyExportFullSize;
        private System.Windows.Forms.Button btn_Import_Annotations;
        private System.Windows.Forms.Button btn_ExportCropReg_Objects;
        private System.Windows.Forms.Button btn_ExpStitchFOV;
        private System.Windows.Forms.TextBox txBx_MoveToTimePoint;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CellImageExpandPixels;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox DownsampleFraction;
        private System.Windows.Forms.TextBox ColumnNameForAnnotation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox FractionUnAnnotated;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox OnlyExportAnnotationPipeSep;
        private System.Windows.Forms.Label label11;
    }
}