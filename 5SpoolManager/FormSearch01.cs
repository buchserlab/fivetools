﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5SpoolManager
{
    public partial class FormSearch01 : Form
    {
        public DT_Index index;
        public IEnumerable<string> topKeys;
        public IEnumerable<string> allKeys;
        public int MaxResults = 1000;

        public FormSearch01()
        {
            InitializeComponent();
            listBox_Results.SelectionMode = SelectionMode.MultiExtended;
        }

        private void FormSearch01_Shown(object sender, EventArgs e)
        {
            this.Text = "LOADING . . ."; Application.DoEvents();

            index = DT_Index.Load();

            CleanUp();
        }

        private void CleanUp()
        {
            this.Text = "Cleaning . . ."; Application.DoEvents();

            topKeys = index.WordIndex.Keys.OrderByDescending(x => index.WordIndex[x].Count).Take(MaxResults);
            var ToRemove = new List<string>() { 
                "LIF", "TILESCAN", "HACKETT", "SUMMERS", "BERMINGHAM", "ASTRICKLAND", "SASAKI", "ACQ", "DATE-TIME", "ANEINER",
                "CH00", "MASKLIST", "MASKS", "ISBELL", "60X", "100X", "ASTRICKLAND HFD", "ASTRICKLAND NMNAT2, KSIMBURGER", "TARAKI", "ASSAY",
                "MAO", "CONC", "10X", "20X", "CAITLIN", "DAPI", "2ND", "LIF TILESCAN", "LCMSMS", "TILESCAN 001", "TILESCAN 002","TILESCAN 003",
                "SASAKI LCMSMS", "ACQ DATE-TIME", "MERGING001SNAPSHOT1", "MERGING001SNAPSHOT2", "MERGING001SNAPSHOT3", "001 MERGING001SNAPSHOT1", "001 MERGING001SNAPSHOT2", "001 MERGING001SNAPSHOT3",
                "CALC", "CALC CONC", "072221 ENDOMFN2MUT", "HACKETT HFD", "NO1 LIF", "NO2 LIF", "NO3 LIF", "BAM" ,"072221 MTG","STAIN","TONG",
                "072221 MVG","05-06-22 CD38","SOUCE","SOUCE DEG","TILESCAN 005","2019-BLANK","TIF","04-26-22 INTESTINE","10X DAPI","MERGING001SNAPSHOT1 CH00","MERGING001SNAPSHOT3 CH00","MERGING001SNAPSHOT2 CH00","//WWW"
            };
            foreach (string key in index.WordIndex.Keys)
            {
                if (key.Length < 7)
                    if (long.TryParse(key, out _)) ToRemove.Add(key);
                if (Regex.IsMatch(key, @"^\d{1,5}[ -_/]\d{1,5}$")) ToRemove.Add(key);
                if (Regex.IsMatch(key, @"^\d{1,5}[ -_/]\d{1,5}[ -_/]\d{1,5}$")) ToRemove.Add(key);
                if (Regex.IsMatch(key, @"DRG-\d{1,4}$")) ToRemove.Add(key);
                if (Regex.IsMatch(key, @"MASK\d{1,4}$")) ToRemove.Add(key);
            }
            foreach (string key in ToRemove)
            {
                if (index.WordIndex.ContainsKey(key)) index.WordIndex.Remove(key);
            }
            //Index only stores keys as upper case
            allKeys = index.WordIndex.Keys.OrderByDescending(x => index.WordIndex[x].Count);
            topKeys = allKeys.Take(MaxResults);

            listBox_Results.Items.Clear();
            listBox_Results.Items.AddRange(topKeys.ToArray());

            this.Text = "File System Index";
        }

        private void txBx_Search_TextChanged(object sender, EventArgs e)
        {
            IEnumerable<string> Res = topKeys;
            string search = txBx_Search.Text.ToUpper().Trim();
            if (search != "")
            {
                string[] searchParts = search.Split(' ');
                Res = searchParts.SelectMany(part => allKeys.Where(x => x.StartsWith(part))).Distinct().Take(MaxResults);
            }
            listBox_Results.Items.Clear();
            listBox_Results.Items.AddRange(Res.ToArray());
        }

        private void listBox_Results_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox_Results.SelectedIndex < 0) return;
            string find = (string)listBox_Results.SelectedItem;
            if (!index.WordIndex.ContainsKey(find)) return;
            var vals = index.WordIndex[find];

            listBox_Files.Items.Clear();
            listBox_Files.Items.AddRange(vals.Select(x => x.filename).ToArray());
        }

        private void btn_CopySelected_Click(object sender, EventArgs e)
        {
            //CleanUp();
            string res = string.Join("\r\n", listBox_Results.SelectedItems.Cast<string>());
            Clipboard.SetText(res);
        }

        private void btn_OpenFolder_Click(object sender, EventArgs e)
        {
            string sel = listBox_Files.SelectedItem.ToString();
            string folderPath = Directory.GetParent(sel).FullName;
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe" //Testing
                };
                Process.Start(startInfo);
            }
        }

        private void btn_OpenFile_Click(object sender, EventArgs e)
        {
            if (listBox_Files.SelectedIndex < 0) return;
            string sel = listBox_Files.SelectedItem.ToString();
            if (File.Exists(sel))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = sel,
                    FileName = "explorer.exe" //Testing
                };
                Process.Start(startInfo);
            }
        }

        
    }
}
