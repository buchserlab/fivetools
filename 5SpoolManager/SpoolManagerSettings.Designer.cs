﻿namespace _5SpoolManager
{
    partial class SpoolManagerSettingsForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnStart = new Button();
            btnStop = new Button();
            Folder_Fast = new TextBox();
            Folder_Mid = new TextBox();
            Folder_Archive = new TextBox();
            FractionFull_Fast = new TextBox();
            FractionFull_Mid = new TextBox();
            HourToRun = new TextBox();
            txBx_Update = new TextBox();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            lblSearchExp_Fast = new LinkLabel();
            lblSearchExp_Mid = new LinkLabel();
            lblSearchExp_Archive = new LinkLabel();
            btn_StartSchedule = new Button();
            btnOpenLogs = new Button();
            label7 = new Label();
            txBx_FolderPlaceHold = new TextBox();
            btnPlaceHold = new Button();
            txBx_HoldDate = new TextBox();
            label8 = new Label();
            Index_Label = new LinkLabel();
            lbl_IndexOpen = new LinkLabel();
            SuspendLayout();
            // 
            // btnStart
            // 
            btnStart.Location = new Point(8, 172);
            btnStart.Name = "btnStart";
            btnStart.Size = new Size(85, 35);
            btnStart.TabIndex = 0;
            btnStart.Text = "Start";
            btnStart.UseVisualStyleBackColor = true;
            btnStart.Click += btnStart_Click;
            // 
            // btnStop
            // 
            btnStop.Enabled = false;
            btnStop.Location = new Point(99, 172);
            btnStop.Name = "btnStop";
            btnStop.Size = new Size(85, 35);
            btnStop.TabIndex = 1;
            btnStop.Text = "Stop";
            btnStop.UseVisualStyleBackColor = true;
            btnStop.Click += btnStop_Click;
            // 
            // Folder_Fast
            // 
            Folder_Fast.Location = new Point(8, 38);
            Folder_Fast.Name = "Folder_Fast";
            Folder_Fast.Size = new Size(239, 23);
            Folder_Fast.TabIndex = 2;
            // 
            // Folder_Mid
            // 
            Folder_Mid.Location = new Point(8, 88);
            Folder_Mid.Name = "Folder_Mid";
            Folder_Mid.Size = new Size(239, 23);
            Folder_Mid.TabIndex = 3;
            // 
            // Folder_Archive
            // 
            Folder_Archive.Location = new Point(8, 139);
            Folder_Archive.Name = "Folder_Archive";
            Folder_Archive.Size = new Size(239, 23);
            Folder_Archive.TabIndex = 4;
            // 
            // FractionFull_Fast
            // 
            FractionFull_Fast.Location = new Point(257, 38);
            FractionFull_Fast.Name = "FractionFull_Fast";
            FractionFull_Fast.Size = new Size(100, 23);
            FractionFull_Fast.TabIndex = 5;
            // 
            // FractionFull_Mid
            // 
            FractionFull_Mid.Location = new Point(257, 88);
            FractionFull_Mid.Name = "FractionFull_Mid";
            FractionFull_Mid.Size = new Size(100, 23);
            FractionFull_Mid.TabIndex = 6;
            // 
            // HourToRun
            // 
            HourToRun.Location = new Point(257, 139);
            HourToRun.Name = "HourToRun";
            HourToRun.Size = new Size(100, 23);
            HourToRun.TabIndex = 7;
            // 
            // txBx_Update
            // 
            txBx_Update.Location = new Point(373, 12);
            txBx_Update.Multiline = true;
            txBx_Update.Name = "txBx_Update";
            txBx_Update.Size = new Size(629, 320);
            txBx_Update.TabIndex = 8;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(8, 20);
            label1.Name = "label1";
            label1.Size = new Size(66, 15);
            label1.TabIndex = 9;
            label1.Text = "Folder_Fast";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(8, 70);
            label2.Name = "label2";
            label2.Size = new Size(66, 15);
            label2.TabIndex = 10;
            label2.Text = "Folder_Mid";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(8, 121);
            label3.Name = "label3";
            label3.Size = new Size(85, 15);
            label3.TabIndex = 11;
            label3.Text = "Folder_Archive";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(257, 20);
            label4.Name = "label4";
            label4.Size = new Size(95, 15);
            label4.TabIndex = 12;
            label4.Text = "FractionFull_Fast";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(257, 70);
            label5.Name = "label5";
            label5.Size = new Size(95, 15);
            label5.TabIndex = 13;
            label5.Text = "FractionFull_Mid";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(257, 121);
            label6.Name = "label6";
            label6.Size = new Size(102, 15);
            label6.TabIndex = 14;
            label6.Text = "HourToRun (24 H)";
            // 
            // lblSearchExp_Fast
            // 
            lblSearchExp_Fast.AutoSize = true;
            lblSearchExp_Fast.Location = new Point(162, 20);
            lblSearchExp_Fast.Name = "lblSearchExp_Fast";
            lblSearchExp_Fast.Size = new Size(69, 15);
            lblSearchExp_Fast.TabIndex = 15;
            lblSearchExp_Fast.TabStop = true;
            lblSearchExp_Fast.Text = "Search Exps";
            // 
            // lblSearchExp_Mid
            // 
            lblSearchExp_Mid.AutoSize = true;
            lblSearchExp_Mid.Location = new Point(162, 70);
            lblSearchExp_Mid.Name = "lblSearchExp_Mid";
            lblSearchExp_Mid.Size = new Size(69, 15);
            lblSearchExp_Mid.TabIndex = 16;
            lblSearchExp_Mid.TabStop = true;
            lblSearchExp_Mid.Text = "Search Exps";
            // 
            // lblSearchExp_Archive
            // 
            lblSearchExp_Archive.AutoSize = true;
            lblSearchExp_Archive.Location = new Point(162, 121);
            lblSearchExp_Archive.Name = "lblSearchExp_Archive";
            lblSearchExp_Archive.Size = new Size(69, 15);
            lblSearchExp_Archive.TabIndex = 17;
            lblSearchExp_Archive.TabStop = true;
            lblSearchExp_Archive.Text = "Search Exps";
            // 
            // btn_StartSchedule
            // 
            btn_StartSchedule.Location = new Point(8, 213);
            btn_StartSchedule.Name = "btn_StartSchedule";
            btn_StartSchedule.Size = new Size(119, 35);
            btn_StartSchedule.TabIndex = 18;
            btn_StartSchedule.Text = "Start Schedule";
            btn_StartSchedule.UseVisualStyleBackColor = true;
            btn_StartSchedule.Click += btn_StartSchedule_Click;
            // 
            // btnOpenLogs
            // 
            btnOpenLogs.Location = new Point(272, 172);
            btnOpenLogs.Name = "btnOpenLogs";
            btnOpenLogs.Size = new Size(85, 35);
            btnOpenLogs.TabIndex = 19;
            btnOpenLogs.Text = "Open Logs";
            btnOpenLogs.UseVisualStyleBackColor = true;
            btnOpenLogs.Click += btnOpenLogs_Click;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(8, 290);
            label7.Name = "label7";
            label7.Size = new Size(156, 15);
            label7.TabIndex = 21;
            label7.Text = "Folder Path for Placing Hold";
            // 
            // txBx_FolderPlaceHold
            // 
            txBx_FolderPlaceHold.Location = new Point(8, 308);
            txBx_FolderPlaceHold.Name = "txBx_FolderPlaceHold";
            txBx_FolderPlaceHold.Size = new Size(271, 23);
            txBx_FolderPlaceHold.TabIndex = 20;
            txBx_FolderPlaceHold.TextChanged += txBx_FolderPlaceHold_TextChanged;
            // 
            // btnPlaceHold
            // 
            btnPlaceHold.Enabled = false;
            btnPlaceHold.Location = new Point(179, 270);
            btnPlaceHold.Name = "btnPlaceHold";
            btnPlaceHold.Size = new Size(100, 35);
            btnPlaceHold.TabIndex = 22;
            btnPlaceHold.Text = "Place Hold";
            btnPlaceHold.UseVisualStyleBackColor = true;
            btnPlaceHold.Click += btnPlaceHold_Click;
            // 
            // txBx_HoldDate
            // 
            txBx_HoldDate.Location = new Point(285, 308);
            txBx_HoldDate.Name = "txBx_HoldDate";
            txBx_HoldDate.Size = new Size(72, 23);
            txBx_HoldDate.TabIndex = 23;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(285, 290);
            label8.Name = "label8";
            label8.Size = new Size(60, 15);
            label8.TabIndex = 24;
            label8.Text = "Hold Date";
            // 
            // Index_Label
            // 
            Index_Label.AutoSize = true;
            Index_Label.Location = new Point(293, 213);
            Index_Label.Name = "Index_Label";
            Index_Label.Size = new Size(74, 15);
            Index_Label.TabIndex = 25;
            Index_Label.TabStop = true;
            Index_Label.Text = "Index Search";
            Index_Label.Click += Index_Label_Click;
            // 
            // lbl_IndexOpen
            // 
            lbl_IndexOpen.AutoSize = true;
            lbl_IndexOpen.Location = new Point(299, 233);
            lbl_IndexOpen.Name = "lbl_IndexOpen";
            lbl_IndexOpen.Size = new Size(68, 15);
            lbl_IndexOpen.TabIndex = 26;
            lbl_IndexOpen.TabStop = true;
            lbl_IndexOpen.Text = "Index Open";
            lbl_IndexOpen.Click += lbl_IndexOpen_Click;
            // 
            // SpoolManagerSettingsForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1014, 344);
            Controls.Add(lbl_IndexOpen);
            Controls.Add(Index_Label);
            Controls.Add(label8);
            Controls.Add(txBx_HoldDate);
            Controls.Add(btnPlaceHold);
            Controls.Add(label7);
            Controls.Add(txBx_FolderPlaceHold);
            Controls.Add(btnOpenLogs);
            Controls.Add(btn_StartSchedule);
            Controls.Add(lblSearchExp_Archive);
            Controls.Add(lblSearchExp_Mid);
            Controls.Add(lblSearchExp_Fast);
            Controls.Add(label6);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(txBx_Update);
            Controls.Add(HourToRun);
            Controls.Add(FractionFull_Mid);
            Controls.Add(FractionFull_Fast);
            Controls.Add(Folder_Archive);
            Controls.Add(Folder_Mid);
            Controls.Add(Folder_Fast);
            Controls.Add(btnStop);
            Controls.Add(btnStart);
            Name = "SpoolManagerSettingsForm";
            Text = "FIVE Spool Manager";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnStart;
        private Button btnStop;
        private TextBox Folder_Fast;
        private TextBox Folder_Mid;
        private TextBox Folder_Archive;
        private TextBox FractionFull_Fast;
        private TextBox FractionFull_Mid;
        private TextBox HourToRun;
        private TextBox txBx_Update;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private LinkLabel lblSearchExp_Fast;
        private LinkLabel lblSearchExp_Mid;
        private LinkLabel lblSearchExp_Archive;
        private Button btn_StartSchedule;
        private Button btnOpenLogs;
        private Label label7;
        private TextBox txBx_FolderPlaceHold;
        private Button btnPlaceHold;
        private TextBox txBx_HoldDate;
        private Label label8;
        private LinkLabel Index_Label;
        private LinkLabel lbl_IndexOpen;
    }
}