﻿namespace _5SpoolManager
{
    partial class FormSearch01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txBx_Search = new TextBox();
            btn_CopySelected = new Button();
            btn_OpenFolder = new Button();
            btn_OpenFile = new Button();
            label1 = new Label();
            label2 = new Label();
            listBox_Results = new ListBox();
            listBox_Files = new ListBox();
            label3 = new Label();
            SuspendLayout();
            // 
            // txBx_Search
            // 
            txBx_Search.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txBx_Search.Location = new Point(10, 42);
            txBx_Search.Name = "txBx_Search";
            txBx_Search.Size = new Size(351, 29);
            txBx_Search.TabIndex = 2;
            txBx_Search.TextChanged += txBx_Search_TextChanged;
            // 
            // btn_CopySelected
            // 
            btn_CopySelected.Location = new Point(7, 542);
            btn_CopySelected.Name = "btn_CopySelected";
            btn_CopySelected.Size = new Size(104, 23);
            btn_CopySelected.TabIndex = 3;
            btn_CopySelected.Text = "Copy Selected";
            btn_CopySelected.UseVisualStyleBackColor = true;
            btn_CopySelected.Click += btn_CopySelected_Click;
            // 
            // btn_OpenFolder
            // 
            btn_OpenFolder.Location = new Point(167, 560);
            btn_OpenFolder.Name = "btn_OpenFolder";
            btn_OpenFolder.Size = new Size(102, 23);
            btn_OpenFolder.TabIndex = 4;
            btn_OpenFolder.Text = "Open Folder";
            btn_OpenFolder.UseVisualStyleBackColor = true;
            btn_OpenFolder.Click += btn_OpenFolder_Click;
            // 
            // btn_OpenFile
            // 
            btn_OpenFile.Location = new Point(275, 560);
            btn_OpenFile.Name = "btn_OpenFile";
            btn_OpenFile.Size = new Size(86, 23);
            btn_OpenFile.TabIndex = 5;
            btn_OpenFile.Text = "Open File";
            btn_OpenFile.UseVisualStyleBackColor = true;
            btn_OpenFile.Click += btn_OpenFile_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(9, 22);
            label1.Name = "label1";
            label1.Size = new Size(71, 15);
            label1.TabIndex = 6;
            label1.Text = "Search Term";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(8, 81);
            label2.Name = "label2";
            label2.Size = new Size(44, 15);
            label2.TabIndex = 7;
            label2.Text = "Results";
            // 
            // listBox_Results
            // 
            listBox_Results.FormattingEnabled = true;
            listBox_Results.ItemHeight = 15;
            listBox_Results.Location = new Point(8, 99);
            listBox_Results.Name = "listBox_Results";
            listBox_Results.Size = new Size(353, 439);
            listBox_Results.TabIndex = 8;
            listBox_Results.SelectedIndexChanged += listBox_Results_SelectedIndexChanged;
            // 
            // listBox_Files
            // 
            listBox_Files.FormattingEnabled = true;
            listBox_Files.ItemHeight = 15;
            listBox_Files.Location = new Point(367, 32);
            listBox_Files.Name = "listBox_Files";
            listBox_Files.Size = new Size(508, 544);
            listBox_Files.TabIndex = 9;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(367, 14);
            label3.Name = "label3";
            label3.Size = new Size(62, 15);
            label3.TabIndex = 10;
            label3.Text = "File results";
            // 
            // FormSearch01
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(887, 593);
            Controls.Add(label3);
            Controls.Add(listBox_Files);
            Controls.Add(listBox_Results);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btn_OpenFile);
            Controls.Add(btn_OpenFolder);
            Controls.Add(btn_CopySelected);
            Controls.Add(txBx_Search);
            Name = "FormSearch01";
            Text = "File System Index";
            Shown += FormSearch01_Shown;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private TextBox txBx_Search;
        private Button btn_CopySelected;
        private Button btn_OpenFolder;
        private Button btn_OpenFile;
        private Label label1;
        private Label label2;
        private ListBox listBox_Results;
        private ListBox listBox_Files;
        private Label label3;
    }
}