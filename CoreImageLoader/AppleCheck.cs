﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
static class AppleCheck
{

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    internal static bool onMacStack = false;
    [STAThread]
    public static bool Check()
    {

        if (OperatingSystem.IsMacOS() || OperatingSystem.IsIOS() || OperatingSystem.IsMacCatalyst() || Environment.OSVersion.Platform == PlatformID.Unix)
        {
            onMacStack = true;
            AppContext.SetSwitch("System.Drawing.EnableUnixSupport", true);
            if (!IsDependencyInstalled("brew", "list mono-libgdiplus"))
            {
                InstallDependency("sudo", "apt-get update && sudo apt-get install -y libgdiplus");
            }
            return true;
        }
        return false;
    }

    static void InstallDependency(string command, string arguments)
    {
        //This should Flag the Apple Stack.
        if (onMacStack) 
        {
            RunCommand(command, arguments);
        }
    }
    static bool IsDependencyInstalled(string command, string arguments)
    {
       
        if (onMacStack)
        {
            // Check using Homebrew if mono-libgdiplus is installed on macOS

            return RunCommand(command, arguments) == 0;
        }
        return false;
    }

    static int RunCommand(string command, string arguments)
    {
        if (onMacStack)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = command;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;

                string error = process.StandardError.ReadToEnd();
                string results = process.StandardOutput.ReadToEnd();

                if (error != null)
                {
                    return -1;
                }


                process.Start();
                process.WaitForExit();

                return process.ExitCode;
            }
        }
        else
        {
            return -1;
        }
    }
}