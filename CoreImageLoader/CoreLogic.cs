﻿
using System.Buffers;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;
using ImageMagick;
using static System.Net.Mime.MediaTypeNames;
using System.Collections.Concurrent;
using SimpleSimd;
using System.Runtime.InteropServices.Marshalling;

namespace CoreImageLoader
{
    public static unsafe class CoreLogic
    {
        public enum ColorCalculationType : byte
        {
            Amplitude = 0,
            Average = 1,
            Add = 2,
            SubtractLeft = 4,
            SubtractRight = 8,
            Difference = 16,
            Multiply = 32,
            Min = 64,
            Max = 128,
        }
        //Some of our Images are 2040x2040 and some are 2048x2048, Recovering if the Image is smaller than we thought is trivial
        //Recovery if the image is larger than we thought requires restarting the entire pipeline.


        static internal readonly int MaxThreads = Environment.ProcessorCount;        
        static internal readonly int SizeUshort = 2;
        internal static readonly object lockobject = new();
        internal static readonly object lockobj = new object();
        internal static readonly object _lockobj = new object();
        internal static readonly object lockobjs = new object();
        internal static readonly object lockObject = new object();

        public static bool AreImagesDifferent(MagickImage img1, MagickImage img2)
        {
            IUnsafePixelCollection<ushort> pixels1 = img1.GetPixelsUnsafe();
            IUnsafePixelCollection<ushort> pixels2 = img2.GetPixelsUnsafe();

            ushort* ptr1 = (ushort*)pixels1.GetAreaPointer(0, 0, img1.Width, img1.Height);
            ushort* ptr2 = (ushort*)pixels2.GetAreaPointer(0, 0, img2.Width, img2.Height);

            for (int i = 0; i < img1.Width * img1.Height; i++)
            {
                if (ptr1[i] != ptr2[i]) return true; // Images differ
            }
            return false; // Images are identical
        }

        //This is designed for debugging, not for speed. That being said it's a useful way to compare two Images.
        public static bool ValidateImage(MagickImage images, int expectedWidth, int expectedHeight, int expectedDepth, out List<string> validationErrors, MagickFormat format = MagickFormat.Tiff, ColorSpace space = ColorSpace.LinearGray)
        {
            validationErrors = new List<string>();
            bool isValid = true;
      
            MagickImage image = images;

            if (image == null)
            {
                validationErrors.Add($"Image is null.");
                isValid = false;
                return false;
            }

            // Check image dimensions
            if (image.Width != expectedWidth || image.Height != expectedHeight)
            {
                validationErrors.Add($"Image has incorrect dimensions: {image.Width}x{image.Height} (expected {expectedWidth}x{expectedHeight}).");
                isValid = false;
            }

            // Check image depth
            if (image.Depth != expectedDepth)
            {
                validationErrors.Add($"Image has incorrect depth: {image.Depth} (expected {expectedDepth}).");
                isValid = false;
            }

            int channelCount = image.ChannelCount;
            if (channelCount > 2)
            {
                validationErrors.Add($"Image has incorrect number of channels: {channelCount} (expected either 1 or 2 Channels).");
                isValid = false;
            }

            if (image.Width %2 != 0 || image.Height %2 != 0)
            {
                validationErrors.Add($"Image Height or Width is not a Power of 2 this is probably an instrument error");
                isValid = false;
            }

            if (format.HasFlag(MagickFormat.Tif) ||format.HasFlag(MagickFormat.Tiff) && (!(image.Format.HasFlag(MagickFormat.Tiff) || image.Format.HasFlag(MagickFormat.Tif))))
            {
                validationErrors.Add($"Image is not in the correct format");
                isValid = false;
            }
            else if (format != image.Format)
            {
                validationErrors.Add($"Image is not in the correct format should be {format} but got {image.Format}");
            }
            else
            {
                //Format is expected.
            }

            if (!image.ColorSpace.ToString().ToUpper().Contains("Gray"))
            {
                validationErrors.Add($"Image is not in a Grayscaled Colorspace but is in {image.ColorSpace}");
            }
            // Validate pixel data and check mean
            try
            {
                IUnsafePixelCollection<ushort> pixels = images.GetPixelsUnsafe();
                long totalPixelValue = 0;
                long pixelCount = 0;
                for (ushort x = 0; x < expectedWidth; x++)
                {
                    for (ushort y = 0; y < expectedHeight; y++)
                    {
                        IPixel<ushort>? pixel = pixels[x, y];
                        if (pixel != null && pixel.Channels > 0)
                        {
                            // Accumulate pixel values
                            totalPixelValue += pixel.GetChannel(0); // Assuming grayscale or first channel
                            pixelCount++;
                        }
                        else
                        {
                            validationErrors.Add($"Pixel at ({x},{y}) is invalid or missing.");
                            isValid = false;
                        }
                    }
                }
                // Calculate mean
                if (pixelCount > 0)
                {
                    double meanPixelValue = (double)totalPixelValue / pixelCount;
                    if (meanPixelValue <= 1 && meanPixelValue >= -1)
                    {
                        validationErrors.Add($"Mean pixel value is {meanPixelValue}, which does not meet the criteria (>1 or < -1).");
                        isValid = false;
                    }
                }
                else
                {
                    validationErrors.Add($"No valid pixels found in the image.");
                    isValid = false;
                }
            }
            catch (Exception ex)
            {
                validationErrors.Add($"Error validating pixel data: {ex.Message}");
                isValid = false;
            }

            return isValid;
        }
        //This Function attempts to split work equally between the thread's a user has. It is not a perfect science.
        //Generally you cannot count on all of your Cores being available I am subtracting two Threads, one is for the ImageQue. The other for Windows bg tasks

        static internal int swidth = 2048;
        static internal int sheight = 2048;
        static internal int shift = 0;
        static string _currentFolder = "";
        static internal MagickImage _MagickImage = new MagickImage(MagickColors.Black, 2048, 2048) { Depth = 16, HasAlpha = false, Format = MagickFormat.Tiff, ColorType = ColorType.Grayscale, ColorSpace=ColorSpace.Gray };
        static internal int CacheSize = 20;
        static internal Dictionary<string, MagickImage> MiniatureCache = new Dictionary<string, MagickImage>(CacheSize);
        static internal object miniCacheLock = new object();
        static internal Random random = new Random();
        //This Function Pings the very first Image in a Folder to Extract it's Metadata and Updates the Template, Geometry, Shift and other globals.
        //Usually users are switching between a small number of folders (usually no more than a few dozen often less) 
        //so there is definitely some benefit to Cacheing as long as the number of templates is limited. 20 is a good number (~200 megabytes) 
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static MagickImage? PingForTemplate(string filename, FileInfo fi)
        {
            lock (miniCacheLock)
            {
                // Resolve the folder path
                FileInfo fil = fi;
                string folderPath = fi.DirectoryName;

                // Check if the template is cached
                if (MiniatureCache.TryGetValue(folderPath, out MagickImage cachedTemplate))
                {
                    // Update current context and return a clone of the cached template
                    _MagickImage = cachedTemplate.Clone() as MagickImage;
                    _currentFolder = folderPath;
                    return _MagickImage;
                }

                // Ping the file to extract metadata
                using MagickImage metadata = new MagickImage(filename);
            

                // Create a new template
                MagickImage image = metadata.Clone() as MagickImage;
          
                // Calculate shift for power-of-2 optimization
                int width = metadata.Width;
                double shiftAmount = Math.Log2(width);
                shift = (shiftAmount == (int)shiftAmount) ? (int)shiftAmount : 0;

                // Cache the new template
                if (MiniatureCache.Count >= CacheSize)
                {
                    // Remove a random cache entry if the limit is reached
                    int indexToRemove = random.Next(0, MiniatureCache.Count - 1);
                    MiniatureCache.Remove(_currentFolder);
                }

                // Update context and add the new template to the cache
                _MagickImage = image.Clone() as MagickImage;
                _currentFolder = folderPath;
                MiniatureCache[folderPath] = image;

                return image;
            }
        }
        //This function proccess the Image based on a Template. Currently a few dozen templates are Cached
        //the reason for this is that under normal circumstances a user is generally working with a limited number of folders.
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static MagickImage ProcessImageUnsafe(string filename)
        {
            lock (lockobject)
            {
                Stopwatch sw = Stopwatch.StartNew();
                int MThreads = MaxThreads;  // number of parallel threads
                MagickImage image;
                FileInfo Fi = new FileInfo(filename);
                // Decide how to get your base image
                if (_currentFolder != Fi.DirectoryName)
                {
                    image = PingForTemplate(filename, Fi);
                }
                else
                {
                    // Already have a template in _MagickImage
                    image = (MagickImage)_MagickImage.Clone();
                }
             
                int fileLength = (int)Fi.Length;
         

                byte[] buffer = ArrayPool<byte>.Shared.Rent(fileLength);

                try
                {
                    // Calculate the chunk for each thread
                    int chunkSize = fileLength / MThreads;
                    int remainder = fileLength % MThreads;

                    Parallel.For(0, MThreads, threadIndex =>
                    {
                        // Each thread reads "chunkSize" except possibly the last one
                        int offset = threadIndex * chunkSize;
                        int bytesToRead = (threadIndex == MThreads - 1)
                            ? (chunkSize + remainder)
                            : chunkSize;

                        // (Yes, this overhead can be worth it if your SSD benefits from concurrency.)
                        using FileStream threadFs = new FileStream(filename,FileMode.Open,FileAccess.Read,FileShare.Read, 16384,
                            FileOptions.SequentialScan
                        );

                        // Seek and read exactly the chunk
                        threadFs.Position = offset;
                        threadFs.ReadExactly(buffer, offset, bytesToRead);
                    });

                    // 2) PARALLEL COPY INTO THE IMAGE
                    ushort* pixelPointer = (ushort*)image.GetPixelsUnsafe().GetAreaPointer(0,0, _MagickImage.Width, _MagickImage.Height);

                    int height = _MagickImage.Width;
                    int width = _MagickImage.Height;

                    // Each row is width "ushort" pixels => width * 2 bytes
                    int rowByteCount = width << 1;
                    // Use MemoryMarshal to get a pointer to the pinned array memory
                    fixed (byte* sourceBasePtr = buffer)
                    {
                        byte* sourceBaseptr = sourceBasePtr;
                        Parallel.For(0, height, y =>
                        {
                            int rowOffset = y * width;

                            // Destination pointer for this row
                            ushort* destRow = pixelPointer + rowOffset;

                            // Source pointer: each pixel is 2 bytes
                            byte* sourcePtr = sourceBaseptr + (rowOffset << 1);

                            // Copy the entire row
                            Buffer.MemoryCopy(sourcePtr, destRow, rowByteCount, rowByteCount);
                        });
                    }

                    Debug.WriteLine($"Reading Image: {sw.ElapsedMilliseconds} ms");
                    return image;
                }
                catch (Exception ex)
                {
                    // If something fails, fallback to a direct load
                    Debug.WriteLine($"Fallback triggered: {ex.Message}");
                    return new MagickImage(filename);
                }
                finally
                {
                    ArrayPool<byte>.Shared.Return(buffer, false);
                }
            }
        }

        //This Gathers the Byte Array in Parallel over the network but does not use a Template.
        //This is not just Safer in the sense that it doesn't use pointer Arithmetic it's safer because
        //We have multiple fallbacks (Calling ImageMagick with the FileName, 
        public static MagickImage ProcessImage(string filename)
        {
            lock (lockobject) // Ensure thread safety for shared _MagickImage
            {
                Stopwatch sw = Stopwatch.StartNew();
                using FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                int fileLength = (int)fs.Length;
                int MThreads = MaxThreads;
                // Rent a byte array from the pool
                byte[] buffer = ArrayPool<byte>.Shared.Rent(fileLength);
                // Pin the buffer using GCHandle
                GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                try
                {
                    int chunkSize = fileLength / MThreads;
                    int remainder = fileLength % MThreads;
                    // Parallel read into the buffer (excluding the remainder)
                    Parallel.For(0, MThreads, threadIndex =>
                    {
                        int offset = threadIndex * chunkSize;

                        using FileStream threadFs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 16384, FileOptions.SequentialScan);
                        threadFs.Seek(offset, SeekOrigin.Begin);
                        threadFs.ReadExactly(buffer, offset, chunkSize);
                    });

                    // Handle the remainder separately
                    if (remainder > 0)
                    {
                        int remainderOffset = MThreads * chunkSize;

                        using FileStream remainderFs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 16384, FileOptions.SequentialScan);
                        remainderFs.Seek(remainderOffset, SeekOrigin.Begin);
                        remainderFs.ReadExactly(buffer, remainderOffset, remainder);
                    }

                    MagickImage image = new MagickImage(buffer);
                    Debug.WriteLine("Loading: " + sw.ElapsedMilliseconds);
                    swidth = image.Width;
                    sheight = image.Height;
                    return image;
                }
                catch (Exception ex)
                {
                    // Fallback: Load the image directly with MagickImage
                    Debug.WriteLine($"Fallback triggered: {ex.Message}");
                    try
                    {
                        FileInfo fi = new FileInfo(filename);
                        Debug.WriteLine($"File Exists?: {fi.Exists}");
                        if (fi.Exists)
                        {
                            return new MagickImage(fi);
                        }
                        else
                        {
                            Debug.WriteLine("File Does not exist at that location");
                            return new MagickImage();
                        }
                    }
                    catch (Exception ex2)
                    {
                        Debug.WriteLine($"Secondary Exception {ex2.Message}");
                        Debug.WriteLine("Secondary Fallback Triggered");
                        try
                        {
                            return new MagickImage(filename);
                        }
                        catch(Exception existing)
                        {
                            Debug.WriteLine("Tertiary Catch Block hit Returning empty MagickImage");
                            return new MagickImage();
                        }
                    }
                }
                finally
                {
                    handle.Free();
                    ArrayPool<byte>.Shared.Return(buffer);
                }
            }
        }

        //This is to store the TRUE Width and Height in the Same Variable.
 
        static internal bool isMacStack = false;
        //This will run Regardless of whether or not we access it.
        //It checks if you have the necessary dependencies Installed and if you do not it attempts to Install them.
        static internal bool isMacOs = AppleCheck.Check();
        //Recommend
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        
        public static ValueTuple<MagickImage, Bitmap, Rectangle> LoadImage(string filename, ushort blackPoint, ushort whitePoint, bool Unsafe = true)
        {
            
            lock (lockobj) // Lock the entire function
            {
                //Attempt to Load a 16 bit GrayScale Tiff.
                Task<MagickImage> matrixTask; matrixTask = Task.Run(() => ProcessImageUnsafe(filename));

                // Placeholder Bitmap and Rectangle, (We have 2.5 Milliseconds of Time so Create these Objects Async.)
                Bitmap bitmap = new Bitmap(swidth, sheight, PixelFormat.Format24bppRgb);
                Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                ValueTuple<MagickImage, Bitmap, Rectangle> vTup = new ValueTuple<MagickImage, Bitmap, Rectangle>();
                matrixTask.Wait(); // Ensure Matrix is completely loaded
                MagickImage matrixImage = matrixTask.Result;

                //Reconciliation. If the Folder has switched, our assumption about the Bitmap could be wrong
                //we need to check it against the actual proccessed Image to be sure.
                if (matrixImage.Width != bitmap.Width || matrixImage.Height != bitmap.Height)
                {
                    if (matrixImage.Width < bitmap.Width && matrixImage.Height < bitmap.Height)
                    {
                        // Crop the placeholder Bitmap to match Matrix size
                        Rectangle cropRect = new Rectangle(0, 0, matrixImage.Width, matrixImage.Height);
                        Bitmap croppedBitmap = bitmap.Clone(cropRect, bitmap.PixelFormat);

                        // Replace the bitmap and update the rectangle
                        bitmap.Dispose();
                        bitmap = croppedBitmap;
                        rect = cropRect;
                    }
                    else
                    {
                        // Resize Bitmap to match MagickImage size
                        bitmap.Dispose();
                        bitmap = new Bitmap(matrixImage.Width, matrixImage.Height, PixelFormat.Format24bppRgb);
                        rect = new Rectangle(0, 0, matrixImage.Width, matrixImage.Height);
                    }
                }
                //Here we know the Height and Width for certain
                swidth = rect.Width;
                sheight = rect.Height;
                LevelingFast(matrixImage, blackPoint, whitePoint);
                vTup = (matrixImage, bitmap, rect);
                return vTup;
            }
        }

        //This is the most common function called from the outside. I am returning the Bitmap and the MagickImage. Loading in Parallel should only be done on M.2 Drives and High Performance SSD's.
        //Loading in Parallel will probably be slower on a Tape drive than doing so sequentially.
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static (MagickImage, Bitmap) PreProcessImage(string filename, ushort blackPoint, ushort whitePoint, ValueTuple<float, float, float>? BGR, bool useParallelLoad = true)
        {
            try
            {
                ValueTuple<MagickImage, Bitmap, Rectangle> results = CoreLogic.LoadImage(filename, blackPoint, whitePoint);
                Bitmap bitmap = results.Item2;
                Rectangle lockBitsRect = results.Item3;
                Bitmap bmap = results.Item1.ToBitmapWithColors(bitmap, lockBitsRect, BGR);
                return (results.Item1, bmap);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error in DLL {ex.Message}");
                return (new MagickImage(), new Bitmap(10, 10));
            }
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ApplyFunctionToChannels(
        IMagickImage<ushort> image, Func<ushort, ushort>[] channelFunctionsBGRA)
        {
            int width = image.Width;
            int height = image.Height;
            int channels = image.ChannelCount; // Assume multi-channel image

            if (channelFunctionsBGRA.Length != channels)
            {
                throw new ArgumentException("The number of channel functions must match the number of channels in the image.");
            }

            using IUnsafePixelCollection<ushort> pixels = image.GetPixelsUnsafe();
            ushort* ptrFirstPixel = (ushort*)pixels.GetAreaPointer(0, 0, width, height);

            Parallel.For(0, height, y =>
            {
                ushort* srcRowPtr = ptrFirstPixel + (y * width * channels); // Account for multiple channels

                for (int x = 0; x < width; x++)
                {
                    ushort* pixelPtr = srcRowPtr + (x * channels);

                    switch (channels)
                    {
                        case 1: // Grayscale
                            pixelPtr[0] = channelFunctionsBGRA[0](pixelPtr[0]);
                            break;
                        case 2: // Grayscale + Alpha
                            pixelPtr[0] = channelFunctionsBGRA[0](pixelPtr[0]); // Grayscale
                            pixelPtr[1] = channelFunctionsBGRA[1](pixelPtr[1]); // Alpha
                            break;
                        case 3: // RGB
                            pixelPtr[0] = channelFunctionsBGRA[0](pixelPtr[0]); // Blue
                            pixelPtr[1] = channelFunctionsBGRA[1](pixelPtr[1]); // Green
                            pixelPtr[2] = channelFunctionsBGRA[2](pixelPtr[2]); // Red
                            break;

                        case 4: // RGBA
                            pixelPtr[0] = channelFunctionsBGRA[0](pixelPtr[0]); // Blue
                            pixelPtr[1] = channelFunctionsBGRA[1](pixelPtr[1]); // Green
                            pixelPtr[2] = channelFunctionsBGRA[2](pixelPtr[2]); // Red
                            pixelPtr[3] = channelFunctionsBGRA[3](pixelPtr[3]); // Alpha
                            break;
                        default:
                            throw new InvalidOperationException("Unsupported channel count.");
                    }
                }
            });

        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ApplyFunctionToPixels(IMagickImage<ushort> image, Func<ushort, ushort> applyToPixel)
        {

            int width = image.Width;
            int height = image.Height;
            int channels = image.ChannelCount; // Assume multi-channel image

            using IUnsafePixelCollection<ushort> pixels = image.GetPixelsUnsafe();
            ushort* ptrFirstPixel = (ushort*)pixels.GetAreaPointer(0, 0, width, height);

            Parallel.For(0, height, y =>
            {
                ushort* srcRowPtr = ptrFirstPixel + (y * width * channels); // Account for multiple channels

                for (int x = 0; x < width; x++)
                {
                    ushort* pixelPtr = srcRowPtr + (x * channels);

                    pixelPtr[0] = applyToPixel(pixelPtr[0]); // Blue
                    pixelPtr[1] = applyToPixel(pixelPtr[1]); // Green
                    pixelPtr[2] = applyToPixel(pixelPtr[2]); // Red
                }
            });

        }
        internal static object lockss = new object();
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ApplyFunctionToPixelsGrayscale(IMagickImage<ushort> image, Func<ushort, ushort> applyToPixel)
        {
            int width = image.Width;
            int height = image.Height;

            using IUnsafePixelCollection<ushort> pixels = image.GetPixelsUnsafe();
            ushort* ptrFirstPixel = (ushort*)pixels.GetAreaPointer(0, 0, width, height);

            Parallel.For(0, height, y =>
            {
                ushort* srcRowPtr = ptrFirstPixel + (y * width); // No channel multiplication for grayscale

                for (int x = 0; x < width; x++)
                {
                    ushort* pixelValuePtr = srcRowPtr + x;

                    // Apply the function to the pixel value
                    *pixelValuePtr = applyToPixel(*pixelValuePtr);
                }
            });
        }

        //Second Step in the Pipeline. GetAreaPointer is basically ImageMagick's version of LockBits.
        //Calculating the Reciprocal of the Range Speeds this up quite a bit. I could add some safety checks
        //but as of right now, BPoint and WPoint are either hardcoded or calculated such that the range is checked;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void LevelingFast(this MagickImage _matrix, ushort blackpoint, ushort whitepoint)
        {
            lock (lockss)
            {
                MagickImage image = _matrix;

                int width = image.Width; 
                int height = image.Height;


                ushort* ptrfirstpixel = (ushort*)image.GetPixelsUnsafe().GetAreaPointer(0, 0, width, height);

                float ScaleFactor = 1.0f / (whitepoint - blackpoint) * ushort.MaxValue;
                
                Parallel.For(0, height, y =>
                {
                    ushort* srcRowPtr = ptrfirstpixel + (y * width);

                    for (int x = 0; x < width; x++)
                    {
                        ushort* pixelValuePtr = srcRowPtr + x;  // Get the pointer to the current pixel value
                        ushort pixelValue = *pixelValuePtr;  // Dereference the pointer to get the pixel value

                        if (pixelValue >= whitepoint)
                        {
                            *pixelValuePtr =  ushort.MaxValue;  // Directly update the pixel value using the pointer
                        }
                        else if (pixelValue <= blackpoint)
                        {
                            *pixelValuePtr = 0;  // Directly update the pixel value using the pointer
                        }
                        else
                        {
                            //Fused Multiply Add does two floating point calculations in one instruction to minimize intermediate rounding.
                            float ScaledValue = MathF.FusedMultiplyAdd(pixelValue - blackpoint, ScaleFactor, 0.5f);
                            if (ScaledValue > ushort.MaxValue)
                            {
                                ScaledValue = ushort.MaxValue;
                            }
                            else
                            {
                                *pixelValuePtr = (ushort)ScaledValue;
                            }
                        }
                    }
                });
            }
        }

        //Logically step 3 should be two seperate concerns but for performance reasons it makes a lot more sense to do this once.
        ///1. Setting up these iterations involves considerable overhead, Parallel, LockBits, Height +Width <summary>
        ///2. At this moment in time I can do all of those things Asynchronously though while the Image Loads.
        ///3. The Bitmap, Bounding Rectangle, and Dimensions are all Free. Since this is the second function in our Pipeline.
        ///4. Finally, the complexity is minimal, there are Five Channels and we need to make sure each one has it's own color
        ///

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Bitmap ToBitmapWithColors(this MagickImage self, Bitmap bitmap, Rectangle rect, ValueTuple<float, float, float>? vT)
        {
            lock (lockobj)
            {
                MagickImage image = self;
                //getting rid of the TQuantum Type struct did eliminate some overhead.
                BitmapData data = bitmap.LockBits(rect, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int height = data.Height;
                int width = data.Width;

                // Get the source pointer for the entire image (ushort image data)
                ushort* srcPtr  = (ushort*)image.GetPixelsUnsafe().GetAreaPointer(0, 0, height, width);

           
                byte* destPtr = (byte*)data.Scan0; // Destination is byte

                int destinationStride = data.Stride;
                int channels = image.ChannelCount;

                try
                {
                    if (vT != null)
                    {
                        float blue = vT.Value.Item1;
                        float green = vT.Value.Item2;
                        float red = vT.Value.Item3;
                        //I could Unroll this loop but since three doesn't divide into 2048 we would have to check
                        //bounds which would negate any benefits. 
                        if (green == 0)
                        {
                            Parallel.For(0, height, row =>
                            {
                                ushort* srcRowPtr = srcPtr + row * width;
                                byte* destRowPtr = destPtr + row * destinationStride;

                                for (int col = 0; col < width - 1; col++)
                                {
                                    byte pixelValueBlue = (byte)((srcRowPtr[col] >> 8) * blue);    // Blue channel
                                    byte pixelValueGreen = 0;    // Green channel
                                    byte pixelValueRed = (byte)((srcRowPtr[col + 2] >> 8) * red); // Red channel

                                    int packedPixels = pixelValueBlue | (pixelValueGreen << 8) | (pixelValueRed << 16);
                                    *(int*)(destRowPtr + col * 3) = packedPixels;
                                }
                            });
                        }
                        else if (blue == 0)
                        {
                            Parallel.For(0, height, row =>
                            {
                                ushort* srcRowPtr = srcPtr + row * width;
                                byte* destRowPtr = destPtr + row * destinationStride;

                                for (int col = 0; col < width - 1; col++)
                                {
                                    byte pixelValueBlue = 0;       // Blue channel
                                    byte pixelValueGreen = (byte)((srcRowPtr[col + 1] >> 8) * green); // Green channel
                                    byte pixelValueRed = (byte)((srcRowPtr[col + 2] >> 8) * red); // Red channel

                                    int packedPixels = pixelValueBlue | (pixelValueGreen << 8) | (pixelValueRed << 16);
                                    *(int*)(destRowPtr + col * 3) = packedPixels;
                                }
                            });
                        }
                        else if(red == 0)
                        {
                            Parallel.For(0, height, row =>
                            {
                                ushort* srcRowPtr = srcPtr + row * width;
                                byte* destRowPtr = destPtr + row * destinationStride;

                                for (int col = 0; col < width - 1; col++)
                                {
                                    byte pixelValueBlue = (byte)((srcRowPtr[col] >> 8) * blue); // Blue Channel
                                    byte pixelValueGreen = (byte)((srcRowPtr[col + 1] >> 8) * green); // Green channel
                                    byte pixelValueRed = 0;

                                    int packedPixels = pixelValueBlue | (pixelValueGreen << 8) | (pixelValueRed << 16);
                                    *(int*)(destRowPtr + col * 3) = packedPixels;
                                }
                            });
                        }
                        else
                        {
                            Parallel.For(0, height, row =>
                            {
                                ushort* srcRowPtr = srcPtr + row * width;
                                byte* destRowPtr = destPtr + row * destinationStride;

                                for (int col = 0; col < width - 1; col++)
                                {
                                    byte pixelValueBlue = (byte)((srcRowPtr[col] >> 8) * blue);    // Blue channel
                                    byte pixelValueGreen = (byte)((srcRowPtr[col + 1] >> 8) * green); // Green channel
                                    byte pixelValueRed = (byte)((srcRowPtr[col + 2] >> 8) * red); // Red channel

                                    int packedPixels = pixelValueBlue | (pixelValueGreen << 8) | (pixelValueRed << 16);
                                    *(int*)(destRowPtr + col * 3) = packedPixels;
                                }
                            });
                        }
                    }
                    else
                    {
                        //If the Parameters are null just return a GrayScaled Bitmap with Three Channels
                        Parallel.For(0, height, row =>
                        {
                            ushort* srcRowPtr = srcPtr + row * width;           // Source data for the row
                            byte* destRowPtr = destPtr + row * destinationStride;
                            // Destination data for the row
                            for (int col = 0; col < width - 1; col++)
                            {
                                byte pixelValueBlue = (byte)((srcRowPtr[col] >> 8));        // Blue channel
                                byte pixelValueGreen = (byte)((srcRowPtr[col + 1] >> 8));   // Green channel
                                byte pixelValueRed = (byte)((srcRowPtr[col + 2] >> 8));     // Red channel

                                int packedPixels = pixelValueBlue |(pixelValueGreen << 8) |(pixelValueRed << 16);

                                *(int*)(destRowPtr + col * 3) = packedPixels;
                            }
                        });
                    }
                }
    
                finally
                {
                    bitmap.UnlockBits(data);
                }
                return bitmap;
            }
        }

        //OpenCV Converter Method:
        /*
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Mat ToOpenCV(this IMagickImage<ushort> self)
        {   
            lock (lockObject)
            {
                int height = self.Height;
                int width = self.Width;

                // Create a new OpenCV Mat matching the Magick image dimensions
                Mat newMat = new Mat(width, height, MatType.CV_16UC1);

                // Access the pixel data using IUnsafePixelCollection
                using IUnsafePixelCollection<ushort> pixels = self.GetPixelsUnsafe();

                // Get pointers to the source and destination image areas
                ushort* pointer = (ushort*)pixels.GetAreaPointer(0, 0, width, height);
                ushort* matrixPointer = (ushort*)newMat.Ptr(0); //For a Grayscaled Image a Pointer to the first Row is all we need.
                int rowSizeInBytes = width * sizeof(ushort);

                // Perform parallel row copying
                Parallel.For(0, height, y =>
                {
                    int yWidth = y * width;
                    ushort* row = matrixPointer + yWidth;
                    ushort* sourceRow = pointer + yWidth;
                    Buffer.MemoryCopy(sourceRow, row, rowSizeInBytes, rowSizeInBytes);
                });

                return newMat;
            }
        }
        */
        internal static bool VerifyConversion(IMagickImage<ushort> magickImage, dynamic openCvMat)
        {
            // Check dimensions
            if (magickImage.Width != openCvMat.Cols || magickImage.Height != openCvMat.Rows)
            {
                Debug.WriteLine("Dimension mismatch.");
                return false;
            }

            // Access MagickImage pixel data
            using IUnsafePixelCollection<ushort> pixels = magickImage.GetPixelsUnsafe();
            nint pointer = pixels.GetAreaPointer(0, 0, magickImage.Width, magickImage.Height);

            // Verify pixel values with limited logging
            int mismatchCount = 0;
            const int maxLogCount = 10;

            for (int y = 0; y < magickImage.Height; y++)
            {
                dynamic* row = null;//(ushort*)openCvMat.Ptr(y);
                for (int x = 0; x < magickImage.Width; x++)
                {
                    int offset = y * magickImage.Width + x;
                    ushort magickPixel = ((ushort*)pointer)[offset];
                    ushort matPixel = row[x];

                    if (magickPixel != matPixel)
                    {
                        if (mismatchCount < maxLogCount)
                        {
                            Debug.WriteLine($"Mismatch at ({y}, {x}): MagickImage={magickPixel}, Mat={matPixel}");
                        }
                        mismatchCount++;
                        if (mismatchCount >= maxLogCount)
                        {
                            Debug.WriteLine("Further mismatches suppressed.");
                        }
                        return false;
                    }
                }
            }
            return true;
        }
       
        //This is Step 4 and the Final Step for most Bitmaps before we Que them. All of the Math is Inlined, most of the Math is simple.
        //The default case is "Add" which is the Color Calculation that is part of our Core Image Pipeline. This is the final proccessing
        //step in the vast majority of cases (Unless Channels are being Clipped.After this we add the Image to our ImageQue.

        internal const float inv255 = 1.0f / 255.0f;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Bitmap ArithmeticBlend(Bitmap sourceBitmap, Bitmap blendBitmap, ColorCalculationType calculationType)
        {
            lock (lockobj)
            {
                Rectangle rect = new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height);
                Rectangle rect2 = new Rectangle(0, 0, blendBitmap.Width, blendBitmap.Height);
                BitmapData sourceData = sourceBitmap.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                BitmapData blendData = blendBitmap.LockBits(rect2, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

                int bytesPerPixel = 3;
                int heightInPixels = sourceData.Height;
                int widthInBytes = rect.Height * bytesPerPixel;
                int blendStride = blendData.Stride;
                int sourceStride = sourceData.Stride;

                byte* ptrFirstPixelSource = (byte*)sourceData.Scan0;
                byte* ptrFirstPixelBlend = (byte*)blendData.Scan0;
                int addUnrollFactor = bytesPerPixel * 8;
                try
                {
                    if (calculationType == ColorCalculationType.Add)
                    {
                        Parallel.For(0, heightInPixels, y =>
                        {
                            byte* sourceLine = ptrFirstPixelSource + (y * sourceStride);
                            byte* blendLine = ptrFirstPixelBlend + (y * blendStride);
             
                          
                            for (int x = 0; x < widthInBytes; x += addUnrollFactor)
                            {

                                // Base pointers for the current 8-pixel block
                                byte* sBase = sourceLine + x;
                                byte* bBase = blendLine + x;

                                // Pixel 1
                                int sumB1 = sBase[0] + bBase[0];
                                int sumG1 = sBase[1] + bBase[1];
                                int sumR1 = sBase[2] + bBase[2];
                                sBase[0] = (byte)((sumB1 > 255) ? 255 : sumB1);
                                sBase[1] = (byte)((sumG1 > 255) ? 255 : sumG1);
                                sBase[2] = (byte)((sumR1 > 255) ? 255 : sumR1);

                                // Pixel 2
                                int sumB2 = sBase[3] + bBase[3];
                                int sumG2 = sBase[4] + bBase[4];
                                int sumR2 = sBase[5] + bBase[5];
                                sBase[3] = (byte)((sumB2 > 255) ? 255 : sumB2);
                                sBase[4] = (byte)((sumG2 > 255) ? 255 : sumG2);
                                sBase[5] = (byte)((sumR2 > 255) ? 255 : sumR2);

                                // Pixel 3
                                int sumB3 = sBase[6] + bBase[6];
                                int sumG3 = sBase[7] + bBase[7];
                                int sumR3 = sBase[8] + bBase[8];
                                sBase[6] = (byte)((sumB3 > 255) ? 255 : sumB3);
                                sBase[7] = (byte)((sumG3 > 255) ? 255 : sumG3);
                                sBase[8] = (byte)((sumR3 > 255) ? 255 : sumR3);

                                // Pixel 4
                                int sumB4 = sBase[9] + bBase[9];
                                int sumG4 = sBase[10] + bBase[10];
                                int sumR4 = sBase[11] + bBase[11];
                                sBase[9] = (byte)((sumB4 > 255) ? 255 : sumB4);
                                sBase[10] = (byte)((sumG4 > 255) ? 255 : sumG4);
                                sBase[11] = (byte)((sumR4 > 255) ? 255 : sumR4);

                                // Pixel 5
                                int sumB5 = sBase[12] + bBase[12];
                                int sumG5 = sBase[13] + bBase[13];
                                int sumR5 = sBase[14] + bBase[14];
                                sBase[12] = (byte)((sumB5 > 255) ? 255 : sumB5);
                                sBase[13] = (byte)((sumG5 > 255) ? 255 : sumG5);
                                sBase[14] = (byte)((sumR5 > 255) ? 255 : sumR5);

                                // Pixel 6
                                int sumB6 = sBase[15] + bBase[15];
                                int sumG6 = sBase[16] + bBase[16];
                                int sumR6 = sBase[17] + bBase[17];
                                sBase[15] = (byte)((sumB6 > 255) ? 255 : sumB6);
                                sBase[16] = (byte)((sumG6 > 255) ? 255 : sumG6);
                                sBase[17] = (byte)((sumR6 > 255) ? 255 : sumR6);

                                // Pixel 7
                                int sumB7 = sBase[18] + bBase[18];
                                int sumG7 = sBase[19] + bBase[19];
                                int sumR7 = sBase[20] + bBase[20];
                                sBase[18] = (byte)((sumB7 > 255) ? 255 : sumB7);
                                sBase[19] = (byte)((sumG7 > 255) ? 255 : sumG7);
                                sBase[20] = (byte)((sumR7 > 255) ? 255 : sumR7);

                                // Pixel 8
                                int sumB8 = sBase[21] + bBase[21];
                                int sumG8 = sBase[22] + bBase[22];
                                int sumR8 = sBase[23] + bBase[23];
                                sBase[21] = (byte)((sumB8 > 255) ? 255 : sumB8);
                                sBase[22] = (byte)((sumG8 > 255) ? 255 : sumG8);
                                sBase[23] = (byte)((sumR8 > 255) ? 255 : sumR8);
                            }
                            
                        });
                    }
                    else
                    {
                        Parallel.For(0, heightInPixels, y =>
                        {
                            byte* sourceLine = ptrFirstPixelSource + (y * sourceStride);
                            byte* blendLine = ptrFirstPixelBlend + (y * blendStride);
                            for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                            {
                                byte* sourcePtrB = sourceLine + x;
                                byte* blendPtrB = blendLine + x;
                                byte* sourcePtrG = sourceLine + x + 1;
                                byte* blendPtrG = blendLine + x + 1;
                                byte* sourcePtrR = sourceLine + x + 2;
                                byte* blendPtrR = blendLine + x + 2;
                                switch (calculationType)
                                {
                                    case ColorCalculationType.Average:
                                        *sourcePtrB = (byte)((*sourcePtrB + *blendPtrB + 1) >> 1);
                                        *sourcePtrG = (byte)((*sourcePtrG + *blendPtrG + 1) >> 1);
                                        *sourcePtrR = (byte)((*sourcePtrR + *blendPtrR + 1) >> 1);
                                        break;

                                    case ColorCalculationType.SubtractLeft:
                                        int diffR1 = *sourcePtrR - *blendPtrR;
                                        *sourcePtrR = (byte)(diffR1 < 0 ? 0 : diffR1);
                                        break;

                                    case ColorCalculationType.SubtractRight:
                                        int diffR2 = *blendPtrR - *sourcePtrR;
                                        *sourcePtrR = (byte)(diffR2 < 0 ? 0 : diffR2);
                                        break;

                                    case ColorCalculationType.Difference:
                                        byte sourceValueR = *sourcePtrR; // Dereference once and store in a local variable
                                        byte blendValueR = *blendPtrR;   // Dereference once and store in a local variable

                                        *sourcePtrR = (byte)(sourceValueR > blendValueR ? sourceValueR - blendValueR : blendValueR - sourceValueR);
                                        break;

                                    case ColorCalculationType.Multiply:
                                        *sourcePtrB = (byte)(*blendPtrB * *sourcePtrB * inv255);
                                        *sourcePtrG = (byte)(*blendPtrG * *sourcePtrG * inv255);
                                        *sourcePtrR = (byte)(*blendPtrR * *sourcePtrR * inv255);
                                        break;

                                    case ColorCalculationType.Min:
                                        if (*blendPtrB < *sourcePtrB)
                                            *sourcePtrB = *blendPtrB;
                                        if (*blendPtrG < *sourcePtrG)
                                            *sourcePtrG = *blendPtrG;
                                        if (*blendPtrR < *sourcePtrR)
                                            *sourcePtrR = *blendPtrR;
                                        break;

                                    case ColorCalculationType.Max:
                                        if (*blendPtrB > *sourcePtrB)
                                            *sourcePtrB = *blendPtrB;

                                        if (*blendPtrG > *sourcePtrG)
                                            *sourcePtrG = *blendPtrG;

                                        if (*blendPtrR > *sourcePtrR)
                                            *sourcePtrR = *blendPtrR;
                                        break;

                                    case ColorCalculationType.Amplitude:
                                        if (*sourcePtrB + *blendPtrB >= 180)
                                            *sourcePtrB = byte.MaxValue;
                                        else
                                            *sourcePtrB = (byte)(MathF.Sqrt((*sourcePtrB * *sourcePtrB) + (*blendPtrB * *blendPtrB)) * 1.414213562f);
                                        if (*sourcePtrG + *blendPtrG >= 180)
                                            *sourcePtrG = byte.MaxValue;
                                        else
                                            *sourcePtrG = (byte)(MathF.Sqrt((*sourcePtrG * *sourcePtrG) + (*blendPtrG * *blendPtrG)) * 1.414213562f);
                                        if (*sourcePtrR + *blendPtrR >= 180)
                                            *sourcePtrR = byte.MaxValue;
                                        else
                                            *sourcePtrR = (byte)(MathF.Sqrt((*sourcePtrR * *sourcePtrR) + (*blendPtrR * *blendPtrR)) * 1.414213562f);
                                        break;
                                }

                            }
                        });
                    }
                }
                finally
                {
                    sourceBitmap.UnlockBits(sourceData);
                    blendBitmap.UnlockBits(blendData);
                }
                return sourceBitmap;
            }
        }


        //There are three ways to get legitimate data from this function
        //1. you can pass a 1 to the Red section of WvDisplayParam. Alteration of the other colors is fine.
        //2. you can pass null to the Value Tuple Constructor which will have the same effect as passing one.
        //3. You can pass a factor other than 0 to the Red Channel in order to conciously change the Metadata.

        //If you pass Zero to RedPixel this will blow up when we 

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void TakeApart_Instantly(

           Bitmap processedBitmap,
           out byte[,] _arr,
           out double redMean,
           out double redStdDev,
           out byte RedMax,
           out byte RedMin, bool minMax = false, bool Mode = false)
        {
            lock (lockobjs)
            {
                byte[,] array = new byte[processedBitmap.Width, processedBitmap.Height];
                double TotalPixels = processedBitmap.Width * processedBitmap.Height;

                (int ByteValue, int numOccurrences) ModeOutput = (0, 0);

                ulong Sum = 0;
                ulong SumSq = 0;
                BitmapData bitmapData = processedBitmap.LockBits(
                    new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height),
                    ImageLockMode.ReadOnly,
                    PixelFormat.Format24bppRgb);
                try
                {
                    byte RMax = byte.MinValue;
                    byte RMin = byte.MaxValue;
                    int bytesPerPixel = 3;
                    int heightInPixels = bitmapData.Height;
                    int widthInBytes = bitmapData.Width * bytesPerPixel;
                    byte* PtrFirstPixel = (byte*)bitmapData.Scan0;
                    int stride = bitmapData.Stride;
                    int mValue = byte.MaxValue + 1;
                    ThreadLocal<byte[]> ModeArray = new ThreadLocal<byte[]>(() => new byte[mValue], true);
                    ThreadLocal<byte> minPixel = new ThreadLocal<byte>(() => byte.MaxValue, true);
                    ThreadLocal<byte> maxPixel = new ThreadLocal<byte>(() => byte.MinValue, true);
                    if (minMax)
                    {
                        Parallel.For(0, heightInPixels, y =>
                        {
                            byte* currentLine = PtrFirstPixel + (y * stride);
                            ulong localSum = 0, localSumSq = 0;
                            byte localRedMax = byte.MinValue;
                            byte localRedMin = byte.MaxValue;

                            byte[] localModeArray = ModeArray.Value;

                            for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                            {
                                byte redPixel = currentLine[x + 2];
                                array[x / 3, y] = redPixel;
                                localSum += redPixel;
                                localSumSq += (ulong)(redPixel * redPixel);
                                if (redPixel > localRedMax) localRedMax = redPixel;
                                if (redPixel < localRedMin) localRedMin = redPixel;
                                if (Mode)
                                {
                                    localModeArray[redPixel]++;
                                }
                            }

                            Interlocked.Add(ref Sum, localSum);
                            Interlocked.Add(ref SumSq, localSumSq);
                            minPixel.Value = localRedMin;
                            maxPixel.Value = localRedMax;
                        });
                    }
                    else
                    {
                        Parallel.For(0, heightInPixels, y =>
                        {
                            byte* currentLine = PtrFirstPixel + (y * stride);
                            ulong localSum = 0, localSumSq = 0;
                            byte[] modeArray = ModeArray.Value;
                            for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                            {
                                byte redPixel = currentLine[x + 2];
                                array[x / 3, y] = redPixel;
                                localSum += redPixel;
                                localSumSq += (ulong)(redPixel * redPixel);
                                if (Mode)
                                {
                                    modeArray[redPixel]++;
                                }
                            }
                            Interlocked.Add(ref Sum, localSum);
                            Interlocked.Add(ref SumSq, localSumSq);

                            ModeArray.Values.Add(modeArray);

                        });
                    }
                    if (!minMax)
                    {
                        RMax =  byte.MinValue;
                        RMin =  byte.MaxValue;
                    }
                    else
                    {
                        RMax = maxPixel.Values.Max();
                        RMin = minPixel.Values.Min();
                    }
                    // Calculate mode
                    if (Mode)
                    {
                        byte[] finalModeArray = new byte[mValue];
                        foreach (byte[] localModeArray in ModeArray.Values)
                        {
                            for (int i = 0; i < mValue; i++)
                            {
                                finalModeArray[i] += localModeArray[i];
                            }
                        }
                        int modeValue = 0, maxOccurrences = 0;
                        for (int i = 0; i < mValue; i++)
                        {
                            if (finalModeArray[i] > maxOccurrences)
                            {
                                maxOccurrences = finalModeArray[i];
                                modeValue = i;
                            }
                        }
                        ModeOutput = (modeValue, maxOccurrences);
                    }
                    RedMax = RMax;
                    RedMin = RMin;
                    redMean = Sum / TotalPixels;
                    redStdDev = Math.Sqrt((SumSq / TotalPixels) - (redMean * redMean));
                    _arr = array;
                }
                finally
                {
                    processedBitmap.UnlockBits(bitmapData);
                }
            }
        }
#pragma warning restore CS0219
#pragma warning restore CS8600// Idea for Cloning Bitmaps very quickly.
#pragma warning restore CS8604
#pragma warning restore CS8602

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe Bitmap CloneBitmap(Bitmap source)
        {
            // Define the area of the bitmap to lock
            Rectangle rect = new Rectangle(0, 0, source.Width, source.Height);

            // Lock the source bitmap in read-only mode
            BitmapData sourceData = source.LockBits(rect, ImageLockMode.ReadOnly, source.PixelFormat);

            try
            {
                int stride = sourceData.Stride;
                int height = source.Height;
                int width = source.Width;

                // Rent a byte array from the shared pool
                byte[] buffer = ArrayPool<byte>.Shared.Rent(stride * height);

                try
                {
                    // Process each row in parallel
                    Parallel.For(0, height, y =>
                    {
                        // Pointers to the current row in the source and target data
                        byte* sourceRow = (byte*)sourceData.Scan0 + y * stride;
                        fixed (byte* targetRow = &buffer[y * stride])
                        {
                            // Copy the row directly from source to target
                            Buffer.MemoryCopy(sourceRow, targetRow, stride, stride);
                        }
                    });

                    // Pin the array and create the bitmap
                    fixed (byte* bufferPtr = buffer)
                    {
                        // Create a new bitmap using the buffer
                        Bitmap clonedBitmap = new Bitmap(width, height, stride, source.PixelFormat, (nint)bufferPtr);

                        // Return the cloned bitmap
                        return clonedBitmap;
                    }
                }
                finally
                {
                    // Return the buffer to the pool
                    ArrayPool<byte>.Shared.Return(buffer);
                }
            }
            finally
            {
                // Unlock the source bitmap
                source.UnlockBits(sourceData);
            }
        }

        //This is the old AdjustColors function. It takes roughly 2700 milliseconds. 75% of that time is spent setting the loop up. 
        //It's very irritating because all we need is a pointer to the Bitmap, I don't know why Microsoft forces us to "Lock"

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Bitmap AdjustColorsFast(Bitmap OriginalImage, float R, float G, float B)
        {
            lock (lockObject)
            {
                int height = OriginalImage.Height;
                int width = OriginalImage.Width;

                // Lock bits for the original image
                BitmapData srcData = OriginalImage.LockBits(new Rectangle(0, 0, width, height), System.Drawing.Imaging.ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                int stride = srcData.Stride;
                byte* ptr = (byte*)srcData.Scan0;
                int encodedState;
                if (G % 1 !=0 || R % 1 != 0 ||  B % 1 != 0)
                {
                    //manual iteration
                    encodedState = 0b111;
                }
                else
                {
                    encodedState =(byte)R << 2 | (byte)G << 1 | (byte)B;
                }
                int rowLength = width*3;
                try
                {
                    switch (encodedState)
                    {
                        case 0b000: // All channels off

                            break;

                        case 0b001: // Only Blue on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);

                                for (int x = 1; x < rowLength; x += 3)
                                {
                                    row[x] = 0;     // Green
                                    row[x + 1] = 0; // Red
                                }
                            });
                            break;

                        case 0b010: // Only Green on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);

                                for (int x = 0; x < rowLength; x += 3)
                                {
                                    row[x] = 0;     // Blue
                                    row[x + 2] = 0; // Red
                                }
                            });
                            break;

                        case 0b100: // Only Red on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);
                                for (int x = 0; x < rowLength; x += 3)
                                {
                                    row[x] = 0;     // Blue
                                    row[x + 1] = 0; // Green
                                }
                            });
                            break;

                        case 0b011: // Blue and Green on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);
                                for (int x = 0; x < rowLength; x += 3)
                                {
                                    row[x + 2] = 0; // Red
                                }
                            });
                            break;

                        case 0b101: // Red and Blue on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);
                                for (int x = 0; x <rowLength; x += 3)
                                {
                                    row[x + 1] = 0; // Green
                                }
                            });
                            break;

                        case 0b110: // Red and Green on
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);
                                for (int x = 0; x < rowLength; x += 3)
                                {
                                    row[x] = 0; // Blue
                                }
                            });
                            break;

                        case 0b111: // All channels on, handle arbitrary scaling
                            Parallel.For(0, height, y =>
                            {
                                byte* row = ptr + (y * stride);
                                for (int x = 0; x < rowLength; x += 3)
                                {
                                    row[x] = (byte)(row[x] * B);       // Blue
                                    row[x + 1] = (byte)(row[x + 1] * G); // Green
                                    row[x + 2] = (byte)(row[x + 2] * R); // Red
                                }
                            });
                            break;

                        default:
                            throw new InvalidOperationException("Unexpected encoded state");
                    };
                }
                finally
                {
                    OriginalImage.UnlockBits(srcData);
                }

                return OriginalImage;
            }
        }

        public static void ApplyFunctionToChannels(Bitmap bitmap, Func<byte, byte>[] channelFunctions)
        {

            if (bitmap.PixelFormat != PixelFormat.Format24bppRgb)
            {
                throw new ArgumentException("Only 24bpp RGB format is supported for channel-based operations.");
            }

            if (channelFunctions.Length != 3)
            {
                throw new ArgumentException("You must provide exactly 3 functions for the R, G, and B channels.");
            }

            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            int bytesPerPixel = 3; // R, G, B
            int stride = bitmapData.Stride;
            int height = bitmap.Height;
            int width = bitmap.Width;

            byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

            try
            {
                Parallel.For(0, height, y =>
                {
                    byte* rowPtr = ptrFirstPixel + (y * stride);

                    for (int x = 0; x < width; x++)
                    {
                        byte* pixelPtr = rowPtr + (x * bytesPerPixel);

                        // Apply functions to each channel
                        pixelPtr[0] = channelFunctions[0](pixelPtr[0]); // Blue
                        pixelPtr[1] = channelFunctions[1](pixelPtr[1]); // Green
                        pixelPtr[2] = channelFunctions[2](pixelPtr[2]); // Red
                    }
                });
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }

        }


        public static void ApplyFunctionToPixels(Bitmap bitmap, Func<byte[], byte[]> pixelFunction)
        {
            if (bitmap.PixelFormat != PixelFormat.Format24bppRgb)
            {
                throw new ArgumentException("Only 24bpp RGB format is supported for pixel-based operations.");
            }

            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            int bytesPerPixel = 3; // R, G, B
            int stride = bitmapData.Stride;
            int height = bitmap.Height;
            int width = bitmap.Width;
            byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

            try
            {
                Parallel.For(0, height, y =>
                {
                    byte* rowPtr = ptrFirstPixel + (y * stride);

                    for (int x = 0; x < width; x++)
                    {
                        byte* pixelPtr = rowPtr + (x * bytesPerPixel);

                        // Extract pixel data (R, G, B)
                        byte[] pixelData = new byte[3];
                        pixelData[0] = pixelPtr[0]; // Blue
                        pixelData[1] = pixelPtr[1]; // Green
                        pixelData[2] = pixelPtr[2]; // Red

                        // Apply the pixel function
                        byte[] newPixelData = pixelFunction(pixelData);

                        // Write back modified pixel data
                        pixelPtr[0] = newPixelData[0];
                        pixelPtr[1] = newPixelData[1];
                        pixelPtr[2] = newPixelData[2];
                    }
                });
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }

        }
        public static void ApplyFunctionToGrayscale(Bitmap bitmap, Func<byte, byte> grayFunction)
        {
            if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed)
            {
                throw new ArgumentException("Only 8bpp grayscale format is supported for grayscale operations.");
            }

            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            int stride = bitmapData.Stride;
            int height = bitmap.Height;
            int width = bitmap.Width;

            byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

            try
            {
                Parallel.For(0, height, y =>
                {
                    byte* rowPtr = ptrFirstPixel + (y * stride);

                    for (int x = 0; x < width; x++)
                    {
                        byte* pixelPtr = rowPtr + x;

                        // Apply the grayscale function
                        *pixelPtr = grayFunction(*pixelPtr);
                    }
                });
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }
        }
    }
}

