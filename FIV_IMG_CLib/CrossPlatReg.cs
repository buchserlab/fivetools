﻿
using CoreImageLoader;
using FIVE.InCellLibrary;
using FIVE_IMG.PL;
using FIVE_IMG.Plate_DB_NS;
using ICSharpCode.SharpZipLib.Tar;
using ImageMagick;
using Microsoft.VisualBasic.ApplicationServices;
using OpenCvSharp;
using OpenCvSharp.Features2D;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml.Serialization;
using Tensorflow;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;
using CV = OpenCvSharp;

namespace FIVE_IMG
{
    //https://stackoverflow.com/questions/49904033/crosscorrelation-of-images
    public class CrossCorr
    {
        public ConcurrentDictionary<double, System.Drawing.Point> SortCorrs = new ConcurrentDictionary<double, System.Drawing.Point>();
        public ConcurrentDictionary<System.Drawing.Point, byte> TriedList = new ConcurrentDictionary<System.Drawing.Point, byte>();
        public static int ccSearch_InitialStride_1 = 7; //InitialStride is the pixel stride that we will try windowing thru the image
        public static int ccSearch_InitialStride_2 = 3;
        public static int ccSearch_InitialStride_3 = 1;
        public static double ccSearch_TopPercntl_1 = 0.002; //Top percentile is the fraction of previous regions that we will "zoom" in on
        public static double ccSearch_TopPercntl_2 = 0.0002;
        public static int ccSearch_MaxTry_1 = 5; //Maximum number of regions to zoom in on
        public static int ccSearch_MaxTry_2 = 3;
        public static Register_Return Register(ImgWrap FindThis, ImgWrap AmongThat)
        {
            var c = new CrossCorr();

            var RR = new Register_Return(AmongThat, FindThis); //Starts the clock
            double score;

            //This is happening in three stages currently.
            //First, we look using a large stride (how many pixels to skip) on both the source and the destination.
            //This should be at maximum ~half the size of your objects, then we go to a small number of the best alignments and refine them further

            score = c.Search(FindThis, AmongThat, ccSearch_InitialStride_1, (int)Math.Ceiling((double)1 + ccSearch_InitialStride_1 / 2));
            RR.Notes += "Round1=" + score + "\r\n"; RR.Score_Worst = score;

            // add search near here, run it on 0.5 degree rotations from -2 to +2 degrees and shift left and right a little bit, save the best one.
            /*
            if (false)
            {   //Turn this on to see the results of the CrossCorr in the clipboard
                var sB = new StringBuilder();
                foreach (var sC in c.SortedCorrs)
                    sB.Append(sC.Value[0].X + "\t" + sC.Value[0].Y + "\t" + sC.Key + "\r\n");
                Clipboard.SetText(sB.ToString());
            }
            */
            RR.FinalR2 = -score; RR.Score_Final = score;
            RR.Location = c.Location();
            RR.Finish(); //Stops the clock
            return RR;
        }

        public System.Drawing.PointF Location()
        {
            IEnumerable<KeyValuePair<double, System.Drawing.Point>> allKeyValuePairs = SortCorrs
                .Select(point => point);


            KeyValuePair<double, System.Drawing.Point> maxKeyValuePair = allKeyValuePairs
                .Aggregate((current, next) => Math.Abs(next.Key) > Math.Abs(current.Key) ? next : current);

            return new System.Drawing.PointF(maxKeyValuePair.Value.X, maxKeyValuePair.Value.Y);
        }

        public double Search(ImgWrap FindThis, ImgWrap AmongThat, int stride, int internalstride)
        {
            return Search(FindThis, AmongThat, stride, internalstride, 0, 0, AmongThat.BMP.Width - FindThis.BMP.Width, AmongThat.BMP.Height - FindThis.BMP.Height);
        }

        public double Search(ImgWrap FindThis, ImgWrap AmongThat, int stride, int internalstride, int startX, int startY, int lastX, int lastY)
        {
            int minX = Math.Max(0, startX);
            int maxX = Math.Min(AmongThat.Width - FindThis.Width, lastX);
            int minY = Math.Max(0, startY);
            int maxY = Math.Min(AmongThat.Height - FindThis.Height, lastY);

            int totalWork = maxX - minX;
            int rangeSize = Math.Max(1, totalWork / Environment.ProcessorCount);
            OrderablePartitioner<Tuple<int, int>> rangePartitioner = Partitioner.Create(minX, maxX, rangeSize);
            Parallel.ForEach(rangePartitioner, (range, loopState) =>
            {
                for (int x = range.Item1; x < range.Item2; x++)
                {
                    for (int y = minY; y < maxY; y++)
                    {
                        System.Drawing.Point p = new System.Drawing.Point(x, y);
                        bool isAdded = TriedList.TryAdd(p, 0);
                        if (!isAdded) continue;
                        double corr = XCOR(FindThis, AmongThat, x, y, internalstride);
                        double key = -corr;
                        SortCorrs.TryAdd(key, p);
                    }
                }
            });

            IEnumerable<KeyValuePair<double, System.Drawing.Point>> allKeyValuePairs = SortCorrs
                .Select(point => point);


            KeyValuePair<double, System.Drawing.Point> maxKeyValuePair = allKeyValuePairs
                .Aggregate((current, next) => Math.Abs(next.Key) > Math.Abs(current.Key) ? next : current);

            return maxKeyValuePair.Key;
        }

        public readonly object lockobject = new object();
        public int height;
        public int width;
        public double FindThismean;
        public double AmongThisMean;
        public double FindThisStdv;
        public double AmongThisStdv;
        public byte[,] findThisArray;
        public byte[,] amongthisArray;
        public bool isInitialized = false;
        public double inverseFindThisStdv = 0;
        public double inverseAmongThisStdv = 0;

        private double XCOR(ImgWrap findThis, ImgWrap amongThat, int x, int y, int stride = 1)
        {

            lock (lockobject)
            {
                if (!isInitialized)
                {
                    height = findThis.Arr.GetLength(0);
                    width = findThis.Arr.GetLength(1);
                    FindThisStdv = findThis.RedStDev;
                    AmongThisStdv = amongThat.RedStDev;
                    findThisArray = findThis.Arr;
                    amongthisArray = amongThat.Arr;
                    FindThismean = findThis.DarkMean;
                    AmongThisMean = amongThat.DarkMean;
                    isInitialized = true;
                    inverseFindThisStdv = 1 / FindThisStdv;
                    inverseAmongThisStdv = 1 / AmongThisStdv;
                }
            }
            int totalPairs = width / stride * height / stride;
            double sum = 0;
            for (int xi = 0; xi < width; xi += stride)
            {
                for (int yi = 0; yi < height; yi += stride)
                {
                    double value1 = (findThisArray[xi, yi] - FindThismean) * inverseFindThisStdv;
                    double value2 = (amongthisArray[x + xi, y + yi] - AmongThisMean) * inverseAmongThisStdv;
                    double options = value1 * value2;
                    sum += options;
                }
            }
            return sum / totalPairs;
        }
    }


    public class Register_Return
    {
        public double PixelSizeSearch { get; set; }
        public double PixelSizeMatch { get; set; }
        public System.Drawing.PointF Location { get; set; }

        public System.Drawing.PointF ShiftMask_Pixels { get; set; }

        public string Notes { get; set; }
        public double MSEC_Total => (Finished - Start).TotalMilliseconds;

        public double FinalR2 { get; set; }
        public bool Include = true;

        public double MSEC_Pixels_1; //This is how long it takes to load the IMG Wraps (sometimes already loaded)
        public double MSEC_Pixels_2;

        public double Score_Final { get; set; }
        public double Score_AvgInitial { get; set; }
        public double Score_Worst { get; set; }

        public DateTime Start;
        public DateTime Finished;
        public double MSECTotal; //Just so this gets saved out
        public double bestAngle;
        public void Finish()
        {
            Finished = DateTime.Now;
            MSECTotal = MSEC_Total; //Just so we don't have to calculate it later
        }

        public Register_Return()
        {
            //For serialization
        }

        public Register_Return(ImgWrap Search, ImgWrap Match)
        {
            Start = DateTime.Now;
            PixelSizeSearch = Search.Width * Search.Height;
            PixelSizeMatch = Match.Width * Match.Height;

            Byte Fake; //Triggers the images to be deconstructed, which takes a little bit of time
            Fake = Search.Arr[0, 0]; MSEC_Pixels_1 = (DateTime.Now - Start).TotalMilliseconds;
            Fake = Match.Arr[0, 0]; MSEC_Pixels_2 = (DateTime.Now - Start).TotalMilliseconds;
        }
    }

    public class MetaMorph_StagePoint
    {
        public System.Drawing.PointF XY;
        public double Z;
        public string Name;

        public MetaMorph_StagePoint()
        {

        }

        public MetaMorph_StagePoint(XDCE_Image FOV, float LeicaImageWidth_um, Associate_InCell_Leica Transform)
        {
            this.Name = FOV.FileName;
            this.XY = Transform.LeicaPositionEstimate_FromGEXDCE(FOV, LeicaImageWidth_um, System.Drawing.PointF.Empty, SizeF.Empty);
        }
    }

    public class MetaMorph_MemoryList : IEnumerable<MetaMorph_StagePoint>
    {
        private List<MetaMorph_StagePoint> _List;
        public Associate_InCell_Leica Transform;

        public static MetaMorph_MemoryList FromInCellWell(XDCE_ImageGroup WellIG, Associate_InCell_Leica Transform, List<int> OnlyTheseFields = null)
        {
            MetaMorph_MemoryList ML = new MetaMorph_MemoryList(Transform);
            if (OnlyTheseFields == null) OnlyTheseFields = Enumerable.Range(WellIG.FOV_Min, WellIG.FOV_Max).ToList();
            for (int i = 0; i < OnlyTheseFields.Count; i++)
            {
                int f = OnlyTheseFields[i];
                XDCE_Image FOV = WellIG.GetField(f, 0);
                ML.AddFromFOV(FOV, InCell_To_Leica.SR.LeicaImageWidth_um);
            }
            return ML;
        }

        public MetaMorph_MemoryList(Associate_InCell_Leica TransformToUse)
        {
            _List = new List<MetaMorph_StagePoint>();
            Transform = TransformToUse;
        }

        public int Count { get => _List.Count; }

        public IEnumerator<MetaMorph_StagePoint> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public MetaMorph_StagePoint this[int index]
        {
            get { return _List[index]; }
        }

        public void Add(MetaMorph_StagePoint SPoint)
        {
            _List.Add(SPoint);
        }
        internal void AddFromFOV(XDCE_Image FOV, float LeicaImageWidth_um, double startingZPoint = double.NaN)
        {
            MetaMorph_StagePoint SP = new MetaMorph_StagePoint(FOV, LeicaImageWidth_um, Transform);
            if (!double.IsNaN(startingZPoint))
            {
                SP.Z = startingZPoint;
            }
            Add(SP);
        }

        public static StringBuilder HeaderExport(int Count)
        {
            StringBuilder SB = new StringBuilder();
            SB.Append("\"Stage Memory List\", Version 6.0\r\n");
            SB.Append("\"Stage Memory List\", Version 6.00, 0, 0, 0, 0, 0, 0, \"microns\", \"microns\"\r\n");
            SB.Append("0\r\n");
            SB.Append(Count + "\r\n");//11 is for the number of points
            return SB;
        }

        public static string LineExport(double X_um, double Y_um, double Z_um, string PointName = "")
        {
            //SB.Append("\"next", 54417.8, 30311, -530.503, 0, 0, FALSE, -9999, TRUE, TRUE, 1, -1, ""
            if (PointName == "") PointName = Math.Round(X_um, 0) + "_" + Math.Round(Y_um, 0);
            return "\"" + PointName + "\", " + X_um + ", " + Y_um + ", " + Z_um + ", 0, 0, FALSE, -9999, TRUE, TRUE, 1, -1, \"\"\r\n";
        }

        public static string LineExport(MetaMorph_StagePoint SPoint)
        {
            return LineExport(SPoint.XY.X, SPoint.XY.Y, SPoint.Z, SPoint.Name);
        }

        public string Export_MemoryListString()
        {
            StringBuilder Sb = HeaderExport(Count);
            foreach (MetaMorph_StagePoint stagePoint in _List) Sb.Append(LineExport(stagePoint));
            return Sb.ToString();
        }

        public bool Export_MemoryListFile(string Path)
        {
            string FullFile = Export_MemoryListString();
            File.WriteAllText(Path, FullFile);
            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }
    }

    public static class INI_Files
    {
        public static (string, System.Drawing.PointF) Get_Well_ini(INCELL_Folder IC_Folder, string WellLabel, string Folder, string Objective, short Binning, int FOV_Count, bool IsThereNextWell, Associate_InCell_Leica Transform, bool Cancel = false)
        {
            System.Drawing.PointF pointInWell = new System.Drawing.PointF();
            if (!IC_Folder.XDCE.Wells.ContainsKey(WellLabel)) return ("", pointInWell);

            XDCE_ImageGroup Fields = IC_Folder.XDCE.Wells[WellLabel];
            List<int> FOVs_Picked;
            if (InCell_To_Leica.SR.Reg_Params.LiveTile)
            {
                var random = new Random();
                if (InCell_To_Leica.fullAuto)
                {
                    int notSoRandFOV = InCell_To_Leica.SR.CurrentWellXDCE_5x.FOV_Max / 2;
                    switch (InCell_To_Leica.DetermineRetryCount())
                    {
                        case 2:
                            break;
                        case 1:
                            notSoRandFOV -= 3;
                            break;
                        case 0:
                            notSoRandFOV += 3;
                            break;
                        default:
                            break;
                    }
                    FOVs_Picked = new List<int>() { notSoRandFOV - 1, notSoRandFOV, notSoRandFOV + 1 };
                }
                else FOVs_Picked = new List<int>() { random.Next(InCell_To_Leica.SR.CurrentWellXDCE_5x.FOV_Min, InCell_To_Leica.SR.CurrentWellXDCE_5x.FOV_Max) };
            }
            else
                FOVs_Picked = new List<int>() { 34, 35, 36 };
            // FOVs_Picked = Fields.RandomFields(FOV_Count, true);
            MetaMorph_MemoryList stagePoints = MetaMorph_MemoryList.FromInCellWell(Fields, Transform, FOVs_Picked);
            return (Get_Well_ini(IC_Folder.PlateID, WellLabel, Folder, Objective, Binning, IsThereNextWell, stagePoints, out pointInWell), pointInWell);
        }

        public static string Get_Well_ini(string PlateID, string Well, string Folder, string Objective, short Binning, bool ContinueWells, MetaMorph_MemoryList ML, out System.Drawing.PointF pointInWell, bool Cancel = false)
        {
            //  - Well Init ini - defines the first 4 calibration wells(also reset the field ini file to not confuse the program)
            pointInWell = new System.Drawing.PointF();
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("WellFolder=\"" + Folder + "\"\r\n");
            sB.Append("Objective=\"" + Objective + "\"\r\n");
            sB.Append("Binning=" + Binning + "\r\n");
            sB.Append("Cancel=" + (Cancel ? "1":"0") + "\r\n");
            sB.Append("ContinueWells=" + (ContinueWells ? "1" : "0") + "\r\n");
            sB.Append("FOVCount=" + ML.Count + "\r\n");
            for (int i = 0; i < ML.Count; i++)
            {
                sB.Append("[" + i + "]\r\nX=" + ML[i].XY.X + "\r\nY=" + ML[i].XY.Y + "\r\nName=\"" + ML[i].Name + "\"\r\n");
                pointInWell = new System.Drawing.PointF(ML[i].XY.X, ML[i].XY.Y);
            }
            return sB.ToString();
        }

        public static string Get_Reg_Ini(bool passedReg)
        {
            InCell_To_Leica.retryReg = !passedReg;
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("TryInitRegister=" + (passedReg ? "0" : "1") + "\r\n");
            return sB.ToString();
        }
        public static string barcodeReaderWorked(bool barcodeWorked)
        {
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("ContinueWells=" + (barcodeWorked ? "1" : "0") + "\r\n");
            return sB.ToString();
        }
        public static string Get_FieldStep_ini(string PlateID, string Well, string FOV, string FOVFolder, string PAFolder, short Binning, bool ContinueFields, string MaskFile, string ShiftMaskFile, string ShiftAmount, bool Skip, bool ForceMove, bool Cancel, bool SkipWell, System.Drawing.PointF PointWell)
        {
            //- generate ini file with field coordinates
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("FOV=\"" + FOV + "\"\r\n");
            sB.Append("FOVFolder=\"" + FOVFolder + "\"\r\n");
            sB.Append("PAFolder=\"" + PAFolder + "\"\r\n");
            sB.Append("Binning=" + Binning + "\r\n");
            sB.Append("ContinueFields=" + (ContinueFields ? "1" : "0") + "\r\n");
            sB.Append("Skip=" + (Skip ? 1 : 0) + "\r\n");
            sB.Append("ForceMove=" + (ForceMove ? 1 : 0) + "\r\n");
            sB.Append("Cancel=" + (Cancel ? 1 : 0) + "\r\n");
            sB.Append("SkipWell=" + (SkipWell ? 1 : 0) + "\r\n");
            sB.Append("MaskFile=\"" + MaskFile + "\"\r\n");
            sB.Append("ShiftMaskFile=\"" + ShiftMaskFile + "\"\r\n");
            sB.Append("ShiftAmount=\"" + ShiftAmount + "\"\r\n");
            sB.Append("X=" + PointWell.X + "\r\nY=" + PointWell.Y + "\r\n");
            //sB.Append("Xa=" + PointAssumed.X + "\r\nYa=" + PointAssumed.Y + "\r\n");
            return sB.ToString();
        }
        public static string UpdateShiftMaskFile(string currentIniContent, string newShiftMaskFilePath)
        {
            var lines = currentIniContent.Split(new[] { "\r\n" }, StringSplitOptions.None);
            StringBuilder sB = new StringBuilder();

            bool shiftMaskFileFound = false;

            foreach (var line in lines)
            {
                if (line.StartsWith("ShiftMaskFile="))
                {
                    sB.AppendLine("ShiftMaskFile=\"" + newShiftMaskFilePath + "\"");
                    shiftMaskFileFound = true;
                }
                else
                {
                    sB.AppendLine(line);
                }
            }

            if (!shiftMaskFileFound)
            {
                sB.AppendLine("ShiftMaskFile=\"" + newShiftMaskFilePath + "\"");
            }

            return sB.ToString();
        }

        public static string Get_MaskStep_ini(string PlateID, string Well, string FOV, string MaskFile, System.Drawing.PointF P)
        {
            //- generate ini file with field coordinates
            StringBuilder sB = new StringBuilder();
            sB.Append("[Header]\r\n");
            sB.Append("PlateID=\"" + PlateID + "\"\r\n");
            sB.Append("Well=\"" + Well + "\"\r\n");
            sB.Append("FOV=\"" + FOV + "\"\r\n");
            sB.Append("MaskFile=\"" + MaskFile + "\"\r\n");
            sB.Append("X=" + P.X + "\r\nY=" + P.Y + "\r\n");

            return sB.ToString();
        }
    }

    public static class InCell_To_Leica
    {
        private static Random _Rand = new Random();
        public static SharedResource SR;
        public static int SRQCapacity = 12;
        public static Queue<SharedResource> SRQ = new Queue<SharedResource>(SRQCapacity);
        public static System.Drawing.PointF pointForWellRegister = new System.Drawing.PointF();
        public static wvDisplayParams wvParams = new wvDisplayParams();
        public static bool retryReg = false;
        public static bool isTestingMode = false;
        public static int successfulFOVcount;
        public static int failedFOVcount;
        public static int successfulAlignmentCount;
        public static int failedAlignmentCount;
        public static bool fullAuto;
        public static float minImgsPerField = 6F;
        public static void AddSR()
        {
            AddSR(InCell_To_Leica.SR);
        }

        public static void AddSR(SharedResource tSR)
        {
            //Worked on this 6/17/2021, but have to come back to it . .
            if (SRQ.Count >= SRQCapacity) SRQ.Dequeue();
            SRQ.Enqueue(tSR.Copy());
        }

        public static void Leica001_WellStart(INCELL_Folder IC_Folder, string WellList, string FieldStart, string WellFieldChecks, short wavelength, float brightness, wvDisplayParams wvDispParams, SVP_Registration_Params RegParams)
        {
            //Setup and save for future use the AIL object (right now static)

            wvParams = wvDispParams;
            int FieldStartInt;
            if (!int.TryParse(FieldStart, out FieldStartInt)) FieldStartInt = 1;
            int WellFieldChecksInt;
            if (!int.TryParse(WellFieldChecks, out WellFieldChecksInt)) WellFieldChecksInt = 5;
            if (WellList == null) WellList = "A - 1";
            fullAuto = RegParams.FullAuto;
            if (RegParams.LiveTile)
            {
                // TODO: Make this more general (division by 16 is a 20x -> 5x thing)
                float fieldFilledPercent = minImgsPerField / 16F;
                var Recon = new INCELL_Folder_Reconstruct(IC_Folder, null, 5, 0.4F, "", true); Recon.Reconstruct_Scan();
                var ICF_5x = Recon.ICFolder_New;
                SR = new SharedResource(IC_Folder, WellList, FieldStartInt, WellFieldChecksInt, wavelength, brightness, @"C:\temp\AILBase\", RegParams, "20210505_100224-C", false, ICF_5x);
                //SR.expectedImages = new LeicaImageQueue();
                //SR.expectedImages.PreLoad(SR.CurrentWellXDCE, InCell_To_Leica.wvParams);
                // TODO- Get pixelBinning from .jnl
                
                string pathToIni = Path.Combine(InCell_To_Leica.SR.Path_INI_Folder, "settings.ini");
                foreach (string line in File.ReadLines(pathToIni))
                {
                    if (line.StartsWith("LeicaPixelBinning="))
                    {
                        if (double.TryParse(line.Split('=')[1], out double value))
                        {
                            InCell_To_Leica.SR.pixelBinning = (float)value;
                            break;
                        }
                    }
                }
            }
            else
                SR = new SharedResource(IC_Folder, WellList, FieldStartInt, WellFieldChecksInt, wavelength, brightness, @"C:\temp\AILBase\", RegParams, "20210505_100224-C", false);
            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "finished 001" + "\r\n");
            //Leica000_CheckAgainstAll(@"C:\Temp\AILBase\FIV538_2\20210505_100224-C");
        }
        /*
        private static void Leica000_CheckAgainstAll(string FolderName)
        {
            //This is a way to check 
            DirectoryInfo DI = new DirectoryInfo(FolderName);
            foreach (FileInfo file in DI.GetFiles("*.tif"))
                CheckLeicaImage_AgainstAllFields(file.FullName);
        }

        private static void CheckLeicaImage_AgainstAllFields(string FileName)
        {
            ImgWrap imgWrap = new ImgWrap(FileName);
            ImageAlign_Return IAR;
            SortedList<double, ImageAlign_Return> Results = new SortedList<double, ImageAlign_Return>();
            float size = 0.45f;
            foreach (XDCE_Image xI in SR.SavedICFolder20x.XDCE.Wells["A - 3"].Images.Where(x => x.wavelength == SR.Wavelength))
            {
                for (float x = 0; x < 1 - size; x += size)
                {
                    for (float y = 0; y < 1 - size; y += size)
                    {
                        IAR = SR.AIL.ImageAlign_Distant(imgWrap, xI, SR.Brightness, new System.Drawing.PointF(x, y), new SizeF(size, size));
                        Results.Add(IAR.RR.Score_Final, IAR);
                        if (Results.Count > 20)
                        {
                            File.Delete(Results.Last().Value.OverlayName); Results.RemoveAt(Results.Count - 1);
                        }
                    }
                }
                Debug.WriteLine(xI.FileName + " " + Results.First().Key);
                //SR.Brightness = SR.Brightness * 0.8f;
            }
            File.WriteAllLines(@"c:\temp\trymatch2.txt", Results.Select(x => (x.Key + "\t" + x.Value.OverlayName)).ToList());
        }
        */
        public static string Leica002_NextWell()
        {
            SR.IncrementWell();
            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"starting 002- well {SR.CurrentWell}" + "\r\n");
            if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                (string ini, pointForWellRegister) = INI_Files.Get_Well_ini(SR.SavedICFolder5x, SR.CurrentWell, SR.Path_WellImages, SR.Objective, SR.BinningWell, SR.WellFieldChecks, SR.IsThereNextWell, SR.AIL);
                File.WriteAllText(SR.FullPath_Wellini, ini);
                return "Created ini file for " + SR.CurrentWell;
            }
            else if (!SR.CurrentWellXDCE.HasMasks)
            {
                return "Well didn't have masks (something went wrong)";
            }
            else
            {
                (string ini, pointForWellRegister) = INI_Files.Get_Well_ini(SR.SavedICFolder20x, SR.CurrentWell, SR.Path_WellImages, SR.Objective, SR.BinningWell, SR.WellFieldChecks, SR.IsThereNextWell, SR.AIL);
                File.WriteAllText(SR.FullPath_Wellini, ini);
                return "Created ini file for " + SR.CurrentWell;
            }
        }

        public static string Leica002_RetryWell()
        {
            if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"retrying 002- well {SR.CurrentWell}" + "\r\n");
                (string ini, pointForWellRegister) = INI_Files.Get_Well_ini(SR.SavedICFolder5x, SR.CurrentWell, SR.Path_WellImages, SR.Objective, SR.BinningWell, SR.WellFieldChecks, SR.IsThereNextWell, SR.AIL);
                File.WriteAllText(SR.FullPath_Wellini, ini);
                return "Retry- Created ini file for " + SR.CurrentWell;
            }
            else if (!SR.CurrentWellXDCE.HasMasks)
            {
                return "Well didn't have masks (something went wrong)";
            }
            else
            {
                (string ini, pointForWellRegister) = INI_Files.Get_Well_ini(SR.SavedICFolder20x, SR.CurrentWell, SR.Path_WellImages, SR.Objective, SR.BinningWell, SR.WellFieldChecks, SR.IsThereNextWell, SR.AIL);
                File.WriteAllText(SR.FullPath_Wellini, ini);
                return "Created ini file for " + SR.CurrentWell;
            }
        }

        public static int DetermineRetryCount()
        {
            string pathToIni = Path.Combine(InCell_To_Leica.SR.Path_INI_Folder, "settings.ini");
            foreach (string line in File.ReadLines(pathToIni))
            {
                if (line.StartsWith("Retry_Counter="))
                {
                    if (int.TryParse(line.Split('=')[1], out int value))
                    {
                        return value;
                    }
                }
            }
            return InCell_To_Leica.SR.maxRetry;
        }

        /// <summary>
        /// Uses the ini file created in 002 to register three images in the same region of the well. Success means all three images returned similar offsets
        /// </summary>
        /// <returns>A boolean if offsets are "close enough", and a note on the mode of failure/success </returns>
        public static (bool passed, string result) Leica003b_RegisterWell()
        {

            //We know that we got the number of images asked for, so we have to use them to refine the transform of interest
            FileHandler.SafelyWrite(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Start Register Well .." + "\r\n");
            //File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Start Register Well .." + "\r\n");

            var PSC = new Plate_Session_Class(new DirectoryInfo(SR.Path_WellImages), null);
            var Well = SR.CurrentWellXDCE;

            // let's extract all the well_points
            List<CV.Point> wellPoints = new List<CV.Point>();
            bool inSection = false;
            double xValue = 0.0, yValue = 0.0;
            var regex = new Regex(@"^\[\d+\]$"); // Regex to match sections like [0], [1], etc.

            foreach (var line in File.ReadLines(SR.FullPath_Wellini))
            {
                if (regex.IsMatch(line.Trim()))
                {
                    if (inSection)
                    {
                        // Process the previous section's X and Y values
                        int truncatedX = (int)xValue;
                        int truncatedY = (int)yValue;

                        xValue = yValue = 0.0;
                        wellPoints.add(new CV.Point(truncatedX, truncatedY));
                    }

                    inSection = true;
                }
                else if (inSection && line.StartsWith("X="))
                {
                    xValue = double.Parse(line.Substring(2));
                }
                else if (inSection && line.StartsWith("Y="))
                {
                    yValue = double.Parse(line.Substring(2));
                }
            }

            // Process the last section's X and Y values
            if (inSection)
            {
                int truncatedX = (int)xValue;
                int truncatedY = (int)yValue;
                wellPoints.add(new CV.Point(truncatedX, truncatedY));
            }

            string pointsInWell = "";
            for (int i = 0; i < wellPoints.Count; i++) 
            {
                pointsInWell += wellPoints[i].ToString()+", ";
            }
            FileHandler.SafelyWrite(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "points for images trimmed from wellini: " + "\r\n" + pointsInWell + "\r\n");
            //File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "points for images trimmed from wellini: " + "\r\n" + pointsInWell + "\r\n");

            List<System.Drawing.PointF> foundOffsets = new List<System.Drawing.PointF>();


            // Go to these points in well, and attempt to register. If all registrations give reasonably close offsets, then continue to register well. Else, try again / quit
            foreach (var point in wellPoints)
            {
                FileHandler.SafelyWrite(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"point- {point}" + "\r\n");
                // File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"point- {point}" + "\r\n");
                // This will run 3 times in our automated code. If all Offsets are within a close range, we're good.
                int firstNumber = point.X; // Your first number
                int secondNumber = point.Y; // Your second number

                string pattern = $@"^{firstNumber}.{{0,5}}_{secondNumber}.{{0,5}}_";

                var wellRegisterImage = PSC.Image_List.FirstOrDefault(image => Regex.IsMatch(image.Name, pattern));
                if (InCell_To_Leica.isTestingMode) wellRegisterImage = PSC.Image_List.First();
                FileHandler.SafelyWrite(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "image got! name begins with " + pattern + "\r\n");
                // File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "image got! name begins with " + pattern + "\r\n");

                SR.Offset_FieldRefined = SR.AIL.Initial_Leica_Offset_um;
                SR.Offset_WellRefined = SR.AIL.Initial_Leica_Offset_um;
                SR.AIL.Error_Divisor = SR.Reg_Params.InitialErrorDivisor;
                //SR.TestingMode = true;
                if (SR.TestingMode)
                {
                    //PSC = new Plate_Session_Class(new DirectoryInfo(@"R:\FIVE\EXP\FIV538\6 Images\20210619A\Well"), null);
                    SR.Offset_WellRefined = new System.Drawing.PointF() { X = 895.4479F, Y = 2318.917F };
                }
                else
                {
                    if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x20_Standard)
                        SR.AIL.FindOffset_CrossPlatform(PSC, Well, SR.Wavelength, SR.Brightness, out SR.Offset_WellRefined, out SR.WellBased_MaxRange_LastSet, true);
                    if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile) 
                    {
                        int saved = 0; int unsaved = 0;
                        SR.AIL.MannyFindOffset_CrossPlatform_20x_5x_Live(wellRegisterImage, Well, SR.Wavelength, SR.Brightness, out SR.Offset_WellRefined, out SR.WellBased_MaxRange_LastSet, ref saved, ref unsaved, true, !InCell_To_Leica.isTestingMode);
                        FileHandler.SafelyWrite(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + SR.Offset_WellRefined + "|" + " Notes on reg: " + SR.AIL.LastNote + "\r\n");
                        // File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + SR.Offset_WellRefined + "|" + " Notes on reg: " + SR.AIL.LastNote + "\r\n");
                        
                        if (SR.Offset_WellRefined == new System.Drawing.PointF(0, 0) && SR.WellBased_MaxRange_LastSet == new System.Drawing.PointF(0, 0))
                        {
                            continue;
                        }
                        else
                        {
                            foundOffsets.add(SR.Offset_WellRefined);
                        }
                    }
                }
            }

            // Get average offset, compare each offset to avg. If the difference is > 300 in x or y, registration definitely failed
            if (SR.automateRegistration)
            {
                PointF avgPoint = new PointF(0, 0);

                if (foundOffsets.Count > 2)
                {
                    float totalX = 0, totalY = 0;
                    foreach (PointF p in foundOffsets)
                    {
                        totalX += p.X;
                        totalY += p.Y;
                    }

                    float avgX = totalX / foundOffsets.Count;
                    float avgY = totalY / foundOffsets.Count;

                    avgPoint = new PointF(avgX, avgY);
                }
                else 
                    return (false, "One or more registrations unsuccessful.");

                foreach (PointF p in foundOffsets)
                {
                    if (Math.Abs(p.X - avgPoint.X) > 300 || Math.Abs(p.Y - avgPoint.Y) > 300)
                        return (false, "Initial registrations returned different offsets");
                }
            }

            return (true, "Registered well and found the following offsets " + SR.Offset_WellRefined.ToString() + " " + SR.AIL.LastNote);
        }


        public static DateTime FOVTracker = DateTime.MinValue;

        /// <summary>
        /// During a run, this updates the ini to tell Leica where the camera should look. Also implements a timer for manual runs
        /// </summary>
        /// <returns>A boolean if offsets are "close enough", and a note on the mode of failure/success </returns>
        public static string Leica004_NextFOV()
        {
            FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", $"{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")}\tGet Next FOV..");
            // File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Get Next FOV.." + "\r\n");
            if (SR.CancelRequested) { WriteOutLatestField(null, false, true); return "Cancel Requested"; }


            SR.IncrementFOV(); XDCE_Image FOV = SR.CurrentFOV;

            //Use the previous learned offset here . .
            WriteOutLatestField(FOV); //Write out the ini information
            FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", $"{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")}\tNext FOV Done");
            // File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Next FOV Done" + "\r\n");
            if (!SR.automateRegistration)
            {
                var totImgs = (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
                    ? SR.FOVCount / SR.CurrentWellXDCE.Wavelength_Count
                    : SR.FOVCount / SR.CurrentWellXDCE_5x.Wavelength_Count;

                string TimeEstimate = "";
                if (SR.MultiObjectiveStyle != MultiObjEnum.x20_x5_LiveTile)
                {
                    SR.AIL.Initial_Leica_Offset_um = SR.GetNextOffset_Mix();
                    if (true) MasksHelper.ExportShiftedMask(FOV, SR.Reg_Params);
                    //This exports a shifted version of the mask to use
                    if (FOVTracker == DateTime.MinValue) FOVTracker = DateTime.Now; //This means we are at the first one
                    if (SR.FOVIdx >= 4)
                    {
                        double imgsRemaining = totImgs - SR.FOVIdx;
                        double timeElapsed = (DateTime.Now - FOVTracker).TotalMinutes;
                        double timePerImg = timeElapsed / SR.FOVIdx;
                        double Minutes = imgsRemaining * timePerImg;
                        TimeEstimate = Minutes.ToString("0.0") + " Min remaining.";
                    }
                }
                else
                {
                    if (FOVTracker == DateTime.MinValue) FOVTracker = DateTime.Now; //This means we are at the first one
                    if (SR.FOVIdx > 1)
                    {
                        double imgsRemaining = totImgs - SR.FOVIdx;
                        double timeElapsed = (DateTime.Now - FOVTracker).TotalMinutes;
                        double timePerImg = timeElapsed / SR.FOVIdx;
                        double Minutes = imgsRemaining * timePerImg;
                        TimeEstimate = Minutes.ToString("0.0") + " Min remaining.";
                    }
                }
                return "Updated INI for " + SR.CurrentWell + "." + FOV.FOV.ToString() + ", which is " + SR.FOVIdx + "/" + totImgs + (TimeEstimate == "" ? "" : " " + TimeEstimate);
            }
            else
                return "";
        }

        private static void WriteOutLatestField(XDCE_Image FOV, bool Skip = false, bool Cancel = false)
        {
            //Now make a new ini for the field we are working on
            if (!Cancel) SR.SetCurrentPos(FOV);
            bool ForceMove = SR.DifferentThanLast_Y; //TODO: This should look at the delta from the current position to the refined position and decide
            bool SkipWell = SR.skipWell;
            string FOVStr = FOV == null ? "" : FOV.FOV.ToString();
            string ini = "";
            ini = INI_Files.Get_FieldStep_ini(SR.PlateID, SR.CurrentWell, FOVStr, SR.Path_FieldImage, SR.Path_PhotoActivatedImages, SR.BinningField, SR.IsThereNextField, SR.CurrentMaskFullPath, SR.CurrentShiftMaskFullPath, SR.Shift, Skip, ForceMove, Cancel, SkipWell, SR.CurrentFOVPos);
            File.WriteAllText(SR.FullPath_FOVini, ini);
            SR.skipWell = false;
        }

        /// <summary>
        /// This is designed to run on a smaller search area to quickly refine the field's position based on the Well's offset
        /// </summary>
        /// <param name="Opt">Usually we are in Opt=1, but if we have already moved to the optimal position then use Opt=2</param>
        /// <returns>A message about the results</returns>
        public static string Leica005_RefineFOV(float InitialErrorDivisor, int Opt = 1)
        {
            int preSavedImages = 0; int notPreSavedImages = 0;
            var ExportOverlayImages = true;
            string Note = "";
            var PSC = new Plate_Session_Class(new DirectoryInfo(SR.Path_FieldImage), null);
            var imgLE = PSC.GetFromPosition(SR.CurrentFOVPos);
            //InCell_To_Leica.SR.expectedImages.PreLoad(InCell_To_Leica.SR.CurrentWellXDCE, InCell_To_Leica.wvParams);
            if (imgLE == null)
            {
                //Debugger.Break(); //Something is wrong, couldn't find the matching field
                WriteOutLatestField(SR.CurrentFOV, true);
                Note = "Error refining FOV, Couldnt find matching field. ";
                InCell_To_Leica.failedFOVcount++;
                File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV" + "\t" + SR.CurrentFOVPos + "\t" + Note + "\r\n");
                return Note + SR.CurrentFOVPos;
            }
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV" + "\t" + SR.CurrentFOVPos + "\t" + imgLE.Name + "\r\n");

            if (Opt == 2) { SR.AIL.Initial_Leica_Offset_um = SR.Offset_FieldRefined; }//Normal in Option 1, continue to use the Mixed Field/Well starting point (changed 5/25/2022)
            SR.AIL.Error_Divisor = InitialErrorDivisor;
            ImageAlign_Return IAR;

            if (SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                SR.AIL.MannyFindOffset_CrossPlatform_20x_5x_Live(imgLE, SR.CurrentWellXDCE, SR.Wavelength, SR.Brightness, out SR.Offset_WellRefined, out SR.WellBased_MaxRange_LastSet, ref preSavedImages, ref notPreSavedImages, ExportOverlayImages, false);
                SR.preSavedImages += preSavedImages; SR.notPreSavedImages += notPreSavedImages;
                if (SR.WellBased_MaxRange_LastSet == System.Drawing.PointF.Empty)
                {
                    Note = "Failed to get good refinement, skipping this FOV";
                    WriteOutLatestField(SR.CurrentFOV, true);
                    InCell_To_Leica.failedFOVcount++;
                    File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV" + "\t" + SR.CurrentFOVPos + "\t" + imgLE.Name + "\t" + Note + "\r\n");
                    return Note;
                }
                else
                {
                    SR.AIL.Initial_Leica_Offset_um = SR.Offset_WellRefined;
                    Note = "GoodAlign";
                    WriteOutLatestField(SR.CurrentFOV);
                    InCell_To_Leica.successfulFOVcount++;
                    File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV" + "\t" + SR.CurrentFOVPos + "\t" + imgLE.Name + "\t" + Note + "\r\n");
                }
                File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + preSavedImages + "images from preload" + "\t" + notPreSavedImages + "images from memory" + "\r\n");
            }
            else
            {
                IAR = SR.AIL.ImageAlign(imgLE, SR.CurrentFOV, SR.Brightness, true, false, false);
                if (IAR.RR == null || IAR.RR.FinalR2 < 0.1)
                {
                    SR.AIL.Error_Divisor *= 1.5F; //It is fixed next time around
                    var IAR1b = SR.AIL.ImageAlign(imgLE, SR.CurrentFOV, SR.Brightness * 2.5F, true, false, false); //Do the registration
                    if (IAR1b.RR != null && IAR1b.RR.FinalR2 > 0.1) IAR = IAR1b;
                    else
                    {
                        Note = "Failed to get good refinement, skipping this FOV " + IAR.Note + "," + IAR1b.Note;
                        WriteOutLatestField(SR.CurrentFOV, true);
                        File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Refine FOV" + "\t" + SR.CurrentFOVPos + "\t" + imgLE.Name + "\t" + Note + "\r\n");
                        return Note;
                    }
                }
                //PointF CurrentFOVPos = SR.AIL.LeicaPositionEstimate_FromGEXDCE(SR.CurrentFOV, SR.LeicaImageWidth_um); ////Just for debugging . . 

                var TempOffset = new System.Drawing.PointF(IAR.Offset.X, IAR.Offset.Y);
                (bool WithinMaxBounds, var OffsetAmount) = SR.IsOffsetWithinMaxBounds(TempOffset);
                if (WithinMaxBounds)
                {
                    //Adjust the position
                    Note = "GoodAlign";
                    SR.Offset_FieldRefined = TempOffset;
                    SR.Shift = "(" + IAR.RR.ShiftMask_Pixels.X.ToString("0.0") + "," + IAR.RR.ShiftMask_Pixels.Y.ToString("0.0") + ")";
                    //The IAR is already saved insde of the ImageAlign bit
                    SR.AIL.Initial_Leica_Offset_um = SR.Offset_FieldRefined; //Use this as the new initial offset
                    WriteOutLatestField(SR.CurrentFOV); //re-make the FOV ini file to go to the refined position
                }
                else
                {
                    Note = "OutsideOfMaxBounds " + OffsetAmount;
                    //Says to skip this field since we think registration may have failed
                    WriteOutLatestField(SR.CurrentFOV, true);
                }
                if (Opt == 2) //This is when we go back to the optimized position a second time . . 
                {
                    Note = "2ndLevelRegistration";
                    //Make a file that has the Mask overlaid with the real file
                    Bitmap Mask = new Bitmap(new Bitmap(SR.CurrentMaskFullPath), imgLE.BMP.Size);
                    Bitmap rBMP = CoreLogic.ArithmeticBlend(Mask, imgLE.BMP, CoreLogic.ColorCalculationType.Difference);
                    DirectoryInfo DI = new DirectoryInfo(SR.Path_Overlays + " wMask");
                    if (!DI.Exists) DI.Create();
                    rBMP.Save(Path.Combine(DI.FullName, imgLE.Name + ".bmp"));
                }
                //Save the name so we can easily delete this image file on the next cycle
                SR.FOVImageToDelete = imgLE.PathToImage;
            }
            //Do the registration
            //if (IAR.RR == null)
            //{
            //    IAR = SR.AIL.ImageAlign(imgLE, SR.CurrentFOV, SR.Brightness * 4, true); //Try registration again but brighten the image
            //} 

            AddSR(SR); //Adds to the SR queue. Saving the IAR happens inside the ImageAlign above
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "FOV Aligned" + "\t" + SR.CurrentFOVPos + "\t" + imgLE.Name + "\t" + Note + "\r\n");
            return "Refined FOV " + SR.Offset_FieldRefined;
        }

        public static string Leica007_SaveCropped()
        {
            //TODO IMPLEMENT this for testing (maybe)
            string newMaskName = ""; //= SR.IAR.ExportShiftedMask(SR.CurrentFOV); //Resave a mask image with the correct stuff
            return newMaskName;
        }

        /// <summary>
        /// Wrap it up! At the end of a plate's registration, write out how many images were preloaded vs not preloaded.
        /// </summary>
        /// <param name="optionalMessage"> Write a log out to the log file if there's anything to note about the run. </param>
        public static void Leica009_WrapUp(string optionalMessage = null)
        {
            SR.WriteOutRegSuccess();
            File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"{SR.preSavedImages} / {SR.notPreSavedImages + SR.preSavedImages} images grabbed from queue" + "\r\n");
            if (optionalMessage != null) File.AppendAllText(SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"{optionalMessage}" + "\r\n");
            if (InCell_To_Leica.fullAuto)
            {
                File.WriteAllText(SR.Path_fullAuto, SR.PlateID + "Finished");
            }
        }

        //Find currently-running instance
        //https://stackoverflow.com/questions/26938984/open-file-in-the-currently-running-instance-of-my-program

        //Send receive message between programs
        //https://stackoverflow.com/questions/11358841/send-receive-message-to-from-two-running-application

        //Pipes
        //https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-use-anonymous-pipes-for-local-interprocess-communication

        //  - Scan on Geic
        //  - InCarta
        //  - Select cells and intensities
        //  - Generate mask images (specific folder that can be built into IC_Folder)
        //- Leica Run Program
        //- Set Obective, Channel, and approximate focus
        //- FIVTools.exe LeicaStart_001
        //  - Loads usual form, but in "Leica Connect mode", add an extra button to the right once things have been tested(Raft Cal)
        //  - Somewhere to enter the wells to scan(like a list A1, A2, A3, etc)
        //  - WellStart ini - defines the first 4 calibration wells(also reset the field ini file to not confuse the program)
        //- Goto Well
        //  - Load INI File(PlateDB/Auto01/WellStart.ini)
        //  - Get 4 registration images(Saves to PlateDB/Auto01/WellTemp
        //  - Calls FIV tools (wait till finished) - remebers previous settings(maybe just loads the previous ini file to get where it left off)
        //    - FIVTools.exe WellRegister_A5
        //	- Generate offsets(wider error in FIV Tools)
        //	- check that things make sense
        //	- generate ini file with field coordinates
        //  - Step every GE FOV
        //    - Leica looks for a file that instructs starting field(PlateDB/Auto01/FieldStep.ini)
        //    - Get small reg image
        //	- Call FIV Tools
        //	  - Get offset from GE Field(smaller error range in FIV Tools
        //	  - FIV tools returns with another file that tells it where to go
        //	  - Preload next image into memory so it is ready (can't keep them all in memory)
        //	- Move Leica stage to exact position
        //	- Photoactivate
        //	- Call FIV Tools that finished photo-activation
        //- Find out from Josh how he does a multi-channel scan (preferred way), so we can build this for both GE and Leica versions
        //- Leica use 4x4 binning for registration images
        //- Where does the instruction file get saved
        //- Folders for my Leica journals
        //- Instead of the main FIV-tools (which remains open and can keep some of the images in-memory), we can have a utility that just talks to FIV tools, but can open and close.Alternatively we can have a setting where FIV tools doesn't load its GUI, and just runs a function then closes
    }

    public class Associate_InCell_Leica
    {
        internal SVP_Registration_Params Reg_Params;
        public System.Drawing.PointF Default_InitialOffset_um; //This one doesn't change
        public System.Drawing.PointF Initial_Leica_Offset_um;  //This one changes
        public float X_Error_Pixels = 510;      //This is like the search space
        public float Y_Error_Pixels = 460;
        public float Error_Divisor = 1.3F;      //Used to be 1.0F 5/23/2023
        public float resize_divisor_forRegistration = 2;
        public SizeF GE_LeicaSizeRatio_ForImage;   //This is when the GE image is produced for used in registration
        public SizeF GE_LeicaSizeRatio_ForMoving;  //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset)
        public string ExportLoc = @"c:\temp\export\";
        public string LastNote = "";
        //Manny added this to see if we should try to tile the images that failed to go.
        bool forceFit = false;
        bool imgQueueLog = false;
        public Associate_InCell_Leica(SVP_Registration_Params Reg_Params)
        {
            this.Reg_Params = Reg_Params;
            Default_InitialOffset_um = Reg_Params.InitialDefaultOffset;
            Initial_Leica_Offset_um = Reg_Params.InitialDefaultOffset;
            GE_LeicaSizeRatio_ForImage = Reg_Params.GE_LeicaSizeRatio_ForImage;
            GE_LeicaSizeRatio_ForMoving = Reg_Params.GE_LeicaSizeRatio_ForMoving;
        }

        public static void GenerateMemoryList(INCELL_Folder icFolder, string Well, string SaveLocationOfMemoryList, float StartingZPoint, SVP_Registration_Params Reg_Params)
        {

            if (!icFolder.XDCE.Wells.ContainsKey(Well)) return;
            XDCE_ImageGroup WellIG = icFolder.XDCE.Wells[Well];
            MetaMorph_MemoryList ML = MetaMorph_MemoryList.FromInCellWell(WellIG, new Associate_InCell_Leica(Reg_Params));
            ML.Export_MemoryListFile(SaveLocationOfMemoryList);
        }

        public static void CompareAllLeicaSessionImages(INCELL_Folder icFolder, int icWvIdx, float icBrightness, string PlateDB_Location, SVP_Registration_Params Reg_Params, bool ExportOverlayImages = false, System.ComponentModel.BackgroundWorker BW = null)
        {
            //I think this is just used in testing
            var AC = new Associate_InCell_Leica(Reg_Params); Plate_Session_Class PSC;

            Debug.Print("Loaded DB and finding matches . .");
            bool TestMode = true;
            if (TestMode)
            {
                PSC = new Plate_Session_Class(new DirectoryInfo(@"E:\Temp\AILBase\FIV497P6\20210505_100224-C\FOV"), null);
                //AC.Error_Divisor = 1; //This makes the error smaller
            }
            else
            {
                Plate_DB_Class PDB = new Plate_DB_Class(PlateDB_Location);
                PSC = PDB.FindNewestSession(icFolder.PlateID_NoScanIndex);
            }
            //AC.FindOffset_CrossPlatform(PSC, icFolder.XDCE, icWvIdx, icBrightness, , , ExportOverlayImages);
        }

        public void FindOffset_CrossPlatform(Plate_Session_Class PSC, XDCE_ImageGroup InCellPlate_or_Well, short icWvIdx, float icBrightness, out System.Drawing.PointF NewOffsets, out System.Drawing.PointF Ranges, bool ExportOverlayImages = false)
        {
            LastNote = "";
            var IAR_Dict = new Dictionary<string, List<ImageAlign_Return>>();
            foreach (ImgWrap imgLE in PSC.Image_List)
            {

                (List<Tuple<XDCE_Image, string>> xI_list, System.Drawing.PointF LeicaInitialCenter_um) = GetClosestXDCEImage(imgLE, InCellPlate_or_Well, icWvIdx);
                var xI = xI_list.First().Item1;
                var IAR = ImageAlign_Multi(imgLE, xI, icBrightness, ExportOverlayImages); //var IAR = ImageAlign(imgLE, InCellPlate_or_Well, icWvIdx, icBrightness, ExportOverlayImages); //Swapped 9/6/2023
                if (IAR.RR != null)
                {
                    if (!IAR_Dict.ContainsKey(IAR.WellLabel)) IAR_Dict.Add(IAR.WellLabel, new List<ImageAlign_Return>());
                    IAR_Dict[IAR.WellLabel].Add(IAR);
                } //The note may indicate why
                else LastNote = IAR.Note;
            }
            if (IAR_Dict.Count == 0)
            {   //None matched
                Ranges = new System.Drawing.PointF(0, 0); NewOffsets = new System.Drawing.PointF(0, 0); return;
            }
            //Take stuff out of the SR List
            var KeepList = new List<SharedResource>(IAR_Dict.Count);
            foreach (var tSR in InCell_To_Leica.SRQ)
            {
                var T = IAR_Dict.First().Value.Where(x => x.OverlayName == tSR.OverlayPath);
                if (T.Count() > 0) KeepList.Add(tSR);
            }
            if (KeepList.Count > 0) InCell_To_Leica.SRQ = new Queue<SharedResource>(KeepList);

            //Construct the look-up table
            List<ImageAlign_Return> IAR_List = IAR_Dict.First().Value;
            string S = string.Join("\r\n", IAR_List.Select(x => x.ExportName + "\t" + x.FractCenter_GE_um.X + "\t" + x.FractCenter_GE_um.Y + "\t" + x.FractCenter_LE_um.X + "\t" + x.FractCenter_LE_um.Y + "\t" + x.RR.FinalR2 + "\t" + x.ImgInternal_RedVariation + "\t" + x.Note));

            //Calculate regression 
            double X_slope, X_intr, X_r2, Y_slope, Y_intr, Y_r2;
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.X).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.X).ToArray(), out X_r2, out X_intr, out X_slope);
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.Y).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.Y).ToArray(), out Y_r2, out Y_intr, out Y_slope);

            //Get the Ranges of offsets from the positions 5/2022
            var Offsets = IAR_List.Select(x => new System.Drawing.PointF(x.FractCenter_GE_um.X - x.FractCenter_LE_um.X, x.FractCenter_GE_um.Y - x.FractCenter_LE_um.Y));
            Ranges = new System.Drawing.PointF(Offsets.Max(x => x.X) - Offsets.Min(x => x.X), Offsets.Max(x => x.Y) - Offsets.Min(x => x.Y));

            //Here is the final best range
            //TODO: This could just use the pre-existing Offsets made above instead of re-making it 5/2022
            NewOffsets = new System.Drawing.PointF(
                IAR_List.Select(x => (x.FractCenter_GE_um.X - x.FractCenter_LE_um.X)).Average(),
                IAR_List.Select(x => (x.FractCenter_GE_um.Y - x.FractCenter_LE_um.Y)).Average());
        }

        /*
         * This function takes in the well, and one 5x image and registers it. 
         */
        public void MannyFindOffset_CrossPlatform_20x_5x_Live(ImgWrap imgLE, XDCE_ImageGroup InCellPlate_or_Well, short icWvIdx, float icBrightness, out System.Drawing.PointF NewOffsets, out System.Drawing.PointF Ranges, ref int preSavedImagesUsed, ref int notPreSavedImagesUsed, bool ExportOverlayImages = false, bool wellRegistration = false)
        {
            LastNote = "";
            int unexpectedImageCount = 0;
            var IAR_Dict = new Dictionary<string, List<ImageAlign_Return>>();
            System.Drawing.PointF avgOffset;
            (var IAR, var NewMask, var overlay, avgOffset, var Range, double avgScore, int imgsAlignedCount, int imgsFailedCount, preSavedImagesUsed, notPreSavedImagesUsed) = ImageAlign_20x_5x(imgLE, InCellPlate_or_Well, icWvIdx, icBrightness, out unexpectedImageCount, ExportOverlayImages, wellRegistration);
            if (!wellRegistration)
            {
                InCell_To_Leica.successfulAlignmentCount += imgsAlignedCount;
                InCell_To_Leica.failedAlignmentCount += imgsFailedCount;
            }
            if (IAR == null)
            {
                Ranges = System.Drawing.PointF.Empty;
                NewOffsets = wellRegistration ? System.Drawing.PointF.Empty : InCell_To_Leica.SR.Offset_WellRefined;
                return;
            }
            var overlayName = InCell_To_Leica.SR.Path_Overlays + $"\\time_{DateTime.Now.ToString("HHmmss")}_name_{imgLE.Name}Overlay.bmp";
            InCell_To_Leica.SR.x5OverlayPath = overlayName;
            InCell_To_Leica.SR.avgOffset = avgOffset;
            MasksHelper.Export5xShiftedOverlay(overlayName, overlay, imgLE);
            if (!wellRegistration)
            {
                MasksHelper.Export5xShiftedMask(NewMask, imgLE.Name);
            }
            if (avgScore > 0.5 && imgsAlignedCount > 4)
            {
                NewOffsets = new System.Drawing.PointF((float)((InCell_To_Leica.SR.AIL.Initial_Leica_Offset_um.X + avgOffset.X)/2), (float)((InCell_To_Leica.SR.AIL.Initial_Leica_Offset_um.Y + avgOffset.Y)/2));
                FileHandler.SafelyWrite(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "above the scoring threshold :D" + "\r\n");
            }
            else
            {
                Ranges = System.Drawing.PointF.Empty;
                NewOffsets = wellRegistration ? System.Drawing.PointF.Empty : InCell_To_Leica.SR.AIL.Initial_Leica_Offset_um;
                FileHandler.SafelyWrite(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "below the scoring threshold D:" + "\r\n");
                return;
            }

            InCell_To_Leica.AddSR();

            if (IAR.RR != null)
            {
                if (!IAR_Dict.ContainsKey(IAR.WellLabel)) IAR_Dict.Add(IAR.WellLabel, new List<ImageAlign_Return>());
                IAR_Dict[IAR.WellLabel].Add(IAR);
            } //The note may indicate why
            else LastNote = IAR.Note;

            if (IAR_Dict.Count == 0)
            {   //None matched
                Ranges = System.Drawing.PointF.Empty; NewOffsets = InCell_To_Leica.SR.Offset_WellRefined;
                return;
            }
            //Take stuff out of the SR List
            var KeepList = new List<SharedResource>(IAR_Dict.Count);
            foreach (var tSR in InCell_To_Leica.SRQ)
            {
                var T = IAR_Dict.First().Value.Where(x => x.OverlayName == tSR.OverlayPath);
                if (T.Count() > 0) KeepList.Add(tSR);
            }
            if (KeepList.Count > 0) InCell_To_Leica.SRQ = new Queue<SharedResource>(KeepList);

            //Construct the look-up table
            List<ImageAlign_Return> IAR_List = IAR_Dict.First().Value;
            string S = string.Join("\r\n", IAR_List.Select(x => x.ExportName + "\t" + x.FractCenter_GE_um.X + "\t" + x.FractCenter_GE_um.Y + "\t" + x.FractCenter_LE_um.X + "\t" + x.FractCenter_LE_um.Y + "\t" + x.RR.FinalR2 + "\t" + x.ImgInternal_RedVariation + "\t" + x.Note));

            //Calculate regression 
            double X_slope, X_intr, X_r2, Y_slope, Y_intr, Y_r2;
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.X).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.X).ToArray(), out X_r2, out X_intr, out X_slope);
            Utilities.MathUtils.LinearRegression(IAR_List.Select(x => x.FractCenter_LE_um.Y).ToArray(), IAR_List.Select(x => x.FractCenter_GE_um.Y).ToArray(), out Y_r2, out Y_intr, out Y_slope);

            //Get the Ranges of offsets from the positions 5/2022
            // var Offsets = IAR_List.Select(x => new System.Drawing.PointF(x.FractCenter_GE_um.X - x.FractCenter_LE_um.X, x.FractCenter_GE_um.Y - x.FractCenter_LE_um.Y));
            // Ranges = new System.Drawing.PointF(Offsets.Max(x => x.X) - Offsets.Min(x => x.X), Offsets.Max(x => x.Y) - Offsets.Min(x => x.Y));
            Ranges = Range;
            //Here is the final best range
        }

        static ColorMatrix colorMatrixWhiteToMagenta = new()
        {
            Matrix00 = 0.5f, // Red
            Matrix11 = 0.5f, // Green
            Matrix22 = 1,    // Blue remains unchanged
            Matrix33 = 1,    // Alpha remains unchanged
            Matrix44 = 1     // Bias remains unchanged
        };

        static ColorMatrix colorMatrixWhiteToYellow = new()
        {
            Matrix00 = 1,    // Red remains unchanged
            Matrix11 = 1,    // Green remains unchanged
            Matrix22 = 0,    // Blue removed
            Matrix33 = 1,    // Alpha remains unchanged
            Matrix44 = 1     // Bias remains unchanged
        };

        static ColorMatrix colorMatrixWhiteToRed = new()
        {
            Matrix00 = 1,    // Red remains unchanged
            Matrix11 = 0,    // Green removed
            Matrix22 = 0,    // Blue removed
            Matrix33 = 1,    // Alpha remains unchanged
            Matrix44 = 1     // Bias remains unchanged
        };

        static ColorMatrix colorMatrixWhiteToAliceBlue = new()
        {
            Matrix00 = 1,    // Red remains unchanged
            Matrix11 = 0,    // Green removed
            Matrix22 = 0,    // Blue removed
            Matrix33 = 1,    // Alpha remains unchanged
            Matrix44 = 1     // Bias remains unchanged
        };
        public static Bitmap SetImageOpacity(Bitmap original, float alpha = 0.6f)
        {
            var newBitmap = new Bitmap(original.Width, original.Height); var attributes = new ImageAttributes();
            var matrix = new ColorMatrix() { Matrix00 = 1, Matrix11 = 1, Matrix22 = 1, Matrix33 = alpha };
            attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            using (var g = Graphics.FromImage(newBitmap))
                g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height), 0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
            return newBitmap;
        }

        private static ImageAttributes CreateImageAttributes(ColorMatrix matrix)
        {
            var imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(matrix);
            return imgAttributes;
        }
        static ImageAttributes TileCheck_imgAttributesWhiteToYellow = CreateImageAttributes(colorMatrixWhiteToYellow);
        static ImageAttributes TileCheck_imgAttributesWhiteToMagenta = CreateImageAttributes(colorMatrixWhiteToMagenta);
        static ImageAttributes TileCheck_imgAttributesWhiteToRed = CreateImageAttributes(colorMatrixWhiteToRed);
        static ImageAttributes TileCheck_imgAttributesWhiteToAliceBlue = CreateImageAttributes(colorMatrixWhiteToAliceBlue);

        static Font TileCheck_Font = new Font("Arial", 60);
        static SolidBrush TileCheck_Brush = new SolidBrush(Color.Blue);

        //if (rIMGs == null) return new ImageAlign_Return("", "Couldn't find a match in this InCell Scan");
        private (List<Tuple<XDCE_Image, string>>, System.Drawing.PointF) GetClosestXDCEImage(ImgWrap imgLE, XDCE_ImageGroup InCellPlate_or_Well, short icWvIdx)
        {
            PointF LeicaInitialCenter_um;
            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                LeicaInitialCenter_um = LeicaImageCenter_Estimate_SR(imgLE);
            }
            else
            {
                LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);
            }

            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Img x bounds are {LeicaInitialCenter_um.X-imgLE.Width_um/2} and {LeicaInitialCenter_um.X + imgLE.Width_um / 2}" + "\r\n" +
                $"Img Y bounds are {LeicaInitialCenter_um.Y - imgLE.Height_um / 2} and {LeicaInitialCenter_um.Y + imgLE.Height_um / 2}" + "\r\n" + "Center estimate is " + LeicaInitialCenter_um);

            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                List<Tuple<List<XDCE_Image>, string>> LiveTileIMGs = InCellPlate_or_Well.GetClosestFieldsForLiveTile(LeicaInitialCenter_um.X, LeicaInitialCenter_um.Y, imgLE.Width_um);
                if (LiveTileIMGs == null)
                    return (null, new System.Drawing.PointF(0, 0));
                List<Tuple<XDCE_Image, string>> xIs = LiveTileIMGs
                .Select(x => new Tuple<XDCE_Image, string>(x.Item1.Where(img => img.wavelength == icWvIdx).First(), x.Item2))
                .ToList();
                return (xIs, LeicaInitialCenter_um);
            }
            else
            {
                List<XDCE_Image> rIMGs = InCellPlate_or_Well.GetClosestFields(LeicaInitialCenter_um.X, LeicaInitialCenter_um.Y);
                if (rIMGs == null)
                    return (null, new System.Drawing.PointF(0, 0));
                var xI = rIMGs.Where(x => x.wavelength == icWvIdx).First();
                return (new List<Tuple<XDCE_Image, string>> { new Tuple<XDCE_Image, string>(xI, "C") }, LeicaInitialCenter_um);
            }


        }

        // Crop!
        public Bitmap CropImage(Bitmap source, Rectangle cropRect)
        {
            // Create a new blank bitmap with the size of the cropping rectangle
            Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);

            // Use Graphics object to draw the section of the source image onto the new bitmap
            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(source,
                            new Rectangle(0, 0, cropRect.Width, cropRect.Height),
                            cropRect,
                            GraphicsUnit.Pixel);
            }

            return target;
        }

        void UpdateOffsetBounds(System.Drawing.PointF currentOffset, ref System.Drawing.PointF minOffset, ref System.Drawing.PointF maxOffset)
        {
            // Update minOffset
            if (currentOffset.X < minOffset.X)
            {
                minOffset.X = currentOffset.X;
            }
            if (currentOffset.Y < minOffset.Y)
            {
                minOffset.Y = currentOffset.Y;
            }

            // Update maxOffset
            if (currentOffset.X > maxOffset.X)
            {
                maxOffset.X = currentOffset.X;
            }
            if (currentOffset.Y > maxOffset.Y)
            {
                maxOffset.Y = currentOffset.Y;
            }
        }


        public (ImageAlign_Return IAR, Bitmap ReconstructedMask, Bitmap ReconstructedOverlay, System.Drawing.PointF AvgOffset, System.Drawing.PointF OffsetRange, double avgScore, int imgAlignedCount, int imgFailedCount, int preSavedImages, int notPreSavedImages) ImageAlign_20x_5x(ImgWrap imgLE, XDCE_ImageGroup InCellPlate_or_Well, short icWvIdx, float icBrightness, out int unexpectedImageCount, bool ExportOverlay = false, bool wellRegistration = false, bool AddCenterTick = false, bool ExportMatch = false)
        {
            var imgQueLog = new StringBuilder();
            imgQueueLog = false;
            bool imageAlignLog = true;
            unexpectedImageCount = 0;
            bool crop5x = true;
            // TODO: Fix magic #s.
            double maskWidth = 2048;
            double maskHeight = 2048;
            int preSavedImages = 0; int notPreSavedImages = 0;

            (List<Tuple<XDCE_Image, string>> xI_CentralList, System.Drawing.PointF LeicaInitialCenter_um) = GetClosestXDCEImage(imgLE, InCellPlate_or_Well, icWvIdx);
            XDCE_Image xI_Central = null;
            Tuple<ImageAlign_Return, Bitmap, Bitmap, PointF, PointF, double> bestAlignment = new Tuple<ImageAlign_Return, Bitmap, Bitmap, PointF, PointF, double>(null, null, null, PointF.Empty, PointF.Empty, 0);
            var minOffset = new PointF(float.MaxValue, float.MaxValue);
            var maxOffset = new PointF(float.MinValue, float.MinValue);

            // Have to make this for reg
            ImageAlign_Return IAR = null;
            List<Tuple<ImgWrap, XDCE_Image, System.Drawing.Point, double, string>> wrapsInFOV = new List<Tuple<ImgWrap, XDCE_Image, System.Drawing.Point, double, string>>();
            double bestDev = Reg_Params.ImageMinStDev;
            int loopLimit = xI_CentralList.Count;
            float scoreThreshold = 0.5F;
            FileHandler.SafelyWrite(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"we have {loopLimit} images" + "\r\n");

            if (loopLimit == 0)
                return (null, null, null, System.Drawing.PointF.Empty, System.Drawing.Point.Empty, 0, 0, 0, 0, 0);
            for (int i = 0; i < loopLimit; i++)
            {
                var value = xI_CentralList[i];
                XDCE_Image img = value.Item1;
                if (img == null) continue;
                RectangleF R_GEFract_Pix = new RectangleF(0, 0, img.Width_Pixels, img.Height_Pixels);
                var CenterPixel_LE = img.Pixel_FromPlateCoordinates(LeicaInitialCenter_um);
                Bitmap potentialInitialRegister;
                FileHandler.SafelyWrite(InCell_To_Leica.SR.FullPath_Log, "About to grab froim queue" + "\r\n");
                //if (InCell_To_Leica.SR.expectedImages.ContainsKey(img, InCell_To_Leica.wvParams))
                if (false)
                {
                    FileHandler.SafelyWrite(InCell_To_Leica.SR.FullPath_Log, "in queue" + "\r\n");
                    if (imgQueueLog)
                    {
                        imgQueLog.AppendLine(DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "grabbed from expectedImages");
                    }
                    //File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "grabbed from expectedImages" + "\r\n");
                    potentialInitialRegister = InCell_To_Leica.SR.expectedImages.Get_Bitmap(img, InCell_To_Leica.wvParams);
                    preSavedImages++;
                    if (potentialInitialRegister.PixelFormat == PixelFormat.DontCare)
                    {
                        // try again, but the other way
                        if (imgQueueLog)
                        {
                            imgQueLog.AppendLine(DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "in expectedImgs, but DontCare format");

                        }
                        //File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "in expectedImgs, but DontCare format" + "\r\n");
                        potentialInitialRegister = GetInCellBMP(img, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                        notPreSavedImages++;
                    }
                }
                else
                {
                    File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, "not in queue" + "\r\n");
                    if (imgQueueLog)
                    {
                        imgQueLog.AppendLine(DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "not found in expectedImages");
                    }
                    //File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "not found in expectedImages" + "\r\n");
                    potentialInitialRegister = GetInCellBMP(img, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                    notPreSavedImages++;
                    if (potentialInitialRegister.PixelFormat == PixelFormat.DontCare)
                    {
                        if (imgQueueLog)
                        {
                            imgQueLog.AppendLine(DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "DontCare format. retrying");
                        }
                        //File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + img.FOV + "\t" + "initReg" + "\t" + "DontCare format. retrying" + "\r\n");
                        potentialInitialRegister = GetInCellBMP(img, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                        notPreSavedImages++;
                    }
                    unexpectedImageCount++;
                }
                FileHandler.SafelyWrite("C:\\temp\\AILBase\\ini\\ImgQueLog.txt", imgQueLog.ToString());
                if (potentialInitialRegister.PixelFormat == PixelFormat.DontCare)
                    continue;
                ImgWrap potentialInitialWrap = new ImgWrap(potentialInitialRegister);
                double wrapStDev = potentialInitialWrap.RedStDev;

                wrapsInFOV.Add(new Tuple<ImgWrap, XDCE_Image, System.Drawing.Point, double, string>(potentialInitialWrap, img, new System.Drawing.Point(InCellPlate_or_Well.X_to_Index[img.PlateX_um], InCellPlate_or_Well.Y_to_Index[img.PlateY_um]), -wrapStDev, value.Item2));
            }

            var stDevOrderedWrapsInFOV = wrapsInFOV.OrderBy(item => item.Item4).ToList();
            int initialAligns = 0;
            var startAligning = DateTime.Now;
            int totalFieldSuccesses = 0;
            int totalFieldFailures = 0;

            foreach (var stuff in stDevOrderedWrapsInFOV)
            {
                initialAligns++;
                ImgWrap potImgWrap = stuff.Item1;
                XDCE_Image potXi = stuff.Item2;
                System.Drawing.Point img_xy = stuff.Item3;
                double imgStDev = -stuff.Item4;

                var IAR_central = ImageAlign_Multi_ForLiveTile(imgLE, potXi, icBrightness, ref preSavedImages, ref notPreSavedImages, crop5x, ExportOverlay, AddCenterTick, ExportMatch, potImgWrap);

                if (IAR_central.RR == null)
                {
                    continue;
                }
                if (imageAlignLog)
                    FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"central align #{initialAligns}: {potXi.FileName}" + "\t" + $"Score was {IAR_central.RR.FinalR2.ToString("0.0")}");

                if (IAR_central.RR.FinalR2 < scoreThreshold) continue;
                if (imageAlignLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"wellRegistration: {wellRegistration}. this offset: {IAR_central.Offset}; reference offset: {InCell_To_Leica.SR.Offset_WellRefined}");

                // double check what central image is based on this offset!
                // save old well offset, check center with new offset, reset to original offset. 
                // bool offsetTooFar = Math.Abs(IAR_central.Offset.X - InCell_To_Leica.SR.Offset_WellRefined.X) > 150 || Math.Abs(IAR_central.Offset.Y - InCell_To_Leica.SR.Offset_WellRefined.Y) > 150;
                var wellOffset = new System.Drawing.PointF(InCell_To_Leica.SR.Offset_WellRefined.X, InCell_To_Leica.SR.Offset_WellRefined.Y);

                bool offsetTooFar = Math.Abs(IAR_central.Offset.X - Default_InitialOffset_um.X) > 150 || Math.Abs(IAR_central.Offset.Y - Default_InitialOffset_um.Y) > 150;
                if (offsetTooFar && !wellRegistration)
                {
                    InCell_To_Leica.SR.Offset_WellRefined = Default_InitialOffset_um;
                    // Let's try to save this Registration!
                    /*IAR_central = ImageAlign_Multi_ForLiveTile(imgLE, potXi, icBrightness, ref preSavedImages, ref notPreSavedImages, crop5x, ExportOverlay, AddCenterTick, ExportMatch, null, true);
                    if (IAR_central == null)
                        continue;
                    if (IAR_central.RR == null)
                        continue;
                    var dist = new System.Drawing.Point((int)Math.Abs(IAR_central.Offset.X - InCell_To_Leica.SR.Offset_WellRefined.X), (int)Math.Abs(IAR_central.Offset.Y - InCell_To_Leica.SR.Offset_WellRefined.Y));
                    offsetTooFar = dist.X > 300 || dist.Y > 300;
                    //if (IAR_central.RR?.FinalR2 < scoreThreshold || offsetTooFar)
                    if (true)
                    {
                        // if (imageAlignLog) File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Saving failed- score was {IAR_central.RR.FinalR2}" + "\r\n");
                        if (imageAlignLog) File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"skipping save, but score was {IAR_central.RR.FinalR2} and dist was {dist.X}, {dist.Y}" + "\r\n");
                        continue;
                    }
                    if (imageAlignLog) File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"tried central align #{initialAligns}" + "\t" + $"Above threshold! nice save" + "\r\n");*/
                }
                else
                {
                    InCell_To_Leica.SR.Offset_WellRefined = IAR_central.Offset;
                }
                (List<Tuple<XDCE_Image, string>> centralList, System.Drawing.PointF initCenter) = GetClosestXDCEImage(imgLE, InCellPlate_or_Well, icWvIdx);
                if (centralList.Any())           
                    xI_Central = centralList.First().Item1;
                else
                    xI_Central = potXi;
                InCell_To_Leica.SR.Offset_WellRefined = wellOffset;

                IAR = IAR_central;
                InCell_To_Leica.SR.Offset_FieldRefined = IAR_central.Offset;


                UpdateOffsetBounds(IAR.Offset, ref minOffset, ref maxOffset);

                int leftOffset, rightOffset, upOffset, downOffset;
                leftOffset = rightOffset = 2;
                upOffset = downOffset = 2;

                var fieldImages = new List<Tuple<XDCE_Image, System.Drawing.Point>>();
                (int c, int r) centralColRow = (InCellPlate_or_Well.X_to_Index[xI_Central.PlateX_um], InCellPlate_or_Well.Y_to_Index[xI_Central.PlateY_um]);  //centralRowCol(int r, int c)  = new Tuple<int, int>(InCellPlate_or_Well.X_to_Index[xI.PlateX_um], InCellPlate_or_Well.Y_to_Index[xI.PlateY_um]);

                for (int rowCounter = centralColRow.r - upOffset; rowCounter <= centralColRow.r + downOffset; rowCounter++)
                {
                    if (rowCounter < 0) continue;
                    for (int colCounter = centralColRow.c - leftOffset; colCounter <= centralColRow.c + rightOffset; colCounter++)
                    {
                        if (colCounter < 0) continue;
                        System.Drawing.Point imgColRow = new System.Drawing.Point(colCounter, rowCounter);
                        if (wrapsInFOV.Any(tup => tup.Item3 == imgColRow)) continue;
                        var imgOfInt = InCellPlate_or_Well.GetImage(rowCounter, colCounter, icWvIdx);
                        if (imgOfInt == null) continue;
                        if (fieldImages.Any(tup => tup.Item1 == imgOfInt)) continue;
                        fieldImages.Add(new Tuple<XDCE_Image, System.Drawing.Point>(imgOfInt, imgColRow));
                    }
                }
                if (imageAlignLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + "Grabbed other images in field");

                double scaleFactor = (double)imgLE.Width_um / xI_Central.Width_microns;

                double maskDimensions = maskWidth;
                double overlayDimensions = xI_Central.Width_Pixels;

                int MaskBMPSize = (int)(maskDimensions * scaleFactor);
                int overlayBMPSize = (int)(overlayDimensions * scaleFactor);

                double adjustRR = (double)MaskBMPSize / imgLE.Width;

                var ReconMask_BMP = new Bitmap(MaskBMPSize, MaskBMPSize);
                var overlayImage = new Bitmap(overlayBMPSize, overlayBMPSize);
                var overlayToMaskRatio = (double)overlayBMPSize / MaskBMPSize;
                bool testMaskRecon = false;
                var sumOfOffsets = new System.Drawing.PointF(0, 0);
                System.Drawing.PointF avgOffset = new System.Drawing.PointF(0, 0);
                crop5x = true;
                double avgScore = 0;

                List<Tuple<XDCE_Image, string, System.Drawing.Point>> unfitImages = new List<Tuple<XDCE_Image, string, System.Drawing.Point>>();
                //var listOfR2 = new List<Tuple<string, Point>>();
                List<PointF> offsets = new List<PointF>();

                using (var g = Graphics.FromImage(ReconMask_BMP))
                {
                    if (testMaskRecon)
                        g.DrawImage(imgLE.BMP, new Rectangle(0, 0, MaskBMPSize, MaskBMPSize), 0, 0, imgLE.BMP.Width, imgLE.BMP.Height, GraphicsUnit.Pixel, TileCheck_imgAttributesWhiteToMagenta);
                    var startTime = DateTime.Now;
                    int alignedWraps = 0;
                    int failedWraps = 0;
                    foreach (var wrap_XDCE_point in wrapsInFOV)
                    {
                        ImgWrap wrap = wrap_XDCE_point.Item1;
                        XDCE_Image xI = wrap_XDCE_point.Item2;
                        System.Drawing.Point spot = wrap_XDCE_point.Item3;
                        var IAR_Sub = ImageAlign_Multi_ForLiveTile(imgLE, xI, icBrightness, ref preSavedImages, ref notPreSavedImages, crop5x, ExportOverlay, AddCenterTick, ExportMatch, wrap);

                        if (IAR_Sub.RR == null)
                        {
                            if (IAR_Sub.Note == "cropped out")
                                continue;
                            failedWraps++;
                            unfitImages.add(new Tuple<XDCE_Image, string, System.Drawing.Point>(xI, "null", spot));
                            continue;
                        }


                        offsets.Add(IAR_Sub.Offset);

                        if (IAR_Sub.RR.FinalR2 < scoreThreshold) // chosen UNrigorously
                        {
                            failedWraps++;
                            unfitImages.add(new Tuple<XDCE_Image, string, System.Drawing.Point>(xI, IAR_Sub.RR.FinalR2.ToString("0.00"), spot));
                            continue;
                        }
                        UpdateOffsetBounds(IAR_Sub.Offset, ref minOffset, ref maxOffset);
                        alignedWraps++;
                        avgScore += IAR_Sub.RR.FinalR2;
                        sumOfOffsets.X += IAR_Sub.Offset.X;
                        sumOfOffsets.Y += IAR_Sub.Offset.Y;

                        var maskX = MaskBMPSize / 2 - adjustRR * ((IAR_Sub.FractCenter_LE_um.X - imgLE.X_umCenter) + xI_Central.Width_microns / 2) / imgLE.umPerpixel;
                        var maskY = MaskBMPSize / 2 - adjustRR * ((IAR_Sub.FractCenter_LE_um.Y - imgLE.Y_umCenter) + xI_Central.Height_microns / 2) / imgLE.umPerpixel;


                        var overlayX = overlayToMaskRatio * maskX;
                        var overlayY = overlayToMaskRatio * maskY;
                        var rectToDraw = new Rectangle((int)maskX, (int)maskY, (int)maskDimensions, (int)maskDimensions);
                        var overlayRect = new Rectangle((int)overlayX, (int)overlayY, (int)overlayDimensions, (int)overlayDimensions);
                        GenerateOverlay(xI, overlayRect, overlayImage, spot);

                        var why = (string)("FOV" + xI.FOV + "\r\n" + "R2=" + IAR_Sub.RR.FinalR2.ToString("0.0") + "@ row,col:" + spot.X + "," + spot.Y);

                        if (!wellRegistration && xI.HasMask)
                        {
                            g.DrawImage(new Bitmap(xI.MaskFullPath), rectToDraw);
                        }

                    }
                    var endTime = DateTime.Now;
                    if (imageAlignLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"{alignedWraps} / {wrapsInFOV.Count} wrapped images aligned in {endTime - startTime} sec");

                    startTime = DateTime.Now;
                    int alignedFieldImages = 0;
                    int failedFieldImages = 0;
                    foreach (var img_rowcol in fieldImages)
                    {
                        var xITry = img_rowcol.Item1;
                        if (xITry == null) continue;
                        if (xITry.wavelength != 0) continue;

                        var spot = img_rowcol.Item2;
                        var FOV = xITry.FOV + 1;

                        var IAR_Sub = ImageAlign_Multi_ForLiveTile(imgLE, xITry, icBrightness, ref preSavedImages, ref notPreSavedImages, crop5x, ExportOverlay, AddCenterTick, ExportMatch);
                        if (IAR_Sub.RR == null)
                        {
                            if (IAR_Sub.Note == "cropped out")
                                continue;
                            failedFieldImages++;
                            unfitImages.add(new Tuple<XDCE_Image, string, System.Drawing.Point>(xITry, "null", spot));
                            continue;
                        }

                        offsets.add(IAR_Sub.Offset);
                        if (IAR_Sub.RR.FinalR2 < scoreThreshold) // chosen UNrigorously
                        {
                            failedFieldImages++;
                            unfitImages.add(new Tuple<XDCE_Image, string, System.Drawing.Point>(xITry, IAR_Sub.RR.FinalR2.ToString("0.00"), spot));
                            continue;
                        }
                        UpdateOffsetBounds(IAR_Sub.Offset, ref minOffset, ref maxOffset);
                        alignedFieldImages++;
                        avgScore += IAR_Sub.RR.FinalR2;
                        sumOfOffsets.X += IAR_Sub.Offset.X;
                        sumOfOffsets.Y += IAR_Sub.Offset.Y;

                        var maskX = MaskBMPSize / 2 - adjustRR * ((IAR_Sub.FractCenter_LE_um.X - imgLE.X_umCenter) + xI_Central.Width_microns / 2) / imgLE.umPerpixel;
                        var maskY = MaskBMPSize / 2 - adjustRR * ((IAR_Sub.FractCenter_LE_um.Y - imgLE.Y_umCenter) + xI_Central.Height_microns / 2) / imgLE.umPerpixel;


                        var overlayX = overlayToMaskRatio * maskX;
                        var overlayY = overlayToMaskRatio * maskY;
                        var rectToDraw = new Rectangle((int)maskX, (int)maskY, (int)maskDimensions, (int)maskDimensions);

                        var overlayRect = new Rectangle((int)overlayX, (int)overlayY, (int)overlayDimensions, (int)overlayDimensions);
                        GenerateOverlay(xITry, overlayRect, overlayImage, spot);

                        if (!wellRegistration && xITry.HasMask)
                        {
                            g.DrawImage(new Bitmap(xITry.MaskFullPath), rectToDraw);
                        }
                    }
                    endTime = DateTime.Now;
                    if (imageAlignLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"{alignedFieldImages} / {fieldImages.Count} unwrapped images aligned in {endTime - startTime} sec");
                    // Keep running tally of successes, out of total well images
                    totalFieldFailures = failedWraps + failedFieldImages;
                    totalFieldSuccesses = alignedWraps + alignedFieldImages;

                    avgOffset.X = offsets.Average(point => point.X); avgOffset.Y = offsets.Average(point => point.Y);
                    double percentFailed = 0;
                    if (totalFieldSuccesses != 0)
                    {
                        percentFailed = unfitImages.Count / totalFieldSuccesses;
                        avgScore = avgScore / totalFieldSuccesses;
                    }

                    if (imageAlignLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\Log.txt", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"avgScore = {avgScore}, imgsAligned = {totalFieldSuccesses}");

                    if (totalFieldSuccesses < 5) continue;
                }

                Bitmap resizedOverlay = new Bitmap(overlayImage, (int)(imgLE.Width), (int)(imgLE.Height));
                overlayImage.Dispose();
                var range = new System.Drawing.PointF(maxOffset.X - minOffset.X, maxOffset.Y - minOffset.Y); ;

                maskWidth = maskHeight = 2048;
                var finalReconMask = ConvertTo1bpp(new Bitmap(ReconMask_BMP, (int)maskWidth, (int)maskHeight));
                ReconMask_BMP.Dispose();
                if (imageAlignLog)
                return (IAR, finalReconMask, resizedOverlay, avgOffset, range, avgScore, totalFieldSuccesses, totalFieldFailures, preSavedImages, notPreSavedImages);
            }
            return (null, null, null, System.Drawing.PointF.Empty, System.Drawing.Point.Empty, 0, 0, 0, 0, 0);
        }

        public static Bitmap ConvertTo1bpp(Bitmap original)
        {
            var blackAndWhite = new Bitmap(original.Width, original.Height, PixelFormat.Format1bppIndexed);

            // Define the palette. Only black and white are needed.
            ColorPalette palette = blackAndWhite.Palette;
            palette.Entries[0] = Color.Black;
            palette.Entries[1] = Color.White;
            blackAndWhite.Palette = palette;

            // Lock the bitmaps' bits.
            BitmapData originalData = original.LockBits(new Rectangle(0, 0, original.Width, original.Height), ImageLockMode.ReadOnly, original.PixelFormat);
            BitmapData bwData = blackAndWhite.LockBits(new Rectangle(0, 0, blackAndWhite.Width, blackAndWhite.Height), ImageLockMode.WriteOnly, blackAndWhite.PixelFormat);

            int originalStride = originalData.Stride;
            int bwStride = bwData.Stride;

            unsafe
            {
                byte* originalPtr = (byte*)originalData.Scan0;
                byte* bwPtr = (byte*)bwData.Scan0;

                for (int y = 0; y < original.Height; y++)
                {
                    byte* oRow = originalPtr + (y * originalStride);
                    byte* bwRow = bwPtr + (y * bwStride);

                    for (int x = 0; x < original.Width; x++)
                    {
                        // Compute grayscale value
                        byte grayValue = (byte)(oRow[x * 4] * 0.11 + oRow[x * 4 + 1] * 0.59 + oRow[x * 4 + 2] * 0.3); // BGR order

                        // Set the pixel value
                        if (grayValue > 127)
                        {
                            // Set the bit to white
                            bwRow[x / 8] |= (byte)(0x80 >> (x % 8));
                        }
                        else
                        {
                            // Set the bit to black
                            bwRow[x / 8] &= (byte)~(0x80 >> (x % 8));
                        }
                    }
                }
            }

            // Unlock the bits of both bitmaps.
            original.UnlockBits(originalData);
            blackAndWhite.UnlockBits(bwData);

            return blackAndWhite;
        }

        public void GenerateOverlay(XDCE_Image xI, Rectangle drawItHere, Bitmap onThis, System.Drawing.Point spot)
        {
            string text = $"FOV,row,col: {xI.FOV},{spot.X},{spot.Y}";
            const int Brightness = 25;
            using (var g = Graphics.FromImage(onThis))
            {
                using Bitmap BMP = RotateImage180(Utilities.DrawingUtils.GetAdjustedImageSt(xI, Brightness));
                g.DrawImage(BMP, drawItHere);

                // Setting up font and brush.
                Font drawFont = new Font("Arial", 80); // choose your font and size
                SolidBrush drawBrush = new SolidBrush(Color.White); // choose your color

                // Calculate the position to start the draw.
                float x = drawItHere.X + (drawItHere.Width - g.MeasureString(text, drawFont).Width) / 2;
                float y = drawItHere.Y + (drawItHere.Height - g.MeasureString(text, drawFont).Height) / 2;

                // Draw string to screen.
                g.DrawString(text, drawFont, drawBrush, x, y);
            }
        }

        public static Bitmap RotateImage180(Bitmap originalImage)
        {
            Bitmap rotatedImage = (Bitmap)originalImage.Clone();
            rotatedImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            return rotatedImage;
        }
        public Bitmap AddPadding(Bitmap originalImage, int padding)
        {
            // Create a new image with padding
            var newWidth = originalImage.Width + (2 * padding);
            var newHeight = originalImage.Height + (2 * padding);
            var newImage = new Bitmap(newWidth, newHeight);

            using (var g = Graphics.FromImage(newImage))
            {
                // Set the background color to black (you can change this)
                g.Clear(Color.Black);
                // Draw the original image onto the new image at the padding position
                g.DrawImage(originalImage, padding, padding);
            }

            return newImage;
        }

        public ImageAlign_Return ImageAlign_Multi(ImgWrap imgLE, XDCE_Image xI, float icBrightness, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false, ImgWrap wrap = null)
        {
            //ExportMatch = true;
            //AddCenterTick = true;
            //Default_InitialOffset_um = new PointF();
            //Initial_Leica_Offset_um = Default_InitialOffset_um;

            // InCell_To_Leica.SR.MultiObjectiveStyle

            bool DoMatchingPoints = false;
            if (DoMatchingPoints)
            {
                //try and check various points
                string S = MatchingPointsText(imgLE, xI);
            }

            //For testing, do 2 rounds of stuff
            ImageAlign_Return IAR;

            IAR = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
            if (IAR.RR != null)
            {
                if (true)
                {
                    if (IAR.RR.FinalR2 < 0.1)
                    {

                        var Error_Divisor_T = Error_Divisor;
                        Error_Divisor *= 1.3F;
                        var IAR1b = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
                        Error_Divisor = Error_Divisor_T;

                        if (IAR1b.RR != null)
                        {
                            if (IAR1b.RR.FinalR2 > IAR.RR.FinalR2) IAR = IAR1b;
                        }
                    }

                    if (IAR.RR.FinalR2 < 0.1) { IAR.Note = "Poor " + IAR.RR.FinalR2.ToString("0.00"); IAR.RR = null; return IAR; }
                }
            }

            bool SecondCheck = false;
            if (SecondCheck)
                // this.Initial_Leica_Offset_um = new System.Drawing.PointF((IAR.Offset.X + Default_InitialOffset_um.X) / 2, (IAR.Offset.Y + Default_InitialOffset_um.Y) / 2); //Willie made this so it can't get pulled too far away
            {
                if (IAR.RR != null)
                {
                    //It should be very fast since it doesn't have much area to explore
                    RefineBaseOnAlignment(this, IAR, 1000); //Fixes up the intial offset and turns the error spread way down 
                    ImageAlign_Return IAR2 = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
                    this.Error_Divisor = 1;
                    float dist = ImageAlign_Return.Distance(IAR, IAR2);
                    IAR.Note += "/r/n Distance between two versions (5 is cut-off) = " + dist;
                    if (dist > 5)
                    {
                        //Questionable, so lets revert back to the starting
                        this.Initial_Leica_Offset_um = Default_InitialOffset_um;
                    }
                    else
                    {
                        //Did good, but lets be safe and go halfway there
                        this.Initial_Leica_Offset_um = Utilities.MathUtils.GetAverage(Default_InitialOffset_um, IAR.Offset);
                    }
                }
            }
            return IAR;
        }

        public ImageAlign_Return ImageAlign_Multi_ForLiveTile(ImgWrap imgLE, XDCE_Image xI, float icBrightness, ref int preSavedImages, ref int notPreSavedImages, bool crop5x = false, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false, ImgWrap wrap = null, bool savingRegistration = false)
        {
            bool DoMatchingPoints = false;
            if (DoMatchingPoints)
            {
                //try and check various points
                string S = MatchingPointsText(imgLE, xI);
            }

            //For testing, do 2 rounds of stuff
            ImageAlign_Return IAR;
            if (crop5x||savingRegistration)
            {
                // set up the cropped image, run a variation of Image Align.
                double scaleFactor = (double)imgLE.Width_um / xI.Width_microns;

                // padding added to all sides
                var padding = xI.Width_microns / imgLE.umPerpixel;
                // Add padding to image
                var paddedIMG = AddPadding(imgLE.BMP, (int)padding);
                // since we added a padding, lets figure out what proportion of the image the hires's take up now.
                var relCropWidth = (padding) / paddedIMG.Width;
                // and let's just get the actual #pixels while here
                var pixelWidth_hiResInPaddedPixels = relCropWidth * paddedIMG.Width;
                bool defaultOffset = true;
                System.Drawing.Point xI_inLeica_um = new System.Drawing.Point((int)(xI.PlateX_um - (InCell_To_Leica.SR.DefaultInitOffset.X)), (int)(xI.PlateY_um - (InCell_To_Leica.SR.DefaultInitOffset.Y)));
                // Here's the xI coords converted with offset. This points to the bottom right of the image in imgLE
                if ((Math.Abs(InCell_To_Leica.SR.Offset_FieldRefined.X - InCell_To_Leica.SR.DefaultInitOffset.X) < 150) || (Math.Abs(InCell_To_Leica.SR.Offset_FieldRefined.Y - InCell_To_Leica.SR.DefaultInitOffset.Y) < 150))
                {
                    File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Field Offset is {InCell_To_Leica.SR.Offset_FieldRefined}, which is acceptable." + "\r\n");
                    xI_inLeica_um = new System.Drawing.Point((int)(xI.PlateX_um - (InCell_To_Leica.SR.Offset_FieldRefined.X)), (int)(xI.PlateY_um - (InCell_To_Leica.SR.Offset_FieldRefined.Y)));
                    defaultOffset = false;
                }
                else
                {
                    File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Field Offset UNNACCEPTABLE: {InCell_To_Leica.SR.Offset_FieldRefined}" + "\r\n");
                }

                // let's get to the top left of the hiRes image. add hiResWidth in padded img, since we're still in microns, x=0,y=0 is still down and to the left.
                var xI_topLeft = new System.Drawing.Point((int)(xI_inLeica_um.X + pixelWidth_hiResInPaddedPixels * imgLE.umPerpixel), (int)(xI_inLeica_um.Y + pixelWidth_hiResInPaddedPixels * imgLE.umPerpixel));

                // now let's subtract the padding!
                var xI_topLeft_um = new System.Drawing.Point((int)(xI_topLeft.X - padding * imgLE.umPerpixel), (int)(xI_topLeft.Y - padding * imgLE.umPerpixel));


                // Now, let's get a vector from top-left of padded to top-left of lowRes.
                var believedX_inPaddedPixels = (int)((imgLE.X_um - xI_topLeft_um.X) / imgLE.umPerpixel);
                var believedY_inPaddedPixels = (int)((imgLE.Y_um - xI_topLeft_um.Y) / imgLE.umPerpixel);
                var believedPoint = new System.Drawing.Point((int)(believedX_inPaddedPixels), (int)(believedY_inPaddedPixels));

                // Let's crop out ~ 20% more for now, just as a start.
                double extraCrop = crop5x ? 0.3 : 0.5;
                double extraWidth = extraCrop * pixelWidth_hiResInPaddedPixels;
                var extraSpacePoint = new System.Drawing.Point((int)(believedPoint.X - extraWidth), (int)(believedPoint.Y - extraWidth));
                var believedWidth = 2 * extraWidth + pixelWidth_hiResInPaddedPixels;
                var believedHeight = 2 * extraWidth + pixelWidth_hiResInPaddedPixels;
                var crop = new Rectangle((int)extraSpacePoint.X, (int)extraSpacePoint.Y, (int)believedWidth, (int)believedHeight);
                // After computing the `crop` rectangle, calculate its intersection with the padded image boundaries.
                Rectangle imgLEBounds = new Rectangle((int)padding, (int)padding, imgLE.Width, imgLE.Height);
                Rectangle intersection = Rectangle.Intersect(imgLEBounds, crop);

                // Calculate the area of the intersection.
                int intersectionArea = intersection.Width * intersection.Height;

                // Calculate the area of the crop.
                int cropArea = crop.Width * crop.Height;
                double micronAreaOfCrop = cropArea * imgLE.umPerpixel;

                // Define a threshold below which you will not proceed with registration.
                double threshold = 0.3; // e.g., proceed only if at least 30% of the crop is on screen.

                // Check if the intersection area is above the threshold.
                if ((double)intersectionArea / cropArea >= threshold)
                {
                    // Proceed with registration.
                    // Your existing registration code goes here.
                    var cropped5x = CropImage(paddedIMG, crop);
                    paddedIMG.Dispose();

                    IAR = CroppedImageAlign(imgLE, xI, icBrightness, cropped5x, extraSpacePoint, (int)padding, ref preSavedImages, ref notPreSavedImages, micronAreaOfCrop, wrap, ExportOverlay, AddCenterTick, ExportMatch, defaultOffset);
                    cropped5x.Dispose();
                }
                else
                {
                    File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Crop mostly offscreen" + "\r\n");
                    // Skip registration for this crop.
                    Debug.Write("Skipping registration for this crop as it is mostly off-screen.");
                    var cropIAR = new ImageAlign_Return(xI.WellLabel, this);
                    cropIAR.Note = "cropped out";
                    return cropIAR;
                }


            }
            else
            {
                IAR = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
            }
            if (IAR.RR != null)
            {
                if (true)
                {
                    if (IAR.RR.FinalR2 < 0.1)
                    {

                        var Error_Divisor_T = Error_Divisor;
                        Error_Divisor *= 1.3F;
                        var IAR1b = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
                        Error_Divisor = Error_Divisor_T;

                        if (IAR1b.RR != null)
                        {
                            if (IAR1b.RR.FinalR2 > IAR.RR.FinalR2) IAR = IAR1b;
                        }
                    }

                    if (IAR.RR.FinalR2 < 0.1) { IAR.Note = "Poor " + IAR.RR.FinalR2.ToString("0.00"); IAR.RR = null; return IAR; }
                }
            }

            bool SecondCheck = false;
            if (SecondCheck)
            {
                if (IAR.RR != null)
                {
                    //It should be very fast since it doesn't have much area to explore
                    // RefineBaseOnAlignment(this, IAR, 1000); //Fixes up the intial offset and turns the error spread way down 
                    ImageAlign_Return IAR2 = ImageAlign(imgLE, xI, icBrightness, ExportOverlay, AddCenterTick, ExportMatch);
                    this.Error_Divisor = 1;
                    float dist = ImageAlign_Return.Distance(IAR, IAR2);
                    IAR.Note += "/r/n Distance between two versions (5 is cut-off) = " + dist;
/*                    if (dist > 5)
                    {
                        //Questionable, so lets revert back to the starting
                        this.Initial_Leica_Offset_um = Default_InitialOffset_um;
                    }
                    else
                    {
                        //Did good, but lets be safe and go halfway there
                        this.Initial_Leica_Offset_um = Utilities.MathUtils.GetAverage(Default_InitialOffset_um, IAR.Offset);
                    }*/
                }
            }
            return IAR;
        }



        private string MatchingPointsText(ImgWrap imgLE, XDCE_Image imgIC)
        {
            System.Drawing.PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);
            MetaMorph_StagePoint SP = new MetaMorph_StagePoint(imgIC, imgLE.Width_um, this);
            StringBuilder sB = new StringBuilder();
            sB.Append("InCel Center GE_Corr\t" + imgIC.PlateXY_Center_um.X + "\t" + imgIC.PlateXY_Center_um.Y + "\r\n"); //These two should match pretty closely
            sB.Append("Leica Center GE_Pred\t" + LeicaInitialCenter_um.X + "\t" + LeicaInitialCenter_um.Y + "\r\n");

            sB.Append("Leica Center LE_Corr\t" + imgLE.Center_um.X + "\t" + imgLE.Center_um.Y + "\r\n"); //This should just be the initial offset away from above

            sB.Append("Leica GoTo   LE_Corr\t" + imgLE.X_um + "\t" + imgLE.Y_um + "\r\n"); //These two should match pretty closely
            sB.Append("InCel StgPos LE_Pred\t" + SP.XY.X + "\t" + SP.XY.Y + "\r\n");

            return sB.ToString();
        }

        private Dictionary<string, ImgWrap> CacheImWrap = new Dictionary<string, ImgWrap>();

        public ImageAlign_Return ImageAlign_Distant(ImgWrap imgLE, XDCE_Image imgINCELL, float icBrightness, System.Drawing.PointF FractionStart, SizeF FractionalSize)
        {
            bool ExportOverlay = true;
            //float resize_divisor_forRegistration = 1; //override the main one

            var IAR = new ImageAlign_Return(imgINCELL.WellLabel, this);
            RectangleF R_GEFract_Pix; Bitmap GEFract_BMP; string ExportFront;
            R_GEFract_Pix = new RectangleF(FractionStart.X * imgINCELL.Width_Pixels, FractionStart.Y * imgINCELL.Height_Pixels, FractionalSize.Width * imgINCELL.Width_Pixels, FractionalSize.Height * imgINCELL.Height_Pixels);
            System.Drawing.PointF CenterPixel_LE = new System.Drawing.PointF(300, 300); //Not actually used except to plot center pixel
            IAR.ExportName = imgINCELL.WellField + "=" + imgLE.Name;
            ExportFront = Path.Combine(ExportLoc, IAR.ExportName);
            GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE, "", false);
            if (GEFract_BMP == null) { IAR.Note = "Problem getting the InCell Image"; return IAR; }

            ImgWrap ImWrapGE = new ImgWrap(GEFract_BMP);
            IAR.ImgInternal_RedVariation = ImWrapGE.RedStDev;

            //Resize the Leica Image and package into ImgWraps
            ImgWrap imgLE_RS;
            if (!CacheImWrap.ContainsKey(imgLE.Name))
            {
                Bitmap LE_BMP = new Bitmap(imgLE.BMP, new System.Drawing.Size((int)(imgLE.Width / resize_divisor_forRegistration), (int)(imgLE.Width / resize_divisor_forRegistration)));
                imgLE_RS = new ImgWrap(LE_BMP);
                CacheImWrap.Add(imgLE.Name, imgLE_RS);
            }
            else { imgLE_RS = CacheImWrap[imgLE.Name]; }

            //Use cross-correlation to register the images together
            IAR.RR = CrossCorr.Register(ImWrapGE, imgLE_RS);

            IAR.FractCenter_GE_um = imgINCELL.PlateCoordinates_um_InCenterOfRectangle(R_GEFract_Pix); //This is on the pre-shrunk coordinates
            IAR.FractCenter_LE_um = new System.Drawing.PointF( //these are shrunk so have to expand, Leica Pixel binning already accounted for, don't do here
                    (float)(imgLE.X_um - (IAR.RR.Location.X + (ImWrapGE.Width / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration),
                    (float)(imgLE.Y_um - (IAR.RR.Location.Y + (ImWrapGE.Height / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration));

            //Export Overlay
            IAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + ".jpg";
            InCell_To_Leica.SR.OverlayPath = IAR.OverlayName;
            if (ExportOverlay) MasksHelper.ExportOverlay(IAR, IAR.OverlayName, ImWrapGE, imgLE_RS);
            GEFract_BMP.Dispose();

            return IAR;
        }

        public ImageAlign_Return ImageAlign(ImgWrap imgLE, XDCE_Image imgINCELL, float icBrightness, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false, ImgWrap wrap = null)
        {
            //Creat IAR then Add some more info for the saved XML file
            var IAR = new ImageAlign_Return(imgINCELL.WellLabel, this);
            //IAR.WellOffset = Default_InitialOffset_um; //Be nice to include this as well, the Well offset that is
            IAR.Offset_Before = Initial_Leica_Offset_um;
            IAR.Leica_StartingPosition = imgLE.Center_um;
            IAR.InCell_StartingPosition = imgINCELL.CenterPixel;
            var dstd = imgLE.RedStDev;
            //icBrightness = 4;
            resize_divisor_forRegistration = 2;

            //There is a slight bit of scale coming from somewhere, the GE Image is slightly smaller
            RectangleF R_GEFract_Pix; System.Drawing.PointF CenterPixel_LE; Bitmap GEFract_BMP; string ExportFront;

            //Where is the center of the Leica Image, when corrected to ge coordinates (in Plate um)
            System.Drawing.PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate_SR(imgLE);

            CenterPixel_LE = imgINCELL.Pixel_FromPlateCoordinates(LeicaInitialCenter_um);

            //We need to chop out a safe area from the GE image to do CrossCorr with the other Leica Image - This is the "GE Fract" Image
            //Error_Divisor = 1.2F;
            R_GEFract_Pix = InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile ?
                new RectangleF(0, 0, imgINCELL.Width_Pixels, imgINCELL.Height_Pixels) :
                GetInCellFractionalRectangle(imgINCELL, imgLE, CenterPixel_LE);

            //Now get the GE image, add any annotation, and prep it for comparison
            IAR.ExportName = imgINCELL.WellField + "=" + imgLE.Name;
            ExportFront = Path.Combine(ExportLoc, IAR.ExportName);
            //ExportMatch = true;
            ImgWrap ImWrapGE;
            if (wrap != null)
                ImWrapGE = new ImgWrap(wrap.BMP, (float)(imgINCELL.Width_microns / wrap.Width));
            else
            {
                //bool inExpected = InCell_To_Leica.SR.expectedImages.ContainsKey(imgINCELL, InCell_To_Leica.wvParams);
                bool inExpected = false;
                if (inExpected)
                {
                    GEFract_BMP = InCell_To_Leica.SR.expectedImages.Get_Bitmap(imgINCELL, InCell_To_Leica.wvParams);
                }
                else
                {
                    GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                }
                int i = 0;
                while (GEFract_BMP.PixelFormat == PixelFormat.DontCare)
                {
                    if (i > 1)
                    {
                        GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                        break;
                    }
                    if (inExpected)
                    {
                        GEFract_BMP = InCell_To_Leica.SR.expectedImages.Get_Bitmap(imgINCELL, InCell_To_Leica.wvParams);
                    }
                    else
                    {
                        GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                    }
                    i++;
                }
                if (GEFract_BMP.PixelFormat == PixelFormat.DontCare)
                {
                    //MessageBox.Show("dont care 4mat");
                    return IAR;
                }
                ImWrapGE = new ImgWrap(GEFract_BMP, (float)(resize_divisor_forRegistration * imgINCELL.Width_microns / GEFract_BMP.Width));
            }
            ImWrapGE.Name = imgINCELL.FileName;
            IAR.ImgInternal_RedVariation = ImWrapGE.RedStDev;
            //Resize the Leica Image and package into ImgWraps
            var LE_BMP = new Bitmap(imgLE.BMP, new System.Drawing.Size((int)(imgLE.Width / resize_divisor_forRegistration), (int)(imgLE.Width / resize_divisor_forRegistration)));
            // Manny changed brightness
            //LE_BMP = Utilities.ColorMatrix.AdjustBrightness_Old(LE_BMP, icBrightness / (float)(imgLE.Width_um / imgINCELL.Width_microns));
            var imgLE_RS = new ImgWrap(LE_BMP, (float)(imgLE.umPerpixel * resize_divisor_forRegistration));
            IAR.ImgExternal_RedVariation = imgLE_RS.RedStDev;

            //Check quality of starting image
            if (ImWrapGE.RedStDev < Reg_Params.ImageMinStDev || imgLE_RS.RedStDev < Reg_Params.ImageMinStDev)
            {
                //MessageBox.Show("low stdev");
                IAR.Note = "Not enough image variation for XCorr. GE=" + ImWrapGE.RedStDev.ToString("0.0") + " LE=" + imgLE_RS.RedStDev.ToString("0.0"); return IAR;
            }
            //Use cross-correlation to register the images together

            IAR.RR = CrossCorr.Register(ImWrapGE, imgLE_RS);
            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"crossCorr with {imgINCELL.FOV} scored was {IAR.RR.FinalR2}" + "\r\n");

            //Record the before and after points
            //Need the center of the GE Fract in GE Coordinates, then the one in LE coordinates
            IAR.FractCenter_GE_um = imgINCELL.PlateCoordinates_um_InCenterOfRectangle(R_GEFract_Pix); //This is on the pre-shrunk coordinates
            IAR.FractCenter_LE_um = new System.Drawing.PointF( //these are shrunk so have to expand, Leica Pixel binning already accounted for, don't do here
                    (float)(imgLE.X_um - (IAR.RR.Location.X + (ImWrapGE.Width / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration),
                    (float)(imgLE.Y_um - (IAR.RR.Location.Y + (ImWrapGE.Height / 2)) * imgLE.umPerpixel * resize_divisor_forRegistration));


            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x20_Standard)
            {


                IAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + ".jpg";
                //newIAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + "NEWFUNCTION" + ".jpg";

                //InCell_To_Leica.SR.OverlayPath = IAR.OverlayName;

                if (ExportOverlay) MasksHelper.ExportOverlay(IAR, IAR.OverlayName, ImWrapGE, imgLE_RS);
                //MasksHelper.ExportOverlay(newIAR, newIAR.OverlayName, ImWrapGE, imgLE_RS);
                //MasksHelper.ExportShiftedMask(imgINCELL, IAR, ImWrapGE.BMP.Size, imgLE_RS.BMP.Size /*, ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets*/);



                IAR.Offset_After = IAR.Offset; //Add some more info for the saved XML file
                InCell_To_Leica.SR.IAR = IAR; //InCell_To_Leica.AddSR(); //Save this in the SR Queue
                //IAR.ExportQC(ExportFront + ".xml"); //Export QC
            }

            //Clean Up the temporary bitmaps
            imgLE_RS.BMP.Dispose();

            return IAR;
        }


        public ImageAlign_Return CroppedImageAlign(ImgWrap imgLE, XDCE_Image imgINCELL, float icBrightness, Bitmap croppedLE, System.Drawing.Point believedPoint, int padding, ref int preSavedImages, ref int notPreSavedImages, double croppedArea_um = double.NaN, ImgWrap wrap = null, bool ExportOverlay = false, bool AddCenterTick = false, bool ExportMatch = false, bool defaultOffset = false)
        {
            //Creat IAR then Add some more info for the saved XML file
            var IAR = new ImageAlign_Return(imgINCELL.WellLabel, this);
            //IAR.WellOffset = Default_InitialOffset_um; //Be nice to include this as well, the Well offset that is
            IAR.Offset_Before = Initial_Leica_Offset_um;
            IAR.Leica_StartingPosition = imgLE.Center_um;
            IAR.InCell_StartingPosition = imgINCELL.CenterPixel;

            //icBrightness = 4;
            resize_divisor_forRegistration = 2;

            //There is a slight bit of scale coming from somewhere, the GE Image is slightly smaller
            RectangleF R_GEFract_Pix; System.Drawing.PointF CenterPixel_LE; Bitmap GEFract_BMP; string ExportFront;

            //Where is the center of the Leica Image, when corrected to ge coordinates (in Plate um)
            System.Drawing.PointF LeicaInitialCenter_um = LeicaImageCenter_Estimate(imgLE);
            if (defaultOffset)
                LeicaInitialCenter_um = LeicaImageCenter_Estimate_Default(imgLE);

            //Use the GE Field's Plate coordinates to find which pixel that center point is at
            CenterPixel_LE = imgINCELL.Pixel_FromPlateCoordinates(LeicaInitialCenter_um);

            //We need to chop out a safe area from the GE image to do CrossCorr with the other Leica Image - This is the "GE Fract" Image
            //Error_Divisor = 1.2F;
            R_GEFract_Pix = InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile ?
                new RectangleF(0, 0, imgINCELL.Width_Pixels, imgINCELL.Height_Pixels) :
                GetInCellFractionalRectangle(imgINCELL, imgLE, CenterPixel_LE);

            //Now get the GE image, add any annotation, and prep it for comparison
            IAR.ExportName = imgINCELL.WellField + "=" + imgLE.Name;
            ExportFront = Path.Combine(ExportLoc, IAR.ExportName);
            //ExportMatch = true;
            //GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE, ExportMatch ? ExportFront : "", AddCenterTick);
            // if (GEFract_BMP == null) { IAR.Note = "Problem getting the InCell Image"; return IAR; }
            //var ImWrapGE = new ImgWrap(GEFract_BMP);
            ImgWrap ImWrapGE;
            if (wrap != null)
                ImWrapGE = new ImgWrap(wrap.BMP, (float)(imgINCELL.Width_microns / wrap.Width));
            else
            {
                //bool inExpected = InCell_To_Leica.SR.expectedImages.ContainsKey(imgINCELL, InCell_To_Leica.wvParams);
                bool inExpected = false;
                if (inExpected)
                {
                    GEFract_BMP = InCell_To_Leica.SR.expectedImages.Get_Bitmap(imgINCELL, InCell_To_Leica.wvParams);
                    preSavedImages++;
                }
                else
                {
                    GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                    notPreSavedImages++;
                }
                int i = 0;
                while (GEFract_BMP.PixelFormat == PixelFormat.DontCare)
                {
                    if (i > 1)
                    {
                        GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                        notPreSavedImages++;
                        break;
                    }
                    if (inExpected)
                    {
                        GEFract_BMP = InCell_To_Leica.SR.expectedImages.Get_Bitmap(imgINCELL, InCell_To_Leica.wvParams);
                        notPreSavedImages++;
                    }
                    else
                    {
                        GEFract_BMP = GetInCellBMP(imgINCELL, imgLE.umPerpixel, icBrightness, R_GEFract_Pix, CenterPixel_LE);
                        notPreSavedImages++;
                    }
                    i++;
                }
                if (GEFract_BMP.PixelFormat == PixelFormat.DontCare)
                {
                    return IAR;
                }
                ImWrapGE = new ImgWrap(GEFract_BMP, (float)(imgINCELL.Width_microns / GEFract_BMP.Width));
            }
            //wrap.BMP.Save("C:\\Users\\blabPrime\\Desktop\\Manny\\cwrap.bmp");

            ImWrapGE.Name = imgINCELL.FileName;
            IAR.ImgInternal_RedVariation = ImWrapGE.RedStDev;
            //Resize the Leica Image and package into ImgWraps
            var CroppedLE_BMP = new Bitmap(croppedLE, new System.Drawing.Size((int)(croppedLE.Width / resize_divisor_forRegistration), (int)(croppedLE.Height / resize_divisor_forRegistration)));
            var CroppedimgLE_RS = new ImgWrap(CroppedLE_BMP, (float)(croppedArea_um / (resize_divisor_forRegistration * CroppedLE_BMP.Height * CroppedLE_BMP.Width)));

            IAR.ImgExternal_RedVariation = CroppedimgLE_RS.RedStDev;

            //Check quality of starting image
            if (ImWrapGE.RedStDev < Reg_Params.ImageMinStDev || CroppedimgLE_RS.RedStDev < Reg_Params.ImageMinStDev)
            {
                IAR.Note = "Not enough image variation for XCorr. GE=" + ImWrapGE.RedStDev.ToString("0.0") + " LE=" + CroppedimgLE_RS.RedStDev.ToString("0.0"); return IAR;
            }

            //Use cross-correlation to register the images together
            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"CrossCorr with {imgINCELL.FOV}" + "\r\n");
            IAR.RR = CrossCorr.Register(ImWrapGE, CroppedimgLE_RS);
            File.AppendAllText(InCell_To_Leica.SR.FullPath_Log, DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff") + "\t" + $"Score was {IAR.RR.FinalR2}" + "\r\n");

            //Record the before and after points
            //Need the center of the GE Fract in GE Coordinates, then the one in LE coordinates
            IAR.FractCenter_GE_um = imgINCELL.PlateCoordinates_um_InCenterOfRectangle(R_GEFract_Pix); //This is on the pre-shrunk coordinates

            IAR.FractCenter_LE_um = new System.Drawing.PointF( //these are shrunk so have to expand, Leica Pixel binning already accounted for, don't do here
                    (float)(imgLE.X_um - (-padding + believedPoint.X + (IAR.RR.Location.X + (ImWrapGE.Width / 2)) * resize_divisor_forRegistration) * imgLE.umPerpixel),
                    (float)(imgLE.Y_um - (-padding + believedPoint.Y + (IAR.RR.Location.Y + (ImWrapGE.Height / 2)) * resize_divisor_forRegistration) * imgLE.umPerpixel));

            //Export Overlay
            var pointToDraw = new PointF((float)(IAR.RR.Location.X + (believedPoint.X - padding) / resize_divisor_forRegistration), (float)(IAR.RR.Location.Y + (believedPoint.Y - padding) / resize_divisor_forRegistration));


            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x20_Standard)
            {
                var LE_BMP = new Bitmap(imgLE.BMP, new System.Drawing.Size((int)(imgLE.Width / resize_divisor_forRegistration), (int)(imgLE.Width / resize_divisor_forRegistration)));
                var imgLE_RS = new ImgWrap(LE_BMP);

                IAR.OverlayName = ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets + ".jpg";

                InCell_To_Leica.SR.OverlayPath = IAR.OverlayName;
                if (ExportOverlay) FIVE_IMG.MasksHelper.CroppedExportOverlay(IAR, IAR.OverlayName, ImWrapGE, imgLE_RS, pointToDraw);
                MasksHelper.ExportShiftedMask(imgINCELL, IAR, ImWrapGE.BMP.Size, CroppedimgLE_RS.BMP.Size /*, ExportFront + "_E" + this.Error_Divisor + "_OV_" + IAR.Offsets*/);



                IAR.Offset_After = IAR.Offset; //Add some more info for the saved XML file
                InCell_To_Leica.SR.IAR = IAR; InCell_To_Leica.AddSR(); //Save this in the SR Queue
                IAR.ExportQC(ExportFront + ".xml"); //Export QC
                LE_BMP.Dispose();
            }

            //Clean Up the temporary bitmaps
            //GEFract_BMP.Dispose();
            CroppedLE_BMP.Dispose();
            return IAR;
        }


        public static void RefineBaseOnAlignment(Associate_InCell_Leica AIL, ImageAlign_Return IAR, float ErrorDivisor = -1)
        {
            AIL.Initial_Leica_Offset_um.X = IAR.FractCenter_GE_um.X - IAR.FractCenter_LE_um.X;
            AIL.Initial_Leica_Offset_um.Y = IAR.FractCenter_GE_um.Y - IAR.FractCenter_LE_um.Y;
            if (ErrorDivisor > 0) AIL.Error_Divisor = ErrorDivisor;
        }

        public int FractRect_XErr => Math.Max(1, (int)(X_Error_Pixels / Error_Divisor));
        public int FractRect_YErr => Math.Max(1, (int)(Y_Error_Pixels / Error_Divisor));

        private RectangleF GetInCellFractionalRectangle(XDCE_Image xI, ImgWrap imgLE, System.Drawing.PointF CenterPixel_LE)
        {
            //We need to chop out a safe area from the GE image to do CrossCorr with the other Leica Image - This is the "GE Fract" Image
            //The larger the area, the smaller this area can be since we don't want to go outside of either image
            float LE_Rel = (imgLE.umPerpixel / (float)xI.Parent.PixelWidth_in_um);
            RectangleF R_GEFract_Pix = new RectangleF(CenterPixel_LE.X - (LE_Rel * imgLE.Width / 2) + FractRect_XErr,
                                                      CenterPixel_LE.Y - (LE_Rel * imgLE.Height / 2) + FractRect_YErr,
                                                      CenterPixel_LE.X + (LE_Rel * imgLE.Width / 2) - FractRect_XErr,
                                                      CenterPixel_LE.Y + (LE_Rel * imgLE.Height / 2) - FractRect_YErr);
            R_GEFract_Pix.X = Math.Max(0, R_GEFract_Pix.X);
            R_GEFract_Pix.Y = Math.Max(0, R_GEFract_Pix.Y);
            R_GEFract_Pix.Width = Math.Min(xI.Width_Pixels - 0.1f, R_GEFract_Pix.Width);
            R_GEFract_Pix.Height = Math.Min(xI.Height_Pixels - 0.1f, R_GEFract_Pix.Height);

            //I am making the rect as x1,y1,x2,y2, which is wrong, so I have to fix the width and height
            R_GEFract_Pix.Width -= R_GEFract_Pix.Left; R_GEFract_Pix.Height -= R_GEFract_Pix.Top;
            return R_GEFract_Pix;
        }

        public Bitmap GetInCellBMP02(XDCE_Image xI, float LE_umPerPixel, ushort black, ushort white, RectangleF R_GEFract_Pix, System.Drawing.PointF CenterPixel_LE, string ExportFront = "")
        {
            //Now get the GE image, add any annotation, and prep it for comparison
            Bitmap GEFract_BMP;

            //We don't know ahead of time the region of this to grab, but we do know the brightness and the binning, so those to things should be done first and stored in memory
            //Bitmap BMP = Utilities.DrawingUtils.GetImageSt(xI);
            var BMP = Utilities.DrawingUtils.GetAdjustedImageSt(xI, black, white);
            GEFract_BMP = new Bitmap(BMP, new System.Drawing.Size((int)(BMP.Width / resize_divisor_forRegistration), (int)(BMP.Height / resize_divisor_forRegistration)));
            GEFract_BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);

            //Last thing to do is just crop out the right area
            float Leica_Pixel_Binning = LE_umPerPixel / (float)xI.Parent.PixelWidth_in_um;
            GEFract_BMP = BMP.Clone(R_GEFract_Pix, BMP.PixelFormat);
            if (ExportFront != "") GEFract_BMP.Save(ExportFront + "_GF.BMP");

            return GEFract_BMP;
        }

        public Bitmap GetInCellBMP(XDCE_Image xI, float LE_umPerPixel, float Brightness, RectangleF R_GEFract_Pix, System.Drawing.PointF CenterPixel_LE, string ExportFront = "", bool AddCenterTick = false)
        {
            //Now get the GE image, add any annotation, and prep it for comparison
            Bitmap GEFract_BMP = null;
            Bitmap BMP = Utilities.DrawingUtils.GetAdjustedImageSt(xI, Brightness);
            if (ExportFront != "") BMP.Save(ExportFront + "_GO.BMP"); //Original
            if (AddCenterTick) AddCenterTickToBMP(CenterPixel_LE, BMP);
            float Leica_Pixel_Binning = LE_umPerPixel / (float)xI.Parent.PixelWidth_in_um;
            //int a = 600, b = 500; GEFract_BMP = BMP.Clone(new Rectangle(2040-a,0,a,b), BMP.PixelFormat); //5/22/2023 testing

            try
            {
                GEFract_BMP = BMP.Clone(R_GEFract_Pix, BMP.PixelFormat);
            }
            catch { return null; }
            var Sz = new System.Drawing.Size(
                (int)(GE_LeicaSizeRatio_ForImage.Width * GEFract_BMP.Width / (resize_divisor_forRegistration * Leica_Pixel_Binning)),
                (int)(GE_LeicaSizeRatio_ForImage.Height * GEFract_BMP.Height / (resize_divisor_forRegistration * Leica_Pixel_Binning)));
            GEFract_BMP = new Bitmap(GEFract_BMP, Sz);
            //GEFract_BMP = new Bitmap(GEFract_BMP, new Size(  //This is what we had before 5/12/2022
            //    (int)(GEFract_BMP.Width / (resize_divisor_forRegistration * Leica_Pixel_Binning)),
            //    (int)(GEFract_BMP.Height / (resize_divisor_forRegistration * Leica_Pixel_Binning))));
            GEFract_BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);
            if (ExportFront != "") GEFract_BMP.Save(ExportFront + "_GF.BMP"); //Fractional
            BMP.Dispose();
            return GEFract_BMP;
        }

        private static void AddCenterTickToBMP(System.Drawing.PointF CenterPixel_LE, Bitmap BMP)
        {
            using (Graphics g = Graphics.FromImage(BMP))
            {
                g.FillRectangle(Utilities.DrawingUtils.RedBrush, CenterPixel_LE.X, CenterPixel_LE.Y, 22, 6);
                g.FillRectangle(Utilities.DrawingUtils.RedBrush, CenterPixel_LE.X, CenterPixel_LE.Y, 6, 22);
            }
        }

        /// <summary>
        /// Tried to estimate the correct position to take a picture that would match a FOV in the GE
        /// </summary>
        /// <param name="FOV"></param>
        /// <returns></returns>
        public System.Drawing.PointF  LeicaPositionEstimate_FromGEXDCE(XDCE_Image FOV, float LeicaImageWidth_um, System.Drawing.PointF SpecifyOffset, SizeF SpecifyStretch)
        {
            float HalfWidth = LeicaImageWidth_um / 2;
            float X_Offset = SpecifyOffset.IsEmpty ? Initial_Leica_Offset_um.X : SpecifyOffset.X;
            float Y_Offset = SpecifyOffset.IsEmpty ? Initial_Leica_Offset_um.Y : SpecifyOffset.Y;
            float X_Stretch = SpecifyStretch.IsEmpty ? GE_LeicaSizeRatio_ForMoving.Width : SpecifyStretch.Width;
            float Y_Stretch = SpecifyStretch.IsEmpty ? GE_LeicaSizeRatio_ForMoving.Height : SpecifyStretch.Height;
            System.Drawing.PointF NewCenter = new PointF((FOV.PlateXY_Center_um.X - X_Offset) * X_Stretch, (FOV.PlateXY_Center_um.Y - Y_Offset) * Y_Stretch);
            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                // TODO: Generalize. at 812 FOVs, need to subtract by - (float)FOV.Width_microns/8, aka half of a subimage. 
                PointF LeicaCoords = new PointF(NewCenter.X + HalfWidth, NewCenter.Y + HalfWidth);
                return LeicaCoords;
            }
            else
                return new PointF(NewCenter.X + HalfWidth, NewCenter.Y + HalfWidth);
        }

        /// <summary>
        /// Returns the estimated GE coordinates for the center of the given Leica Image
        /// </summary>
        public PointF LeicaImageCenter_Estimate(ImgWrap imgLE)
        {
            //New as of 4/26, realized that the center based on using the microscope really is minus minus
            return new PointF((imgLE.X_umCenter + Initial_Leica_Offset_um.X),
                              (imgLE.Y_umCenter + Initial_Leica_Offset_um.Y));
        }

        public PointF LeicaImageCenter_Estimate_SR(ImgWrap imgLE)
        {
            //New as of 4/26, realized that the center based on using the microscope really is minus minus
            return new PointF((imgLE.X_umCenter + InCell_To_Leica.SR.Offset_WellRefined.X),
                              (imgLE.Y_umCenter + InCell_To_Leica.SR.Offset_WellRefined.Y));
        }
        public PointF LeicaImageCenter_Estimate_Default(ImgWrap imgLE)
        {
            //New as of 4/26, realized that the center based on using the microscope really is minus minus
            return new PointF((imgLE.X_umCenter + InCell_To_Leica.SR.DefaultInitOffset.X),
                              (imgLE.Y_umCenter + InCell_To_Leica.SR.DefaultInitOffset.Y));
        }
        internal Associate_InCell_Leica Copy()
        {
            var Dest = new Associate_InCell_Leica(this.Reg_Params);
            Dest.Error_Divisor = this.Error_Divisor;
            Dest.GE_LeicaSizeRatio_ForImage = this.GE_LeicaSizeRatio_ForImage;
            Dest.resize_divisor_forRegistration = this.resize_divisor_forRegistration;
            Dest.X_Error_Pixels = this.X_Error_Pixels;
            Dest.Y_Error_Pixels = this.Y_Error_Pixels;
            return Dest;
        }
    }

    public enum MultiObjEnum
    {
        x20_x20_Standard = 0,
        x20_x5_PreTile = 1, //Treat it like usual
        x20_x5_LiveTile = 2,
    }

    public class SharedResource
    {
        public SharedResource(INCELL_Folder IC_Folder, string WellList, int firstFieldStart, int wellFieldChecks, short waveLength, float brightness, string BasePath, SVP_Registration_Params RegParams, string SessionID = "", bool ClearFolder = true, INCELL_Folder IC_Folder_5x = null, bool CleanOverlays = true)

        {
            CancelRequested = false;
            IsThereNextWell = true;
            Reg_Params = RegParams;
            DefaultInitOffset = Reg_Params.InitialDefaultOffset;

            Wavelength = waveLength;
            Brightness = brightness;
            PlateID = IC_Folder.PlateID_NoScanIndex;
            FirstFieldIdx_Start = firstFieldStart;
            WellFieldChecks = wellFieldChecks;
            this.SessionID = SessionID == "" ? DateTime.Now.ToString(Plate_Session_Class.DateFormat) + "-C" : SessionID;
            automateRegistration = InCell_To_Leica.fullAuto;
            Path_Root = BasePath; //This has functionality to create folders
            if (IC_Folder_5x != null)
            {
                maxFOVsaved = -1;
                MultiObjectiveStyle = MultiObjEnum.x20_x5_LiveTile;
                LeicaImageWidth_um = 2652;
            }

            _SavedWellList = WellList.Split(',').ToList();
            for (int i = 0; i < _SavedWellList.Count; i++) _SavedWellList[i] = _SavedWellList[i].ToUpper().Trim();
            this.WellList = WellList;
            _SavedICFolder20x = IC_Folder;
            _SavedICFolder5x = IC_Folder_5x;
            _NextSavedWellIdx = 0;
            _SavedAIL = new Associate_InCell_Leica(Reg_Params) { ExportLoc = Path_Overlays };
            CurrentWell = SavedWellList[_NextSavedWellIdx];
            pixelBinning = 3.9F; // TODO Unable to locate where this value is being set in the .jnl, so I'm manually setting it here.
            preSavedImages = 0; notPreSavedImages = 0;
            maxRetry = 2; // # of times we'll retry registration for a well in FullAuto mode
            if (ClearFolder)
            {
                //Clear out the Well and Overlay folders
                DirectoryInfo DI;
                DI = new DirectoryInfo(this.Path_Overlays); foreach (FileInfo FI in DI.GetFiles()) FI.Delete();
                DI = new DirectoryInfo(this.Path_WellImages); foreach (FileInfo FI in DI.GetFiles()) FI.Delete();
            }
        }


        //Not used, this was a possible way to talk
        public void AO()
        {
            //Memory Mapped Files
            //https://docs.microsoft.com/en-us/dotnet/standard/io/memory-mapped-files
            using (MemoryMappedFile mmf = MemoryMappedFile.CreateNew("testmap", 10000))
            {
                bool mutexCreated;
                Mutex mutex = new Mutex(true, "testmapmutex", out mutexCreated);
                using (MemoryMappedViewStream stream = mmf.CreateViewStream())
                {
                    BinaryWriter writer = new BinaryWriter(stream);
                    writer.Write(1);
                }
                mutex.ReleaseMutex();

                Console.WriteLine("Start Process B and press ENTER to continue.");
                Console.ReadLine();

                Console.WriteLine("Start Process C and press ENTER to continue.");
                Console.ReadLine();

                mutex.WaitOne();
                using (MemoryMappedViewStream stream = mmf.CreateViewStream())
                {
                    BinaryReader reader = new BinaryReader(stream);
                    Console.WriteLine("Process A says: {0}", reader.ReadBoolean());
                    Console.WriteLine("Process B says: {0}", reader.ReadBoolean());
                    Console.WriteLine("Process C says: {0}", reader.ReadBoolean());
                }
                mutex.ReleaseMutex();
            }
        }

        public SharedResource Copy()
        {
            SharedResource CopyFrom = this;
            SharedResource Dest = new SharedResource(CopyFrom.SavedICFolder20x, CopyFrom.WellList, CopyFrom.FirstFieldIdx_Start, CopyFrom.WellFieldChecks, CopyFrom.Wavelength, CopyFrom.Brightness, CopyFrom.Path_Root, CopyFrom.Reg_Params, CopyFrom.SessionID, false);
            Dest.AIL = CopyFrom.AIL.Copy();
            Dest.IAR = CopyFrom.IAR;
            Dest.CurrentFOV = CopyFrom.CurrentFOV;
            Dest.CurrentWell = CopyFrom.CurrentWell;
            Dest.OverlayPath = CopyFrom.OverlayPath;
            Dest.x5OverlayPath = CopyFrom.x5OverlayPath;
            Dest.MultiObjectiveStyle = CopyFrom.MultiObjectiveStyle;
            Dest.avgOffset = CopyFrom.avgOffset;
            return Dest;
        }

        public SVP_Registration_Params Reg_Params;
        public MultiObjEnum MultiObjectiveStyle { get; set; } = MultiObjEnum.x20_x20_Standard;
        public short Wavelength { get; set; }
        public int maxFOVsaved { get; set; }
        public float pixelBinning { get; set; }
        public float Brightness { get; set; }
        public PointF avgOffset { get; set; }
        public string WellList { get; set; }
        public int maxRetry { get; set; }
        public bool skipWell { get; set; }
        public LeicaImageQueue expectedImages { get; set; }
        public bool automateRegistration { get; set; }
        public string OverlayPath { get; set; }
        public int preSavedImages { get; set; }
        public int notPreSavedImages { get; set; }
        public string x5OverlayPath { get; set; }

        private INCELL_Folder _SavedICFolder20x;
        public INCELL_Folder SavedICFolder20x { get => _SavedICFolder20x; }

        private INCELL_Folder _SavedICFolder5x;
        public INCELL_Folder SavedICFolder5x { get => _SavedICFolder5x; }

        private List<string> _SavedWellList;
        public List<string> SavedWellList { get => _SavedWellList; }

        public XDCE_ImageGroup CurrentWellXDCE_5x
        {
            get
            {
                return SavedICFolder5x.XDCE.Wells[CurrentWell];
            }
        }
        public string CurrentWell { get; internal set; }
        public XDCE_ImageGroup CurrentWellXDCE
        {
            get
            {
                return SavedICFolder20x.XDCE.Wells[CurrentWell];
            }
        }
        public PointF DefaultInitOffset { get; set; }

        private int _NextSavedWellIdx;
        public void IncrementWell()
        {
            do
            {
                if (_NextSavedWellIdx != 0)
                    WriteOutRegSuccess();
                CurrentWell = SavedWellList[_NextSavedWellIdx++];
                if (_NextSavedWellIdx >= SavedWellList.Count)
                {
                    IsThereNextWell = false;
                    _NextSavedWellIdx--; //Just to not break anything
                    break;
                }
            } while (!CurrentWellXDCE.HasMasks);
            if (CurrentWell == SavedWellList[0]) /*I.E. the first well of the set*/
                _FOVIdx = Math.Max(-1, FirstFieldIdx_Start - 2); //-1 since it is 1 to 0 indexed , -1 again since we will increment it to start
            else
                _FOVIdx = -1;
        }
        public void WriteOutRegSuccess()
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            // Define the folder name and the path to the folder
            string folderName = "Registration_Info";
            string folderPath = Path.Combine(desktopPath, folderName);

            // Define the file name and the path to the file
            string fileName = $"Reg_{PlateID}.txt";
            string filePath = Path.Combine(folderPath, fileName);

            // Text to write into the file
            string textToWrite = $"{CurrentWell}: {Math.Round((double)InCell_To_Leica.successfulFOVcount / ((double)FOVCount / (double)CurrentWellXDCE.Wavelength_Count) * (double)100)}% of registrations succeeded.\r\n";
            textToWrite += $"Within the images, {InCell_To_Leica.successfulAlignmentCount} alignments succeeded and {InCell_To_Leica.failedAlignmentCount} alignments failed.\r\n";
            FileHandler.SafelyWrite(filePath, textToWrite);

            File.AppendAllText(Path_RegInfo, $"{CurrentWell}: {InCell_To_Leica.successfulFOVcount} successful, {InCell_To_Leica.failedFOVcount} failed out of {(FOVCount / CurrentWellXDCE.Wavelength_Count) - 1} \r\n");
            InCell_To_Leica.successfulFOVcount = 0;
            InCell_To_Leica.failedFOVcount = 0;
            InCell_To_Leica.successfulAlignmentCount = 0;
            InCell_To_Leica.failedAlignmentCount = 0;
        }

        private int _FOVIdx = 0;
        public int FOVIdx { get => _FOVIdx; }

        public int FOVCount
        {
            get => MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile ? CurrentWellXDCE_5x.Images.Count : CurrentWellXDCE.MaskFields.Count;
        }
        public void IncrementFOV()
        {
            PreviousFOV = CurrentFOV;
            _FOVIdx++;
            if (_SavedNextFOV != null)
            {
                _SavedThisFOV = _SavedNextFOV;
            }
            else
            {
                XDCE_Image tXI;
                if (MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
                {
                    tXI = CurrentWellXDCE_5x.GetFields(FOVIdx)[0];
                }
                else
                    tXI = CurrentWellXDCE.MaskFields[FOVIdx];
                // in LiveTile, we don't have a mask generated yet. The path to the mask is meant to be used without moving Leica Motors.
                // We know what the name of the mask file should be ahead of time, since we have Offset_WellRefined. 
                // just take the 5x recon's X&Y, add the updated offsets, put that into the ini, then tell Leica to move to those coords. 
                CurrentMaskFullPath = tXI.MaskFullPath;
                CurrentShiftMaskFullPath = tXI.MaskShiftedFullPath;
                if (MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
                    _SavedThisFOV = CurrentWellXDCE_5x.GetField(tXI.FOV, this.Wavelength);
                else
                    _SavedThisFOV = CurrentWellXDCE.GetField(tXI.FOV, this.Wavelength);

            }
            _SavedNextFOV = null;
            if (MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
                IsThereNextField = (_FOVIdx + 1) < CurrentWellXDCE_5x.FOVCount;
            else
                IsThereNextField = (_FOVIdx + 1) < CurrentWellXDCE.MaskFields.Count;
        }

        internal void SetTesting(string Well, int FOV_Idx)
        {
            CurrentWell = Well;
            _FOVIdx = FOV_Idx;
        }

        internal void SetCurrentPos(XDCE_Image FOV)
        {
            // TODO: This needs to change if we try a different image size
            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
                LeicaImageWidth_um = 2652;
            CurrentFOVPos = AIL.
                LeicaPositionEstimate_FromGEXDCE(FOV, LeicaImageWidth_um, PointF.Empty, SizeF.Empty);
        }

        /// <summary>
        /// Mixes the Well Based Offset with the Field Refined Offset. If the Mix == 0, then everything is Well based, if the Mix == 1 then everything is the last Field
        /// </summary>
        /// <returns></returns>
        internal PointF GetNextOffset_Mix()
        {
            float x1 = Offset_FieldRefined.X;
            float y1 = Offset_FieldRefined.Y;

            if (x1 == 0 && y1 == 0) return Offset_WellRefined; //first time and there is no Field Refined

            float x2 = Offset_WellRefined.X;
            float y2 = Offset_WellRefined.Y;

            float Mix = Reg_Params.LastFOVOffset_over_WellOffset_MixRatio;

            return new PointF(x1 * Mix + x2 * (1 - Mix), y1 * Mix + y2 * (1 - Mix));
        }

        internal (bool, System.Drawing.Point) IsOffsetWithinMaxBounds(PointF tempOffset)
        {
            //Find the difference from Well
            float dX = Math.Abs(tempOffset.X - Offset_WellRefined.X);
            float dY = Math.Abs(tempOffset.Y - Offset_WellRefined.Y);

            return ((dX < Reg_Params.FOVOffset_MaxDeviationFromWell.X) && (dY < Reg_Params.FOVOffset_MaxDeviationFromWell.Y), new System.Drawing.Point((int)dX, (int)dY));
        }

        public XDCE_Image PreviousFOV { get; internal set; }
        private XDCE_Image _SavedNextFOV;
        private XDCE_Image _SavedThisFOV;

        public XDCE_Image CurrentFOV
        {
            get
            {
                //XDCE_ImageGroup Well = CurrentWellXDCE;
                //FOV = Well.GetField(FOVIdx + Well.FOV_Min, 0); //Usually the first field is the one the mask is made from . . but this could be an issue
                //return CurrentWellXDCE.MaskFields[FOVIdx]; //Changed 5/17/2021
                return _SavedThisFOV;
            }
            set { _SavedThisFOV = value; }
        }

        public bool FOVsDone { get => false; }

        public string PlateID { get; set; }
        public string SessionID { get; set; }

        private Associate_InCell_Leica _SavedAIL;
        public Associate_InCell_Leica AIL
        {
            get
            {
                return _SavedAIL;
            }
            internal set { _SavedAIL = value; }
        }

        private string _Path_Root;
        public string Path_Root
        {
            get => _Path_Root; set
            {
                DirectoryInfo DI = new DirectoryInfo(value); if (!DI.Exists) DI.Create();
                _Path_Root = value;
                //Now we can update the rest of the folders
                DI = new DirectoryInfo(Path_WellImages); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_FieldImage); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_Overlays); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_INI_Folder); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_PhotoActivatedImages); if (!DI.Exists) DI.Create();
                DI = new DirectoryInfo(Path_5xMasks); if (!DI.Exists) DI.Create();
            }
        }

        public string Path_WellImages { get => Path.Combine(Path_Root, PlateID, SessionID, "Well"); }
        public string Path_FieldImage { get => Path.Combine(Path_Root, PlateID, SessionID, "FOV"); }
        public string Path_PhotoActivatedImages { get => Path.Combine(Path_Root, PlateID, SessionID, "PhotoActivated Images"); }
        public string Path_INI_Folder { get => Path.Combine(Path_Root, "INI"); }
        public string Path_barcode { get => Path.Combine(Path_Root, "barcode.txt"); }
        public string Path_Overlays { get => Path.Combine(Path_Root, PlateID, SessionID, "Overlays"); }
        public string Path_RegInfo { get => Path.Combine(Path_Root, PlateID, SessionID, "RegInfo.txt"); }
        public string Path_TimeKeeping { get => Path.Combine(Path_INI_Folder, "TimeKeeping.txt"); }
        public string Path_5xMasks { get => Path.Combine(Path_Root, PlateID, SessionID, "x5Masks"); }
        public string Path_fullAuto { get => Path.Combine(Path_Root, "BAB_comms.txt"); }
        public string FullPath_Wellini { get => Path.Combine(Path_INI_Folder, "Wells.ini"); }
        public string FullPath_RegIni { get => Path.Combine(Path_INI_Folder, "Register.ini"); }
        public string FullPath_FOVini { get => Path.Combine(Path_INI_Folder, "Fields.ini"); }
        public string FullPath_Maskini { get => Path.Combine(Path_INI_Folder, "Masks.ini"); }
        public string FullPath_Log { get => Path.Combine(Path_INI_Folder, "Log.txt"); }
        public string Objective { get => "20x"; }
        public PointF CurrentFOVPos { get; internal set; }
        public string Shift { get; set; }
        public short BinningField { get => 4; }
        public short BinningWell { get => 4; }
        // TODO: Issue here, LeicaImageWidth could be 20x, 10x, or 5x. THis is the 20x value. I change this elsewhere in code.
        public float LeicaImageWidth_um { get; set; } = 2048 * 0.325f;
        public bool IsThereNextWell { get; set; }
        public bool IsThereNextField { get; set; }
        public bool TestingMode = false;
        public bool TestingMode20x_5x_LiveRecon = true;
        public ImageAlign_Return IAR;

        public PointF WellBased_MaxRange_LastSet;
        public PointF Offset_WellRefined;
        public PointF Offset_FieldRefined { get; internal set; }
        public string FOVImageToDelete { get; internal set; }
        public string CurrentMaskFullPath { get; set; }
        public string CurrentShiftMaskFullPath { get; set; }
        public bool CancelRequested { get; set; }
        public bool DifferentThanLast_Y
        {
            get
            {
                if (PreviousFOV == null) return true;
                try
                {
                    return Math.Abs(PreviousFOV.PlateY_um - CurrentFOV.PlateY_um) > 1.5;
                }
                catch
                {
                    return true;
                }
            }
        }
        public bool DifferentThanLast_X
        {
            get
            {
                if (PreviousFOV == null) return true;
                try { return Math.Abs(PreviousFOV.PlateX_um - CurrentFOV.PlateX_um) > 1.5; } catch { return true; }
            }
        }
        public int FirstFieldIdx_Start { get; set; }
        public int WellFieldChecks { get; set; }
    }

    public class ImageAlign_Return
    {
        public string WellLabel;
        public string Note;
        public string ExportName { get; set; }
        public string OverlayName;
        public PointF FractCenter_GE_um;
        public PointF FractCenter_LE_um;
        public Register_Return RR;
        public double ImgInternal_RedVariation;
        public double ImgExternal_RedVariation;
        public DateTime StartTime;
        public DateTime EndTime;

        [XmlIgnore]
        public Associate_InCell_Leica Parent;


        public PointF Offset_Before { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF Offset_After { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF Leica_StartingPosition { get; set; } //Just informative in the saved .xml file, not used in the calculation
        public PointF InCell_StartingPosition { get; set; } //Just informative in the saved .xml file, not used in the calculation

        public ImageAlign_Return()
        {
            //Need this to serialize
        }

        public ImageAlign_Return(string well, Associate_InCell_Leica AIL_PArent)
        {
            StartTime = DateTime.Now;
            WellLabel = well;
            Note = "";
            Parent = AIL_PArent;
        }

        public ImageAlign_Return(string well, string OnlyNote)
        {
            WellLabel = well;
            Note = OnlyNote;
        }



        public static float Distance(ImageAlign_Return Offset1, ImageAlign_Return Offset2)
        {
            return Distance(Offset1.Offset, Offset2.Offset);
        }

        public static float Distance(PointF Offset1, PointF Offset2)
        {
            return (float)Math.Sqrt((Offset1.X - Offset2.X) * (Offset1.X - Offset2.X) + (Offset1.Y - Offset2.Y) * (Offset1.Y - Offset2.Y));
        }

        internal void ExportQC(string FullFileName)
        {
            this.EndTime = DateTime.Now;
            //Serialize this and save it as an XML file
            using (var writer = new StreamWriter(FullFileName))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }

        public static ImageAlign_Return Load(string fullName)
        {
            try
            {
                using (var stream = File.OpenRead(fullName))
                {
                    var serializer = new XmlSerializer(typeof(ImageAlign_Return));
                    return (ImageAlign_Return)serializer.Deserialize(stream);
                }
            }
            catch
            {

            }
            return null;
        }

        public static void ReadFromFolder(string FolderPath)
        {
            var R = new Random();
            DirectoryInfo DI = new DirectoryInfo(FolderPath);
            ImageAlign_Return IAR;
            StringBuilder sB = new StringBuilder();
            bool First = true;
            foreach (FileInfo FI in DI.GetFiles("*.xml"))
            {
                IAR = ImageAlign_Return.Load(FI.FullName);
                if (First)
                {
                    sB.Append("Name" + "\t" + IAR.FullResultsString(true) + "\r\n");
                    First = false;
                }
                sB.Append(FI.Name + "\t" + IAR.FullResultsString() + "\r\n");
                if (R.Next(0, 200) < 2) Debug.Print(FI.Name);
            }
            File.WriteAllText(Path.Combine(FolderPath, "_XMLCompiled.txt"), sB.ToString());
        }

        public string FullResultsString(bool H = false)
        {
            char d = '\t';

            string ProcessVal(bool Header, string Name, string Type, object Ret)
            {
                if (Type == "PointF")
                {
                    if (Header) return Name + ".X" + d + Name + ".Y";
                    var PF = (PointF)Ret;
                    return "" + PF.X + d + PF.Y;
                }
                if (Header) return Name;
                return "" + Ret;
            }

            StringBuilder sB = new StringBuilder();
            foreach (System.Reflection.PropertyInfo prop in this.GetType().GetProperties()) sB.Append(ProcessVal(H, prop.Name, prop.PropertyType.Name, prop.GetValue(this)) + d);
            foreach (System.Reflection.FieldInfo prop in this.GetType().GetFields()) sB.Append(ProcessVal(H, prop.Name, prop.FieldType.Name, prop.GetValue(this)) + d);
            if (RR != null)
            {
                foreach (System.Reflection.PropertyInfo prop in RR.GetType().GetProperties()) sB.Append(ProcessVal(H, prop.Name, prop.PropertyType.Name, prop.GetValue(RR)) + d);
                foreach (System.Reflection.FieldInfo prop in RR.GetType().GetFields()) sB.Append(ProcessVal(H, prop.Name, prop.FieldType.Name, prop.GetValue(RR)) + d);
            }
            //foreach (System.Reflection.PropertyInfo prop in this.GetType().GetProperties()) sB.Append(prop.Name + ":" + prop.GetValue(this) + "\t");
            //foreach (System.Reflection.FieldInfo field in this.GetType().GetFields()) sB.Append(field.Name + ":" + field.GetValue(this) + "\t");
            //if (RR != null)
            //{
            //    foreach (System.Reflection.PropertyInfo prop in RR.GetType().GetProperties()) sB.Append(prop.Name + ":" + prop.GetValue(RR) + "\t");
            //    foreach (System.Reflection.FieldInfo field in RR.GetType().GetFields()) sB.Append(field.Name + ":" + field.GetValue(RR) + "\t");
            //}
            return sB.ToString().Replace("\n", " ");
        }

        public PointF Offset
        {
            get
            {
                return new PointF((FractCenter_GE_um.X - FractCenter_LE_um.X), (FractCenter_GE_um.Y - FractCenter_LE_um.Y));
            }
        }

        public string Offsets
        {
            get
            {
                return "x" + (FractCenter_GE_um.X - FractCenter_LE_um.X).ToString("0") + " y" + (FractCenter_GE_um.Y - FractCenter_LE_um.Y).ToString("0");
            }
        }
    }

    public static class LeicaOnTheFly
    {
        static bool Testing = true;

        public static SharedRes_OnTheFly SRO;
        public static void InitStuff(INCELL_Folder iC_Folder, Seg.CropImageSettings CropSettings, MLSettings_PL mLSettings_PL, string ExpName)
        {
            ImageExtensions.Add(".TIFF"); ImageExtensions.Add(".TIF"); ImageExtensions.Add(".BMP");

            SRO = new SharedRes_OnTheFly(iC_Folder, CropSettings, mLSettings_PL, ExpName);
            //Now move those inital images to a new folder

            if (!Testing) MoveImages(iC_Folder, "Initial");
            if (Testing) NextField();
        }

        public static void NextField()
        {
            if (!Testing) SRO.IC_Folder.RefreshImages();
            var ImageParts = SRO.IC_Folder.XDCE.WellFOV_Dict;
            var Res = Seg.Segmentation.RunSegmentation(false, DateTime.Now, ImageParts, SRO.CropSettings, SRO.IC_Folder.InCell_Wavelength_Notes); //Actually Segmentation Part of it
            if (!Testing) MoveImages(SRO.IC_Folder, "Regular");
        }

        internal static HashSet<string> ImageExtensions = new HashSet<string>();

        public static void MoveImages(INCELL_Folder iC_Folder, string NewLocationName)
        {
            DirectoryInfo DI = new DirectoryInfo(Path.Combine(iC_Folder.FullPath, SRO.ExpName, NewLocationName));
            if (!DI.Exists) DI.Create(); string ext;
            foreach (string file in Directory.GetFiles(iC_Folder.FullPath))
            {
                ext = Path.GetExtension(file).ToUpper();
                if (ImageExtensions.Contains(ext))
                    File.Move(file, Path.Combine(DI.FullName, Path.GetFileName(file)));
            }
        }
    }

    public class SharedRes_OnTheFly
    {
        public INCELL_Folder IC_Folder;
        public Seg.CropImageSettings CropSettings;
        public MLSettings_PL MLSettings { get => CropSettings.MLSettings; }
        public string ExpName;

        public SharedRes_OnTheFly(INCELL_Folder iC_Folder, Seg.CropImageSettings CropSettings, MLSettings_PL mLSettings_PL, string ExpName)
        {
            this.IC_Folder = iC_Folder;
            this.ExpName = ExpName.Trim();
            this.CropSettings = new Seg.CropImageSettings(CropSettings); //Important so we can change these independent from the main ones

            this.CropSettings.RunMLProcessing = "PL";
            this.CropSettings.RandomizeImageIntensity_Min = 1;
            this.CropSettings.MLSettings = mLSettings_PL;
        }
    }

}