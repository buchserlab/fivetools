﻿using ImageMagick;
using OpenCvSharp.Flann;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tensorflow;
using Tensorflow.NumPy;
using FIVE;
using FIV_IMG_CLib;
using OpenCvSharp;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Runtime.ConstrainedExecution;
using System.Xml.Linq;
using Tensorflow.Keras.Layers;
using Newtonsoft.Json;
using System.Text.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;
using System.Text.Json.Serialization.Metadata;
using System.Net.NetworkInformation;
using Tensorflow.Operations.Initializers;

public static class PythonEnvironment
{
    static string PythonPath { get; set; }
    static string ModelPath { get => @"E:\Yolo11Stuff\Neuron_1\best.pt"; } // Path to the YOLO model file
    static string OutputDir { get => @"E:\Yolo11Stuff\Neuron_2"; }
    static string InputDir { get => @"E:\Yolo11Stuff\Neuron_1"; }
    public static string EscapeFileName(string input)
    {
        if (string.IsNullOrEmpty(input))
            return input;

        StringBuilder builder = new StringBuilder(input.Length * 2); // Allocate enough capacity

        foreach (char c in input)
        {
            switch (c)
            {
                case '\\':
                    builder.Append("\\\\"); // Escape backslashes
                    break;
                case '\'':
                    builder.Append("\\'"); // Escape single quotes
                    break;
                case '\"':
                    builder.Append("\\\""); // Escape double quotes
                    break;
                case '\n':
                    builder.Append("\\n"); // Escape newline
                    break;
                case '\r':
                    builder.Append("\\r"); // Escape carriage return
                    break;
                case '\t':
                    builder.Append("\\t"); // Escape tab
                    break;
                case '\b':
                    builder.Append("\\b"); // Escape backspace
                    break;
                case '\f':
                    builder.Append("\\f"); // Escape form feed
                    break;
                default:
                    builder.Append(c); // Append other characters unchanged
                    break;
            }
        }

        return builder.ToString();
    }

    public static async Task<(bool pythonFound, bool pipAlreadyInstalled, bool pipWasInstalled,
                              bool torchAlreadyInstalled, bool torchWasInstalled,
                              bool yoloAlreadyInstalled, bool yoloWasInstalled, bool allClear)>
        Main()
        {
        bool pythonFound = false;
        bool pipAlreadyInstalled = false;
        bool pipWasInstalled = false;
        bool torchAlreadyInstalled = false;
        bool torchWasInstalled = false;
        bool yoloAlreadyInstalled = false;
        bool yoloWasInstalled = false;
        bool allClear = false;

        try
        {
            PythonPath = FindPythonInstallation();

            if (string.IsNullOrEmpty(PythonPath))
            {
                Trace.WriteLine("Python installation not found.");
                return (pythonFound: false, pipAlreadyInstalled: false, pipWasInstalled: false,
                        torchAlreadyInstalled: false, torchWasInstalled: false,
                        yoloAlreadyInstalled: false, yoloWasInstalled: false, allClear: true);
            }
            else
            {
                pythonFound = true;
                Trace.WriteLine($"Python found at: {PythonPath}");
            }

            // Check if pip is installed
            if (!CheckPipInstalled(PythonPath))
            {
                Trace.WriteLine("Pip not installed. Attempting to install...");
                try
                {
                    await InstallPipAsync(PythonPath);
                    pipWasInstalled = true; // Pip was missing and successfully installed
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"Error: {ex.Message}" + "Failed to install Pip");
                }
            }
            else
            {
                pipAlreadyInstalled = true; // Pip was already installed
            }
            // Check if Ultralytics is installed if this is true then Torch is also installed.
            if (!CheckUltralyticsInstalled(PythonPath))
            {
                Trace.WriteLine("YOLO not installed. Attempting to install...");
                //this will also install torch if you don't have it.
                await InstallYoloAsync(PythonPath);
                yoloWasInstalled = true; // YOLO was missing and successfully installed
                torchWasInstalled = true; // Torch was missing and successfully installed
                allClear = true;
            }
            else
            {
                yoloAlreadyInstalled = true; // YOLO was already installed
                torchAlreadyInstalled = true;
                Trace.WriteLine("YOLO is already installed.");
                allClear = true;
            }
            if (allClear)
            {
                string inputsPath = InputDir;

                string[] imagePaths = Directory.GetFiles(inputsPath, "*.bmp");
                StringBuilder stringBuilder = new StringBuilder();
              
                stringBuilder.Append('[');

                foreach (string path in imagePaths)
                {
                    stringBuilder.Append($"'{path}'");
                    stringBuilder.Append(',');
                    stringBuilder.Append(' ');
                }

                // Remove the trailing ", "
                stringBuilder.Remove(stringBuilder.Length - 2,2);

                stringBuilder.Append(']');
                string pythonImageArray = EscapeFileName(stringBuilder.ToString());
                int imagesLen = imagePaths.Length;
                string pythonCommand = "";
                if (imagesLen > 0)
                {



                    StringBuilder sb = new StringBuilder();
                    //Let's keep this as short and as simple as possible. If it fails debugging will be a nightmare.
                    string imports = $"-c \"import json; import torch; import ultralytics; from ultralytics import YOLO; ";
                    sb.Append(imports);
                    sb.Append($"model = YOLO(r'{ModelPath}'); ");
                    sb.Append($"image_array = {stringBuilder.ToString()}; ");
                    sb.Append($"re = model.predict(source=image_array, save=True, project=r'{OutputDir}');");
                    sb.Append("fin = {};");

                    for (int i = 0; i < imagesLen; i++)
                    {
                        // Create a valid JSON structure in Python
                        sb.Append("ou = {};");

                        // Serialize masks
                        sb.Append($"ou['m_xy'] = [polygon.tolist() for polygon in re[{i}].masks.xy if polygon is not None] if re[{i}].masks else [];");
                        sb.Append($"ou['m_shape'] = list(re[{i}].masks.shape) if re[{i}].masks else [];");

                        // Serialize boxes
                        sb.Append($"ou['b_xywh'] = re[{i}].boxes.xywh.tolist() if re[{i}].masks else [];");
                        sb.Append($"ou['b_xyxy'] = re[{i}].boxes.xyxy.tolist() if re[{i}].masks else [];");
                        sb.Append($"ou['b_cls'] = re[{i}].boxes.cls.tolist() if re[{i}].masks else [];");
                        sb.Append($"ou['b_data'] = re[{i}].boxes.data.tolist() if re[{i}].masks else [];");
                        sb.Append($"ou['b_conf'] = re[{i}].boxes.conf.tolist() if re[{i}].masks else [];");
                        sb.Append($"ou['m_xy'] = ou['m_xy'][0] if ou['m_xy'] and len(ou['m_xy']) == 1 else ou['m_xy'];");
                        sb.Append($"fin['obj{i}'] = ou;");
                    }

                    // Retrieve the final Python command as a single string
                    sb.AppendLine($"fin['Names'] = model.names");
                    sb.Append($"print(json.dumps(fin, indent=3));");
                    pythonCommand = sb.ToString();

                    RunPythonCommand(PythonPath, pythonCommand, true, imagePaths, ModelPath);
                }
                else
                {
                    pythonCommand = "-c \"print('No images found in the input directory.');\"";
                }

                //RunPythonCommand(PythonPath, pythonCommand); // Run YOLO inference (example code
                //runyolo();
            }
            else
            {
                Trace.WriteLine("Not all clear, cannot run YOLO.");
            }

        }
        catch (Exception ex)
        {
            Trace.WriteLine($"Error: {ex.Message}");
        }
        if (pythonFound && (pipAlreadyInstalled || pipWasInstalled) && (torchAlreadyInstalled || torchWasInstalled) && (yoloAlreadyInstalled || yoloWasInstalled))
        {
            allClear = true;
        }
        return (pythonFound: pythonFound, pipAlreadyInstalled: pipAlreadyInstalled, pipWasInstalled: pipWasInstalled,
                                torchAlreadyInstalled: torchAlreadyInstalled, torchWasInstalled: torchWasInstalled,
                                                    yoloAlreadyInstalled: yoloAlreadyInstalled, yoloWasInstalled: yoloWasInstalled, allClear: allClear);
    }


    public static string FindPythonInstallation()
    {
        // 1. Check the users Path Variable
        string pythonFromPath = GetPythonFromEnvironment();
        if (string.IsNullOrEmpty(pythonFromPath))
        {
            // 2. Check for the Python Launcher (py.exe)
            string pythonLauncherPath = @"C:\Windows\py.exe";
            if (File.Exists(pythonLauncherPath))
            {
                Trace.WriteLine($"Found Python Launcher: {pythonLauncherPath}");
                return pythonLauncherPath;
            }
        }
        //3 look for custom Installations (Anaconda/Miniconda)
        List<string> potentialPaths = new List<string>();
        string[] basePaths = new[]
        {
             @"C:\", @"D:\", @"E:\",@"K\", @"R:\"
        };

        foreach (string basePath in basePaths)
        {

            int enumerator = Math.Max(0, DateTime.Now.Year - 2024); // Offset by 2024
            int startVersion = 13 + enumerator; // Starting version dynamically adjusted
            int endVersion = 9 + enumerator; // Minimum version to support

            for (int version = startVersion; version >= endVersion; version--)
            {
                string pythonVersion = $"Python3{version}";

                // Dynamically add potential Python paths
                potentialPaths.Add(Path.Combine(basePath, pythonVersion, "python.exe"));
                potentialPaths.Add(Path.Combine(basePath, "Program Files", pythonVersion, "python.exe"));
                potentialPaths.Add(Path.Combine(basePath, "Program Files (x86)", pythonVersion, "python.exe"));
                potentialPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Programs", "Python", pythonVersion, "python.exe"));
            }

            // Add Anaconda/Miniconda paths
            potentialPaths.Add(Path.Combine(basePath, "Anaconda3", "python.exe"));
            potentialPaths.Add(Path.Combine(basePath, "Miniconda3", "python.exe"));
            potentialPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Miniconda3", "python.exe"));
            potentialPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Anaconda3", "python.exe"));
        }

        // 4. Check Potential Paths
        foreach (string path in potentialPaths)
        {
            if (File.Exists(path))
            {
                Console.WriteLine($"Found Python at: {path}");
                return path;
            }
        }


        return !string.IsNullOrEmpty(pythonFromPath) ? pythonFromPath : null;
    }

    static string GetPythonFromEnvironment()
    {
        ProcessStartInfo processStartInfo = new ProcessStartInfo
        {
            FileName = "cmd.exe",
            Arguments = "/c where python",
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true
        };

        using Process process = Process.Start(processStartInfo);
        string output = process.StandardOutput.ReadToEnd();
        process.WaitForExit();
        string[] paths = output.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        return paths.Length > 0 ? paths[0] : null;
    }

    static bool CheckPipInstalled(string pythonPath)
    {
        return RunPythonCommand(pythonPath, "-m pip --version");
    }

    static async Task InstallPipAsync(string pythonPath)
    {
        string getPipUrl = "https://bootstrap.pypa.io/get-pip.py";
        string getPipFile = Path.Combine(Path.GetTempPath(), "get-pip.py");

        using (var client = new HttpClient())
        using (var fileStream = new FileStream(getPipFile, FileMode.Create, FileAccess.Write))
        {
            await client.DownloadWithProgressAsync(getPipUrl, fileStream, progress =>
            {
                Trace.WriteLine($"Pip download progress: {progress:F2}%");
            });
        }

        RunPythonCommand(pythonPath, $"\"{getPipFile}\"");
        if (File.Exists(getPipFile))
        {
            File.Delete(getPipFile);
        }
    }

    static bool CheckUltralyticsInstalled(string pythonPath)
    {
        return RunPythonCommand(pythonPath, "-c \"import ultralytics; print(ultralytics.__version__)\"");
    }
    private static ProcessStartInfo CreatePythonProcessStartInfo(string pythonPath, string arguments)
    {
        return new ProcessStartInfo
        {
            FileName = pythonPath,
            Arguments = arguments,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            CreateNoWindow = true
        };
    }
    static async Task InstallYoloAsync(string pythonPath)
    {
        try
        {
            Trace.WriteLine("Starting YOLO and Ultralytics installation...");

            ProcessStartInfo startInfo = CreatePythonProcessStartInfo(pythonPath, "-m pip install yolo && pip install -U ultralytics");
            using (Process process = new Process { StartInfo = startInfo })
            {
                process.OutputDataReceived += (sender, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data))
                    {
                        Trace.WriteLine(args.Data); // Display installation progress in real-time
                    }
                };

                process.ErrorDataReceived += (sender, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data))
                    {
                        Trace.WriteLine($"ERROR: {args.Data}"); // Log any error messages
                    }
                };

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                await process.WaitForExitAsync();

                if (process.ExitCode == 0)
                {
                    Trace.WriteLine("YOLO and Ultralytics installation completed successfully.");
                }
                else
                {
                    Trace.WriteLine($"YOLO installation failed with exit code {process.ExitCode}.");
                }
            }
        }
        catch (Exception ex)
        {
            Trace.WriteLine($"An error occurred while installing YOLO: {ex.Message}");
        }
    }

    static bool CheckPackageInstalled(string pythonPath, string packageName)
    {
        return RunPythonCommand(pythonPath, $"-c \"import {packageName}; print({packageName}.__version__)\"");
    }

    static bool RunPythonCommand(string pythonPath, string arguments, bool parseJson = false, string[] images = null, string model = "")
    {
        try
        {
            ProcessStartInfo startInfo = CreatePythonProcessStartInfo(pythonPath, arguments);

            using Process process = Process.Start(startInfo);
            string output = process.StandardOutput.ReadToEnd();
            if (parseJson)
            {
                try
                {
                   YoloSerializationTools.ProccessJson(output, images, ModelPath);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"Error: {ex.Message}" + ex.StackTrace);
                }
            }
            string error = process.StandardError.ReadToEnd();

            if (!string.IsNullOrEmpty(error))
            {
                Trace.WriteLine($"Error: {error}");
                return false;
            }
            process.WaitForExit();
            return true;

        }
        catch (Exception ex)
        {
            Trace.WriteLine($"Command failed: {ex.Message}");
        }

        return false;
    }

    static async Task RunPythonCommandAsync(string pythonPath, string arguments)
    {
        try
        {
            ProcessStartInfo startInfo = CreatePythonProcessStartInfo(pythonPath, arguments);

            using (Process process = Process.Start(startInfo))
            {
                string output = await process.StandardOutput.ReadToEndAsync();
                string error = await process.StandardError.ReadToEndAsync();
                await process.WaitForExitAsync();

                if (!string.IsNullOrWhiteSpace(output))
                {
                    Trace.WriteLine(output);
                }

                if (!string.IsNullOrWhiteSpace(error))
                {
                    Trace.WriteLine(error);
                }
            }
        }
        catch (Exception ex)
        {
            Trace.WriteLine($"Command failed: {ex.Message}");
        }
    }
}




public static class HttpClientExtensions
{
    public static async Task DownloadWithProgressAsync(
        this HttpClient client,
        string requestUri,
        Stream destination,
        Action<float>? progress = null,
        int bufferSize = 81920,
        CancellationToken cancellationToken = default)
    {
        using HttpResponseMessage response = await client.GetAsync(requestUri, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
        response.EnsureSuccessStatusCode();

        long? contentLength = response.Content.Headers.ContentLength;
        using Stream downloadStream = await response.Content.ReadAsStreamAsync(cancellationToken);

        byte[] buffer = new byte[bufferSize];
        long totalBytesRead = 0L;
        bool reportProgress = progress != null && contentLength.HasValue;

        while (true)
        {
            int bytesRead = await downloadStream.ReadAsync(buffer, 0, buffer.Length, cancellationToken);
            if (bytesRead == 0)
            {
                break; // End of stream
            }

            await destination.WriteAsync(buffer, 0, bytesRead, cancellationToken);
            totalBytesRead += bytesRead;

            if (reportProgress)
            {
                float percentage = (float)totalBytesRead / contentLength.Value * 100f;
                progress?.Invoke(percentage);
            }
        }

        // Final progress report to ensure 100% is reported
        if (reportProgress)
        {
            progress?.Invoke(100f);
        }
    }
}