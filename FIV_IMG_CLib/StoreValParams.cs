﻿using FIVE.InCellLibrary;
using FIVE.TF;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace FIVE_IMG
{
    public class Store_Validation_Parameters
    {
        public string SequenceFull { get; set; }
        public string SequenceStart { get; set; }
        public string SequenceEnd { get; set; }
        public string Name { get; set; }
        public string FolderPath { get; set; }
        public List<string> MRU_FolderPaths { get; set; }

        [XmlIgnore]
        protected HashSet<string> MRU_FolderPaths_Hash { get; set; }
        public string SearchPattern { get; set; }
        public bool RenameColumns_DuringInCartaCompile { get; set; }

        public string HCS_Image_SourceFolder { get; set; }
        public string HCS_Image_DestinationFolder { get; set; }
        public string HCS_Image_PlateName { get; set; }
        public bool HCS_Image_CropSquare { get; set; }
        public SVP_Registration_Params RegParams { get; set; }
        public FIVE.TF.ModelInferSettings ModelInferSettings { get; set; }
        public string NormDivisorColumn { get; set; }
        public string fs_Port { get; set; }
        public string fs_dbname { get; set; }
        public string fs_Username { get; set; }
        public string fs_Password { get; set; }
        public string fs_Filetoload { get; set; }
        public int PrePickCheck_Annotated_MaxToLoad { get; set; }
        public string FIVTools_Version { get; set; }
        public DateTime SaveTime { get; set; }
        public int ChopSettings_OrigWidth { get; set; } = 510;
        public int ChopSettings_OutputWidth { get; set; } = 192;
        public string ChopSettings_ExportBaseFolder { get; set; } = @"c:\temp\chops\";
        public INCELL_MeasuredOffsets SavedOffsets { get; set; }

        public PL.MLSettings_PL MLSettings_PL { get; set; }

        public Seg.CropImageSettings CropSettings { get; set; }

        [XmlIgnore]
        public ClassDefinition ClassDef { get; set; }

        public static string DefaultPath = Path.Combine(Path.GetTempPath(), "geicDesktoptools_settings.xml");

        public bool RegenMRUs(string NewFolderPath = "")
        {
            if (MRU_FolderPaths == null) MRU_FolderPaths = new List<string>();
            if (MRU_FolderPaths_Hash == null) MRU_FolderPaths_Hash = new HashSet<string>(MRU_FolderPaths);
            if (NewFolderPath == "") return false;
            if (!MRU_FolderPaths_Hash.Contains(NewFolderPath))
            {
                MRU_FolderPaths.Add(NewFolderPath); MRU_FolderPaths_Hash.Add(NewFolderPath);
                return true;
            }
            return false;
        }

        public Store_Validation_Parameters()
        {
            RenameColumns_DuringInCartaCompile = true;
            NormDivisorColumn = "mlPlateIndex";
            RegParams = new SVP_Registration_Params() { Mask_PixelsExpand = 0, InitialErrorDivisor = 1.5F, InitialDefaultOffset = new PointF(940, 2203) };

            fs_Port = "5432";
            fs_dbname = "postgres";
            fs_Username = "postgres";
            fs_Password = "ge2";

            CropSettings = new Seg.CropImageSettings();
            MLSettings_PL = new PL.MLSettings_PL();
            ModelInferSettings = FIVE.TF.ModelInferSettings.Default01();

            PrePickCheck_Annotated_MaxToLoad = 384;
        }

        public static Store_Validation_Parameters Load()
        {
            return Load(DefaultPath);
        }

        public static Store_Validation_Parameters Load(string Path)
        {
            if (!File.Exists(Path)) return null;
            try
            {
                using (var stream = File.OpenRead(Path))
                {
                    var serializer = new XmlSerializer(typeof(Store_Validation_Parameters));
                    var SVP = (Store_Validation_Parameters)serializer.Deserialize(stream);
                    if (SVP.NormDivisorColumn == null) SVP.NormDivisorColumn = "mlPlateIndex";
                    SVP.RegParams.Check();
                    if (SVP.MLSettings_PL == null) SVP.MLSettings_PL = new PL.MLSettings_PL();
                    if (SVP.CropSettings == null || SVP.CropSettings.Version == 0) SVP.CropSettings = new Seg.CropImageSettings(true);
                    if (SVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon == null) SVP.CropSettings.WellLookUp_WellLabel_Comma_Folder_Semicolon = "A - 1,WT;B - 1;MU";
                    if (SVP.CropSettings.RunMLProcessing == null) SVP.CropSettings.RunMLProcessing = "";
                    if (SVP.ModelInferSettings.ModelPath_Classification == null) SVP.ModelInferSettings.ModelPath_Classification = ModelInferSettings.Default01().ModelPath_Classification;
                    if (SVP.ModelInferSettings.ModelPath_Measure == null) SVP.ModelInferSettings.ModelPath_Measure = ModelInferSettings.Default01().ModelPath_Measure;
                    return SVP;
                }
            }
            catch
            {
                return null;
            }
        }

        public void Save()
        {
            Save(DefaultPath);
        }

        public void Save(string Path)
        {
            this.SaveTime = DateTime.Now;
            using (var writer = new StreamWriter(Path))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }

        public Store_Validation_Parameters Copy()
        {
            var destination = new Store_Validation_Parameters();
            var fields = typeof(Store_Validation_Parameters).GetFields();
            var properties = typeof(Store_Validation_Parameters).GetProperties();

            foreach (var field in fields)
            {
                var value = field.GetValue(this);
                field.SetValue(destination, value);
            }
            foreach (var prop in properties)
            {
                if (prop.CanRead && prop.CanWrite)
                {
                    var value = prop.GetValue(this);
                    prop.SetValue(destination, value);
                }
            }

            return destination;
        }
    }

    public class SVP_Registration_Params
    {
        public bool Rotate180 { get; set; }
        public float InitialErrorDivisor { get; set; }
        public PointF InitialDefaultOffset { get; set; }
        public double ImageMinStDev { get; set; }
        public int Mask_PixelsExpand { get; set; }
        public bool Mask_20x_to_10x { get; set; }
        public int MskImg_InCell_Width { get; set; }
        public int MskImg_Final_Leica { get; set; }
        public bool CropForLeica { get; set; }
        public string PlateNameBeginning { get; set; }
        public bool LiveTile { get; set; }
        public bool FullAuto { get; set; }

        /// <summary>
        /// Bay put this in to try and match up the masks exactly to the DMD Size
        /// </summary>
        public int MskImg_dither_Width { get; set; } //Bay put this in to try and match up the masks exactly to the DMD size (800x600?)

        public int MskShift_Final_AddPixels_X { get; set; } //Adds this amount of pixels to additionally shift in X (Default 0)
        public int MskShift_Final_AddPixels_Y { get; set; } //Adds this amount of pixels to additional shift in Y (Default 0)
        public int MskShift_Final_ZoomPixels { get; set; } //Removes this many pixels from each side to "zoom" in the mask. Negative adds black pixels to zoom out (Default -1)
        public float MskShift_ShiftMultiplier { get; set; } //This adusts the "strength" of the mask shift by this factor. 2 means it shifts twice as far as it thinks it should be shifted. 0.5 means to shift it half as far. - anything will flop the direction of the shifts (Default 0.5)

        public PointF FOVOffset_MaxDeviationFromWell { get; set; } //If offset for a FOV changes more than this amount from the Well Offset, then this FOV will be considered to FAIL registration and will be skipped (Default 25,25)
        public float LastFOVOffset_over_WellOffset_MixRatio { get; set; } //For an UNREGISTERED FOV, a MixRatio = 1 means to use the last field's Offset, MixRatio = 0 means to use the Well Offset, and 0.5 means to mix them equally (Default 1)

        public SizeF GE_LeicaSizeRatio_ForImage { get; set; } //This is when the GE image is produced for use in registration (Default 1,1)
        public SizeF GE_LeicaSizeRatio_ForMoving { get; set; } //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset) (Default 1,1)

        //The next set have to do with Reconstruction

        public float scanRecon_MagnificationNew { get; set; }
        public float scanRecon_FractionFilled { get; set; }
        public string scanRecon_AppendPlateID { get; set; }


        public SVP_Registration_Params()
        {
            //Below are the defaults

            //Crop for Leica : The GE image is slighly larger, so we will scale it up to 1040 (instead of 1024), then chop off 8 pixels all the way around
            CropForLeica = true;
            Rotate180 = true;
            MskImg_InCell_Width = 2040;
            MskImg_Final_Leica = 1024;
            ImageMinStDev = 20;
            //MskImg_toChop = 8; //This is necessary for the cells to line up with the masks - removed
            MskImg_dither_Width = 600; //Bay put this in to try and match up the masks exactly to the DMD size (800x600?)
            LastFOVOffset_over_WellOffset_MixRatio = 0.5F;
            FOVOffset_MaxDeviationFromWell = new PointF(100, 100);
            GE_LeicaSizeRatio_ForImage = new SizeF(1.0149F, 1.0149F); //This is when the GE image is produced for used in registration
            GE_LeicaSizeRatio_ForMoving = new SizeF(1, 1);            //This is when the GE coordinates are adapted to find the Leica coordinates (in combination with the offset)
            PlateNameBeginning = DateTime.Now.ToString("yyyy");
            scanRecon_MagnificationNew = 5;
            scanRecon_FractionFilled = 0.3F;
            scanRecon_AppendPlateID = "_Recon|";
        }

        internal void Check()
        {
            if (InitialErrorDivisor <= 0)
            {
                InitialErrorDivisor = 1.5F; CropForLeica = true; Rotate180 = true;
            }
            if (InitialDefaultOffset.IsEmpty) InitialDefaultOffset = new PointF(1000, 2500);
            if (FOVOffset_MaxDeviationFromWell.IsEmpty) FOVOffset_MaxDeviationFromWell = new PointF(100, 100);
            if (MskImg_InCell_Width <= 0) MskImg_InCell_Width = 2040;
            if (MskImg_Final_Leica <= 0) MskImg_Final_Leica = 1024;
            if (GE_LeicaSizeRatio_ForImage.IsEmpty)
            {
                GE_LeicaSizeRatio_ForImage = new SizeF(1.0149F, 1.0149F);
                GE_LeicaSizeRatio_ForMoving = new SizeF(1, 1);
            }
        }
    }


    /// <summary>
    /// Saves the mlPlateIndex and the Well Label to define the mlClassLayout
    /// </summary>
    public class ClassDefinition
    {
        public ClassDefinition()
        {
            ExpID = "";
            Version = "0.1";
            DateFirstSaved = DateTime.Now;
            Dict_PlateIndex_WellLabel_to_ClassLayout = new Dictionary<string, string>();
        }

        public string Version { get; set; }

        public string ExpID { get; set; }

        public DateTime DateFirstSaved { get; set; }

        public void Add(string PlateIndex, string WellLabel, string Class)
        {
            Dict_PlateIndex_WellLabel_to_ClassLayout.Add(KeyMaker(PlateIndex, WellLabel), Class);
        }

        public Dictionary<string, string> Dict_PlateIndex_WellLabel_to_ClassLayout { get; set; }

        public static string KeyMaker(string PlateIndex, string WellLabel)
        {
            return PlateIndex + "|" + WellLabel;
        }

        public string GetClassLabel(string PlateIndex, string WellLabel)
        {
            var Key = KeyMaker(PlateIndex, WellLabel);
        TryAgain:
            if (Dict_PlateIndex_WellLabel_to_ClassLayout.ContainsKey(Key))
                return Dict_PlateIndex_WellLabel_to_ClassLayout[Key];
            var T = PlateIndex.Split('A');
            if (T.Length >= 2)
            {
                T = T[1].Split('_');
                if (T.Length > 0)
                {
                    Key = KeyMaker(T[0], WellLabel);
                    goto TryAgain;
                }
            }
            return "";
        }

        public string GetClassDefinitions_Style_Spotfire()
        {
            string PlateIdxName = "mlPlateIndex";
            string WellLabelName = "WELL LABEL";

            var sB = new StringBuilder(); string[] arr;
            sB.Append("CASE\r\n");
            foreach (var item in Dict_PlateIndex_WellLabel_to_ClassLayout)
            {
                arr = item.Key.Split('|');
                sB.Append("  WHEN ([" + PlateIdxName + "] = \"" + arr[0] + "\") and ([" + WellLabelName + "] = \"" + arr[1] + "\" THEN \"" + item.Value + "\"\r\n");
            }
            sB.Append("END\r\n");
            return sB.ToString();
        }

        public string GetClassDefinitions_Style_PlateMap()
        {
            var sB = new StringBuilder(); string[] arr;
            char d = '\t';
            sB.Append("mlPlateIndex" + d + "WELL LABEL" + d + "mlClassLayout" + "\r\n");
            foreach (var item in Dict_PlateIndex_WellLabel_to_ClassLayout)
            {
                arr = item.Key.Split('|');
                sB.Append(arr[0] + d + arr[1] + d + item.Value + "\r\n");
            }
            return sB.ToString();
        }

        public static ClassDefinition LoadfromPlatemap(string PlateMapText)
        {
            var CD = new ClassDefinition(); char d = '\t'; string[] arr;
            var lines = PlateMapText.Split('\n').ToList();
            string[] headers = lines[0].Split(d);
            lines.RemoveAt(0);
            if (headers.Length <= 1)
            {
                headers = lines[0].Split(d); lines.RemoveAt(0);
            }
            foreach (var line in lines)
            {
                arr = line.Split(d);
                if (arr.Length > 1)
                {
                    CD.Add(arr[1], arr[4], arr[8]);
                }
            }
            return CD;
        }

        public string WellLookUp_WellLabel_Comma_Folder_Semicolon
        {
            get
            {
                var sB = new StringBuilder();
                foreach (var item in Dict_PlateIndex_WellLabel_to_ClassLayout)
                {
                    sB.Append(item.Key + "," + item.Value + ";");
                }
                return sB.ToString();
            }
        }

        public bool Save(string SavePath)
        {
            string data = System.Text.Json.JsonSerializer.Serialize(this);
            data = data.Replace(",", ",\r\n");
            File.WriteAllText(SavePath, data);
            return true;
        }

        public static ClassDefinition Load(string LoadPath)
        {
            string json = File.ReadAllText(LoadPath);
            try
            {
                var ret = System.Text.Json.JsonSerializer.Deserialize(json, typeof(ClassDefinition));

                if (ret == null)
                {
                    return new ClassDefinition();
                }
                return (ClassDefinition)ret;
            } catch (Exception ex)
            {
                return new ClassDefinition();
            }
        }
    }


}

