using FIVE.FOVtoRaftID;
using FIVE.ImageCheck;
using FIVE.InCellLibrary;
using FIVE.TF;
using FIVE_IMG;
using FIVE_IMG.Seg;
using Microsoft.VisualBasic;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tensorflow;
using Tensorflow.NumPy;

namespace FIVE
{

    namespace ML_IMG
    {

        public class ML_Context
        {
            public enum RunTypeEnum { Std, Classify_Measure_01 }

            public RunTypeEnum RunType;
            public ModelInferSettings MIS;
            public CropImageSettings CIS;
            public ClassDefinition ClassDef;
            public bool ModelTypeClassify;
            public wvDisplayParams CurrentWVParams;
            public TF_Image_Model TFIM;
            public List<TF_Image_Model> TFIM_Measures;
            public List<TF_Image_Model> TFIM_Classifies;
            public TF_Image_Model TFIM_Classify;
            public XDCE_ImageGroup CurrentWell;
            public bool SaveAnnotationBackToPointlist;
            public TF_Measure_Results MeasureResults;
            public ImageQueue ImgQueue;
            public BackgroundWorker BW;
            public string RequestedModelPath;
            public int BuildUpCroppedCount;
            private DateTime startWell;
            public static DateTime TotalStart;

            // Default Values
            private int CountAll = 0;
            private int LastCountRemaining = 0;
            private string LastWellPlate = "";
            public static string WholeRunTrack = "";
            public static int TotalWells = 0;
            public static int FinishedWells = 0;
            public bool StripGreen = false;
            public ImageCheck_PointList ImageCheck_PointsList = null;
            public int svMultiTargetIdx = 0;
            private Dictionary<string, (XDCE_Image xI, Bitmap bmp, ImageCheck_Point anno, bool skip, double milliLoad, double milliCrop)> BuildUpCropped = new();
            public bool UseRafts = true;

            public ML_Context(XDCE_ImageGroup currentWell, wvDisplayParams currentWVParams, ImageQueue imgQueue, ModelInferSettings modelInferSettings, CropImageSettings CIS, ClassDefinition ClassDef, ImageCheck_PointList imageChk_List = null, BackgroundWorker bw = null, bool ReclassifyAndMeasure = false, bool UseExistingRafts = true)
            {
                BuildUpCroppedCount = DefaultBuildUpCount;
                RunType = RunTypeEnum.Std;
                CurrentWell = currentWell;
                CurrentWVParams = currentWVParams;
                MIS = modelInferSettings;
                this.ClassDef = ClassDef;
                ModelTypeClassify = MIS.ActiveType_Classify;
                ImgQueue = imgQueue;
                ImageCheck_PointsList = imageChk_List;
                SaveAnnotationBackToPointlist = MIS.cmPerformClassifications && MIS.cmRecordBackAnnotations; //Adjusted 3/2024
                UseRafts = UseExistingRafts;
                BW = bw;

                if (ReclassifyAndMeasure)
                {
                    (TFIM_Classify, _) = LoadModelfromNVP(MIS, TriState.True);
                    TFIM_Classifies = TF_Image_Model.FromListOfPaths(MIS.ModelPaths_Classification_Multi);
                    TFIM_Measures = TF_Image_Model.FromListOfPaths(MIS.ModelPaths_Measure_Multi);
                }
                else
                {
                    (TFIM, RequestedModelPath) = LoadModelfromNVP(MIS);
                }
                MeasureResults = new TF_Measure_Results(ClassDef);
            }

            public static (TF_Image_Model model, string modelPath) LoadModelfromNVP(ModelInferSettings MIS, TriState UseActiveOrSpecify = TriState.UseDefault)
            {
                bool ModelClassify = UseActiveOrSpecify == TriState.UseDefault ? MIS.ActiveType_Classify : (UseActiveOrSpecify == TriState.True ? true : false);
                string FileOrFolderName = ModelClassify ? MIS.ModelPath_Classification : MIS.ModelPath_Measure;
                (var success, FileOrFolderName) = TF_Image_Model.GetModelFile_FromFolder(FileOrFolderName);
                if (!success) return (null, FileOrFolderName);

                var TFIM = TF_Image_Model.LoadFromFrozen(FileOrFolderName);
                if (ModelClassify) TFIM.Metadata.ThresholdDefault = MIS.Classification_KeepThreshold;

                return (TFIM, FileOrFolderName);
            }

            public bool Report_QueryCancel(string Message)
            {
                //TRUE means cancel
                if (BW != null)
                {
                    if (BW.CancellationPending) return true;
                    BW.ReportProgress(0, Message);
                }
                return false;
            }

            public void ML_ClassifyMeasure()
            {
                var startTime = DateTime.Now;
                RunType = RunTypeEnum.Classify_Measure_01;
                ML_AnnotateRafts();
                ModelMeasure_Finish(MIS.SaveMeasuresFolder);
                TotalTimeElapsed = (DateTime.Now - startTime).TotalMinutes.ToString("0.00");
            }

            public void ML_AnnotateRafts(bool processAll = true)
            {
                UseRafts = true;
                bool HasRafts_Regions = CurrentWell.HasRaftCalibration || CurrentWell.Rafts.Count > 0;
                if (CurrentWVParams.Tracings.Count > 0 && !HasRafts_Regions) UseRafts = false;
                if (UseRafts && (!CurrentWell.HasRaftCalibration && CurrentWell.Rafts.Count == 0))
                {
                    Report_QueryCancel("Not Calibrated."); return;
                }
                startWell = DateTime.Now; CountAll = 1;
                if (TFIM == null && TFIM_Measures.Count > 0) TFIM = TFIM_Measures[0];
                if (TFIM == null && TFIM_Classifies.Count > 0) TFIM = TFIM_Classifies[0];
                int CropSizeToUse = int.MinValue;
                //double ZoomToUse = TFIM.Metadata.ModelExpects_ResizeByCropTrue_ZoomFalse.HasValue ? (TFIM.Metadata.ModelExpects_ResizeByCropTrue_ZoomFalse.Value ? 1 : 0) : 1; //If no value assume Zoom=1, otherwise CropTrue = 1 / ZoomFalse = 0
                double ZoomToUse = TFIM.Metadata.DoCropvsZoom ? (TFIM.Metadata.DoCropvsZoom ? 1 : 0) : 1; //If no value assume Zoom=1, otherwise CropTrue = 1 / ZoomFalse = 0
                double rescaleDivisor = TFIM.Metadata.DoRescale ? 256 : 1; //TODO - If we are pushing 16-bit data to the model then this needs to change

                bool ProcessImage(string FileName, XDCE_Image xIKnown, Bitmap bmap, int CountRemaining, bool FastPart = true)
                {
                    XDCE_Image xI; double milliLoad = 0; bool bmapAvail = false;
                    if (xIKnown == null) { var XIs = CurrentWell.Images.Where(x => x.FileName == FileName); if (XIs.Count() == 0) return false; xI = XIs.First(); if (xI == null) return false; } else { xI = xIKnown; }

                    if (CropSizeToUse == int.MinValue) //if (UseRafts) //8/10/2023 turned this off . . probably just get it from the model no matter what
                    {
                        var rects = GetCropRectangles(xI); if (rects.Count == 0) return false;
                        var startingWidth = rects[0].Width * 0.9;
                        if (TFIM.Metadata.DoResize)
                        {
                            CropSizeToUse = TFIM.Input_Width > 1 ? (int)TFIM.Input_Width : (int)startingWidth;
                            if (!TFIM.Metadata.DoCropvsZoom) ZoomToUse = (double)CropSizeToUse / startingWidth;
                        }
                        else CropSizeToUse = (int)startingWidth;
                    }

                    if (RunType == RunTypeEnum.Std && UseRafts) { if (!CanAnyRaftsBeAnalyzed(xI)) return false; }//This is because of the selection for which rafts to annotate

                    if (bmap == null)
                    { var sTime = DateTime.Now; (bmap, bmapAvail) = ImgQueue.CombinedBMAP(CurrentWell, xI, CurrentWVParams); milliLoad = (DateTime.Now - sTime).TotalMilliseconds; }

                    if (RunType == RunTypeEnum.Std) DeriveAnnotationsFromModel(xI, bmap, CropSizeToUse, ZoomToUse, rescaleDivisor, milliLoad);
                    if (RunType == RunTypeEnum.Classify_Measure_01)
                    {
                        DeriveAnnotationsFromModel_Measure(xI, bmap, CropSizeToUse, ZoomToUse, rescaleDivisor, milliLoad);
                        ImgQueue.Remove(xI, CurrentWVParams); //We can remove from the queue to avoid confusion and memory issues
                    }
                    Report(CountRemaining, xI, bmapAvail);
                    if (MeasureResults.DataPoints.Count > 3)  //Write some stuff out so we know what is going on . . mark that as exported so we don't do it twice
                        ModelMeasure_Finish(MIS.SaveMeasuresFolder); 
                    return true;
                }

                if (processAll)
                {
                    //First run through all the images in the Queue
                    var ToProcess = new HashSet<string>(CurrentWell.Images.Where(x => x.wavelength == 0).Select(x => x.FileName));
                    CountAll = ToProcess.Count;

                    //Has to be the right plate!!!!  Can't just be enqueued
                    var IQD = new Dictionary<ImgQueueKey, Bitmap>(ImgQueue.Dict); //Safer to copy this since it gets modified by accident
                    foreach (var KVP in IQD)
                    {
                        if (KVP.Value == null) continue;
                        if (KVP.Key.WVPstr != CurrentWVParams.ToString()) continue;
                        if (KVP.Key.PlateStr != CurrentWell.PlateID) continue;
                        string tName = KVP.Key.NameStr;

                        ToProcess.Remove(tName);
                        ProcessImage(tName, null, KVP.Value, ToProcess.Count, true);
                    }

                    //Now continue through the rest of the images
                    int i = 0;
                    foreach (string tName in ToProcess)
                    {
                        ProcessImage(tName, null, null, (ToProcess.Count - ++i), false);
                    }
                    //Last time there are some remaining ones . . 
                    if (RunType == RunTypeEnum.Classify_Measure_01 && BuildUpCropped.Count > 0) DeriveAnnotationsFromModel_Measure(null, null, 0);
                }

                //Finished processing
                if (!ModelTypeClassify)
                    ModelMeasure_Finish(CurrentWell.ParentFolder);
            }

            public static string DefaultMeasureName { get; set; } = "ModelMeasure01.txt";
            public static int DefaultBuildUpCount { get; set; }
            public string TotalTimeElapsed { get; set; }

            private void Report(int CountRemaining = -1, XDCE_Image xI = null, bool bmapAvail = true, string Note = "")
            {
                if (CountRemaining < 0) CountRemaining = LastCountRemaining;
                else LastCountRemaining = CountRemaining;

                string FOV = "Inferring ", WellPlate = LastWellPlate;
                if (xI != null)
                {
                    FOV = "FOV:" + xI.FOV.ToString() + " "; WellPlate = xI.Parent.PlateID + " " + xI.WellLabel;
                    LastWellPlate = WellPlate;
                }

                var FOVFinished = (CountAll - CountRemaining);
                var TimeRunning = (DateTime.Now - startWell);
                var TimeRemaining = (CountRemaining * (TimeRunning.TotalMinutes < 0.25 ? 0.25 : TimeRunning.TotalMinutes) / FOVFinished);
                var TimeRunningTotal = (DateTime.Now - TotalStart);
                var TimeTotal = TimeRunningTotal * (CountAll * TotalWells) / (FinishedWells * CountAll + FOVFinished);
                var TimeRemainingTotal = (TimeTotal - TimeRunningTotal).TotalMinutes;
                string repLines = CountRemaining.ToString() + (bmapAvail ? " bAv " : " bLd ") + FOV;
                if (RunType == RunTypeEnum.Std)
                {
                    repLines +=
                        TFIM.Metadata.ModelName.Substring(0, Math.Min(TFIM.Metadata.ModelName.Length, 12)) + "\r\n" +
                        TimeRemaining.ToString("0.0 min left in well, ") + TimeRunning.TotalMinutes.ToString("0.0 m elapsed") + "\r\n" + ImageCheck_PointsList.Count + " Points\r\n" + WellPlate;
                }
                else
                {
                    repLines +=
                        "Classify/Measure\r\n" +
                        TimeRemaining.ToString("0.0 min left in well, ") + TimeRunning.TotalMinutes.ToString("0.0 m elapsed") + "\r\n" + WellPlate + "\r\n" +
                        TimeRemainingTotal.ToString("0 min to go") + "\r\n" + WholeRunTrack + "\r\n" + Note.Trim();
                }
                Report_QueryCancel(repLines);
            }

            private (bool Working, Bitmap croppedBitmap) CroppedBitmapFromRaft(ReturnRaft RR, XDCE_Image xI, Bitmap bmap, int CropSize, double Zoom = 1, bool equalPadding = false, float MaxFractionLostToKeep = 0.25F)
            {
                Bitmap bmapReturn;
                SizeF sz = bmap.Size;
                var r = Rectangle.Round(ReturnRaft.RectFromCorners(xI, RR, sz));

                if (Zoom > 1) { throw new NotImplementedException(); }

                int CropSizeT = (int)(CropSize / Zoom); //Account for the zoom, keeps the centering correct

                var SmallerDim = (int)Math.Min(r.Width, r.Height);
                if (SmallerDim <= 3) return (false, null);
                if (CropSizeT < 1) CropSizeT = SmallerDim;

                int MaxPadPixels = (int)(MaxFractionLostToKeep * CropSizeT); // What is the max number of pixels to allow to be padded
                if (CropSizeT > r.Width + MaxPadPixels || CropSizeT > r.Height + MaxPadPixels) return (false, null);
                int left = r.Left, top = r.Top, right = left + r.Width, bottom = top + r.Height;
                if (CropSizeT > r.Width || CropSizeT > r.Height)
                {
                    // Check if there is enough room in the original bitmap to expand the rectangle
                    if (CropSizeT - r.Width <= MaxPadPixels && CropSizeT - r.Height <= MaxPadPixels &&
                        left - (CropSizeT - r.Width) / 2 >= 0 && top - (CropSizeT - r.Height) / 2 >= 0 &&
                        right + (CropSizeT - r.Width + 1) / 2 <= bmap.Width && bottom + (CropSizeT - r.Height + 1) / 2 <= bmap.Height)
                    {
                        left -= (CropSizeT - r.Width) / 2; top -= (CropSizeT - r.Height) / 2;
                        var r2 = new Rectangle(left, top, CropSizeT, CropSizeT);
                        bmapReturn = bmap.Clone(r2, PixelFormat.Format24bppRgb);
                    }
                    else //Padding is required
                    {
                        bmapReturn = new Bitmap(CropSizeT, CropSizeT, PixelFormat.Format24bppRgb);
                        using (Graphics g = Graphics.FromImage(bmapReturn))
                        {
                            g.Clear(Color.Black);
                            int width = Math.Min(CropSizeT, r.Width), height = Math.Min(CropSizeT, r.Height);

                            // Ensure you don't go outside the bounds of the original image
                            int srcX = Math.Max(0, left), srcY = Math.Max(0, top);
                            int srcWidth = Math.Min(bmap.Width - srcX, width), srcHeight = Math.Min(bmap.Height - srcY, height);

                            // Calculate the position to draw the cropped region on the new image
                            int destX, destY;  // Set this to false for padding only on the sides where the original image doesn't exist
                            if (equalPadding) { destX = (CropSizeT - width) / 2; destY = (CropSizeT - height) / 2; }
                            else { destX = srcX == 0 ? CropSizeT - width : 0; destY = srcY == 0 ? CropSizeT - height : 0; }

                            g.DrawImage(bmap, new Rectangle(destX, destY, srcWidth, srcHeight), new Rectangle(srcX, srcY, srcWidth, srcHeight), GraphicsUnit.Pixel);
                        }
                    }
                }
                else
                {
                    var r2 = new Rectangle(left, top, CropSizeT, CropSizeT); // r.Width, r.Height);
                    bmapReturn = bmap.Clone(r2, PixelFormat.Format24bppRgb);
                }
                if (Zoom < 1) bmapReturn = new Bitmap(bmapReturn, new Size(CropSize, CropSize));
                return (true, bmapReturn);
            }

            public static (bool Working, Bitmap croppedBitmap) CroppedBitmapFromRect(Bitmap bmap, RectangleF rect)
            {
                var r2 = rect;
                var bmapMult = bmap.Clone(r2, PixelFormat.Format24bppRgb);
                return (true, bmapMult);
            }

            private bool AssignICA(XDCE_Image xI, string RaftID, float[] AllScores, float BestClassProbability, int BestClass, Dictionary<string, ImageCheck_Point> annos = null)
            {
                var TFIMUse = RunType == RunTypeEnum.Std ? TFIM : TFIM_Classify;
                var Meta = TFIMUse.Metadata; TF_Model_ClassInfo TClass = null; ImageCheck_Annotation OldAnno = null;
                var Well = xI.Parent.Wells[xI.WellLabel];
                var ICPL = Well.ImageCheck_PointsList;
                var LPt = ICPL == null ? new List<ImageCheck_Point>() : ICPL.FromDictionaryRaftID(RaftID);
                var Pt = LPt.Count == 0 ? ImageCheck_Point.FromImage(xI, RaftID) : LPt[0];
                if (Pt.PlateID == "") Pt.PlateID = xI.Parent.ParentFolder.PlateID;

                bool shouldEnterIfBlock = false;
                if (BestClassProbability < Meta.ThresholdDefault) shouldEnterIfBlock = true;
                else if (!Meta.Rebuilt_Index2ClassInfo.ContainsKey(BestClass)) shouldEnterIfBlock = true;
                else
                {
                    TClass = Meta.Rebuilt_Index2ClassInfo[BestClass];
                    if (TClass.Ignore) shouldEnterIfBlock = true;
                }
                if (shouldEnterIfBlock)
                {
                    //At least setup the previous id if it exists
                    if (Pt.Annotations.Count > 0)
                    {
                        Pt.PlateID = xI.Parent.ParentFolder.PlateID;
                        Pt.Annotations[0].DisallowSkip = true;
                        annos[RaftID] = Pt;
                    }
                    return false;
                }

                if (annos[RaftID] != null) Pt = annos[RaftID];
                else annos[RaftID] = Pt; //Copy this back to the list in case we need it

                if (SaveAnnotationBackToPointlist) { OldAnno = Pt.Annotations.Count > 0 ? Pt.Annotations[0] : null; Pt.Annotations.Clear(); } //We are just deciding to wipe out the old annotation and put a new one in
                var ICA = new ImageCheck_Annotation(); ICA.Name = Meta.ModelName;
                ICA.Note = Meta.DeriveFullAnnotation(AllScores);
                ICA.Value = TClass.Name;
                ICA.Score = BestClassProbability;
                ICA.Color = TClass.Color;
                Pt.Add(ICA);
                if (OldAnno != null)
                {
                    Pt.Annotations[0].Name = "New and Old";
                    if (OldAnno.Value.ToString().ToUpper() != Pt.Annotations[0].Value.ToString().ToUpper())
                    {
                        Pt.Annotations[0].DisallowSkip = true;
                    }
                }

                if (SaveAnnotationBackToPointlist)
                {
                    if (ImageCheck_PointsList == null) ImageCheck_PointsList = new();
                    ImageCheck_PointsList.Add(Pt);
                    if (RunType == RunTypeEnum.Classify_Measure_01) ICPL.Add(Pt);
                }
                return true;
            }

            public static void MeasureTest2(BasicSegmentatioResults res)
            {
                var Objects = res.DeOverlap; var Img = res.CombinedBMAP;
                string ExportPath = "c:\\temp\\ROI Measure\\"; if (!Directory.Exists(ExportPath)) Directory.CreateDirectory(ExportPath);
                var MeasureResults = new TF_Measure_Results(null);
                var bmps = new Dictionary<string, Bitmap>();
                var annos = new Dictionary<string, ImageCheck_Point>();

                foreach (var Crp in res.CropRectangles)
                {

                }

                int CropSize = 128;
                foreach (var Obj in Objects)
                {
                    Obj.SetExportName("jpg");
                    var (worked, bmp) = CroppedBitmapFromRect(Img, Obj.CropRect(CropSize / res.Binning, res.Binning, 2040));
                    if (worked && (bmp.Width == bmp.Height) && (bmp.Height == CropSize))
                    {
                        bmps.Add(Obj.ExportName, bmp); annos.Add(Obj.ExportName, Obj.ToICP());
                        bmp.Save(Path.Combine(ExportPath, Obj.ExportName));
                    }
                }

                var TFIM_Measures = TF_Image_Model.FromListOfPaths(ModelInferSettings.Default_MitoMeasure_Paths);
                var (y, x) = TF_Image_Model.LoadImagesToND(bmps);

                foreach (var TFMm in TFIM_Measures)
                {
                    var sTime = DateTime.Now;
                    var (NDresM, _) = TFMm.Predict(x);
                    var (res2m, _, _) = TF_Image_Model.NDArrayConvert(NDresM);
                    var milliPredict_M = (DateTime.Now - sTime).TotalMilliseconds;
                    MeasureResults.AddSet(TFMm, y, res2m, annos, null, null, 0, milliPredict_M);
                }

                MeasureResults.Export(ExportPath);
            }

            public void DeriveAnnotationsFromModel(XDCE_Image xI, Bitmap bmap, int CropSize, double Zoom = 1, double rescaleDivisor = 1, double milliLoad = double.NaN)
            {
                if (xI != null) //For the last time it is going to be run
                {
                    //Get the cropped bitmaps
                    var (bmps1, milliCrop1) = GetCroppedBMPs(xI, bmap, CropSize, Zoom, RunType != RunTypeEnum.Classify_Measure_01);
                    if (bmps1.Count == 0) return; //None are left (should be caught in the previous step)

                    foreach (var key in bmps1.Keys)
                        BuildUpCropped.TryAdd(key, (xI, bmps1[key].bmps, bmps1[key].annos, bmps1[key].skips, milliLoad, milliCrop1 / bmps1.Count));

                    //But don't run the rest of the processing unless
                    if (BuildUpCropped.Count < BuildUpCroppedCount) return;
                }
                //Get the big set
                var xIs = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.xI);
                var bmps = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.bmp);
                var annos = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.anno);
                var skips = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.skip);
                var milliLoads = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.milliLoad);
                var milliCrops = BuildUpCropped.ToDictionary(kvp => kvp.Key, kvp => kvp.Value.milliCrop);

                //int count = BuildUpCropped.Count;
                //var xIs = new Dictionary<string, XDCE_Image>(count);
                //var bmps = new Dictionary<string, Bitmap>(count);
                //var annos = new Dictionary<string, ImageCheck_Point>(count);
                //var skips = new Dictionary<string, bool>(count);
                //var milliLoads = new Dictionary<string, double>(count);
                //var milliCrops = new Dictionary<string, double>(count);
                //foreach (var kvp in BuildUpCropped)
                //{
                //    var a = kvp.Value;
                //    xIs.Add(kvp.Key, a.xI); bmps.Add(kvp.Key, a.bmp); annos.Add(kvp.Key, a.anno); skips.Add(kvp.Key, a.skip);
                //    milliLoads.Add(kvp.Key, a.milliLoad); milliCrops.Add(kvp.Key, a.milliCrop);
                //}

                //Load the images into memory and predict
                var sTime = DateTime.Now;
                var (y, x) = TF_Image_Model.LoadImagesToND(bmps, rescaleDivisor);
                var (NDres, NDimgs) = TFIM.Predict(x);
                var (res2, maxPer, maxVal) = TF_Image_Model.NDArrayConvert(NDres);
                var milliPredict = (DateTime.Now - sTime).TotalMilliseconds;

                // - - To Check the BMPs
                //foreach (var KVP in bmps) KVP.Value.Save(@"c:\temp\check"+KVP.Key+".bmp");

                if (ModelTypeClassify)
                { //Classification Processing
                    for (int i = 0; i < y.Length; i++) AssignICA(xIs[y[i]], y[i], TF_Image_Model.Slice(res2, i), maxVal[i], maxPer[i], annos);
                }
                else
                { //Measurement Processing
                    MeasureResults.AddSet(TFIM, y, res2, annos, milliLoads, milliCrops, milliPredict);
                }

                PostSave_Dispose(bmps, annos, y, x, null, new Dictionary<string, NDArray[]>() { { TFIM.ModelNameSafe, NDimgs } }, new Dictionary<string, NDArray[]>() { { TFIM.ModelNameSafe, NDres } });
            }

            public void DeriveAnnotationsFromModel_Measure(XDCE_Image xI, Bitmap bmap, int CropSize, double Zoom = 1, double rescaleDivisor = 1, double milliLoad = double.NaN)
            {
                if (xI != null) //For the last time it is going to be run
                {
                    //Get the cropped bitmaps
                    var (bmps1, milliCrop1) = GetCroppedBMPs(xI, bmap, CropSize, Zoom, RunType != RunTypeEnum.Classify_Measure_01);
                    if (bmps1.Count == 0) { return; } //None are left (should be caught in the previous step)

                    foreach (var key in bmps1.Keys)
                        BuildUpCropped.TryAdd(key, (xI, bmps1[key].bmps, bmps1[key].annos, bmps1[key].skips, milliLoad, milliCrop1 / bmps1.Count));

                    //But don't run the rest of the processing unless
                    if (BuildUpCropped.Count < BuildUpCroppedCount) return;
                }
                //Get the big set
                int count = BuildUpCropped.Count;
                var xIs = new Dictionary<string, XDCE_Image>(count);
                var bmps = new Dictionary<string, Bitmap>(count);
                var annos = new Dictionary<string, ImageCheck_Point>(count);
                var skips = new Dictionary<string, bool>(count);
                var milliLoads = new Dictionary<string, double>(count);
                var milliCrops = new Dictionary<string, double>(count);
                foreach (var kvp in BuildUpCropped)
                {
                    var k = kvp.Key; var a = kvp.Value; 
                    xIs.Add(k, a.xI); bmps.Add(k, a.bmp); annos.Add(k, a.anno); skips.Add(k, a.skip); milliLoads.Add(k, a.milliLoad); milliCrops.Add(k, a.milliCrop);
                }
                Report(Note:"Inferring on " + skips.Count(x => x.Value == false) + "/" + count);

                DateTime sTime; double milliPredict_C = 0; double milliSubset_M; double milliPredict_M; NDArray xs = null; string[] ys2 = null; NDArray[] NDimgs = null, NDresM = null;
                var PredSegImgs = new Dictionary<string, NDArray[]>(); var PredMeasures = new Dictionary<string, NDArray[]>();

                //Load the images into memory and predict
                sTime = DateTime.Now;
                var (y, x) = TF_Image_Model.LoadImagesToND(bmps, rescaleDivisor);
                var milliLoadIntoNDArrays = (DateTime.Now - sTime).TotalMilliseconds;
                
                //Get rid of any green channel
                if (StripGreen)
                {
                    x[Slice.All, Slice.All, 1] = 0f;
                }

                //Classify
                if (MIS.cmPerformClassifications)
                {
                    if (TFIM_Classifies.Count == 1 && TFIM_Classify == null) TFIM_Classify = TFIM_Classifies[0];
                    if (TFIM_Classify != null)
                    {
                        sTime = DateTime.Now;
                        var (NDresC, _) = TFIM_Classify.Predict(x);
                        var milliPredictTF = (DateTime.Now - sTime).TotalMilliseconds;
                        var (res2, maxPer, maxVal) = TF_Image_Model.NDArrayConvert(NDresC);
                        for (int i = 0; i < y.Length; i++)
                        {
                            AssignICA(xIs[y[i]], y[i], TF_Image_Model.Slice(res2, i), maxVal[i], maxPer[i], annos);
                            if (annos[y[i]] == null) skips[y[i]] = true;
                            else (skips[y[i]], _) = CanRaftBeAnalyzed(y[i], annos[y[i]]);
                        }
                        milliPredict_C = (DateTime.Now - sTime).TotalMilliseconds;
                    }
                    if (TFIM_Classifies.Count > 1)
                    {
                        sTime = DateTime.Now;
                        foreach (var TFMc in TFIM_Classifies)
                        {
                            TFIM_Classify = TFMc;
                            sTime = DateTime.Now;
                            var (NDresC, _) = TFMc.Predict(x);
                            var (res2, maxPer, maxVal) = TF_Image_Model.NDArrayConvert(NDresC);
                            milliPredict_M = (DateTime.Now - sTime).TotalMilliseconds;
                            //Now we want to save out examples of all of those in subfolders with the model names
                            string tSave = Path.Combine(MIS.svImageFolder, TFMc.ModelName);
                            if (!Directory.Exists(tSave)) Directory.CreateDirectory(tSave);
                            for (int i = 0; i < y.Length; i++)
                            {
                                //AssignICA(xIs[y[i]], y[i], TF_Image_Model.Slice(res2, i), maxVal[i], maxPer[i], annos);
                                string tAnno = Path.Combine(tSave, maxPer[i].ToString());
                                if (!Directory.Exists(tAnno)) Directory.CreateDirectory(tAnno);
                                bmps[y[i]].Save(Path.Combine(tAnno, y[i] + ".jpg"));
                            }
                        }
                    }
                }

                if (TFIM_Measures.Count > 0 && MIS.cmPerformMeasures)
                {
                    //Prep Subset
                    sTime = DateTime.Now;
                    if (skips.Count(x => x.Value == true) == 0)
                    { xs = x; ys2 = y; } //None to skip
                    else
                    {
                        xs = new NDArray(new Shape(0, x.shape[1], x.shape[2], x.shape[3]), x.dtype);
                        var ys = new List<string>();
                        for (int i = 0; i < y.Length; i++)
                        {
                            string key = y[i];
                            if (skips.ContainsKey(key) && !skips[key])
                            {
                                NDArray row = x[new Slice(i, i + 1), ":", ":", ":"];
                                xs = np.concatenate(new NDArray[] { xs, row }, axis: 0);
                                ys.Add(y[i]);
                            }
                        }
                        ys2 = ys.ToArray();
                        var dims = xs.shape;
                    }
                    milliSubset_M = (DateTime.Now - sTime).TotalMilliseconds;

                    if (ys2.Length > 0)
                    {
                        //Measure
                        foreach (var TFMm in TFIM_Measures)
                        {
                            sTime = DateTime.Now;
                            (NDresM, NDimgs) = TFMm.Predict(xs);
                            (float[,] res2m, int[] _, float[] _) = TF_Image_Model.NDArrayConvert(NDresM);
                            milliPredict_M = (DateTime.Now - sTime).TotalMilliseconds;
                            MeasureResults.AddSet(TFMm, ys2, res2m, annos, milliLoads, milliCrops, milliPredict_C, milliPredict_M);
                            PredSegImgs.Add(TFMm.ModelNameSafe, NDimgs); PredMeasures.Add(TFMm.ModelNameSafe, NDresM);
                        }
                    }
                }

                Report(Note: "Saving " + ys2.Length + " Sets of Images . . (slow)");
                PostSave_Dispose(bmps, annos, ys2, xs, x, PredSegImgs, PredMeasures, xI?.Parent.PlateID);
            }

            private void PostSave_Dispose(Dictionary<string, Bitmap> BMPsToSave, Dictionary<string, ImageCheck_Point> annos, string[] y, NDArray TensorXtoDispose, NDArray TensorXStoDispose = null, Dictionary<string, NDArray[]> PredSegmentedImages = null, Dictionary<string, NDArray[]> PredMeasures = null, string MetaName = "")
            {
                if (MIS.SaveImagesWhileInferring) if (!Directory.Exists(MIS.svImageFolder)) Directory.CreateDirectory(MIS.svImageFolder);

                if (PredSegmentedImages != null && PredSegmentedImages.Count > 0) 
                    foreach (var modelName in PredSegmentedImages.Keys) {
                        if (PredSegmentedImages[modelName] == null) continue;
                        if (PredSegmentedImages[modelName].Length > 0)
                            TF_Image_Model.SaveNDArrSetToImages(PredSegmentedImages[modelName], Path.Combine(MIS.svImageFolder, modelName), y, TensorXtoDispose, BMPsToSave, PredMeasures[modelName], false, MetaName:MetaName);
                    }
                string svName;
                //Save and then Dispose all bitmaps
                foreach (var bmp in BMPsToSave)
                {
                    if (MIS.SaveImagesWhileInferring)
                    {
                        string Anno = "NA";
                        if (annos.ContainsKey(bmp.Key))
                        {
                            List<ImageCheck_Annotation> AnnoList = annos[bmp.Key].Annotations;
                            if (AnnoList.Count > 0)
                                Anno = AnnoList[svMultiTargetIdx].Value.ToString();
                        }
                        if (Anno == "NA") continue;
                        var DP = MeasureResults.GetDataPoint(bmp.Key);
                        string Meas = DP != null ? DP.data[0].ToString("000.00") : "";
                        svName = (Meas + " " + Anno + " " + annos[bmp.Key].PlateID + " " + bmp.Key).Trim();
                        Bitmap Resize = bmp.Value;
                        //Resize = new Bitmap(bmp.Value, new Size(128, 128));
                        Resize.Save(Path.Combine(MIS.svImageFolder, svName + MIS.svImageExt));
                    }
                    bmp.Value.Dispose();
                }
                if (TensorXtoDispose != null) TensorXtoDispose.Dispose();
                if (TensorXStoDispose != null) TensorXStoDispose.Dispose(); //Dispose the ND arrays
                foreach (var KVP in PredSegmentedImages) if (KVP.Value != null) foreach (var image in KVP.Value) image.Dispose();
                BuildUpCropped = new(); //Resest this after processing
            }

            private (Dictionary<string, (Bitmap bmps, ImageCheck_Point annos, bool skips)> bmpDict, double milliCrop) GetCroppedBMPs(XDCE_Image xI, Bitmap bmap, int CropSize, double Zoom = 1, bool actuallySkip = true)
            {
                var sTime = DateTime.Now;
                var bmps = new Dictionary<string, (Bitmap bmps, ImageCheck_Point annos, bool skips)>();

                if (bmap.PixelFormat == PixelFormat.DontCare)
                {   //Means there is an error with the bitmap or it doesn't exist
                    Debug.Print("Have to reload BMP?");

                    (bmap, _) = ImgQueue.CombinedBMAP(CurrentWell, xI, CurrentWVParams);
                }

                if (UseRafts)
                {
                    RefreshRafts(xI); // Setup the rafts

                    foreach (var RSet in xI.Rafts)
                    {
                        // First determine whether this raft is able to be analyzed
                        (var skip, var currentAnnotation) = CanRaftBeAnalyzed(RSet.Key);
                        //if (RSet.Key == "D2T6" || RSet.Key == "F7T2" || RSet.Key == "C0R7") {}
                        if (skip && actuallySkip) continue;

                        // Then get the cropped image from the raft (or region)
                        var (worked, bmapMult) = CroppedBitmapFromRaft(RSet.Value.RR, xI, bmap, CropSize, Zoom);
                        if (worked)
                        {
                            if (bmapMult.Width != CropSize || bmapMult.Height != CropSize) { Debugger.Break(); } //Should get to this, right?
                            bmps.Add(RSet.Key, (bmapMult, currentAnnotation, skip));
                        }
                    }
                }
                else
                {
                    var res = BasicSegmentatioResults.BasicSegmentation(CIS, CurrentWVParams, xI, ImgQueue, 4, false, false, false);
                    foreach (var Obj in res.DeOverlap)
                    {
                        Obj.SetExportName("jpg");
                        var (worked, bmp) = CroppedBitmapFromRect(res.CombinedBMAP, Obj.CropRect((float)CropSize / res.Binning, res.Binning, res.CombinedBMAP.Width - 1));
                        if (worked && (bmp.Width == bmp.Height) && (bmp.Height == CropSize))
                        {
                            bmps.Add(Obj.ExportName, (bmp, Obj.ToICP(CropSize), false));
                            //bmp.Save(Path.Combine(@"C:\Temp\ROI Measure", Obj.ExportName));
                        }
                    }
                }
                return (bmps, (DateTime.Now - sTime).TotalMilliseconds);
            }

            /// <summary>
            /// Returns a sorted list, the top entry is the most common
            /// </summary>
            public static List<Size> GetCropRectangles(XDCE_Image xI, double MaxOverMinRatio = 1.1)
            {
                RefreshRafts(xI);

                ConcurrentDictionary<Size, int> sizeCounts = new ConcurrentDictionary<Size, int>();
                SizeF sF = new SizeF(xI.Width_Pixels, xI.Height_Pixels);

                Parallel.ForEach(xI.Rafts, RSet =>
                {
                    RectangleF r = ReturnRaft.RectFromCorners(xI, RSet.Value.RR, sF);

                    float w = r.Width;
                    float h = r.Height;
                    float bigger;
                    float smaller;
                    if (w < h)
                    {
                        bigger = h;
                        smaller = w;
                    }
                    else
                    {
                        bigger = h;
                        smaller = w;
                    }

                    if (bigger / smaller > MaxOverMinRatio)
                        return;

                    Size size = new Size((int)w, (int)h);

                    // First, try to add the key

                    if (!sizeCounts.TryAdd(size, 1))
                    {
                        sizeCounts.AddOrUpdate(size, 1, (_, currentValue) => currentValue + 1);
                    }

                });
                // Retrieve the most common size from the stack

                List<Size> results = new List<Size> { sizeCounts.MaxBy(t => t.Value).Key };

                return results;
            }
            private bool CanAnyRaftsBeAnalyzed(XDCE_Image xI)
            {
                RefreshRafts(xI);

                foreach (var RSet in xI.Rafts)
                {
                    (var skip, var _) = CanRaftBeAnalyzed(RSet.Key);
                    if (!skip) return true;
                }
                return false;
            }

            public static void RefreshRafts(XDCE_Image xI)
            {
                var CurrentWell = xI.Parent.Wells[xI.WellLabel];
                int count = xI.Rafts.Count;
                if (!CurrentWell.HasRaftCalibration || count > 0) return; //Means there are regions

                if (xI.RaftCalCode != CurrentWell.CalibrationRaftSettings.CalCode)
                    xI.Rafts = new Dictionary<string, xI_RaftInfo>();
                if (count == 0) 
                    xI.RefreshRafts(CurrentWell, null);
            }

            private (bool skip, ImageCheck_Point ICP) CanRaftBeAnalyzed(string RaftID, ImageCheck_Point ICPUse = null)
            {
                string CurrentAnnotation = ""; ImageCheck_Point ICP = ICPUse;
                if (ICP == null)
                {
                    if (CurrentWell.ImageCheck_PointsList == null)
                    {
                        return (false, ICP);
                    }
                    else
                    {
                        var lICP = CurrentWell.ImageCheck_PointsList.FromDictionaryRaftID(RaftID);
                        if (lICP.Count != 0) ICP = lICP[0];
                    }
                }
                else
                {
                    if (ICP.Annotations[0].DisallowSkip) return (false, ICP); //Used to force certain annotations (previous ones) to get analyzed
                }
                if (ICP == null) CurrentAnnotation = "";
                else CurrentAnnotation = ICP.Annotations[0].Value.ToString();

                switch (RunType == RunTypeEnum.Std ? MIS.ExistingAnnotations : 2)
                {
                    case 1:
                        break;
                    case 2:
                        if (CurrentAnnotation == "") return (true, ICP);
                        var OnlyExisting = (!ModelTypeClassify || RunType == RunTypeEnum.Classify_Measure_01) ? MIS.Measure_OnlyExistingByNameList : new List<string>();
                        foreach (string Anno in OnlyExisting) {
                            if (CurrentAnnotation.ToUpper() == Anno) { return (false, ICP); }
                        }
                        return (true, ICP);
                    case 3:
                        if (CurrentAnnotation != "") return (true, ICP);
                        break;
                    default:
                        break;
                }

                return (false, ICP);
            }

            internal string ModelMeasure_Finish(INCELL_Folder folder)
            {
                return ModelMeasure_Finish(folder.FullPath);
            }

            internal string ModelMeasure_Finish(string OutputFolder)
            {
                if (MeasureResults.DataPoints.Count <= 0) return "";
                return MeasureResults.Export(OutputFolder, DefaultMeasureName);
            }

        }
    }
}
