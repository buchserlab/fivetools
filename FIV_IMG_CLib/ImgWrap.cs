﻿using CoreImageLoader;
using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;


namespace FIVE_IMG
{
    //1/24/2024 this is the only function that currently references ImgWrap outside of SharedUtils . . 
    //FIVE_IMG.Seg.ThreshImage.CreateFrom()
    //Create from needs to have the 'classic' function of mean, not the dark mean. I think Dark Mean is something distinct from mean, so should probably have a different name

    //FIVE_IMG.PL.PLDeployModule - Also uses it, but this whole module is deprecated (still good to keep the code around for now, but there is no more PL)

    public class ImgWrap
    {
        private Bitmap _BMP;
        public Bitmap BMP
        {
            get
            {
                if (_BMP == null)
                {
                    _BMP = new Bitmap(PathToImage);
                }
                return _BMP;
            }
            set { _BMP = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public string PathToImage { get; internal set; }

        public int Width { get; set; }
        public int Height { get; set; }


        private byte _RedMin;
        private byte _RedMax;
        private double _RedMean;
        private double _RedStDev;

        public int RedMin
        {
            get
            {
                //if there is no array TakeApart theBitmap
                if (_arr == null)
                    TakeApart();
                //iterate through the array and grab the min value
                return _RedMin;
            }
        }

        public int RedMax
        {
            get
            {
                //if there is no array TakeApart theBitmap
                if (_arr == null)
                    TakeApart();
                //iterate through the array and get the max value
                return _RedMax;
            }
        }
        public double RedMean { get { if (_arr == null) TakeApart(); return _RedMean; } }
        public double RedStDev { get { if (_arr == null) TakeApart(); return _RedStDev; } }

        public float X_um = float.NaN;
        public float Y_um = float.NaN;
        public PointF Location_um { get => new PointF(X_um, Y_um); }
        public float Z = float.NaN;
        public float umPerpixel = float.NaN;
        public string channel = "";

        private byte[,] _arr;
        public byte[,] Arr
        {
            get
            {
                if (_arr == null)
                {
                    Width = BMP.Width; 
                    Height = BMP.Height;
                    TakeApart();
                    //TakeApart_Fast();
                }
                return _arr;
            }
        }
        private double? _DarkMean = null;
        private readonly object _lock = new object();
        public double DarkMean
        {
            get
            {
                if (_arr == null)
                    TakeApart();
                //probably don't need this but we have a lot of concurrency now.
                lock (_lock)
                {
                    if (_DarkMean == null)
                        _DarkMean = CalculateDarkMean(_arr);
                }

                return _DarkMean.Value;
            }
        }
        public double TotalPixels { get => Width * Height; }
        public float X_umCenter
        {
            get => X_um - (Width_um / 2);
        } //5x should be 2650 um / 682 pixels
        public float Y_umCenter
        {
            get => Y_um - (Height_um / 2);
        }
        public PointF Center_um { get => new PointF(X_umCenter, Y_umCenter); }
        public float Width_um { get => umPerpixel * Width; }
        public float Height_um { get => umPerpixel * Height; }

        public double NormColorAtPoint(int x, int y)
        {
            return ((Arr[x, y] - RedMean) / RedStDev);
        }

        private string _Name = string.Empty;
        public string Name
        {
            get
            {
                if (_Name == string.Empty) _Name = PathToImage;
                return _Name;
            }
            set { _Name = value; }
        }

        public ImgWrap()
        {
            _BMP = null;
            _arr = null;
        }

        public ImgWrap(Bitmap bitMAP)
        {
            BMP = bitMAP;
            Height = BMP.Height;
            Width = BMP.Width;
        }

        public ImgWrap(string FullFileName, Bitmap bitMAP)
        {
            BMP = bitMAP;
            PathToImage = FullFileName;
            Height = BMP.Height;
            Width = BMP.Width;
        }

        public ImgWrap(Bitmap bmp, float umPerPx)
        {
            BMP = bmp;
            umPerpixel = umPerPx;
            Height = BMP.Height;
            Width = BMP.Width;
        }
        /// <summary>
        /// When using this constructor we try to figure out X Y Z from the filename
        /// </summary>
        /// <param name="PathToImage"></param>
        public ImgWrap(string PathToImage)
        {
            this.PathToImage = PathToImage;
            //_BMP = new Bitmap(PathToImage);
            string extra;
            string tName;
            GetXYZFromName(PathToImage, out tName, out X_um, out Y_um, out Z, out umPerpixel, out channel, out extra);
            Name = tName;
            Height = BMP.Height;
            Width = BMP.Width;
        }

        public static void GetXYZFromName(string PathToImage, out string Name, out float x, out float y, out float z, out float umPerpix, out string chx, out string extra)
        {
            x = y = z = umPerpix = float.NaN; extra = chx = "";
            Name = Path.GetFileNameWithoutExtension(PathToImage);
            string[] Arr = Name.Split('_');
            try
            {
                x = float.Parse(Arr[0]);
                y = float.Parse(Arr[1]);
                z = float.Parse(Arr[2]);
                umPerpix = float.Parse(Arr[3]);
                chx = Arr[4];
                extra = Arr[5];
            }
            catch { }
        }

        //Valid Extensions for Bitmap
        public static HashSet<string> supportedImageFormats = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        { ".bmp",".jpg",".jpeg", ".jfif",".jpe", ".png", ".gif",".tif", ".tiff",".ico", ".webp",  ".heif",".heic", ".wmf",".emf", };
        public static bool IsImage(string FullPath)
        {
            string Ext = Path.GetExtension(FullPath).Trim();
            return supportedImageFormats.Contains(Ext);
        }

        public static string GetMimeType(Image i)
        {
            foreach (ImageCodecInfo codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == i.RawFormat.Guid)
                    return codec.MimeType;
            }

            return "image/unknown";
        }

        private void TakeApart()
        {
            //On a 2040x2040 image, the Fast Method takes 575 ms (avg of 3), and the slow method takes 2460 ms (avg 2)
            //TakeApart_Fast();
            CoreLogic.TakeApart_Instantly(BMP, out _arr, out _RedMean, out _RedStDev, out _RedMax, out _RedMin);
            //TakeApart_Much_Much_Faster(BMP, 90);  //9/14/2023 testing, so far not faster . . but may need to test on larger images
        }

        public double CalculateDarkMean(byte[,] image)
        {
            var percentageToRemove = 0.2;
            // Flatten the image into a list
            List<byte> values = new List<byte>((int)image.LongLength);

            for (int i = 0; i < image.GetLength(0); i++)
            {
                for (int j = 0; j < image.GetLength(1); j++)
                {
                    values.Add(image[i, j]);
                }
            }

            // Sort the list
            values.Sort();

            // Calculate the number of values to remove from each end
            int totalToRemove = (int)(values.Count * percentageToRemove);
            int toRemoveEachEnd = totalToRemove / 2;

            // Remove the specified percentage of values from the top and bottom
            //values = values.Skip(toRemoveEachEnd).Take(values.Count - totalToRemove).ToList();
            // This takes them all from the beginning
            values = values.Take(values.Count - totalToRemove).ToList();
            // Compute the mean and standard deviation of the remaining values
            double mean = values.Average(t => (double)t);
            return mean;
        }

        /// <summary>
        /// Returns the middle part of the image where the size of the image is base on the fraction of the original's width
        /// </summary>
        internal ImgWrap MiddleCrop(double FractionOfWidth)
        {
            int Size = (int)(Width * FractionOfWidth);
            int Top = (Height / 2) - (Size / 2);
            int Left = (Width / 2) - (Size / 2);

            Rectangle R = new Rectangle(Top, Left, Size, Size);
            //System.Diagnostics.Debug.Print(R.Location.ToString());

            Bitmap BMPCrop = this.BMP.Clone(R, BMP.PixelFormat);

            ImgWrap IMCrop = new ImgWrap(BMPCrop);
            IMCrop.PathToImage = this.PathToImage;
            IMCrop.X_um = X_um + Left;
            IMCrop.Y_um = Y_um + Top;
            IMCrop.Z = Z;

            return IMCrop;
        }

        public double DistanceFrom(ImgWrap ImageCompare, bool IgnoreZ = true)
        {
            // Compute squared differences for X and Y
            float deltaX = X_um - ImageCompare.X_um;
            float deltaY = Y_um - ImageCompare.Y_um;
            float distanceSquared = deltaX * deltaX + deltaY * deltaY;

            if (!IgnoreZ)
            {
                // Include Z-axis in the calculation
                float deltaZ = Z - ImageCompare.Z;
                distanceSquared += deltaZ * deltaZ;
            }

            // Return the square root of the total
            return Math.Sqrt(distanceSquared);
        }


        /// <summary>
        /// The outer most array is empty, but inside of that is the x,y image and the 3 color channels
        /// </summary>
        /// <returns></returns>
        public short[][][][] ArrayForPL()
        {
            int x = Arr.GetLength(0);
            int y = Arr.GetLength(1);
            short[][][][] tempArray = new short[1][][][];
            tempArray[0] = new short[x][][];
            for (int i = 0; i < x; i++)
            {
                tempArray[0][i] = new short[y][];
                for (int j = 0; j < y; j++)
                {
                    tempArray[0][i][j] = new short[3];
                    tempArray[0][i][j][0] = Arr[j, i];
                    tempArray[0][i][j][1] = Arr[j, i];
                    tempArray[0][i][j][2] = Arr[j, i];
                }
            }
            return tempArray;
        }
    }

}
