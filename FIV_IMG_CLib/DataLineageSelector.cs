﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIV_IMG_CLib
{
    public partial class DataLineageSelector : Form
    {
        public DLCatalog Catalog;
        public string SelectedExperiment;
        public string SelectedPath;
        public List<string> BackgroundList;

        public DataLineageSelector(DLCatalog Catalog)
        {
            this.Catalog = Catalog;
            InitializeComponent();
        }

        public DataLineageSelector()
        {
            InitializeComponent();
        }

        private void DataLineageSelector_Load(object sender, EventArgs e)
        {
            txBx_CatalogLocation.Text = Catalog.Folder;
            txBx_Search.Text = "";
            BackgroundList = Catalog.Select(x => x.LineEntry).ToList();
            UpdateList();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            DLCatalogEntry CE = Catalog.GetEntryByLine(listBox_Main.SelectedItem.ToString());
            SelectedExperiment = CE.ExpName;
            SelectedPath = CE.Path;
            Close();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txBx_Search_TextChanged(object sender, EventArgs e)
        {
            UpdateList();
        }

        private void UpdateList()
        {
            listBox_Main.Items.Clear();
            IEnumerable<string> tStr = BackgroundList.Where(x => x.ToUpper().Trim().Contains(txBx_Search.Text.ToUpper().Trim()));
            foreach (var item in tStr)
                listBox_Main.Items.Add(item);
        }

        private void listBox_Main_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox_Main_DoubleClick(object sender, EventArgs e)
        {
            btn_Load_Click(sender, e);
        }
    }
}
