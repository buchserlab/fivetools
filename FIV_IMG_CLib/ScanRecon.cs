﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using FIVE.InCellLibrary;
using ImageMagick;
using System.IO;
using System.Xml.Linq;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Threading;
using System.Security.Cryptography.Xml;
using System.Net.Http;
using System.Windows.Forms;
using System.ComponentModel;
using System.Text;
using Tensorflow;
using System.Security.Cryptography.X509Certificates;

namespace FIVE_IMG
{
    public class INCELL_Folder_Reconstruct
    {
        public INCELL_Folder ICFolder_Orig;
        public INCELL_MeasuredOffsets MeasuredOffsets => ICFolder_Orig.XDCE.MeasuredOffsets;
        public float Magnification_New;
        public float Magnification_Original;
        public float FractionFilledToInclude;
        public string AppendToName_PipeIsNewMag;
        public INCELL_Folder ICFolder_New;
        public Dictionary<(string WellLabel, int OldFOV), List<Alignment_PerFOV>> FOVLookUp = new();

        private DateTime ScanStart;
        private DateTime WellStart;
        public bool MemoryOnly = false; //Used to reconstruct the positions but not actually any images

        private Font oFont;
        private Font tFont;
        private Pen oPen;
        private Pen nPen;
        private Brush nBrush;

        public static bool DebugMode = false;
        public static BackgroundWorker _BW_Current;
        public static bool BWReport(string Message, float Fraction = 0F)
        {
            if (_BW_Current == null)
            {
                Debug.Print(Message);
                return false;
            }
            else
            {
                _BW_Current.ReportProgress((int)(100 * Fraction), Message);
                return _BW_Current.CancellationPending;
            }
        }

        public static void PerformReconstruction(INCELL_Folder folderToPass, INCELL_MeasuredOffsets measuredOffsets, float newMagnification, string associatedTabularFile, float fractionFilledToInclude, string appendToName_PipeIsNewMag, BackgroundWorker BW = null)
        {
            //newMagnification = 12; fractionFilledToInclude = 0.1F;
            DebugMode = false; _BW_Current = BW;
            var Recon = new INCELL_Folder_Reconstruct(folderToPass, measuredOffsets, newMagnification, fractionFilledToInclude, appendToName_PipeIsNewMag);
            Recon.Reconstruct_Scan();
            Recon.Reconstruct_AssociatedTabularFile(associatedTabularFile);
            BWReport("Done!");
        }

        private void SetupOutlines()
        {
            oFont = new Font("Arial", 20);
            tFont = new Font("Arial", 40);
            oPen = new Pen(Color.FromArgb(50, Color.Magenta), 6);
            nPen = new Pen(Color.FromArgb(140, Color.Green), 3);
            nBrush = new SolidBrush(Color.FromArgb(20, Color.Green));
        }

        public INCELL_Folder_Reconstruct(INCELL_Folder folderToPass, INCELL_MeasuredOffsets measuredOffsets, float newMagnification, float fractionFilledToInclude, string appendToName_PipeIsNewMag, bool MemoryOnly = false)
        {
            this.MemoryOnly = MemoryOnly;
            ICFolder_Orig = folderToPass;
            ICFolder_Orig.XDCE.MeasuredOffsets = measuredOffsets == null ? new() : measuredOffsets; //Important, this needs to be done BEFORE the plateX and plateY are calculated . . perhaps settings this can cause a re-calulation?

            Magnification_New = newMagnification;
            Magnification_Original = folderToPass.XDCE.ObjectiveMagnification;
            FractionFilledToInclude = fractionFilledToInclude;
            AppendToName_PipeIsNewMag = appendToName_PipeIsNewMag;

            //Start getting new folder ready
            ICFolder_New = INCELL_Folder.CopyFrom(ICFolder_Orig, ICFolder_Orig.PlateID + appendToName_PipeIsNewMag.Replace("|", Magnification_New.ToString() + "x"), Magnification_New, MemoryOnly);

            SetupOutlines();
        }

        public void Reconstruct_Scan()
        {
            ScanStart = DateTime.Now;
            FOVLookUp = new();
            foreach (var well in ICFolder_Orig.XDCE.Wells.Values)
            {
                WellStart = DateTime.Now;
                BWReport("Starting well " + well.NameAtLevel);
                Reconstruct_Well(well);
                BWReport((DateTime.Now - WellStart).TotalSeconds.ToString("0") + " Seconds for " + well.NameAtLevel);
                if (DebugMode) break;
            }
            if (MemoryOnly) return;
            ICFolder_New.XDCE.SaveFile();
        }

        public void Reconstruct_Well(XDCE_ImageGroup WellOrig)
        {
            int SizePerOriginalMagTile = (int)(Magnification_New * WellOrig.Images[0].Height_Pixels / Magnification_Original);
            bool Include20xThumb = false; bool OutlinesOnly = false;
            double Ratio = (double)SizePerOriginalMagTile / WellOrig.Images[0].Height_microns;
            double MinX = WellOrig.X_to_Index.Keys[0], MaxX = WellOrig.X_to_Index.Keys[^1] + WellOrig.Images[0].Width_microns;
            double MinY = WellOrig.Y_to_Index.Keys[0], MaxY = WellOrig.Y_to_Index.Keys[^1] + WellOrig.Images[0].Height_microns;

            var WellNew = XDCE_ImageGroup.CopyExisting(WellOrig, ICFolder_New, Magnification_New);
            ICFolder_New.XDCE.AddWellClean(WellNew);

            var bmp = new Bitmap(1 + (int)((MaxX - MinX) * Ratio), 1 + (int)((MaxY - MinY) * Ratio));

            using (var g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.LightYellow);
                for (int FOV = WellOrig.FOV_Min; FOV < WellOrig.FOV_Max; FOV++)
                {
                    var xISet = WellOrig.GetFields(FOV);
                    int placeX = (int)((xISet[0].PlateX_um - MinX) * Ratio);
                    int placeY = (int)((xISet[0].PlateY_um - MinY) * Ratio);
                    Rectangle rect = new Rectangle(placeX, placeY, SizePerOriginalMagTile, SizePerOriginalMagTile);

                    if (Include20xThumb)
                    {
                        (var mI, var bmap) = FIVE_IMG.mIQueue.GetMImage(xISet[0].FullPath, 20, 2600);
                        Debug.Print(FOV.ToString() + " of " + WellOrig.FOV_Max);
                        g.DrawImage(bmap, rect);
                    }
                    else
                    {
                        g.FillRectangle(Brushes.LightSteelBlue, rect);
                    }
                    g.DrawRectangle(oPen, rect);
                    g.DrawString(FOV.ToString(), oFont, Brushes.Magenta, placeX + 4, placeY + 4);
                }
            }
            BWReport("Made first Image with Outlines");

            // New FOVs ---------------------------------

            var newFOVs_xIs = GenerateNewFOVPattern(WellOrig, WellOrig.Images[0].Width_microns, WellOrig.Images[0].Height_microns, MinX, MaxX, MinY, MaxY);
            int SizePerNewMagTile = WellOrig.Images[0].Height_Pixels; // (int)(Magnification_Original * SizePerOriginalMagTile / Magnification_New); //If we are using a non-standard SzePerOriginal size, then use this

            using (var g = Graphics.FromImage(bmp))
            {
                for (int idx = 0; idx < newFOVs_xIs.Count; idx++)
                {
                    var newFOV = newFOVs_xIs[idx].newFOV;
                    var relXIs = newFOVs_xIs[idx].relatedXIs;

                    int placeX = (int)((newFOV.X - MinX) * Ratio);
                    int placeY = (int)((newFOV.Y - MinY) * Ratio);
                    var rectOnDisplay = new Rectangle(placeX, placeY, SizePerNewMagTile, SizePerNewMagTile);

                    Bitmap Example = null;
                    for (short wv = 0; wv < WellOrig.Wavelength_Count; wv++)
                    {
                        BWReport("Work on wavelength " + (wv + 1) + "/" + WellOrig.Wavelength_Count + " in FOV " + idx.ToString() + "/" + newFOVs_xIs.Count);
                        var tBMP = Reconstruct_FOV(idx, wv, newFOV, relXIs, Ratio, SizePerOriginalMagTile, SizePerNewMagTile, WellNew);
                        if (wv == 0) Example = tBMP;
                    }
                    if (!MemoryOnly)
                        if (!OutlinesOnly)
                            g.DrawImage(Example, rectOnDisplay);
                        else
                            g.FillRectangle(nBrush, rectOnDisplay);

                    g.DrawRectangle(nPen, rectOnDisplay);
                    g.DrawString(idx.ToString(), tFont, Brushes.Green, (float)(placeX + SizePerNewMagTile / 2.7F), (float)(placeY + SizePerNewMagTile / 2.7F));
                }
            }

            if (MemoryOnly) return;

            string savePath = Path.Combine(ICFolder_New.FullPath, "Preview " + WellNew.NameAtLevel + " " + Magnification_Original.ToString("000x_") + Magnification_New.ToString("000x_F=") + FractionFilledToInclude.ToString("0.00") + ".jpg");
            bmp.Save(savePath);
            //Consider clearing the Queue here . . 
            mIQueue.Clear();
            //Process.Start(new ProcessStartInfo(savePath) { UseShellExecute = true });
        }

        public Bitmap Reconstruct_FOV(int FOV, short wavelength, RectangleF newFOVRect_um, List<XDCE_Image> relatedXIs, double Ratio, int SizePerOriginalMagTile, int SizePerNewMagTile, XDCE_ImageGroup WellNew) //double MinX, double MaxX, double MinY, double MaxY, 
        {
            var relXIs_WV = relatedXIs.Where(f => f.wavelength == wavelength);
            int CountRel = relXIs_WV.Count(); float Counter = 0F;

            //Figure out the top left Image
            var topLeftImage = relXIs_WV.OrderBy(x => x.PlateX_um).ThenBy(x => x.PlateY_um).FirstOrDefault();
            var xI_New = XDCE_Image.NewBasedOn(WellNew, topLeftImage, FOV, newFOVRect_um);

            if (MemoryOnly) return null;

            var bmN = new Bitmap(SizePerNewMagTile, SizePerNewMagTile);
            var miN = new MagickImage(new MagickColor("black"), SizePerNewMagTile, SizePerNewMagTile);
            if (false)
            {
                double minX = relXIs_WV.Min(x => x.PlateX_um);
                double maxX = relXIs_WV.Max(x => x.PlateX_um);
                double minY = relXIs_WV.Min(x => x.PlateY_um);
                double maxY = relXIs_WV.Max(x => x.PlateY_um);
            }

            using (var g = Graphics.FromImage(bmN))
            {
                foreach (var xI in relXIs_WV)
                {
                    BWReport("", Counter++ / CountRel);
                    int placeX = (int)((xI.PlateX_um - newFOVRect_um.Left) * Ratio);
                    int placeY = (int)((xI.PlateY_um - newFOVRect_um.Top) * Ratio);
                    var rect = new Rectangle(placeX, placeY, SizePerOriginalMagTile, SizePerOriginalMagTile);

                    //Store the params of where things go
                    var Key = (xI.WellLabel, xI.FOV);
                    if (!FOVLookUp.ContainsKey(Key)) FOVLookUp.Add(Key, new List<Alignment_PerFOV>());
                    var chk = FOVLookUp[Key].Where(x => x.FOV_New == xI_New.FOV);
                    if (chk.Count() == 0) FOVLookUp[Key].Add(new Alignment_PerFOV(xI, xI_New, FOV, rect));

                    if (!DebugMode)
                    {
                        (var mI, var bmap) = mIQueue.GetMImage(xI.FullPath, 20, 2600);
                        g.DrawImage(bmap, rect);
                        mI.Resize(mIQueue.GeoFromRect(rect));
                        miN.Composite(mI, placeX, placeY, CompositeOperator.Over);
                        //Can't dispose here since another part may need it, have to dispose the whole queue instead after the well perhaps?
                    }
                }
            }
            //string savePath = @"c:\temp\" + FOV.ToString(); 
            //bmN.Save(savePath + ".jpg");

            //Save New Image
            if (!DebugMode) miN.Write(xI_New.FullPath);
            return bmN;
        }

        public List<(RectangleF newFOV, List<XDCE_Image> relatedXIs)> GenerateNewFOVPattern(XDCE_ImageGroup Well, double width_microns, double height_microns, double minX, double maxX, double minY, double maxY)
        {
            float newWidth_microns = (float)width_microns * Magnification_Original / Magnification_New;
            float newHeight_microns = (float)height_microns * Magnification_Original / Magnification_New;

            int numRows = (int)Math.Ceiling(Magnification_New * Well.Y_to_Index.Count / Magnification_Original);
            int numCols = (int)Math.Ceiling(Magnification_New * Well.X_to_Index.Count / Magnification_Original);

            // Calculate offsets for centering
            float offsetX = (float)((maxX - minX) - numCols * newWidth_microns) / 2;
            float offsetY = (float)((maxY - minY) - numRows * newHeight_microns) / 2;

            var newFOVs = new List<(RectangleF newFOVs, List<XDCE_Image> relatedXIs)>();
            var origRects = new List<(RectangleF newFOVs, List<XDCE_Image> relatedXIs)>();
            for (int FOV = Well.FOV_Min; FOV < Well.FOV_Max; FOV++)
            {
                var relXIs = Well.GetFields(FOV);
                var f = relXIs[0];
                var rect = new RectangleF((float)f.PlateX_um, (float)f.PlateY_um, (float)f.Width_microns, (float)f.Height_microns);
                origRects.Add((rect, relXIs));
            }

            //var origRects = Well.Images.Where(i => i.wavelength == 0).Select(f => (f, new RectangleF((float)f.PlateX_um, (float)f.PlateY_um, (float)f.Width_microns, (float)f.Height_microns)));

            for (int row = 0; row < numRows; row++)
            {
                bool isEvenRow = row % 2 == 0;
                for (int col = 0; col < numCols; col++)
                {
                    float x = (float)minX + offsetX + (isEvenRow ? col : (numCols - col - 1)) * newWidth_microns;
                    float y = (float)minY + offsetY + row * newHeight_microns;

                    //See whether to include this or not
                    var newRec = new RectangleF(x, y, newWidth_microns, newHeight_microns);
                    (double AreaFilled, List<XDCE_Image> relatedXIs) = FOVAreaFilled(newRec, origRects);
                    if (AreaFilled > this.FractionFilledToInclude) newFOVs.Add((newRec, relatedXIs));
                }
            }

            return newFOVs;
        }

        public (double AreaFilled, List<XDCE_Image> relatedXIs) FOVAreaFilled(RectangleF newFOV, IEnumerable<(RectangleF originalFOV, List<XDCE_Image> xIs)> originalFOVs)
        {
            double filledArea = 0; var relXIs = new List<XDCE_Image>();
            foreach ((RectangleF originalFOV, List<XDCE_Image> xIs) in originalFOVs)
            {
                var intersection = RectangleF.Intersect(newFOV, originalFOV);
                if (!intersection.IsEmpty)
                {
                    filledArea += intersection.Width * intersection.Height;
                    relXIs.AddRange(xIs);
                }
            }
            double aFilled = filledArea / (newFOV.Width * newFOV.Height);
            return (aFilled, relXIs);
        }




        //Below is just for Tabular Reconstruction (which you have to do 2nd so you know all the look ups) ------------------------------------------------------------------------

        private Alignment_PerFOV atf_CurrentLookUp;
        private List<string> atf_Headers;
        private string[] atf_Row;

        public void Reconstruct_AssociatedTabularFile(string FilePath)
        {
            if (FilePath == "") return;
            char d = '\t'; string res; var sB = new StringBuilder();
            string atf_Well; int atf_FOV_Old, x, y;

            string[] AllText = File.ReadAllText(FilePath).Split("\r\n");
            //Setup Header Columms
            atf_Headers = AllText[0].ToUpper().Split(d).ToList();
            int atf_Col_FOV = atf_Headers.IndexOf("FOV"), atf_Col_Well = atf_Headers.IndexOf("WELL LABEL");
            int atf_Col_X = atf_Headers.IndexOf(atf_Headers.Where(x => x.Contains("LEFT")).First());
            int atf_Col_Y = atf_Headers.IndexOf(atf_Headers.Where(x => x.Contains("TOP")).First());

            sB.Append(AllText[0] + d + "FOV Orig" + d + "Note" + d + "\r\n");
            for (int r = 1; r < AllText.Length; r++)
            {
                atf_Row = AllText[r].Split(d);
                atf_Well = atf_Row[atf_Col_Well]; atf_FOV_Old = int.Parse(atf_Row[atf_Col_FOV]);
                var Key = (atf_Well, atf_FOV_Old); if (!FOVLookUp.ContainsKey(Key)) continue;
                x = int.Parse(atf_Row[atf_Col_X]); y = int.Parse(atf_Row[atf_Col_Y]);
                atf_CurrentLookUp = FOVLookUpFind(Key, x, y);
                var ret = cnv_NewPositions(x, y, atf_CurrentLookUp);
                if (!ret.use) continue; //This point isn't included in the new set
                atf_Row[atf_Col_X] = ret.newX.ToString(); atf_Row[atf_Col_Y] = ret.newY.ToString();
                for (int c = 0; c < atf_Headers.Count; c++)
                {
                    var tVal = atf_Row[c];
                    res = atf_Headers[c] switch
                    {
                        "FOV" => atf_CurrentLookUp.FOV_New.ToString(),
                        "IMAGENAME" => atf_CurrentLookUp.Image_New,
                        "OBJECT ID" => cnv_ObjectFix(tVal, atf_FOV_Old),
                        var header when header.Contains("WIDTH") || header.Contains("HEIGHT") || header.Contains("AXIS") => cnv_AlterDimensions(tVal, header),
                        _ => tVal
                    };
                    sB.Append(res + d);
                }
                sB.Append(atf_CurrentLookUp.FOV_Old.ToString() + d + atf_CurrentLookUp.Note + d + "\r\n");
            }
            string SavePath = Path.Combine(this.ICFolder_New.FullPath, Path.GetFileNameWithoutExtension(FilePath) + "_recon.txt");
            File.WriteAllText(SavePath, sB.ToString());
        }

        private (bool use, double newX, double newY) cnv_NewPositions(int x, int y, Alignment_PerFOV atf_CurrentLookUp)
        {
            //Use means that it is in the bounds of this new FOV
            var r = atf_CurrentLookUp.Rect;
            double Fact = Magnification_Original / Magnification_New;
            double newX = (x / Fact) + r.X;
            double newY = (y / Fact) + r.Y;
            bool use = newX > 0 && newX < 2040 && newY > 0 && newY < 2040;

            return (use, newX, newY);
        }

        private Alignment_PerFOV FOVLookUpFind((string Well, int FOV_Old) key, int x, int y)
        {
            var set = FOVLookUp[key];
            if (set.Count == 1) return set[0];
            List<Alignment_PerFOV> GoodSet = new();
            foreach (var new_FOV in set)
            {
                var nP = cnv_NewPositions(x, y, new_FOV);
                if (nP.use) GoodSet.add(new_FOV);
            }
            if (GoodSet.Count > 1)
            { //This means that two of the points seem to be legal . . ?
                GoodSet[0].Note = "Two options here"; return GoodSet[0];
            }
            if (GoodSet.Count == 0) return set[0];
            return GoodSet[0];
        }

        private string cnv_ObjectFix(string OldObjectName, int FOV_Old)
        {
            double Val = double.Parse(OldObjectName);
            return (Val + FOV_Old * 1000).ToString("0");
        }

        private string cnv_AlterDimensions(string DimValue, string DimName)
        {
            if (DimName.Contains("ANGLE")) return DimValue;
            double Val = double.Parse(DimValue);
            //The rest can just be adjusted based on the mag differences
            return (Magnification_New * Val / Magnification_Original).ToString();
        }
    }

    public class Alignment_PerFOV
    {
        public string WellLabel;
        public int FOV_Old;
        public int FOV_New;
        public string Note = "";
        public Rectangle Rect;
        public string Image_New;

        public Alignment_PerFOV(XDCE_Image xI_Small, XDCE_Image xI_Large, int fOV_New, Rectangle rect)
        {
            FOV_Old = xI_Small.FOV;
            WellLabel = xI_Small.WellLabel;
            FOV_New = fOV_New;
            Rect = rect;
            Image_New = xI_Large.FileName;
        }

        public (string WellLabel, int FOV_Old) Name => (WellLabel, FOV_Old);
    }

    public class INCELL_AlignToLowerMag
    {
        //Just pasted from  manny 6/8/2023, but the version below may be better

        public static void MannyTest01(INCELL_Folder fHigh, INCELL_Folder fLow)
        {

            var xILow = fLow.XDCE.Images.First();

            // Set up a tuple array. Each tuple is a low res img and a list of hires imgs overlapping with it.
            Tuple<XDCE_Image, List<XDCE_Image>>[] overlapArray = new Tuple<XDCE_Image, List<XDCE_Image>>[fLow.XDCE.Images.Count];

            /* TODO:
            1. Ensure we increment through only 1 img type (?)
            2. account for position correction (done-ish)
            3. account for scale
            */


            /* The way this works, in English:
            We increment through all of the low resolution images (lowRes). We then run crossCorrelation on 
            the high resolution images (hiRes), to find out where in the lowRes image they belong. If the 
            CrossCorr score is above a certain threshold, we find out which sides of the hiRes lie with in the lowRes (I do this 
            as a second check to make sure we actually have an overlap). Then, add that hiRes img to a temporary list called subIMGs
            Once we've gone through all the hiRes imgs once, we add the lowRes img and the subIMGs list to a tuple array. Now, we have
            a list of hiRes imgs and the associated lowRes imgs they overlap with.

            As I typed this out, i realized an issue: this works when our lowRes img takes up the entire field we're concerned with.
            If we have multiple lowRes imgs, then the coordinates of the hiRes will always lie within the lowRes. My gut way to get 
            around this issue is to ALWAYS take a 4x picture and determine the coordinates of the edges/center points from that.
            Without an objective coordinate system, is there some way we can determine if 2 images overlap? I think CrossCorr works,
            but I can't think of a second measure we could use.
             */
            int i = 0;
            foreach (XDCE_Image lowRes in fLow.XDCE.Images)
            {
                // Get field of lowRes img
                double loXLeftPoint = lowRes.x;
                double loXRightPoint = loXLeftPoint + lowRes.Width_microns;
                double loXTopPoint = lowRes.y;
                double loXBotPoint = loXTopPoint + lowRes.Height_microns;
                List<XDCE_Image> subIMGs = new List<XDCE_Image>();

                foreach (XDCE_Image hiRes in fHigh.XDCE.Images)
                {
                    // get field of hiRes
                    // Convert center pixel to microns
                    bool adjustVals = false, addImg = false;


                    /* TODO:
                        Currently, the values for the edges are based on the hiRes images
                        Unfortunately, this means our values are not true relative to the base img.
                        I have no idea how we could correct this, beyond just running CrossCorr on every img.
                        That's not a terrible solution, but it would be faster if we didn't run CrossCorr
                        when we don't have to
                     */


                    double hiResCenterX = hiRes.CenterPixel.X * (hiRes.Width_microns / hiRes.Width_Pixels);
                    double hiResCenterY = hiRes.CenterPixel.Y * (hiRes.Height_microns / hiRes.Height_Pixels);

                    // Add half of width/height to get edges
                    double hiXLeftPoint = hiResCenterX - hiRes.Width_microns / 2;
                    double hiXRightPoint = hiResCenterX + hiRes.Width_microns / 2;
                    double hiXBotPoint = hiResCenterY - hiRes.Height_microns / 2;
                    double hiXTopPoint = hiResCenterY + hiRes.Height_microns / 2;

                    // Use CrossCorr for adjusting center/edge positions
                    var locationData = CrossCorr.Register(null, null); //lowRes, hiRes);
                    if (locationData.Score_Final > 0) // Arbitrary score
                    {
                        adjustVals = true;
                    }

                    if (adjustVals)
                    {
                        // the center pixel, according to cross correlation
                        var adjustedCenter = locationData.Location;

                        // Use the same method as above to get edges
                        double adjustedX = adjustedCenter.X * (hiRes.Width_microns / hiRes.Width_Pixels);
                        double adjustedY = adjustedCenter.Y * (hiRes.Height_microns / hiRes.Height_Pixels);

                        hiXLeftPoint = adjustedX - hiRes.Width_microns / 2;
                        hiXRightPoint = adjustedX + hiRes.Width_microns / 2;
                        hiXBotPoint = adjustedX - hiRes.Height_microns / 2;
                        hiXTopPoint = adjustedX + hiRes.Height_microns / 2;

                    }


                    if (hiXLeftPoint < loXRightPoint && hiXLeftPoint > loXLeftPoint)
                    {
                        // small img's left side is inside big img
                        addImg = true;
                    }
                    else if (hiXRightPoint > loXLeftPoint && hiXRightPoint < loXRightPoint)
                    {
                        // small img's right side is inside big img
                        addImg = true;
                    }
                    else if (hiXTopPoint > loXBotPoint && hiXTopPoint < loXTopPoint)
                    {
                        // small img's top side is inside big img
                        addImg = true;

                    }
                    else if (hiXBotPoint < loXTopPoint && hiXBotPoint > loXBotPoint)
                    {
                        // small img's bottom side is inside big img
                        addImg = true;
                    }
                    if (addImg)
                    {
                        // Add imgs to a list
                        subIMGs.Add(hiRes);
                    }
                }
                // add lowRes and subIMGs
                overlapArray[i] = new Tuple<XDCE_Image, List<XDCE_Image>>(lowRes, subIMGs);
                i++;

            }
        }
    }
}
