﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FIV_IMG_CLib
{
    public partial class DataLineageForm : Form
    {
        public DLContainer DLActive;
        public DataLineageSelector DLSelector;
        Dictionary<string, TextBox> TextBoxes;
        Dictionary<string, Tuple<NumericUpDown, CounterHolder, List<DLSave>>> UpDowns;
        public bool IsDirty = false;

        public DataLineageForm()
        {
            InitializeComponent();

            //Setup the Textboxes Dictionary
            TextBoxes = new Dictionary<string, TextBox>();
            foreach (TextBox tb in this.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);
            foreach (TextBox tb in panel1.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);
            foreach (TextBox tb in panel2.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);
            foreach (TextBox tb in panel3.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);
            foreach (TextBox tb in panel4.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);
            foreach (TextBox tb in panel5.Controls.OfType<TextBox>()) TextBoxes.Add(tb.Name, tb);

            //Go through and add a changed event so that we know when things are dirty (important for switching 'pages')
            foreach (var tb in TextBoxes) tb.Value.TextChanged += new System.EventHandler(this.Data_TextChanged);
        }

        private void DataLineageForm_Load(object sender, EventArgs e)
        {
            btn_Load_Click(sender, e);
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            DLSelector = new DataLineageSelector(DLCatalog.LoadFromFolder());
            DLSelector.ShowDialog();
            DLActive = DLContainer.Load(DLSelector.SelectedPath);

            //Setup UpDowns
            UpDowns = new Dictionary<string, Tuple<NumericUpDown, CounterHolder, List<DLSave>>>();
            UpDowns.Add("Pr", new Tuple<NumericUpDown, CounterHolder, List<DLSave>>(UpDown_Perturbation, new CounterHolder(), DLActive.Perturbations));
            UpDowns.Add("Ce", new Tuple<NumericUpDown, CounterHolder, List<DLSave>>(UpDown_Cells, new CounterHolder(), DLActive.Cells));
            UpDowns.Add("Ph", new Tuple<NumericUpDown, CounterHolder, List<DLSave>>(UpDown_Phenotype, new CounterHolder(), DLActive.Phenotype));
            UpDowns.Add("Gn", new Tuple<NumericUpDown, CounterHolder, List<DLSave>>(UpDown_Genotype, new CounterHolder(), DLActive.Genotype));
            UpDowns.Add("Ex", new Tuple<NumericUpDown, CounterHolder, List<DLSave>>(null, new CounterHolder(), null)); //This one can't actually change, but we need a 0 placeholder

            SaveLoad_UnderlyingData(false);
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            SaveLoad_UnderlyingData(true);
            if (!DLActive.Save())
            {
                //Do something here, it means there was a name collision
            }
        }

        private void btn_SaveAs_Click(object sender, EventArgs e)
        {

        }

        public void SaveLoad_UnderlyingData(bool SaveTrue_LoadFalse)
        {
            if (SaveTrue_LoadFalse) if (DLActive == null) DLActive = new DLContainer(); //Make a new DLContainer and save into it if there is none
            else if (DLActive == null) return; //Pull data from DLContainer back into form

            foreach (var txBx in TextBoxes)
            {
                var N = new DL_Item_Name(txBx.Key);
                if (SaveTrue_LoadFalse)
                    DLActive.AddParam(N, UpDowns[N.SectionAbbrev].Item2.Count, txBx.Value.Text);
                else
                {
                    txBx.Value.Text = DLActive.GetParamValue(N, UpDowns[N.SectionAbbrev].Item2.Count);
                    txBx.Value.BackColor = Color.White;
                }
            }
            IsDirty = false;
        }

        private void labelClear_Click(object sender, EventArgs e)
        {
            foreach (var tB in TextBoxes) tB.Value.Text = "";
        }

        private void UpDown_ValueChanged(object sender, EventArgs e)
        {
            //Go through and update all the active indices
            int nIdx; NumericUpDown UpDown; List<DLSave> Lst; CounterHolder CH;
            if (IsDirty)
                SaveLoad_UnderlyingData(true);
            foreach (var UpDownKVP in UpDowns)
            {
                if (UpDownKVP.Value.Item1 == null) continue; //this is the Experimental one which can't change
                UpDown = UpDownKVP.Value.Item1; nIdx = (int)UpDown.Value;
                Lst = UpDownKVP.Value.Item3;
                CH = UpDownKVP.Value.Item2;
                if (nIdx < Lst.Count)
                {
                    //Just change the active index
                    CH.Count = nIdx;
                }
                else if (nIdx == Lst.Count)
                {
                    //Add one and change the index
                    Lst.Add(new DLPerturbation());
                    CH.Count = nIdx;
                }
                else
                {
                    //Reset back
                    UpDown.Value = Lst.Count - 1;
                }
            }

            //Redraw
            SaveLoad_UnderlyingData(false);
        }

        private void UpDown_Perturbation_Enter(object sender, EventArgs e)
        {
            
        }

        private void Data_TextChanged(object sender, EventArgs e)
        {
            var tB = (TextBox)sender;
            tB.BackColor = Color.Pink;
            IsDirty = true;
        }

        
    }

    public class DLCatalogEntry
    {
        public string Path;
        public DateTime SaveDate;
        public DLContainer dLContainer;
        public string Scientist { get => dLContainer.Experiment.Scientist; }
        public string ExpName { get => dLContainer.Experiment.ExpName; }
        public string ProjectName { get => dLContainer.Experiment.ProjectName; }
        public string QuestionPurpose { get => dLContainer.Experiment.QuestionPurpose; }
        public string Status { get => dLContainer.Experiment.Status; }

        public DLCatalogEntry(string filePath)
        {
            var DLCon = DLContainer.Load(filePath);
            Path = filePath;
            SaveDate = File.GetLastWriteTime(filePath);
            dLContainer = DLCon;
        }

        public override string ToString()
        {
            return Scientist_ExpName;
        }

        public string Scientist_ExpName { get => Scientist + '\t' + ExpName; }

        private static int padLen = 19;

        public string LineEntry
        {
            get => Scientist.PadRight(padLen).Substring(0, padLen) + ' ' + ExpName.PadRight(9).Substring(0, 9) + ' ' +
                  ProjectName.PadRight(padLen).Substring(0, padLen) + ' ' + SaveDate.ToString("MM/dd/yyyy");
        }
    }

    public class CounterHolder
    {
        public int Count;
        public void Increment() { Count++; }
        public CounterHolder()
        {
            Count = 0;
        }
    }

    public class DLCatalog : IEnumerable<DLCatalogEntry>
    {
        private List<DLCatalogEntry> _List;
        private Dictionary<string, DLCatalogEntry> _LineEntry_DCE_Lookup;

        public DLCatalog()
        {
            _List = new List<DLCatalogEntry>();
        }

        public DLCatalogEntry this[int index]
        {
            get { return _List[index]; }
            //set { /* set the specified index to value here */ }
        }

        public int Count { get => _List.Count; }
        public string Folder { get; internal set; }

        public static string DefaultCatalogPath = @"R:\dB\Software\FIVE_Tools\DLCatalog\";

        public static DLCatalog LoadFromFolder(string FolderPath = "")
        {
            if (FolderPath == "") FolderPath = DefaultCatalogPath;
            var DLC = new DLCatalog(); DLC.Folder = FolderPath;
            foreach (string Fl in Directory.GetFiles(FolderPath, "*.json"))
            {
                DLC.Add(new DLCatalogEntry(Fl));
            }
            return DLC;
        }

        public void Add(DLCatalogEntry dLCatalogEntry)
        {
            _List.Add(dLCatalogEntry);
        }

        public IEnumerator<DLCatalogEntry> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        internal DLCatalogEntry GetEntryByLine(string LineEntry)
        {
            if (_LineEntry_DCE_Lookup == null)
            {
                _LineEntry_DCE_Lookup = new Dictionary<string, DLCatalogEntry>();
                foreach (var CE in this) _LineEntry_DCE_Lookup.Add(CE.LineEntry, CE);
            }
            return _LineEntry_DCE_Lookup[LineEntry];
        }
    }


    public class DLContainer
    {
        public DLExperiment Experiment;
        public List<DLSave> Perturbations;
        public List<DLSave> Cells;
        public List<DLSave> Phenotype;
        public List<DLSave> Genotype;

        // - - 1/20/2022 thought I needed these but they are implemented in the FORM instead
        //public int Perturbation_ActiveIndex;
        //public int Cells_ActiveIndex;
        //public int Phenotype_ActiveIndex;
        //public int Genotype_ActiveIndex;

        public DLContainer()
        {
            Experiment = new DLExperiment();
            Perturbations = new List<DLSave>() { new DLPerturbation() };
            Cells = new List<DLSave>() { new DLCells() };
            Phenotype = new List<DLSave>() { new DLPhenotype() };
            Genotype = new List<DLSave>() { new DLGenotype() };

            // - - - implemented through the form
            //Perturbation_ActiveIndex = Cells_ActiveIndex = Phenotype_ActiveIndex = Genotype_ActiveIndex = 0;
        }

        public Dictionary<string, object> ConvertForJSON()
        {
            return new Dictionary<string, object>
            { { nameof(Experiment), Experiment }, { nameof(Perturbations), Perturbations } , { nameof(Cells), Cells } , { nameof(Phenotype),Phenotype}, { nameof(Genotype), Genotype } };
        }

        public bool Save(string Location = "")
        {
            if (Location == "")
            {
                Location = Path.Combine(DLCatalog.DefaultCatalogPath, this.Experiment.ExpName + ".json");
            }
            if (File.Exists(Location)) return false;
            string data = JsonSerializer.Serialize(ConvertForJSON());
            File.WriteAllText(Location, data);
            return true;
        }

        public bool Save_XML_Deprecated(string Location)
        {
            //First run PreSave on all the DLSaves here . . 
            Experiment.PreSave();
            foreach (var Pr in Perturbations) Pr.PreSave();
            foreach (var Ce in Cells) Ce.PreSave();
            foreach (var Ph in Phenotype) Ph.PreSave();
            foreach (var Gn in Genotype) Gn.PreSave();

            //Now serialize
            XmlSerializer x = new XmlSerializer(typeof(DLContainer));
            using (StreamWriter SW = new StreamWriter(Location))
            {
                x.Serialize(SW, this);
                SW.Close();
                return true;
            }
        }

        public static DLContainer Load(string Location)
        {
            if (Location == null) return null;
            if (Location == "") return null;
            Dictionary<string, object> TempDict = new Dictionary<string, object>();
            TempDict = (Dictionary<string, object>)JsonSerializer.Deserialize(File.ReadAllText(Location), TempDict.GetType());
            return ConvertFromJSON_Intermediate(TempDict);
        }

        public static DLContainer ConvertFromJSON_Intermediate(Dictionary<string, object> ToConvert)
        {
            DLContainer DLC = new DLContainer();
            //string T = ToConvert[nameof(Experiment)].ToString();
            if (ToConvert.ContainsKey(nameof(Experiment))) DLC.Experiment = (DLExperiment)JsonSerializer.Deserialize(ToConvert[nameof(Experiment)].ToString(), typeof(DLExperiment));
            if (ToConvert.ContainsKey(nameof(Perturbations))) DLC.Perturbations = (List<DLSave>)JsonSerializer.Deserialize(ToConvert[nameof(Perturbations)].ToString(), typeof(List<DLSave>));
            if (ToConvert.ContainsKey(nameof(Cells))) DLC.Cells = (List<DLSave>)JsonSerializer.Deserialize(ToConvert[nameof(Cells)].ToString(), typeof(List<DLSave>));
            if (ToConvert.ContainsKey(nameof(Phenotype))) DLC.Phenotype = (List<DLSave>)JsonSerializer.Deserialize(ToConvert[nameof(Phenotype)].ToString(), typeof(List<DLSave>));
            if (ToConvert.ContainsKey(nameof(Genotype))) DLC.Genotype = (List<DLSave>)JsonSerializer.Deserialize(ToConvert[nameof(Genotype)].ToString(), typeof(List<DLSave>));
            return DLC;
        }

        public static DLContainer Load_XML_Deprecated(string Location)
        {
            XmlSerializer x = new XmlSerializer(typeof(DLContainer));
            using (FileStream F = new FileStream(Location, FileMode.Open))
            {
                DLContainer CR = (DLContainer)x.Deserialize(F);
                F.Close();
                return CR;
            }
        }

        public DLSave GetFromAbbrev(string Abbrev, int Idx = 0)
        {
            Abbrev = Abbrev.ToUpper().Trim();
            switch (Abbrev)
            {
                case "EX": //DLExperiment
                    return Experiment;
                case "PR":
                    return Perturbations[Idx];
                case "CE":
                    return Cells[Idx];
                case "PH":
                    return Phenotype[Idx];
                case "GN":
                    return Genotype[Idx]; // == -1 ? Genotype_ActiveIndex : Idx];  //- - An alternate way, but moved this logic to the FORM
                default:
                    return null;
            }
        }

        internal void AddParam(DL_Item_Name Name, int Idx, string Value)
        {
            GetFromAbbrev(Name.SectionAbbrev, Idx).AddChange(Name.FieldName, Value);
        }

        internal string GetParamValue(DL_Item_Name Name, int Idx = 0)
        {
            return GetFromAbbrev(Name.SectionAbbrev, Idx).GetOrCreate(Name.FieldName);
        }
    }

    public class DL_Item_Name
    {
        public string Name;
        public string SectionAbbrev;
        public string FieldName;
        public string Other;

        public DL_Item_Name(string TextBoxName)
        {
            Name = TextBoxName;
            SplitName(TextBoxName);
        }

        private void SplitName(string TextBox_Name)
        {
            SectionAbbrev = TextBox_Name.Substring(0, 2);
            FieldName = TextBox_Name.Substring(3);
            Other = "";

            //- - Original usage before 1/2022
            //string SectionAbbrev, FieldName, Other;
            //SplitName(TextBox_Name, out SectionAbbrev, out FieldName, out Other);
        }
    }

    public class DLSave
    {
        //public List<KeyValuePair<string, object>> KVPs;
        //public List<Tuple<string, string>> KVPs;

        private Dictionary<string, string> _Dict;

        public Dictionary<string, string> Dict
        {
            get
            {
                if (_Dict == null)
                {
                    _Dict = new Dictionary<string, string>();
                    //if (KVPs == null)
                    //{
                    //    KVPs = new List<Tuple<string, string>>(); //KVPs = new List<KeyValuePair<string, object>>();
                    //}
                    //else
                    //{
                    //    foreach (var KVP in KVPs)
                    //    {
                    //        //_Dict.Add(KVP.Key, KVP.Value);
                    //        _Dict.Add(KVP.Item1, KVP.Item2);
                    //    }
                    //}
                }
                return _Dict;
            }
            set
            {
                _Dict = value;
            }
        }

        public void AddChange(string fieldName, string value)
        {
            if (Dict.ContainsKey(fieldName))
                Dict[fieldName] = value;
            else
                Dict.Add(fieldName, value);
        }

        public string GetOrCreate(string fieldName)
        {
            if (!Dict.ContainsKey(fieldName))
                Dict.Add(fieldName, "");
            return Dict[fieldName];
        }

        public string GetVal(string fieldName)
        {
            if (Dict.ContainsKey(fieldName)) return Dict[fieldName];
            else return "";
        }

        /// <summary>
        /// Run this BEFORE serializing
        /// </summary>
        public void PreSave()
        {
            if (_Dict == null) return;
            //KVPs = new List<Tuple<string, string>>(); //KVPs = new List<KeyValuePair<string, object>>();
            //foreach (var KVP in _Dict)
            //{
            //    KVPs.Add(new Tuple<string, string>(KVP.Key, KVP.Value.ToString()));
            //}
        }
    }

    public class DLExperiment : DLSave
    {
        public string Scientist { get => GetVal("Scientist"); }
        public string ExpName { get => GetVal("ExpName"); }
        public string ProjectName { get => GetVal("ProjectName"); }
        public string QuestionPurpose { get => GetVal("QuestionPurpose"); }
        public string Status { get => GetVal("Status"); }
    }

    public class DLPerturbation : DLSave
    {

    }

    public class DLCells : DLSave
    {

    }

    public class DLPhenotype : DLSave
    {

    }

    public class DLGenotype : DLSave
    {

    }
}
