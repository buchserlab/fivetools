﻿
using System.Drawing;

namespace FIV_IMG_CLib
{
    partial class DataLineageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Cells = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Ex_Scientist = new System.Windows.Forms.TextBox();
            this.Ex_ExpName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Ex_QuestionPurpose = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Ex_Notes = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Ex_BaseFolder = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Ex_ProjectName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.UpDown_Cells = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Ce_Treatments = new System.Windows.Forms.TextBox();
            this.Ce_Media = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Ce_SampleID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Ce_Source = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.UpDown_Phenotype = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.Ph_ModelInfo = new System.Windows.Forms.TextBox();
            this.Ph_SurvivalDetails = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Ph_FlowSettings = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.Ph_ImagingSettings = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.Ph_DataLoc = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Ph_StainingReporters = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.UpDown_Genotype = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.Gn_StrongestAlleleTable = new System.Windows.Forms.TextBox();
            this.Gn_FastQLocations = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.Gn_SampleIDs = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.Gn_PCR1Cycles = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.Gn_PreAmpCycles = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.Gn_PlateLU = new System.Windows.Forms.TextBox();
            this.Gn_Notes = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.UpDown_Perturbation = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.Pr_Format = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Pr_Name = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Pr_Representation = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Pr_SampleID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Pr_Details = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Load = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.lbl_Template_RaftSeq = new System.Windows.Forms.Label();
            this.lbl_Template_Sorting = new System.Windows.Forms.Label();
            this.lbl_Template_Selection = new System.Windows.Forms.Label();
            this.lbl_Template_MorphNSort = new System.Windows.Forms.Label();
            this.btn_SaveAs = new System.Windows.Forms.Button();
            this.Ex_Status = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btn_Layout = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.labelClear = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Cells)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Phenotype)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Genotype)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Perturbation)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_Cells
            // 
            this.lbl_Cells.AutoSize = true;
            this.lbl_Cells.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Cells.Location = new System.Drawing.Point(13, 14);
            this.lbl_Cells.Name = "lbl_Cells";
            this.lbl_Cells.Size = new System.Drawing.Size(51, 24);
            this.lbl_Cells.TabIndex = 0;
            this.lbl_Cells.Text = "Cells";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Scientist";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Phenotype";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 24);
            this.label4.TabIndex = 4;
            this.label4.Text = "Genotype";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 24);
            this.label5.TabIndex = 5;
            this.label5.Text = "Experiment";
            // 
            // Ex_Scientist
            // 
            this.Ex_Scientist.Location = new System.Drawing.Point(3, 61);
            this.Ex_Scientist.Name = "Ex_Scientist";
            this.Ex_Scientist.Size = new System.Drawing.Size(168, 20);
            this.Ex_Scientist.TabIndex = 6;
            this.Ex_Scientist.Text = "Marianna Vakaki";
            // 
            // Ex_ExpName
            // 
            this.Ex_ExpName.Location = new System.Drawing.Point(3, 100);
            this.Ex_ExpName.Name = "Ex_ExpName";
            this.Ex_ExpName.Size = new System.Drawing.Size(168, 20);
            this.Ex_ExpName.TabIndex = 8;
            this.Ex_ExpName.Text = "FIV582";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Exp Name";
            // 
            // Ex_QuestionPurpose
            // 
            this.Ex_QuestionPurpose.Location = new System.Drawing.Point(3, 217);
            this.Ex_QuestionPurpose.Multiline = true;
            this.Ex_QuestionPurpose.Name = "Ex_QuestionPurpose";
            this.Ex_QuestionPurpose.Size = new System.Drawing.Size(168, 107);
            this.Ex_QuestionPurpose.TabIndex = 10;
            this.Ex_QuestionPurpose.Text = "Examining the effect of Mito Related gRNAs on the proximal mitochondria in iPSC n" +
    "eurons";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Question/Purpose";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(950, 425);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Ex_Notes);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.Ex_BaseFolder);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.Ex_ProjectName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.Ex_QuestionPurpose);
            this.panel1.Controls.Add(this.Ex_Scientist);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Ex_ExpName);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(184, 419);
            this.panel1.TabIndex = 0;
            // 
            // Ex_Notes
            // 
            this.Ex_Notes.Location = new System.Drawing.Point(3, 344);
            this.Ex_Notes.Multiline = true;
            this.Ex_Notes.Name = "Ex_Notes";
            this.Ex_Notes.Size = new System.Drawing.Size(168, 72);
            this.Ex_Notes.TabIndex = 16;
            this.Ex_Notes.Text = "N/A";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 328);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Notes";
            // 
            // Ex_BaseFolder
            // 
            this.Ex_BaseFolder.Location = new System.Drawing.Point(3, 178);
            this.Ex_BaseFolder.Name = "Ex_BaseFolder";
            this.Ex_BaseFolder.Size = new System.Drawing.Size(168, 20);
            this.Ex_BaseFolder.TabIndex = 14;
            this.Ex_BaseFolder.Text = "R:\\FIVE\\EXP\\FIV582\\";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Base Folder";
            // 
            // Ex_ProjectName
            // 
            this.Ex_ProjectName.Location = new System.Drawing.Point(3, 139);
            this.Ex_ProjectName.Name = "Ex_ProjectName";
            this.Ex_ProjectName.Size = new System.Drawing.Size(168, 20);
            this.Ex_ProjectName.TabIndex = 12;
            this.Ex_ProjectName.Text = "iPSC Screen 03";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Project Name";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.Controls.Add(this.UpDown_Cells);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.Ce_Treatments);
            this.panel2.Controls.Add(this.Ce_Media);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.Ce_SampleID);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.Ce_Source);
            this.panel2.Controls.Add(this.lbl_Cells);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(383, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(184, 419);
            this.panel2.TabIndex = 1;
            // 
            // UpDown_Cells
            // 
            this.UpDown_Cells.Location = new System.Drawing.Point(119, 20);
            this.UpDown_Cells.Name = "UpDown_Cells";
            this.UpDown_Cells.Size = new System.Drawing.Size(52, 20);
            this.UpDown_Cells.TabIndex = 31;
            this.UpDown_Cells.ValueChanged += new System.EventHandler(this.UpDown_ValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 208);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Treatments";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 167);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Media and Supplements";
            // 
            // Ce_Treatments
            // 
            this.Ce_Treatments.Location = new System.Drawing.Point(3, 224);
            this.Ce_Treatments.Multiline = true;
            this.Ce_Treatments.Name = "Ce_Treatments";
            this.Ce_Treatments.Size = new System.Drawing.Size(168, 192);
            this.Ce_Treatments.TabIndex = 20;
            this.Ce_Treatments.Text = "Untreated";
            // 
            // Ce_Media
            // 
            this.Ce_Media.Location = new System.Drawing.Point(3, 183);
            this.Ce_Media.Name = "Ce_Media";
            this.Ce_Media.Size = new System.Drawing.Size(168, 20);
            this.Ce_Media.TabIndex = 19;
            this.Ce_Media.Text = "iPSC Step 5 Media";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 86);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Source (thawed/animal)";
            // 
            // Ce_SampleID
            // 
            this.Ce_SampleID.Location = new System.Drawing.Point(3, 61);
            this.Ce_SampleID.Name = "Ce_SampleID";
            this.Ce_SampleID.Size = new System.Drawing.Size(168, 20);
            this.Ce_SampleID.TabIndex = 18;
            this.Ce_SampleID.Text = "5109";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "SampleID (LIMS)";
            // 
            // Ce_Source
            // 
            this.Ce_Source.Location = new System.Drawing.Point(3, 102);
            this.Ce_Source.Multiline = true;
            this.Ce_Source.Name = "Ce_Source";
            this.Ce_Source.Size = new System.Drawing.Size(168, 59);
            this.Ce_Source.TabIndex = 18;
            this.Ce_Source.Text = "GEiC iCas9 iPSC Cells (xx01)\r\nMV managed\r\nProtocol: MN01\r\nMatured Motor Neurons";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.OldLace;
            this.panel3.Controls.Add(this.UpDown_Phenotype);
            this.panel3.Controls.Add(this.label39);
            this.panel3.Controls.Add(this.Ph_ModelInfo);
            this.panel3.Controls.Add(this.Ph_SurvivalDetails);
            this.panel3.Controls.Add(this.label38);
            this.panel3.Controls.Add(this.Ph_FlowSettings);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.Ph_ImagingSettings);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.Ph_DataLoc);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.Ph_StainingReporters);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(573, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(184, 419);
            this.panel3.TabIndex = 2;
            // 
            // UpDown_Phenotype
            // 
            this.UpDown_Phenotype.Location = new System.Drawing.Point(124, 18);
            this.UpDown_Phenotype.Name = "UpDown_Phenotype";
            this.UpDown_Phenotype.Size = new System.Drawing.Size(52, 20);
            this.UpDown_Phenotype.TabIndex = 31;
            this.UpDown_Phenotype.ValueChanged += new System.EventHandler(this.UpDown_ValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(13, 347);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "Model Info";
            // 
            // Ph_ModelInfo
            // 
            this.Ph_ModelInfo.Location = new System.Drawing.Point(3, 363);
            this.Ph_ModelInfo.Multiline = true;
            this.Ph_ModelInfo.Name = "Ph_ModelInfo";
            this.Ph_ModelInfo.Size = new System.Drawing.Size(168, 53);
            this.Ph_ModelInfo.TabIndex = 32;
            this.Ph_ModelInfo.Text = "Model Data, model parameters";
            // 
            // Ph_SurvivalDetails
            // 
            this.Ph_SurvivalDetails.Location = new System.Drawing.Point(3, 233);
            this.Ph_SurvivalDetails.Multiline = true;
            this.Ph_SurvivalDetails.Name = "Ph_SurvivalDetails";
            this.Ph_SurvivalDetails.Size = new System.Drawing.Size(168, 34);
            this.Ph_SurvivalDetails.TabIndex = 31;
            this.Ph_SurvivalDetails.Text = "Type of cell death (less division, removal by flow";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 217);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(117, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "Survival Screen Details";
            // 
            // Ph_FlowSettings
            // 
            this.Ph_FlowSettings.Location = new System.Drawing.Point(3, 183);
            this.Ph_FlowSettings.Multiline = true;
            this.Ph_FlowSettings.Name = "Ph_FlowSettings";
            this.Ph_FlowSettings.Size = new System.Drawing.Size(168, 31);
            this.Ph_FlowSettings.TabIndex = 29;
            this.Ph_FlowSettings.Text = "Instrument, PSI, Gates, Collected Numbers";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(13, 170);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 28;
            this.label37.Text = "Flow Settings";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(13, 116);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(85, 13);
            this.label36.TabIndex = 27;
            this.label36.Text = "Imaging Settings";
            // 
            // Ph_ImagingSettings
            // 
            this.Ph_ImagingSettings.Location = new System.Drawing.Point(3, 133);
            this.Ph_ImagingSettings.Multiline = true;
            this.Ph_ImagingSettings.Name = "Ph_ImagingSettings";
            this.Ph_ImagingSettings.Size = new System.Drawing.Size(168, 34);
            this.Ph_ImagingSettings.TabIndex = 26;
            this.Ph_ImagingSettings.Text = "Scope, Exposures, Confocality, Objective";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 296);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(139, 13);
            this.label25.TabIndex = 25;
            this.label25.Text = "Data Location (Image/Flow)";
            // 
            // Ph_DataLoc
            // 
            this.Ph_DataLoc.Location = new System.Drawing.Point(3, 312);
            this.Ph_DataLoc.Name = "Ph_DataLoc";
            this.Ph_DataLoc.Size = new System.Drawing.Size(168, 20);
            this.Ph_DataLoc.TabIndex = 24;
            this.Ph_DataLoc.Text = "i:\\20X_24well_JCB_4Color\\20X_24well_JCB_4Color_FIV494_20x_45_1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Staining/Reporters";
            // 
            // Ph_StainingReporters
            // 
            this.Ph_StainingReporters.Location = new System.Drawing.Point(3, 61);
            this.Ph_StainingReporters.Multiline = true;
            this.Ph_StainingReporters.Name = "Ph_StainingReporters";
            this.Ph_StainingReporters.Size = new System.Drawing.Size(168, 52);
            this.Ph_StainingReporters.TabIndex = 19;
            this.Ph_StainingReporters.Text = "Reporters, antibodies, stains. Concentrations, timing.";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.OldLace;
            this.panel4.Controls.Add(this.UpDown_Genotype);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.Gn_StrongestAlleleTable);
            this.panel4.Controls.Add(this.Gn_FastQLocations);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.Gn_SampleIDs);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.Gn_PCR1Cycles);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.Gn_PreAmpCycles);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.Gn_PlateLU);
            this.panel4.Controls.Add(this.Gn_Notes);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(763, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 419);
            this.panel4.TabIndex = 3;
            // 
            // UpDown_Genotype
            // 
            this.UpDown_Genotype.Location = new System.Drawing.Point(129, 18);
            this.UpDown_Genotype.Name = "UpDown_Genotype";
            this.UpDown_Genotype.Size = new System.Drawing.Size(52, 20);
            this.UpDown_Genotype.TabIndex = 34;
            this.UpDown_Genotype.ValueChanged += new System.EventHandler(this.UpDown_ValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 312);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 26;
            this.label35.Text = "Notes";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 269);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 13);
            this.label34.TabIndex = 35;
            this.label34.Text = "Strongest Allele Table";
            // 
            // Gn_StrongestAlleleTable
            // 
            this.Gn_StrongestAlleleTable.Location = new System.Drawing.Point(3, 285);
            this.Gn_StrongestAlleleTable.Name = "Gn_StrongestAlleleTable";
            this.Gn_StrongestAlleleTable.Size = new System.Drawing.Size(168, 20);
            this.Gn_StrongestAlleleTable.TabIndex = 34;
            this.Gn_StrongestAlleleTable.Text = "R:\\FIVE\\EXP\\FIV582\\5 NGS\\FIV582_Alleles.txt";
            // 
            // Gn_FastQLocations
            // 
            this.Gn_FastQLocations.Location = new System.Drawing.Point(3, 247);
            this.Gn_FastQLocations.Name = "Gn_FastQLocations";
            this.Gn_FastQLocations.Size = new System.Drawing.Size(168, 20);
            this.Gn_FastQLocations.TabIndex = 33;
            this.Gn_FastQLocations.Text = "R:\\FIVE\\EXP\\FIV582\\5 NGS\\FastQ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(12, 178);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 13);
            this.label33.TabIndex = 32;
            this.label33.Text = "Plate LU";
            // 
            // Gn_SampleIDs
            // 
            this.Gn_SampleIDs.Location = new System.Drawing.Point(3, 155);
            this.Gn_SampleIDs.Name = "Gn_SampleIDs";
            this.Gn_SampleIDs.Size = new System.Drawing.Size(168, 20);
            this.Gn_SampleIDs.TabIndex = 31;
            this.Gn_SampleIDs.Text = "B101, B102, B103";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 139);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "SampleIDs (CGS)";
            // 
            // Gn_PCR1Cycles
            // 
            this.Gn_PCR1Cycles.Location = new System.Drawing.Point(3, 116);
            this.Gn_PCR1Cycles.Name = "Gn_PCR1Cycles";
            this.Gn_PCR1Cycles.Size = new System.Drawing.Size(168, 20);
            this.Gn_PCR1Cycles.TabIndex = 29;
            this.Gn_PCR1Cycles.Text = "25";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 100);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(69, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "PCR1 Cycles";
            // 
            // Gn_PreAmpCycles
            // 
            this.Gn_PreAmpCycles.Location = new System.Drawing.Point(3, 77);
            this.Gn_PreAmpCycles.Name = "Gn_PreAmpCycles";
            this.Gn_PreAmpCycles.Size = new System.Drawing.Size(168, 20);
            this.Gn_PreAmpCycles.TabIndex = 24;
            this.Gn_PreAmpCycles.Text = "8";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 61);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 13);
            this.label30.TabIndex = 23;
            this.label30.Text = "PreAmp Cycles";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 231);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 13);
            this.label28.TabIndex = 27;
            this.label28.Text = "FastQ Location";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(13, 45);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 26;
            this.label29.Text = "Metadata";
            // 
            // Gn_PlateLU
            // 
            this.Gn_PlateLU.Location = new System.Drawing.Point(3, 194);
            this.Gn_PlateLU.Multiline = true;
            this.Gn_PlateLU.Name = "Gn_PlateLU";
            this.Gn_PlateLU.Size = new System.Drawing.Size(168, 34);
            this.Gn_PlateLU.TabIndex = 21;
            this.Gn_PlateLU.Text = "{{101,A1P1},{102,A2P1},{103,A1P2}}";
            // 
            // Gn_Notes
            // 
            this.Gn_Notes.Location = new System.Drawing.Point(3, 328);
            this.Gn_Notes.Multiline = true;
            this.Gn_Notes.Name = "Gn_Notes";
            this.Gn_Notes.Size = new System.Drawing.Size(168, 88);
            this.Gn_Notes.TabIndex = 20;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel5.Controls.Add(this.UpDown_Perturbation);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.Pr_Format);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.Pr_Name);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.Pr_Representation);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.Pr_SampleID);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.Pr_Details);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(193, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(184, 419);
            this.panel5.TabIndex = 4;
            // 
            // UpDown_Perturbation
            // 
            this.UpDown_Perturbation.Location = new System.Drawing.Point(119, 18);
            this.UpDown_Perturbation.Name = "UpDown_Perturbation";
            this.UpDown_Perturbation.Size = new System.Drawing.Size(52, 20);
            this.UpDown_Perturbation.TabIndex = 30;
            this.UpDown_Perturbation.ValueChanged += new System.EventHandler(this.UpDown_ValueChanged);
            this.UpDown_Perturbation.Enter += new System.EventHandler(this.UpDown_Perturbation_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 201);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "Details";
            // 
            // Pr_Format
            // 
            this.Pr_Format.Location = new System.Drawing.Point(3, 139);
            this.Pr_Format.Name = "Pr_Format";
            this.Pr_Format.Size = new System.Drawing.Size(168, 20);
            this.Pr_Format.TabIndex = 28;
            this.Pr_Format.Text = "Lenti, Titer=2.4E8PFU/ml";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 123);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 27;
            this.label23.Text = "Format";
            // 
            // Pr_Name
            // 
            this.Pr_Name.Location = new System.Drawing.Point(3, 100);
            this.Pr_Name.Name = "Pr_Name";
            this.Pr_Name.Size = new System.Drawing.Size(168, 20);
            this.Pr_Name.TabIndex = 26;
            this.Pr_Name.Text = "MSPH7";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Name";
            // 
            // Pr_Representation
            // 
            this.Pr_Representation.Location = new System.Drawing.Point(3, 178);
            this.Pr_Representation.Name = "Pr_Representation";
            this.Pr_Representation.Size = new System.Drawing.Size(168, 20);
            this.Pr_Representation.TabIndex = 18;
            this.Pr_Representation.Text = "R:\\FIVE\\EXP\\FIV582\\5 NGS\\WB01";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(118, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Starting Representation";
            // 
            // Pr_SampleID
            // 
            this.Pr_SampleID.Location = new System.Drawing.Point(3, 61);
            this.Pr_SampleID.Name = "Pr_SampleID";
            this.Pr_SampleID.Size = new System.Drawing.Size(168, 20);
            this.Pr_SampleID.TabIndex = 24;
            this.Pr_SampleID.Text = "872";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "SampleID (LIMS)";
            // 
            // Pr_Details
            // 
            this.Pr_Details.Location = new System.Drawing.Point(3, 217);
            this.Pr_Details.Multiline = true;
            this.Pr_Details.Name = "Pr_Details";
            this.Pr_Details.Size = new System.Drawing.Size(168, 199);
            this.Pr_Details.TabIndex = 17;
            this.Pr_Details.Text = "CRISPR KO Human Library, MSPH7 2578 gRNAs, 623 genes. Ordered 20190306. Library w" +
    "as cloned by Monica, then grown in Lenti by Colin. Titer by qPCR method. Then to" +
    " U2OS iCas9 Cells.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 24);
            this.label9.TabIndex = 15;
            this.label9.Text = "Perturbation";
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(701, 475);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(81, 26);
            this.btn_Load.TabIndex = 12;
            this.btn_Load.Text = "Load . .";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(791, 475);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(81, 26);
            this.btn_Save.TabIndex = 13;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // lbl_Template_RaftSeq
            // 
            this.lbl_Template_RaftSeq.AutoSize = true;
            this.lbl_Template_RaftSeq.ForeColor = Color.FromKnownColor(KnownColor.MenuHighlight);
            this.lbl_Template_RaftSeq.Location = new System.Drawing.Point(307, 448);
            this.lbl_Template_RaftSeq.Name = "lbl_Template_RaftSeq";
            this.lbl_Template_RaftSeq.Size = new System.Drawing.Size(96, 13);
            this.lbl_Template_RaftSeq.TabIndex = 17;
            this.lbl_Template_RaftSeq.Text = "Raft-Seq Template";
            // 
            // lbl_Template_Sorting
            // 
            this.lbl_Template_Sorting.AutoSize = true;
            this.lbl_Template_Sorting.ForeColor = Color.FromKnownColor(KnownColor.MenuHighlight);
            this.lbl_Template_Sorting.Location = new System.Drawing.Point(162, 448);
            this.lbl_Template_Sorting.Name = "lbl_Template_Sorting";
            this.lbl_Template_Sorting.Size = new System.Drawing.Size(87, 13);
            this.lbl_Template_Sorting.TabIndex = 18;
            this.lbl_Template_Sorting.Text = "Sorting Template";
            // 
            // lbl_Template_Selection
            // 
            this.lbl_Template_Selection.AutoSize = true;
            this.lbl_Template_Selection.ForeColor = Color.FromKnownColor(KnownColor.MenuHighlight);
            this.lbl_Template_Selection.Location = new System.Drawing.Point(15, 448);
            this.lbl_Template_Selection.Name = "lbl_Template_Selection";
            this.lbl_Template_Selection.Size = new System.Drawing.Size(98, 13);
            this.lbl_Template_Selection.TabIndex = 19;
            this.lbl_Template_Selection.Text = "Selection Template";
            // 
            // lbl_Template_MorphNSort
            // 
            this.lbl_Template_MorphNSort.AutoSize = true;
            this.lbl_Template_MorphNSort.ForeColor = Color.FromKnownColor(KnownColor.MenuHighlight);
            this.lbl_Template_MorphNSort.Location = new System.Drawing.Point(462, 448);
            this.lbl_Template_MorphNSort.Name = "lbl_Template_MorphNSort";
            this.lbl_Template_MorphNSort.Size = new System.Drawing.Size(117, 13);
            this.lbl_Template_MorphNSort.TabIndex = 20;
            this.lbl_Template_MorphNSort.Text = "Morph-N-Sort Template";
            // 
            // btn_SaveAs
            // 
            this.btn_SaveAs.Enabled = false;
            this.btn_SaveAs.Location = new System.Drawing.Point(881, 475);
            this.btn_SaveAs.Name = "btn_SaveAs";
            this.btn_SaveAs.Size = new System.Drawing.Size(81, 26);
            this.btn_SaveAs.TabIndex = 21;
            this.btn_SaveAs.Text = "Save As . .";
            this.btn_SaveAs.UseVisualStyleBackColor = true;
            this.btn_SaveAs.Click += new System.EventHandler(this.btn_SaveAs_Click);
            // 
            // Ex_Status
            // 
            this.Ex_Status.Location = new System.Drawing.Point(67, 478);
            this.Ex_Status.Name = "Ex_Status";
            this.Ex_Status.Size = new System.Drawing.Size(315, 20);
            this.Ex_Status.TabIndex = 23;
            this.Ex_Status.Text = "Seeding on Raft-Array 9/21, Screening Day 9/22";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(15, 481);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Status >";
            // 
            // btn_Layout
            // 
            this.btn_Layout.Location = new System.Drawing.Point(483, 476);
            this.btn_Layout.Name = "btn_Layout";
            this.btn_Layout.Size = new System.Drawing.Size(75, 23);
            this.btn_Layout.TabIndex = 24;
            this.btn_Layout.Text = "Layout";
            this.btn_Layout.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(401, 481);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(76, 13);
            this.label27.TabIndex = 25;
            this.label27.Text = "Class Layout >";
            // 
            // labelClear
            // 
            this.labelClear.AutoSize = true;
            this.labelClear.ForeColor = Color.FromKnownColor(KnownColor.MenuHighlight);
            this.labelClear.Location = new System.Drawing.Point(706, 459);
            this.labelClear.Name = "labelClear";
            this.labelClear.Size = new System.Drawing.Size(31, 13);
            this.labelClear.TabIndex = 26;
            this.labelClear.Text = "Clear";
            this.labelClear.Click += new System.EventHandler(this.labelClear_Click);
            // 
            // DataLineageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 509);
            this.Controls.Add(this.labelClear);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.btn_Layout);
            this.Controls.Add(this.Ex_Status);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.btn_SaveAs);
            this.Controls.Add(this.lbl_Template_MorphNSort);
            this.Controls.Add(this.lbl_Template_Selection);
            this.Controls.Add(this.lbl_Template_Sorting);
            this.Controls.Add(this.lbl_Template_RaftSeq);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_Load);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "DataLineageForm";
            this.Text = "Data Lineage and Metadata";
            this.Load += new System.EventHandler(this.DataLineageForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Cells)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Phenotype)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Genotype)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpDown_Perturbation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Cells;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Ex_Scientist;
        private System.Windows.Forms.TextBox Ex_ExpName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Ex_QuestionPurpose;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox Ex_BaseFolder;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Ex_ProjectName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Ex_Notes;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Label lbl_Template_RaftSeq;
        private System.Windows.Forms.TextBox Ce_Source;
        private System.Windows.Forms.TextBox Ph_StainingReporters;
        private System.Windows.Forms.TextBox Gn_Notes;
        private System.Windows.Forms.TextBox Pr_Details;
        private System.Windows.Forms.Label lbl_Template_Sorting;
        private System.Windows.Forms.Label lbl_Template_Selection;
        private System.Windows.Forms.Label lbl_Template_MorphNSort;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Ce_Treatments;
        private System.Windows.Forms.TextBox Ce_Media;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Ce_SampleID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btn_SaveAs;
        private System.Windows.Forms.TextBox Pr_Representation;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Pr_SampleID;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox Ph_DataLoc;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Pr_Format;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Pr_Name;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Ex_Status;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btn_Layout;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Ph_ModelInfo;
        private System.Windows.Forms.TextBox Ph_SurvivalDetails;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Ph_FlowSettings;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox Ph_ImagingSettings;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox Gn_StrongestAlleleTable;
        private System.Windows.Forms.TextBox Gn_FastQLocations;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox Gn_SampleIDs;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox Gn_PCR1Cycles;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Gn_PreAmpCycles;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Gn_PlateLU;
        private System.Windows.Forms.Label labelClear;
        private System.Windows.Forms.NumericUpDown UpDown_Cells;
        private System.Windows.Forms.NumericUpDown UpDown_Perturbation;
        private System.Windows.Forms.NumericUpDown UpDown_Phenotype;
        private System.Windows.Forms.NumericUpDown UpDown_Genotype;
    }
}