﻿using CommunityToolkit.HighPerformance;
using CommunityToolkit.HighPerformance.Helpers;
using FuzzySharp;
using ImageMagick;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Internal;
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Point = System.Drawing.Point;
using Range = System.Range;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Tensorflow.Util;
using System.Numerics;
using OpenCvSharp.Aruco;
using MathNet.Numerics.Statistics;

namespace FIVE
{

    public static class Generics
    {

        /// <summary>
        /// KDTree<T> is a spatial data structure for organizing points in a k-dimensional space,
        /// enabling efficient nearest neighbor and range queries.
        /// </summary>
        /// <typeparam name="T">The type of elements stored, constrained to implement <see cref="ICoordinate"/>.</typeparam>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Efficiently indexes and queries multi-dimensional data, such as 2D or 3D spatial points.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Efficient nearest neighbor and range search capabilities.</item>
        ///   <item>Support for dynamic rebuilding to maintain balance.</item>
        ///   <item>Customizable with user-defined types implementing <see cref="ICoordinate"/>.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var points = new List<Point> { new Point(1, 2), new Point(3, 4) };
        /// var kdTree = new KDTree<Point>(points);
        /// var nearest = kdTree.FindNearestWithDistance(new PointF(3.5f, 3.5f));
        /// Trace.WriteLine($"Nearest Point: ({nearest.Neighbor.X}, {nearest.Neighbor.Y}), Distance: {nearest.Distance}");
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// RTree<T> is a dynamic, hierarchical spatial data structure for indexing and querying spatial data 
        /// such as rectangles or bounding boxes.
        /// </summary>
        /// <typeparam name="T">The type of items to store, typically associated with rectangles or bounding boxes.</typeparam>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Optimized for spatial indexing and range queries over multi-dimensional data.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Supports dynamic insertion and deletion of spatial data.</item>
        ///   <item>Balances tree nodes automatically for efficient query performance.</item>
        ///   <item>Customizable parameters for minimum and maximum node entries.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var rtree = new RTree<int>();
        /// rtree.Add(new Rectangle(0, 0, 10, 10), 1);
        /// var results = rtree.Search(new Rectangle(5, 5, 15, 15));
        /// Trace.WriteLine($"Found {results.Count} overlapping items.");
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// HybridDictionary<TKey, TValue> is a hybrid collection that switches between array-based and dictionary-based storage 
        /// for optimal performance depending on the number of elements.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Combines the performance benefits of arrays for small datasets with the scalability of dictionaries for larger datasets.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Automatic transition between array and dictionary storage modes.</item>
        ///   <item>Maintains sorted order for array-based storage.</item>
        ///   <item>Supports all typical dictionary operations.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var dict = new HybridDictionary<string, int>();
        /// dict.Add("one", 1);
        /// dict.Add("two", 2);
        /// Trace.WriteLine(dict["one"]); // Outputs: 1
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// SortedList<T> is a collection that maintains its elements in sorted order at all times.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides an automatically sorted list for efficient retrieval and insertion of elements.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Elements are always maintained in sorted order.</item>
        ///   <item>Supports binary search for efficient lookups.</item>
        ///   <item>Implements <see cref="IList{T}"/> for compatibility with list operations.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var sortedList = new SortedList<int>();
        /// sortedList.Add(5);
        /// sortedList.Add(3);
        /// sortedList.Add(8);
        /// Trace.WriteLine(string.Join(", ", sortedList)); // Outputs: 3, 5, 8
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// MultiSet<T> is a collection that allows duplicate elements and tracks their counts.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides a flexible collection for managing and counting duplicate elements.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Tracks counts of each element.</item>
        ///   <item>Supports parallel and bulk operations for large datasets.</item>
        ///   <item>Implements typical set operations such as union, intersection, and difference.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var multiset = new MultiSet<string>();
        /// multiset.Add("apple");
        /// multiset.Add("apple");
        /// Trace.WriteLine(multiset.CountOf("apple")); // Outputs: 2
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// ConcurrentArray<T> is a thread-safe array implementation that supports concurrent operations.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides a thread-safe array for concurrent reads and writes in multi-threaded environments.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Automatically resizes to handle additional elements.</item>
        ///   <item>Supports efficient thread-safe access and modification.</item>
        ///   <item>Uses <see cref="ArrayPool{T}"/> to optimize memory usage.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var array = new ConcurrentArray<int>(initialCapacity: 10);
        /// array.Add(42);
        /// Trace.WriteLine(array[0]); // Outputs: 42
        /// </code>
        /// </remarks>
        /// 
        //KDTree<T>
        /// <summary>
        /// KDTree<T> is a spatial data structure for organizing points in a k-dimensional space,
        /// enabling efficient nearest neighbor and range queries.
        /// </summary>
        /// <typeparam name="T">The type of elements stored, constrained to implement <see cref="ICoordinate"/>.</typeparam>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Efficiently indexes and queries multi-dimensional data, such as 2D or 3D spatial points.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Efficient nearest neighbor and range search capabilities.</item>
        ///   <item>Support for dynamic rebuilding to maintain balance.</item>
        ///   <item>Customizable with user-defined types implementing <see cref="ICoordinate"/>.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var points = new List<Point> { new Point(1, 2), new Point(3, 4) };
        /// var kdTree = new KDTree<Point>(points);
        /// var nearest = kdTree.FindNearestWithDistance(new PointF(3.5f, 3.5f));
        /// Trace.WriteLine($"Nearest Point: ({nearest.Neighbor.X}, {nearest.Neighbor.Y}), Distance: {nearest.Distance}");
        /// </code>
        /// </remarks>
        ///RTree<T>
        /// <summary>
        /// RTree<T> is a dynamic, hierarchical spatial data structure for indexing and querying spatial data 
        /// such as rectangles or bounding boxes.
        /// </summary>
        /// <typeparam name="T">The type of items to store, typically associated with rectangles or bounding boxes.</typeparam>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Optimized for spatial indexing and range queries over multi-dimensional data.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Supports dynamic insertion and deletion of spatial data.</item>
        ///   <item>Balances tree nodes automatically for efficient query performance.</item>
        ///   <item>Customizable parameters for minimum and maximum node entries.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var rtree = new RTree<int>();
        /// rtree.Add(new Rectangle(0, 0, 10, 10), 1);
        /// var results = rtree.Search(new Rectangle(5, 5, 15, 15));
        /// Trace.WriteLine($"Found {results.Count} overlapping items.");
        /// </code>
        /// </remarks>
        /// HybridDictionary<TKey, TValue>
        /// <summary>
        /// HybridDictionary<TKey, TValue> is a hybrid collection that switches between array-based and dictionary-based storage 
        /// for optimal performance depending on the number of elements.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Combines the performance benefits of arrays for small datasets with the scalability of dictionaries for larger datasets.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Automatic transition between array and dictionary storage modes.</item>
        ///   <item>Maintains sorted order for array-based storage.</item>
        ///   <item>Supports all typical dictionary operations.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var dict = new HybridDictionary<string, int>();
        /// dict.Add("one", 1);
        /// dict.Add("two", 2);
        /// Trace.WriteLine(dict["one"]); // Outputs: 1
        /// </code>
        /// </remarks>
        ///SortedList<T>
        /// <summary>
        /// SortedList<T> is a collection that maintains its elements in sorted order at all times.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides an automatically sorted list for efficient retrieval and insertion of elements.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Elements are always maintained in sorted order.</item>
        ///   <item>Supports binary search for efficient lookups.</item>
        ///   <item>Implements <see cref="IList{T}"/> for compatibility with list operations.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var sortedList = new SortedList<int>();
        /// sortedList.Add(5);
        /// sortedList.Add(3);
        /// sortedList.Add(8);
        /// Trace.WriteLine(string.Join(", ", sortedList)); // Outputs: 3, 5, 8
        /// </code>
        /// </remarks>
        //MultiSet<T>
        /// <summary>
        /// MultiSet<T> is a collection that allows duplicate elements and tracks their counts.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides a flexible collection for managing and counting duplicate elements.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Tracks counts of each element.</item>
        ///   <item>Supports parallel and bulk operations for large datasets.</item>
        ///   <item>Implements typical set operations such as union, intersection, and difference.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var multiset = new MultiSet<string>();
        /// multiset.Add("apple");
        /// multiset.Add("apple");
        /// Trace.WriteLine(multiset.CountOf("apple")); // Outputs: 2
        /// </code>
        /// </remarks>
        /// <summary>
        /// ConcurrentArray<T> is a thread-safe array implementation that supports concurrent operations.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides a thread-safe array for concurrent reads and writes in multi-threaded environments.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Automatically resizes to handle additional elements.</item>
        ///   <item>Supports efficient thread-safe access and modification.</item>
        ///   <item>Uses <see cref="ArrayPool{T}"/> to optimize memory usage.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var array = new ConcurrentArray<int>(initialCapacity: 10);
        /// array.Add(42);
        /// Trace.WriteLine(array[0]); // Outputs: 42
        /// </code>
        /// </remarks>
        /// <summary>
        /// ConcurrentSet<T> is a thread-safe implementation of a set, allowing concurrent access and modifications.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Manages a thread-safe collection of unique elements with efficient concurrent operations.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Ensures uniqueness of elements in a multi-threaded environment.</item>
        ///   <item>Supports parallel and bulk set operations.</item>
        ///   <item>Implements <see cref="ISet{T}"/> for compatibility with standard set operations.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var set = new ConcurrentSet<string>();
        /// set.Add("apple");
        /// set.Add("banana");
        /// Trace.WriteLine(set.Contains("apple")); // Outputs: True
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// DataGenerator is a utility class for generating synthetic data from various collections and data structures.
        /// </summary>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Automates the creation of randomized or patterned synthetic data for testing and simulation purposes.
        /// 
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Supports a wide range of data sources (arrays, dictionaries, sets, tuples, etc.).</item>
        ///   <item>Customizable data generation using factories or predefined collections.</item>
        ///   <item>Generates nested or hierarchical data structures.</item>
        /// </list>
        /// 
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// var generator = DataGenerator.FromArray(new[] { 1, 2, 3 }, 5);
        /// foreach (var item in generator.GenerateData())
        /// {
        ///     Trace.WriteLine(item);
        /// }
        /// </code>
        /// </remarks>
        /// /// <summary>
        /// Attempts to safely cast an object to the specified type `T` without throwing an exception on failure.
        /// </summary>
        /// <typeparam name="T">The target type to cast the input object to.</typeparam>
        /// <param name="value">The object to attempt casting.</param>
        /// <param name="result">
        /// The result of the cast if successful, or the default value of type `T` if the cast fails.
        /// </param>
        /// <returns>
        /// <c>true</c> if the cast is successful; otherwise, <c>false</c>.
        /// </returns>
        /// 
        /// <remarks>
        /// <para><strong>Purpose:</strong></para>
        /// Provides a robust method to cast an object to a specific type, avoiding exceptions in cases of invalid casts.
        /// This is particularly useful in scenarios involving dynamic data or unknown types.
        ///
        /// <para><strong>Key Features:</strong></para>
        /// <list type="bullet">
        ///   <item>Handles common primitive types (e.g., <c>int</c>, <c>float</c>, <c>bool</c>) with optimized cases.</item>
        ///   <item>Supports nullable and enum types with special handling.</item>
        ///   <item>Falls back to <c>Convert.ChangeType</c> for general casting scenarios.</item>
        ///   <item>Gracefully returns <c>false</c> without exceptions for unsupported or invalid casts.</item>
        /// </list>
        ///
        /// <para><strong>Example Usage:</strong></para>
        /// <code>
        /// object input = "123";
        /// if (TryCast<int>(input, out var result))
        /// {
        ///     Trace.WriteLine($"Cast succeeded: {result}");
        /// }
        /// else
        /// {
        ///     Trace.WriteLine("Cast failed.");
        /// }
        /// </code>
        /// <code>
        /// object enumInput = "Running";
        /// if (TryCast<ProcessState>(enumInput, out var enumResult))
        /// {
        ///     Trace.WriteLine($"Cast to enum succeeded: {enumResult}");
        /// }
        /// else
        /// {
        ///     Trace.WriteLine("Failed to cast to enum.");
        /// }
        /// </code>
        /// </remarks>
        /// 
        public static class InterlockedExtensions
        {
            public class Ref<T> where T : notnull, IComparable<T>
            {
                private T _value;

                public T Value
                {
                    get => _value;
                    set => _value = value;
                }

                public ref T GetValueRef()
                {
                    return ref _value;
                }

                public Ref(T value)
                {
                    _value = value;
                }
            }
            public static void AtomicMath<T>(Ref<T> target, Func<T, T> func) where T : class, IComparable<T>
            {
                T currentValue;
                T newValue;

                do
                {
                    // Read the current value atomically
                    currentValue = target.Value;

                    // Apply the function to calculate the new value
                    newValue = func(currentValue);

                    // Atomically attempt to update the value
                } while (Interlocked.CompareExchange(ref target.GetValueRef(), newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref byte target, Func<byte, byte> Function)
            {
                int intTarget = target; // Copy the byte into an int for atomic operations
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = Function.Invoke((byte)currentValue); // Apply the function
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
                target = (byte)intTarget; // Update the original byte value
            }
            public static void AtomicMath(ref Ref<BigInteger> target, Func<BigInteger, Ref<BigInteger>> Function)
            {
                Ref<BigInteger> currentValue;
                Ref<BigInteger> newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, default,default); // Atomic read
                    newValue = Function.Invoke(currentValue.Value);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref sbyte target, Func<sbyte, sbyte> Function)
            {
                int intTarget = target; // Copy the byte into an int for atomic operations
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = Function.Invoke((sbyte)currentValue); // Apply the function
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
                target = (sbyte)intTarget; // Update the original byte value
            }

            public static void AtomicMath<T>(ref T target, Func<T, T> Function) where T:class
            {
                T currentValue;
                T newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange<T>(ref target, default, default); // Atomic read
                    newValue = Function.Invoke(currentValue); // Apply the function
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref short target, Func<short, short> Function)
            {
                int intTarget = target; // Copy the byte into an int for atomic operations
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = Function.Invoke((short)currentValue); // Apply the function
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
                target = (short)intTarget; // Update the original byte value
            }
            public static void AtomicMath(ref ushort target, Func<ushort, ushort> Function)
            {
                int intTarget = target; // Copy the byte into an int for atomic operations
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = Function.Invoke((ushort)currentValue); // Apply the function
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
                target = (ushort)intTarget; // Update the original byte value
            }
            public static void AtomicMath(ref object target, Func<object, object> Function)
            {
                object currentValue;
                object newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0, 0.0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMath(ref uint target, Func<uint, uint> Function)
            {
                uint currentValue;
                uint newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMath(ref int target, Func<int, int> Function)
            {
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref IntPtr target, Func<IntPtr, IntPtr> Function)
            {
                IntPtr currentValue;
                IntPtr newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMath(ref UIntPtr target, Func<UIntPtr, UIntPtr> Function)
            {
                UIntPtr currentValue;
                UIntPtr newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref long target, Func<long, long> Function)
            {
                long currentValue;
                long newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref ulong target, Func<ulong, ulong> Function)
            {
                ulong currentValue;
                ulong newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref double target, Func<double, double> Function)
            {
                double currentValue;
                double newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0, 0.0); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMath(ref object target, Func<BigInteger, BigInteger> Function)
            {
                object currentValue;
                BigInteger newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0f, 0.0f); // Atomic read
                    newValue = Function.Invoke((BigInteger)currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicReciprocal(ref ushort target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);

                target = (ushort)intTarget; // Demote back to short
            }

            public static void AtomicReciprocal(ref double target)
            {
                double intTarget = target; // Promote to int for atomic operations
                double currentValue;
                double newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }
            public static void AtomicReciprocal(ref float target)
            {
                float intTarget = target; // Promote to int for atomic operations
                float currentValue;
                float newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = MathF.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }
            public static void AtomicReciprocal(ref int target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }

            public static void AtomicReciprocal(ref uint target)
            {
                uint intTarget = target; // Promote to int for atomic operations
                uint currentValue;
                uint newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (uint)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }
            public static void AtomicReciprocal(ref long target)
            {
                long intTarget = target; // Promote to int for atomic operations
                long currentValue;
                long newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (long)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }

            public static void AtomicReciprocal(ref ulong target)
            {
                ulong intTarget = target; // Promote to int for atomic operations
                ulong currentValue;
                ulong newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (ulong)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);
            }
            public static void AtomicReciprocal(ref short target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);

                target = (short)intTarget; // Demote back to short
            }
            public static void AtomicReciprocal(ref byte target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.ReciprocalEstimate(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);

                target = (byte)intTarget; // Demote back to short
            }

            public static void AtomicMath(ref float target, Func<float, float> Function)
            {
                float currentValue;
                float newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0f, 0.0f); // Atomic read
                    newValue = Function.Invoke(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicSqrt(ref short target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.Sqrt(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);

                target = (short)intTarget; // Demote back to short
            }
            public static void AtomicMin(ref ushort target, ushort value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (ushort)intTarget; // Demote back to a byte
            }
            public static void AtomicSqrt(ref ushort target)
            {
                int intTarget = target; // Promote to int for atomic operations
                int currentValue;
                int newValue;

                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    newValue = (int)Math.Sqrt(currentValue); // Calculate square root
                } while (Interlocked.CompareExchange(ref intTarget, newValue, currentValue) != currentValue);

                target = (ushort)intTarget; // Demote back to short
            }
            public static void AtomicMax(ref ushort target, ushort value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (ushort)intTarget; // Demote back to a byte
            }

            public static void AtomicMin(ref short target, short value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (short)intTarget; // Demote back to a byte
            }
            public static void AtomicMax(ref short target, short value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (short)intTarget; // Demote back to a byte
            }
            public static void AtomicMin(ref byte target, byte value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (byte)intTarget; // Demote back to a byte
            }
            public static void AtomicMax(ref byte target, byte value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (byte)intTarget; // Demote back to a byte
            }
            public static void AtomicMin(ref sbyte target, sbyte value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (sbyte)intTarget; // Demote back to a byte
            }
            public static void AtomicMax(ref sbyte target, sbyte value)
            {
                int intTarget = target; // Promote to an int
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref intTarget, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref intTarget, value, currentValue) != currentValue);
                target = (sbyte)intTarget; // Demote back to a byte
            }
            public static void AtomicMax(ref int target, int value)
            {
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }
            public static void AtomicSqrt(ref int target)
            {
                int currentValue;
                int newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = (int)Math.Sqrt(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }
            public static void AtomicMin(ref int target, int value)
            {
                int currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicMax(ref uint target, uint value)
            {
                uint currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicSqrt(ref uint target)
            {
                uint currentValue;
                uint newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = (uint)Math.Sqrt(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMin(ref uint target, uint value)
            {
                uint currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicMax(ref ulong target, ulong value)
            {
                ulong currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicSqrt(ref ulong target)
            {
                ulong currentValue;
                ulong newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    newValue = (ulong)Math.Sqrt(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMin(ref ulong target, ulong value)
            {
                ulong currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0, 0); // Atomic read
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicMax(ref float target, float value)
            {
                float currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0f, 0.0f); // Atomic read
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicSqrt(ref float target)
            {
                float currentValue;
                float newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0f, 0.0f); // Atomic read
                    newValue = MathF.Sqrt(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMin(ref float target, float value)
            {
                float currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0f, 0.0f); // Atomic read
                    if (value >= currentValue) break; // No update
                } while (Interlocked.CompareExchange(ref target, currentValue, currentValue) != currentValue);
            }

            public static void AtomicMax(ref double target, double value)
            {
                double currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0, 0.0);
                    if (value <= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }

            public static void AtomicSqrt(ref double target)
            {
                double currentValue;
                double newValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0, 0.0);
                    newValue = Math.Sqrt(currentValue);
                } while (Interlocked.CompareExchange(ref target, newValue, currentValue) != currentValue);
            }

            public static void AtomicMin(ref double target, double value)
            {
                double currentValue;
                do
                {
                    currentValue = Interlocked.CompareExchange(ref target, 0.0, 0.0);
                    if (value >= currentValue) break; // No update needed
                } while (Interlocked.CompareExchange(ref target, value, currentValue) != currentValue);
            }
            public static void AtomicMin(ref DateTime target, DateTime value)
            {
                long currentTicks;
                long valueTicks = value.Ticks;

                do
                {
                    currentTicks = Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), 0, 0); // Atomic read
                    if (valueTicks >= currentTicks) break; // No update needed if value >= current
                } while (Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), valueTicks, currentTicks) != currentTicks);
            }

            public static void AtomicMax(ref DateTime target, DateTime value)
            {
                long currentTicks;
                long valueTicks = value.Ticks;

                do
                {
                    currentTicks = Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), 0, 0); // Atomic read
                    if (valueTicks <= currentTicks) break; // No update needed if value <= current
                } while (Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), valueTicks, currentTicks) != currentTicks);
            }

            public static void AtomicDateSubtract(ref DateTime target, TimeSpan value)
            {
                long subtractTicks = value.Ticks; // The number of ticks to subtract
                long currentTicks;
                long newTicks;

                do
                {
                    currentTicks = Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), 0, 0); // Atomic read
                    newTicks = currentTicks - subtractTicks; // Perform subtraction
                    if (newTicks < DateTime.MinValue.Ticks)
                        throw new ArgumentOutOfRangeException(nameof(target), "Resulting DateTime is out of range.");
                } while (Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), newTicks, currentTicks) != currentTicks);
            }
            public static void AtomicDateAdd(ref DateTime target, TimeSpan value)
            {
                long addTicks = value.Ticks; // The number of ticks to add
                long currentTicks;
                long newTicks;

                do
                {
                    currentTicks = Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), 0, 0); // Atomic read
                    newTicks = currentTicks + addTicks; // Perform addition
                    if (newTicks > DateTime.MaxValue.Ticks)
                        throw new ArgumentOutOfRangeException(nameof(target), "Resulting DateTime is out of range.");
                } while (Interlocked.CompareExchange(ref Unsafe.As<DateTime, long>(ref target), newTicks, currentTicks) != currentTicks);
            }
        }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static unsafe void Adjust<TQuantumType>(
     this IMagickImage<TQuantumType> self,

     ValueTuple<float, float, float> BGR)
     where TQuantumType : struct, IConvertible
        {

            float blue = BGR.Item1;
            float green = BGR.Item2;
            float red = BGR.Item3;
            IMagickImage<TQuantumType> magickImage = self;
            int height = magickImage.Height;
            int width = magickImage.Width;
            IUnsafePixelCollection<TQuantumType> pixels = magickImage.GetPixelsUnsafe();
            nint area = pixels.GetAreaPointer(0, 0, width, height);
            ushort* srcPtr = (ushort*)area;
            float blueFactor = (blue == 1) ? 1.0f : BGR.Item1;
            float greenFactor = (red == 1) ? 1.0f : BGR.Item2;
            float redFactor = (green == 1) ? 1.0f : BGR.Item3;
            int channels = magickImage.ChannelCount;

            Parallel.For(0, height, y =>
            {
                ushort* srcRowPtr = srcPtr + (y * width);
                for (int x = 0; x < width; x = x + 3)
                {
                    // Pointer to the current pixel in the row, using stride
                    ushort* pixelValuePtr = srcRowPtr + x;  // Each pixel has 3 channels


                    // Apply the factor to each channel using pointer dereferencing
                    *(pixelValuePtr) = (ushort)(*(pixelValuePtr) * blueFactor);    // Blue channel
                    *(pixelValuePtr + 1) = (ushort)(*(pixelValuePtr + 1) * greenFactor);  // Green channel
                    *(pixelValuePtr + 2) = (ushort)(*(pixelValuePtr + 2) * redFactor);
                }
            });

        }

        public static int BinarySearch<T>(IList<T> list, T target) where T : IComparable<T>
        {
            int left = 0;
            int right = list.Count - 1;

            while (left <= right)
            {
                int mid = left + (right - left) / 2; // Prevent overflow
                int comparison = list[mid].CompareTo(target);

                if (comparison == 0)
                {
                    return mid; // Target found
                }
                else if (comparison < 0)
                {
                    left = mid + 1; // Search in the right half
                }
                else
                {
                    right = mid - 1; // Search in the left half
                }
            }

            return ~left; // Target not found, return bitwise complement of insertion point
        }

        public static Bitmap DeepCloneBitmap(Bitmap source)
        {
            Rectangle rect = new Rectangle(0, 0, source.Width, source.Height);
            BitmapData sourceData = source.LockBits(rect, ImageLockMode.ReadOnly, source.PixelFormat);

            // Allocate a new buffer for pixel data
            int bufferSize = sourceData.Stride * source.Height;
            IntPtr newBuffer = Marshal.AllocHGlobal(bufferSize);

            try
            {
                // Copy data from source to the new buffer
                unsafe
                {
                    Buffer.MemoryCopy((void*)sourceData.Scan0, (void*)newBuffer, bufferSize, bufferSize);
                }

                // Create new bitmap using the new buffer
                Bitmap clonedBitmap = new Bitmap(source.Width, source.Height, sourceData.Stride, source.PixelFormat, newBuffer);

                return clonedBitmap;
            }
            finally
            {
                source.UnlockBits(sourceData);
                Marshal.FreeHGlobal(newBuffer); // Cleanup if not needed afterward
            }
        }
        public unsafe sealed class MutableBitmap : IDisposable
        {
            private readonly Bitmap _bitmap;
            private BitmapData _bitmapData;
            private bool _disposed;
            private bool _isLocked;
            public byte* Scan
            {
                get
                {
                    return (byte*)_bitmapData.Scan0;
                }
            }
            public int Stride
            {
                get
                {
                    return _bitmapData.Stride;
                }
            }
            public static class Metadata
            {
                // Properties
                public static int MinValue { get; set; }
                public static int MaxValue { get; set; }
                public static long Sum { get; set; }
                public static long SumSquared { get; set; }
                public static float Variance { get; set; }
                public static float Mean { get; set; }
                public static float StandardDeviation { get; set; }
                public static byte[,] _coordinateData { get; set; }
                public static void Compute(byte[,] data)
                {
                    ExtractMetadata(data);
                }
            }
            public PixelFormat PixelFormat { get; }
            public Rectangle Rect { get; }
            public int Width { get; set; }
            public int Height { get; set; }
            public int TotalPixels { get; set; }
            public Point Location { get; }

            public (float XRes, float YRes) ResolutionXY;
            public System.Drawing.Size Size { get; }

            public byte[,] CoordinateData
            {
                get
                {
                    if (Metadata._coordinateData == null)
                    {
                        if (!_isLocked)
                        {
                            _bitmapData = _bitmap.LockBits(Rect, ImageLockMode.ReadWrite, PixelFormat);
                            _isLocked = true;
                        }
                        TakeApart(_bitmapData);
                    }
                    return Metadata._coordinateData;
                }
            }

            public MutableBitmap(Bitmap bitmap, PixelFormat pixelFormat = PixelFormat.Format24bppRgb, bool GetMetaData = false, bool GetMetadata = false, Point p = default)
            {
                if (bitmap == null)
                    throw new ArgumentNullException(nameof(bitmap));

                _bitmap = bitmap;
                PixelFormat = pixelFormat;
                // Cache these once, to avoid repeated GDI+ calls
                Width = bitmap.Width;
                Height = bitmap.Height;
                this.TotalPixels = Width * Height;
                Size = bitmap.Size;
                ResolutionXY.XRes = bitmap.HorizontalResolution;
                ResolutionXY.YRes = bitmap.VerticalResolution;
                this.Rect = default ? new Rectangle(0, 0, Width, Height) : new Rectangle(p, this.Size);
                Location = Rect.Location;
                _bitmapData = _bitmap.LockBits(Rect, ImageLockMode.ReadWrite, PixelFormat);
                _isLocked = true;
                if (GetMetaData)
                {
                    TakeApart(_bitmapData);
                }
            }
            public static void TakeApart(BitmapData bitmap)
            {
                int width = bitmap.Width;
                int height = bitmap.Height;
                byte[,] coordinates = new byte[height, width];
                byte* scan = (byte*)bitmap.Scan0;
                int stride = bitmap.Stride;

                    // Global aggregates for metadata
                    int globalMin = byte.MaxValue;
                    int globalMax = byte.MinValue;
                    long globalSum = 0;
                    long globalSumSq = 0;

                    Parallel.For(0, height, y =>
                    {
                        // Thread-local aggregates
                        int localMin = byte.MaxValue;
                        int localMax = byte.MinValue;
                        long localSum = 0;
                        long localSumSq = 0;

                        byte* currentLine = scan + (y * stride);
                        for (int x = 0; x < width; x = x+3)
                        {
                            byte redPixel = currentLine[x + 2];
                            coordinates[y, x] = redPixel;

                            // Update thread-local aggregates
                            localMin = redPixel < localMin ? redPixel : localMin;
                            localMax = redPixel > localMax ? redPixel : localMax;
                            localSum += redPixel;
                            localSumSq += redPixel * redPixel;
                        }

                        // Aggregate thread-local metadata
                        Interlocked.Add(ref globalSum, localSum);
                        Interlocked.Add(ref globalSumSq, localSumSq);

                        int originalMin, newMin;
                        do
                        {
                            originalMin = globalMin;
                            newMin = Math.Min(originalMin, localMin);
                        }
                        while (originalMin != Interlocked.CompareExchange(ref globalMin, newMin, originalMin));

                        int originalMax, newMax;
                        do
                        {
                            originalMax = globalMax;
                            newMax = Math.Max(originalMax, localMax);
                        }
                        while (originalMax != Interlocked.CompareExchange(ref globalMax, newMax, originalMax));
                    });

                    // Finalize and print metadata
                    double totalPixels = width * height;
                    Metadata.MinValue = globalMin;
                    Metadata.MaxValue = globalMax;
                    Metadata._coordinateData = coordinates;
                    Metadata.Sum = globalSum;
                    Metadata.SumSquared = globalSumSq;
                    Metadata.Mean = (float)(globalSum / totalPixels);
                    Metadata.Variance = (float)((globalSumSq / totalPixels) - (Metadata.Mean * Metadata.Mean));
                    Metadata.StandardDeviation = MathF.Sqrt(Metadata.Variance);
      
            }
            public static void ExtractMetadata(byte[,] existingCoordinates)
            {
                if (existingCoordinates == null)
                    throw new ArgumentNullException(nameof(existingCoordinates), "The input array cannot be null.");

                int height = existingCoordinates.GetLength(0);
                int width = existingCoordinates.GetLength(1);

                // Global aggregates for metadata
                int globalMin = byte.MaxValue;
                int globalMax = byte.MinValue;
                long globalSum = 0;
                long globalSumSq = 0;

                // Pin the array in memory
                GCHandle handle = GCHandle.Alloc(existingCoordinates, GCHandleType.Pinned);
                try
                {
                    // Get a pointer to the pinned array
                    byte* pinnedArray = (byte*)handle.AddrOfPinnedObject();

                    Parallel.For(0, height, y =>
                    {
                        // Thread-local aggregates
                        int localMin = byte.MaxValue;
                        int localMax = byte.MinValue;
                        long localSum = 0;
                        long localSumSq = 0;

                        for (int x = 0; x < width; x++)
                        {
                            // Access the pinned array directly
                            byte value = pinnedArray[y * width + x];

                            // Update thread-local aggregates
                            localMin = value < localMin ? value : localMin;
                            localMax = value > localMax ? value : localMax;
                            localSum += value;
                            localSumSq += value * value;
                        }

                        // Atomically update global aggregates
                        Interlocked.Add(ref globalSum, localSum);
                        Interlocked.Add(ref globalSumSq, localSumSq);

                        int originalMin, newMin;
                        do
                        {
                            originalMin = globalMin;
                            newMin = Math.Min(originalMin, localMin);
                        }
                        while (originalMin != Interlocked.CompareExchange(ref globalMin, newMin, originalMin));

                        int originalMax, newMax;
                        do
                        {
                            originalMax = globalMax;
                            newMax = Math.Max(originalMax, localMax);
                        }
                        while (originalMax != Interlocked.CompareExchange(ref globalMax, newMax, originalMax));
                    });

                    // Final metadata calculations
                    double totalPixels = width * height;
                    double mean = globalSum / totalPixels;
                    double variance = (globalSumSq / totalPixels) - (mean * mean);
                    double stdDev = Math.Sqrt(variance);

                    // Assign values to metadata properties
                    Metadata.MinValue = globalMin;
                    Metadata.MaxValue = globalMax;
                    Metadata.Sum = globalSum;
                    Metadata.SumSquared = globalSumSq;
                    Metadata.Mean = (float)mean;
                    Metadata.Variance = (float)variance;
                    Metadata.StandardDeviation = (float)stdDev;
                }
                finally
                {
                    // Release the pinned handle
                    handle.Free();
                }
            }

            /// <summary>
            /// Unlocks the underlying bitmap bits if locked.
            /// You can call this explicitly, or rely on Dispose() to do it automatically.
            /// </summary>
            public void Unlock()
            {
                ThrowIfDisposed();
                if (!_isLocked)
                    throw new InvalidOperationException("Bitmap is already unlocked.");

                _bitmap.UnlockBits(_bitmapData);
                _isLocked = false;
                _bitmapData = null;
            }

            /// <summary>
            /// If still locked, unlock on dispose.
            /// </summary>
            public void Dispose()
            {
                if (_disposed) return; // allow double Dispose without throwing
                _disposed = true;

                if (_isLocked)
                {
                    _bitmap.UnlockBits(_bitmapData);
                    _isLocked = false;
                }
                _bitmapData = null;
            }

            private void ThrowIfDisposed()
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(MutableBitmap));
            }
        }
        public static unsafe void Copy(string src, IntPtr dest, int len)
        {
            if (len != 0)
            {
                fixed (char* rawStringData = src)
                {
                    Memmove(ref *(byte*)dest, ref Unsafe.As<char, byte>(ref *rawStringData), (nuint)len);
                }
            }
        }
        public static void Memmove(ref byte dest, ref byte src, nuint len)
        {
            // Handle overlapping buffers
            if (((nuint)Unsafe.ByteOffset(ref src, ref dest) < len) || ((nuint)Unsafe.ByteOffset(ref dest, ref src) < len))
            {
                HandleOverlap(ref dest, ref src, len);
                return;
            }

            // Process in chunks of 64 bytes
            while (len >= 64)
            {
                Unsafe.As<byte, long>(ref dest) = Unsafe.As<byte, long>(ref src);
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 8)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 8));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 16)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 16));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 24)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 24));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 32)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 32));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 40)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 40));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 48)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 48));
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 56)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 56));
                dest = ref Unsafe.Add(ref dest, 64);
                src = ref Unsafe.Add(ref src, 64);
                len -= 64;
            }

            // Process in chunks of 32 bytes
            while (len >= 32)
            {
                Unsafe.As<byte, long>(ref dest) = Unsafe.As<byte, long>(ref src);
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 16)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 16));
                dest = ref Unsafe.Add(ref dest, 32);
                src = ref Unsafe.Add(ref src, 32);
                len -= 32;
            }

            // Process in chunks of 16 bytes
            while (len >= 16)
            {
                Unsafe.As<byte, long>(ref dest) = Unsafe.As<byte, long>(ref src);
                Unsafe.As<byte, long>(ref Unsafe.Add(ref dest, 8)) = Unsafe.As<byte, long>(ref Unsafe.Add(ref src, 8));
                dest = ref Unsafe.Add(ref dest, 16);
                src = ref Unsafe.Add(ref src, 16);
                len -= 16;
            }

            // Process in chunks of 8 bytes
            if (len >= 8)
            {
                Unsafe.As<byte, long>(ref dest) = Unsafe.As<byte, long>(ref src);
                dest = ref Unsafe.Add(ref dest, 8);
                src = ref Unsafe.Add(ref src, 8);
                len -= 8;
            }

            // Process in chunks of 4 bytes
            if (len >= 4)
            {
                Unsafe.As<byte, int>(ref dest) = Unsafe.As<byte, int>(ref src);
                dest = ref Unsafe.Add(ref dest, 4);
                src = ref Unsafe.Add(ref src, 4);
                len -= 4;
            }

            // Process remaining bytes
            while (len > 0)
            {
                dest = src;
                dest = ref Unsafe.Add(ref dest, 1);
                src = ref Unsafe.Add(ref src, 1);
                len--;
            }
        }
        private static void HandleOverlap(ref byte dest, ref byte src, nuint len)
        {
            if (Unsafe.AreSame(ref dest, ref src))
            {
                // Perfect overlap, no copy needed
                return;
            }

            // Handle overlap by copying backwards
            for (nuint i = len; i > 0; i--)
            {
                Unsafe.Add(ref dest, (int)(i - 1)) = Unsafe.Add(ref src, (int)(i - 1));
            }
        }

        public static unsafe T[] ToArrayFast<T>(this ICollection<T> data, int rows, int columns, int channels, bool keys = false) where T : unmanaged
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (rows <= 0)
                throw new ArgumentOutOfRangeException(nameof(rows), "Number of rows must be positive.");
            if (columns <= 0)
                throw new ArgumentOutOfRangeException(nameof(columns), "Number of columns must be positive.");
            if (channels <= 0)
                throw new ArgumentOutOfRangeException(nameof(channels), "Number of channels must be positive.");

            int totalSize = rows * columns * channels;
            if (data.Count != totalSize)
                throw new ArgumentException("Data count does not match the specified dimensions.");

            T[] result = new T[totalSize];

            // Check for contiguous types
            if (data is T[] array)
            {
                // Direct memory copy for arrays
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                try
                {
                    IntPtr sourcePtr = handle.AddrOfPinnedObject();
                    fixed (T* destPtr = result)
                    {
                        System.Buffer.MemoryCopy((void*)sourcePtr, destPtr, totalSize * sizeof(T), totalSize * sizeof(T));
                    }
                }
                finally
                {
                    handle.Free();
                }
            }
            else if (data is List<T> list)
            {
                // Safe copy for List<T>
                list.CopyTo(result, 0);
            }

            else if (data is ReadOnlyMemory<T> RMemory)
            {
                // Safe copy for List<T>
                return RMemory.ToArray();
            }
            else if (data is Memory<T> Memory)
            {
                // Safe copy for List<T>
               return  Memory.ToArray();
            }
            else if(data is ArraySegment<T> Segment)
            {
                Segment.CopyTo(result, 0);
                return result;
            }
            else if (data is SortedList<T> sortedList)
            {
                GCHandle handle = GCHandle.Alloc(sortedList, GCHandleType.Pinned);
                try
                {
                    IntPtr sourcePtr = handle.AddrOfPinnedObject();
                    fixed (T* destPtr = result)
                    {
                        System.Buffer.MemoryCopy((void*)sourcePtr, destPtr, totalSize * sizeof(T), totalSize * sizeof(T));
                    }
                }
                finally
                {
                    handle.Free();
                }
            }
            else if (data is Dictionary<T, T> dictionary)
            {
                // Copy values from Dictionary
                int index = 0;
                foreach (var key in dictionary.Keys)
                {
                    result[index++] = key;
                }
                foreach (var value in dictionary.Values)
                {
                    result[index++] = value;
                }
             
            }
            else
            {
                // Fallback for non-contiguous types
                int index = 0;
                foreach (var item in data)
                {
                    if (index >= totalSize)
                        throw new ArgumentException("Data exceeds the specified dimensions.");
                    result[index++] = item;
                }
                if (index != totalSize)
                    throw new ArgumentException("Data count does not match the specified dimensions.");
            }

            return result;
        }
        public static unsafe T[,] ToArray2DFast<T>(this T[] data, int rows, int columns, int channels) where T : unmanaged
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            if (rows <= 0)
                throw new ArgumentOutOfRangeException(nameof(rows), "Number of rows must be positive.");
            if (columns <= 0)
                throw new ArgumentOutOfRangeException(nameof(columns), "Number of columns must be positive.");
            if (channels <= 0)
                throw new ArgumentOutOfRangeException(nameof(channels), "Number of channels must be positive.");
            if (data.Length != rows * columns * channels)
                throw new ArgumentException("The length of the data array does not match the specified dimensions and channels.");

            T[,] result = new T[rows, columns * channels]; // Adjust for channels in the 2D array layout

            int totalSizeInBytes = data.Length * sizeof(T);

            fixed (T* sourcePtr = data)
            fixed (T* destPtr = result)
            {
                System.Buffer.MemoryCopy(sourcePtr, destPtr, totalSizeInBytes, totalSizeInBytes);
            }

            return result;
        }

        private static void SetBitmapDensity<TQuantumType>(IMagickImage<TQuantumType> image, Bitmap bitmap, bool useDpi)
            where TQuantumType : struct, IConvertible
        {
            if (useDpi)
            {
                var dpi = GetDefaultDensity(image, useDpi ? DensityUnit.PixelsPerInch : DensityUnit.Undefined);
                bitmap.SetResolution((float)dpi.X, (float)dpi.Y);
            }
        }

        private static Density GetDefaultDensity(IMagickImage image, DensityUnit units)
        {
            //Throw.IfNull(nameof(image), image);

            if (units == DensityUnit.Undefined || (image.Density.Units == DensityUnit.Undefined && image.Density.X == 0 && image.Density.Y == 0))
                return new Density(96);

            return image.Density.ChangeUnits(units);
        }

        private static string GetMapping(PixelFormat format)
        {
            switch (format)
            {
                case PixelFormat.Format24bppRgb:
                    return "BGR";
                case PixelFormat.Format32bppArgb:
                    return "BGRA";
                default:
                    throw new NotImplementedException(format.ToString());
            }
        }

        private static bool IsSupportedImageFormat(ImageFormat format)
            => format.Guid.Equals(ImageFormat.Bmp.Guid) ||
               format.Guid.Equals(ImageFormat.Gif.Guid) ||
               format.Guid.Equals(ImageFormat.Icon.Guid) ||
               format.Guid.Equals(ImageFormat.Jpeg.Guid) ||
               format.Guid.Equals(ImageFormat.Png.Guid) ||
               format.Guid.Equals(ImageFormat.Tiff.Guid);

        private static bool IssRGBCompatibleColorspace(ColorSpace colorSpace)
            => colorSpace == ColorSpace.sRGB ||
               colorSpace == ColorSpace.RGB ||
               colorSpace == ColorSpace.scRGB ||
               colorSpace == ColorSpace.Transparent ||
               colorSpace == ColorSpace.Gray ||
               colorSpace == ColorSpace.LinearGray;

        public enum RatioType
        {
            Levenshtein,     // Standard Levenshtein Distance Matching returns int.
            Partial,         // Partial Matching
            TokenSort,       // Token Matching with strings sorted alphabetically
            TokenSet,        // Token Matching with duplicates removed
            TokenDifference, // Token Anti-Matching (looks at differences)
            Weighted,        // Weighted Matching of multiple algorithms (Levenshtein, Partial, TokenSort, TokenSet)
            LevenshteinExact, //Standard Levehnstein Distance returns double.
        }


        //Compare two strings using different types of Fuzzy Logic.  Default is Levenshtein
        public static double CompareStrings(string s1, string s2, RatioType ratioType = RatioType.Levenshtein)
        {
            switch (ratioType)
            {
                case RatioType.LevenshteinExact:
                    return Levenshtein.GetRatio(s1, s2);
                case RatioType.TokenDifference:
                    return Fuzz.TokenDifferenceRatio(s1, s2);
                case RatioType.Partial:
                    return Fuzz.PartialRatio(s1, s2);
                case RatioType.TokenSort:
                    return Fuzz.TokenSortRatio(s1, s2);
                case RatioType.TokenSet:
                    return Fuzz.TokenSetRatio(s1, s2);
                case RatioType.Weighted:
                    return Fuzz.WeightedRatio(s1, s2);
                case RatioType.Levenshtein:
                default:
                    return Fuzz.Ratio(s1, s2);
            }
        }

        public static bool TryCast<T>(object value, out T result)
        {
            result = default!;
            Type destinationType = typeof(T);

            if (value is string str && string.IsNullOrEmpty(str))
            {
                result = default!;
                return false; // Indicate failure for empty strings
            }

            try
            {
                // Switch on the value type directly for common cases
                switch (value)
                {
                    case null:
                    case DBNull:
                        // Defer nullable handling to the end
                        return false;

                    case bool b when destinationType == typeof(bool):
                        result = (T)(object)b;
                        return true;
                    case byte bt when destinationType == typeof(byte):
                        result = (T)(object)bt;
                        return true;
                    case sbyte sb when destinationType == typeof(sbyte):
                        result = (T)(object)sb;
                        return true;
                    case short s when destinationType == typeof(short):
                        result = (T)(object)s;
                        return true;
                    case ushort us when destinationType == typeof(ushort):
                        result = (T)(object)us;
                        return true;
                    case int i when destinationType == typeof(int):
                        result = (T)(object)i;
                        return true;
                    case uint ui when destinationType == typeof(uint):
                        result = (T)(object)ui;
                        return true;
                    case long l when destinationType == typeof(long):
                        result = (T)(object)l;
                        return true;
                    case ulong ul when destinationType == typeof(ulong):
                        result = (T)(object)ul;
                        return true;
                    case float f when destinationType == typeof(float):
                        result = (T)(object)f;
                        return true;
                    case double d when destinationType == typeof(double):
                        result = (T)(object)d;
                        return true;
                    case decimal dec when destinationType == typeof(decimal):
                        result = (T)(object)dec;
                        return true;
                    case char c when destinationType == typeof(char):
                        result = (T)(object)c;
                        return true;
                    case Guid guid:
                        result = (T)(object)guid;
                        return true;
                    case object obj when destinationType.IsEnum:
                        Type enumUnderlyingType = Enum.GetUnderlyingType(destinationType);
                        if (value.GetType() == enumUnderlyingType || value is IConvertible)
                        {
                            var convertedEnum = Convert.ChangeType(value, enumUnderlyingType);
                            if (Enum.IsDefined(destinationType, convertedEnum))
                            {
                                result = (T)Enum.ToObject(destinationType, convertedEnum);
                                return true;
                            }
                        }

                        if (value is string enumString && Enum.TryParse(destinationType, enumString, true, out var parsedEnum))
                        {
                            result = (T)parsedEnum;
                            return true;
                        }

                        return false;
                }

                if (value is string guidString && Guid.TryParse(guidString, out var parsedGuid))
                {
                    result = (T)(object)parsedGuid;
                    return true;
                }

                // Handle nullable types and value types explicitly
                Type targetType = Nullable.GetUnderlyingType(destinationType) ?? destinationType;

                if (targetType.IsValueType)
                {
                    object defaultValue = Activator.CreateInstance(targetType);
                    if (defaultValue!.Equals(value))
                    {
                        result = (T)defaultValue;
                        return true;
                    }
                }

                // General fallback using Convert
                result = (T)Convert.ChangeType(value, targetType);
                return true;
            }
            catch
            {
                return false; // Gracefully handle invalid casts
            }
        }
        public class DataGenerator
        {
            private static readonly ThreadLocal<Random> Random = new(() => new Random());
            private IList<object> Values;
            private IEnumerable<object> IEnumerables;
            private object Primitive;
            Func<object> CustomFactory;
            private ICollection<object> ICollections;
            private IDictionary<object, object> KeyValuePairs;
            private IDictionary<object, IList<object>> NestedKeyValuePairs;
            private System.Array Arrays;
            private System.Linq.Lookup<object, object> Lookups;
            private (object, object)? Tuple;
            private (object, object, object)? Tuple3;
            private (object, object, object, object)? Tuple4;
            private (object, object, object, object, object)? Tuple5;
            private (object, object, object, object, object, object)? Tuple6;
            private Array EnumValues;
            private SortedList<object, object> SortedListValues;
            private List<object> SetValues;
            private SortedSet<object> SortedSetValues;
            private int NumberOfItems;

            // Static Factory Methods
            public static DataGenerator FromIEnumerable(IEnumerable<object> types, int numberOfItems)
            {
                if (types == null || !types.Any()) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { IEnumerables = types, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromFactory(Func<object> factory, int numberOfItems)
            {
                if (factory == null) throw new ArgumentNullException(nameof(factory));
                return new DataGenerator { CustomFactory = factory, NumberOfItems = numberOfItems };
            }
            private IEnumerable<dynamic> GenerateFromCollectionBase<T>(IEnumerable<T> collection)
            {
                if (collection == null || !collection.Any())
                    throw new InvalidOperationException("Collection must not be null or empty.");

                var list = collection.ToList();
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return list[Random.Value.Next(list.Count)];
                }
            }
            public static DataGenerator FromPrimitive(int minValue, int maxValue, int numberOfItems)
            {
                if (minValue > maxValue) throw new ArgumentException("minValue must be less than or equal to maxValue.");
                return new DataGenerator
                {
                    CustomFactory = () => Random.Value.Next(minValue, maxValue),
                    NumberOfItems = numberOfItems
                };
            }
            private static bool TryCastGen<T>(object input, out T output)
            {
                try
                {
                    output = (T)input; // Attempt to cast
                    return true;
                }
                catch (InvalidCastException)
                {
                    output = default;
                    return false;
                }
            }
            public static DataGenerator FromPrimitive(object obj, int numberofItems)
            {
                if (obj == null) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { Primitive = obj, NumberOfItems = numberofItems };
            }
            public static DataGenerator FromSortedList(SortedList<object, object> types, int numberOfItems)
            {
                if (types == null || !types.Any()) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { SortedListValues = types, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromICollection(ICollection<object> types, int numberOfItems)
            {
                if (types == null || !types.Any()) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { ICollections = types, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromArray(IEnumerable<object> types, int numberOfItems)
            {
                if (types == null || !types.Any()) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { Arrays = types.ToArray(), NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromList(IList<object> types, int numberOfItems)
            {
                if (types == null || !types.Any()) throw new ArgumentException("Types must not be null or empty.");
                return new DataGenerator { Values = types, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromDictionary(IDictionary<object, object> keyValuePairs, int numberOfItems)
            {
                if (keyValuePairs == null || !keyValuePairs.Any()) throw new ArgumentException("KeyValuePairs must not be null or empty.");
                return new DataGenerator { KeyValuePairs = keyValuePairs, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromDictionaryList(IDictionary<object, IList<object>> nestedkeyValuePairs, int numberOfItems)
            {
                if (nestedkeyValuePairs == null || !nestedkeyValuePairs.Any()) throw new ArgumentException("KeyValuePairs must not be null or empty.");
                return new DataGenerator { NestedKeyValuePairs = nestedkeyValuePairs, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromSortedSet(SortedSet<object> values, int numberOfItems)
            {
                if (values == null || !values.Any()) throw new ArgumentException("SortedSet values must not be null or empty.");
                return new DataGenerator { SortedSetValues = values, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromTuple((object, object) tuple, int numberOfItems)
            {
                return new DataGenerator { Tuple = tuple, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromTuple((object, object, object) tuple, int numberOfItems)
            {
                return new DataGenerator { Tuple3 = tuple, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromTuple((object, object, object, object) tuple, int numberOfItems)
            {
                return new DataGenerator { Tuple4 = tuple, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromTuple((object, object, object, object, object) tuple, int numberOfItems)
            {
                return new DataGenerator { Tuple5 = tuple, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromTuple((object, object, object, object, object, object) tuple, int numberOfItems)
            {
                return new DataGenerator { Tuple6 = tuple, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromEnum(Type enumType, int numberOfItems)
            {
                if (!enumType.IsEnum) throw new ArgumentException("Provided type must be an enum.");
                return new DataGenerator { EnumValues = Enum.GetValues(enumType), NumberOfItems = numberOfItems };
            }

            public static DataGenerator FromSet(ISet<object> values, int numberOfItems)
            {
                if (values == null || !values.Any()) throw new ArgumentException("Set values must not be null or empty.");
                return new DataGenerator { SetValues = values.ToList(), NumberOfItems = numberOfItems };
            }

            // Generate Data Method
            public IEnumerable<dynamic> GenerateData()
            {
                if (Arrays != null) return GenerateFromArray();
                if (CustomFactory != null) return GenerateFromFactory();
                if (Primitive != null) return GenerateRandomPrimitive(Primitive.GetType());
                if (Values != null) return GenerateFromList();
                if (KeyValuePairs != null) return GenerateFromKeyValuePairs();
                if (NestedKeyValuePairs != null) return GenerateFromNestedKeyValuePairs();
                if (Tuple != null) return GenerateFromTuple();
                if (Tuple3 != null) return GenerateFromTuple3();
                if (Tuple4 != null) return GenerateFromTuple4();
                if (Tuple5 != null) return GenerateFromTuple5();
                if (Tuple6 != null) return GenerateFromTuple6();
                if (EnumValues != null) return GenerateFromEnum();
                if (SetValues != null) return GenerateFromSet();
                if (ICollections != null) return GenerateFromCollection();
                if (IEnumerables != null) return GenerateFromEnumerable();
                if (SortedSetValues != null) return GenerateFromSortedSet();
                if (SortedListValues != null) return GenerateFromSortedList();
                if (Lookups!= null) return GenerateFromLookup();
                throw new InvalidOperationException("Invalid configuration for DataGenerator.");
            }
            private dynamic GenerateRandomPrimitive(Type type)
            {
                if (type == typeof(string))
                {
                    // Define the character set as a ReadOnlySpan<char>
                    ReadOnlySpan<char> charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                    int length = Random.Value.Next(1, 256); // Random length between 1 and 255
                    Span<char> buffer = stackalloc char[length]; // Allocate on the stack for efficiency
                    Span<byte> randomBytes = stackalloc byte[length]; // Allocate random bytes on the stack

                    // Fill the byte span with random values
                    Random.Value.NextBytes(randomBytes);

                    // Map randomBytes to valid indices in charSet
                    for (int i = 0; i < length; i++)
                    {
                        buffer[i] = charSet[randomBytes[i] % charSet.Length];
                    }

                    return new string(buffer);
                }
                return type switch
                {
                    Type t when t == typeof(int) => Random.Value.Next(int.MinValue, int.MaxValue), // Random int
                    Type t when t == typeof(uint) => (uint)Random.Value.Next(0, int.MaxValue), // Random unsigned int
                    Type t when t == typeof(long) => Random.Value.NextInt64(long.MinValue, long.MaxValue), // Random long
                    Type t when t == typeof(ulong) => (ulong)Random.Value.NextInt64(0, long.MaxValue), // Random unsigned long
                    Type t when t == typeof(short) => (short)Random.Value.Next(short.MinValue, short.MaxValue + 1), // Random short
                    Type t when t == typeof(ushort) => (ushort)Random.Value.Next(ushort.MinValue, ushort.MaxValue + 1), // Random unsigned short
                    Type t when t == typeof(sbyte) => (sbyte)Random.Value.Next(sbyte.MinValue, sbyte.MaxValue + 1), // Random signed byte
                    Type t when t == typeof(byte) => (byte)Random.Value.Next(byte.MinValue, byte.MaxValue + 1), // Random byte
                    Type t when t == typeof(double) =>
Random.Value.NextDouble() * (Random.Value.Next(0, 2) == 0 ? 1 : -1), // Random double (positive/negative)
                    Type t when t == typeof(float) =>
                        (float)(Random.Value.NextDouble() * (Random.Value.Next(0, 2) == 0 ? 1 : -1)), // Random float
                    Type t when t == typeof(bool) => Random.Value.Next(0, 2) == 0, // Random bool
                    Type t when t == typeof(char) => (char)Random.Value.Next(65, 91), // Random char (A-Z)
                    Type t when t == typeof(DateTime) =>
                        DateTime.Now.AddDays(Random.Value.Next(-1000, 1000)) // Random DateTime ±1000 days
                        .AddHours(Random.Value.Next(0, 24)) // Add random hours
                        .AddMinutes(Random.Value.Next(0, 60)) // Add random minutes
                        .AddSeconds(Random.Value.Next(0, 60)), // Add random seconds
                    Type t when t == typeof(Guid) => Guid.NewGuid(), // Random GUID
                    Type t when t == typeof(string) => Guid.NewGuid().ToString(), // Random string (as GUID)
                    Type t when t == typeof(System.Drawing.Point) => new System.Drawing.Point(Random.Value.Next(0, 255), Random.Value.Next(0, 255)), // Random string (as GUID)
                    Type t when t == typeof(System.Drawing.PointF) => new System.Drawing.PointF(Random.Value.NextSingle(), Random.Value.NextSingle()), // Random string (as GUID)
                    Type t when t == typeof(System.Drawing.Color) => Color.FromArgb(Random.Value.Next(0, 255), Random.Value.Next(0, 255), Random.Value.Next(0, 255)), // Random string (as GUID)

                    _ => throw new NotSupportedException($"Random generation for type {type} is not supported.")
                };
            }
            public static Bitmap GenerateRandomBitmap(int width = 512, int height = 512)
            {
                // Create the bitmap
                Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

                // Lock the bitmap for direct memory access
                BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height),
                                                        ImageLockMode.WriteOnly,
                                                        PixelFormat.Format24bppRgb);

                int stride = bitmapData.Stride;           // Stride accounts for row padding
                int BytesPerPixel = 3;                    // Format24bppRgb = 3 bytes per pixel
                ThreadLocal<Random> threadRandom = new(() => new Random());

                unsafe
                {
                    try
                    {
                        int widthInBytes = width * BytesPerPixel;
                        byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                        // Parallel loop for row-wise random color generation
                        Parallel.For(0, height, y =>
                        {
                            byte* currentLine = ptrFirstPixel + (y * stride);

                            Random random = threadRandom.Value!;
                            for (int x = 0; x < widthInBytes; x += BytesPerPixel)
                            {
                                // Generate random RGB values
                                byte blue = (byte)random.Next(0, 256);
                                byte green = (byte)random.Next(0, 256);
                                byte red = (byte)random.Next(0, 256);

                                // Write RGB values to the bitmap memory
                                currentLine[x] = blue;     // Blue
                                currentLine[x + 1] = green; // Green
                                currentLine[x + 2] = red;   // Red
                            }
                        });
                    }
                    finally
                    {
                        bitmap.UnlockBits(bitmapData); // Unlock to apply changes
                    }
                }

                return bitmap;
            }
            private IEnumerable<dynamic> GenerateFromNestedKeyValuePairs()
            {
                if (NestedKeyValuePairs == null || !NestedKeyValuePairs.Any())
                    throw new InvalidOperationException("NestedKeyValuePairs must not be null or empty.");

                List<object> keyList = NestedKeyValuePairs.Keys.ToList();

                for (int i = 0; i < NumberOfItems; i++)
                {
                    object randomKey = keyList[Random.Value.Next(keyList.Count)];

                    IList<object> valueList = NestedKeyValuePairs[randomKey];

                    var randomValue = valueList[Random.Value.Next(valueList.Count)];

                    yield return new { Key = randomKey, Value = randomValue };
                }
            }

            private IEnumerable<dynamic> GenerateFromFactory()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return CustomFactory.Invoke();
                }
            }
            private IEnumerable<dynamic> GenerateFromSortedList()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    object randomKey = KeyValuePairs.Keys.ElementAt(Random.Value.Next(KeyValuePairs.Count));
                    yield return new { Key = randomKey, Value = KeyValuePairs[randomKey] };
                }
            }

            // Async Generate Data Method
            public async IAsyncEnumerable<dynamic> GenerateDataAsync()
            {
                foreach (var item in GenerateData())
                {
                    await Task.Delay(1); // Simulate async operation
                    yield return item;
                }
            }
            private IEnumerable<dynamic> GenerateFromNestedData(object nestedData, int maxDepth = int.MaxValue, int currentDepth = 0)
            {
                if (currentDepth > maxDepth)
                    yield break; // Prevent processing beyond the specified depth

                switch (nestedData)
                {
                    case IList<object> nestedList:
                        foreach (var item in nestedList)
                        {
                            foreach (var result in GenerateFromNestedData(item, maxDepth, currentDepth + 1))
                            {
                                yield return result;
                            }
                        }
                        break;


                    case Array nestedArray:
                        foreach (var item in nestedArray)
                        {
                            foreach (var result in GenerateFromNestedData(item, maxDepth, currentDepth + 1))
                            {
                                yield return result;
                            }
                        }
                        break;

                    case IDictionary<object, IList<object>> nestedKeyValuePairs:
                        foreach (var kvp in nestedKeyValuePairs)
                        {
                            foreach (var result in GenerateFromNestedData(kvp.Value, maxDepth, currentDepth + 1))
                            {
                                yield return new { Key = kvp.Key, Value = result };
                            }
                        }
                        break;

                    case IDictionary<object, object> keyValuePairs:
                        foreach (var kvp in keyValuePairs)
                        {
                            yield return new { Key = kvp.Key, Value = kvp.Value };
                        }
                        break;

                    default:
                        yield return nestedData; // Base case: Return scalar values
                        break;
                }
            }

            private IEnumerable<dynamic> GenerateFromKeyValuePairs(IDictionary<object, object> keyValuePairs)
            {
                if (keyValuePairs == null || !keyValuePairs.Any())
                    throw new InvalidOperationException("KeyValuePairs must not be null or empty.");

                var keys = keyValuePairs.Keys.ToList(); // Extract keys for random selection

                for (int i = 0; i < NumberOfItems; i++)
                {
                    // Pick a random key
                    object randomKey = keys[Random.Value.Next(keys.Count)];

                    // Retrieve the corresponding value
                    object value = keyValuePairs[randomKey];

                    // Return the key-value pair as an anonymous object
                    yield return new { Key = randomKey, Value = value };
                }
            }

            private IEnumerable<dynamic> GenerateFromList()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    var randomElement = Values[Random.Value.Next(Values.Count)];
                    foreach (var result in GenerateFromNestedData(randomElement))
                    {
                        yield return result;
                    }
                }
            }
            private IEnumerable<object> GenerateFromLookup()
            {
                // Ensure Lookups is valid and non-empty
                if (Lookups == null || !Lookups.Any())
                    throw new InvalidOperationException("Lookup data must not be null or empty.");

                // Extract keys for random selection
                List<object> keys = Lookups.Select(t => t.Key).ToList();

                for (int i = 0; i < NumberOfItems; i++)
                {
                    // Randomly select a key
                    var randomKey = keys[Random.Value.Next(keys.Count)];

                    // Retrieve associated values
                    var values = Lookups[randomKey]?.ToArray();
                    if (values == null || values.Length == 0)
                        continue;

                    // If the values are nested, handle them recursively
                    foreach (var value in values)
                    {
                        foreach (var result in GenerateFromNestedData(value))
                        {
                            yield return new { Key = randomKey, Value = result };
                        }
                    }
                }
            }
            private IEnumerable<dynamic> GenerateFromArray()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    var randomElement = Arrays.GetValue(Random.Value.Next(Arrays.Length));
                    foreach (var result in GenerateFromNestedData(randomElement))
                    {
                        yield return result;
                    }
                }
            }
            private IEnumerable<dynamic> GenerateFromNestedKeyValuePairs(IDictionary<object, IList<object>> nestedKeyValuePairs)
            {
                foreach (var kvp in nestedKeyValuePairs)
                {
                    foreach (var value in GenerateFromNestedData(kvp.Value))
                    {
                        yield return new { kvp.Key, Value = value };
                    }
                }
            }
            private IEnumerable<dynamic> GenerateFromKeyValuePairs()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    var randomKey = KeyValuePairs.Keys.ElementAt(Random.Value.Next(KeyValuePairs.Count));
                    yield return new { Key = randomKey, Value = KeyValuePairs[randomKey] };
                }
            }

            private IEnumerable<dynamic> GenerateFromTuple()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return new { Item1 = Tuple.Value.Item1, Item2 = Tuple.Value.Item2 };
                }
            }
            private IEnumerable<dynamic> GenerateFromTuple3()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return new { Item1 = Tuple3.Value.Item1, Item2 = Tuple3.Value.Item2, Item3 = Tuple3.Value.Item3 };
                }
            }
            public static DataGenerator FromNestedList(IList<object> nestedList, int numberOfItems)
            {
                if (nestedList == null || !nestedList.Any()) throw new ArgumentException("Nested list must not be null or empty.");
                return new DataGenerator { Values = nestedList, NumberOfItems = numberOfItems };
            }
            public static DataGenerator FromLookup<TKey, TValue>(ILookup<TKey, TValue> lookup, int numberOfItems)
            {
                if (lookup == null || !lookup.Any()) throw new ArgumentException("Lookup must not be null or empty.");
                return new DataGenerator { Lookups = (Lookup<object, object>)lookup, NumberOfItems = numberOfItems };
            }
            private IEnumerable<dynamic> GenerateFromTuple4()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return new { Item1 = Tuple4.Value.Item1, Item2 = Tuple4.Value.Item2, Item3 = Tuple4.Value.Item3, Item4 = Tuple4.Value.Item4 };
                }
            }
            private IEnumerable<dynamic> GenerateFromTuple5()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return new { Tuple5.Value.Item1, Item2 = Tuple5.Value.Item2, Item3 = Tuple5.Value.Item3, Item4 = Tuple5.Value.Item4, Item5 = Tuple5.Value.Item5 };
                }
            }
            private IEnumerable<dynamic> GenerateFromTuple6()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return new { Tuple6.Value.Item1, Item2 = Tuple6.Value.Item2, Item3 = Tuple6.Value.Item3, Item4 = Tuple6.Value.Item4, Item5 = Tuple6.Value.Item5, Item6 = Tuple6.Value.Item6 };
                }
            }

            private IEnumerable<dynamic> GenerateFromEnum()
            {
                if (EnumValues == null || EnumValues.Length == 0)
                    throw new InvalidOperationException("Enum values must not be null or empty.");

                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return EnumValues.GetValue(Random.Value.Next(EnumValues.Length));
                }
            }
            private IEnumerable<dynamic> GenerateFromNestedList(IList<object> nestedList)
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    object randomElement = nestedList[Random.Value.Next(nestedList.Count)];
                    if (randomElement is IList<object> innerList)
                    {
                        yield return GenerateFromNestedList(innerList); // Recursive generation for nested list
                    }
                    else
                    {
                        yield return randomElement; // Base case: return the element
                    }
                }
            }
            private IEnumerable<dynamic> GenerateFromNestedArray(Array nestedArray)
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    object randomElement = nestedArray.GetValue(Random.Value.Next(nestedArray.Length));
                    if (randomElement is Array innerArray)
                    {
                        yield return GenerateFromNestedArray(innerArray); // Recursive generation for nested array
                    }
                    else
                    {
                        yield return randomElement; // Base case: return the element
                    }
                }
            }

            private IEnumerable<dynamic> GenerateFromSet()
            {
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return SetValues[Random.Value.Next(SetValues.Count)];
                }
            }

            private IEnumerable<dynamic> GenerateFromCollection()
            {
                var collectionList = ICollections.ToList();
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return collectionList[Random.Value.Next(collectionList.Count)];
                }
            }

            private IEnumerable<dynamic> GenerateFromEnumerable()
            {
                var enumerableList = IEnumerables.ToList();
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return enumerableList[Random.Value.Next(enumerableList.Count)];
                }
            }

            private IEnumerable<dynamic> GenerateFromSortedSet()
            {
                var sortedSetList = SortedSetValues.ToList();
                for (int i = 0; i < NumberOfItems; i++)
                {
                    yield return sortedSetList[Random.Value.Next(sortedSetList.Count)];
                }
            }
        }


        public interface ICoordinate
        {
            Point? point { get; }
            float X { get; }
            float Y { get; }
        }

        public struct RString : ICollection<char>, IEnumerable<RString>, IComparable<RString>, IEquatable<RString>, IEqualityComparer<RString>, IFormattable, IJsonOnSerializing, IJsonOnDeserializing, IJsonOnSerialized, IJsonOnDeserialized
        {
            private StringBuilder sb;

            private string? _serializedValue;

            public string SerializedValue
            {
                get
                {
                    if (_serializedValue == null)
                    {
                        _serializedValue = sb.ToString();
                    }
                    return _serializedValue;
                }
            }
            public  static string ToUpperAndTrim(string input, ref StringBuilder sb)
            {
                if (sb == null)
                    sb = new StringBuilder();
                sb.Length = 0;
                sb.EnsureCapacity(input.Length);
                bool insideToken = false;
                foreach (char c in input)
                {
                    if (char.IsWhiteSpace(c))
                    {
                        if (insideToken)
                        {
                            // Add a single space for intermediate spaces, but avoid multiple spaces
                            if (sb.Length > 0 && sb[^1] != ' ')
                            {
                                sb.Append(' ');
                            }
                        }
                    }
                    else
                    {
                        // Append uppercase character and mark as inside a token
                        sb.Append(char.ToUpperInvariant(c));
                        insideToken = true;
                    }
                }
                // Remove trailing space if added
                if (sb.Length > 0 && sb[^1] == ' ')
                {
                    sb.Length--;
                }
                return sb.ToString();
            }

            public static string RemoveNullOrWhiteSpace(string input, ref StringBuilder sb)
            {
                if (sb == null)
                    sb = new StringBuilder();
                sb.Length = 0;
                sb.EnsureCapacity(input.Length);
                foreach (char c in input)
                {
                    if (!char.IsWhiteSpace(c))
                    {
                        sb.Append(c);
                    }
                }
                return sb.ToString();
            }

            public static string Split(string input, ref StringBuilder sb)
            {
                if (sb == null)
                    sb = new StringBuilder();
                sb.Length = 0;
                sb.EnsureCapacity(input.Length);
                foreach (char c in input)
                {
                    if (!char.IsWhiteSpace(c))
                    {
                        sb.Append(c);
                    }
                }
                return sb.ToString();
            }
            private void InvalidateSerializedValue()
            {
                _serializedValue = null;
            }
            public int Count => sb.Length;

            public bool IsReadOnly => false;

            // Constructor
            public RString(RString value)
            {
                sb = new StringBuilder();
                sb.Append(value);
            }
            public RString(string value)
            {
                sb = new StringBuilder();
                sb.Append(value);
            }
            public RString(ReadOnlyMemory<char> span)
            {
                sb = new StringBuilder(span.Length);
                sb.Append(span);
            }
            public RString(ReadOnlySpan<char> span)
            {
                sb = new StringBuilder(span.Length);
                sb.Append(span);
            }
            public RString(string value, int size)
            {
                sb = new StringBuilder(size);
                sb.Append(value);
            }
            public RString(char val, int size)
            {
                sb = new StringBuilder(size);
                sb.Append(val);
            }
            public RString(int size)
            {
                sb = new StringBuilder(size);
            }

            public char this[int index] => sb[index]; // Let StringBuilder handle range checks

            public static bool IsNullOrEmpty(RString value)
            {
                return value.Count == 0 || value.Count == 1 && value[0] == '\0';
            }
            public bool IsNullOrEmpty()
            {
                return sb.Length == 0 || sb.Length == 1 && sb[0] == '\0';
            }

            public static void Replace(RString input, ReadOnlySpan<char> oldValue, ReadOnlySpan<char> newValue)
            {
                input.sb.Replace(oldValue.ToString(), newValue.ToString());
            }

            public static void Replace(RString input, RString oldValue, RString newValue)
            {
                input.sb.Replace(oldValue.SerializedValue, newValue.SerializedValue);
            }

            public static void Replace(RString input, char oldChar, char newChar)
            {
                input.sb.Replace(oldChar, newChar);
            }

            public void Replace(string oldValue, string newValue)
            {
                sb.Replace(oldValue, newValue);
                InvalidateSerializedValue();
            }

            public void Replace(RString value, string oldValue, string newValue)
            {
                value.sb.Replace(oldValue, newValue);
                InvalidateSerializedValue();
            }

            public void Replace(char oldValue, char newValue)
            {
                sb.Replace(oldValue, newValue);
                InvalidateSerializedValue();
            }

            public void Replace(RString oldValue, string newValue)
            {
                sb.Replace(oldValue.SerializedValue, newValue);
                InvalidateSerializedValue();
            }
            public static void RegexReplace(RString input, Regex regex, string replacement)
            {
                // Clear the input's StringBuilder to prepare for replacement
                StringBuilder result = new StringBuilder();

                // Use Regex's MatchEvaluator to build the result directly
                regex.Replace(input.SerializedValue, match =>
                {
                    result.Append(replacement); // Append the replacement
                    return string.Empty; // Indicate that we've handled appending manually
                });

                // Update the input's StringBuilder with the result
                input.sb.Clear();
                input.sb.Append(result);
            }
            public void RegexReplace(RString input, string pattern, string replacement, RegexOptions options = RegexOptions.None)
            {
                // Create the Regex object
                Regex regex = new Regex(pattern, options);

                // Use the optimized Replace method
                RegexReplace(input, regex, replacement);
            }
            public int IndexOf(char value)
            {
                foreach (var chunk in sb.GetChunks())
                {
                    int index = chunk.Span.IndexOf(value);
                    if (index != -1)
                    {
                        return index;
                    }
                }
                return -1; // Not found
            }

            public int IndexOf(string value)
            {
                return this.IndexOf(value);
            }

            public bool Normalize(RString value)
            {
                return value.Normalize(this);
            }
            public static RString From(char[] chars) => new RString(chars);
            public static RString From(IEnumerable<char> chars) => new RString(chars.ToArray());
            public static RString From(ReadOnlySpan<char> chars) => new RString(chars.ToArray());
            public void Split(char delimiter, List<RString> result)
            {
                result.Clear(); // Clear the result list before populating
                int currentTokenStart = 0; // Tracks the start of the current token in the StringBuilder
                int currentTokenLength = 0; // Tracks the length of the current token

                foreach (var chunk in sb.GetChunks())
                {
                    ReadOnlyMemory<char> memory = chunk;

                    for (int i = 0; i < memory.Length; i++)
                    {
                        if (memory.Span[i] == delimiter)
                        {
                            // Add the current token if it exists
                            if (currentTokenLength > 0)
                            {
                                AddOrUpdateRStringInResult(result, currentTokenStart, currentTokenLength);
                                currentTokenStart += currentTokenLength + 1; // Move past delimiter
                                currentTokenLength = 0; // Reset for the next token
                            }
                        }
                        else
                        {
                            currentTokenLength++;
                        }
                    }
                }
                // Add the final token if it exists
                if (currentTokenLength > 0)
                {
                    AddOrUpdateRStringInResult(result, currentTokenStart, currentTokenLength);
                }
            }

            private void AddOrUpdateRStringInResult(List<RString> result, int start, int length)
            {
                // Check if there's an unused RString in the list
                if (result.Count < result.Capacity)
                {
                    // Reuse an existing RString instance
                    RString existing = result[result.Count];
                    existing.UpdateFromRangeInPlace(sb, start, length);
                    result.Add(existing);
                }
                else
                {
                    // Add a new RString if capacity is exceeded
                    RString newRString = new RString();
                    newRString.UpdateFromRangeInPlace(sb, start, length);
                    result.Add(newRString);
                }
            }
            public void UpdateFromRangeInPlace(StringBuilder source, int start, int length)
            {
                // Clear only the necessary range within the StringBuilder
                sb.Length = 0; // Reset the existing StringBuilder
                int remaining = length;

                foreach (var chunk in source.GetChunks())
                {
                    if (start < chunk.Length)
                    {
                        // Copy the relevant portion of the chunk
                        int charsToCopy = Math.Min(remaining, chunk.Length - start);
                        sb.Append(chunk.Span.Slice(start, charsToCopy));
                        remaining -= charsToCopy;

                        if (remaining <= 0)
                            break; // All required characters are copied
                    }
                    else
                    {
                        start -= chunk.Length; // Skip this chunk
                    }
                }
            }
            public void UpdateFromRange(StringBuilder source, int start, int length)
            {
                sb.Clear(); // Clear existing content
                foreach (var chunk in source.GetChunks())
                {
                    if (start < chunk.Length)
                    {
                        int charsToCopy = Math.Min(length, chunk.Length - start);
                        sb.Append(chunk.Span.Slice(start, charsToCopy));
                        length -= charsToCopy;

                        if (length <= 0)
                            break;
                    }
                    else
                    {
                        start -= chunk.Length; // Skip this chunk
                    }
                }
            }

            public static RString Trim(RString input, RString trimChars)
            {
                if (trimChars.Count == 0)
                {
                    return input; // Nothing to trim
                }

                if (trimChars.Count > 16)
                {
                    // Use HashSet for faster lookups when trimChars is large
                    HashSet<char> charSet = new HashSet<char>(trimChars);
                    return input.TrimInternal(c => charSet.Contains(c));
                }
                else
                {
                    // Use simple Contains for smaller trimChars
                    return input.TrimInternal(c => trimChars.Contains(c));
                }
            }

            public static void Join(RString destination, IEnumerable<RString> strings, char delimiter)
            {
                destination.sb.AppendJoin(delimiter, strings);
            }

            public static void Join(RString destination, IEnumerable<RString> strings, string delimiter)
            {
                destination.sb.AppendJoin(delimiter, strings);
            }

            public void Join(IEnumerable<string> strings, string delimiter)
            {
                sb.AppendJoin(delimiter, strings);
            }
            public void Join(IEnumerable<string> strings, char delimiter)
            {
                sb.AppendJoin(delimiter, strings);
            }
            public int LastIndexOf(string value)
            {
                ReadOnlySpan<char> searchSpan = value.AsSpan();
                if (searchSpan.Length > sb.Length)
                    return -1;

                int lastIndex = -1; // Tracks the last occurrence's global index
                int currentOffset = 0; // Tracks the global offset for each chunk

                foreach (var chunk in sb.GetChunks())
                {
                    ReadOnlySpan<char> chunkSpan = chunk.Span;

                    // Search for the substring in the current chunk
                    int indexInChunk = chunkSpan.LastIndexOf(searchSpan);
                    if (indexInChunk >= 0)
                    {
                        // Calculate the global index of the last occurrence
                        lastIndex = currentOffset + indexInChunk;
                    }

                    // Update the global offset for the next chunk
                    currentOffset += chunkSpan.Length;
                }

                return lastIndex; // Return the last occurrence or -1 if not found
            }
            public RString Trim()
            {
                int start = 0;
                int end = sb.Length - 1;

                // Find the first non-whitespace character from the start
                while (start <= end && char.IsWhiteSpace(sb[start]))
                {
                    start++;
                }

                // Find the first non-whitespace character from the end
                while (end >= start && char.IsWhiteSpace(sb[end]))
                {
                    end--;
                }

                // Trim the StringBuilder to the new range
                if (start > 0 || end < sb.Length - 1)
                {
                    sb.Remove(end + 1, sb.Length - end - 1); // Remove trailing characters
                    sb.Remove(0, start); // Remove leading characters
                }

                return this;
            }
            public RString Trim(RString trimstr)
            {
                if (IsNullOrEmpty(trimstr))
                {
                    return this; // Nothing to trim
                }

                return trimstr.Count > 16
                    ? TrimInternal(c => new HashSet<char>(trimstr).Contains(c))
                    : TrimInternal(c => trimstr.Contains(c));
            }

            public RString Trim(string trimstr)
            {
                if (string.IsNullOrEmpty(trimstr))
                {
                    return this; // Nothing to trim
                }

                return trimstr.Length > 16
                    ? TrimInternal(c => new HashSet<char>(trimstr).Contains(c))
                    : TrimInternal(trimstr.Contains);
            }

            public RString Trim(IEnumerable<char> trimChars)
            {
                string trimStr = new string(trimChars.ToArray());
                if (string.IsNullOrEmpty(trimStr))
                {
                    return this; // Nothing to trim
                }

                return trimStr.Length > 16
                    ? TrimInternal(c => new HashSet<char>(trimStr).Contains(c))
                    : TrimInternal(trimStr.Contains);
            }

            private RString TrimInternal(Predicate<char> shouldTrim)
            {
                int start = 0, end = sb.Length - 1;

                bool startFound = false; // Tracks if the start has been identified
                int currentOffset = 0;   // Tracks the global offset across chunks

                foreach (ReadOnlyMemory<char> chunk in sb.GetChunks())
                {
                    ReadOnlySpan<char> span = chunk.Span;

                    for (int i = 0; i < span.Length; i++)
                    {
                        if (!startFound)
                        {
                            if (!shouldTrim(span[i]))
                            {
                                start = currentOffset + i;
                                startFound = true; // Mark that the start is found
                            }
                        }

                        // Continue to update `end` while traversing
                        if (startFound && !shouldTrim(span[i]))
                        {
                            end = currentOffset + i; // Update the end with the last non-trimmed position
                        }
                    }

                    currentOffset += span.Length; // Update global offset
                }

                if (!startFound || (start == 0 && end == sb.Length - 1)) // If no non-trimmed character was found
                {
                    return this; // No trimming needed, return the current instance
                }

                return this[start..(end + 1)];
            }


            public void ToUpper()
            {
                // Operate directly on the internal StringBuilder
                for (int chunkIndex = 0; chunkIndex < sb.Length; chunkIndex++)
                {
                    char currentChar = sb[chunkIndex];
                    sb[chunkIndex] = char.ToUpper(currentChar); // Replace with uppercase
                }
            }

            public static void ToUpper(RString str)
            {
                // Modify the given RString's internal StringBuilder directly
                for (int chunkIndex = 0; chunkIndex < str.sb.Length; chunkIndex++)
                {
                    char currentChar = str.sb[chunkIndex];
                    str.sb[chunkIndex] = char.ToUpper(currentChar); // Replace with uppercase
                }
            }
            public void ToLower()
            {
                // Operate directly on the internal StringBuilder
                for (int chunkIndex = 0; chunkIndex < sb.Length; chunkIndex++)
                {
                    char currentChar = sb[chunkIndex];
                    sb[chunkIndex] = char.ToLower(currentChar); // Replace with lowercase
                }
            }

            public static void ToLower(RString str)
            {
                // Modify the given RString's internal StringBuilder directly
                for (int chunkIndex = 0; chunkIndex < str.sb.Length; chunkIndex++)
                {
                    char currentChar = str.sb[chunkIndex];
                    str.sb[chunkIndex] = char.ToLower(currentChar); // Replace with lowercase
                }
            }

            public bool StartsWith(string value)
            {
                ReadOnlySpan<char> prefix = value.AsSpan();
                if (prefix.Length > sb.Length)
                {
                    return false;
                }

                int prefixIndex = 0;
                foreach (var chunk in sb.GetChunks())
                {
                    ReadOnlySpan<char> span = chunk.Span;
                    for (int i = 0; i < span.Length && prefixIndex < prefix.Length; i++)
                    {
                        if (span[i] != prefix[prefixIndex++])
                        {
                            return false;
                        }
                    }

                    if (prefixIndex == prefix.Length)
                    {
                        return true; // All prefix characters matched
                    }
                }

                return false; // Not all prefix characters matched
            }

            public bool EndsWith(string value)
            {
                ReadOnlySpan<char> suffix = value.AsSpan();
                if (suffix.Length > sb.Length)
                {
                    return false;
                }

                // Collect chunks for reverse traversal
                var chunks = new List<ReadOnlyMemory<char>>();
                foreach (var chunk in sb.GetChunks())
                {
                    chunks.Add(chunk);
                }

                int suffixIndex = suffix.Length - 1;
                for (int chunkIndex = chunks.Count - 1; chunkIndex >= 0; chunkIndex--)
                {
                    ReadOnlySpan<char> span = chunks[chunkIndex].Span;

                    for (int i = span.Length - 1; i >= 0 && suffixIndex >= 0; i--)
                    {
                        if (span[i] != suffix[suffixIndex--])
                        {
                            return false;
                        }
                    }

                    if (suffixIndex < 0)
                    {
                        return true; // All suffix characters matched
                    }
                }

                return false; // Not all suffix characters matched
            }
            public static void Substring(RString str, int startIndex, int length)
            {
                str.Substring(startIndex, length);
            }
            public void Substring(int startIndex, int length)
            {
                string ss = SerializedValue.Substring(startIndex, length);
                sb.Length = 0;
                sb.Append(ss);
            }
            public void CopyTo(int sourceIndex, Span<char> destination, int count)
            {
                int remaining = count;
                sb.CopyTo(sourceIndex, destination, remaining);
            }
            public void CopyTo(int sourceIndex, char[] destination, int count)
            {
                sb.CopyTo(sourceIndex, destination, 0, sb.Length);
            }
            public char[] ToCharArray()
            {
                char[] chars = new char[sb.Length];
                sb.CopyTo(0, chars, 0, sb.Length);
                return chars;
            }

            public RString this[int start, int end]
            {
                get
                {
                    // Delegate to the range-based indexer for consistency
                    return this[new Range(start, end)];
                }
                set
                {
                    // Delegate to the range-based indexer for consistency
                    this[new Range(start, end)] = value;
                }
            }

            public RString this[Range range]
            {
                get
                {
                    int start = range.Start.Value;
                    int end = range.End.Value;

                    if (start < 0 || end > sb.Length || start > end)
                        throw new ArgumentOutOfRangeException(nameof(range), "The specified range is invalid.");

                    int length = end - start;

                    // Extract the range and modify the current instance
                    StringBuilder tempBuilder = new StringBuilder(length);

                    for (int i = start; i < end;)
                    {
                        tempBuilder.Append(sb[i]);
                    }

                    sb = tempBuilder;
                    return this;              // Return the current instance
                }
                set
                {
                    int start = range.Start.Value;
                    int end = range.End.Value;

                    if (start < 0 || end > sb.Length || start > end)
                        throw new ArgumentOutOfRangeException(nameof(range), "The specified range is invalid.");

                    // Perform the replacement directly in the StringBuilder
                    sb.Remove(start, end - start);
                    sb.Insert(start, value.sb.ToString());
                }
            }

            public RString(StringBuilder result) : this()
            {
                this.sb = result;
            }

            public RString(char[] chars)
            {
                if (chars == null)
                    throw new ArgumentNullException(nameof(chars));

                this.sb = new StringBuilder(chars.Length);
                this.sb.Append(chars);
            }


            //!Operator
            public static bool operator !(RString r)
            {
                return r.sb.Length == 0; // Return true if the RString is empty
            }

            public static bool operator ==(RString r1, RString r2)
            {
                return r1.Equals(r2);
            }

            public static bool operator !=(RString r1, RString r2)
            {
                return !(r1 == r2);
            }
            public static bool operator ==(RString r1, string r2)
            {
                return r1.sb.Equals(r2.AsSpan());
            }
            // Overload != for symmetry

            public static bool operator !=(RString r1, string r2)
            {
                return !(r1.sb.Equals(r2.AsSpan()));
            }
            public static RString operator +(RString r1, string r2)
            {
                r1.sb.Append(r2);
                return r1;
            }
            public static RString operator +(RString r1, RString r2)
            {
                r1.sb.Append(r2.sb);
                return r1;
            }

            public static implicit operator RString(string v)
            {
                return new RString(v);
            }

            public static int IndexOf(RString r, string r2)
            {
                return IndexOf(r.sb, r2.AsSpan());
            }
            public static int IndexOf(RString r, RString r2)
            {
                return IndexOf(r.sb, r2.SerializedValue);
            }
            private static int IndexOf(StringBuilder sb, ReadOnlySpan<char> pattern)
            {
                int sbLength = sb.Length;
                int patternLength = pattern.Length;

                if (patternLength == 0)
                    return 0; // Empty pattern is always found at the start

                if (patternLength > sbLength)
                    return -1; // Pattern is longer than the StringBuilder content

                int i = 0;
                int j = 0;

                while (i < sbLength)
                {
                    if (sb[i] == pattern[j])
                    {
                        j++; // Move to the next character in the pattern
                        if (j == patternLength)
                        {
                            return i - patternLength + 1; // Found a match, return start index
                        }
                    }
                    else
                    {
                        i -= j; // Reset `i` back to where the potential match started
                        j = 0;  // Reset `j` to start of the pattern
                    }

                    i++;
                }

                return -1; // No match found
            }

            // Override ToString
            public override string ToString()
            {
                return sb.ToString();
            }

            public override bool Equals(object obj) => obj is RString other && sb.Equals(other.sb);

            public override int GetHashCode()
            {
                return sb.GetHashCode();
            }

            public void Add(char item)
            {
                sb.Append(item);
            }

            public void Add(string item)
            {
                sb.Append(item);
            }

            public void Clear()
            {
                sb.Clear();
            }

            public bool Contains(char item)
            {
                return sb.ToString().Contains(item);
            }
            public bool Contains(string item)
            {
                return sb.ToString().Contains(item);
            }
            public void CopyTo(char[] array, int arrayIndex)
            {
                sb.CopyTo(0, array.AsSpan(), arrayIndex);
            }
            public bool RemoveAt(int position, out char character)
            {
                character = sb[position];
                if (position >= sb.Length)
                    return false;
                sb.Remove(position, 1);
                return true;
            }
            public void Remove(char item)
            {
                for (int i = 0; i < sb.Length; i++)
                {
                    if (sb[i] == item)
                    {
                        sb.Remove(i, 1);

                    }
                }

            }
            public bool Remove(string item)
            {
                for (int i = 0; i < sb.Length; i++)
                {
                    if (sb[i] == item[i])
                    {
                        sb.Remove(i, 1);
                        return true;
                    }
                }
                return false;
            }

            public void Remove(int start, int end)
            {
                sb.Remove(start, end);
            }


            public void Insert(int location, string value)
            {
                sb.Insert(location, value);
            }


            public IEnumerator<char> GetEnumerator()
            {
                StringBuilder.ChunkEnumerator chunkEnumerator = sb.GetChunks();
                while (chunkEnumerator.MoveNext())
                {
                    ReadOnlyMemory<char> chunk = chunkEnumerator.Current; // Get the current chunk

                    for (int i = 0; i < chunk.Length; i++)
                    {
                        yield return chunk.Slice(i, 1).Span[0]; // Access each character directly from ReadOnlyMemory
                    }
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator(); // Delegate to the generic version
            }

            public int CompareTo(RString other)
            {
                // Early exit if total lengths differ
                if (this.sb.Length != other.sb.Length)
                {
                    return this.sb.Length.CompareTo(other.sb.Length);
                }

                // Enumerators for chunk-by-chunk comparison
                StringBuilder.ChunkEnumerator enumerator1 = this.sb.GetChunks();
                StringBuilder.ChunkEnumerator enumerator2 = other.sb.GetChunks();

                while (enumerator1.MoveNext() && enumerator2.MoveNext())
                {
                    ReadOnlySpan<char> span1 = enumerator1.Current.Span;
                    ReadOnlySpan<char> span2 = enumerator2.Current.Span;

                    // Compare spans directly
                    int comparison = span1.SequenceCompareTo(span2);
                    if (comparison != 0)
                    {
                        return comparison; // Exit early on mismatch
                    }
                }

                // Both are equal if no differences were found
                return 0;
            }
            public static IEqualityComparer<RString> GetEqualityComparer = EqualityComparer<RString>.Default;
            public bool Equals(RString x, RString y)
            {
                return x.sb.Equals(y.sb);
            }

            public int GetHashCode([DisallowNull] RString obj)
            {
                return obj.sb.GetHashCode();
            }

            public bool Equals(RString other)
            {
                return other.sb.Equals(this.sb);
            }

            public void OnSerializing()
            {
                _serializedValue = sb.ToString();
            }

            // Called after serialization has completed
            public void OnSerialized()
            {
                // Clear the serialized value if necessary
                _serializedValue = null;
            }

            // Called during deserialization to restore the object state
            public void OnDeserializing()
            {
                // Initialize StringBuilder if necessary
                if (string.IsNullOrEmpty(_serializedValue))
                {
                    sb = new StringBuilder();
                }
            }

            // Called after deserialization has completed
            public void OnDeserialized()
            {
                if (!string.IsNullOrEmpty(_serializedValue))
                {
                    sb = new StringBuilder(_serializedValue);
                    _serializedValue = null;
                }
            }
            public string ToString(string format, IFormatProvider formatProvider)
            {
                return string.Format(formatProvider, format, this.sb.ToString());
            }

            IEnumerator<RString> IEnumerable<RString>.GetEnumerator()
            {
                foreach (var chunk in sb.GetChunks())
                {
                    // Assume RString has a constructor that takes ReadOnlyMemory<char> directly
                    yield return new RString(chunk);
                }
            }

            bool ICollection<char>.Remove(char item)
            {
                StringBuilder.ChunkEnumerator enumerator = this.sb.GetChunks();
                int offset = 0;

                // Iterate through chunks
                while (enumerator.MoveNext())
                {
                    ReadOnlyMemory<char> chunk = enumerator.Current;
                    ReadOnlySpan<char> chunkSpan = chunk.Span;

                    // Search for the character in the current chunk
                    for (int i = 0; i < chunkSpan.Length; i++)
                    {
                        if (chunkSpan[i] == item)
                        {
                            // Calculate the index in the StringBuilder
                            int indexInSb = offset + i;

                            // Remove the character from the StringBuilder
                            sb.Remove(indexInSb, 1);

                            // Return true since the character was found and removed
                            return true;
                        }
                    }

                    // Update the offset for the next chunk
                    offset += chunk.Length;
                }
                return false;
            }
        }

        public class KDTree<T> where T : ICoordinate
        {
            private ISet<T> children;
            public struct Rectangle
            {
                public float MinX { get; set; }
                public float MinY { get; set; }
                public float MaxX { get; set; }
                public float MaxY { get; set; }

                public int iMinX { get; set; }
                public int iMinY { get; set; }
                public int iMaxX { get; set; }
                public int iMaxY { get; set; }

                public float Width => MaxX - MinX;
                public float Height => MaxY - MinY;
                public float iWidth => MaxX - MinX;
                public float iHeight => MaxY - MinY;
                public Rectangle(int minX, int minY, int maxX, int maxY)
                {
                    iMinX = minX;
                    iMinY = minY;
                    iMaxX = maxX;
                    iMaxY = maxY;
                }
                public Rectangle(float minX, float minY, float maxX, float maxY)
                {
                    MinX = minX;
                    MinY = minY;
                    MaxX = maxX;
                    MaxY = maxY;
                }
                public Rectangle(System.Drawing.Rectangle rect)
                {
                    MinX = rect.Left;
                    MinY = rect.Top;
                    MaxX = rect.Left + rect.Width;
                    MaxY = rect.Top + rect.Height;
                }

                public float GetEnlargement(Rectangle rect)
                {
                    float newMinX = Math.Min(MinX, rect.MinX);
                    float newMinY = Math.Min(MinY, rect.MinY);
                    float newMaxX = Math.Max(MaxX, rect.MaxX);
                    float newMaxY = Math.Max(MaxY, rect.MaxY);

                    float originalArea = GetArea();
                    float newArea = (newMaxX - newMinX) * (newMaxY - newMinY);

                    return newArea - originalArea;
                }

                public float GetArea()
                {
                    return (MaxX - MinX) * (MaxY - MinY);
                }
            }
            private class Node
            {
                public T Point;
                public Node Left;
                public Node Right;
                public int Axis;

                public Node(T point, int axis)
                {
                    Point = point;
                    Axis = axis;
                }
            }

            private Node root;

            public KDTree(int size = 0, bool allowDuplicates = false)
            {
                children = allowDuplicates ? new MultiSet<T>(size) : new HashSet<T>(size);
            }


            public KDTree(IEnumerable<T> points)
            {
                var pointsArray = points.ToArray();
                Shuffle(pointsArray); // Randomize the points
                root = Build(pointsArray, 0, 0, pointsArray.Length);
            }

            private Node Build(T[] pointList, int axis, int start, int end)
            {
                if (start >= end) return null;

                int medianIndex = start + (end - start) / 2;
                T median = SelectKth(pointList, medianIndex, axis, start, end);

                var node = new Node(median, axis);

                int nextAxis = 1 - axis;
                node.Left = Build(pointList, nextAxis, start, medianIndex);
                node.Right = Build(pointList, nextAxis, medianIndex + 1, end);

                return node;
            }
            private List<T> CollectPoints(Node node)
            {
                if (node == null) return new List<T>();
                var points = new List<T> { node.Point };
                points.AddRange(CollectPoints(node.Left));
                points.AddRange(CollectPoints(node.Right));
                return points;
            }
            public void Rebuild()
            {
                List<T> allPoints = CollectPoints(root);
                T[] pointsArray = allPoints.ToArray();
                Shuffle(pointsArray); // Randomize the points
                root = Build(pointsArray, 0, 0, pointsArray.Length); // Rebuild the tree
            }
            private static int PartitionInPlace(T[] group, float pivotValue, int axis, int start, int end)
            {
                int i = start - 1;
                for (int j = start; j < end; j++)
                {
                    float value = axis == 0 ? group[j].X : group[j].Y;
                    if (value < pivotValue)
                    {
                        i++;
                        if (i != j)
                        {
                            Swap(ref group[i], ref group[j]);
                        }
                    }
                }
                return i + 1;
            }

            private static T SelectKth(T[] group, int k, int axis, int start, int end)
            {
                while (start < end)
                {
                    float pivotValue = axis == 0 ? group[start].X : group[start].Y;
                    int pivotIndex = PartitionInPlace(group, pivotValue, axis, start, end);

                    if (pivotIndex == k)
                        return group[k];
                    else if (pivotIndex > k)
                        end = pivotIndex;
                    else
                        start = pivotIndex + 1;
                }
                return group[start];
            }

            private static void Swap(ref T a, ref T b)
            {
                T temp = a;
                a = b;
                b = temp;
            }
            public (T Neighbor, double Distance) FindNearestWithDistance(ICoordinate target)
            {
                // Find the nearest point using the existing method
                T nearestPoint = FindNearest(target);

                if (nearestPoint == null)
                {
                    return (default, double.MaxValue); // No neighbor found
                }

                // Calculate the actual distance
                double squaredDistance = DistanceSquared(nearestPoint, target);
                return (nearestPoint, Math.Sqrt(squaredDistance));
            }

            private T FindNearest(ICoordinate target)
            {
                var stack = new Stack<(Node Node, bool VisitedOther)>();
                Node current = root;
                T bestPoint = default;
                double bestDist = double.MaxValue;

                while (current != null || stack.Count > 0)
                {
                    while (current != null)
                    {
                        // Calculate the squared distance to the current node
                        double dist = DistanceSquared(current.Point, target);
                        if (dist < bestDist)
                        {
                            bestDist = dist;
                            bestPoint = current.Point;
                        }

                        // Determine the difference in the current splitting axis
                        float difference = current.Axis == 0 ? target.X - current.Point.X : target.Y - current.Point.Y;

                        // Traverse the closer subtree first, and push the other one onto the stack
                        stack.Push((current, false)); // Save the current node, marking "other" not visited yet
                        current = difference <= 0 ? current.Left : current.Right;
                    }

                    // Backtrack to the last node
                    var (node, visitedOther) = stack.Pop();

                    if (!visitedOther)
                    {
                        float diff = node.Axis == 0 ? target.X - node.Point.X : target.Y - node.Point.Y;

                        // Check if the "other" subtree should be visited
                        if (Math.Abs(diff) < bestDist)
                        {
                            stack.Push((node, true)); // Mark the current node as fully visited
                            current = diff <= 0 ? node.Right : node.Left; // Explore the other subtree
                        }
                    }
                }

                return bestPoint;
            }

            private static void Shuffle(T[] points)
            {
                Random rng = new Random();
                for (int i = points.Length - 1; i > 0; i--)
                {
                    int j = rng.Next(i + 1);
                    Swap(ref points[i], ref points[j]);
                }
            }
            private static double DistanceSquared(T point, ICoordinate target)
            {
                double dx = point.X - target.X;
                double dy = point.Y - target.Y;
                return dx * dx + dy * dy;
            }

        }
        public static ISet<T> TryGetSet<T>(IEnumerable<T> other, bool createSet = false)
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            if (other is ISet<T> set)
            {
                // Directly return the set if it's already an ISet<T> with no need for transformation
                return set switch
                {
                    HashSet<T> hashSet => hashSet,                              // Already a HashSet
                    SortedSet<T> sortedSet => sortedSet,                        // Already a SortedSet
                    System.Collections.Immutable.ImmutableSortedSet<T> immutableSortedSet => immutableSortedSet, // Keep immutable
                    System.Collections.Immutable.ImmutableHashSet<T> immutableHashSet => immutableHashSet,       // Keep immutable
                    System.Collections.Generic.IReadOnlySet<T> readOnlySet => (ISet<T>)readOnlySet,              // Cast IReadOnlySet<T> to ISet<T>
                    _ => set                                                    // Handle other ISet<T> implementations as-is
                };
            }
            else
            {
                if (createSet)
                {
                    return new HashSet<T>(other);
                }
                else
                {
                    return null;
                }
            }
        }

        public unsafe struct PixelProcessor : IAction2D
        {
            private readonly RectangleF rect;
            private readonly byte* ptr;
            private readonly int stride;
            private readonly float B;
            private readonly float G;
            private readonly float R;
            public byte Coordinates;

            public PixelProcessor(Rectangle r, byte* ptr, int stride, float B, float G, float R)
            {
                this.ptr = ptr;
                this.stride = stride;
                this.B = B;
                this.G = G;
                this.R = R;
            }
            public static void ProcessPixel(byte Pixel, Action<byte> DoToPixel)
            {
                DoToPixel.Invoke(Pixel);
            }

            public static void ProcessPixelChannels(byte R, byte G, byte B, Action<byte, byte, byte> DoToChannel)
            {
                DoToChannel.Invoke(B, G, R);
            }
            public static void ProcessPixel(ushort Pixel, Action<ushort> DoToPixel)
            {
                DoToPixel.Invoke(Pixel);
            }
            public static void ProcessPixelChannels(ushort R, ushort G, ushort B, Action<ushort, ushort, ushort> DoToChannel)
            {
                DoToChannel.Invoke(B, G, R);
            }
            public static void ProcessPixelRow(byte* ptr, int stride, int y, int width, Action<nint, Range> ProccessRow)
            {
                byte* firstRowPixel = ptr + (y * stride);
                nint ptrFirstRow = new(firstRowPixel);
                Range range = new Range(0, width);
                ProccessRow.Invoke(ptrFirstRow, range);
            }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Proccess(int y, int x)
            {
                byte* row = ptr + (y * stride); // Navigate to the row
                int pixelIndex = x * 3;         // Calculate pixel index within the row

                // Perform pixel operations
                row[pixelIndex] = (byte)(row[pixelIndex] * B);       // Blue
                row[pixelIndex + 1] = (byte)(row[pixelIndex + 1] * G); // Green
                row[pixelIndex + 2] = (byte)(row[pixelIndex + 2] * R); // Red
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Invoke(int y, int x)
            {
                byte* row = ptr + (y * stride); // Navigate to the row
                int pixelIndex = x * 3;         // Calculate pixel index within the row

                // Perform pixel operations
                row[pixelIndex] = (byte)(row[pixelIndex] * B);       // Blue
                row[pixelIndex + 1] = (byte)(row[pixelIndex + 1] * G); // Green
                row[pixelIndex + 2] = (byte)(row[pixelIndex + 2] * R); // Red
            }
        }
        public class RTree<T>
        {
            private const int DefaultMaxNodeEntries = 10;
            private const int DefaultMinNodeEntries = 4;

            private readonly ReaderWriterLockSlim _Addlocker = new ReaderWriterLockSlim();
            private readonly ReaderWriterLockSlim _Removelocker = new ReaderWriterLockSlim();
            private int _maxNodeEntries;
            private int _minNodeEntries;

            private readonly Dictionary<int, Node> _nodeMap = new Dictionary<int, Node>();
            private readonly Dictionary<int, int> _childToParentMap = new Dictionary<int, int>();
            private readonly Queue<int> _recycledNodeIds = new Queue<int>();

            private int _rootNodeId;
            private int _highestUsedNodeId;
            private int _size;

            private readonly Dictionary<int, T> _idToItems = new Dictionary<int, T>();
            private readonly Dictionary<T, int> _itemsToIds = new Dictionary<T, int>();

            public int Count
            {
                get
                {
                    _Addlocker.EnterReadLock();
                    try
                    {
                        return _size;
                    }
                    finally
                    {
                        _Addlocker.ExitReadLock();
                    }
                }
            }

            public RTree(int maxNodeEntries = DefaultMaxNodeEntries, int minNodeEntries = DefaultMinNodeEntries)
            {
                if (maxNodeEntries < 2)
                    throw new ArgumentException("Maximum node entries must be at least 2.", nameof(maxNodeEntries));
                if (minNodeEntries < 1 || minNodeEntries > maxNodeEntries / 2)
                    throw new ArgumentException("Invalid minimum node entries.", nameof(minNodeEntries));

                _maxNodeEntries = maxNodeEntries;
                _minNodeEntries = minNodeEntries;

                Initialize();
            }

            private void Initialize()
            {
                _rootNodeId = GetNextNodeId();
                var rootNode = new Node(_rootNodeId, 1);
                _nodeMap[_rootNodeId] = rootNode;
            }

            private int GetNextNodeId()
            {
                if (_recycledNodeIds.Count > 0)
                    return _recycledNodeIds.Dequeue();

                return ++_highestUsedNodeId;
            }
            private void RecycleNodeId(int nodeId)
            {
                _recycledNodeIds.Enqueue(nodeId);
            }

            public void Add(Rectangle mbr, T item)
            {
                _Addlocker.EnterWriteLock();
                try
                {
                    int id = GetNextNodeId();
                    _idToItems[id] = item;
                    _itemsToIds[item] = id;

                    var leafNode = ChooseNode(mbr, 1);
                    if (leafNode.EntryCount < _maxNodeEntries)
                    {
                        leafNode.AddEntry(mbr, id);
                        UpdateParentMapping(leafNode.NodeId, id); // Maintain parent-child mapping
                    }
                    else
                    {
                        var splitNode = SplitNode(leafNode, mbr, id);
                        AdjustTree(leafNode, splitNode);
                    }

                    _size++;
                }
                finally
                {
                    _Addlocker.ExitWriteLock();
                }
            }

            private void UpdateParentMapping(int parentId, int childId)
            {
                _childToParentMap[childId] = parentId;
            }

            private Node GetParentNode(Node child)
            {
                if (!_childToParentMap.TryGetValue(child.NodeId, out var parentId))
                    throw new InvalidOperationException("Parent node not found.");

                return _nodeMap[parentId];
            }

            private Node ChooseNode(Rectangle mbr, int level)
            {
                Node currentNode = _nodeMap[_rootNodeId];

                while (currentNode.Level != level)
                {
                    int bestIndex = 0;
                    float bestEnlargement = GetEnlargement(mbr, currentNode.Entries[0]);
                    for (int i = 1; i < currentNode.EntryCount; i++)
                    {
                        float enlargement = GetEnlargement(mbr, currentNode.Entries[i]);

                        if (enlargement < bestEnlargement ||
                            (enlargement == bestEnlargement &&
                             GetArea(currentNode.Entries[i]) < GetArea(currentNode.Entries[bestIndex])))
                        {
                            bestEnlargement = enlargement;
                            bestIndex = i;
                        }
                    }

                    currentNode = _nodeMap[currentNode.Ids[bestIndex]];
                }

                return currentNode;
            }
            public static double Median(List<int> list)
            {
                if (list == null || list.Count == 0)
                    throw new InvalidOperationException("Cannot compute the median of an empty list.");

                int n = list.Count;
                int midIndex1 = (n - 1) / 2;
                int midIndex2 = n % 2 == 0 ? n / 2 : midIndex1;

                if (n <= 100)
                {
                    list.Sort();
                    return n % 2 == 0 ? (list[midIndex1] + list[midIndex2]) / 2.0 : list[midIndex1];
                }

                return MedianHelper(list, 0, n - 1, midIndex1, midIndex2);
            }

            private static double MedianHelper(List<int> list, int left, int right, int midIndex1, int midIndex2)
            {
                float reciprocal10 = 0.1f;
                float reciprocal5 = 0.2f;
                float reciprocal2 = 0.5f; // Equivalent to dividing by 2

                while (true)
                {
                    if (left == right)
                    {
                        return list[left];
                    }

                    // Inline pivot selection using Median of Medians
                    int n = right - left + 1;
                    int nReciprocal5 = (int)(n * reciprocal5);
                    int nReciprocal10 = (int)(n * reciprocal10);

                    if (n <= 5)
                    {
                        list.Sort(left, n, Comparer<int>.Default);
                        int medianIndex = left + (int)(n * reciprocal2);
                        return list[medianIndex];
                    }

                    for (int i = 0, leftOffset = left; i < nReciprocal5; i++, leftOffset += 5)
                    {
                        int subRangeLength = Math.Min(leftOffset + 4, right) - (leftOffset + 1);
                        int medianIndex = leftOffset + (int)(subRangeLength * reciprocal2);
                        list.Sort(leftOffset, subRangeLength, Comparer<int>.Default);
                        if (leftOffset != medianIndex)
                        {
                            (list[leftOffset], list[medianIndex]) = (list[medianIndex], list[leftOffset]);
                        }
                    }

                    int pivotIndex = (int)MedianHelper(list, left, left + nReciprocal5 - 1, nReciprocal10, nReciprocal10);
                    int pivotValue = list[pivotIndex];

                    // Partition with parallelized logic
                    int storeIndex = left;

                    for (int i = left; left < right + 1; i++)
                    {
                        if (list[i] < pivotValue & (storeIndex -1)!= i)
                        {
                            int newIndex = storeIndex - 1;
                            (list[newIndex], list[i]) = (list[i], list[newIndex]);
                        }
                    }

                    // Swap pivot to its final place
                    if (storeIndex != right)
                    {
                        list[storeIndex] ^= list[right];
                        list[right] ^= list[storeIndex];
                        list[storeIndex] ^= list[right];
                    }

                    // Determine next bounds
                    if (midIndex1 == storeIndex)
                    {
                        if (midIndex1 == midIndex2)
                            return list[midIndex1];
                        return (list[midIndex1] + list[midIndex2]) * reciprocal2;
                    }
                    else if (midIndex1 < storeIndex)
                    {
                        right = storeIndex - 1;
                    }
                    else
                    {
                        left = storeIndex + 1;
                    }
                }
            }


            // Inline logic for Median of Medians pivot selection

            private Node SplitNode(Node node, Rectangle mbr, int id)
            {
                var newNode = new Node(GetNextNodeId(), node.Level);
                var allEntries = new List<(Rectangle, int)>(node.EntriesWithIds) { (mbr, id) };

                node.Clear();

                // Step 1: Pick seeds (initialize two groups)
                PickSeeds(allEntries, node, newNode);

                // Step 2: Distribute remaining entries
                foreach (var (entryMbr, entryId) in allEntries)
                {
                    // Check if adding the entry would satisfy the minimum condition
                    if (node.EntryCount + (allEntries.Count - node.EntryCount - newNode.EntryCount) <= _minNodeEntries)
                    {
                        node.AddEntry(entryMbr, entryId);
                    }
                    else if (newNode.EntryCount + (allEntries.Count - node.EntryCount - newNode.EntryCount) <= _minNodeEntries)
                    {
                        newNode.AddEntry(entryMbr, entryId);
                    }
                    else
                    {
                        // Add to the least enlarging node
                        AddToLeastEnlargingNode(entryMbr, entryId, node, newNode);
                    }
                }

                return newNode;
            }

            private void PickSeeds(List<(Rectangle, int)> entries, Node group1, Node group2)
            {
                int seed1X = 0, seed2X = 0;
                int seed1Y = 0, seed2Y = 0;

                float minLeft = float.MaxValue, maxRight = float.MinValue;
                float minTop = float.MaxValue, maxBottom = float.MinValue;

                // Step 1: Find min and max projections for each axis
                for (int i = 0; i < entries.Count; i++)
                {
                    var rect = entries[i].Item1;

                    // X-axis extremes
                    if (rect.Left < minLeft) { minLeft = rect.Left; seed1X = i; }
                    if (rect.Right > maxRight) { maxRight = rect.Right; seed2X = i; }

                    // Y-axis extremes
                    if (rect.Top < minTop) { minTop = rect.Top; seed1Y = i; }
                    if (rect.Bottom > maxBottom) { maxBottom = rect.Bottom; seed2Y = i; }
                }

                // Step 2: Compute normalized separations
                float width = maxRight - minLeft;
                float height = maxBottom - minTop;

                float separationX = width > 0 ? Math.Abs(entries[seed2X].Item1.Left - entries[seed1X].Item1.Right) / width : 0;
                float separationY = height > 0 ? Math.Abs(entries[seed2Y].Item1.Top - entries[seed1Y].Item1.Bottom) / height : 0;

                // Step 3: Choose the axis with the largest normalized separation
                int seed1, seed2;
                if (separationX > separationY)
                {
                    seed1 = seed1X;
                    seed2 = seed2X;
                }
                else
                {
                    seed1 = seed1Y;
                    seed2 = seed2Y;
                }

                // Step 4: Assign seeds to groups
                group1.AddEntry(entries[seed1].Item1, entries[seed1].Item2);
                group2.AddEntry(entries[seed2].Item1, entries[seed2].Item2);

                // Step 5: Remove seeds (order matters to avoid index shifting)
                if (seed1 > seed2)
                {
                    entries.RemoveAt(seed1);
                    entries.RemoveAt(seed2);
                }
                else
                {
                    entries.RemoveAt(seed2);
                    entries.RemoveAt(seed1);
                }
            }
            private void AddToLeastEnlargingNode(Rectangle mbr, int id, Node group1, Node group2)
            {
                float enlargement1 = GetEnlargement(group1.Mbr, mbr);
                float enlargement2 = GetEnlargement(group2.Mbr, mbr);

                if (enlargement1 < enlargement2 ||
                    (enlargement1 == enlargement2 && GetArea(group1.Mbr) < GetArea(group2.Mbr)))
                {
                    group1.AddEntry(mbr, id);
                }
                else
                {
                    group2.AddEntry(mbr, id);
                }
            }
            private static Rectangle RecalculateMbr(Node node)
            {
                if (node.EntryCount == 0)
                    return new Rectangle();

                var mbr = node.Entries[0];
                for (int i = 1; i < node.EntryCount; i++)
                {
                    mbr = GetEnlarged(mbr, node.Entries[i]);
                }

                return mbr;
            }
            private void RemoveNode(Node node)
            {
                if (node == null) throw new ArgumentNullException(nameof(node));

                // Remove node from the map
                if (_nodeMap.Remove(node.NodeId))
                {
                    RecycleNodeId(node.NodeId);
                }

                // Remove references from parent
                if (_childToParentMap.TryGetValue(node.NodeId, out var parentId))
                {
                    var parent = _nodeMap[parentId];
                    int index = parent.Ids.IndexOf(node.NodeId);
                    if (index >= 0)
                    {
                        parent.Ids.RemoveAt(index);
                        parent.Entries.RemoveAt(index);
                        parent.Mbr = RecalculateMbr(parent);
                    }
                }
            }
            private void AdjustTree(Node node, Node newNode)
            {
                while (true)
                {
                    if (newNode == null)
                        return;

                    if (node == _nodeMap[_rootNodeId])
                    {
                        // Handle root splitting
                        var newRoot = new Node(GetNextNodeId(), node.Level + 1);
                        newRoot.AddEntry(node.Mbr, node.NodeId);
                        newRoot.AddEntry(newNode.Mbr, newNode.NodeId);
                        _rootNodeId = newRoot.NodeId;
                        _nodeMap[_rootNodeId] = newRoot;
                        return;
                    }

                    var parent = GetParentNode(node);

                    // Add the new node to the parent
                    parent.AddEntry(newNode.Mbr, newNode.NodeId);

                    // Remove the current node if it falls below the minimum number of entries
                    if (node.EntryCount < _minNodeEntries)
                    {
                        RemoveNode(node);
                    }

                    // If the parent overflows, split it
                    if (parent.EntryCount > _maxNodeEntries)
                    {
                        newNode = SplitNode(parent, parent.Mbr, parent.NodeId);
                    }
                    else
                    {
                        newNode = null;
                    }

                    node = parent;
                }
            }

            public static float GetEnlargement(Rectangle rect1, Rectangle rect2)
            {
                float minX = Math.Min(rect1.Left, rect2.Left);
                float minY = Math.Min(rect1.Top, rect2.Top);
                float maxX = Math.Max(rect1.Right, rect2.Right);
                float maxY = Math.Max(rect1.Bottom, rect2.Bottom);

                float originalArea = (rect1.Right - rect1.Left) * (rect1.Bottom - rect1.Top);
                float enlargedArea = (maxX - minX) * (maxY - minY);

                return enlargedArea - originalArea;
            }

            public static Rectangle GetEnlarged(Rectangle rect1, Rectangle rect2)
            {
                int minX = Math.Min(rect1.Left, rect2.Left);
                int minY = Math.Min(rect1.Top, rect2.Top);
                int maxX = Math.Max(rect1.Right, rect2.Right);
                int maxY = Math.Max(rect1.Bottom, rect2.Bottom);

                return new Rectangle(minX, minY, maxX - minX, maxY - minY);
            }

            public static float GetArea(Rectangle rect)
            {
                return (rect.Right - rect.Left) * (rect.Bottom - rect.Top);
            }

            private void CondenseTree(Node node)
            {
                while (node.Level != 1)
                {
                    var parent = GetParentNode(node);
                    int parentEntryIndex = parent.Ids.IndexOf(node.NodeId);

                    if (node.EntryCount < _minNodeEntries)
                    {
                        // Remove the node and adjust the parent's entries
                        parent.RemoveEntry(parentEntryIndex);
                        RemoveNode(node); // Recycle node ID
                    }
                    else
                    {
                        // Update the parent's MBR for this entry
                        parent.Entries[parentEntryIndex] = node.Mbr;
                    }

                    node = parent;
                }

                // Handle root node adjustment
                if (_nodeMap[_rootNodeId].EntryCount == 1 && _nodeMap[_rootNodeId].Level > 1)
                {
                    var root = _nodeMap[_rootNodeId];
                    _rootNodeId = root.Ids[0];
                    _nodeMap.Remove(root.NodeId);
                    RecycleNodeId(root.NodeId);
                }
            }
            public bool Delete(Rectangle mbr, T item)
            {
                _Removelocker.EnterWriteLock();
                try
                {
                    if (!_itemsToIds.TryGetValue(item, out var id))
                        return false;

                    _idToItems.Remove(id);
                    _itemsToIds.Remove(item);

                    var leafNode = FindLeafNode(_rootNodeId, mbr, id);
                    if (leafNode == null)
                        return false;

                    int index = leafNode.Ids.IndexOf(id);
                    if (index >= 0)
                    {
                        leafNode.Ids.RemoveAt(index);
                        leafNode.Entries.RemoveAt(index);
                        leafNode.Mbr = RecalculateMbr(leafNode);
                    }

                    CondenseTree(leafNode);
                    _size--;
                    return true;
                }
                finally
                {
                    _Removelocker.ExitWriteLock();
                }
            }
            private Node FindLeafNode(int nodeId, Rectangle mbr, int id)
            {
                var node = _nodeMap[nodeId];
                if (node.Level == 1) // Leaf node
                {
                    if (node.Ids.Contains(id) && node.Entries[node.Ids.IndexOf(id)].Equals(mbr))
                        return node;

                    return null;
                }

                for (int i = 0; i < node.EntryCount; i++)
                {
                    if (node.Entries[i].Contains(mbr))
                    {
                        var result = FindLeafNode(node.Ids[i], mbr, id);
                        if (result != null)
                            return result;
                    }
                }

                return null;
            }

            private class Node
            {
                public int NodeId { get; }
                public int Level { get; }
                public Rectangle Mbr { get; set; } = new Rectangle();
                public List<Rectangle> Entries { get; } = new List<Rectangle>();
                public List<int> Ids { get; } = new List<int>();
                public int EntryCount => Entries.Count;

                public Node(int nodeId, int level)
                {
                    NodeId = nodeId;
                    Level = level;
                }
                public void RemoveEntry(int index)
                {
                    if (index < 0 || index >= EntryCount)
                        throw new ArgumentOutOfRangeException(nameof(index));

                    // Remove the entry at the specified index
                    Entries.RemoveAt(index);
                    Ids.RemoveAt(index);

                    // Recalculate the MBR to reflect the updated set of entries
                    if (Entries.Count > 0)
                    {
                        Mbr = Entries[0];
                        for (int i = 1; i < Entries.Count; i++)
                        {
                            Mbr = GetEnlarged(Mbr, Entries[i]);
                        }
                    }
                    else
                    {
                        Mbr = new Rectangle(); // Reset MBR if no entries are left
                    }
                }
                public void AddEntry(Rectangle rect, int id)
                {
                    Entries.Add(rect);
                    Ids.Add(id);
                    Mbr = GetEnlarged(Mbr, rect);
                }

                public void Clear()
                {
                    Entries.Clear();
                    Ids.Clear();
                    Mbr = new Rectangle();
                }

                public IEnumerable<(Rectangle, int)> EntriesWithIds
                {
                    get
                    {
                        for (int i = 0; i < Entries.Count; i++)
                        {
                            yield return (Entries[i], Ids[i]);
                        }
                    }
                }
            }
        }
        public class HybridDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>, IComparable<List<KeyValuePair<TKey, TValue>>>
        {
            private const int Threshold = 16;

            private KeyValuePair<TKey, TValue>[]? _arrayStorage;
            private int _count;
            private Dictionary<TKey, TValue>? _dictionaryStorage;
            private readonly IEqualityComparer<TKey> _keyComparer;

            public HybridDictionary(IEqualityComparer<TKey>? keyComparer = null)
            {
                _arrayStorage = new KeyValuePair<TKey, TValue>[Threshold];
                _keyComparer = keyComparer ?? EqualityComparer<TKey>.Default;
                _count = 0;
                _dictionaryStorage = null;
            }

            public int Count => _dictionaryStorage?.Count ?? _count;

            public bool IsReadOnly => false;

            public ICollection<TKey> Keys
            {
                get
                {
                    if (_dictionaryStorage != null)
                        return _dictionaryStorage.Keys;

                    var keys = new List<TKey>(_count);
                    for (int i = 0; i < _count; i++)
                    {
                        keys.Add(_arrayStorage![i].Key);
                    }

                    return keys;
                }
            }

            public ICollection<TValue> Values
            {
                get
                {
                    if (_dictionaryStorage != null)
                        return _dictionaryStorage.Values;

                    var values = new List<TValue>(_count);
                    for (int i = 0; i < _count; i++)
                    {
                        values.Add(_arrayStorage![i].Value);
                    }

                    return values;
                }
            }

            public TValue this[TKey key]
            {
                get
                {
                    if (TryGetValue(key, out var value))
                        return value;

                    throw new KeyNotFoundException();
                }
                set
                {
                    if (ContainsKey(key))
                        Remove(key);
                    Add(key, value);

                }
            }



            public void Add(TKey key, TValue value)
            {
                if (_dictionaryStorage != null)
                {
                    _dictionaryStorage.Add(key, value);
                    return;
                }

                if (_count >= Threshold)
                {
                    TransitionToDictionary();
                    _dictionaryStorage![key] = value;
                    return;
                }

                ref var searchRef = ref Unsafe.NullRef<KeyValuePair<TKey, TValue>>();
                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, key))
                    {
                        searchRef = ref candidate;
                        break;
                    }
                }

                if (!Unsafe.IsNullRef(ref searchRef))
                    throw new ArgumentException("An element with the same key already exists.");

                Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), _count++) = new KeyValuePair<TKey, TValue>(key, value);
            }

            public bool Contains(KeyValuePair<TKey, TValue> item)
            {
                if (_dictionaryStorage != null)
                {
                    TValue value = CollectionsMarshal.GetValueRefOrNullRef(_dictionaryStorage, item.Key);
                    return Unsafe.Equals(value, item.Value);
                }

                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, item.Key) && EqualityComparer<TValue>.Default.Equals(candidate.Value, item.Value))
                        return true;
                }

                return false;
            }

            public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException(nameof(array));

                if (arrayIndex < 0 || arrayIndex > array.Length)
                    throw new ArgumentOutOfRangeException(nameof(arrayIndex));

                if (array.Length - arrayIndex < Count)
                    throw new ArgumentException("The array is too small to copy the elements.");

                if (_dictionaryStorage != null)
                {
                    foreach (var kvp in _dictionaryStorage)
                        array[arrayIndex++] = kvp;
                }
                else
                {
                    for (int i = 0; i < _count; i++)
                        array[arrayIndex++] = Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                }
            }

            public bool Remove(KeyValuePair<TKey, TValue> item)
            {
                if (_dictionaryStorage != null)
                    return _dictionaryStorage.Remove(item.Key);

                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, item.Key) && EqualityComparer<TValue>.Default.Equals(candidate.Value, item.Value))
                    {
                        Remove(item.Key);
                        return true;
                    }
                }

                return false;
            }

            public bool ContainsKey(TKey key)
            {
                if (_dictionaryStorage != null)
                    if (!Unsafe.IsNullRef(ref CollectionsMarshal.GetValueRefOrNullRef(_dictionaryStorage, key)))
                        return _dictionaryStorage.ContainsKey(key);

                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, key))
                        return true;
                }

                return false;
            }

            public bool Remove(TKey key)
            {
                if (_dictionaryStorage != null)
                    return _dictionaryStorage.Remove(key);

                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, key))
                    {
                        candidate = Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), _count - 1);
                        _arrayStorage![_count - 1] = default;
                        _count--;
                        return true;
                    }
                }

                return false;
            }

            public bool TryGetValue(TKey key, out TValue value)
            {
                if (_dictionaryStorage != null)
                    return _dictionaryStorage.TryGetValue(key, out value);

                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    if (_keyComparer.Equals(candidate.Key, key))
                    {
                        value = candidate.Value;
                        return true;
                    }
                }

                value = default!;
                return false;
            }

            public void Clear()
            {
                if (_dictionaryStorage != null)
                {
                    _dictionaryStorage.Clear();
                    _dictionaryStorage = null;
                }
                else
                {
                    Array.Clear(_arrayStorage!, 0, _count);
                    _count = 0;
                }
            }

            private void TransitionToDictionary()
            {
                _dictionaryStorage = new Dictionary<TKey, TValue>(_keyComparer);
                for (int i = 0; i < _count; i++)
                {
                    ref var candidate = ref Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                    _dictionaryStorage.Add(candidate.Key, candidate.Value);
                }

                _arrayStorage = null;
            }

            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            {
                if (_dictionaryStorage != null)
                {
                    foreach (var kvp in _dictionaryStorage)
                    {
                        yield return kvp;
                    }
                }

                for (int i = 0; i < _count; i++)
                {
                    yield return Unsafe.Add(ref MemoryMarshal.GetArrayDataReference(_arrayStorage!), i);
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public void Add(KeyValuePair<TKey, TValue> item)
            {
                _dictionaryStorage[item.Key] = item.Value;
            }

            public int CompareTo(List<KeyValuePair<TKey, TValue>> other)
            {
                if (_dictionaryStorage == null && other == this._arrayStorage.ToList()) return 1;
                else if (other == this._dictionaryStorage.ToList()) return 1;
                else return 0;
            }

           
        }




        public class SortedList<T> : IList<T>, IComparable<SortedList<T>>, ICollection<T>
        {
            private readonly List<T> _backingList;
            private readonly IComparer<T> _comparer;

            public SortedList(IComparer<T>? comparer = null)
            {
                _backingList = new List<T>();
                _comparer = comparer ?? Comparer<T>.Default;
            }
            public SortedList(IComparer<T>? comparer = null, int initialCapacity = 16)
            {
                if (initialCapacity < 0)
                    throw new ArgumentOutOfRangeException(nameof(initialCapacity), "Initial capacity must be non-negative.");

                _backingList = new List<T>(initialCapacity);
                _comparer = comparer ?? Comparer<T>.Default;
            }
            public T this[int index]
            {
                get => _backingList[index];
                set
                {
                    // Allow setting only if the value maintains the sorted order
                    if ((index > 0 && _comparer.Compare(_backingList[index - 1], value) > 0) ||
                        (index < _backingList.Count - 1 && _comparer.Compare(value, _backingList[index + 1]) > 0))
                    {
                        throw new InvalidOperationException("Setting this value would break the sorted order.");
                    }

                    _backingList[index] = value;
                }
            }

            public int Count => _backingList.Count;
            public bool IsReadOnly => false;

            public void Add(T item)
            {
                // Insert item in sorted order
                int index = _backingList.BinarySearch(item, _comparer);
                if (index < 0)
                {
                    index = ~index; // BinarySearch returns a complement of the insertion point for missing elements
                }
                _backingList.Insert(index, item);
            }
            private bool IsSorted(IList<T> list)
            {
                for (int i = 1; i < list.Count; i++)
                {
                    if (_comparer.Compare(list[i - 1], list[i]) > 0)
                        return false;
                }
                return true;
            }

            // Efficiently merge two sorted lists
            private void MergeSorted(IList<T> sortedValues)
            {
                var result = new List<T>(_backingList.Count + sortedValues.Count);

                int i = 0, j = 0;
                while (i < _backingList.Count && j < sortedValues.Count)
                {
                    if (_comparer.Compare(_backingList[i], sortedValues[j]) <= 0)
                    {
                        result.Add(_backingList[i++]);
                    }
                    else
                    {
                        result.Add(sortedValues[j++]);
                    }
                }

                while (i < _backingList.Count) result.Add(_backingList[i++]);
                while (j < sortedValues.Count) result.Add(sortedValues[j++]);

                _backingList.Clear();
                _backingList.AddRange(result);
            }
            public void AddRange(IEnumerable<T> values)
            {
                if (values == null) throw new ArgumentNullException(nameof(values));

                var newValues = values as IList<T> ?? values.ToList();

                if (newValues.Count == 0) return;

                // Merge if the newValues are sorted
                if (IsSorted(newValues))
                {
                    MergeSorted(newValues);
                }
                else
                {
                    _backingList.AddRange(newValues);
                    _backingList.Sort(_comparer);
                }
            }

            public void Clear() => _backingList.Clear();

            public int CompareTo(SortedList<T> other)
            {
                if (other == null)
                    throw new ArgumentNullException(nameof(other));

                // Compare counts first
                int countComparison = _backingList.Count.CompareTo(other._backingList.Count);
                if (countComparison != 0)
                    return countComparison;

                // Compare elements lexicographically
                Comparer<T> comparer = Comparer<T>.Default;
                for (int i = 0; i < _backingList.Count; i++)
                {
                    int elementComparison = comparer.Compare(_backingList[i], other._backingList[i]);
                    if (elementComparison != 0)
                        return elementComparison; // Return as soon as a difference is found
                }

                // Lists are equal
                return 0;
            }
            public bool Contains(T item) => _backingList.BinarySearch(item, _comparer) >= 0;

            public void CopyTo(T[] array, int arrayIndex) => _backingList.CopyTo(array, arrayIndex);

            public IEnumerator<T> GetEnumerator() => _backingList.GetEnumerator();

            public int IndexOf(T item)
            {
                int index = _backingList.BinarySearch(item, _comparer);
                return index >= 0 ? index : -1;
            }

            public void Insert(int index, T item)
            {
                throw new NotSupportedException("Cannot insert at an arbitrary position in a sorted list.");
            }

            public bool Remove(T item)
            {
                int index = IndexOf(item);
                if (index >= 0)
                {
                    _backingList.RemoveAt(index);
                    return true;
                }
                return false;
            }

            public void RemoveAt(int index) => _backingList.RemoveAt(index);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        public class Multimap<TKey, TValue> : IDictionary<TKey, TValue>, IEnumerable<KeyValuePair<TKey, ICollection<TValue>>>
        {
            private readonly Dictionary<TKey, ICollection<TValue>> items;
            private ILookup<TKey, TValue>? lookupCache;
            private bool isCacheValid;

            public Multimap(IEqualityComparer<TKey>? keyComparer = null, Func<ICollection<TValue>>? collectionFactory = null)
            {
                items = new Dictionary<TKey, ICollection<TValue>>(keyComparer ?? EqualityComparer<TKey>.Default);
                CollectionFactory = collectionFactory ?? (() => new List<TValue>()); // Default to List<TValue>
                isCacheValid = false;
            }

            // Factory for creating collections
            private Func<ICollection<TValue>> CollectionFactory { get; }

            // Gets the collection of keys
            public ICollection<TKey> Keys => items.Keys;

            // Gets the collection of value collections
            public ICollection<ICollection<TValue>> Values => items.Values;

            ICollection<TValue> IDictionary<TKey, TValue>.Values
            {
                get
                {
                    // Flatten all collections into a single list
                    TValue[] allValues = items.Values.SelectMany(collection => collection).ToArray();
                    return allValues;
                }
            }

            // Number of key-value pairs
            public int Count => items.Count;

            bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => false;

            TValue IDictionary<TKey, TValue>.this[TKey key]
            {
                get
                {
                    if (key == null) throw new ArgumentNullException(nameof(key));

                    // Return the first value if it exists, otherwise throw KeyNotFoundException
                    if (items.TryGetValue(key, out var values) && values.Any())
                    {
                        return values.First();
                    }
                    throw new KeyNotFoundException($"The key '{key}' does not exist.");
                }
                set
                {
                    if (key == null) throw new ArgumentNullException(nameof(key));

                    // Replace the collection with a new one containing only the specified value
                    ICollection<TValue> newCollection = CollectionFactory();
                    newCollection.Add(value);
                    items[key] = newCollection;
                    isCacheValid = false; // Invalidate cache
                }
            }
            // Indexer to access or initialize value collections
            public ICollection<TValue> this[TKey key]
            {
                get
                {
                    if (key == null) throw new ArgumentNullException(nameof(key));
                    if (!items.TryGetValue(key, out var values))
                    {
                        values = CollectionFactory();
                        items[key] = values;
                        isCacheValid = false; // Invalidate cache
                    }
                    return values;
                }
            }

            // Adds a single value for a key
            public void Add(TKey key, TValue value)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));
                if (!items.TryGetValue(key, out var values))
                {
                    values = CollectionFactory();
                    items[key] = values;
                }
                values.Add(value);
                isCacheValid = false;
            }

            // Bulk add for multiple values under a single key
            public void AddRange(TKey key, IEnumerable<TValue> values)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));
                if (values == null) throw new ArgumentNullException(nameof(values));

                if (!items.TryGetValue(key, out var valueSet))
                {
                    valueSet = CollectionFactory();
                    items[key] = valueSet;
                }

                // Optimize for known collection types
                switch (valueSet)
                {
                    case List<TValue> list:
                        list.AddRange(values); // Bulk add for List<T>
                        break;
                    case SortedList<TValue> sortedList:
                        sortedList.AddRange(values);
                        break;
                    case HashSet<TValue> set:
                        set.UnionWith(values); // Bulk add for HashSet<T> (ensures uniqueness)
                        break;
                    case SortedSet<TValue> sortedSet:
                        sortedSet.UnionWith(values); // Bulk add for SortedSet<T> (ensures uniqueness and sorted order)
                        break;
                    case MultiSet<TValue> multiSet:
                        multiSet.UnionWith(values);
                        break;

                    default:
                        // General fallback for ICollection<T>
                        foreach (var value in values)
                        {
                            valueSet.Add(value);
                        }
                        break;
                }

                isCacheValid = false; // Invalidate cache
            }
            // Removes a specific value under a key
            public bool Remove(TKey key, TValue value)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));

                // Try to get the collection associated with the key
                if (items.TryGetValue(key, out var valueSet))
                {
                    if (valueSet.Remove(value)) // Remove the value
                    {
                        // If the collection is empty, remove the key entirely
                        if (valueSet.Count == 0)
                            items.Remove(key);

                        isCacheValid = false; // Invalidate cache
                        return true;
                    }
                }

                return false; // Key or value not found
            }
            // Removes all values under a specific key
            public bool RemoveAll(TKey key)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));

                // Directly remove the key and associated collection
                if (items.Remove(key))
                {
                    isCacheValid = false; // Invalidate cache
                    return true;
                }

                return false; // Key not found
            }
            public void Clear()
            {
                items.Clear();
                isCacheValid = false;
            }

            // Converts to Lookup for efficient lookups
            public ILookup<TKey, TValue> AsLookup()
            {
                if (!isCacheValid)
                {
                    // Use ToLookup to create a flattened key-value mapping
                    lookupCache = items
                    .SelectMany(kvp => kvp.Value.Select(value => new { kvp.Key, Value = value }))
                    .ToLookup(item => item.Key, item => item.Value);

                    isCacheValid = true;
                }

                return lookupCache!;
            }

            public IEnumerator<KeyValuePair<TKey, ICollection<TValue>>> GetEnumerator()
            {
                foreach (var kvp in items)
                {
                    yield return new KeyValuePair<TKey, ICollection<TValue>>(kvp.Key, kvp.Value);
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            bool IDictionary<TKey, TValue>.ContainsKey(TKey key) => items.ContainsKey(key);

            bool IDictionary<TKey, TValue>.Remove(TKey key) => items.Remove(key);

            bool IDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
            {
                if (items.TryGetValue(key, out var values) && values.Count > 0)
                {
                    value = values.First();
                    return true;
                }
                value = default;
                return false;
            }

            void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) => Add(item.Key, item.Value);

            bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item) =>
                items.TryGetValue(item.Key, out var values) && values.Contains(item.Value);

            void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
            {
                if (array == null) throw new ArgumentNullException(nameof(array));
                if (arrayIndex < 0 || arrayIndex >= array.Length) throw new ArgumentOutOfRangeException(nameof(arrayIndex));

                foreach (var kvp in items)
                {
                    foreach (var value in kvp.Value)
                    {
                        if (arrayIndex >= array.Length) throw new ArgumentException("Array is too small.");
                        array[arrayIndex++] = new KeyValuePair<TKey, TValue>(kvp.Key, value);
                    }
                }
            }

            bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item) => Remove(item.Key, item.Value);

            IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
            {
                foreach (var kvp in items)
                {
                    foreach (var value in kvp.Value)
                    {
                        yield return new KeyValuePair<TKey, TValue>(kvp.Key, value);
                    }
                }
            }
        }

#nullable enable
        public class MultiSet<T> : ISet<T?>, ICollection<T?>, IComparable<MultiSet<T?>>
        {
            private readonly Dictionary<T?, int> _dictionary;
            private int _count;
            private int _nullCount;


            public MultiSet(IEqualityComparer<T>? comparer = null)
            {
                _dictionary = new Dictionary<T, int>(comparer ?? EqualityComparer<T?>.Default);
                _count = 0;
                _nullCount = 0;
            }

            public MultiSet(int size)
            {
                _dictionary = new Dictionary<T?, int>(size);
            }

            public bool Add(T? item)
            {
                if (item == null)
                {
                    _nullCount++;
                    return true;
                }
                if (_dictionary.TryGetValue(item, out int currentCount))
                {
                    _dictionary[item] = currentCount + 1;
                }
                else
                {
                    _dictionary[item] = 1;
                }

                _count++;
                return true;
            }

            public bool Remove(T? item)
            {
                if (item == null)
                {
                    if (_nullCount > 0)
                    {
                        _nullCount--;
                        return true;
                    }
                    return false;
                }
                if (_dictionary.TryGetValue(item, out int currentCount))
                {
                    if (currentCount > 1)
                    {
                        _dictionary[item] = currentCount - 1;
                    }
                    else
                    {
                        _dictionary.Remove(item);
                    }

                    _count--;
                    return true;
                }

                return false;
            }

            public int CountOf(T? item)
            {
                if (item == null)
                {
                    return _nullCount;
                }
                return _dictionary.TryGetValue(item, out int count) ? count : 0;
            }

            public int CountOf(object? item)
            {
                if (item == null)
                {
                    return _nullCount;
                }
                return _dictionary.TryGetValue((T)item, out int count) ? count : 0;
            }


            public bool Contains(T? item) => item == null ? (_nullCount != 0) : _dictionary.ContainsKey(item);

            public bool Contains(object? item) => item == null ? (_nullCount != 0) : _dictionary.ContainsKey((T)item);

            public void Clear()
            {
                _dictionary.Clear();
                _count = 0;
                _nullCount = 0;
            }

            public void CopyTo(T?[] array, int arrayIndex)
            {
                if (array == null) throw new ArgumentNullException(nameof(array));
                if (arrayIndex < 0 || arrayIndex > array.Length) throw new ArgumentOutOfRangeException(nameof(arrayIndex));
                if (array.Length - arrayIndex < _count) throw new ArgumentException("Insufficient array capacity");

                int index = arrayIndex;
                foreach (var kvp in _dictionary)
                {
                    for (int i = 0; i < kvp.Value; i++)
                    {
                        array[index++] = kvp.Key;
                    }
                }
            }

            public int Count => _count + _nullCount;

            public bool IsReadOnly => false;

            public IEnumerator<T?> GetEnumerator() => new MultiSetEnumerator(this);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
            public void AddParallel(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Step 1: Aggregate counts in parallel into a local thread-safe dictionary
                ConcurrentDictionary<T, int> localResults = new ConcurrentDictionary<T, int>();
                Parallel.ForEach(other, item =>
                {
                    localResults.AddOrUpdate(item, 1, (_, count) => count + 1);
                });

                foreach (KeyValuePair<T, int> kvp in localResults)
                {
                    if (_dictionary.TryGetValue(kvp.Key, out int currentCount))
                    {
                        _dictionary[kvp.Key] = currentCount + kvp.Value;
                    }
                    else
                    {
                        _dictionary[kvp.Key] = kvp.Value;
                    }

                    // Increment the total count by the added items
                    _count += kvp.Value;
                }

            }

            public void UnionWith(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is MultiSet<T> otherMultiSet)
                {
                    foreach (var (item, count) in otherMultiSet._dictionary)
                    {
                        if (_dictionary.TryGetValue(item, out int currentCount))
                        {
                            _dictionary[item] = currentCount + count;
                        }
                        else
                        {
                            _dictionary[item] = count;
                        }

                        _count += count;
                    }
                }
                else
                {
                    foreach (var item in other)
                    {
                        Add(item);
                    }
                }
            }

            public bool IsSubsetOf(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is SortedSet<T> sortedSet)
                {
                    foreach (var item in _dictionary.Keys)
                    {
                        if (!sortedSet.Contains(item))
                            return false;
                    }
                    return true;
                }

                HashSet<T> otherSet = other is HashSet<T> set ? set : new HashSet<T>(other);
                foreach (var item in _dictionary.Keys)
                {
                    if (!otherSet.Contains(item))
                        return false;
                }

                return true;
            }

            public bool IsSupersetOf(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is SortedSet<T> sortedSet)
                {
                    foreach (var item in sortedSet)
                    {
                        if (!_dictionary.ContainsKey(item))
                            return false;
                    }
                    return true;
                }

                foreach (var item in other)
                {
                    if (!_dictionary.ContainsKey(item))
                        return false;
                }

                return true;
            }

            public bool Overlaps(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                foreach (var item in other)
                {
                    if (_dictionary.ContainsKey(item))
                        return true;
                }
                return false;
            }

            public bool SetEquals(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Create a dictionary to track counts in `other`
                var otherCounts = new Dictionary<T, int>();

                foreach (var item in other)
                {
                    if (otherCounts.ContainsKey(item))
                    {
                        otherCounts[item]++;
                    }
                    else
                    {
                        otherCounts[item] = 1;
                    }
                }

                // Compare the two dictionaries
                if (_dictionary.Count != otherCounts.Count)
                    return false;

                foreach (var (item, count) in _dictionary)
                {
                    if (!otherCounts.TryGetValue(item, out int otherCount) || count != otherCount)
                        return false;
                }

                return true;
            }
            void ICollection<T?>.Add(T? item)
            {
                Add(item);
            }

            public bool IsProperSubsetOf(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                var otherCounts = new Dictionary<T, int>();

                foreach (var item in other)
                {
                    if (otherCounts.ContainsKey(item))
                    {
                        otherCounts[item]++;
                    }
                    else
                    {
                        otherCounts[item] = 1;
                    }
                }

                bool hasExtra = false;

                // Check all items in the current multiset
                foreach (var (item, count) in _dictionary)
                {
                    if (!otherCounts.TryGetValue(item, out int otherCount) || count > otherCount)
                        return false; // `other` does not fully contain the current multiset

                    if (count < otherCount)
                        hasExtra = true; // `other` has strictly greater counts
                }

                // Check if `other` has additional items not in the current set
                if (!hasExtra)
                {
                    foreach (var (item, count) in otherCounts)
                    {
                        if (!_dictionary.ContainsKey(item))
                        {
                            hasExtra = true;
                            break;
                        }
                    }
                }

                return hasExtra;
            }

            public bool IsProperSupersetOf(IEnumerable<T?> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                var otherCounts = new Dictionary<T?, int>();

                foreach (var item in other)
                {
                    if (otherCounts.ContainsKey(item))
                    {
                        otherCounts[item]++;
                    }
                    else
                    {
                        otherCounts[item] = 1;
                    }
                }

                bool hasExtra = false;

                // Check all items in `other`
                foreach (var (item, count) in otherCounts)
                {
                    if (!_dictionary.TryGetValue(item, out int currentCount) || count > currentCount)
                        return false; // Current multiset does not fully contain `other`

                    if (count < currentCount)
                        hasExtra = true; // Current multiset has strictly greater counts
                }

                // Check if the current multiset has additional items not in `other`
                if (!hasExtra)
                {
                    foreach (var (item, count) in _dictionary)
                    {
                        if (!otherCounts.ContainsKey(item))
                        {
                            hasExtra = true;
                            break;
                        }
                    }
                }

                return hasExtra;
            }

            public int CompareTo(MultiSet<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                int countComparison = Count.CompareTo(other.Count);
                if (countComparison != 0) return countComparison;

                var thisKeys = _dictionary.Keys.OrderBy(x => x, Comparer<T>.Default).ToList();
                var otherKeys = other._dictionary.Keys.OrderBy(x => x, Comparer<T>.Default).ToList();

                for (int i = 0; i < thisKeys.Count; i++)
                {
                    int comparison = Comparer<T>.Default.Compare(thisKeys[i], otherKeys[i]);
                    if (comparison != 0) return comparison;
                }

                return 0; // Equal
            }

            public void ExceptWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                foreach (var item in other)
                {
                    if (_dictionary.TryGetValue(item, out int count))
                    {
                        // Remove one occurrence of the item
                        if (count > 1)
                        {
                            _dictionary[item] = count - 1; // Decrease count
                        }
                        else
                        {
                            _dictionary.Remove(item); // Remove item entirely if count becomes 0
                        }

                        _count--; // Adjust total count
                    }
                }
            }

            public void IntersectWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Create a dictionary to store counts from `other`
                var otherCounts = new Dictionary<T, int>();

                foreach (var item in other)
                {
                    if (otherCounts.ContainsKey(item))
                    {
                        otherCounts[item]++;
                    }
                    else
                    {
                        otherCounts[item] = 1;
                    }
                }

                // Adjust the counts in the current set based on `otherCounts`
                var keysToRemove = new List<T>();
                foreach (var (item, count) in _dictionary)
                {
                    if (otherCounts.TryGetValue(item, out int otherCount))
                    {
                        // Keep the minimum count between the two sets
                        _dictionary[item] = Math.Min(count, otherCount);
                    }
                    else
                    {
                        // Mark for removal if the item does not exist in `other`
                        keysToRemove.Add(item);
                    }
                }

                // Remove keys that are not in `other`
                foreach (var key in keysToRemove)
                {
                    _count -= _dictionary[key];
                    _dictionary.Remove(key);
                }

                // Recalculate total count
                _count = _dictionary.Values.Sum();
            }

            public MultiSet<T> MultiSetIntersect(MultiSet<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                var result = new MultiSet<T>(_dictionary.Comparer);

                foreach (var (item, count) in _dictionary)
                {
                    if (other._dictionary.TryGetValue(item, out int otherCount))
                    {
                        result._dictionary[item] = Math.Min(count, otherCount);
                    }
                }

                result._nullCount = Math.Min(_nullCount, other._nullCount);
                result._count = result._dictionary.Values.Sum() + result._nullCount;
                return result;
            }
            public MultiSet<T> MultiSetUnion(MultiSet<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                var result = new MultiSet<T>(_dictionary.Comparer);

                foreach (var (item, count) in _dictionary)
                {
                    result._dictionary[item] = count;
                }

                foreach (var (item, count) in other._dictionary)
                {
                    if (result._dictionary.TryGetValue(item, out int existingCount))
                    {
                        result._dictionary[item] = existingCount + count;
                    }
                    else
                    {
                        result._dictionary[item] = count;
                    }
                }

                result._nullCount = _nullCount + other._nullCount;
                result._count = result._dictionary.Values.Sum() + result._nullCount;
                return result;
            }

            public IEnumerable<T?> Mode()
            {
                int maxFrequency = Math.Max(_dictionary.Values.DefaultIfEmpty(0).Max(), _nullCount);
                if (_nullCount == maxFrequency && _nullCount > 0)
                {
                    yield return default;
                }

                foreach (var (item, count) in _dictionary)
                {
                    if (count == maxFrequency)
                    {
                        yield return item;
                    }
                }
            }

            public IEnumerable<T?> Median()
            {
                // Total count of all elements including nulls
                int totalCount = _dictionary.Values.Sum() + _nullCount;

                if (totalCount == 0)
                {
                    yield break; // No elements, no median
                }

                // Calculate the middle indices
                int midIndex1 = (totalCount - 1) / 2; // Middle index for odd or first middle index for even
                int midIndex2 = totalCount % 2 == 0 ? totalCount / 2 : midIndex1; // Second middle index for even

                int cumulativeCount = 0;

                // Handle nulls if present
                if (_nullCount > 0)
                {
                    cumulativeCount += _nullCount;
                    if (midIndex1 < cumulativeCount)
                    {
                        yield return default;
                    }
                    if (midIndex2 < cumulativeCount && midIndex2 != midIndex1)
                    {
                        yield return default;
                    }
                }
                
                // Iterate through the dictionary in sorted order
                foreach (var kvp in _dictionary.OrderBy(kvp => kvp.Key))
                {
                    cumulativeCount += kvp.Value;

                    if (midIndex1 < cumulativeCount)
                    {
                        yield return kvp.Key;
                    }
                    if (midIndex2 < cumulativeCount && midIndex2 != midIndex1)
                    {
                        yield return kvp.Key;
                    }

                    // Stop early if both medians are found
                    if (midIndex1 < cumulativeCount && midIndex2 < cumulativeCount)
                    {
                        break;
                    }
                }
            }

            public void SymmetricExceptWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Create a dictionary to track counts in `other`
                var otherCounts = new Dictionary<T, int>();

                foreach (var item in other)
                {
                    if (otherCounts.ContainsKey(item))
                    {
                        otherCounts[item]++;
                    }
                    else
                    {
                        otherCounts[item] = 1;
                    }
                }

                // Directly calculate the new `_count` while processing
                int newCount = 0;

                // Handle items in the current multiset
                var keysToRemove = new List<T>();
                foreach (var (item, count) in _dictionary)
                {
                    if (otherCounts.TryGetValue(item, out int otherCount))
                    {
                        // Calculate the symmetric difference in counts
                        int difference = Math.Abs(count - otherCount);

                        if (difference > 0)
                        {
                            _dictionary[item] = difference;
                            newCount += difference; // Update the total count
                        }
                        else
                        {
                            keysToRemove.Add(item); // Mark for removal if counts are equal
                        }

                        // Remove the item from `otherCounts` as it's already processed
                        otherCounts.Remove(item);
                    }
                    else
                    {
                        newCount += count; // Keep the count for items not in `other`
                    }
                }

                // Remove items marked for deletion
                foreach (var key in keysToRemove)
                {
                    _dictionary.Remove(key);
                }

                // Add remaining items from `otherCounts` to the current multiset
                foreach (var (item, count) in otherCounts)
                {
                    if (_dictionary.ContainsKey(item))
                    {
                        _dictionary[item] += count;
                    }
                    else
                    {
                        _dictionary[item] = count;
                    }

                    newCount += count; // Update the total count directly
                }

                // Update the total count
                _count = newCount;
            }

            private struct MultiSetEnumerator : IEnumerator<T>
            {
                private readonly MultiSet<T> _multiSet;
                private readonly IEnumerator<KeyValuePair<T, int>> _dictionaryEnumerator;
                private T _current;
                private int _remaining;

                public MultiSetEnumerator(MultiSet<T> multiSet)
                {
                    _multiSet = multiSet;
                    _dictionaryEnumerator = _multiSet._dictionary.GetEnumerator();
                    _current = default!;
                    _remaining = 0;
                }

                public T Current => _current;

                object IEnumerator.Current => Current!;

                public bool MoveNext()
                {
                    if (_remaining > 0)
                    {
                        _remaining--;
                        return true;
                    }

                    if (_dictionaryEnumerator.MoveNext())
                    {
                        var currentPair = _dictionaryEnumerator.Current;
                        _current = currentPair.Key;
                        _remaining = currentPair.Value - 1;
                        return true;
                    }

                    return false;
                }

                public void Reset()
                {
                    _dictionaryEnumerator.Reset();
                    _current = default!;
                    _remaining = 0;
                }

                public void Dispose()
                {
                    _dictionaryEnumerator.Dispose();
                }
            }

            public bool Add(object? value)
            {
                if (value == null)
                {
                    _nullCount++;
                    return true;
                }
                else
                {
                    return Add((T)value); // Forward non-null values to the existing Add method
                }
            }

            public bool Remove(object? value)
            {
                if (value == null)
                {
                    if (_nullCount > 0)
                    {
                        _nullCount--;
                        return true;
                    }
                    return false; // Can't remove null if none exist
                }
                else
                {
                    return Remove((T)value); // Forward non-null values to the existing Remove method
                }
            }
        }


        public class ConcurrentArray<T> : ICollection<T>, IDisposable
        {
            private static readonly ArrayPool<T> _pool = ArrayPool<T>.Shared;
            private T[] _buffer;                // Shared buffer for elements
            private int _count;                 // Tracks the current element count
            private int _capacity;              // Capacity of the array
            private object _lock = new object();

            private static Type type = typeof(T);

            public static bool CanUseTryCast = type.IsPrimitive || type.IsEnum || type == typeof(string);
            public static bool CanUseVolatile => type.IsClass ||CanUseTryCast || (type != null && Nullable.GetUnderlyingType(type) != null);
            public ConcurrentArray(int initialCapacity = 16)
            {
                if (initialCapacity <= 0)
                    throw new ArgumentOutOfRangeException(nameof(initialCapacity), "Initial capacity must be greater than zero.");

                _capacity = initialCapacity;
                _buffer = _pool.Rent(initialCapacity);
                _count = 0;
            }

            public int Count => Volatile.Read(ref _count); // Atomic read for thread-safe access

            public bool IsReadOnly => false;

            /// <summary>
            /// Adds an item to the array in a thread-safe manner.
            /// </summary>
            private readonly object _lockobj = new object();

            public void Add(T item)
            {
                lock (_lockobj)
                {
                    if (_count >= _capacity)
                    {
                        Resize(); // Resize buffer if capacity is exceeded
                    }

                    _buffer[_count++] = item; // Safely add the item and increment the count
                }
            }

            /// <summary>
            /// Clears the array by resetting the count.
            /// </summary>
            public void Clear()
            {
                // Set count to 0 atomically
                int oldCount = Interlocked.Exchange(ref _count, 0);

                // Optionally clear the used portion of the buffer
                Array.Clear(_buffer, 0, oldCount);
            }

            /// <summary>
            /// Checks if the array contains a specific item.
            /// </summary>
            public bool Contains(T item)
            {
                T[] localBuffer;
                int localCount;

                lock (_lock)
                {
                    // Capture the current state of _buffer and _count
                    localBuffer = _buffer;
                    localCount = _count;
                }

                // Iterate over the captured state outside the lock to reduce contention
                for (int i = 0; i < localCount; i++)
                {
                    if (EqualityComparer<T>.Default.Equals(localBuffer[i], item))
                        return true;
                }

                return false;
            }

            /// <summary>
            /// Copies the elements of the array to another array.
            /// </summary>
            public void CopyTo(T[] array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException(nameof(array));
                if (arrayIndex < 0 || arrayIndex + _count > array.Length)
                    throw new ArgumentOutOfRangeException(nameof(arrayIndex));

                Array.Copy(_buffer, 0, array, arrayIndex, _count);
            }

            /// <summary>
            /// Removes the first occurrence of a specific item.
            /// </summary>
            public bool Remove(T item)
            {
                lock (_lockobj)
                {
                    int oldCount = _count;
                    for (int i = 0; i < oldCount; i++)
                    {
                        if (EqualityComparer<T>.Default.Equals(_buffer[i], item))
                        {
                            _buffer[i] = _buffer[--_count]; // Move the last element and reduce the count
                            _buffer[_count] = default!;    // Clear the last slot
                            return true;
                        }
                    }
                    return false; // Item not found
                }
            }

            /// <summary>
            /// Gets or sets an element at the specified index.
            /// </summary>
            /// 

            public T this[int index]
            {
                get
                {
                    if (index < 0 || index >= _count)
                        throw new ArgumentOutOfRangeException(nameof(index));

                    lock (_lock)
                    {
                        return _buffer[index];
                    }
                }

                set
                {
                    if (index < 0 || index >= _count)
                        throw new ArgumentOutOfRangeException(nameof(index));

                    lock (_lock)
                    {
                        _buffer[index] = value;
                    }
                }
            }

            public T[] Slice(int start, int stop)
            {
                if (start < 0 || stop > _count || start > stop)
                    throw new ArgumentOutOfRangeException("Invalid slice range.");

                lock (_lock)
                {
                    // Create a Span<T> over the valid range and convert it to an array
                    return new Span<T>(_buffer, start, stop - start).ToArray();
                }
            }
            /// <summary>
            /// Resizes the buffer dynamically when capacity is exceeded.
            /// </summary>
            private void Resize()
            {
                lock (_lock)
                {
                    int newCapacity = _capacity * 2;
                    T[] newBuffer = new T[newCapacity];
                    Array.Copy(_buffer, 0, newBuffer, 0, _count);
                    _buffer = newBuffer;
                    _capacity = newCapacity;
                }
            }


            /// <summary>
            /// Enumerates the array in a thread-safe manner by creating a snapshot.
            /// </summary>
            public IEnumerator<T> GetEnumerator()
            {
                T[] snapshot;
                int count;

                // Lock to ensure a consistent snapshot
                lock (_lock)
                {
                    count = _count;
                    snapshot = new T[count];
                    Array.Copy(_buffer, snapshot, count);
                }

                // Enumerate the snapshot without locking
                for (int i = 0; i < count; i++)
                {
                    yield return snapshot[i];
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            /// <summary>
            /// Disposes the buffer and returns it to the pool.
            /// </summary>
            private bool _disposed = false;

            public void Dispose()
            {
                if (_disposed) return;

                _pool.Return(_buffer, clearArray: false);
                _buffer = Array.Empty<T>();
                _count = 0;

                _disposed = true;
                GC.SuppressFinalize(this);
            }
        }
        public class ConcurrentSet<T> : IEnumerable<T>, ISet<T>, IComparable<ConcurrentSet<T>> where T : class
        {
            private readonly ConcurrentDictionary<T, bool> _values = new ConcurrentDictionary<T, bool>();

            public int Count => _values.Count;

            public bool IsReadOnly => false;

            public bool Add(T item)
            {
                return _values.TryAdd(item, true);
            }

            public void Clear()
            {
                _values.Clear();
            }

            public bool Contains(T item)
            {
                return _values.ContainsKey(item);
            }



            public void CopyTo(T[] array, int arrayIndex)
            {

                if (array == null) throw new ArgumentNullException(nameof(array));
                if (arrayIndex < 0) throw new ArgumentOutOfRangeException(nameof(arrayIndex));
                if (array.Length - arrayIndex < Count) throw new ArgumentException("The array is too small to hold the items.");
                if (array.Length == 0) throw new ArgumentException("empty array");
                try
                {
                    _values.Keys.CopyTo(array, arrayIndex);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Copy operation failed due to concurrent modifications.", ex);
                }
            }
            public bool Remove(T item)
            {
                return _values.TryRemove(item, out _);
            }

            public IEnumerator<T> GetEnumerator()
            {
                return _values.Keys.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            // IntersectWith using dynamic type checks
            public void IntersectWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is ICollection<T> collection && collection.Count < _values.Count)
                {
                    // Iterate through the smaller collection
                    foreach (var item in collection)
                    {
                        // If the item is not in the larger set, remove it
                        if (!_values.TryRemove(item, out _))
                        {
                            continue;
                        }
                    }
                }
                else if (other is SortedSet<T> sortedSet)
                {
                    // Use SortedSet directly for O(log n) lookups
                    foreach (var item in _values.Keys)
                    {
                        if (!sortedSet.Contains(item))
                        {
                            _values.TryRemove(item, out _);
                        }
                    }
                }
                else if (other is HashSet<T> otherSet)
                {
                    foreach (var item in _values.Keys)
                    {
                        if (!otherSet.Contains(item))
                        {
                            _values.TryRemove(item, out _);
                        }
                    }
                }
                else
                {
                    // Fallback to HashSet for general IEnumerable
                    HashSet<T> newSet = new HashSet<T>(other);
                    foreach (var item in _values.Keys)
                    {
                        if (!newSet.Contains(item))
                        {
                            _values.TryRemove(item, out _);
                        }
                    }
                }
            }

            public void ExceptWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is ICollection<T> collection && collection.Count < _values.Count)
                {
                    foreach (var item in collection)
                    {
                        _values.TryRemove(item, out _);
                    }
                }
                else
                {
                    HashSet<T> otherSet = other is HashSet<T> set ? set : new HashSet<T>(other);
                    foreach (var item in otherSet)
                    {
                        _values.TryRemove(item, out _);
                    }
                }
            }

            public void UnionWith(IEnumerable<T> other, bool overwrite = false)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                foreach (var item in other)
                {
                    if (overwrite)
                    {
                        _values[item] = true; // Overwrite regardless of existence
                    }
                    else
                    {
                        _values.TryAdd(item, true); // Skip if already exists
                    }
                }
            }

            public void SymmetricExceptWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is ICollection<T> collection && collection.Count < _values.Count)
                {
                    foreach (var item in collection)
                    {
                        if (!_values.TryRemove(item, out _))
                        {
                            _values.TryAdd(item, true);
                        }
                    }
                }
                else
                {
                    ISet<T> otherSet = other switch
                    {
                        HashSet<T> hashSet => hashSet,           // Already a HashSet, use it directly
                        SortedSet<T> sortedSet => sortedSet,     // Already a SortedSet, use it directly
                        _ => new HashSet<T>(other)               // For other types, create a new HashSet
                    };

                    foreach (T item in otherSet)
                    {
                        if (!_values.TryRemove(item, out _))
                        {
                            _values[item] = true;
                        }
                    }
                }
            }

            public bool IsSubsetOf(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is SortedSet<T> sortedSet)
                {
                    foreach (var item in _values.Keys)
                    {
                        if (!sortedSet.Contains(item))
                            return false;
                    }
                    return true;
                }

                HashSet<T> otherSet = other is HashSet<T> set ? set : new HashSet<T>(other);
                foreach (var item in _values.Keys)
                {
                    if (!otherSet.Contains(item))
                        return false;
                }

                return true;
            }

            public bool IsSupersetOf(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                if (other is SortedSet<T> sortedSet)
                {
                    foreach (var item in sortedSet)
                    {
                        if (!_values.ContainsKey(item))
                            return false;
                    }
                    return true;
                }

                foreach (var item in other)
                {
                    if (!_values.ContainsKey(item))
                        return false;
                }

                return true;
            }

            public bool Overlaps(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                foreach (var item in other)
                {
                    if (_values.ContainsKey(item))
                        return true;
                }
                return false;
            }

            public bool SetEquals(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Handle empty sets
                if (!_values.Keys.Any() && !other.Any()) return true; // Both are empty
                if (!_values.Keys.Any() || !other.Any()) return false; // Only one is empty

                // If `other` is a `HashSet<T>` or `SortedSet<T>`, use efficient built-in checks
                if (other is ICollection<T> otherCollection)
                {
                    // Early exit: If sizes differ, sets cannot be equal
                    if (otherCollection.Count != _values.Count)
                        return false;

                    if (otherCollection is HashSet<T> || otherCollection is SortedSet<T>)
                    {
                        return otherCollection.All(_values.ContainsKey);
                    }
                }

                // General case: Compare the two sets using ExceptWith
                var otherSet = new HashSet<T>(other);

                // Early exit: If sizes differ, sets cannot be equal
                if (_values.Count != otherSet.Count)
                    return false;

                // Use ExceptWith to check for equality
                otherSet.ExceptWith(_values.Keys);
                return otherSet.Count == 0; // If `otherSet` is empty, sets are equal
            }
            void ICollection<T>.Add(T item)
            {
                _values[item] = true;
            }

            public bool IsProperSubsetOf(IEnumerable<T> other)
            {
                if (other == null && !other.Any()) throw new ArgumentNullException(nameof(other));

                // Try to get the count of `other`
                int otherCount = other.TryGetNonEnumeratedCount(out int count) ? count : -1;

                // If `other` has fewer or equal elements, it cannot contain a proper subset
                if (otherCount != -1 && otherCount <= _values.Count)
                    return false;

                // If `other` is a `SortedSet` or `HashSet`, use their efficient methods
                if (other is SortedSet<T> sortedSet)
                {
                    return sortedSet.IsProperSupersetOf(_values.Keys); // Reversed logic for subset
                }

                if (other is HashSet<T> hashSet)
                {
                    return hashSet.IsProperSupersetOf(_values.Keys); // Reversed logic for subset
                }

                // Fallback for general `IEnumerable`
                var otherSet = new HashSet<T>(other);

                // Exit early if any item in `_values.Keys` is not in `otherSet`
                foreach (var item in _values.Keys)
                {
                    if (!otherSet.Contains(item))
                        return false;
                }

                // Final size comparison for a proper subset
                return _values.Count < otherSet.Count;
            }

            public bool IsProperSupersetOf(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Try to get the count of `other`
                int otherCount = other.TryGetNonEnumeratedCount(out int count) ? count : -1;

                // If `this` has fewer or equal elements, it cannot contain a proper superset
                if (otherCount != -1 && _values.Count <= otherCount)
                    return false;

                // If `other` is a `SortedSet` or `HashSet`, use their efficient methods
                if (other is SortedSet<T> sortedSet)
                {
                    return sortedSet.IsProperSubsetOf(_values.Keys); // Reversed logic for superset
                }
                else if (other is HashSet<T> hashSet)
                {
                    return hashSet.IsProperSubsetOf(_values.Keys); // Reversed logic for superset
                }

                // Fallback for general `IEnumerable`
                var otherSet = new HashSet<T>(other);

                // Exit early if any item in `other` is not in `_values`
                foreach (var item in otherSet)
                {
                    if (!_values.ContainsKey(item))
                        return false;
                }

                // Final size comparison for a proper superset
                return _values.Count > otherSet.Count;
            }
            public void UnionWith(IEnumerable<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Optimize based on the type of `other`
                if (other is ICollection<T> collection)
                {
                    foreach (T item in collection)
                    {
                        _values.TryAdd(item, true);
                    }
                }
            }

            public int CompareTo(ConcurrentSet<T> other)
            {
                if (other == null) throw new ArgumentNullException(nameof(other));

                // Compare element counts first
                int countComparison = this.Count.CompareTo(other.Count);
                if (countComparison != 0)
                    return countComparison;

                // Use SortedSet<T> for lexicographical comparison
                var thisSorted = new SortedSet<T>(_values.Keys);
                var otherSorted = new SortedSet<T>(other._values.Keys);

                using (var thisEnumerator = thisSorted.GetEnumerator())
                using (var otherEnumerator = otherSorted.GetEnumerator())
                {
                    while (thisEnumerator.MoveNext() && otherEnumerator.MoveNext())
                    {
                        int comparison = Comparer<T>.Default.Compare(thisEnumerator.Current, otherEnumerator.Current);
                        if (comparison != 0)
                            return comparison;
                    }
                }

                return 0; // If all elements are equal
            }
        }

        public class MemoryCacheHandler
        {
            private IMemoryCache _memoryCache;
            private readonly TimeSpan _defaultSlidingExpiration = TimeSpan.FromHours(1);

            // Constructor for Dependency Injection
            public MemoryCacheHandler(IMemoryCache memoryCache)
            {
                _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));
            }

            // Constructor for Custom Cache Initialization
            public MemoryCacheHandler()
            {
                MemoryCacheOptions memoryCacheOptions = new MemoryCacheOptions
                {
                    SizeLimit = 10000, // Total cache size
                    ExpirationScanFrequency = TimeSpan.FromMinutes(5), // Scan every 5 minutes
                    CompactionPercentage = 0.25, // Evict 25% of items during compaction
                    Clock = new SystemClock() // Default clock
                };

                _memoryCache = new MemoryCache(memoryCacheOptions);
            }

            // Retrieve or create an entry with a sliding expiration
            public T GetOrCreate<T>(object key, Func<ICacheEntry, T> createItem)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));
                if (createItem == null) throw new ArgumentNullException(nameof(createItem));

                return _memoryCache.GetOrCreate(key, entry =>
                {
                    entry.SlidingExpiration = _defaultSlidingExpiration;
                    return createItem(entry);
                });
            }

            public void Clear()
            {
                _memoryCache.Dispose();
                MemoryCacheOptions memoryCacheOptions = new MemoryCacheOptions
                {
                    SizeLimit = 10000, // Total cache size
                    ExpirationScanFrequency = TimeSpan.FromMinutes(5), // Scan every 5 minutes
                    CompactionPercentage = 0.25, // Evict 25% of items during compaction
                    Clock = new SystemClock() // Default clock
                };

                _memoryCache = new MemoryCache(memoryCacheOptions);
            }

            public bool AddAll(IEnumerable<ICacheEntry> entries)
            {
                if (entries == null)
                    throw new ArgumentNullException(nameof(entries));
                foreach (var entry in entries)
                {
                    _memoryCache.CreateEntry(entry);
                }
                return true;
            }
            // Set a cache entry (overwrites any existing entry)
            public void SetEntry(object key, object value, TimeSpan? slidingExpiration = null)
            {
                if (key == null) throw new ArgumentNullException(nameof(key));
                if (value == null) throw new ArgumentNullException(nameof(value));

                var options = new MemoryCacheEntryOptions
                {
                    SlidingExpiration = slidingExpiration ?? _defaultSlidingExpiration
                };

                _memoryCache.Set(key, value, options);
            }

            // Retrieve an entry or add it to the cache
            public T GetEntry<T>(object key)
            {
                if (key == null)
                    throw new ArgumentNullException(nameof(key));

                return _memoryCache.TryGetValue(key, out T value) ? value : default;
            }


        }
    }
}