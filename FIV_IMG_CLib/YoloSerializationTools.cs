﻿namespace FIV_IMG_CLib
{
    using FIVE;
    using Newtonsoft.Json.Linq;
    using OpenCvSharp;
    using OpenCvSharp.Features2D;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using System.Text.RegularExpressions;
    using static FIVE.Generics;
    using static System.Runtime.InteropServices.JavaScript.JSType;
    using JsonIgnoreAttribute = System.Text.Json.Serialization.JsonIgnoreAttribute;
    using JsonSerializer = System.Text.Json.JsonSerializer;


        public static class YoloSerializationTools
        {
            public class MaskXYConverter : System.Text.Json.Serialization.JsonConverter<List<List<double[]>>>
            {
                //This handles the case when a List<List<double[]> only comes back with a single element.
                public override List<List<double[]>> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
                {
                    List<List<double[]>> result = new List<List<double[]>>();
                    List<double[]> doubles = new List<double[]>();
                    if (reader.TokenType == JsonTokenType.StartArray)
                    {
                        while (reader.Read() && reader.TokenType != JsonTokenType.EndArray)
                        {
                            if (reader.TokenType == JsonTokenType.StartArray)
                            {
                                try
                                {
                                    double[] nestedArray = JsonSerializer.Deserialize<double[]>(ref reader, options);

                                    if (nestedArray != null)
                                    {
                                        doubles.Add(nestedArray);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Trace.WriteLine($"Failed to deserialize as either List<double[]> or List<List<double[]>>: {ex.Message}");
                                }
                            }
                            else
                            {
                                Trace.WriteLine("Skipping unexpected JSON token while parsing maskXY.");
                            }
                        }
                        result.Add(doubles);
                    }
                    else
                    {
                        Trace.WriteLine("Expected StartArray but found something else.");
                    }

                    return result;
                }

                public override void Write(Utf8JsonWriter writer, List<List<double[]>> value, JsonSerializerOptions options)
                {
                    JsonSerializer.Serialize(writer, value, options);
                }

                private static T ParseToken<T>(JToken token)
                {
                    return token != null ? token.ToObject<T>() : default;
                }

            }

        // Dictionary to store the results

       
        public static Multimap<int, (int ClassId, string Name)> ParseDataWithStrings(List<string> data,Dictionary<string, int> Classes)
        {
            Multimap<int, (int ClassId, string Name)> result = new Multimap<int, (int ClassId, string Name)>();

            foreach (string line in data)
            {
                // Extract the index and content
                string[] parts = line.Split(": ", 2);
              
                if (parts.Length < 2) continue;

                int index = int.Parse(parts[0]);
                string[] content = parts[1].Split(' '); // Split by spaces
                string reducedContent = string.Join(" ", content[1..^1]).TrimEnd(','); // Skip the first and last elements

                // 2. Split the reduced content by commas
                string[] tokens = reducedContent.Split(',');

                // 3. Trim each token and convert to lowercase (if normalization is required)
                for (int i = 0; i < tokens.Length; i++)
                {
                    tokens[i] = tokens[i].Trim().ToLower();
                }


                foreach (string token in tokens)
                {
                    if (string.IsNullOrEmpty(token)) 
                        continue;
                    int count = 1;
                    string processedToken = token;

                    string[] partsToken = token.Split(' ', 2);
                    if (int.TryParse(partsToken[0], out int parsedCount))
                    {
                        count = parsedCount;
                        processedToken = partsToken.Length > 1 ? partsToken[1] : string.Empty;
                        if (count > 1)
                            processedToken = processedToken.TrimEnd('s');
                    }

                    // Directly check the processed token against the dictionary
                    if (Classes.TryGetValue(processedToken, out int classId))
                    {
                        for (int i = 0; i < count; i++) // Add entries based on the count
                        {
                            result.Add(index, (classId, processedToken));
                        }
                    }
                    else if (Classes.TryGetValue(processedToken + 's', out classId))
                    {
                        for (int i = 0; i < count; i++) // Add entries based on the plural form with 's'
                        {
                            result.Add(index, (classId, processedToken + 's'));
                        }
                    }
                    else if (Classes.TryGetValue(processedToken + "es", out classId))
                    {
                        for (int i = 0; i < count; i++) // Add entries based on the plural form with "es"
                        {
                            result.Add(index, (classId, processedToken + "es"));
                        }
                    }
                    else
                    {
                        // Attempt to trim common plural suffixes
                        string trimmedToken = processedToken.TrimEnd('s').TrimEnd('e');
                        if (Classes.TryGetValue(trimmedToken, out classId))
                        {
                            for (int i = 0; i < count; i++)
                            {
                                result.Add(index, (classId, trimmedToken));
                            }
                        }
                        else
                        {
                            // Perform fuzzy matching for high similarity
                            var bestMatch = Classes
                                .Select(kvp => new { Key = kvp.Key, Similarity = CompareStrings(processedToken, kvp.Key) })
                                .OrderByDescending(x => x.Similarity)
                                .FirstOrDefault(x => x.Similarity >= 0.95); // Threshold for a "good" match

                            if (bestMatch != null)
                            {
                                classId = Classes[bestMatch.Key];
                                for (int i = 0; i < count; i++)
                                {
                                    result.Add(index, (classId, bestMatch.Key)); // Use the best match
                                }
                            }
                            else
                            {
                                // Handle unmatched tokens (optional)
                                Trace.WriteLine($"Warning: Token '{processedToken}' could not be classified.");
                            }
                        }
                    }
                }
            }

            return result;
        }




            [JsonSerializable(typeof(YoloModelResult))] // This works with source generators
            public partial class YoloModelResult
            {
                // 3D array of coordinate points
                [JsonPropertyName("m_xy")]
                public List<List<double[]>> MaskXY { get; set; } = new();

                // Shape dimensions
                [JsonPropertyName("m_shape")]
                public List<int> MaskShape { get; set; } = new();

                // Bounding box Center in x,y,width,height format
                [JsonPropertyName("b_xywh")]
                public List<List<double>> BoxXyWH { get; set; } = new();

                // Bounding box Rectangle in x1,y1,x2,y2 format
                [JsonPropertyName("b_xyxy")]
                public List<List<double>> BoxXyXy { get; set; } = new();

                // Class labels
                [JsonPropertyName("b_cls")]
                public List<double> BoxClasses { get; set; } = new();

                // Detailed bounding box data 
                [JsonPropertyName("b_data")]
                public List<List<double>> BoxData { get; set; } = new();

                // Confidence scores
                [JsonPropertyName("b_conf")]
                public List<double> BoxConfScore { get; set; } = new();

                // Indicates if any predictions were made
                [JsonIgnore]
                public bool ContainsData => MaskXY != null && MaskXY.Count > 0;

                [JsonIgnore]
                public string FileName { get; set; }

                [JsonIgnore]
                public string ModelPath { get; set; }

                [JsonIgnore]
                public int ImageNumber { get; set; }
                //Area > 4000 = Neuron
                //Area < 2000 = Neurite
                //Area > 4000 = 
                [JsonIgnore]
                public string Annotation { get; set; }

                [JsonIgnore]
                public List<ImageAnalysisRecord> ImageAnalysisRecords = new List<ImageAnalysisRecord>();


            }
        public class ImageAnalysisRecord
        {
            // Properties
            public string ModelPath { get; set; }
            public string FilePath { get; set; }
            public int ImageNumber { get; set; }
            public string PlateId { get; set; }
            public string Annotation { get; set; }
            public int ClassId { get; set; }
            public string ClassName { get; set; }
            public double CenterX { get; set; }
            public double CenterY { get; set; }
            public double Width { get; set; }
            public double Height { get; set; }
            public double BoundingBoxArea { get; set; }
            public double? MaskArea { get; set; }
            public double? MajorAxisLength { get; set; }
            public double? MinorAxisLength { get; set; }
            public double? Intensity { get; set; }
            public double? NeuriteLength { get; set; }
            public double? OverlapScore { get; set; }
            public double Confidence { get; set; }
            public static (double MajorAxisLength, double MinorAxisLength) CalculateAxesFromBoundingBox(double x1, double y1, double x2, double y2)
            {
                // Calculate width and height
                double width = Math.Abs(x2 - x1);
                double height = Math.Abs(y2 - y1);

                // Determine major and minor axes
                double majorAxis = Math.Max(width, height);
                double minorAxis = Math.Min(width, height);

                return (majorAxis, minorAxis);
            }
            public static double CalculateMaskOutsideBoundingBoxPercentage(RectangleF boundingBox, List<double[]> maskCoordinates)
            {
                // Count total points
                int totalPoints = maskCoordinates.Count;

                // Count points outside the bounding box
                int outsidePoints = 0;
                foreach (var coordinate in maskCoordinates)
                {
                    double x = coordinate[0];
                    double y = coordinate[1];
                    if (!boundingBox.Contains((float)x, (float)y))
                    {
                        outsidePoints++;
                    }
                }

                // Calculate the percentage of points outside the bounding box
                return (double)outsidePoints / totalPoints * 100.0;
            }

            public static double CalculateNeuriteLength(RectangleF boundingBox, List<double[]> maskCoordinates)
            {
                // Step 1: Filter coordinates within the bounding box
                var filteredCoordinates = new List<ValueTuple<double, double>>();
                foreach (var coordinate in maskCoordinates)
                {
                    double x = coordinate[0];
                    double y = coordinate[1];
                    if (boundingBox.Contains((float)x, (float)y))
                    {
                        filteredCoordinates.Add((x, y));
                    }
                }

                // Step 2: Calculate the cumulative Euclidean distance
                double length = 0.0;
                for (int i = 1; i < filteredCoordinates.Count; i++)
                {
                    (double x1, double y1) = filteredCoordinates[i - 1];
                    (double x2, double y2) = filteredCoordinates[i];
                    length += Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                }

                return length;
            }
            public static double CalculatePolygonArea(List<double[]> coordinates)
            {
                int n = coordinates.Count;
                double area = 0;

                for (int i = 0; i < n; i++)
                {
                    double x1 = coordinates[i][0];
                    double y1 = coordinates[i][1];
                    double x2 = coordinates[(i + 1) % n][0]; // Wrap around for the last vertex
                    double y2 = coordinates[(i + 1) % n][1];

                    area += x1 * y2 - y1 * x2;
                }

                return Math.Abs(area *0.5);
            }

            public static string CalculateAnnotation(
                double maskArea, double width, double height, double boundingBoxArea)
            {
                double aspectRatio = width / height;
                double maskToBoxRatio = maskArea / boundingBoxArea;

                if (maskArea < 1500 && aspectRatio > 1.5)
                {
                    return "1 Neuron"; // Small mask area and elongated shape.
                }
                else if (maskArea >= 1500 && maskArea <= 2500 && maskToBoxRatio > 0.5)
                {
                    return "1 Cell Tub No Neurite"; // Intermediate mask area, compact.
                }
                else
                {
                    return "1 Neuron Very Short"; // Larger mask areas or irregular shapes.
                }
            }

            public string GetPlateId(string filePath)
            {
                // Extract the file name
                string fileName = Path.GetFileNameWithoutExtension(filePath);

                // Find the position of the first dash
                int dashIndex = fileName.IndexOf('-');

                // Extract the substring up to the dash
                if (dashIndex > 0)
                {
                    return fileName.Substring(0, dashIndex);
                }

                // Return a default or empty string if no dash is found
                return string.Empty;
            }
            // T
            public ImageAnalysisRecord(YoloModelResult result, string[] images, int imageNumber, int resultNumber, Dictionary<int, List<(int, string)>> classInformation)
            {
                try
                {
                    //This Takes the organized Class Information and YoloModelResult information to create a Row for a Database or CSV.
                    ModelPath = result.ModelPath;
                    FilePath = result.FileName;
                    ImageNumber = imageNumber;
                    PlateId = GetPlateId(images[imageNumber]).Trim();
                    ClassId = classInformation[imageNumber][resultNumber].Item1;
                    string className = classInformation[imageNumber][resultNumber].Item2;
                    if(Char.IsAsciiDigit(className[ClassName.Length-1]))
                    {
                        className = className[..^1];
                        //I don't think we would ever go into double digits but it doesn't hurt to check.
                        if (Char.IsAsciiDigit(className[ClassName.Length-1]))
                        {
                            className = className[..^1];
                        }
                    }
                
                    ClassName = className;
                    // Box dimensions
                    CenterX = result.BoxXyWH[resultNumber][0];
                    CenterY = result.BoxXyWH[resultNumber][1];
                    Width = result.BoxXyWH[resultNumber][2];
                    Height = result.BoxXyWH[resultNumber][3];

                    // Bounding box area and axes
                    double x1 = result.BoxXyXy[resultNumber][0];
                    double y1 = result.BoxXyXy[resultNumber][1];
                    double x2 = result.BoxXyXy[resultNumber][2];
                    double y2 = result.BoxXyXy[resultNumber][3];
                    BoundingBoxArea = (x2 - x1) * (y2 - y1);
                    PointF midPoint = new PointF((float)CenterX, (float)CenterY);
                    SizeF size = new SizeF((float)Width, (float)Height);
                    RectangleF BoundingRectangle = new RectangleF(midPoint, size);

                    (double MajorAxisLength, double MinorAxisLength) axes = CalculateAxesFromBoundingBox(x1, y1, x2, y2);
                    MajorAxisLength = axes.MajorAxisLength;
                    MinorAxisLength = axes.MinorAxisLength;

                    // Mask and annotation
                    List<double[]> maskCoordinates = result.MaskXY[resultNumber];
                    double polygonArea = CalculatePolygonArea(maskCoordinates),
                    MaskArea = Math.Round(polygonArea, MidpointRounding.AwayFromZero);
                    Annotation = CalculateAnnotation((double)MaskArea, Width, Height, BoundingBoxArea);
                    //Gets the Average Intensity of the coordinates.
                    Intensity = maskCoordinates.SelectMany(t => t).Average();

                    if (ClassName.ToLower().Contains("neurite"))
                    {
                        //Gets the Euclidean Distance of the Neurite.
                        NeuriteLength = CalculateNeuriteLength(BoundingRectangle, maskCoordinates); 
                    }
                    if (ClassName.ToLower().Contains("mito"))
                    {
                        //Percentage of Points in the Mask that Lie outside of the Bounding Box
                        OverlapScore = CalculateMaskOutsideBoundingBoxPercentage(BoundingRectangle, maskCoordinates);
                    }
                    Confidence = result.BoxConfScore[resultNumber];
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.ToString());
                }
            }

          
            public static string GetFieldNames(string delimiter = ",")
            {
                return string.Join(delimiter, typeof(ImageAnalysisRecord)
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Select(p => p.Name));
            }
            public override string ToString()
            {
                return $"{EscapeCsv(ModelPath)},{EscapeCsv(FilePath)},{ImageNumber},{EscapeCsv(PlateId)}," +
                       $"{EscapeCsv(Annotation)},{ClassId},{EscapeCsv(ClassName)},{CenterX}," +
                       $"{CenterY},{Width},{Height},{BoundingBoxArea},{EscapeNullableDouble(MaskArea)}," +
                       $"{EscapeNullableDouble(MajorAxisLength)},{EscapeNullableDouble(MinorAxisLength)},{EscapeNullableDouble(Intensity)},{EscapeNullableDouble(NeuriteLength)}," +
                       $"{EscapeNullableDouble(OverlapScore)},{Confidence}";
            }
            private string EscapeCsv(string field)
            {
                if (string.IsNullOrEmpty(field))
                    return "";

                if (field.Contains(",") || field.Contains("\"") || field.Contains("\n"))
                {
                    // Escape double quotes by replacing with double double-quotes
                    field = field.Replace("\"", "\"\"");
                    return $"\"{field}\""; // Wrap in double quotes
                }

                return field;
            }
            private string EscapeNullableDouble(double? value)
            {
                return value.HasValue ? value.Value.ToString("G", CultureInfo.InvariantCulture) : "null";
            }
        }
            public static List<ImageAnalysisRecord> GetImageAnalysisRecords(YoloModelResult[] yoloModelResults, Multimap<int, ValueTuple<int, string>> classInformation, string[] imgPaths)
            {
                List<ImageAnalysisRecord> records = new List<ImageAnalysisRecord>();
                Dictionary<int, List<(int, string)>> orderedClassInformation = new Dictionary<int, List<(int, string)>>();
                int verificationCount = 0;
                int noData = yoloModelResults.Count(t => t.ContainsData == false);
                verificationCount -= noData;
                foreach (KeyValuePair<int, ICollection<(int, string)>> kvp in classInformation)
                {
                    int imageNumber = kvp.Key;
                    ICollection<(int, string)> classes = kvp.Value;

                    // Increment verificationCount
                    verificationCount += classes.Count;

                    // Create an ordered list from the HashSet and add it to the dictionary
                    List<(int, string)> orderedClasses = classes.OrderBy(entry => entry.Item1).ToList();
                        orderedClassInformation[imageNumber] = orderedClasses;
                }
                int iterator = 0;
                string failedImages = null;
                foreach (YoloModelResult yolo in yoloModelResults)
                {
                    if (yolo.ContainsData)
                    {
                        for (int i = 0; i < yolo.BoxData.Count; i++)
                        {
                            ImageAnalysisRecord record = new ImageAnalysisRecord(yolo, imgPaths, iterator, i, orderedClassInformation);
                            records.Add(record);
                        }
                        yolo.ImageAnalysisRecords.AddRange(records);
                    }
                    else
                    {
                        failedImages += imgPaths[iterator];
                    }
                    iterator++;
                }
                string confirmationString = $"{imgPaths.Length} Images Containing {verificationCount} Records: Converting...... {records.Count} of {verificationCount} Succesfully Converted {noData} failure(s): {failedImages}";
                Trace.WriteLine(confirmationString);
                return records;
            }

            public static bool outputToCSV(IEnumerable<ImageAnalysisRecord> records, char delimiter = ',') 
            {
                try
                {
                    var sb = new StringBuilder();

                    // Define the headers manually, without the "ImageAnalysisRecord" prefix
                    string header = ImageAnalysisRecord.GetFieldNames();
                    sb.AppendLine(header);

                    // Append each record to the CSV
                    foreach (ImageAnalysisRecord row in records)
                    {
                        sb.AppendLine(row.ToString());
                    }

                    // Output to file
                    string csvContent = sb.ToString();
                    File.WriteAllText("E:\\Yolo11Stuff\\Neuron_1\\output.csv", csvContent);
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"Failed to output {ex.Message}   {ex.StackTrace}");
                    return false;
                }
            }
            public static void ProccessJson(string CLIoutput, string[] images, string ModelPath)
            {
                //Seperate out the JSON from the String, we need to proccess BOTH.
                int startIndex = CLIoutput.IndexOf('{');
                if (startIndex != -1)
                {
                    string JSON = CLIoutput[startIndex..].Trim();
          
                    try
                    {
                        //Proccess the Data in the Json
                        YoloModelResult[] results = GetYoloModelResults(JSON, images, ModelPath, out Dictionary<string, int> classes);
                        //Transform each YoloModel Result into it's constituent ImageAnalysis Records.
                        List<string> items = CLIoutput[..startIndex].Split(Environment.NewLine, StringSplitOptions.TrimEntries).Skip(1).Take(images.Length).ToList();
                        //Proccess the Data in the Strings
                        Multimap<int, ValueTuple<int, string>> OrganizedClasses = ParseDataWithStrings(items, classes);
                        List<ImageAnalysisRecord> records = GetImageAnalysisRecords(results, OrganizedClasses, images);
                        if(outputToCSV(records))
                        {
                            Trace.WriteLine("Success");
                        }

                }
                catch (Exception ex)
                    {
                        Trace.WriteLine($"Error parsing JSON: {ex.Message}");
                        throw;
                    }
                }
            }
            public static YoloModelResult[] GetYoloModelResults(string JSON, string[] images, string ModelPath, out Dictionary<string, int> Classes)
            {
                // Parse the JSON document

                using JsonDocument doc = JsonDocument.Parse(JSON);
                JsonElement root = doc.RootElement;
                JsonElement.ObjectEnumerator jsonProperties = root.EnumerateObject();

                int count = images.Length;
                YoloModelResult[] Results = new YoloModelResult[count];
                JsonSerializerOptions options = new JsonSerializerOptions
                {
                    Converters = { new YoloSerializationTools.MaskXYConverter() },
                    PropertyNameCaseInsensitive = true,
                    ReferenceHandler = ReferenceHandler.IgnoreCycles,
                    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                    Encoder = null,
                    AllowTrailingCommas = false,
                    NumberHandling = JsonNumberHandling.Strict,
                    PropertyNamingPolicy = null
                };

                Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();

                // Iterate through the array of objects in the root element
                int iterator = 0;
                foreach (JsonProperty element in jsonProperties)
                {
                    JsonElement ac = element.Value;
                // Class Mappings are different for ever Model, in order to get them dynamically I've serialized a custom dictionary.
                // The "Names" Property is a mapping of Class Name to Id's That exists at Level 1 of the Hierarchy.
                    if (keyValuePairs.Count == 0 && element.Name == "Names")
                    {
                        Dictionary<string, string> properties = ac.Deserialize<Dictionary<string, string>>();
                        foreach (KeyValuePair<string, string> property in properties)
                        {
                            string key = property.Key;
                            string value = property.Value;
                            if (int.TryParse(key, out int keyValue))
                            {
                                keyValuePairs.Add(value.ToLower().Trim(), keyValue);
                            }
                        }
                        continue;
                    }
                    YoloModelResult Result = new YoloModelResult();
                    try
                    {
                        try
                        {
                            Result = ac.Deserialize<YoloModelResult>();
                            Result.FileName = images[iterator];
                            Result.ImageNumber = iterator;
                            Result.ModelPath = ModelPath;
                        }
                        catch (Exception ex)
                        {
                            Result = ac.Deserialize<YoloModelResult>(options);
                            Result.FileName = images[iterator];
                            Result.ImageNumber = iterator;
                            Result.ModelPath = ModelPath;
                        }
                        if (Result.MaskXY.Count ==0)
                        {

                            Results[iterator++] = Result;
                        }
                        else
                        {
                            Results[iterator++] = Result;
                        }
                    }
                    catch (Exception ex)
                    {
                        Results[iterator++] = Result;
                    }

                }
                Classes = keyValuePairs;
                return Results;
            }
        }
    }
