using FIVE;
using FIVE.InCellLibrary;
using ImageMagick;
using ImageMagick.Formats;
using OpenCvSharp;
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using static Tensorflow.ApiDef.Types;

namespace FIVE_IMG
{

    public static class mIQueue
    {
        public static (MagickImage mI, Bitmap BM) GetMImage(string FullPath, ushort blacklev = 20, ushort whitelev = 2600)
        {
            string key = FullPath + " " + blacklev + " " + whitelev;
            if (!_Queue.ContainsKey(key))
            {
                var mIPre = new MagickImage(FullPath);
                var mI = new MagickImage(mIPre);
                mI.Level(blacklev, whitelev);
                mI.AutoLevel();
                var bmap = mI.ToBitmap(); // ImageFormat.MemoryBmp);
                _Queue.Add(key, (mIPre, bmap));
            }
            else
            {
                //Just means we have already loaded it, and it is in the overlap . . but perhaps some part is disposed?
            }
            return _Queue[key];
        }

        private static Dictionary<string, (MagickImage mI, Bitmap BM)> _Queue = new();

        public static List<MagickImage> GetMImageSet(XDCE_Image xI, bool LoadIntoQueue = true)
        {
            var Imgs = xI.Parent.GetImages_FromWellFOV(xI.WellField);
            var ret = new List<MagickImage>(Imgs.Count);
            foreach (var Img in Imgs)
                ret.Add(new MagickImage(Img.FullPath));
            return ret;
        }

        public static MagickGeometry GeoFromRect(Rectangle Rect)
        {
            return new MagickGeometry(Rect.X, Rect.Y, Rect.Width, Rect.Height);
        }

        public static List<MagickImage> CropSet(List<MagickImage> ToCrop, RectangleF CropRect)
        {
            var geo = GeoFromRect(Rectangle.Round(CropRect));
            var ret = new List<MagickImage>(ToCrop.Count);
            foreach (var Img in ToCrop)
            {
                var mI = (MagickImage)Img.Clone(geo);
                ret.Add(mI);
            }
            return ret;
        }

        internal static void Clear()
        {
            foreach (var KVP in _Queue)
            {
                KVP.Value.mI.Dispose();
                KVP.Value.BM.Dispose();
            }
        }
    }
    public struct ImgQueueKey
    {
        public string PlateStr = "";
        private string _NameStr = "";
        public string NameStr
        {
            get => _NameStr; set
            {
                _NameStr = value;
          
                Match match = ImageQueue.regex.Match(_NameStr);

                if (match.Success)
                {
                    Well = match.Groups["Well"].Value;
                    FOV = match.Groups["Field"].Value;
                }
            }
        }

        public override string ToString()
        {
            return NameStr;
        }

        public string WVPstr = "";

        public string Well = "";
        public string FOV = "";

        public ImgQueueKey()
        {

        }

        internal static ImgQueueKey From(string Plate, string Name, wvDisplayParams Params)
        {
            return new ImgQueueKey() { PlateStr = Plate, NameStr = Name, WVPstr = Params.ToString() };
        }

        internal static ImgQueueKey From(XDCE_Image xI, wvDisplayParams Params)
        {
            return new ImgQueueKey() { PlateStr = xI.Parent.PlateID, NameStr = xI.FileName, WVPstr = Params.ToString() };
        }
    }

    public class ImageQueue : IEnumerable<Bitmap>
    {
        public static readonly Regex regex = new Regex(@"(?<Well>[A-Za-z0-9 -]+)\(FLD\s*(?<Field>\d+)", RegexOptions.Compiled | RegexOptions.NonBacktracking);

        private Dictionary<ImgQueueKey, Bitmap> _Dict;

        private Queue<ImgQueueKey> _Queue;

        private int _Capacity;
        public int Capacity
        {
            get => _Capacity;
            set
            {
                _Capacity = value;
            }
        }

        public ImageQueue()
        {
            _Capacity = GetCapacity();
            Init(_Capacity);
        }

        public static int GetCapacity()
        {
            // Get memory info from the garbage collector
            GCMemoryInfo memoryInfo = GC.GetGCMemoryInfo();

            // Total usable memory after accounting for fragmentation
            long usableMemory = memoryInfo.TotalAvailableMemoryBytes - memoryInfo.FragmentedBytes;

            // Set the size of each image in bytes
            int imageSizeInMegaBytes = 16384 * 1000;  // Adjust this based on your actual image size

            // Define the portion of memory to allocate for images (e.g., 27.5%)
            float divisor = 0.275f; // 27.5% of usable memory for images

            // Calculate the maximum number of images that can fit in the usable memory
            int maxImagesInQueue = (int)((usableMemory * divisor) / imageSizeInMegaBytes);

            // Return the result
            return maxImagesInQueue;
        }

        private int Count { get => _Dict.Count; }

        /// <summary>
        /// Name_From_XDCE(Img), wvDisplayParams
        /// </summary>
        public Dictionary<ImgQueueKey, Bitmap> Dict { get => _Dict; }
        public Queue<ImgQueueKey> Queue { get => _Queue; }
        public ConcurrentDictionary<ImgQueueKey, List<string>> LoadStatusHistory;

        public static bool WriteLog = false;
        public static string WriteLogLocation = @"c:\temp\ImgQueueLog.txt";

        public enum ImgQueueLoadStatus { Loading, Loaded, Removed }
        private ConcurrentDictionary<ImgQueueKey, ImgQueueLoadStatus> LoadStatus;

        private void Init(int Capacity)
        {
         
            _Capacity = Capacity;
            _Dict = new();
            _Queue = new();
            LoadStatus = new();
            LoadStatusHistory = new();

            if (WriteLog) Directory.CreateDirectory(Path.GetDirectoryName(WriteLogLocation));
           
        }
        private void DoWriteLog(string Msg)
        {
            if (WriteLog) File.AppendAllText(WriteLogLocation, DateTime.Now.ToString() + "\t" + Msg);
        }

        /// <summary>
        /// Adds a new entry, and removes and older one
        /// </summary>
        public void Add(ImgQueueKey Key, Bitmap Image)
        {
         
            if (_Queue.Count >= Capacity)
            {
                var DeQueue = _Queue.Dequeue();
                //if (DeQueue == null) return;
                Remove(DeQueue);
            }

            //if (LoadStatus.ContainsKey(Key)) return; //This is already set to loading, so don't use it
            //Because we have some parallelization going on, double check this . . 
            if (_Dict.ContainsKey(Key)) return;

            _Queue.Enqueue(Key);
            _Dict.Add(Key, Image);
        }

        public void Add(XDCE_Image Img, wvDisplayParams Params, System.Drawing.Bitmap BMP)
        {
            Add(ImgQueueKey.From(Img, Params), BMP);
        }

        public void Remove(XDCE_Image Img, wvDisplayParams Params)
        {
            var DeQueue = ImgQueueKey.From(Img, Params);
            Remove(DeQueue);
        }
      
        public void Remove(ImgQueueKey DeQueue)
        {
            //RepHistory(DeQueue, "Ready Remove 1");
            if (!_Dict.ContainsKey(DeQueue))
            { //This means we haven't gotten to use it yet, we don't want to unload it and load it again
                //RepHistory(DeQueue, "Ready Remove 2");
                if (_Queue.Count < Capacity + 10)
                {
                 //   RepHistory(DeQueue, "Keep instead of remove");
                    _Queue.Enqueue(DeQueue);
                    return; //Since we already took it out, we have to put it back in
                }
                else
                {
                    //Queue is too full, we will run out of memory, so we have to let it get removed
                }
            }
            //RepHistory(DeQueue, "Ready Remove 4");
            RemoveElement(DeQueue); //Won't do anything if it was already removed
            Bitmap ToDispose = _Dict[DeQueue];
            _Dict.Remove(DeQueue);
            LoadStatus[DeQueue] = ImgQueueLoadStatus.Removed;
            ToDispose.Dispose();
            //RepHistory(DeQueue, "Ready Remove 5");
            if (WriteLog)
                DoWriteLog(DeQueue.PlateStr + "\t" + DeQueue.Well + "\t" + DeQueue.FOV + "\t" + "Rem" + "\t" + _Dict.Count + "\r\n");
        }

        public void RemoveElement(ImgQueueKey target)
        {
            if (_Queue.Count == 0) return;
            if (!_Queue.Contains(target)) return;
            if (_Queue.Peek().Equals(target)) _Queue.Dequeue();
            else
            {
                Queue<ImgQueueKey> tempQueue = new Queue<ImgQueueKey>(_Queue.Count - 1);
                while (_Queue.Count > 0)
                {
                    ImgQueueKey current = _Queue.Dequeue();
                    if (!current.Equals(target)) tempQueue.Enqueue(current);
                }
                _Queue = tempQueue;
            }
        }

        public static bool RecordHistory = false;
        private void RepHistory(ImgQueueKey Key, string Msg)
        {
            if (!RecordHistory) return;
            if (!LoadStatusHistory.ContainsKey(Key)) LoadStatusHistory[Key] = new List<string>();
            LoadStatusHistory[Key].Add(Msg);
        }

        public bool ContainsKey(XDCE_Image Img, wvDisplayParams Params)
        {
            return ContainsKey(ImgQueueKey.From(Img, Params));
        }

        public bool ContainsKey(ImgQueueKey Key)
        {
            return _Dict.ContainsKey(Key);
        }

        public System.Drawing.Bitmap Get_Bitmap(XDCE_Image xI, wvDisplayParams Params)
        {
            var Key = ImgQueueKey.From(xI, Params);
            return _Dict[Key];
        }

        public (Bitmap bmp, bool WasAlreadyAvail) CombinedBMAP(XDCE_ImageGroup Welli, XDCE_Image xI, wvDisplayParams wvPs, bool BackgroundProcess = false)
        {
            var Key = ImgQueueKey.From(xI, wvPs);
            //RepHistory(Key, "Requested");

            Bitmap bmap;
            if (LoadStatus.TryGetValue(Key, out ImgQueueLoadStatus Status))
            {
                if (Status == ImgQueueLoadStatus.Removed)
                {
                    //RepHistory(Key, "Back Return");
                    if (BackgroundProcess) 
                        return (null, true);
                    //Otherwise we still need to get it, must have been removed by the wrong process
                    //RepHistory(Key, "Back Pass");
                }
                if (Status == ImgQueueLoadStatus.Loading)
                {
                    //RepHistory(Key, "Loading");
                    if (BackgroundProcess) 
                        return (null, false); //Since it is for the background process, we don't need to wait here
                    //RepHistory(Key, "Loading Pass");
                    //Debug.Print("waiting on loading. . " + Key.NameStr.ToString());
                    int iterations = 10;
                    while (Status == ImgQueueLoadStatus.Loading)
                    {
                        if(LoadStatus.TryGetValue(Key, out Status) && Status != ImgQueueLoadStatus.Loaded)
                        {
                            Thread.Sleep(25); //Loading
                            iterations--;
                        }
                        if(iterations ==0)
                        {
                            break;
                        }
                    }
                }
                if (this.ContainsKey(xI, wvPs))
                {
                    //RepHistory(Key, "Ready Get");
                    bmap = this.Get_Bitmap(xI, wvPs);

                    for (int attempts = 0; attempts < 3; attempts++)
                    {
                        if (LoadStatus.TryGetValue(Key, out ImgQueueLoadStatus IsStatus)) 
                            if (IsStatus == ImgQueueLoadStatus.Removed) return (null, true); //Could have just gotten removed
                        try
                        { 
                            if (bmap.Width > 0) 
                                return (bmap, true); }//It may be disposed or not ready, this will trigger the catch to try again
                        catch
                        {   //First time, just try it again
                            RepHistory(Key, "Trying again . . "); Debug.Print("Going to try to get an image again . . "); 
                            Thread.Sleep(25);
                        }
                    }
                    this.Remove(xI, wvPs);
                    //Just let this fall through so it gets regenerated
                }
            }

             //RepHistory(Key, "Ready Load 1");
            LoadStatus[Key] = ImgQueueLoadStatus.Loading;
            if(WriteLog)
                DoWriteLog(Key.PlateStr + "\t" + Key.Well + "\t" + Key.FOV + "\t" + (BackgroundProcess ? "Bkg" : "Add") + "\t" + _Dict.Count + "\r\n");

            //Adjusted the bit below on 1/5/2024 - otherwise it was loading the wrong well
           
            if (Welli.NameAtLevel != xI.WellLabel)
                bmap = Utilities.DrawingUtils.CombinedBMAPst(xI, wvPs, null, 1, wvPs.ColorCalcTypeStr);
            else
                bmap = Utilities.DrawingUtils.CombinedBMAPst(xI, wvPs, Welli, 1, wvPs.ColorCalcTypeStr);

            this.Add(xI, wvPs, bmap);
            LoadStatus[Key] = ImgQueueLoadStatus.Loaded;
            //RepHistory(Key, "Ready Load 2");
            return (bmap, false);
            
        }

        public IEnumerator<Bitmap> GetEnumerator()
        {
            return _Dict.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Dict.Values.GetEnumerator();
        }


        /// <summary>
        /// Within RaftCheck this is managed with a different background process, but here we have a background process that will try and load everything
        /// </summary>
        public void PreLoad(XDCE_ImageGroup well, wvDisplayParams wvPs)
        {
            var BW = new BackgroundWorker();
            BW.DoWork += BW_DoWork;

            var Args = (Wells: new List<XDCE_ImageGroup>() { well }, WVPs: wvPs);

            BW.RunWorkerAsync(Args);
        }

        public void PreLoad(List<INCELL_Folder> folders, wvDisplayParams wvPs)
        {
            var BW = new BackgroundWorker();
            BW.DoWork += BW_DoWork;

            var wells = folders.SelectMany(x => x.XDCE.Wells.Values).ToList();
            var Args = (Wells: wells, WVPs: wvPs);

            BW.RunWorkerAsync(Args);
        }

        private void BW_DoWork(object sender, DoWorkEventArgs e)
        {
            var Args = e.Argument as (List<XDCE_ImageGroup> Wells, wvDisplayParams WVPs)?;
            bool First = true;
            foreach (var well in Args.Value.Wells)
            {
                Debug.Print("IMG Queue Background " + well.NameAtLevel + " " + _Queue.Count + "/" + Capacity + " Dict = " + _Dict.Count);
                BW_Work_Well(well, Args.Value.WVPs, First); First = false;
            }

        }

        private void BW_Work_Well(XDCE_ImageGroup well, wvDisplayParams WVPs, bool First = false)
        {
            List<int> orderedFOVs;

            /*Individuals
           
                //var percentiles = new List<double>() { 0, 0.25, 0.3, 0.35, 0.4, 0.45 }; //Load Pattern A
                //var percentiles = new List<double>() { 0, 0.9, 0.12, 0.15, 0.18, 0.21 }; //Load Pattern B
                //var percentiles = new List<double>() { 0, 0.09, 0.10, 0.12, 0.13, 0.15, 0.16, 0.18, 0.19, 0.21, 0.22, 0.24, 0.25 }; //Load Pattern C
                var percentiles = new List<double>() { 0, 0.04, 0.05, 0.07, 0.08, 0.09, 0.10, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.20, 0.21, 0.22, 0.23, 0.24, 0.25, 0.4 }; //Load Pattern D
                var indices = percentiles.Select(p => well.FOV_Min + (int)Math.Floor((well.FOV_Max - well.FOV_Min) * p)).ToList();
                orderedFOVs = indices;
                orderedFOVs.AddRange(Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).Except(indices).Reverse());
            
             Percentile Ranges
           
                //var percentileRanges = new List<(double start, double end)> { (0, 0.014), (0.1, 0.07), (0.16, 0.11), (0.25, 0.2), (0.35, 0.3), (0.45, 0.4), (0.55, 0.5), (0.65, 0.6), (0.75, 0.7) }; //Pattern E
                var percentileRanges = new List<(double start, double end)> { (0, 0.014), (0.15, 0.1), (0.25, 0.2), (0.35, 0.3), (0.45, 0.4), (0.55, 0.5), (0.65, 0.6), (0.75, 0.7) }; //G
                //orderedFOVs = GenerateFOVArray(well.FOV_Min, well.FOV_Max, percentileRanges);
                //var percentileRanges = new List<(double start, double end)> { (0, 0.014) }; //H
                orderedFOVs = GenerateFOVArray(well.FOV_Min, well.FOV_Max, percentileRanges, true);
           */
            //Intervals
          
            if (well.FOV_Max > 100)
            {
                if (First)
                {
                    var percentileRanges = new List<(double start, double end)> { (0, 0.014), (0.15, 0.1), (0.25, 0.2), (0.35, 0.3), (0.45, 0.4), (0.55, 0.5), (0.65, 0.6), (0.75, 0.7) }; //G
                    orderedFOVs = GenerateFOVArray(well.FOV_Min, well.FOV_Max, percentileRanges, false);
                }
                else
                {
                    int interval = 3;
                    orderedFOVs = Enumerable.Range(well.FOV_Min, 4).ToList();
                    var indices = Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).Where((_, index) => index % interval == 0).ToList();
                    orderedFOVs.AddRange(indices.Except(orderedFOVs));
                    orderedFOVs.AddRange(Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).Reverse().Except(orderedFOVs));
                }
            }
            else
            {
                orderedFOVs = Enumerable.Range(well.FOV_Min, Math.Min(well.FOV_Max, 3)).ToList();
                orderedFOVs.AddRange(Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).Reverse().Except(orderedFOVs));
            }
           
            //orderedFOVs = Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).ToList(); //Pattern F

            foreach (var FOV in orderedFOVs)
            {
                List<XDCE_Image> xI = well.GetFields(FOV);
                CombinedBMAP(well, xI[0], WVPs, true);
                Thread.SpinWait(3);
            }
        }

        public static Random _Rand = new Random();

        static List<int> GenerateFOVArray(int FOV_Min, int FOV_Max, List<(double start, double end)> percentileRanges, bool RandomizeRemaining = false)
        {
            var orderedFOVs = new List<int>();

            foreach (var (start, end) in percentileRanges)
            {
                int startIndex = FOV_Min + (int)Math.Floor((FOV_Max - FOV_Min) * start);
                int endIndex = FOV_Min + (int)Math.Floor((FOV_Max - FOV_Min) * end);

                if (startIndex < endIndex)
                    orderedFOVs.AddRange(Enumerable.Range(startIndex, endIndex - startIndex + 1));
                else
                    orderedFOVs.AddRange(Enumerable.Range(endIndex, startIndex - endIndex + 1).Reverse());
            }
            //Fill in the rest
            List<int> orderedFOVs_Full = orderedFOVs;
            if (RandomizeRemaining)
                orderedFOVs.AddRange(Enumerable.Range(FOV_Min, FOV_Max - FOV_Min + 1).Except(orderedFOVs).OrderBy(_Rand.Next));
            else
                orderedFOVs.AddRange(Enumerable.Range(FOV_Min, FOV_Max - FOV_Min + 1).Except(orderedFOVs).Reverse());
            return orderedFOVs_Full;
        }


    }

    public class LeicaImageQueue : IEnumerable<Bitmap>
    {
        private SortedList<ImgQueueKey, Bitmap> _ImageQue;

        private List<string> _wellList = null;
        public List<string> wellList
        {
            get => _wellList;
            set => _wellList = value;
        }

        private int _Capacity;
        public int Capacity
        {
            get => _Capacity;
            set
            {
                _Capacity = value;
            }
        }

        public LeicaImageQueue(int Capacity = 7 * 36)
        {
            Init(Capacity);
        }

        private int Count { get => _ImageQue.Count; }

        /// <summary>
        /// Name_From_XDCE(Img), wvDisplayParams
        /// </summary>
        //cleaned up by Josh 4/1/2023
        public SortedList<ImgQueueKey, Bitmap> Dict { get => _ImageQue; }
        public ConcurrentDictionary<ImgQueueKey, List<string>> LoadStatusHistory;
    
        public static bool WriteLog = false;
        public enum ImgQueueLoadStatus { Loading, Loaded, Removed }
        private ConcurrentDictionary<ImgQueueKey, ImgQueueLoadStatus> LoadStatus;

        //cleaned up by Josh 4/1/2023
        private void Init(int Capacity)
        {
            _Capacity = Capacity;
            //Initialize The Que with the Capacity;
            _ImageQue = new SortedList<ImgQueueKey, Bitmap>(Capacity);
            LoadStatus = new ConcurrentDictionary<ImgQueueKey, ImgQueueLoadStatus>();
            LoadStatusHistory = new ConcurrentDictionary<ImgQueueKey, List<string>>();
        }

        /// <summary>
        /// Adds a new entry, and removes and older one
        /// </summary>
        //Cleaned up by Josh 4/1/2023
        public void Add(ImgQueueKey Key, Bitmap Image)
        {
            if(_ImageQue.Count >= Capacity)
            {
                //Grab the oldest key and remove it.
                ImgQueueKey ToRemove = _ImageQue.Keys[0];
                Remove(ToRemove);
            }

            //if (LoadStatus.ContainsKey(Key)) return; //This is already set to loading, so don't use it
            //Because we have some parallelization going on, double check this . . 
            if (_ImageQue.ContainsKey(Key))
                Remove(Key);
            //Clone the image so that disposing of it elsewhere doesn't dispose of it in the Que.
            Bitmap ClonedImage = Image.Clone() as Bitmap;
            _ImageQue.Add(Key, ClonedImage);
        }

        public void Add(XDCE_Image Img, wvDisplayParams Params, System.Drawing.Bitmap BMP)
        {
            Add(ImgQueueKey.From(Img, Params), BMP);
        }

        public void Remove(XDCE_Image Img, wvDisplayParams Params)
        {
            var DeQueue = ImgQueueKey.From(Img, Params);
            Remove(DeQueue);
        }
        //Cleaned up by Josh 4/1/2023
        public void Remove(ImgQueueKey DeQueue)
        {
            RepHistory(DeQueue, "Ready Remove 1");
            //WriteLog = true;
            if (!_ImageQue.ContainsKey(DeQueue))
            { //This means we haven't gotten to use it yet, we don't want to unload it and load it again
                RepHistory(DeQueue, "Already Removed");
                LoadStatus[DeQueue] = ImgQueueLoadStatus.Removed;
                return; //Since we already took it out, we have to put it back in
            }
            else
            {
                RepHistory(DeQueue, "Ready Remove 4");
                try
                {
                    _ImageQue[DeQueue].Dispose();
                }
                catch (Exception ex)
                {
                    RepHistory(DeQueue, "Dispose Error: " + ex.Message);
                }
                _ImageQue.Remove(DeQueue);
                LoadStatus[DeQueue] = ImgQueueLoadStatus.Removed;

                RepHistory(DeQueue, "Ready Remove 5");
                if (WriteLog) File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + DeQueue.PlateStr + "\t" + DeQueue.Well + "\t" + DeQueue.FOV + "\t" + "Rem" + "\t" + _ImageQue.Count + "\r\n");
            }
        }

        public void RemoveElement(ImgQueueKey target)
        {
            _ImageQue.Remove(target);
        }

        public static bool RecordHistory = false;
        private void RepHistory(ImgQueueKey Key, string Msg)
        {
            if (!RecordHistory) return;
            if (!LoadStatusHistory.ContainsKey(Key)) LoadStatusHistory[Key] = new List<string>();
            LoadStatusHistory[Key].Add(Msg);
        }

        public bool ContainsKey(XDCE_Image Img, wvDisplayParams Params)
        {
            return ContainsKey(ImgQueueKey.From(Img, Params));
        }

        public bool ContainsKey(ImgQueueKey Key)
        {
            return _ImageQue.ContainsKey(Key);
        }

        public System.Drawing.Bitmap Get_Bitmap(XDCE_Image xI, wvDisplayParams Params)
        {
            ImgQueueKey Key = ImgQueueKey.From(xI, Params);
            return _ImageQue[Key];
        }

        public (Bitmap bmp, bool WasAlreadyAvail) CombinedBMAP(XDCE_ImageGroup Welli, XDCE_Image xI, wvDisplayParams wvPs, bool BackgroundProcess = false)
        {
            var Key = ImgQueueKey.From(xI, wvPs);
            RepHistory(Key, "Requested");

            Bitmap bmap;
            if (LoadStatus.ContainsKey(Key))
            {
                if (LoadStatus[Key] == ImgQueueLoadStatus.Removed)
                {
                    RepHistory(Key, "Back Return");
                    if (BackgroundProcess) return (null, true);
                    //Otherwise we still need to get it, must have been removed by the wrong process
                    RepHistory(Key, "Back Pass");
                }
                if (LoadStatus[Key] == ImgQueueLoadStatus.Loading)
                {
                    RepHistory(Key, "Loading");
                    if (BackgroundProcess) return (null, false); //Since it is for the background process, we don't need to wait here
                    RepHistory(Key, "Loading Pass");
                    Debug.Print("waiting on loading. . " + Key.NameStr.ToString());
                    while (LoadStatus[Key] == ImgQueueLoadStatus.Loading) Thread.Sleep(25); //Loading
                    Thread.Sleep(350); //Wait a little longer to give the bckg process time to catch up
                }
                if (this.ContainsKey(xI, wvPs))
                {
                    RepHistory(Key, "Ready Get");
                    bmap = this.Get_Bitmap(xI, wvPs);

                    for (int attempts = 0; attempts < 4; attempts++)
                    {
                        if (LoadStatus.ContainsKey(Key)) if (LoadStatus[Key] == ImgQueueLoadStatus.Removed) return (null, true); //Could have just gotten removed
                        try
                        {
                            if (bmap.Width > 0)
                                return (bmap, true);
                        }//It may be disposed or not ready, this will trigger the catch to try again
                        catch
                        {   //First time, just try it again
                            RepHistory(Key, "Trying again . . "); Debug.Print("Going to try to get an image again . . "); Thread.Sleep(25);
                        }
                    }
                    this.Remove(xI, wvPs);
                    //Just let this fall through so it gets regenerated
                }
            }

            RepHistory(Key, "Ready Load 1");
            LoadStatus[Key] = ImgQueueLoadStatus.Loading;
            if (WriteLog) File.AppendAllText(@"c:\temp\ImgQueueLog.txt", DateTime.Now.ToString() + "\t" + Key.PlateStr + "\t" + Key.Well + "\t" + Key.FOV + "\t" + (BackgroundProcess ? "Bkg" : "Add") + "\t" + _ImageQue.Count + "\r\n");

            //Adjusted the bit below on 1/5/2024 - otherwise it was loading the wrong well
            if (InCell_To_Leica.SR.MultiObjectiveStyle == MultiObjEnum.x20_x5_LiveTile)
            {
                bmap = GetBMPForLeica(xI);
            }
            else if (Welli.NameAtLevel != xI.WellLabel)
            {
                bmap = Utilities.DrawingUtils.CombinedBMAPst(xI, wvPs, null, 1, wvPs.ColorCalcTypeStr);
            }
            else
            {
                bmap = Utilities.DrawingUtils.CombinedBMAPst(xI, wvPs, Welli, 1, wvPs.ColorCalcTypeStr);
            }
            this.Add(xI, wvPs, bmap);
            LoadStatus[Key] = ImgQueueLoadStatus.Loaded;
            RepHistory(Key, "Ready Load 2");
            return (bmap, false);

        }

        public Bitmap GetBMPForLeica(XDCE_Image xI)
        {
        
            Rectangle R_GEFract_Pix = new Rectangle(0, 0, xI.Width_Pixels, xI.Height_Pixels);
            float LE_umPerPixel = InCell_To_Leica.SR.pixelBinning;
            float Brightness = InCell_To_Leica.SR.Brightness;
            Bitmap GEFract_BMP;
            Bitmap BMP = Utilities.DrawingUtils.GetAdjustedImageSt(xI, Brightness);
            float Leica_Pixel_Binning = LE_umPerPixel / (float)xI.Parent.PixelWidth_in_um;

            try
            {
                GEFract_BMP = BMP.Clone(R_GEFract_Pix, BMP.PixelFormat);

            }

            catch { return null; }
            var Sz = new System.Drawing.Size(
                (int)(InCell_To_Leica.SR.AIL.GE_LeicaSizeRatio_ForImage.Width * GEFract_BMP.Width / (InCell_To_Leica.SR.AIL.resize_divisor_forRegistration * Leica_Pixel_Binning)),
                (int)(InCell_To_Leica.SR.AIL.GE_LeicaSizeRatio_ForImage.Height * GEFract_BMP.Height / (InCell_To_Leica.SR.AIL.resize_divisor_forRegistration * Leica_Pixel_Binning)));
            GEFract_BMP = new Bitmap(GEFract_BMP, Sz);
            GEFract_BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);
            BMP.Dispose();
            return GEFract_BMP;
        }


        public IEnumerator<Bitmap> GetEnumerator()
        {
            return _ImageQue.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _ImageQue.Values.GetEnumerator();
        }


        /// <summary>
        /// Within RaftCheck this is managed with a different background process, but here we have a background process that will try and load everything
        /// </summary>
        public void PreLoad(XDCE_ImageGroup well, wvDisplayParams wvPs)
        {
            FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"preloading well {well.PlateID}");
            var BW = new BackgroundWorker();
            BW.DoWork += BW_DoWork;

            var Args = (Wells: new List<XDCE_ImageGroup>() { well }, WVPs: wvPs);

            BW.RunWorkerAsync(Args);
        }

        public void PreLoad(List<INCELL_Folder> folders, wvDisplayParams wvPs)
        {
            var BW = new BackgroundWorker();
            BW.DoWork += BW_DoWork;

            var wells = folders.SelectMany(x => x.XDCE.Wells.Values).ToList();
            var Args = (Wells: wells, WVPs: wvPs);

            BW.RunWorkerAsync(Args);
        }

        private void BW_DoWork(object sender, DoWorkEventArgs e)
        {
            if (_wellList == null)
                fixWelllist();
            var Args = e.Argument as (List<XDCE_ImageGroup> Wells, wvDisplayParams WVPs)?;
            bool First = true;
            foreach (var well in Args.Value.Wells)
            {
                Debug.Print("IMG Queue  " + well.NameAtLevel + " " + _ImageQue.Count + "/" + Capacity + " SortedList = " + _ImageQue.Count);
                BW_Work_Well(well, Args.Value.WVPs, First); First = false;
            }

        }
        //cleaned up by Josh 4/1/2023
        private void BW_Work_Well(XDCE_ImageGroup well, wvDisplayParams WVPs, bool First = false)
        {
            int initWindowSize = _Capacity / 2 + 1;
            // need to keep our imageList centered at currentPos. each time we load in new images, on avg, should add one img worth of images. #20x FOVs / #5x FOVs
            int windowShift = well.FOV_Max / (InCell_To_Leica.SR.FOVCount / InCell_To_Leica.SR.CurrentWellXDCE.Wavelength_Count);
            List<int> orderedFOVs = new List<int> { };
            int initPosition = InCell_To_Leica.SR.maxFOVsaved;
            int numImagesToSave = well.FOV_Max - initPosition;
            int numOpenPositions = Capacity - Count;

            if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"initWindowSize: {initWindowSize}\r\nwindowShift: {windowShift}\r\ninitPosition: {initPosition}\r\nnumImagesToSave: {numImagesToSave}\r\nnumOpenPositions: {numOpenPositions}");
            if (numImagesToSave <= numOpenPositions) // We can save all of them
            {
                if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", "we can just fit all of them");
                orderedFOVs.AddRange(Enumerable.Range(initPosition + 1, well.FOV_Max - initPosition));
            }
            else if (numOpenPositions > initWindowSize) // Can't save all, but we have plenty of room. Fill it halfway
            {
                if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"Can't fit em all, but enough room to half-fill it. Probably an initial load");
                if (well.FOV_Max - initPosition <= initWindowSize)
                {
                    if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"Not that many images left."); 
                    // fewer FOVs than our initWindow. Just fill up with FOV count
                    orderedFOVs.AddRange(Enumerable.Range(initPosition + 1, well.FOV_Max - initPosition));
                    InCell_To_Leica.SR.maxFOVsaved = well.FOV_Max;
                }
                else
                {
                    if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", "Half-fill it.");
                    // more FOVs than window size. fill up to window size
                    orderedFOVs.AddRange(Enumerable.Range(initPosition + 1, initWindowSize));
                    InCell_To_Leica.SR.maxFOVsaved = initPosition + initWindowSize;
                }
            }
            else
            {
                if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"Can't fill them all, and can't half fill it. Expand/shift the window");
                if (well.FOV_Max - initPosition <= windowShift)
                {
                    if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"only a few left");
                    // fewer FOVs remaining than shift size. add the remaining imgs
                    orderedFOVs.AddRange(Enumerable.Range(initPosition + 1, well.FOV_Max - initPosition));
                    InCell_To_Leica.SR.maxFOVsaved = well.FOV_Max;
                }
                else
                {
                    if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"plenty of images left. regular shift");
                    // add 16 imgs (removes automatically)
                    orderedFOVs.AddRange(Enumerable.Range(initPosition + 1, windowShift));
                    InCell_To_Leica.SR.maxFOVsaved = initPosition + windowShift;
                }
            }

            if (InCell_To_Leica.SR.maxFOVsaved >= well.FOV_Max)
            {
                if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"We're saving up to the last FOV in the well\r\nInCell_To_Leica.SR.maxFOVsaved: {InCell_To_Leica.SR.maxFOVsaved} >= well.FOV_Max: {well.FOV_Max}");
                // we're at the end of this well.
                wellList.Remove(well.Images.First().WellLabel);
                InCell_To_Leica.SR.maxFOVsaved = -1; // Ready to start for next well
            }
            FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"lowest FOV we're saving: {orderedFOVs.Min()}\r\nhighest FOV we're saving: {orderedFOVs.Max()}");
            foreach (var FOV in orderedFOVs)
            {
                if (WriteLog) FileHandler.SafelyWrite("C:\\temp\\AILBase\\INI\\ImgQueueLog.txt", $"FOV: {FOV}");
                var xI = well.GetFields(FOV);
                CombinedBMAP(well, xI[0], WVPs, true);
                Thread.SpinWait(3);
            }




            /*int FOV_Min = well.FOV_Min;
            int FOV_Max = well.FOV_Max;

            IEnumerable<int> rangeFov = Enumerable.Range(FOV_Min, FOV_Max - FOV_Min + 1);
            if (well.FOV_Max > 100)
            {
                if (First)
                {
                    List<(double start, double end)> percentileRanges = new List<(double start, double end)> { (0, 0.014), (0.15, 0.1), (0.25, 0.2), (0.35, 0.3), (0.45, 0.4), (0.55, 0.5), (0.65, 0.6), (0.75, 0.7) }; //G
                    orderedFOVs = GenerateFOVArray(FOV_Min, FOV_Max, percentileRanges, false);
                }
                else
                {
                    int interval = 3;
                    orderedFOVs = Enumerable.Range(FOV_Min, 4).ToList();
                    List<int> indices = rangeFov.Where((_, index) => index % interval == 0).ToList();
                    orderedFOVs.AddRange(indices.Except(orderedFOVs));
                    orderedFOVs.AddRange(rangeFov.Reverse().Except(orderedFOVs));
                }
            }
            else
            {
                orderedFOVs = Enumerable.Range(well.FOV_Min, Math.Min(well.FOV_Max, 3)).ToList();
                orderedFOVs.AddRange(rangeFov.Reverse().Except(orderedFOVs));
            }*/

            //orderedFOVs = Enumerable.Range(well.FOV_Min, well.FOV_Max - well.FOV_Min + 1).ToList(); //Pattern F

/*            foreach (var FOV in orderedFOVs)
            {
                var xI = well.GetFields(FOV);
                CombinedBMAP(well, xI[0], WVPs, true);
                Thread.SpinWait(3);
            }*/
        }

        private void fixWelllist()
        {
            wellList = InCell_To_Leica.SR.SavedWellList;
        }
        public static Random _Rand = new Random();

        static List<int> GenerateFOVArray(int FOV_Min, int FOV_Max, List<(double start, double end)> percentileRanges, bool RandomizeRemaining = false)
        {
            var orderedFOVs = new List<int>();

            foreach (var (start, end) in percentileRanges)
            {
                int startIndex = FOV_Min + (int)Math.Floor((FOV_Max - FOV_Min) * start);
                int endIndex = FOV_Min + (int)Math.Floor((FOV_Max - FOV_Min) * end);

                if (startIndex < endIndex)
                    orderedFOVs.AddRange(Enumerable.Range(startIndex, endIndex - startIndex + 1));
                else
                    orderedFOVs.AddRange(Enumerable.Range(endIndex, startIndex - endIndex + 1).Reverse());
            }
            //Fill in the rest
            var orderedFOVs_Full = orderedFOVs;
            if (RandomizeRemaining)
                orderedFOVs.AddRange(Enumerable.Range(FOV_Min, FOV_Max - FOV_Min + 1).Except(orderedFOVs).OrderBy(_Rand.Next));
            else
                orderedFOVs.AddRange(Enumerable.Range(FOV_Min, FOV_Max - FOV_Min + 1).Except(orderedFOVs).Reverse());
            return orderedFOVs_Full;
        }


    }

}
