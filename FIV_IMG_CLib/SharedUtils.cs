using CommunityToolkit.HighPerformance;
using CommunityToolkit.HighPerformance.Helpers;
using CoreImageLoader;
using FIVE;
using FIVE.InCellLibrary;
using FIVE.Masks;
using ImageMagick;
using Microsoft.Extensions.Caching.Memory;
using OpenCvSharp;
using PureHDF;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.Marshalling;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Tensorflow.Gradients;
using static FIVE.Generics;
using static System.Windows.Forms.DataFormats;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace FIVE_IMG
{
    public static class FileHandler
    {
        /// <summary>
        /// Safely writes text to a specified file, creating the directory path if necessary.
        /// </summary>
        /// <param name="path">The full path of the file where text will be written.</param>
        /// <param name="text">The text to write to the file. If no text is specified, an empty file will be created.</param>
        public static void SafelyWrite(string path, string text = "")
        {
            try
            {
                // Check if the directory exists for the given path, and if not, create it
                string directoryPath = Path.GetDirectoryName(path);
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                // Write the text to the file, creating the file if it does not exist
                File.AppendAllText(path, text);

                // Inform the user that the operation was successful
                Trace.WriteLine($"Text has been successfully written to '{path}'.");
            }
            catch (UnauthorizedAccessException ex)
            {
                // Handle lack of permission
                Trace.WriteLine($"Error: Permission denied to write to '{path}'. {ex.Message}");
            }
            catch (PathTooLongException ex)
            {
                // Handle cases where the path is too long
                Trace.WriteLine($"Error: The file path is too long. {ex.Message}");
            }
            catch (DirectoryNotFoundException ex)
            {
                // Handle cases where the directory is not found
                Trace.WriteLine($"Error: The directory was not found. {ex.Message}");
            }
            catch (IOException ex)
            {
                // Handle other I/O errors
                Trace.WriteLine($"Error: An I/O error occurred while writing to the file. {ex.Message}");
            }
            catch (Exception ex)
            {
                // Handle all other exceptions
                Trace.WriteLine($"An unexpected error occurred: {ex.Message}");
            }
        }
    }
    public static class MasksHelper
    {
        public static string ExportAll(INCELL_Folder folder, SVP_Registration_Params SRVP, System.ComponentModel.BackgroundWorker BW = null)
        {
            var fI = new FileInfo(folder.MaskList_FullPath);
            if (!fI.Exists) return "Couldn't find MaskList.tsv file.  Please save a file with the list of cells you would like to generate masks for. If should be names like this\r\n" + fI.FullName + "\r\nmake sure it has the Well, FOV, Imagename, Left, Top, Width, and Height of the cell in tab-separated format";
            var ML = new MaskList(fI.FullName, folder.PlateID, folder.XDCE.PixelWidth_in_um);

            bool problem = false;
            if (ML == null) problem = true; else if (ML.Count == 0) problem = true;
            if (problem) return INCELL_Folder.ErrorLog;

            if (BW != null) BW.ReportProgress(0, "Loaded Mask List.");
            SRVP.CropForLeica = true;
            MasksHelper.SaveMasks(ML, fI.DirectoryName, SRVP, BW);
            return "";
        }

        public static void ExportOverlay(ImageAlign_Return IAR, string ExportFullFileName, ImgWrap ImgInternal, ImgWrap ImgExternal)
        {
            //First need to "Expand" the Internal Image with Black Border (by pasting it into a black image)
            Bitmap destBitmap = new Bitmap(ImgExternal.Width, ImgExternal.Height);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                Rectangle ImageRect_SRC = new Rectangle(0, 0, ImgInternal.Width, ImgInternal.Height);
                Rectangle ImageRect_Dest = new Rectangle(0, 0, ImgExternal.Width, ImgExternal.Height);
                Rectangle destRegion = new Rectangle(Point.Round(IAR.RR.Location), ImgInternal.BMP.Size);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);
                g.DrawImage(ImgInternal.BMP, destRegion, ImageRect_SRC, GraphicsUnit.Pixel);
                g.DrawRectangle(Pens.Yellow, destRegion);
            }
            Bitmap rBMP = CoreLogic.ArithmeticBlend(destBitmap, ImgExternal.BMP, CoreLogic.ColorCalculationType.Difference);
            rBMP.Save(Path.Combine(ExportFullFileName));
        }
        public static void CroppedExportOverlay(ImageAlign_Return IAR, string ExportFullFileName, ImgWrap ImgInternal, ImgWrap ImgExternal, PointF drawHere)
        {
            //First need to "Expand" the Internal Image with Black Border (by pasting it into a black image)
            var adjLocation = drawHere;
            Bitmap destBitmap = new Bitmap(ImgExternal.Width, ImgExternal.Height);
            ImgExternal.BMP.Save("C:\\Users\\blabPrime\\Desktop\\Manny\\external.bmp");
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                Rectangle ImageRect_SRC = new Rectangle(0, 0, ImgInternal.Width, ImgInternal.Height);
                Rectangle ImageRect_Dest = new Rectangle(0, 0, ImgExternal.Width, ImgExternal.Height);
                Rectangle destRegion = new Rectangle(Point.Round(adjLocation), ImgInternal.BMP.Size);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);
                g.DrawImage(ImgInternal.BMP, destRegion, ImageRect_SRC, GraphicsUnit.Pixel);
                g.DrawRectangle(Pens.Yellow, destRegion);
            }
            Bitmap rBMP = CoreLogic.ArithmeticBlend(destBitmap, ImgExternal.BMP, CoreLogic.ColorCalculationType.Difference);
            rBMP.Save(Path.Combine(ExportFullFileName));
        }

        public static void Export5xShiftedOverlay(string ExportFullFileName, Bitmap ImgInternal, ImgWrap ImgExternal)
        {
            //First need to "Expand" the Internal Image with Black Border (by pasting it into a black image)
            Bitmap destBitmap = new Bitmap(ImgExternal.Width, ImgExternal.Height);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                Rectangle ImageRect_SRC = new Rectangle(0, 0, ImgInternal.Width, ImgInternal.Height);
                Rectangle ImageRect_Dest = new Rectangle(0, 0, ImgExternal.Width, ImgExternal.Height);
                Rectangle destRegion = new Rectangle(0, 0, ImgInternal.Width, ImgInternal.Height);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);
                g.DrawImage(ImgInternal, destRegion, ImageRect_SRC, GraphicsUnit.Pixel);
            }
            Bitmap rBMP = CoreLogic.ArithmeticBlend(destBitmap, ImgExternal.BMP, CoreLogic.ColorCalculationType.Difference);
            Directory.CreateDirectory(Path.GetDirectoryName(ExportFullFileName));
            rBMP.Save(Path.Combine(ExportFullFileName));
        }

        public static string Export5xShiftedMask(Bitmap new5xMask, string imgLEname)
        {  //Manny 9/2023
            var saveName = InCell_To_Leica.SR.Path_5xMasks + $"\\{imgLEname}Mask.tif";
            Directory.CreateDirectory(InCell_To_Leica.SR.Path_5xMasks);
            InCell_To_Leica.SR.CurrentShiftMaskFullPath = saveName;
            new5xMask.Save(saveName, ImageFormat.Tiff);
            return saveName;
        }

        /// <summary>
        /// This is used just to process the mask without having done registration
        /// </summary>
        public static string ExportShiftedMask(XDCE_Image xI, SVP_Registration_Params RegParams)
        {
            //if (!xI.HasMask) xI = xI.Parent.GetField(xI.FOV, 1);
            //Just a cheating way to pass on the RegParams but nothing else
            var IAR = new ImageAlign_Return();
            IAR.Parent = new Associate_InCell_Leica(RegParams);
            IAR.RR = new Register_Return();
            return ExportShiftedMask(xI, IAR, Size.Empty, Size.Empty);
        }

        /// <summary>
        /// This is used to really shift the mask after registration has been performed
        /// </summary>
        public static string ExportShiftedMask(XDCE_Image xI, ImageAlign_Return IAR, Size GEFract_Size, Size Overlap, string MaskSaveTemp = "")
        {
            if (!xI.HasMask) return "no mask";
            var MaskImage = new Bitmap(xI.MaskFullPath);
            var FI = new FileInfo(xI.MaskFullPath);
            long ImgBits = (8 * FI.Length) / (MaskImage.Size.Width * MaskImage.Size.Height);


            bool ShiftMode = !GEFract_Size.IsEmpty;
            float shift_X = 0, shift_Y = 0;

            if (ShiftMode)
            {
                float CenterPixel_LE_small_X = IAR.RR.Location.X + (GEFract_Size.Width / 2); float CenterPixel_LE_small_Y = IAR.RR.Location.Y + (GEFract_Size.Height / 2);
                shift_X = CenterPixel_LE_small_X - ((float)Overlap.Width / 2); shift_Y = CenterPixel_LE_small_Y - ((float)Overlap.Height / 2);
                float Factor = (float)MaskImage.Width / (Overlap.Width);
                Factor *= IAR.Parent.Reg_Params.MskShift_ShiftMultiplier; //This adjusts the "strength" of the mask shift by this factor. 2 means it shifts twice as far as it thinks it should
                shift_X *= Factor; shift_Y *= Factor;
            }
            shift_X += IAR.Parent.Reg_Params.MskShift_Final_AddPixels_X; shift_Y += IAR.Parent.Reg_Params.MskShift_Final_AddPixels_Y; //Adds this amount of pixels to additionally shift in X
            IAR.RR.ShiftMask_Pixels = new PointF(shift_X, shift_Y); //Save this so we can see it elsewhere

            int Zm = IAR.Parent.Reg_Params.MskShift_Final_ZoomPixels; //Removes this many pixels from each side to "zoom" in the mask. Negative adds black pixels to zoom out
            Bitmap destBitmap = new Bitmap(MaskImage.Width, MaskImage.Height);
            using (Graphics g = Graphics.FromImage(destBitmap))
            {
                //Fill the destination image background to black
                Rectangle ImageRect_Dest = new Rectangle(0, 0, destBitmap.Width, destBitmap.Height);
                g.FillRectangle(Brushes.Black, ImageRect_Dest);

                //Now actually plug the mask image into the output
                Rectangle IR_SRC = new Rectangle((int)Math.Max(0, -shift_X), (int)Math.Max(0, -shift_Y), MaskImage.Width - (int)Math.Abs(shift_X), MaskImage.Height - (int)Math.Abs(shift_Y));
                Rectangle IR_sZoom = Zm > 0 ? new Rectangle(IR_SRC.X + Zm, IR_SRC.Y + Zm, IR_SRC.Width - (Zm * 2), IR_SRC.Height - (Zm * 2)) : IR_SRC; //If zoom is positive, then this shrinks the source rectangle, otherwise doesn't change
                Rectangle destRegion = new Rectangle((int)Math.Max(0, shift_X), (int)Math.Max(0, shift_Y), IR_SRC.Width, IR_SRC.Height);
                Rectangle destZoom = Zm > 0 ? destRegion : new Rectangle(destRegion.X - Zm, destRegion.Y - Zm, destRegion.Width + (Zm * 2), destRegion.Height + (Zm * 2)); //If zoom is negative (zoom out), then source rectangle doesnt change, but the destination region is shrunk (since this zoom is negative that is why the signs are swapped)
                g.DrawImage(MaskImage, destZoom, IR_sZoom, GraphicsUnit.Pixel);
            }

            if (ImgBits <= 2)
                MasksHelper.Save1BitTiff(destBitmap, xI.MaskShiftedFullPath);
            else
                MasksHelper.Save8BitTiff(destBitmap, xI.MaskShiftedFullPath);
            return xI.MaskShiftedFullPath;
        }
        //if this does get implemented batch it by grouping by display color.
        public static Bitmap DrawObjectsPixels(IEnumerable<Seg.SegObject> SegObjs, int WidthOriginal, int WidthNew, Bitmap Background = null)
        {
            var bmap = Background == null ? new Bitmap(WidthNew, WidthNew) : new Bitmap(Background, new Size(WidthNew, WidthNew));
            float Ratio = (float)WidthNew / WidthOriginal;

            var tFont = new Font("Arial", 11);
            using (var g = Graphics.FromImage(bmap))
            {
                if (Background == null)
                    g.Clear(Color.Black);

                // Group objects by their display color
                var groupedByColor = SegObjs.GroupBy(obj => obj.DisplayColor);

                foreach (var group in groupedByColor)
                {
                    using (var brush = new SolidBrush(group.Key))
                    {
                        foreach (var obj in group)
                        {
                            // Example drawing the object pixels
                            DrawObjectPixels(obj, g, brush, Ratio);

                            // Example adding text labels
                            g.DrawString(
                                obj.Report,
                                tFont,
                                brush,
                                new PointF((float)(obj.Rect.X * Ratio), (float)(obj.Rect.Y * Ratio))
                            );
                        }
                    }
                }
            }

            return bmap;
        }
        public static Bitmap DrawObjectsOutlines(IEnumerable<Seg.SegObject> SegObjs, int WidthOriginal, int WidthNew, Bitmap Background = null)
        {
            var bmap = Background == null ? new Bitmap(WidthNew, WidthNew) : new Bitmap(Background, new Size(WidthNew, WidthNew));
            float Ratio = (float)WidthNew / WidthOriginal;
            var tBrush = new SolidBrush(Color.Aqua);
            var fBrush = new SolidBrush(Color.AntiqueWhite);
            var tFont = new Font("Arial", 11);
            GraphicsPath path = new GraphicsPath();
            using (var g = Graphics.FromImage(bmap))
            {
                if (Background == null) 
                    g.Clear(Color.Black);
                foreach (var Obj in SegObjs)
                {
                    RectangleF rect = Obj.Rect;
                    DrawObjectPixels(Obj, g, tBrush, Ratio);

                    path.AddString(
                        Obj.Report,                         
                        tFont.FontFamily,                   
                        (int)tFont.Style,                   
                        tFont.SizeInPoints,
                        new PointF((float)(rect.X * Ratio), (float)(rect.Y * Ratio)), // Text position
                        System.Drawing.StringFormat.GenericDefault         // Text formatting
                    );
                }
                g.FillPath(fBrush, path);
            }
         
            return bmap;
        }

        private static void DrawObjectPixels(Seg.SegObject obj, Graphics g, SolidBrush tBrush, float ratio)
        {
            //create the rectangles
            Rectangle[] rectangles = new Rectangle[obj.BoundaryPixels.Count];
            int iterator = 0;
            foreach (var Pix in obj.BoundaryPixels)
            {
                Rectangle rectangle = new Rectangle((int)(Pix.P.X * ratio), (int)(Pix.P.Y * ratio), (int)ratio, (int)ratio);
                rectangles[iterator] = rectangle;
                iterator++;
            }
            g.FillRectangles(tBrush, rectangles);
        }

        public static Bitmap DrawMask(IEnumerable<Seg.SegObject> SegObjs, int OrigWidth, int NewWidth, int pixelExpand = 0, bool RandColors = false, Size? RegionSize = null, Bitmap Background = null)
        {
            var ML = new MaskList("", "", 1);


            // Use LINQ for mapping
            List<MaskListEntry> Cells = SegObjs.Select(Obj =>
            {
                var rect = Obj.Rect;  // Cache Obj.Rect to avoid repeated access
                var size = Obj.Size;  // Cache Obj.Size for reuse
                float PiReciprocal = (1f/(float)Math.PI);
                return new MaskListEntry
                {
                    Parent = ML,
                    Left_pix = rect.Left,
                    Top_pix = rect.Top,
                    Width_pix = size.Width,
                    Height_pix = size.Height,
                    DisplayOnlyColor = Obj.DisplayColor,
                    axisAngle_deg = ((float)(180 * Obj.m_MajorAxisAngle_Radians * PiReciprocal)),
                    majorAxisLength_um = Math.Max(rect.Width, rect.Height),
                    minorAxisLength_um = Math.Min(rect.Width, rect.Height)
                };
            }).ToList();
            return DrawMask(Cells, OrigWidth, NewWidth, pixelExpand, RandColors, RegionSize, Background);
        }

        public static Bitmap DrawMask(List<MaskListEntry> Cells, int WidthOriginal, int WidthNew, int pixelExpand = 0, bool RandColors = false, Size? RegionSize = null, Bitmap Background = null)
        {
            Size rSize = RegionSize ?? Size.Empty;
            var bmap = Background == null ? new Bitmap(WidthNew, WidthNew) : new Bitmap(Background, new Size(WidthNew, WidthNew));
            float Ratio = (float)WidthNew / WidthOriginal;
            int xC = (int)(rSize.Width * Ratio);
            int yC = (int)(rSize.Height * Ratio);
            using (var g = Graphics.FromImage(bmap))
            {
                if (Background == null) g.Clear(Color.Black); 
                //g.FillRectangle(solidBrushBlack, new Rectangle(new Point(0, 0), bmap.Size));
                var tBrush = solidBrushBlack;
                RectangleF[] rectangles = new RectangleF[Cells.Count];
                for (int i = 0; i < Cells.Count; i++)
                {
                    if (Background == null) 
                        drawEllipse(g, bmap, Cells[i], Ratio, pixelExpand, RandColors);
                    if (rSize != Size.Empty)
                    {
                        PointF centerPix = Cells[i].Center_Pix;
                        rectangles[i] = new RectangleF((int)(centerPix.X * Ratio - xC * 0.5), (int)(centerPix.Y * Ratio - yC *0.5), xC, yC);
                    }
                }
                if (rSize != Size.Empty)
                {
                    g.DrawRectangles(penGrayThick, rectangles);
                }
            }
            return bmap;
        }

        private static Random _Rand = new Random();
        public static Pen penBlack = new Pen(Color.Black, 1);
        public static Pen penWhite = new Pen(Color.White, 1);
        public static Pen penGray = new Pen(Color.Gray, 1);
        public static Pen penGrayThick = new Pen(Color.Gray, 6);
        public static SolidBrush solidBrushBlack = new SolidBrush(Color.Black);
        public static SolidBrush solidBrushWhite = new SolidBrush(Color.White);
        public static SolidBrush solidBrushGray = new SolidBrush(Color.Gray);
        public static SolidBrush solidBrushRed = new SolidBrush(Color.Red);
        public static Matrix transformMatrix = new Matrix();
        public static void drawEllipse(Graphics g, Bitmap bmap, MaskListEntry ellipse, float Resize, int pixelExpand = 0, bool RandColors = false)
        {
            var r = new Random();
            Brush solidBrushUse;
            if (RandColors)
            {
                solidBrushUse = new SolidBrush(ellipse.DisplayOnlyColor);
            }
            else
                solidBrushUse = MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Grayscale) ?
                new SolidBrush(Color.FromArgb((int)(ellipse.Intensity * 255), (int)(ellipse.Intensity * 255), (int)(ellipse.Intensity * 255))) :
                solidBrushWhite;

            bool Rotate = true;
            int outer = pixelExpand;
            int leftBound = (int)(ellipse.Left_pix * Resize);
            int topBound = (int)(ellipse.Top_pix * Resize);
            int width = (int)(ellipse.Width_pix * Resize);
            int height = (int)(ellipse.Height_pix * Resize);
            int xa, ya;

            Color pixelcolor;
            if (pixelExpand > 0)
            {
                leftBound = leftBound - outer;
                topBound = topBound - outer;
                width = width + 2 * outer;
                height = height + 2 * outer;
            }

            if (Rotate)
            {
   
                float centerX = leftBound + width / 2.0f;
                float centerY = topBound + height / 2.0f;

                // Translate to the center of the ellipse, apply rotation, and then translate back
                transformMatrix.Translate(centerX, centerY);
                transformMatrix.Rotate(ellipse.axisAngle_deg);
                transformMatrix.Translate(-centerX, -centerY);

                // Apply the combined transformation
                g.Transform = transformMatrix;

                // Draw the ellipse
                g.FillEllipse(solidBrushUse, leftBound, topBound, width, height);

         
                transformMatrix.Reset();
                g.ResetTransform();

            }
            else
            {
                g.FillEllipse(solidBrushUse, leftBound, topBound, width, height);
            }
            //If grayscale is on, now we subtract from the ellipse to get the dithered mask
            if(ellipse.Intensity_Method == null)
            {
                return;
            }
            else if (MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Dither))
            {
                SortedList<double, int[]> pixels = new SortedList<double, int[]>();
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        xa = x + leftBound; ya = y + topBound;
                        pixelcolor = bmap.GetPixel(xa, ya);
                        if (pixelcolor.R > 0) //i.e. is white
                        {
                            double rand = r.NextDouble();
                            while (pixels.ContainsKey(rand)) r.NextDouble();
                            pixels.Add(rand, new int[] { xa, ya });
                        }
                    }
                }
                int totalPixelsToDraw = (int)(pixels.Count * ellipse.Intensity); // Calculate how many pixels to draw
                var allRectangles = new Rectangle[totalPixelsToDraw]; // Preallocate the array

                int counter = 0;

                // Collect all rectangles up to the required count
                foreach (KeyValuePair<double, int[]> pix in pixels)
                {
                    if (counter >= totalPixelsToDraw) break;
                    allRectangles[counter] = new Rectangle(pix.Value[0], pix.Value[1], 1, 1);
                    counter++;
                }

                // Draw all rectangles in one go
                if (totalPixelsToDraw > 0)
                {
                    g.FillRectangles(solidBrushBlack, allRectangles);
                }
            }
            else if (MaskList.CheckStringAgainst_IntensityType(ellipse.Intensity_Method, MaskIntensityType.Skeleton))
            {
                int ao = 1;
                byte[,] arr = new byte[width + ao * 2, height + ao * 2]; int total = 0;
                //11/9/2021 New method that avoids holes
                //First go through and figure out which points are in the list
                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        xa = x + leftBound; ya = y + topBound; pixelcolor = bmap.GetPixel(xa, ya);
                        arr[x + ao, y + ao] = (byte)((pixelcolor.R > 0) ? 1 : 0); //1 if white
                        if (pixelcolor.R > 0) total++;
                    }
                }
                //Now go and turn off blocks of 2 based on the saved list
                int counter = 0; Point tP; int xo, yo, xt, yt; bool exec;
                xo = width / 2; yo = height / 2;
                List<Rectangle> rects = new List<Rectangle>();
                for (int i = 0; i < MaskSkeletonOrder.Count; i++)
                {
                    exec = false;
                    tP = MaskSkeletonOrder[i]; xt = tP.X * 2 + xo; yt = tP.Y * 2 + yo;
                    if ((xt >= (width + ao)) || (xt < -ao) || (yt >= (height + ao)) || (yt < -ao)) continue;
                    if (arr[xt + ao, yt + ao] == 1) exec = true;
                    if (!exec)
                    {
                        if ((xt + 1 >= (width + ao)) || (yt + 1 >= (height + ao)))
                            continue;
                        if (arr[xt + ao + 1, yt + ao + 1] == 1) exec = true;
                        if (arr[xt + ao + 0, yt + ao + 1] == 1) exec = true;
                        if (arr[xt + ao + 1, yt + ao + 0] == 1) exec = true;
                    }
                    if (exec)
                    {
                        rects.Add(new Rectangle(xt, yt, 2, 2));
                        counter += 4;
                        if (counter >= (total - (total * ellipse.Intensity))) break;
                    }

                }
                g.FillRectangles(solidBrushBlack, rects.ToArray());
            }
    }

        private static List<Point> _MaskSkeletonOrder;
        private static List<Point> MaskSkeletonOrder
        {
            //11/8/2021 this allows a semi-random pattern that is better for Leica scope region generation
            get
            {
                if (_MaskSkeletonOrder == null)
                {
                    _MaskSkeletonOrder = new List<Point>();
                    var PL = _MaskSkeletonOrder;
                    {
                        PL.Add(new Point(10, 4));
                        PL.Add(new Point(-11, 10));
                        PL.Add(new Point(-11, 0));
                        PL.Add(new Point(-11, -10));
                        PL.Add(new Point(-11, -2));
                        PL.Add(new Point(-11, 12));
                        PL.Add(new Point(-11, 6));
                        PL.Add(new Point(10, -8));
                        PL.Add(new Point(-11, -8));
                        PL.Add(new Point(10, 0));
                        PL.Add(new Point(-11, 8));
                        PL.Add(new Point(10, 2));
                        PL.Add(new Point(10, -10));
                        PL.Add(new Point(-11, 2));
                        PL.Add(new Point(-11, 4));
                        PL.Add(new Point(10, 8));
                        PL.Add(new Point(10, 6));
                        PL.Add(new Point(-11, -4));
                        PL.Add(new Point(10, -6));
                        PL.Add(new Point(10, 10));
                        PL.Add(new Point(-11, -6));
                        PL.Add(new Point(10, 12));
                        PL.Add(new Point(10, -2));
                        PL.Add(new Point(10, -4));
                        PL.Add(new Point(-10, -6));
                        PL.Add(new Point(-10, 6));
                        PL.Add(new Point(-10, -8));
                        PL.Add(new Point(9, 4));
                        PL.Add(new Point(-10, -2));
                        PL.Add(new Point(9, -4));
                        PL.Add(new Point(9, 8));
                        PL.Add(new Point(-10, 4));
                        PL.Add(new Point(-10, 12));
                        PL.Add(new Point(9, 12));
                        PL.Add(new Point(9, -8));
                        PL.Add(new Point(-10, -4));
                        PL.Add(new Point(9, 2));
                        PL.Add(new Point(9, 10));
                        PL.Add(new Point(-10, 8));
                        PL.Add(new Point(9, -2));
                        PL.Add(new Point(-10, 10));
                        PL.Add(new Point(9, -10));
                        PL.Add(new Point(-10, 2));
                        PL.Add(new Point(-10, 0));
                        PL.Add(new Point(9, 6));
                        PL.Add(new Point(9, -6));
                        PL.Add(new Point(9, 0));
                        PL.Add(new Point(-10, -10));
                        PL.Add(new Point(8, -6));
                        PL.Add(new Point(8, 10));
                        PL.Add(new Point(8, 8));
                        PL.Add(new Point(-9, -4));
                        PL.Add(new Point(8, -8));
                        PL.Add(new Point(-9, 10));
                        PL.Add(new Point(8, 4));
                        PL.Add(new Point(-9, -2));
                        PL.Add(new Point(-9, 2));
                        PL.Add(new Point(-9, -8));
                        PL.Add(new Point(8, 6));
                        PL.Add(new Point(8, 2));
                        PL.Add(new Point(-9, 0));
                        PL.Add(new Point(8, -4));
                        PL.Add(new Point(8, 12));
                        PL.Add(new Point(8, 0));
                        PL.Add(new Point(-9, 6));
                        PL.Add(new Point(-9, 4));
                        PL.Add(new Point(-9, -6));
                        PL.Add(new Point(8, -2));
                        PL.Add(new Point(-9, -10));
                        PL.Add(new Point(-9, 12));
                        PL.Add(new Point(8, -10));
                        PL.Add(new Point(-9, 8));
                        PL.Add(new Point(-8, 10));
                        PL.Add(new Point(7, 2));
                        PL.Add(new Point(-8, -4));
                        PL.Add(new Point(7, 4));
                        PL.Add(new Point(-8, -6));
                        PL.Add(new Point(-8, 8));
                        PL.Add(new Point(-8, 4));
                        PL.Add(new Point(7, 0));
                        PL.Add(new Point(-8, 12));
                        PL.Add(new Point(-8, -10));
                        PL.Add(new Point(7, 12));
                        PL.Add(new Point(7, -10));
                        PL.Add(new Point(7, 8));
                        PL.Add(new Point(7, -6));
                        PL.Add(new Point(-8, -8));
                        PL.Add(new Point(7, -2));
                        PL.Add(new Point(7, 10));
                        PL.Add(new Point(-8, -2));
                        PL.Add(new Point(-8, 6));
                        PL.Add(new Point(7, 6));
                        PL.Add(new Point(7, -4));
                        PL.Add(new Point(-8, 0));
                        PL.Add(new Point(-8, 2));
                        PL.Add(new Point(7, -8));
                        PL.Add(new Point(6, -2));
                        PL.Add(new Point(6, -8));
                        PL.Add(new Point(6, 6));
                        PL.Add(new Point(-7, 0));
                        PL.Add(new Point(-7, 6));
                        PL.Add(new Point(-7, 4));
                        PL.Add(new Point(-7, -2));
                        PL.Add(new Point(-7, -10));
                        PL.Add(new Point(-7, 10));
                        PL.Add(new Point(6, -4));
                        PL.Add(new Point(-7, 8));
                        PL.Add(new Point(6, 4));
                        PL.Add(new Point(6, 8));
                        PL.Add(new Point(6, -6));
                        PL.Add(new Point(-7, -6));
                        PL.Add(new Point(6, -10));
                        PL.Add(new Point(-7, 2));
                        PL.Add(new Point(-7, -4));
                        PL.Add(new Point(6, 10));
                        PL.Add(new Point(-7, 12));
                        PL.Add(new Point(6, 0));
                        PL.Add(new Point(6, 12));
                        PL.Add(new Point(-7, -8));
                        PL.Add(new Point(6, 2));
                        PL.Add(new Point(-6, 0));
                        PL.Add(new Point(-6, -2));
                        PL.Add(new Point(5, 6));
                        PL.Add(new Point(5, -6));
                        PL.Add(new Point(-6, 2));
                        PL.Add(new Point(-6, -8));
                        PL.Add(new Point(-6, 12));
                        PL.Add(new Point(-6, 10));
                        PL.Add(new Point(5, 8));
                        PL.Add(new Point(5, -10));
                        PL.Add(new Point(5, -8));
                        PL.Add(new Point(5, -4));
                        PL.Add(new Point(-6, -4));
                        PL.Add(new Point(5, 10));
                        PL.Add(new Point(5, -2));
                        PL.Add(new Point(-6, -6));
                        PL.Add(new Point(-6, 6));
                        PL.Add(new Point(5, 0));
                        PL.Add(new Point(-6, 8));
                        PL.Add(new Point(5, 12));
                        PL.Add(new Point(5, 4));
                        PL.Add(new Point(5, 2));
                        PL.Add(new Point(-6, 4));
                        PL.Add(new Point(-6, -10));
                        PL.Add(new Point(4, -8));
                        PL.Add(new Point(4, -2));
                        PL.Add(new Point(-5, 4));
                        PL.Add(new Point(-5, 6));
                        PL.Add(new Point(-5, -10));
                        PL.Add(new Point(4, 8));
                        PL.Add(new Point(4, -4));
                        PL.Add(new Point(-5, -2));
                        PL.Add(new Point(4, 10));
                        PL.Add(new Point(-5, 10));
                        PL.Add(new Point(4, 0));
                        PL.Add(new Point(4, 2));
                        PL.Add(new Point(4, 6));
                        PL.Add(new Point(-5, 8));
                        PL.Add(new Point(-5, -4));
                        PL.Add(new Point(-5, -8));
                        PL.Add(new Point(-5, -6));
                        PL.Add(new Point(-5, 2));
                        PL.Add(new Point(4, -6));
                        PL.Add(new Point(4, -10));
                        PL.Add(new Point(4, 4));
                        PL.Add(new Point(-5, 12));
                        PL.Add(new Point(4, 12));
                        PL.Add(new Point(-5, 0));
                        PL.Add(new Point(3, -8));
                        PL.Add(new Point(-4, 0));
                        PL.Add(new Point(3, -6));
                        PL.Add(new Point(3, 6));
                        PL.Add(new Point(-4, 8));
                        PL.Add(new Point(3, -2));
                        PL.Add(new Point(3, -10));
                        PL.Add(new Point(3, 8));
                        PL.Add(new Point(3, 12));
                        PL.Add(new Point(-4, 12));
                        PL.Add(new Point(-4, -4));
                        PL.Add(new Point(-4, 2));
                        PL.Add(new Point(-4, -6));
                        PL.Add(new Point(-4, -10));
                        PL.Add(new Point(3, 4));
                        PL.Add(new Point(3, 2));
                        PL.Add(new Point(-4, 10));
                        PL.Add(new Point(3, 10));
                        PL.Add(new Point(3, 0));
                        PL.Add(new Point(3, -4));
                        PL.Add(new Point(-4, -2));
                        PL.Add(new Point(-4, 6));
                        PL.Add(new Point(-4, -8));
                        PL.Add(new Point(-4, 4));
                        PL.Add(new Point(2, 6));
                        PL.Add(new Point(2, 8));
                        PL.Add(new Point(-3, 4));
                        PL.Add(new Point(2, -2));
                        PL.Add(new Point(-3, -10));
                        PL.Add(new Point(-3, -2));
                        PL.Add(new Point(2, 2));
                        PL.Add(new Point(2, 4));
                        PL.Add(new Point(-3, -4));
                        PL.Add(new Point(-3, -8));
                        PL.Add(new Point(2, 12));
                        PL.Add(new Point(-3, 10));
                        PL.Add(new Point(-3, 12));
                        PL.Add(new Point(2, -6));
                        PL.Add(new Point(2, -10));
                        PL.Add(new Point(-3, -6));
                        PL.Add(new Point(-3, 2));
                        PL.Add(new Point(2, 10));
                        PL.Add(new Point(2, -8));
                        PL.Add(new Point(-3, 6));
                        PL.Add(new Point(2, 0));
                        PL.Add(new Point(2, -4));
                        PL.Add(new Point(-3, 0));
                        PL.Add(new Point(-3, 8));
                        PL.Add(new Point(-2, -2));
                        PL.Add(new Point(-2, 2));
                        PL.Add(new Point(1, -2));
                        PL.Add(new Point(-2, 0));
                        PL.Add(new Point(1, -6));
                        PL.Add(new Point(-2, 12));
                        PL.Add(new Point(1, 8));
                        PL.Add(new Point(1, -4));
                        PL.Add(new Point(-2, -10));
                        PL.Add(new Point(-2, -8));
                        PL.Add(new Point(-2, 6));
                        PL.Add(new Point(1, -8));
                        PL.Add(new Point(-2, 4));
                        PL.Add(new Point(1, -10));
                        PL.Add(new Point(1, 4));
                        PL.Add(new Point(-2, -6));
                        PL.Add(new Point(1, 6));
                        PL.Add(new Point(1, 2));
                        PL.Add(new Point(-2, 8));
                        PL.Add(new Point(-2, 10));
                        PL.Add(new Point(1, 0));
                        PL.Add(new Point(1, 12));
                        PL.Add(new Point(1, 10));
                        PL.Add(new Point(-2, -4));
                        PL.Add(new Point(-1, 12));
                        PL.Add(new Point(0, -6));
                        PL.Add(new Point(0, -4));
                        PL.Add(new Point(-1, 6));
                        PL.Add(new Point(-1, -4));
                        PL.Add(new Point(0, 12));
                        PL.Add(new Point(0, 6));
                        PL.Add(new Point(-1, 10));
                        PL.Add(new Point(0, 4));
                        PL.Add(new Point(-1, -8));
                        PL.Add(new Point(-1, -10));
                        PL.Add(new Point(-1, 2));
                        PL.Add(new Point(-1, 8));
                        PL.Add(new Point(0, 8));
                        PL.Add(new Point(-1, -6));
                        PL.Add(new Point(0, -10));
                        PL.Add(new Point(-1, 0));
                        PL.Add(new Point(-1, 4));
                        PL.Add(new Point(0, 2));
                        PL.Add(new Point(-1, -2));
                        PL.Add(new Point(0, -2));
                        PL.Add(new Point(0, 0));
                        PL.Add(new Point(0, -8));
                        PL.Add(new Point(0, 10));
                        PL.Add(new Point(-8, -7));
                        PL.Add(new Point(-7, 1));
                        PL.Add(new Point(6, 7));
                        PL.Add(new Point(-1, -7));
                        PL.Add(new Point(-6, 11));
                        PL.Add(new Point(-4, 1));
                        PL.Add(new Point(-10, 3));
                        PL.Add(new Point(-3, -5));
                        PL.Add(new Point(-11, 9));
                        PL.Add(new Point(0, -11));
                        PL.Add(new Point(-9, 11));
                        PL.Add(new Point(5, 9));
                        PL.Add(new Point(0, 3));
                        PL.Add(new Point(-4, -1));
                        PL.Add(new Point(1, 11));
                        PL.Add(new Point(-3, 9));
                        PL.Add(new Point(10, 9));
                        PL.Add(new Point(-8, 3));
                        PL.Add(new Point(-2, 1));
                        PL.Add(new Point(-2, 11));
                        PL.Add(new Point(-7, 5));
                        PL.Add(new Point(2, 1));
                        PL.Add(new Point(-11, -1));
                        PL.Add(new Point(8, 11));
                        PL.Add(new Point(-2, 3));
                        PL.Add(new Point(-6, -3));
                        PL.Add(new Point(-5, -5));
                        PL.Add(new Point(-5, -11));
                        PL.Add(new Point(8, -3));
                        PL.Add(new Point(3, 11));
                        PL.Add(new Point(-5, -1));
                        PL.Add(new Point(1, -3));
                        PL.Add(new Point(-8, 9));
                        PL.Add(new Point(0, 7));
                        PL.Add(new Point(5, -9));
                        PL.Add(new Point(4, -1));
                        PL.Add(new Point(-1, -1));
                        PL.Add(new Point(-11, -3));
                        PL.Add(new Point(-11, 11));
                        PL.Add(new Point(5, 7));
                        PL.Add(new Point(4, 3));
                        PL.Add(new Point(-4, 7));
                        PL.Add(new Point(7, -1));
                        PL.Add(new Point(4, -11));
                        PL.Add(new Point(9, 11));
                        PL.Add(new Point(-9, -1));
                        PL.Add(new Point(-1, 11));
                        PL.Add(new Point(-2, -7));
                        PL.Add(new Point(1, 3));
                        PL.Add(new Point(-3, -1));
                        PL.Add(new Point(-4, -7));
                        PL.Add(new Point(-10, -7));
                        PL.Add(new Point(-1, -5));
                        PL.Add(new Point(-6, -9));
                        PL.Add(new Point(4, -9));
                        PL.Add(new Point(-6, -1));
                        PL.Add(new Point(-8, -11));
                        PL.Add(new Point(-9, 3));
                        PL.Add(new Point(-7, -5));
                        PL.Add(new Point(0, 1));
                        PL.Add(new Point(-6, -5));
                        PL.Add(new Point(-10, 5));
                        PL.Add(new Point(9, 5));
                        PL.Add(new Point(-5, 11));
                        PL.Add(new Point(-11, -7));
                        PL.Add(new Point(8, -5));
                        PL.Add(new Point(9, -1));
                        PL.Add(new Point(-9, 7));
                        PL.Add(new Point(8, -9));
                        PL.Add(new Point(10, -5));
                        PL.Add(new Point(10, 1));
                        PL.Add(new Point(-9, 9));
                        PL.Add(new Point(-7, -1));
                        PL.Add(new Point(2, -5));
                        PL.Add(new Point(6, 9));
                        PL.Add(new Point(5, -7));
                        PL.Add(new Point(-3, 1));
                        PL.Add(new Point(2, -1));
                        PL.Add(new Point(-5, 5));
                        PL.Add(new Point(-6, -11));
                        PL.Add(new Point(-8, 1));
                        PL.Add(new Point(-9, -5));
                        PL.Add(new Point(0, 5));
                        PL.Add(new Point(0, -9));
                        PL.Add(new Point(5, 11));
                        PL.Add(new Point(4, 1));
                        PL.Add(new Point(-3, -9));
                        PL.Add(new Point(-6, 9));
                        PL.Add(new Point(2, -9));
                        PL.Add(new Point(-4, 3));
                        PL.Add(new Point(-1, 7));
                        PL.Add(new Point(-11, 7));
                        PL.Add(new Point(-2, -9));
                        PL.Add(new Point(-1, -11));
                        PL.Add(new Point(-2, 5));
                        PL.Add(new Point(3, 1));
                        PL.Add(new Point(-9, 1));
                        PL.Add(new Point(-9, -11));
                        PL.Add(new Point(-3, -3));
                        PL.Add(new Point(2, -3));
                        PL.Add(new Point(-1, 5));
                        PL.Add(new Point(3, -5));
                        PL.Add(new Point(0, 9));
                        PL.Add(new Point(0, -7));
                        PL.Add(new Point(6, 11));
                        PL.Add(new Point(2, -7));
                        PL.Add(new Point(-6, 7));
                        PL.Add(new Point(7, 9));
                        PL.Add(new Point(-7, -3));
                        PL.Add(new Point(6, -5));
                        PL.Add(new Point(10, 11));
                        PL.Add(new Point(-6, 3));
                        PL.Add(new Point(1, -5));
                        PL.Add(new Point(-11, -9));
                        PL.Add(new Point(10, 3));
                        PL.Add(new Point(5, 5));
                        PL.Add(new Point(7, 11));
                        PL.Add(new Point(-10, -3));
                        PL.Add(new Point(7, 5));
                        PL.Add(new Point(-5, 7));
                        PL.Add(new Point(1, 1));
                        PL.Add(new Point(7, 3));
                        PL.Add(new Point(9, 7));
                        PL.Add(new Point(-5, -3));
                        PL.Add(new Point(-8, 11));
                        PL.Add(new Point(-5, -9));
                        PL.Add(new Point(1, -11));
                        PL.Add(new Point(-2, -11));
                        PL.Add(new Point(9, 3));
                        PL.Add(new Point(3, 7));
                        PL.Add(new Point(3, -1));
                        PL.Add(new Point(-7, -11));
                        PL.Add(new Point(-5, 9));
                        PL.Add(new Point(-4, 9));
                        PL.Add(new Point(-3, 7));
                        PL.Add(new Point(10, -9));
                        PL.Add(new Point(2, 5));
                        PL.Add(new Point(-4, 11));
                        PL.Add(new Point(-10, -5));
                        PL.Add(new Point(-7, -9));
                        PL.Add(new Point(5, 1));
                        PL.Add(new Point(-3, 11));
                        PL.Add(new Point(-4, -5));
                        PL.Add(new Point(0, -3));
                        PL.Add(new Point(-1, -9));
                        PL.Add(new Point(9, -7));
                        PL.Add(new Point(5, -3));
                        PL.Add(new Point(4, -5));
                        PL.Add(new Point(8, 7));
                        PL.Add(new Point(-2, -5));
                        PL.Add(new Point(-6, -7));
                        PL.Add(new Point(0, 11));
                        PL.Add(new Point(-4, -11));
                        PL.Add(new Point(-5, 1));
                        PL.Add(new Point(5, 3));
                        PL.Add(new Point(0, -5));
                        PL.Add(new Point(9, 1));
                        PL.Add(new Point(8, 3));
                        PL.Add(new Point(-3, 5));
                        PL.Add(new Point(-10, 7));
                        PL.Add(new Point(6, -3));
                        PL.Add(new Point(-2, 7));
                        PL.Add(new Point(-9, 5));
                        PL.Add(new Point(1, -7));
                        PL.Add(new Point(-1, 9));
                        PL.Add(new Point(7, 7));
                        PL.Add(new Point(-5, 3));
                        PL.Add(new Point(9, 9));
                        PL.Add(new Point(4, 11));
                        PL.Add(new Point(3, 3));
                        PL.Add(new Point(-8, -3));
                        PL.Add(new Point(6, -7));
                        PL.Add(new Point(-11, 5));
                        PL.Add(new Point(-7, 9));
                        PL.Add(new Point(2, 3));
                        PL.Add(new Point(-3, -7));
                        PL.Add(new Point(8, 1));
                        PL.Add(new Point(-7, -7));
                        PL.Add(new Point(-1, -3));
                        PL.Add(new Point(-5, -7));
                        PL.Add(new Point(10, -7));
                        PL.Add(new Point(8, 5));
                        PL.Add(new Point(-10, -9));
                        PL.Add(new Point(4, -7));
                        PL.Add(new Point(6, -1));
                        PL.Add(new Point(0, -1));
                        PL.Add(new Point(4, 9));
                        PL.Add(new Point(6, 5));
                        PL.Add(new Point(-1, 3));
                        PL.Add(new Point(6, -11));
                        PL.Add(new Point(-10, -1));
                        PL.Add(new Point(1, 9));
                        PL.Add(new Point(1, -9));
                        PL.Add(new Point(8, 9));
                        PL.Add(new Point(-11, 3));
                        PL.Add(new Point(9, -11));
                        PL.Add(new Point(6, 1));
                        PL.Add(new Point(3, -3));
                        PL.Add(new Point(-8, -1));
                        PL.Add(new Point(-11, -11));
                        PL.Add(new Point(10, 7));
                        PL.Add(new Point(-11, -5));
                        PL.Add(new Point(3, 5));
                        PL.Add(new Point(-9, -3));
                        PL.Add(new Point(3, 9));
                        PL.Add(new Point(5, -5));
                        PL.Add(new Point(5, -11));
                        PL.Add(new Point(-4, -3));
                        PL.Add(new Point(-3, 3));
                        PL.Add(new Point(8, -1));
                        PL.Add(new Point(10, 5));
                        PL.Add(new Point(2, 7));
                        PL.Add(new Point(7, -11));
                        PL.Add(new Point(-11, 1));
                        PL.Add(new Point(-6, 5));
                        PL.Add(new Point(3, -11));
                        PL.Add(new Point(-3, -11));
                        PL.Add(new Point(1, 7));
                        PL.Add(new Point(5, -1));
                        PL.Add(new Point(-4, 5));
                        PL.Add(new Point(9, -5));
                        PL.Add(new Point(-7, 11));
                        PL.Add(new Point(-9, -7));
                        PL.Add(new Point(-1, 1));
                        PL.Add(new Point(6, -9));
                        PL.Add(new Point(10, -11));
                        PL.Add(new Point(-10, 1));
                        PL.Add(new Point(7, -5));
                        PL.Add(new Point(8, -11));
                        PL.Add(new Point(4, 7));
                        PL.Add(new Point(6, 3));
                        PL.Add(new Point(2, 9));
                        PL.Add(new Point(-10, 11));
                        PL.Add(new Point(-2, -1));
                        PL.Add(new Point(-10, -11));
                        PL.Add(new Point(-6, 1));
                        PL.Add(new Point(10, -3));
                        PL.Add(new Point(1, -1));
                        PL.Add(new Point(4, 5));
                        PL.Add(new Point(9, -9));
                        PL.Add(new Point(-2, 9));
                        PL.Add(new Point(3, -9));
                        PL.Add(new Point(7, -9));
                        PL.Add(new Point(7, -3));
                        PL.Add(new Point(-8, -5));
                        PL.Add(new Point(-8, 7));
                        PL.Add(new Point(-8, -9));
                        PL.Add(new Point(-7, 3));
                        PL.Add(new Point(10, -1));
                        PL.Add(new Point(2, 11));
                        PL.Add(new Point(-4, -9));
                        PL.Add(new Point(7, 1));
                        PL.Add(new Point(8, -7));
                        PL.Add(new Point(-7, 7));
                        PL.Add(new Point(-8, 5));
                        PL.Add(new Point(-10, 9));
                        PL.Add(new Point(3, -7));
                        PL.Add(new Point(9, -3));
                        PL.Add(new Point(1, 5));
                        PL.Add(new Point(-9, -9));
                        PL.Add(new Point(7, -7));
                        PL.Add(new Point(-2, -3));
                        PL.Add(new Point(2, -11));
                        PL.Add(new Point(4, -3));
                    }
                }
                return _MaskSkeletonOrder;
            }
        }

        public static void SaveMask(List<MaskListEntry> Cells, string FullPath, SVP_Registration_Params SVRP)
        {
            int MskImg_toChop = 0; //This used to be a setting, but now that we shift the masks, this has been removed 6/28/2022
            if (SVRP.MskImg_Final_Leica < 100) SVRP.MskImg_Final_Leica = 1024; //Just in case the settings got messed up

            Bitmap BMP = DrawMask(Cells, SVRP.MskImg_InCell_Width, SVRP.CropForLeica ? (SVRP.MskImg_Final_Leica + MskImg_toChop * 2) : SVRP.MskImg_Final_Leica, SVRP.Mask_PixelsExpand);
            if (SVRP.CropForLeica) BMP = BMP.Clone(new Rectangle(MskImg_toChop, MskImg_toChop, SVRP.MskImg_Final_Leica, SVRP.MskImg_Final_Leica), BMP.PixelFormat);
            if (SVRP.Rotate180) BMP.RotateFlip(RotateFlipType.Rotate180FlipNone);

            //Figure out how to save the mask
            int nGray = Cells.Count(x => MaskList.CheckStringAgainst_IntensityType(x.Intensity_Method, MaskIntensityType.Grayscale));
            if (nGray > 0)
                Save8BitTiff(BMP, FullPath);
            else
                Save1BitTiff(BMP, FullPath);
        }

        public static XmlDocument ExtractXmp()
        {
            //This may or may not work, untested as below . . probably don't need it now
            byte[] jpegBytes = null;
            var asString = System.Text.Encoding.UTF8.GetString(jpegBytes);
            var start = asString.IndexOf("<x:xmpmeta");
            var end = asString.IndexOf("</x:xmpmeta>") + 12;
            if (start == -1 || end == -1)
                return null;
            var justTheMeta = asString.Substring(start, end - start);
            var returnVal = new XmlDocument();
            returnVal.LoadXml(justTheMeta);
            return returnVal;
        }

        public static void Metadata()
        {


            //https://stackoverflow.com/questions/2280948/reading-data-metadata-from-jpeg-xmp-or-exif-in-c-sharp
            //string FullName = "";
            //using (FileStream fs = new FileStream(FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            //{
            //    BitmapSource img = BitmapFrame.Create(fs);
            //    BitmapMetadata md = (BitmapMetadata)img.Metadata;
            //    string date = md.DateTaken;
            //    Console.WriteLine(date);
            //    return date;
            //}
        }

        public static void Save1BitTiff(Bitmap img, string SavePath)
        {
            bool Make1Bit = true;
            if (Make1Bit)
            {
                ImageCodecInfo TiffCodec = null;
                foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                    if (codec.MimeType == "image/tiff")
                    {
                        TiffCodec = codec;
                        break;
                    }
                EncoderParameters parameters = new EncoderParameters(2);
                parameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, (long)1);
                parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionNone);
                img.Save(SavePath, TiffCodec, parameters);
            }
            else
            {
                img.Save(SavePath);
            }
            img.Dispose();
        }

        public static void Save8BitTiff(Bitmap img, string SavePath)
        {
            ImageCodecInfo TiffCodec = null;
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                if (codec.MimeType == "image/tiff")
                {
                    TiffCodec = codec;
                    break;
                }
            EncoderParameters parameters = new EncoderParameters(2);
            parameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, (long)8);
            parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionNone);
            img.Save(SavePath, TiffCodec, parameters);
            img.Dispose();
        }

        public static void SaveMasks(MaskList Masks, string directoryName, SVP_Registration_Params SRVP, System.ComponentModel.BackgroundWorker BW = null)
        {
            int Counter = 0; int Count = Masks.WellFieldDict.Count;
            foreach (KeyValuePair<string, List<MaskListEntry>> WellField in Masks.WellFieldDict)
            {
                SaveMask(WellField.Value, Path.Combine(directoryName, Path.GetFileNameWithoutExtension(WellField.Value[0].FileName) + ".TIF"), SRVP);
                if (BW != null)
                {
                    string t = Counter.ToString() + "/" + Count.ToString() + " = " + (100 * Counter / Count).ToString("0.0");
                    if (Counter++ % 10 == 0) BW.ReportProgress(0, t);
                }
            }
        }
    }

    public static class CellZoom
    {
        public static void ZoomTest()
        {
            Debugger.Break();
            string Folder = @"K:\RaftExport\FIV471A3_1\A\";
            DirectoryInfo DI = new DirectoryInfo(Folder);
            SizeF SizeSave = new SizeF(120, 120);
            Parallel.ForEach(DI.GetFiles("*.bmp"), file => { Zoom_Resave(file.FullName, "", 0, SizeSave); });
        }

        public static void Zoom_Resave(string SourceImage, string DestFullName = "", float BufferPix = 20, SizeF SaveSize = default(SizeF))
        {
            Bitmap BMP, BMP2;
            bool ExportSegImage = false;

            //Get the bitmap, Threshold, and segment the image to find the cells
            BMP = new Bitmap(SourceImage);
            Seg.SegObjects SOs = Zoom_Internal_SO(BMP);
            if (SOs.Count < 1 || SOs.Count > 2) return;

            //Make the destination if it doesn't exist
            if (DestFullName == "") DestFullName = DeriveNewDestImageName(SourceImage/*,  SOs.First().Area_Approx.ToString("00000") + "_"*/);

            if (ExportSegImage) { BMP2 = MasksHelper.DrawMask(SOs, BMP.Width, BMP.Width); BMP2.Save(SourceImage.Replace(".", ".Seg.")); }

            //Crop out the interesting area
            if (SaveSize.IsEmpty)
            {
                //Do any size based on the bounding box
                BMP2 = BMP.Clone(SOs.BoundingRect(BufferPix, BMP.Width, BMP.Height), BMP.PixelFormat);
            }
            else
            {
                //Do the central method and restrict the size
                RectangleF R = SOs.BoundingRect(BufferPix, BMP.Width, BMP.Height);
                float XDelta = -(R.Width - SaveSize.Width) / 2;
                float YDelta = -(R.Height - SaveSize.Height) / 2;
                float NewX = Math.Max(0, R.Left - XDelta);
                float NewY = Math.Max(0, R.Top - YDelta);
                if ((NewX + SaveSize.Width) > BMP.Width) NewX = BMP.Width - SaveSize.Width;
                if ((NewY + SaveSize.Height) > BMP.Height) NewY = BMP.Height - SaveSize.Height;
                RectangleF R2 = new RectangleF(new PointF(NewX, NewY), SaveSize);
                try
                {
                    BMP2 = BMP.Clone(R2, BMP.PixelFormat);
                }
                catch
                {
                    BMP2 = null;
                }
            }
            if (BMP2 != null) BMP2.Save(DestFullName);

            //Let go of the Bitmaps
            BMP.Dispose();
            if (BMP2 != null) BMP2.Dispose();
        }

        private static string DeriveNewDestImageName(string SourceImage, string Prefix = "")
        {
            string DestFullName;
            FileInfo FI = new FileInfo(SourceImage);
            DirectoryInfo DINew = new DirectoryInfo(Path.Combine(FI.Directory.Parent.FullName, "Zoomed"));
            if (!DINew.Exists) DINew.Create();
            DestFullName = Path.Combine(DINew.FullName, Prefix + FI.Name);
            return DestFullName;
        }

        private static Seg.SegObjects Zoom_Internal_SO(Bitmap BMP, float MinRatio = 0.3F, float MinNuclearArea = 400, float MaxNuclearArea = 20000)
        {
            Seg.ThreshImage TI = new Seg.ThreshImage(BMP);
            Seg.SegObjects SOs = Seg.SegObjects.ObjectsFromImage(TI);
            SOs.RemoveStickNuclei(MinRatio); //Get rid of some artifacts
            int sizeBefore = SOs.Count;
            SOs.RemoveNucleiBySize(MinNuclearArea, MaxNuclearArea); //Get rid of really small or really large nuclei
            if (SOs.Count < sizeBefore)
            {

            }
            return SOs;
        }
    }

    namespace Utilities
    {
        public class MathUtils
        {
            public static PointF GetAverage(PointF p1, PointF p2)
            {
                return new PointF((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2);
            }

            public static void LinearRegression(float[] xVals, float[] yVals, out double rSquared, out double yIntercept, out double slope)
            {
                double sumOfX = 0, sumOfY = 0, sumOfXSq = 0, sumOfYSq = 0, sumCodeviates = 0;
                int count = xVals.Length;

                // Compute all required sums in a single loop
                for (var i = 0; i < count; i++)
                {
                    var x = xVals[i];
                    var y = yVals[i];
                    sumOfX += x;
                    sumOfY += y;
                    sumOfXSq += x * x;
                    sumOfYSq += y * y;
                    sumCodeviates += x * y;
                }

                // Precompute the reciprocal of count
                double reciprocalCount = 1.0 / count;

                // Precompute means using the reciprocal
                double meanX = sumOfX * reciprocalCount;
                double meanY = sumOfY * reciprocalCount;

                // Precompute terms for covariance and variance
                double ssX = sumOfXSq - (sumOfX * meanX);       // Variance of X
                double ssY = sumOfYSq - (sumOfY * meanY);       // Variance of Y
                double covariance = sumCodeviates - meanY * sumOfX; // Covariance of X and Y

                // Precompute reciprocals
                double reciprocalSSX = 1.0 / ssX;

                // Compute slope and y-intercept
                slope = covariance * reciprocalSSX;
                yIntercept = meanY - slope * meanX;

                // Compute r-squared using precomputed reciprocals
                rSquared = covariance * covariance * reciprocalSSX / ssY;
            }
        }
        public class DrawingUtils
        {
            public static Brush RedBrush = new SolidBrush(Color.Red);

            public static Bitmap GetAdjustedImageSt(XDCE_Image xI, float Brightness, wvDisplayParam parameters = null)
            {
                if (xI == null) return null;
                ushort blackPoint = 10; ushort whitePoint = (ushort)(ushort.MaxValue / Brightness);
                if (!xI.Parent.ParentFolder.isNewFormat)
                    return GetAdjustedImageSt(xI.FullPath, xI, blackPoint, whitePoint, parameters);
                else
                {
                    string ImagePath = AdjustImagePath(xI);
                    return GetAdjustedImageSt(ImagePath, xI, blackPoint, whitePoint, parameters);
                }
                
            }

            public static Bitmap GetAdjustedImageSt(XDCE_Image xI, ushort blackPoint, ushort whitePoint, wvDisplayParam parameters = null)
            {
                if (xI == null) return null;
                if (!xI.Parent.ParentFolder.isNewFormat)
                    return GetAdjustedImageSt(xI.FullPath, xI, blackPoint, whitePoint, parameters);
                else
                {   
                    string ImagePath = AdjustImagePath(xI);
                    return GetAdjustedImageSt(ImagePath, xI, blackPoint, whitePoint, parameters);
                }
            }

            public static Bitmap GetAdjustedImageSt(string ImageFolder, XDCE_Image xI, float Brightness, wvDisplayParam parameters = null)
            {
                ushort blackPoint = 10; ushort whitePoint = (ushort)(ushort.MaxValue / Brightness);
                string ImagePath = AdjustImagePath(xI);
                return GetAdjustedImageSt(ImagePath, xI, blackPoint, whitePoint, parameters);
            }
            public static Bitmap GetAdjustedImageSt(string ImageFolder, XDCE_Image xI, ushort blackPoint, ushort whitePoint, wvDisplayParam parameters = null)
            {   //only Initialize a new Bitmap if something is actually wrong.
                Bitmap bmap;
                if (xI == null) return new Bitmap(10, 10);
                try
                {
                    ValueTuple<float, float, float>? BGR; 
                    if (parameters != null)
                    {
                        BGR = new ValueTuple<float, float, float>((float)parameters.c_B, (float)parameters.c_G, (float)parameters.c_R);
                        bmap = CoreLogic.PreProcessImage(ImageFolder, blackPoint, whitePoint, BGR).Item2;
                    }
                    else
                    {
                        bmap = CoreLogic.PreProcessImage(ImageFolder, blackPoint, whitePoint, null).Item2;
                    }  
                }
                catch
                {
                    Trace.WriteLine("Error: Unable to adjust image, FileName:" + xI.FileName);
                    bmap = new Bitmap(10, 10);
                }
                return bmap;
            }
            //Compile the Regex we might have to use it several thousand times.
            private static readonly Regex TimepointRegex = new Regex(@"^timepoint(\d+)$", RegexOptions.Compiled | RegexOptions.NonBacktracking);
            private static int ExtractTimepoint(string directoryName)
            {
                const string prefix = "timepoint";
                if (directoryName.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
                {
                    // Extract numeric part
                    string numericPart = new string(directoryName
                        .Skip(prefix.Length)         // Skip the "timepoint" part
                        .TakeWhile(char.IsDigit)    // Take while the characters are digits
                        .ToArray());                // Convert to string

                    // Convert directly to an integer
                    try
                    {
                        return int.Parse(numericPart);
                    }
                    catch(Exception ex)
                    {
                        Trace.WriteLine(ex.ToString()); 
                    }
                }
                return -1; // Return -1 or some sentinel value for invalid directories
            }
            private static Dictionary<int, string> GetTimepointMap(string parentDir)
            {
                if (_timepointCache.TryGetValue(parentDir, out var cachedMap))
                {
                    return cachedMap; // Return cached data if available
                }

                // Build the timepoint map if not already cached
                var timepointMap = Directory.GetDirectories(parentDir)
                    .Select(dir => (dir, timepoint: ExtractTimepoint(Path.GetFileName(dir))))
                    .Where(pair => pair.timepoint != -1)
                    .ToDictionary(
                        pair => pair.timepoint,
                        pair => pair.dir
                    );

                // Cache the result for future use
                _timepointCache[parentDir] = timepointMap;

                return timepointMap;
            }
            private static readonly Dictionary<string, Dictionary<int, string>> _timepointCache = new();
            private static object lockObj = new object();
            public static string AdjustImagePath(XDCE_Image image)
            {
                //We may have some Asynchronicity here. Since this function is fast Locking it has no downsides
                //and could save us from a difficult to track down bug or race condition.
                lock (lockObj)
                {
                    // Handle null image or empty path upfront
                    if (image == null || string.IsNullOrWhiteSpace(image.FullPath))
                    {
                        string FolderPath = Path.Combine(image.Parent.ParentFolder.FullPath + "\\timepoint0", image.FileName);
                        if (!Directory.Exists(FolderPath))
                        {
                            if (FolderPath.Contains("\\XDCE_info", StringComparison.OrdinalIgnoreCase))
                            {
                                FolderPath = FolderPath.Substring(0, FolderPath.IndexOf("\\XDCE_info", StringComparison.OrdinalIgnoreCase));

                                // Attempt to find "timepoint0" directory
                                string timepointDirectory = Directory
                                    .GetDirectories(FolderPath)
                                    .FirstOrDefault(d => d.Contains("timepoint0", StringComparison.OrdinalIgnoreCase));

                                if (timepointDirectory != null)
                                {
                                    image.Parent.FolderPath = timepointDirectory; // Update parent folder
                                    return Path.Combine(timepointDirectory, image.FileName);
                                }
                            }
                        }

                    }
  

                    // Cache the folder path to avoid multiple accesses
                    string folderPath = image.Parent.FolderPath;

                    // I have not been able to hunt down the origin of this or consistently reproduce it but 
                    // I have seen the XDCE_Info get added to the FolderPath. Correcting it here corrects the isssue
                    // before we start using the Image but this indicates some earlier logical error.
                    if (folderPath.Contains("\\XDCE_info", StringComparison.OrdinalIgnoreCase))
                    {
                        folderPath = folderPath.Substring(0, folderPath.IndexOf("\\XDCE_info", StringComparison.OrdinalIgnoreCase));

                        // Attempt to find "timepoint0" directory
                        string timepointDirectory = Directory
                            .GetDirectories(folderPath)
                            .FirstOrDefault(d => d.Contains("timepoint0", StringComparison.OrdinalIgnoreCase));

                        if (timepointDirectory != null)
                        {
                            image.Parent.FolderPath = timepointDirectory; // Update parent folder
                            return Path.Combine(timepointDirectory, image.FileName);
                        }
                    }

                    // If the image is not part of a time series, return the current path
                    if (!image.IsTimeSeries)
                    {
                        return Path.Combine(folderPath, image.FileName);
                    }

                    // Handle time series case
                    try
                    {
                        string currentDir = Path.GetDirectoryName(image.FullPath);
                        string parentDir = Directory.GetParent(currentDir)?.FullName;
                        if (string.IsNullOrWhiteSpace(parentDir))
                        {
                            throw new InvalidOperationException("Unable to determine the parent directory.");
                        }

                        // Build a timepoint mapping
                        Dictionary<int, string> timepointMap = GetTimepointMap(parentDir);

                        if (timepointMap.Count == 0)
                        {
                            throw new InvalidOperationException("No timepoint directories found.");
                        }

                        // Extract the time series order from the filename
                        string[] fileParts = image.FileName.Split('_');
                        if (fileParts.Length > 0 && int.TryParse(fileParts[0][1..], out int order))
                        {
                            image.TimeSeriesOrder = order;
                            image.IsTimeSeries = true;

                            if (timepointMap.TryGetValue(order - 1, out string matchingPath))
                            {
                                image.FullPath = Path.Combine(matchingPath, image.FileName);
                                return image.FullPath;
                            }
                            else
                            {
                                Trace.WriteLine($"Warning: No matching folder for TimeSeriesOrder {order}");
                                return $"Warning: No matching folder for TimeSeriesOrder {order}";
                            }
                        }

                        throw new InvalidOperationException($"Unable to extract TimeSeriesOrder from {image.FileName}");
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine($"Error: {ex.Message}");
                        return $"Error: {ex.Message}";
                    }
                }
            }
            public static Bitmap CombinedBMAPst(XDCE_Image xI, wvDisplayParams CurrentWVParams, XDCE_ImageGroup Welli = null, float RandomizeStrechMin = 1, string ColorCalcTypeSz = "Add")
            {
                List<Bitmap> bmaps = new List<Bitmap>();
                try
                {
                    wvDisplayParams wvPs = CurrentWVParams;
                    Bitmap bmap, BMTemp;
                    CoreLogic.ColorCalculationType ColorCalcType = ColorMatrix.GetColorCalcFromString(ColorCalcTypeSz);
                    if (Welli == null || xI.IsTimeSeries) 
                        Welli = xI.Parent.Wells[xI.WellLabel];

                    //First get the parameters that the user wants and load all the bitmaps
                    foreach (wvDisplayParam wvP in wvPs.Actives)
                    {
                        XDCE_Image xi = Welli.GetField(xI.FOV, wvP.wvIdx, xI.TimeSeriesOrder);
                        float bright = RandomizeStrechMin >=1 ? wvP.Brightness : RandomStretch(wvP.Brightness, RandomizeStrechMin);

                        if (wvP.Brightness_Low == 0)
                        {
                            BMTemp = GetAdjustedImageSt(xi.FullPath, xi, bright, wvP);
                        }
                        else
                        {
                            ushort blackPoint = (ushort)(wvP.Brightness_Low);
                            ushort whitePoint = (ushort)(ushort.MaxValue/(float)bright);
                            BMTemp = GetAdjustedImageSt(xi, blackPoint, whitePoint, wvP);
                        }
                        
                        if (BMTemp == null)
                        {
                            DisposeBitmaps(bmaps, null);
                            return new Bitmap(10, 10);
                        }

                        bmaps.Add(BMTemp);
                    }

                    //Multichannel stuff
                    if (bmaps.Count == 0)
                        return new Bitmap(10, 10);
                    bmap = bmaps[0];
                    for (int i = 1; i < bmaps.Count; i++) //Note we start at 1 not 0
                    {
                        if (bmaps[i] == null)
                        {
                            DisposeBitmaps(bmaps, bmap);
                            return new Bitmap(10, 10);
                        }
                        try
                        {
                            bmap = CoreLogic.ArithmeticBlend(bmaps[i], bmap, ColorCalcType);
                        }
                        catch(Exception ex)
                        {
                            Trace.WriteLine(ex.Message);
                        }
                    
                    }

                    //Clip based on Threshold - - - - - -- - - - 
                    bmap = ClipChannels(xI, bmap, CurrentWVParams, Welli);

                    //Dispose the bitmaps we aren't using
                    DisposeBitmaps(bmaps, bmap);
                    return bmap;
                }
                catch
                {
                    // Dispose all bitmaps in case of an exception
                    DisposeBitmaps(bmaps, null);
                    return new Bitmap(10, 10);
                }

                void DisposeBitmaps(List<Bitmap> bitmaps, Bitmap currentBitmap)
                {
                    foreach (var bitmap in bitmaps)
                        if (bitmap != currentBitmap) bitmap.Dispose();
                }
            }


            public static Bitmap ClipChannels(XDCE_Image xI, Bitmap bmap, wvDisplayParams CurrentWVParams, XDCE_ImageGroup CurrentWell)
            {
                if (CurrentWVParams.UseClipping)
                {
                    //First do the positive direction
                    //Only show VIS part of it where the OBJ part of it is checked
                    wvDisplayParam wvP = null;
                    foreach (var wvPt in CurrentWVParams)
                        if (wvPt.ObjectAnalysis) { wvP = wvPt; break; }
                    if (wvP != null)
                    {
                        var BMTemp = DrawingUtils.GetAdjustedImageSt(CurrentWell.GetField(xI.FOV, wvP.wvIdx), wvP.Brightness, wvP);
                        var Thresh = new Seg.ThreshImage(BMTemp, (int)wvP.Threshold, true, 1);
                        BMTemp = Thresh.GetPostThreshBitmap(false, CurrentWVParams.ClippingExpand_positive);
                        bmap = CoreLogic.ArithmeticBlend(bmap, BMTemp, CoreLogic.ColorCalculationType.Multiply);
                    }

                    //Now do the negative direction (multiply by the inverse mask)
                    wvP = null;
                    foreach (var wvPt in CurrentWVParams)
                        if (wvPt.DI_Analysis) { wvP = wvPt; break; }
                    if (wvP != null)
                    {
                        var BMTemp = DrawingUtils.GetAdjustedImageSt(CurrentWell.GetField(xI.FOV, wvP.wvIdx), wvP.Brightness, wvP);
                        var Thresh = new Seg.ThreshImage(BMTemp, (int)wvP.Threshold, true, 1);
                        BMTemp = Thresh.GetPostThreshBitmap(true, CurrentWVParams.ClippingExpand_negative);
                        bmap = CoreLogic.ArithmeticBlend(bmap, BMTemp, CoreLogic.ColorCalculationType.Multiply);
                    }
                }

                return bmap;
            }

            private static Random _Rand = new Random();

            private static float RandomStretch(float brightness, float randomizeStrechMin)
            {
                float min = randomizeStrechMin;
                return (float)(brightness * (min + _Rand.NextDouble() * (1 - min)));
            }
        }

        public class ColorMatrix
        {
            //For small Enums this is actually faster than a dictionary and uses no memory.
            public static CoreLogic.ColorCalculationType GetColorCalcFromString(string CCType) { Enum.TryParse(CCType, true, out CoreLogic.ColorCalculationType result); return result; }
            //Keeping this in Case it needs updated out side of the DLL.
            public enum ColorCalculationType : byte
            {
                Amplitude = 0,
                Average = 1,
                Add = 2,
                SubtractLeft = 4,
                SubtractRight = 8,
                Difference = 16,
                Multiply = 32,
                Min = 64,
                Max =128,
                
            }
        }
    }
}
