using FIVE;
using FIVE.FOVtoRaftID;
using FIVE.ImageCheck;
using FIVE.InCellLibrary;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FIVE_IMG.Seg
{
    public static class Segmentation
    {
        public static ImageQueue ImgQueue;

        public static SegResults RunSegmentation(bool UseRafts, DateTime Start, ConcurrentDictionary<string, XDCE_ImageGroup> ImageParts, CropImageSettings CropSettings, INCELL_WV_Notes wvNotes, ImageQueue ImgQueuePass = null, System.ComponentModel.BackgroundWorker BW = null)
        {
            if (BW != null) BW.ReportProgress(0, "Go!");
            ImgQueue = ImgQueuePass == null?new ImageQueue(): ImgQueuePass;

            //We could use the bool return to break out of the loop early if needed
            bool WithinLoop(SegResults Res, KeyValuePair<string, XDCE_ImageGroup> KVP)
            {
                // Use this if you want to skip wells, but I would rather just export all
                //if (CropSettings != null) if (!CropSettings.WellDict.ContainsKey(KVP.Value.Images.First().WellLabel)) continue;

                Segmentation_Parts(KVP, Res, wvNotes, UseRafts, CropSettings);
                Res.ProcessedCountIncrement();

                if (Res.ProcessedCount % (UseRafts ? 15 : 1) == 0)
                    if (BW != null) BW.ReportProgress(0, KVP.Key + " " + Res.ProcessedPercent + ", " +
                          ((DateTime.Now - Start).TotalSeconds / Res.ProcessedCount).ToString("0.0") + " s / image." );
                //if (Bag.Count > MaxToProcess) break;
                return true;
            }

            var ResA = new FIVE_IMG.Seg.SegResults(ImageParts.Count()); //This will keep the Object-and per-Image results straight
            CropSettings.ParallelizeSegmentation = false;
            //CropSettings.SaveFolder = @"E:\Temp\RaftExport\FIV574C";
            if (CropSettings.ParallelizeSegmentation)
            {
                Parallel.ForEach(ImageParts, (KVP, state) =>  //This is also untested - probably will break with rafts because of not having them in the same image
                {   //7/22 This is breaking since we need to have each thread on different BMPs
                    WithinLoop(ResA, KVP);
                    if (BW != null) if (BW.CancellationPending) state.Break();
                });
            }
            else
            {
                var wellKeys = ImageParts.First().Value.ParentFolder.XDCE.Wells.Keys;
                
                // Create a lookup dictionary for well keys and their indices
                var wellKeyIndices = wellKeys.Select((key, index) => new { key, index })
                                             .ToDictionary(x => x.key, x => x.index);

                var IP = ImageParts
                    .OrderBy(x => wellKeyIndices.ContainsKey(x.Value.Images[0].WellLabel) ? wellKeyIndices[x.Value.Images[0].WellLabel] : int.MaxValue)
                    .ThenBy(x => x.Value.Images[0].FOV);

                //var IP = ImageParts
                //    .OrderBy(x => x.Value.Images[0].Well_Row)
                //    .ThenBy(x => x.Value.Images[0].Well_Column)
                //    .ThenBy(x => x.Value.Images[0].FOV);

                var PreviousKVP = IP.First();
                foreach (var KVP in IP)
                {
                    //previous well is different, save out these regions
                    if (CropSettings.OnlyForRegionAssignment && KVP.Value.Images[0].WellLabel != PreviousKVP.Value.Images[0].WellLabel)
                        PreviousKVP.Value.SaveWellRegions();

                    WithinLoop(ResA, KVP); PreviousKVP = KVP;
                    if (BW != null) if (BW.CancellationPending) break;
                }
                PreviousKVP.Value.SaveWellRegions();
            }
            return ResA;
        }

        private static void Segmentation_Parts(KeyValuePair<string, XDCE_ImageGroup> RaftOrWell, SegResults Results, INCELL_WV_Notes wvNotes, bool Rafts = false, CropImageSettings Settings = null)
        {
            XDCE_ImageGroup Imgs = RaftOrWell.Value; XDCE_Image xI;
            if (Settings.OnlyForRegionAssignment)
            {
                int bin = 4;
                var wvParam = wvNotes.DisplayParams.Tracings.First();
                xI = Imgs.Images[wvParam.wvIdx];
                if (xI.Rafts.Count > 0) return; //already has them
                var Res = BasicSegmentatioResults.BasicSegmentation(Settings, wvNotes.DisplayParams, xI, ImgQueue, bin, false, false, false);

                var tRafts = xI.Rafts;
                foreach (var Region in Res.DeOverlap) Region.AddasRaftInfo(tRafts, xI, Settings.CropSize, bin);

                //Now assign back the RaftList
                return;
            }

            bool First = true;
            //Object ID Part ---------------------------
            foreach (var wvParam in wvNotes.DisplayParams.Tracings)
            {
                INCELL_WV_Note Nt = wvNotes.List[wvParam.wvIdx];
                Segmentation_Part(Results, Imgs.Images[wvParam.wvIdx], wvParam.Brightness, wvNotes,
                    (int)Nt.Threshold_Value, Nt.QuickName, 2, true, (Rafts ? RaftOrWell.Key : ""), Settings);
                First = false; //Only need to export the cropped images once // 7/2023 need to re-implement that . . 
            }

            //Degeneration Index Part  ---------------------------

            var pre = wvNotes.DisplayParams.List.Where(x => x.DI_Analysis);
            if (pre.Count() == 0) return;
            var di = wvNotes.DisplayParams[0];
            xI = Imgs.Images[di.wvIdx];
            var bmpPreThresh = Utilities.DrawingUtils.GetAdjustedImageSt(xI.Parent.FolderPath, xI, di.Brightness);
            var result = BasicSegmentatioResults.DegenerationIndex(Settings, di.Threshold, xI, bmpPreThresh, null, 2);
            Results.AppendDI(xI, "", wvNotes.List[di.wvIdx].ToString(), result);
        }

        public static Random Rand = new Random();

        private static void Segmentation_Part(SegResults Results, XDCE_Image xI, float brightness, INCELL_WV_Notes wvNotes, int Threshold, string ChannelName, int ThreshBinning = 2, bool CountObjects = true, string RaftID = "", CropImageSettings Settings = null)
        {
            //Old Way . . 
            Bitmap bmap_thresh = Utilities.DrawingUtils.GetAdjustedImageSt(xI.Parent.FolderPath, xI, brightness);
            Bitmap bmap_export = Utilities.DrawingUtils.CombinedBMAPst(xI, wvNotes.DisplayParams, null, Settings.RandomizeImageIntensity_Min);
            //Bitmap bmap2 = wvNotes.DisplayParams.Actives.Count == 1 & wvNotes.DisplayParams.Actives[0].wvIdx == xI.wavelength ?
            //    bmap : Utilities.DrawingUtils.CombinedBMAPst(xI, wvNotes.DisplayParams, null, Settings.RandomizeImageIntensity_Min);
            bool UseRafts = RaftID != "";
            UseRafts = false; //20211122 - need to fix
            if (UseRafts)
            {
                FIVE.FOVtoRaftID.ReturnRaft RR = xI.Parent.Wells[xI.WellLabel].CalibrationRaftSettings.FindRaftID_RR(xI, RaftID);
                RectangleF r = FIVE.FOVtoRaftID.ReturnRaft.RectFromCorners(xI, RR, bmap_thresh.Size);
                if (r.Width < 1 || r.Height < 1 || r.Top < 1 || r.Left < 1) return;
                bmap_thresh = bmap_thresh.Clone(r, bmap_thresh.PixelFormat);
            }

            var Thresh = new ThreshImage(bmap_thresh, Threshold, CountObjects, ThreshBinning);
            if (CountObjects)
            {
                var Objs = SegObjects.ObjectsFromImage(Thresh);
                foreach (var obj in Objs)
                {
                    obj.CropSavePath = Settings.GetSavePath(xI, obj); //Could make this a property, but for now it is saved manually - must be BEFORE AppendObject or won't get saved
                    if (Settings.StoreResults) Results.AppendObject(xI, RaftID, ChannelName, obj);

                    if (!Settings.SaveCroppedImages) continue;
                    if (obj.m_Area_Pixels > Settings.MinAreaPixels && obj.m_Area_Pixels < Settings.MaxAreaPixels)
                    {
                        //Export each of these images around the objects - - account for binning
                        SaveorExecOnSingleCell(Settings, bmap_export, obj, ThreshBinning);
                    }
                }
            }
            else
            {
                //No longer implemented as of 7/26/2021  //Prefix(); //sB.Append(Header ? "ThreshPixels" : Thresh.Pixels.ToString()); sB.Append(delim); //sB.Append("\r\n");
            }
            bmap_thresh.Dispose();
            bmap_export.Dispose();
        }

        private static void SaveorExecOnSingleCell(CropImageSettings Settings, Bitmap WholeField, SegObject obj, float ThreshBinning)
        {
            bool ShowTestPoint = false;
            Bitmap crop1, final; RectangleF cropRect; PointF posInFull;
            Settings.GetRectangle(obj, WholeField.Size, ThreshBinning, out cropRect, out posInFull);
            try
            {
                if (ShowTestPoint)
                {
                    var STP = new Bitmap(WholeField);
                    using (var g = Graphics.FromImage(STP))
                    {
                        g.DrawRectangle(Pens.Yellow, Rectangle.Round(cropRect));
                    }
                    STP.Save(obj.CropSavePath + "T.jpg");
                }
                //Chop out the region the cell is in
                crop1 = WholeField.Clone(cropRect, PixelFormat.Format24bppRgb);
                //Add blank area around the cell to make it the correct final dimensions
                final = new Bitmap(Settings.CropSize.Width, Settings.CropSize.Height);
                using (var g = Graphics.FromImage(final))
                {
                    g.Clear(Color.Black); //Could make this the avg Bacgkround color
                    g.DrawImage(crop1, posInFull);
                }
                if (Settings.RunMLProcessing == "PL")
                {
                    obj.PLResults = PL.PLDeployModule.PredictOnImage(final, Settings.MLSettings).numericRes;
                }
                else
                {
                    final.Save(obj.CropSavePath);
                }
                crop1.Dispose();
                final.Dispose();
            }
            catch { }
        }
    }

    public static class DegenerationIndex
    {
        //DEprecated . . use
        //public static BasicSegmentatioResults DegenerationIndex
        //instead
    }

#if (TRUE) //Classes vs. Structs for Linked Pix
    //As of 11/28, Classes are faster on Willie's computer, but there could be a trick with references that make the other form faster

public class LinkedPix
{
    public Point P;

    public override string ToString()
    {
        return P.ToString() + " " + Links.Count;
    }

    public HashSet<LinkedPix> Links;
    public int Links_Permanent;
 
    public void Add(LinkedPix P)
    {
        Links.Add(P);
        Links_Permanent++;
    }

    public SegObject MemberOfObject { get; set; }
    public bool IsMemberOfObject { get => MemberOfObject != null; }

    private void Init()
    {
        Links_Permanent = 0;
        MemberOfObject = null;
        Links = new HashSet<LinkedPix>();
    }

    public LinkedPix(int x, int y)
    {
        Init();
        P = new Point(x, y);
    }

    public LinkedPix(Point p)
    {
        Init();
        P = p;
    }
  
    public LinkedPix RotateCopy(double theta_radians) 
    {
        //only Access Members once, only call Sin and Cos once.
        double tR = Math.Cos(theta_radians);
        double tS = Math.Sin(theta_radians);
        int x = P.X;
        int y = P.Y;
        LinkedPix LP2 = new LinkedPix(new Point((int)(x * tR - y * tS), (int)(y * tR + x * tS)));
        return LP2;
    }

      public void LinksRemoveTwoWay(LinkedPix ToRemove)
     {
       //Remove from each others
       ToRemove.Links.Remove(this);
       Links.Remove(ToRemove);
     }

    /// <summary>
    /// Removes this from each of its links and removes these links
    /// </summary>
        internal void LinksCrossRemove()
        {
            //this is O.K. because a pixel can't be linked to itself.
            foreach (LinkedPix tPix in Links)
            {
                // Remove 'thisPix' from the links of connected pixels
                tPix.Links.Remove(this);
            }
            //Clear Existing set instead of instantiating new one.
            Links.Clear();
        }

    public bool IsBoundaryPixel()
    {
        return Links_Permanent < 4;
    }

}

#else


    public struct LinkedPix
    {
        public Point P;

        public override string ToString()
        {
            return P.ToString() + " " + Links.Count;
        }

        public HashSet<LinkedPix> Links;
        public SegObject MemberOfObject { get; set; }
        public bool IsMemberOfObject { get => MemberOfObject != null; }

        private void Init()
        {
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = new Point();
        }

        public LinkedPix(bool T)
        {
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = new Point();
        }

        //public LinkedPix(int x, int y)
        //{
        //    Init();
        //    P = new Point(x, y);
        //}

        public LinkedPix(Point p)
        {
            //Init();
            MemberOfObject = null;
            Links = new HashSet<LinkedPix>();
            P = p;
        }

        public LinkedPix RotateCopy(double theta_radians)
        {
            //Assumes that 0,0 is the origin
            var LP2 = new LinkedPix(
            //LP2.P =
                new Point(
                (int)(P.X * Math.Cos(theta_radians) - P.Y * Math.Sin(theta_radians)),
                (int)(P.Y * Math.Cos(theta_radians) + P.X * Math.Sin(theta_radians))));
            return LP2;
        }

        public void LinksRemoveTwoWay(LinkedPix ToRemove, bool TwoWay = false)
        {
            //Remove from each others
            ToRemove.Links.Remove(this);
            Links.Remove(ToRemove);
        }

        /// <summary>
        /// Removes this from each of its links and removes these links
        /// </summary>
        internal void LinksCrossRemove()
        {

            foreach (LinkedPix tPix in Links)
            {
                // Remove 'thisPix' from the links of connected pixels
                tPix.Links.Remove(this);
            }
            //Clear Existing set instead of instantiating new one.
            Links.Clear();
        }
    }


#endif

    public class LinkedPixSet : IEnumerable<LinkedPix>
    {
        private HashSet<LinkedPix> _Set;

        public LinkedPixSet()
        {
            _Set = new HashSet<LinkedPix>();
        }
        public void Add(LinkedPix Pix)
        {
            _Set.Add(Pix);
        }
        public void Create(IEnumerable<LinkedPix> values)
        {
            _Set = new HashSet<LinkedPix>(values);
        }
        public void Remove(LinkedPix Pix)
        {
            _Set.Remove(Pix);
        }
        public void ExceptWith(HashSet<LinkedPix> Pix)
        {
            _Set.ExceptWith(Pix);
        }
        public void UnionWith(HashSet<LinkedPix> Pix)
        {
            _Set.UnionWith(Pix);
        }
        public IEnumerator<LinkedPix> GetEnumerator()
        {
            return _Set.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Set.GetEnumerator();
        }

        public int Count { get => _Set.Count; }
    }

    public class ThreshImage : LinkedPixSet
    {
        Dictionary<Point, LinkedPix> PointDict;
        public static double DefaultThreshold_AboveMean = 4;
        public static int Slice_ModulusFactor = 12;
        public float BinningUsed = 1;

        public Size Size;

        public Bitmap GetPostThreshBitmap(bool Invert = false, int Expand = 0)
        {
            var C_Thresh = Invert ? Color.Black : Color.White;
            var C_Bground = Invert ? Color.White : Color.Black;
            var B = new SolidBrush(C_Thresh);
            var tB = new Bitmap(Size.Width, Size.Height);

            //Expand = 0; //COmmented out 4/1/2024
            using (Graphics g = Graphics.FromImage(tB))
            {
                g.Clear(C_Bground);
                int iterator = 0;
                var rectangles = new Rectangle[PointDict.Count];
                int ExpandSize = 1 + (Expand * 2);
                 
                foreach (KeyValuePair<Point, LinkedPix> pt in PointDict)
                {
                    rectangles[iterator] = new Rectangle(pt.Key.X - Expand, pt.Key.Y - Expand, ExpandSize, ExpandSize);
                    iterator++;
                }
                g.FillRectangles(B, rectangles);
            }
            return tB;
        }

        private Bitmap _PostThreshBitMap;
        public Bitmap PostThreshBitmap
        {
            get
            {
                if (_PostThreshBitMap == null)
                {
                    _PostThreshBitMap = GetPostThreshBitmap();
                }
                return _PostThreshBitMap;
            }
        }

        //Keeps the LinkedPix that belong to the thresholded part of the image
        private void Init()
        {
            PointDict = new Dictionary<Point, LinkedPix>();
        }

        public ThreshImage()
        {
            Init();
        }

        public ThreshImage(Bitmap bmp, float threshold = -1, bool CreateLinks = true, float BinBitMap = 4, bool Slice = false)
        {
            if (BinBitMap > 1)
            {
                Size tSize = new Size((int)(bmp.Width / BinBitMap), (int)(bmp.Height / BinBitMap));
                bmp = new Bitmap(bmp, tSize);
            }
            BinningUsed = BinBitMap;
            CreateFrom(bmp, threshold, CreateLinks, Slice);
        }

        public ThreshImage(XDCE_Image Img, float brightness, float Threshold, float BinBitMap, bool CreateLinks = true)
        {
            Bitmap bmp = FIVE_IMG.Utilities.DrawingUtils.GetAdjustedImageSt(Img.FullPath, Img, brightness); //217 208 225 ms
            Size tSize = new Size((int)(bmp.Width / BinBitMap), (int)(bmp.Height / BinBitMap));
            bmp = new Bitmap(bmp, tSize); //25 27 28 ms
            BinningUsed = BinBitMap;
            CreateFrom(bmp, Threshold, CreateLinks);
        }

        //  - this is the only function that currently references ImgWrap outside of SharedUtils . . 
        //JOSH 4/3/2024 Sped up this function by declaring re-usable variables.
            private void CreateFrom(Bitmap BMP, float threshold = -1, bool CreateLinks = true, bool Slice = false, bool RemoveIsolatedPixels = true)
        {
            Init();
            bool AutoThreshold = (threshold == -1);
            if (BMP == null) return;
            Size = BMP.Size; 
            bool anyPixels = false;
            var imWrap = new ImgWrap(BMP);
            double mean_multiplier = 1;
            //declare these variables for later.
            int heightminusOne = imWrap.Height - 1;
            int widthminusOne = imWrap.Width - 1;
            int height = imWrap.Height;
            byte[,] arr = imWrap.Arr;
            
        TryAgainNewThreshold:
            if (AutoThreshold)
            {
                double mean = imWrap.RedMean; //79 143 69 ms
                mean_multiplier = DefaultThreshold_AboveMean;
                threshold = (int)(mean * mean_multiplier);
            }

            //Just start at 0,0 and make links with pixels that are in 2 dimensions also positive (back link them to the start) and add just those positives to this set
            void EstablishLinks(int xi, int yi, LinkedPix Pixi)
            {
                if (arr[xi, yi] > threshold)
                {
                    Point pi = new Point(xi, yi);
                    LinkedPix Pix2 = GetOrAdd(pi);
                    Pixi.Add(Pix2);
                    Pix2.Add(Pixi);
                }
            }

            LinkedPix Pix; Point p;
            for (int y = 0; y < heightminusOne; y++)
            {
                if (Slice && y % Slice_ModulusFactor < 1) continue; //This is for Axon Degeneration to break up the axons
                for (int x = 0; x < widthminusOne; x++)
                {
                    if (Slice && x % Slice_ModulusFactor < 1) continue;
                    if (arr[x, y] > threshold)
                    {
                        anyPixels = true;
                        if (CreateLinks)
                        {
                            p = new Point(x, y);
                            Pix = GetOrAdd(p);
                            //this saves several unneccessary additions.
                            int xplus1 = x + 1;
                            int yplus1 = y + 1;
                            if (yplus1 < height) EstablishLinks(x, yplus1, Pix);
                            if (xplus1 < height) EstablishLinks(xplus1, y, Pix);
                        }
                    }
                }
            }
            //remove single Pixel links since these are artifacts and don't pass the size test anyway.
            if (RemoveIsolatedPixels)
            {
                foreach (var key in PointDict.Keys.ToList()) // Use ToList() to avoid modifying the collection during iteration
                {
                    if (PointDict[key].Links.Count == 0)
                    {
                        PointDict.Remove(key);
                    }
                }
            }

            Create(PointDict.Values);
            if (anyPixels == false && AutoThreshold)
            {
                mean_multiplier /= 2; threshold = 0;
                goto TryAgainNewThreshold;
            }
        }

        public LinkedPix GetOrAdd(Point p)
        {
            if (PointDict.ContainsKey(p))
            {
                return PointDict[p];
            }
            else
            {
                var LP = new LinkedPix(p);
                PointDict.Add(p, LP);
                return LP;
            }
        }
    }

    public class SegObject : LinkedPixSet
    {
        public SegObject()
        {

        }

        public override string ToString()
        {
            return Centroid.ToString() + " " + m_Area_Pixels.ToString("0.0") + " area";
        }

        private void RegenMajorMinorAxis()
        {
            BoundingRectangle(this, out _Centroid, out _Rect);
            _Size = _Rect.Size;
            (_MinorMajorAxisRatio, _MajorAxisAngle_Radians) = Calculate_MinorMajorAxisRatioAndAngle();
        }

        public static void BoundingRectangle(IEnumerable<LinkedPix> pixels, out PointF center, out RectangleF rect, int count = 0)
        {
            float Xmin = float.MaxValue;
            float Xmax = float.MinValue;
            float Ymin = float.MaxValue;
            float Ymax = float.MinValue;
            float totalX = 0;
            float totalY = 0;
            int counter = count == 0 ? pixels.Count() : count;

            foreach (LinkedPix pix in pixels)
            {
              
                float x = pix.P.X;
                float y = pix.P.Y;

                totalX += x;
                totalY += y;

                if (x < Xmin) Xmin = x;
                if (x > Xmax) Xmax = x;
                if (y < Ymin) Ymin = y;
                if (y > Ymax) Ymax = y;
            }

            center = new PointF(totalX / counter, totalY / counter);
            SizeF size = new SizeF(Xmax - Xmin, Ymax - Ymin);
            rect = new RectangleF(new PointF(Xmin, Ymin), size);
        }

        private List<LinkedPix> _boundaryPixels = null;

        public List<LinkedPix> BoundaryPixels
        {
            get
            {
                if (_boundaryPixels == null)
                {
                    _boundaryPixels = new();
                    foreach (LinkedPix pix in this)
                        if (pix.IsBoundaryPixel())
                            _boundaryPixels.Add(pix);
                }
                return _boundaryPixels;
            }
        }

        private double? _feretsDiameter = null;
        public double m_FeretsDiameter
        {
            get
            {
                if (_feretsDiameter == null)
                {
                    double maxDist = 0;
                    for (int i = 0; i < BoundaryPixels.Count - 1; i++)
                    {
                        for (int j = i + 1; j < BoundaryPixels.Count; j++)
                        {
                            double dist = Math.Sqrt(Math.Pow(BoundaryPixels[j].P.X - BoundaryPixels[i].P.X, 2) + Math.Pow(BoundaryPixels[j].P.Y - BoundaryPixels[i].P.Y, 2));
                            if (dist > maxDist)
                            {
                                maxDist = dist;
                            }
                        }
                    }
                    _feretsDiameter = maxDist;
                }
                return _feretsDiameter.Value;
            }
        }

        private double? _compactness = null;
        public double m_Compactness
        {
            get
            {
                if (_compactness == null)
                {
                    double radius = m_FeretsDiameter / 2;
                    _compactness = m_Area_Pixels / (Math.PI * Math.Pow(radius, 2));
                }
                return _compactness.Value;
            }
        }

        private double? _perimeterDensity = null;
        public double m_PerimeterDensity
        {
            get
            {
                if (_perimeterDensity == null)
                {
                    _perimeterDensity = (double)BoundaryPixels.Count / m_Area_Pixels;
                }
                return _perimeterDensity.Value;
            }
        }


     private double Calculate_MinorMajorAxisRatio_Old()
   {
       double Angle_Divisor = 11; //Divide 180 this many times to check the Major and Minor Axes

       List<LinkedPix> LPL; SizeF SZ; PointF PT; RectangleF ReturnRect;
       SortedList<double, double> Ratios = new SortedList<double, double>();
       for (double theta = 0; theta < Math.PI; theta += Math.PI / Angle_Divisor)
       {
           LPL = new List<LinkedPix>();
           foreach (LinkedPix pix in this)
               LPL.Add(pix.RotateCopy(theta));
           BoundingRectangle(LPL, out PT, out ReturnRect, LPL.Count);
           SZ = ReturnRect.Size;
           double Ratio = (SZ.Width < SZ.Height ? SZ.Width / SZ.Height : SZ.Height / SZ.Width);
           if (!Ratios.ContainsKey(Ratio)) Ratios.Add(Ratio, 0);
           Ratios[Ratio]++;
       }
       return Ratios.Keys[0];
   }

        private (double MinRatio, double MajorAxisAngle) Calculate_MinorMajorAxisRatioAndAngle()
        {
            double Angle_Divisor = 11; //Divide 90 this many times to check the Major and Minor Axes

            List<LinkedPix> LPL; SizeF SZ; RectangleF ReturnRect;
            double minRatio = double.MaxValue;
            double minRatioAngle = 0;
            double angleRange = Math.PI / 2;

            for (double theta = 0; theta < angleRange; theta += angleRange / Angle_Divisor)
            {
                LPL = new List<LinkedPix>();
                foreach (LinkedPix pix in this)
                    LPL.Add(pix.RotateCopy(theta));
                BoundingRectangle(LPL, out _, out ReturnRect, LPL.Count);
                SZ = ReturnRect.Size;
                double Ratio = (SZ.Width < SZ.Height ? SZ.Width / SZ.Height : SZ.Height / SZ.Width);
                if (Ratio < minRatio)
                {
                    minRatio = Ratio;
                    // If width is greater, the current angle is the angle of the major axis.
                    // If height is greater, the major axis is perpendicular to the current angle.
                    minRatioAngle = SZ.Width > SZ.Height ? theta : theta + Math.PI / 2;
                }
            }
            // Adjust the angle to the range [0, 2*PI)
            minRatioAngle = minRatioAngle % (2 * Math.PI);
            return (minRatio, minRatioAngle);
        }

        public RectangleF CropRect(float PixelWidthCrop, float Bin = 1, int MaxWidth = int.MaxValue)
        {
            float Full = Bin * PixelWidthCrop;
            float Half = Full / 2;
            float X1 = Center_X * Bin - Half; X1 = Math.Max(0, X1);
            float X2 = Center_X * Bin + Half; X2 = Math.Min(MaxWidth, X2);
            float Y1 = Center_Y * Bin - Half; Y1 = Math.Max(0, Y1);
            float Y2 = Center_Y * Bin + Half; Y2 = Math.Min(MaxWidth, Y2);
            return new RectangleF(X1, Y1, X2 - X1, Y2 - Y1);
        }

        internal ImageCheck_Point ToICP(float CropSize = -1, float Bin = 1)
        {
            var ICP = new ImageCheck_Point();
            ICP.PlateID = this.PlateID;
            ICP.Well_Row = this.WellLabel.Substring(0, 1);
            ICP.Well_Col = this.WellLabel.Substring(4);
            ICP.FOV = this.FOV;
            ICP.RaftID = "nr" + (Center_X * Bin).ToString("0000") + "," + (Center_Y * Bin).ToString("0000") + "," + CropSize;
            return ICP;
        }

        public string SetExportName(string Extension)
        {
            ExportName = PlateID + "." + WellLabel + "." + FOV + ".(" + RegionName + ").jpg";
            return ExportName;
        }

        private static Dictionary<string, int> RaftIDTrackDict = new();

        public void AddasRaftInfo(Dictionary<string, xI_RaftInfo> tRafts, XDCE_Image xI, Size OriginalCropSize, int bin = 1)
        {
            //First get the Index to use for this raft
            if (!RaftIDTrackDict.ContainsKey(xI.PlateID)) RaftIDTrackDict.Add(xI.PlateID, 0);

            //Now assign a new RaftID and add to the raft list
            string RaftID = RaftInfoS.RaftID_FromIdx(RaftIDTrackDict[xI.PlateID]++);

            var x = xI.PlateX_um + (bin * xI.Width_microns * this.Center_X / xI.Width_Pixels);
            var y = xI.PlateY_um + (bin * xI.Height_microns * this.Center_Y / xI.Height_Pixels);

            var UR = new UnknownRaft(x, y);
            var RR = new ReturnRaft(UR);
            RR.RaftID = RaftID;
            RR.XOnRaft = x; RR.YOnRaft = y; RR.MinDistToEdge = xI.Width_microns * OriginalCropSize.Width / (2 * xI.Width_Pixels);
            double left = bin * this.Center_X - OriginalCropSize.Width / 2;
            double right = bin * this.Center_X + OriginalCropSize.Width / 2;
            double top = bin * this.Center_Y - OriginalCropSize.Height / 2;
            double bottom = bin * this.Center_Y + OriginalCropSize.Height / 2;
            RR.RaftCorners = new List<Tuple<double, double>>() {
                new Tuple<double, double>(xI.PlateX_um + xI.Width_microns * left / xI.Width_Pixels,xI.PlateY_um + xI.Height_microns * top / xI.Height_Pixels),
                new Tuple<double, double>(xI.PlateX_um + xI.Width_microns * right / xI.Width_Pixels, xI.PlateY_um + xI.Height_microns * top / xI.Height_Pixels),
                new Tuple<double, double>(xI.PlateX_um + xI.Width_microns * right / xI.Width_Pixels, xI.PlateY_um + xI.Height_microns * bottom / xI.Height_Pixels),
                new Tuple<double, double>(xI.PlateX_um + xI.Width_microns * left / xI.Width_Pixels, xI.PlateY_um + xI.Height_microns * bottom / xI.Height_Pixels)
            };


            var xRI = new xI_RaftInfo(RR, x, y, new PointF(bin * this.Center_X / xI.Width_Pixels, bin * this.Center_Y / xI.Height_Pixels));
            var RIS = new RaftInfoS();
            tRafts.Add(RaftID, xRI);
        }

        public string RegionName
        {
            get
            {
                return Center_X.ToString("0.0") + "," + Center_Y.ToString("0.0");
            }
        }

        public string ExportName = "";

        private PointF _Centroid;
        public PointF Centroid
        {
            get
            {
                if (_Centroid == PointF.Empty) RegenMajorMinorAxis();
                return _Centroid;
            }
            //set => _Centroid = value;
        }

        public string PlateID;
        public string WellLabel;
        public string RaftID;
        public int FOV;

        public float Center_X { get => Centroid.X; }
        public float Center_Y { get => Centroid.Y; }

        //public float Size_Dim_Longer { get => Math.Max(Size.Width, Size.Height); }
        public float Size_Dim_Shorter { get => Math.Min(Size.Width, Size.Height); }

        //public float Dim_LongerShorter_Ratio { get => Size_Dim_Longer / Size_Dim_Shorter; }

        //public float Dim_ShorterLonger_Ratio { get => Size_Dim_Shorter / Size_Dim_Longer; }

        private static Random _Rand = new Random();
        public Color DisplayColor = Color.FromArgb(_Rand.Next(0, 255), _Rand.Next(0, 255), _Rand.Next(0, 255));

        public float Area_Bounding { get => (float)(Math.PI * Size.Width * Size.Height); }

        public int m_Area_Pixels { get => Count; }

        private RectangleF _Rect;
        public RectangleF Rect
        {
            get
            {
                if (_Rect == RectangleF.Empty) RegenMajorMinorAxis();
                return _Rect;
            }
        }

        private double _MinorMajorAxisRatio = -52.45;
        public double m_MajorMinorAxisRatio
        {
            get
            {
                if (_MinorMajorAxisRatio == -52.45) RegenMajorMinorAxis();
                return _MinorMajorAxisRatio;
            }
        }

        private double _MajorAxisAngle_Radians = double.NaN;
        public double m_MajorAxisAngle_Radians
        {
            get
            {
                if (double.IsNaN(_MajorAxisAngle_Radians)) RegenMajorMinorAxis();
                return _MajorAxisAngle_Radians;
            }
        }

        private SizeF _Size;
        public SizeF Size
        {
            get
            {
                if (_Size == SizeF.Empty) RegenMajorMinorAxis();
                return _Size;
            }
        }

        public int ObjID { get; set; }
        public string CropSavePath { get; internal set; }
        public double[] PLResults { get; internal set; }

        private string _Report = "-1";
        public string Report
        {
            get
            {
                if (_Report == "-1")
                {
                    _Report = $"Area={m_Area_Pixels}\r\n" +
                              //$"AspR={m_MajorMinorAxisRatio.ToString("F2")}\r\n" +
                              //$"Angl={m_MajorAxisAngle_Radians.ToString("F2")}\r\n" +
                              $"Frt={m_FeretsDiameter.ToString("F2")}\r\n" +
                              $"Cmp={m_Compactness.ToString("F2")}\r\n" +
                              $"PerD={m_PerimeterDensity.ToString("F2")}";
                }
                return _Report;
            }
        }
    }

    public class SegObjects : IEnumerable<SegObject>
    {
        private HashSet<SegObject> _Set;

        private void Init()
        {
            _Set = new HashSet<SegObject>();
        }

        public SegObjects()
        {
            Init();
        }

        public int Count { get => _Set.Count; }
        public void Add(SegObject Obj)
        {
            Obj.ObjID = _Set.Count;
            _Set.Add(Obj);
        }

              public static SegObjects ObjectsFromImage(ThreshImage Image, XDCE_Image xI = null, float ShorterDimMinSize = 1F)
      {
          //Take into account the binning
          SegObjects Objs = new SegObjects();
          while (Image.Count > 0)
          {
              LinkedPix Pix = Image.First();
              if (Pix.IsMemberOfObject)
              {
                  //if it's already a member of an object, then it's already been processed; this avoid double check
                  continue;
              }
              SegObject sObj = SearchFromPix(Pix, Image);
              if (sObj.Size_Dim_Shorter > ShorterDimMinSize)
              {
                  Objs.Add(sObj);
                  if (xI != null)
                  {
                      sObj.PlateID = xI.Parent.PlateID;
                      sObj.WellLabel = xI.WellLabel;
                      sObj.FOV = xI.FOV;
                      sObj.RaftID = "";
                  }
              }
              
          }
          return Objs;
      }

      public static SegObject SearchFromPix(LinkedPix Start, LinkedPixSet Parent)
      {
          //we have a pix that is not a member of an object, so we need to expand it
          SegObject sObj = new SegObject();
          Expand(sObj, Start, Parent);
          return sObj;
      }


      internal static void Expand(SegObject sObj, LinkedPix Pix, LinkedPixSet Parent)
      {
          Queue<LinkedPix> NeedsWork = new Queue<LinkedPix>();
          NeedsWork.Enqueue(Pix); // Add the initial pix to the queue

          HashSet<LinkedPix> Full = new HashSet<LinkedPix>(); // To keep track of all processed pix

          while (NeedsWork.Count > 0)
          {
              LinkedPix aPix = NeedsWork.Dequeue();

              if (Full.Add(aPix))
              {
                  // Set the MemberOfObject property as soon as the pixel is added to Full
                  aPix.MemberOfObject = sObj;

                  foreach (var linkedPix in aPix.Links)
                  {
                      if (!Full.Contains(linkedPix))
                      {
                          NeedsWork.Enqueue(linkedPix);
                      }
                  }
                  aPix.LinksCrossRemove();
              }
          }

          // Now, Full already contains all pixels with the MemberOfObject property set
          sObj.UnionWith(Full);
          Parent.ExceptWith(Full);
      }


        public IEnumerator<SegObject> GetEnumerator()
        {
            return ((IEnumerable<SegObject>)_Set).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_Set).GetEnumerator();
        }

        /// <summary>
        /// Stick nuclei are very elongated objects that are abberant when looking at the nuclear channel
        /// </summary>
        internal void RemoveStickNuclei(float MinRatio = 0.3F)
        {
            _Set.RemoveWhere(x => x.m_MajorMinorAxisRatio < MinRatio);
        }
 
        internal RectangleF BoundingRect(float bufferPix = 0, float MaxWidth = float.PositiveInfinity, float MaxHeight = float.  PositiveInfinity)
        {
            // Initialize with opposite extremes to ensure correct calculation
            float MinTop = float.PositiveInfinity;
            float MinLeft = float.PositiveInfinity;
            float MaxBottom = float.NegativeInfinity;
            float MaxRight = float.NegativeInfinity;

            // Iterate over the set once, calculating min and max values.
            foreach (var item in _Set)
            {
                MinTop = Math.Min(MinTop, item.Rect.Top);
                MinLeft = Math.Min(MinLeft, item.Rect.Left);
                MaxBottom = Math.Max(MaxBottom, item.Rect.Bottom);
                MaxRight = Math.Max(MaxRight, item.Rect.Right);
            }

            // Apply buffer and boundary constraints
            if (bufferPix !=0)
            {
                MinTop = Math.Max(0, MinTop - bufferPix);
                MinLeft = Math.Max(0, MinLeft - bufferPix);
                MaxBottom = Math.Min(MaxHeight, MaxBottom + bufferPix);
                MaxRight = Math.Min(MaxWidth, MaxRight + bufferPix);
            }

            // Create and return the rectangle
            RectangleF R = new RectangleF(MinLeft, MinTop, MaxRight - MinLeft, MaxBottom - MinTop);
            return R;
        }

        internal void RemoveNucleiBySize(float MinNuclearArea, float MaxNuclearArea)
        {
            _Set.RemoveWhere(x => x.m_Area_Pixels < MinNuclearArea || x.m_Area_Pixels > MaxNuclearArea);
        }

        public static IEnumerable<SegObject> DeOverlap(IEnumerable<SegObject> filterObjs, double MaximumFractionOverlap, int RegionWidth, int MaxTryInCluster = 12)
        {
            var ItemsToRemove = new HashSet<SegObject>();
            var rects = new Dictionary<SegObject, RectangleF>();
            foreach (var obj in filterObjs) rects[obj] = 
                    obj.CropRect(RegionWidth); // First figure out all the rectangles

            var overlappingClusters = FindClusters(filterObjs, rects, MaximumFractionOverlap, RegionWidth);

            var random = new Random();
            while (overlappingClusters.Count > 0)
            {
                foreach (var cluster in overlappingClusters) // Process the overlapping clusters
                {
                    if (cluster.Count <= 1) continue;
                    if (cluster.Count == 2) { ItemsToRemove.Add(cluster[1]); cluster.RemoveAt(1); continue; }

                    int maxSubclusters = int.MinValue;
                    SegObject bestToRemove = null;
                    var objectsToEvaluate = cluster;
                    if (cluster.Count > MaxTryInCluster)
                    {
                        var indices = Enumerable.Range(0, cluster.Count);
                        var shuffledIndices = indices.OrderBy(x => random.Next()).Take(MaxTryInCluster);
                        objectsToEvaluate = shuffledIndices.Select(index => cluster[index]).ToList();
                    }

                    foreach (var obj in objectsToEvaluate)
                    {
                        var tempCluster = new List<SegObject>(cluster);
                        tempCluster.Remove(obj);
                        var subClusters = FindClusters(tempCluster, rects, MaximumFractionOverlap, RegionWidth);

                        if (subClusters.Count > maxSubclusters)
                        { // Check if removing obj results in fewer subclusters
                            maxSubclusters = subClusters.Count; bestToRemove = obj;
                        }
                    }

                    if (bestToRemove != null)
                    {
                        cluster.Remove(bestToRemove);
                        ItemsToRemove.Add(bestToRemove);
                    }
                }
                overlappingClusters.RemoveAll(x => x.Count <= 1);
            }
            return filterObjs.Except(ItemsToRemove);
        }


        private static List<List<SegObject>> FindClusters(IEnumerable<SegObject> objects, Dictionary<SegObject, RectangleF> rects, double MaximumFractionOverlap, int RegionWidth)
        {
            var clusters = new List<List<SegObject>>();

            foreach (var obj in objects)
            {
                List<SegObject> currentCluster = null;
                foreach (var otherObj in objects)
                {
                    if (obj == otherObj) continue;
                    if (CalculateOverlap(rects[obj], rects[otherObj], RegionWidth) < MaximumFractionOverlap) continue;

                    foreach (var cluster in clusters) // Find the cluster containing otherObj, if any
                    {
                        if (cluster.Contains(otherObj))
                        {
                            currentCluster = cluster;
                            break;
                        }
                    }

                    if (currentCluster == null) // If there's no existing cluster, create a new one
                    {
                        currentCluster = new List<SegObject> { obj, otherObj };
                        clusters.Add(currentCluster);
                    }
                    else
                    {
                        // Add obj to the existing cluster if it's not already there
                        if (!currentCluster.Contains(obj)) currentCluster.Add(obj);
                    }
                }
            }

            return clusters;
        }


        public static IEnumerable<SegObject> DeOverlap_Old(IEnumerable<SegObject> filterObjs, double MaximumFractionOverlap, int OrigWidth, int RegionWidth)
        {
            var ItemsToRemove = new HashSet<SegObject>();
            var overlappingClusters = new List<List<SegObject>>();
            var rects = new Dictionary<SegObject, RectangleF>();
            foreach (var obj in filterObjs) rects[obj] = obj.CropRect(RegionWidth); // First figure out all the rectangles

            foreach (var obj in filterObjs) // Populate overlapping clusters
            {
                List<SegObject> currentCluster = null;
                foreach (var otherObj in filterObjs)
                {
                    if (obj == otherObj) continue;
                    if (CalculateOverlap(rects[obj], rects[otherObj], RegionWidth) < MaximumFractionOverlap) continue;

                    foreach (var cluster in overlappingClusters) // Find the cluster containing otherObj, if any
                    {
                        if (cluster.Contains(otherObj))
                        {
                            currentCluster = cluster;
                            break;
                        }
                    }

                    if (currentCluster == null) // If there's no existing cluster, create a new one
                    {
                        currentCluster = new List<SegObject> { obj, otherObj };
                        overlappingClusters.Add(currentCluster);
                    }
                    else
                    {
                        // Add obj to the existing cluster if it's not already there
                        if (!currentCluster.Contains(obj)) currentCluster.Add(obj);
                    }
                }
            }

            //foreach (var cluster in overlappingClusters) { // Process the overlapping clusters
            //    if (cluster.Count > 1) ItemsToRemove.Add(cluster[0]);
            //}

            var random = new Random();
            foreach (var cluster in overlappingClusters) // Process the overlapping clusters
            {
                if (cluster.Count <= 1) continue;

                int minSubclusters = int.MinValue;
                SegObject bestToRemove = null;

                foreach (var obj in cluster)
                {
                    var tempCluster = new List<SegObject>(cluster);
                    tempCluster.Remove(obj);

                    // Find subclusters without obj
                    var subClusters = new List<List<SegObject>>();
                    foreach (var subObj in tempCluster)
                    {
                        List<SegObject> currentSubCluster = null;
                        foreach (var otherSubObj in tempCluster)
                        {
                            if (subObj == otherSubObj) continue;
                            if (CalculateOverlap(rects[subObj], rects[otherSubObj], RegionWidth) < MaximumFractionOverlap) continue;

                            foreach (var subCluster in subClusters) // Find the subcluster containing otherSubObj, if any
                            {
                                if (subCluster.Contains(otherSubObj))
                                {
                                    currentSubCluster = subCluster;
                                    break;
                                }
                            }

                            if (currentSubCluster == null) // If there's no existing subcluster, create a new one
                            {
                                currentSubCluster = new List<SegObject> { subObj, otherSubObj };
                                subClusters.Add(currentSubCluster);
                            }
                            else
                            {
                                // Add subObj to the existing subcluster if it's not already there
                                if (!currentSubCluster.Contains(subObj)) currentSubCluster.Add(subObj);
                            }
                        }
                    }

                    // Check if removing obj results in more subclusters
                    if (subClusters.Count > minSubclusters)
                    {
                        minSubclusters = subClusters.Count;
                        bestToRemove = obj;
                    }
                }

                if (bestToRemove != null) ItemsToRemove.Add(bestToRemove);
            }

            return filterObjs.Except(ItemsToRemove);
        }

        private static List<SegObject> FindClusterCausingOverlap(List<List<SegObject>> overlappingClusters, Dictionary<SegObject, RectangleF> rects, int RegionWidth)
        {
            List<SegObject> clusterToRemove = null;
            double maxOverlapArea = 0;

            foreach (var cluster in overlappingClusters)
            {
                double totalOverlapArea = 0;
                for (int i = 0; i < cluster.Count; i++)
                {
                    for (int j = i + 1; j < cluster.Count; j++)
                    {
                        double fractionOverlap = CalculateOverlap(rects[cluster[i]], rects[cluster[j]], RegionWidth);
                        double overlapArea = fractionOverlap * (RegionWidth * RegionWidth);
                        totalOverlapArea += overlapArea;
                    }
                }

                if (totalOverlapArea > maxOverlapArea)
                {
                    maxOverlapArea = totalOverlapArea;
                    clusterToRemove = cluster;
                }
            }

            return clusterToRemove;
        }

        private static double CalculateOverlap(RectangleF rect1, RectangleF rect2, int RegionWidth)
        {
            var intersection = RectangleF.Intersect(rect1, rect2);
            double overlapArea = intersection.Width * intersection.Height;
            double fractionOverlap = overlapArea / (RegionWidth * RegionWidth);

            return fractionOverlap;
        }



    }

    /// <summary>
    /// Records the results from a single object
    /// </summary>
    public class SegResultsObj
    {
        public SegResults Parent;
        internal Dictionary<int, object> Data;
        internal Dictionary<int, float> Measurements;
        public string AggKey;
        public override string ToString()
        {
            return AggKey;
        }
        //private Dictionary<int, object> MetaData;

        public SegResultsObj(SegResults ParentSegRes, XDCE_Image xI, string RaftID, string Channel, SegObject Obj)
        {
            Parent = ParentSegRes;
            Data = new Dictionary<int, object>();
            Measurements = new Dictionary<int, float>();
            //MetaData = new Dictionary<int, object>();

            AddInternal(nameof(xI.Parent.PlateID), xI.Parent.PlateID, true);
            AddInternal(nameof(xI.WellLabel), xI.WellLabel, true);
            AddInternal(nameof(xI.FOV), xI.FOV, true);
            AddInternal(nameof(RaftID), RaftID, true);
            AddInternal(nameof(Channel), Channel, true);
            AddInternal(nameof(Obj.CropSavePath), Obj.CropSavePath, true);

            AggKey = xI.Parent.PlateID + "." + xI.WellLabel + "." + xI.FOV + "." + RaftID + "." + Channel.Replace(" ", "").Substring(0, 4);

            bool useRefl = false;
            if (useRefl)
            {
                System.Reflection.PropertyInfo[] Props = Obj.GetType().GetProperties();
                foreach (var Prop in Props)
                    if (Prop.PropertyType.IsPrimitive)
                        AddInternal(Prop.Name, Prop.GetValue(Obj));
            }
            else
            {
                AddInternal("Center X", Obj.Center_X);
                AddInternal("Center Y", Obj.Center_Y);
                AddInternal("Area Pixels", Obj.m_Area_Pixels);
                AddInternal("Area Bounding", Obj.Area_Bounding);
                AddInternal("Major MinorRatio", Obj.m_MajorMinorAxisRatio);
                AddInternal("Major AxisAngle", Obj.m_MajorAxisAngle_Radians);
            }
        }

        private void AddInternal(string Name, object Val, bool IsMetadata = false)
        {
            int Key2 = Parent.MasterHeaderKeyMaker(Name);
            Data.Add(Key2, Val);
            if (!IsMetadata) Measurements.Add(Key2, float.Parse(Val.ToString()));
        }

        public static char delim = '\t';

        internal string Export(bool Headers)
        {
            var sB = new StringBuilder();
            for (int i = 0; i < Parent.MasterHeaders.Count; i++)
            {
                string s = Data[i] == null ? "" : Data[i].ToString();
                sB.Append((Headers ? Parent.MasterHeadersLabel[i] : s) + delim);
            }
            return sB.ToString();
        }
    }

    /// <summary>
    /// Used to aggregate image, raft, or well-level data
    /// </summary>
    public class SegResultsAgg
    {
        public SegResults ResultParent;
        public string Name { get; set; }
        public Dictionary<int, float> Sums;
        public int Count { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public SegResultsAgg(SegResults Parent, string KeyName)
        {
            Name = KeyName;
            ResultParent = Parent;
            Sums = new Dictionary<int, float>();
            Count = 0;
        }

        public void Append(SegResultsObj tSRO)
        {
            Count++;
            //Interlocked.Add(ref Count, Count+1);
            foreach (var KVP in tSRO.Measurements)
            {
                if (!Sums.ContainsKey(KVP.Key)) Sums.Add(KVP.Key, 0);
                Sums[KVP.Key] += KVP.Value;
            }
        }

        public static char delim = SegResultsObj.delim;

        public string Export(bool Header = false)
        {
            var sB = new StringBuilder();
            sB.Append((Header ? "Key" : Name) + delim);
            for (int i = 0; i < ResultParent.MasterHeaders.Count; i++)
                if (Sums.ContainsKey(i))
                    sB.Append((Header ? ResultParent.MasterHeadersLabel[i] : (Sums[i] / Count).ToString()) + delim);
            sB.Append(Header ? "Objects" : Count.ToString() + delim);
            return sB.ToString();
        }
    }

    public class DIResultAgg
    {
        public Dictionary<string, double> KVPs;
        public string AggKey;

        public DIResultAgg(XDCE_Image xI, string RaftID, string Channel)
        {
            AggKey = xI.Parent.PlateID + "." + xI.WellLabel + "." + xI.FOV + "." + RaftID + "." + Channel.Replace(" ", "").Substring(0, 4);
            KVPs = new Dictionary<string, double>();
        }

        public DIResultAgg(XDCE_Image xI, string RaftID, string Channel, BasicSegmentatioResults result)
        {
            AggKey = xI.Parent.PlateID + "." + xI.WellLabel + "." + xI.FOV + "." + RaftID + "." + Channel.Replace(" ", "").Substring(0, 4);
            KVPs = new Dictionary<string, double>();
            KVPs.Add("FAA", result.FAA);
            KVPs.Add("TAA", result.TAA);
            KVPs.Add("DI", result.DI);
        }

        public override string ToString()
        {
            return AggKey;
        }


    }

    public class SegResults
    {
        internal ConcurrentBag<SegResultsObj> BagObj;
        internal Dictionary<string, SegResultsAgg> BagAgg;
        internal Dictionary<string, DIResultAgg> DIAgg;
        internal Dictionary<string, int> MasterHeaders;
        internal Dictionary<int, string> MasterHeadersLabel;

        public SegResults(int TotalCount)
        {
            BagObj = new ConcurrentBag<SegResultsObj>();
            BagAgg = new Dictionary<string, SegResultsAgg>();
            DIAgg = new();
            MasterHeaders = new Dictionary<string, int>();
            MasterHeadersLabel = new Dictionary<int, string>();
            ProcessedCounter = new ConcurrentBag<short>();
            TotalCountToProcess = TotalCount;
        }

        public int TotalCountToProcess { get; private set; }

        public int ResultsCount { get => BagObj.Count; }

        public int MasterHeaderKeyMaker(string Name)
        {
            string tName = Name.Trim();
            if (!MasterHeaders.ContainsKey(tName))
            {
                int tKey = MasterHeaders.Count;
                MasterHeaders.Add(tName, tKey);
                MasterHeadersLabel.Add(tKey, tName);
            }
            return MasterHeaders[tName];
        }
        public static string AggKeyMaker(SegResultsObj tSRO)
        {
            return tSRO.AggKey;
        }

        private ConcurrentBag<short> ProcessedCounter;
        public void ProcessedCountIncrement() { ProcessedCounter.Add(0); }
        public int ProcessedCount { get => ProcessedCounter.Count; }
        public string ProcessedPercent { get => ((float)ProcessedCount / TotalCountToProcess).ToString("0.0%"); }

        public void AppendObject(XDCE_Image xI, string raftID, string channelName, SegObject Obj)
        {
            var tSRO = new SegResultsObj(this, xI, raftID, channelName, Obj);
            BagObj.Add(tSRO);
            string AggKey = AggKeyMaker(tSRO);
            if (!BagAgg.ContainsKey(AggKey)) BagAgg.Add(AggKey, new SegResultsAgg(this, AggKey));
            BagAgg[AggKey].Append(tSRO);
        }

        internal void AppendDI(XDCE_Image xI, string raftID, string channelName, BasicSegmentatioResults result)
        {
            var tDIAgg = new DIResultAgg(xI, raftID, channelName, result);
            DIAgg.Add(tDIAgg.AggKey, tDIAgg);
        }

        public void ExportObjects(string ExportFilePath)
        {
            if (BagObj.Count == 0)
                return;
            using (var SW = new StreamWriter(ExportFilePath))
            {
                SW.WriteLine(BagObj.First().Export(true));
                foreach (var Line in BagObj) SW.WriteLine(Line.Export(false));
                SW.Close();
            }
        }

        public void ExportAggregations(string ExportFilePath)
        {
            using (var SW = new StreamWriter(ExportFilePath))
            {
                SW.WriteLine(BagAgg.First().Value.Export(true));
                foreach (var Line in BagAgg.Values) SW.WriteLine(Line.Export(false));
                SW.Close();
            }
        }

        public void ExportDIs(string ExportFilePath)
        {
            var sB = new StringBuilder();
            bool First = true;
            foreach (var diFOVs in DIAgg)
            {
                if (First)
                {
                    sB.Append("Key\t");
                    foreach (var K in diFOVs.Value.KVPs.Keys) sB.Append(K + '\t');
                    sB.Append("\r\n");
                    First = false;
                }
                sB.Append(diFOVs.Value.AggKey + "\t");
                foreach (var V in diFOVs.Value.KVPs.Values) sB.Append(V.ToString() + '\t');
                sB.Append("\r\n");
            }
            File.WriteAllText(ExportFilePath + "_DI.txt", sB.ToString());
        }

    }

    public class CropImageSettings
    {
        private Dictionary<string, string> _WellDict;

        [XmlIgnore]
        public Dictionary<string, string> WellDict
        {
            get
            {
                if (_WellDict == null && WellLookUp_WellLabel_Comma_Folder_Semicolon != "")
                {
                    WellDictionary_Refresh_FromString();
                }
                return _WellDict;
            }
            set { _WellDict = value; }
        }

        [XmlIgnore]
        public PL.MLSettings_PL MLSettings { get; set; }

        public string WellLookUp_WellLabel_Comma_Folder_Semicolon { get; set; }

        public bool WellDictionary_Refresh_FromString()
        {
            var TempOld = _WellDict;
            try
            {
                _WellDict = new Dictionary<string, string>();
                string[] Arr = WellLookUp_WellLabel_Comma_Folder_Semicolon.Split(';');
                foreach (string item in Arr)
                {
                    string[] Arr2 = item.Split(',');
                    if (Arr2.Length == 2)
                    {
                        _WellDict.Add(Arr2[0], Arr2[1]);
                    }
                }
                return true;
            }
            catch
            {
                _WellDict = TempOld;
                return false;
            }
        }

        public string WellDictMatch(XDCE_Image xI)
        {
            //First try just the well
            string key;
            key = xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try the full plateID and the well
            key = xI.Parent.PlateID + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try the plateID no Index and the well
            key = xI.Parent.ParentFolder.PlateID_NoScanIndex + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try the plate index and the well
            key = xI.Parent.ParentFolder.PlateIndex + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];

            //Now try plate # 1
            key = "1" + "|" + xI.WellLabel;
            if (WellDict.ContainsKey(key)) return WellDict[key];
            return "UNK";
        }

        public void String_FromWellDictionary()
        {
            WellLookUp_WellLabel_Comma_Folder_Semicolon = String.Join(";", WellDict.Select(x => x.Key + "," + x.Value));
        }

        public CropImageSettings()
        {
            ParallelizeSegmentation = false;
            if (CropSize.Height == 0)
            {
            }
        }

        public CropImageSettings(CropImageSettings CopyFrom)
        {
            foreach (var Prop in typeof(CropImageSettings).GetProperties())
            {
                Prop.SetValue(this, Prop.GetValue(CopyFrom));
            }
        }

        public CropImageSettings(bool LoadDefaults)
        {
            if (LoadDefaults)
            {
                //For now, this is the best way to do cell/object based exportes.
                //System.Diagnostics.Debugger.Break();
                WellDict = new Dictionary<string, string>(); WellDict.Add("A - 1", "WT C1"); WellDict.Add("B - 2", "MU C1"); //WellDest.Add("C - 2", "G1"); WellDest.Add("A - 3", "G2"); WellDest.Add("B - 3", "G2");
                String_FromWellDictionary();

                CropSize = new Size(100, 100);
                SaveFolder = @"K:\_Training\Leica MFN2\";
                DirectoryInfo DI = new DirectoryInfo(SaveFolder); if (!DI.Exists) DI.Create();
                MinAreaPixels = 70; MaxAreaPixels = 1000;
                SaveTypeExtension_NoDot = "tiff";
                RandomizeImageIntensity_Min = 1; //This turns it off
                RunMLProcessing = "";
                Version = 3; //This can help us determine if we need to update pieces
                ParallelizeSegmentation = false;
                MaxOverlapAllowed = 0.3F;
                DI_Fragmented_CompactnessThresh = 0.3;
                DI_MinPerimeterDensity = 0.4;
                DI_Max_Area_Bounding = 5000;
                DI_Slice_ModulusFactor_PreBin = 64;
                SaveCroppedImages = true;
            }
        }

        public int Version { get; set; }

        public Size CropSize { get; set; }
        public string SaveFolder { get; set; }
        public int MinAreaPixels { get; set; }
        public int MaxAreaPixels { get; set; }
        public float MaxOverlapAllowed { get; set; }
        public double DI_Fragmented_CompactnessThresh { get; set; }
        public double DI_MinPerimeterDensity { get; set; }
        public int DI_Max_Area_Bounding { get; set; }
        public int DI_Slice_ModulusFactor_PreBin { get; set; }
        public bool OnlyForRegionAssignment { get; set; }
        public float RandomizeImageIntensity_Min { get; set; }
        public bool SaveCroppedImages { get; set; }

        public string SaveTypeExtension_NoDot { get; set; }
        public string RunMLProcessing { get; set; }
        public bool ParallelizeSegmentation { get; set; }


        public bool StoreResults = true;

        public void GetRectangle(SegObject obj, SizeF SourceRect, float ThreshBinning, out RectangleF CropRegion, out PointF StartOfRegion)
        {
            //Since the thresholded image was binned to speed things up (And we don't need the resolution to get the objects), we have to reverse out the binning here
            float x1 = (obj.Center_X * ThreshBinning) - CropSize.Width / 2;
            float y1 = (obj.Center_Y * ThreshBinning) - CropSize.Height / 2;
            float x2 = (obj.Center_X * ThreshBinning) + CropSize.Width / 2;
            float y2 = (obj.Center_Y * ThreshBinning) + CropSize.Height / 2;

            //Make sure it doesn't go off the edges of the canvas
            float x1p = Math.Min(SourceRect.Width, Math.Max(0, x1));
            float y1p = Math.Min(SourceRect.Height, Math.Max(0, y1));
            float x2p = Math.Min(SourceRect.Width, Math.Max(0, x2));
            float y2p = Math.Min(SourceRect.Height, Math.Max(0, y2));

            var P1 = new PointF(x1p, y1p);
            var S = new SizeF(x2p - x1p, y2p - y1p);
            CropRegion = new RectangleF(P1, S);
            StartOfRegion = new PointF(Math.Max(0, -x1), Math.Max(0, -y1));
        }

        public string GetSavePath(XDCE_Image xI, SegObject obj)
        {
            string FileName = xI.Parent.PlateID + "." + xI.WellField + "_" + obj.ObjID + "." + SaveTypeExtension_NoDot;
            string SubFolder = WellDictMatch(xI);

            string drive = Path.GetPathRoot(SaveFolder);
            if (!Directory.Exists(drive))
                SaveFolder = @"c:\temp\";
            if (!Directory.Exists(SaveFolder))
                Directory.CreateDirectory(SaveFolder);
            string Folder = Path.Combine(SaveFolder, SubFolder);
            DirectoryInfo DI = new DirectoryInfo(Folder); if (!DI.Exists) DI.Create();
            return Path.Combine(Folder, FileName);
        }
    }

    public class BasicSegmentatioResults
    {
        public static BasicSegmentatioResults BasicSegmentation(CropImageSettings cs, float Thresh, XDCE_Image xI, Bitmap bmpPreThresh, Bitmap bmpCombined, int bin = 4, bool MakeThreshImage = false, bool MakeObjectImage = false, bool MakeRectBitmap = false)
        {
            //Right now, the area that is restricted is NOT adjusted based on the binning or uM, it is simply based on pixels in the binned image ****

            int FinalWidth = xI.Width_Pixels; //But could change
            if (cs == null) cs = new CropImageSettings();
            if (cs.MinAreaPixels <= 0) cs.MinAreaPixels = 10;
            if (cs.MaxAreaPixels <= 0) cs.MaxAreaPixels = 200;
            if (cs.CropSize == new Size(0, 0)) cs.CropSize = new Size(128, 128);
            if (cs.MaxOverlapAllowed < 0.001) cs.MaxOverlapAllowed = 0.001F;

            var result = new BasicSegmentatioResults();
            result.Binning = bin;
            result.CombinedBMAP = bmpCombined;

            // Threshold the image that is present:
            var start = DateTime.Now;
            var thresh = new ThreshImage(bmpPreThresh, Thresh, true, bin);
            result.PostThreshBitmap = MakeThreshImage ? thresh.PostThreshBitmap : null;
            result.ThreshTime = (DateTime.Now - start).TotalMilliseconds;

            // Object detect on the threshold:
            start = DateTime.Now;
            result.Objs = SegObjects.ObjectsFromImage(thresh, xI);
            result.ObjTime = (DateTime.Now - start).TotalMilliseconds;

            // Filter and de-overlap objects
            
            start = DateTime.Now;
            Size cropSizeUse = new Size(cs.CropSize.Width / bin, cs.CropSize.Height / bin);
            //Attempting to Filter and De-Overlap a single Seg-Object can Cause bugs even if the Segment is legitimate.
            if (result.Objs.Count != 1)
            {
                result.FilterObjs = result.Objs.Where(o => o.m_Area_Pixels >= cs.MinAreaPixels && o.m_Area_Pixels <= cs.MaxAreaPixels);
            }
            else
            {
                result.FilterObjs = result.Objs;
            }
            result.FilteredObjectsBitmap = MakeObjectImage ? MasksHelper.DrawMask(result.FilterObjs, thresh.Size.Width, FinalWidth, 0, true) : null;
            result.FilteredObjectsOutline = MakeObjectImage ? MasksHelper.DrawObjectsOutlines(result.FilterObjs, thresh.Size.Width, FinalWidth) : null;
            result.DeOverlap = null;
            if (result.Objs.Count != 1)
            {
                result.DeOverlap = SegObjects.DeOverlap(result.FilterObjs, cs.MaxOverlapAllowed, cropSizeUse.Width);
            }
            else
            {
                result.DeOverlap = result.FilterObjs;
            }
            result.DeOverlappedObjectsBitmap = MakeRectBitmap ? MasksHelper.DrawMask(result.DeOverlap, thresh.Size.Width, FinalWidth, 0, true, cropSizeUse, result.CombinedBMAP) : null;
            result.CropRectangles = result.DeOverlap.ToDictionary(k => k.ObjID, x => x.CropRect(cs.CropSize.Width, bin));
            result.RegionsTime = (DateTime.Now - start).TotalMilliseconds;

            //Just to see if the sizing is right
            //ML_Context.MeasureTest2(result);

            return result;
        }

        public static BasicSegmentatioResults BasicSegmentation(CropImageSettings cs, wvDisplayParams wvPs, XDCE_Image xI, ImageQueue ImgQueue, int bin = 4, bool MakeThreshImage = false, bool MakeObjectImage = false, bool MakeRectBitmap = false)
        {
            var trylist = wvPs.List.Where(x => x.ObjectAnalysis);
            if (trylist.Count() <= 0) return null;
            var objWVp = trylist.First();
            Bitmap CombinedBMAP = null; bool avail;
            if (!cs.OnlyForRegionAssignment) 
                (CombinedBMAP, avail) = ImgQueue.CombinedBMAP(xI.Parent, xI, wvPs);
            var bmpPreThresh = Utilities.DrawingUtils.GetAdjustedImageSt(xI.FullPath, xI, objWVp.Brightness);

            return BasicSegmentation(cs, objWVp.Threshold, xI, bmpPreThresh, CombinedBMAP, bin, MakeRectBitmap, MakeObjectImage, MakeRectBitmap);
        }

        public static BasicSegmentatioResults DegenerationIndex(CropImageSettings cs, float Thresh, XDCE_Image xI, Bitmap bmpPreThresh, Bitmap bmpCombined, int bin = 4, bool MakeThreshImage = false, bool MakeObjectImage = false, bool MakeRectBitmap = false)
        {
            int FinalWidth = xI.Width_Pixels; //But could change
            if (cs == null) cs = new CropImageSettings();
            if (cs.DI_Fragmented_CompactnessThresh <= 0) cs.DI_Fragmented_CompactnessThresh = 0.3;
            if (cs.DI_MinPerimeterDensity <= 0) cs.DI_MinPerimeterDensity = 0.4;
            if (cs.DI_Max_Area_Bounding <= 0) cs.DI_Max_Area_Bounding = 5000;
            if (cs.DI_Slice_ModulusFactor_PreBin <= 0) cs.DI_Slice_ModulusFactor_PreBin = 64;

            var result = new BasicSegmentatioResults();
            result.Binning = bin;
            result.CombinedBMAP = bmpCombined;

            // Threshold the image that is present:
            var start = DateTime.Now;
            ThreshImage.Slice_ModulusFactor = cs.DI_Slice_ModulusFactor_PreBin / bin;
            var thresh = new ThreshImage(bmpPreThresh, Thresh, true, bin, true);
            result.PostThreshBitmap = MakeThreshImage ? thresh.PostThreshBitmap : null;
            result.ThreshTime = (DateTime.Now - start).TotalMilliseconds;

            // Object detect on the threshold:
            start = DateTime.Now;
            result.Objs = SegObjects.ObjectsFromImage(thresh, xI, 0.3F);
            result.ObjTime = (DateTime.Now - start).TotalMilliseconds;

            // Filter
            start = DateTime.Now;
            result.FilterObjs = result.Objs.Where(o => o.m_PerimeterDensity > 0.001); //All comers
            result.FilteredObjectsBitmap = MakeObjectImage ? MasksHelper.DrawMask(result.FilterObjs, thresh.Size.Width, FinalWidth, 0, true) : null;
            result.FilteredObjectsOutline = MakeObjectImage ? MasksHelper.DrawObjectsOutlines(result.FilterObjs, thresh.Size.Width, FinalWidth) : null;
            result.RegionsTime = (DateTime.Now - start).TotalMilliseconds;

            // Fragmentation Calc
            start = DateTime.Now;
            result.FAA = 0; result.TAA = 0;
            foreach (var Obj in result.FilterObjs)
            {
                if (Obj.m_PerimeterDensity < cs.DI_MinPerimeterDensity || Obj.Area_Bounding > cs.DI_Max_Area_Bounding)
                {
                    Obj.DisplayColor = Color.Purple;
                }
                else
                {
                    result.TAA += Obj.m_Area_Pixels;
                    if (Obj.m_Compactness > cs.DI_Fragmented_CompactnessThresh)
                    {
                        result.FAA += Obj.m_Area_Pixels;
                        Obj.DisplayColor = Color.Red;
                    }
                    else Obj.DisplayColor = Color.LightGreen;
                }
            }
            result.DI = result.FAA / result.TAA;
            result.FragmentPreviewBitmap = MakeObjectImage ? MasksHelper.DrawObjectsOutlines(result.FilterObjs, thresh.Size.Width, FinalWidth) : null;
            result.FragmentTime = (DateTime.Now - start).TotalMilliseconds;

            return result;
        }

        public Bitmap CombinedBMAP { get; set; }
        public Bitmap PostThreshBitmap { get; set; }
        public Bitmap FilteredObjectsBitmap { get; set; }
        public Bitmap FilteredObjectsOutline { get; set; }
        public Bitmap DeOverlappedObjectsBitmap { get; set; }
        public Bitmap FragmentPreviewBitmap { get; set; }
        public double ThreshTime { get; set; }
        public double ObjTime { get; set; }
        public double RegionsTime { get; set; }
        public double FragmentTime { get; set; }
        public double FAA { get; set; }
        public double TAA { get; set; }
        public double DI { get; set; }
        public int Binning = 4;
        public SegObjects Objs { get; set; }
        public IEnumerable<SegObject> FilterObjs { get; set; }
        public IEnumerable<SegObject> DeOverlap { get; set; }
        public Dictionary<int, RectangleF> CropRectangles { get; set; }

        public static Bitmap AddLabel(Bitmap bmp, string Label, int Binning = 1)
        {
            return AddLabel(bmp, Label, Color.White, Binning);
        }

        public static Bitmap AddLabel(Bitmap bmp, string Label, Color color, int Binning = 1, bool LowerLeft = true)
        {
            var tFont = new Font("Arial", 60 / Binning);
            var bmap = new Bitmap(bmp);
            using (var g = Graphics.FromImage(bmap))
            {
                g.DrawString(Label, tFont, new SolidBrush(color), new Point(10, (int)(0.9 * bmp.Height)));
            }
            return bmap;
        }

        internal void DisposeBMAPS()
        {
            if (CombinedBMAP != null) CombinedBMAP.Dispose();
            if (PostThreshBitmap != null) PostThreshBitmap.Dispose();
            if (FilteredObjectsBitmap != null) FilteredObjectsBitmap.Dispose();
            if (DeOverlappedObjectsBitmap != null) DeOverlappedObjectsBitmap.Dispose();
        }
    }
}
