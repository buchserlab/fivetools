using FIVE.ImageCheck;
using FIVE_IMG;
using ImageMagick.Formats;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics.Arm;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Tensorflow;
using Tensorflow.Keras.Engine;
using Tensorflow.NumPy;
using System.Threading;
using System.Threading.Tasks;
using static Tensorflow.Binding;
using System.Collections.Concurrent;

namespace FIVE
{
    namespace TF
    {
        public class ModelInferSettings
        {
            public string ModelPath_Classification { get; set; }
            public string ModelPath_Measure { get; set; }
            public List<string> ModelPaths_Measure_Multi { get; set; }
            public List<string> ModelPaths_Classification_Multi { get; set; }
            public float Classification_KeepThreshold { get; set; }
            public bool SaveImagesWhileInferring_Classify { get; set; }
            public bool SaveImagesWhileInferring_Measure { get; set; }
            public byte ExistingAnnotations_1All_2Existing_3Not_Classification { get; set; }
            public byte ExistingAnnotations_1All_2Existing_3Not_Measure { get; set; }
            public string Measure_OnlyExistingByName { get; set; }

            private List<string> _Measure_OnlyExistingByNameList = null;
            [XmlIgnore]
            public List<string> Measure_OnlyExistingByNameList { get { OnlyExistingCheckLists(); return _Measure_OnlyExistingByNameList; } }
            public string svImageFolder { get; set; }
            public string svImageExt { get; set; }

            public string cmClassification_OnlyExistingByName { get; set; }
            public bool cmPerformClassifications { get; set; }
            public bool cmPerformMeasures { get; set; }
            public bool cmSaveImages_Overall { get; set; }
            public bool cmRecordBackAnnotations { get; set; }
            public bool cmDeleteExistingAnnotations { get; set; }
            public bool cmSaveSegOutputImages { get; set; }
            public bool cmRaftBased { get; set; }


            public string SaveMeasuresFolder { get; set; } //Set from other sources if blank

            [XmlIgnore]
            public bool ActiveType_Classify = true;

            [XmlIgnore]
            public string ModelPath => ActiveType_Classify ? ModelPath_Classification : ModelPath_Measure;
            [XmlIgnore]
            public bool SaveImagesWhileInferring => ActiveType_Classify ? SaveImagesWhileInferring_Classify : SaveImagesWhileInferring_Measure;
            [XmlIgnore]
            public byte ExistingAnnotations => ActiveType_Classify ? ExistingAnnotations_1All_2Existing_3Not_Classification : ExistingAnnotations_1All_2Existing_3Not_Measure;

            public ModelInferSettings()
            {
                ModelPath_Classification = ModelPath_Measure = "";
                Classification_KeepThreshold = 0.65F;
                SaveImagesWhileInferring_Classify = false;
                SaveImagesWhileInferring_Measure = true;
                ExistingAnnotations_1All_2Existing_3Not_Classification = 1;
                ExistingAnnotations_1All_2Existing_3Not_Measure = 2;
                Measure_OnlyExistingByName = "";
                svImageFolder = @"e:\temp\check\";
                svImageExt = ".jpg";

                cmClassification_OnlyExistingByName = "";
                cmPerformClassifications = false;
                cmPerformMeasures = false;
                cmSaveImages_Overall = false;
                cmRecordBackAnnotations = false;
                cmSaveSegOutputImages = false;
                cmRaftBased = true;
            }

            public static ModelInferSettings Default01()
            {
                var MIS = new ModelInferSettings();

                MIS.ModelPath_Classification = @"S:\Raft\FIV827\001 SF Sort\M 20230403\ acc_0.7969 CNN_AC_R CNNs_7 kn_4 flt_18 iRt_1.5 ds__270_ nE_169 bSz_64 bNm_0 i_22\acc_0.7969 CNN_AC_R CNNs_7 kn_4.pb";
                MIS.ModelPath_Measure = @"S:\Phys\FIV823 NeuroMito\M\ acc_0.1496 CNN_Ar_mLS CNNs_7 kn_2 flt_23 iRt_1.5 ds__128_ nE_72_72 bSz_32 bNm_0 i_0\acc_0.1496 CNN_Ar_mLS.pb";
                MIS.Classification_KeepThreshold = 0.66F;
                MIS.SaveImagesWhileInferring_Classify = false;
                MIS.SaveImagesWhileInferring_Measure = true;
                MIS.ExistingAnnotations_1All_2Existing_3Not_Classification = 1;
                MIS.ExistingAnnotations_1All_2Existing_3Not_Measure = 2;
                MIS.Measure_OnlyExistingByName = "1 Neuron";

                MIS.cmClassification_OnlyExistingByName = "";
                MIS.cmPerformClassifications = false;
                MIS.cmPerformMeasures = false;
                MIS.cmSaveImages_Overall = false;
                MIS.cmRecordBackAnnotations = false;
                MIS.cmSaveSegOutputImages = false;
                MIS.cmRaftBased = true;

                return MIS;
            }

            public void OnlyExistingCheckLists()
            {
                if (_Measure_OnlyExistingByNameList == null)
                {
                    var t = Measure_OnlyExistingByName.Trim().Split(';').Select(x => x.Trim().ToUpper());
                    _Measure_OnlyExistingByNameList = new List<string>(t.Where(x => x != ""));
                }
            }
            public (string error, string note) PreCheck_ClassifyMeasure(double FractionCal)
            {
                //Make sure the models at least exist
                string note = "";
                if (cmPerformClassifications)
                {
                    if (!File.Exists(ModelPath_Classification))
                    {
                        if (!Directory.Exists(ModelPath_Classification))
                        {
                            note += "Couldn't find classification model.";
                        }
                    }
                }
                if (cmPerformMeasures)
                {
                    //At least one of these needs to exist to continue
                    foreach (var item in ModelPaths_Measure_Multi)
                    {
                        if (!File.Exists(item))
                        {
                            if (!Directory.Exists(item))
                                return ("Couldn't find Measurement model.", note);
                        }
                    }
                }
                return ("", note);
            }

            public static ModelInferSettings Load(string fileName)
            {
                if (!File.Exists(fileName)) return null;
                try
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        var serializer = new XmlSerializer(typeof(ModelInferSettings));
                        var MIS = (ModelInferSettings)serializer.Deserialize(stream);
                        return MIS;
                    }
                }
                catch
                {
                    return null;
                }
            }



            public void Save(string Path)
            {
                //this.SaveTime = DateTime.Now;
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }

            public static List<string> Default_MitoMeasure_Paths
            {
                get
                {
                    string Prefix_S = @"S:\Phys\FIV813 Mito\M 5aS1 20230411";
                    string Prefix_R = @"R:\dB\Software\FIVE_Tools\Models\NonRaft\Mito 20x 5aS1 20230411";
                    return new List<string>() { Prefix_R + @"\acc_0.1594 CNN_Nr_mLS CNNs_7 kn_2 flt_30 iRt_1.5 ds__90_ nE_25,25 bSz_64 bNm_0 i_1\",
                                                Prefix_R + @"\acc_0.1644 CNN_Ar_mLS CNNs_6 kn_3 flt_18 iRt_1.75 ds__159, 72_ nE_20,25 bSz_64 bNm_0 i_7\" };
                }
            }

            public static List<string> Default_NeuriteMeasure_Paths
            {
                get
                {
                    {
                        //var Folders = new List<string>() {
                        //    @"S:\Phys\FIV823 NeuroMito\4b M 20230404\ acc_0.1496 CNN_Ar_mLS CNNs_7 kn_2 flt_23 iRt_1.5 ds__128_ nE_72_72 bSz_32 bNm_0 i_0",

                        //    @"S:\Phys\FIV823 NeuroMito\4bS2 M 20230414\acc_0.1298 CNN_Ar_mLS CNNs_7 kn_3 flt_28 iRt_1.25 ds__182, 119, 104, 86, 77_ nE_137,320 bSz_64 bNm_0 i_32",
                        //    @"S:\Phys\FIV823 NeuroMito\4bS2 M 20230414\acc_0.1327 CNN_Nr_mLS CNNs_8 kn_3 flt_32 iRt_1.75 ds__736, 548, 70_ nE_207,320 bSz_64 bNm_0 i_23",
                        //    @"S:\Phys\FIV823 NeuroMito\4bS2 M 20230414\acc_0.1368 CNN_Ar_mLS CNNs_7 kn_2 flt_25 iRt_1.25 ds__137, 42_ nE_122,320 bSz_64 bNm_0 i_22",
                        //    @"S:\Phys\FIV823 NeuroMito\4bS2 M 20230414\acc_0.1295 CNN_Nr_sLS CNNs_6 kn_4 flt_36 iRt_1.5 ds__641, 95_ nE_96,320 bSz_64 bNm_0 i_6",

                        //    @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1549 CNN_Ar_sLS CNNs_7 kn_3 flt_18 iRt_1.25 ds__33_ nE_50,50 bSz_32 bNm_0 i_1",
                        //    @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1490 CNN_Ar_mLS CNNs_7 kn_3 flt_24 iRt_1.25 ds__116_ nE_30,50 bSz_32 bNm_0 i_0",

                        //    @"S:\Phys\FIV823 NeuroMito\4b M 20230408\acc_0.1291 CNN_Ar_mLS CNNs_6 kn_2 flt_34 iRt_1.25 ds__313, 133, 31_ nE_60,60 bSz_32 bNm_0 i_5",
                        //    @"S:\Phys\FIV823 NeuroMito\4b M 20230408\acc_0.1500 CNN_Ar_mLS CNNs_8 kn_4 flt_18 iRt_1.5 ds__338, 181, 30_ nE_60,60 bSz_32 bNm_0 i_4",
                        //    @"S:\Phys\FIV823 NeuroMito\4b M 20230408\acc_0.1758 CNN_Ar_sLS CNNs_8 kn_4 flt_26 iRt_1.5 ds__374, 96, 68_ nE_60,60 bSz_32 bNm_1 i_0",
                        //};
                    }

                    var Folders = new List<string>()
                    {
                        //@"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1357 CNN_Ar_mLS CNNs_8 kn_3 flt_36 iRt_1.25 ds__68, 45_ nE_53,150 bSz_32 bNm_0 i_2",  // # 7
                        @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1362 CNN_Nr_mLS CNNs_8 kn_3 flt_26 iRt_1.25 ds__12_ nE_59,150 bSz_32 bNm_0 i_13",     // # 6
                        @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1405 CNN_Nr_sLS CNNs_8 kn_3 flt_30 iRt_1.25 ds__9_ nE_53,150 bSz_32 bNm_0 i_19",      // # 5
                        //@"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1348 CNN_Ar_mLS CNNs_6 kn_3 flt_27 iRt_1.75 ds__1000, 497, 355, 124, 113, 61_ nE_55,150 bSz_32 bNm_0 i_14", //Next slowest # 4
                        @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230418\acc_0.1438 CNN_Nr_mLS CNNs_7 kn_2 flt_32 iRt_1.75 ds__218, 151, 56, 29_ nE_53,150 bSz_32 bNm_0 i_5", // # 3
                        @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230425\acc_0.1229 CNN_Ar_sLS CNNs_7 kn_3 flt_21 iRt_1.5 ds__316_ nE_131,250 bSz_32 bNm_0 i_9",    // # 2
                        @"S:\Phys\FIV823 NeuroMito\4cS1 M 20230425\acc_0.1143 CNN_Nr_sLS CNNs_7 kn_3 flt_26 iRt_1.5 ds__69, 51_ nE_160,250 bSz_32 bNm_0 i_14" // # 1
                    };
                    return Folders;
                }
            }

            public static List<string> Default_AllNeuriteMeasures_Paths
            {
                get
                {
                    var Folders = new List<string>();
                    string SearchStart = @"S:\Phys\FIV823 NeuroMito\";
                    foreach (var firstLevelFolder in Directory.GetDirectories(SearchStart))
                    {
                        // Now search within this folder for our desired folders
                        foreach (var secondLevelFolder in Directory.GetDirectories(firstLevelFolder))
                        {
                            var folderName = Path.GetFileName(secondLevelFolder);
                            if (folderName.Trim().ToUpper().StartsWith("ACC_"))
                                Folders.Add(secondLevelFolder);
                        }
                    }
                    return Folders;
                }
            }

        }

        public class TF_Model_Metadata
        {
            //Remember classes could also be measurement types

            [JsonIgnore]
            public int ClassesCount => Index2ClassInfo.Count;
            public Dictionary<int, TF_Model_ClassInfo> Index2ClassInfo { get; set; } //Better to not use this directly, use rebuilt one instead
            public string ModelName { get; set; }
            public string ModelPath { get; set; }
            public string ModelType { get; set; }
            public string ModelDate { get; set; }
            public int TrainingSize { get; set; }
            public string ColumnsIncluded { get; set; }
            public string ModelParams { get; set; }
            public string PreferredInputShape { get; set; }
            public Dictionary<int, List<string>> OutputMapping { get; set; }
            public List<string> InputChannelNames { get; set; }
            public string AdditionalNote { get; set; }
            public bool? WillModelRescale { get; set; }
            public bool? WillModelResize { get; set; }
            public bool? ModelExpects_ResizeByCropTrue_ZoomFalse { get; set; } //Older version
            public bool? ResizeByCropTrue_ZoomFalse { get; set; } //Newer version

            [JsonIgnore]
            private Dictionary<int, TF_Model_ClassInfo> _Rebuilt_Index2ClassInfo = null;
            [JsonIgnore]
            public Dictionary<int, TF_Model_ClassInfo> Rebuilt_Index2ClassInfo
            {
                get
                {
                    if (_Rebuilt_Index2ClassInfo == null)
                    {
                        if (OutputMapping == null || OutputMapping.Count == 0)
                        {
                            _Rebuilt_Index2ClassInfo = Index2ClassInfo;
                        }
                        else
                        {
                            //Issue is that the outputs are in a different arrangement than the training inputs to the model.  So we have to rebuild it to match correctly
                            _Rebuilt_Index2ClassInfo = new Dictionary<int, TF_Model_ClassInfo>(ClassesCount);
                            int c = 0;
                            for (int targetIdx = 0; targetIdx < 1000; targetIdx++)
                            {
                                if (!OutputMapping.ContainsKey(targetIdx)) continue;
                                for (int i = 0; i < OutputMapping[targetIdx].Count; i++)
                                {
                                    string name = OutputMapping[targetIdx][i];
                                    var CI = Index2ClassInfo.Where(x => x.Value.Name == name).FirstOrDefault().Value;
                                    _Rebuilt_Index2ClassInfo.Add(c++, CI);
                                }
                            }
                        }
                    }
                    return _Rebuilt_Index2ClassInfo;
                }
            }

            public bool DoRescale
            {
                get
                {
                    if (WillModelRescale == null) return false;
                    return !WillModelRescale.Value;
                }
            }

            public bool DoResize
            {
                get
                {
                    if (WillModelResize == null) return false;
                    return !WillModelResize.Value;
                }
            }

            public bool DoCropvsZoom
            {
                get
                {
                    if (ResizeByCropTrue_ZoomFalse == null)
                    {
                        if (ModelExpects_ResizeByCropTrue_ZoomFalse == null) return true;
                        return ModelExpects_ResizeByCropTrue_ZoomFalse.Value;
                    }
                    return ResizeByCropTrue_ZoomFalse.Value;
                }
            }


            /// <summary>
            /// Prediction probabilities above this amount will be called positive for this class, but can be adjusted with the per-class settings
            /// </summary>
            public float ThresholdDefault { get; set; }

            public TF_Model_Metadata()
            {
                Index2ClassInfo = new Dictionary<int, TF_Model_ClassInfo>();
                ThresholdDefault = 0.9F;
                ModelName = "";
                ModelType = "";
                ModelPath = "";
                AdditionalNote = "";
                ModelDate = DateTime.Now.ToShortDateString();
                TrainingSize = 0;
                ColumnsIncluded = "";
            }

            public TF_Model_ClassInfo this[int index]
            {
                get => Index2ClassInfo[index];
            }

            public static TF_Model_Metadata Example_acc81PerCNN()
            {
                var TFMM = new TF_Model_Metadata();

                TFMM.Add(0, "0 Empty", -1, new int[3] { 184, 118, 232 }, true);
                TFMM.Add(1, "0 Neurites No Cell", -1, new int[3] { 193, 73, 75 }, true);
                TFMM.Add(2, "1 Cell Tub No Neurite", -1, new int[3] { 255, 255, 0 }, false);
                TFMM.Add(3, "1 Dead (no tubulin)", -1, new int[3] { 113, 206, 14 }, true);
                TFMM.Add(4, "1 Neuron", -1, new int[3] { 0, 255, 30 }, false);
                TFMM.Add(5, "1 Neuron Poor Qual", -1, new int[3] { 0, 200, 0 }, false);
                TFMM.Add(6, "1 Cells Tub No Neurite", -1, new int[3] { 167, 122, 36 }, false);
                TFMM.Add(7, "2 Nuc 1+ Neuron", -1, new int[3] { 255, 30, 0 }, false);
                TFMM.Add(8, "2 Nuc 1+ Neuron Poor Qual", -1, new int[3] { 87, 138, 227 }, true);
                TFMM.Add(9, "3-4 Nuclei", -1, new int[3] { 250, 245, 255 }, true);
                TFMM.Add(10, "5+ Nuclei", -1, new int[3] { 62, 45, 26 }, true);
                TFMM.Add(11, "Focus (only if you really cant tell)", -1, new int[3] { 253, 246, 41 }, true);

                TFMM.ModelName = "acc=0.8177 CNN Cnns=6.5";
                TFMM.ThresholdDefault = 0.65F;
                TFMM.ModelType = "Multiclass";
                TFMM.ModelDate = DateTime.Now.AddDays(-30).ToLongTimeString();
                TFMM.ModelPath = "S:\\Raft\\FIV827\\M 20230124 MC\\m acc=0.8177 CNN Cnns=6.5\\frozen_graph.pb";
                return TFMM;
            }

            public void Add(int Idx, string Name, int TrainingCount, int[] RGB, bool ignore = false, string comment = "")
            {
                var TFCI = new TF_Model_ClassInfo() { Index = Idx, Name = Name, TrainingCount = TrainingCount, RGB = RGB, Comment = comment, ThresholdMultiplier = 1, Ignore = ignore };
                Index2ClassInfo.Add(Idx, TFCI);
            }

            public void Save(string FilePath, bool UseDefaultName = true)
            {
                string NewPath = UseDefaultName ? Path.Combine(Path.GetDirectoryName(FilePath), DefaultName) : FilePath;
                File.WriteAllText(NewPath, Serialize());
            }

            public static string DefaultName = "FIVE_metadata.json";

            public static TF_Model_Metadata LoadNeighbor(string FilepathOfPB)
            {
                string NewPath = Path.Combine(Path.GetDirectoryName(FilepathOfPB), DefaultName);
                return Load(NewPath);
            }

            public static TF_Model_Metadata Load(string Filepath)
            {
                if (!File.Exists(Filepath)) return null;
                TF_Model_Metadata TMM = null;
                try
                {
                    string json = File.ReadAllText(Filepath);
                    TMM = Deserialize(json);
                }
                catch
                {
                    //problem loading the json
                }

                if (TMM.AdditionalNote.Contains("5a6S0b") && TMM.WillModelResize == null)
                {
                    //Deal with some issues to bring this up-to-date
                    TMM.ModelName = Filepath.Split('\\')[5];
                    TMM.WillModelRescale = false; TMM.WillModelResize = false;
                    TMM.ModelExpects_ResizeByCropTrue_ZoomFalse = false;
                    TMM.InputChannelNames = new List<string>() { "Hoechst", "Tubulin", "Mito" };
                    TMM.OutputMapping = new Dictionary<int, List<string>>()
                    {
                        { 2, new List<string> { "Axon Length um" } },
                        { 3, new List<string> { "n Soma Mito", "Soma Mito LWR" } },
                        { 4, new List<string> { "n Proximal Mito" } },
                        { 5, new List<string> { "n MidNeurite Mito", "Neurite Mito LWR", "Mito Size" } },
                        { 6, new List<string> { "n Distal Mito" } }
                    };

                    //Fix the rescaling so we know what to divide by (256 vs 2^16)
                }

                return TMM;
            }

            public string Serialize()
            {
                var options = new JsonSerializerOptions { WriteIndented = true, Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) } };
                return JsonSerializer.Serialize<TF_Model_Metadata>(this, options);

            }

            public static TF_Model_Metadata Deserialize(string json)
            {
                return JsonSerializer.Deserialize<TF_Model_Metadata>(json);
            }

            public IEnumerator<TF_Model_ClassInfo> GetEnumerator()
            {
                return Index2ClassInfo.Values.GetEnumerator();
            }

            public string DeriveFullAnnotation(float[] allScores)
            {
                //This was changed from SortedDictionary, performance is one reason but more importantly dictionary is inappropriate without unique keys.
                //Floating point arithmetic already has accuracy problems using Float.Epsilon to create a set of Unique Keys is never a good idea. 

                List<(float Score, string ClassName)> scoresWithClasses = new List<(float Score, string ClassName)>();

                for (int i = 0; i < ClassesCount; i++)
                {
                    scoresWithClasses.Add((-allScores[i], Index2ClassInfo[i].Name));
                }

                // Sort by score in descending order (since scores were negated)
                scoresWithClasses.Sort((a, b) => a.Score.CompareTo(b.Score));

                // Create the annotation string
                return string.Join("\r\n", scoresWithClasses.Select(x => x.ClassName + " = " + (-x.Score).ToString("0.000")));
            }
        }

        public class TF_Model_ClassInfo
        {
            public TF_Model_ClassInfo()
            {
                Index = -1;
                TrainingCount = -1;
                RGB = new int[] { 0, 0, 0 };
                Ignore = false;
                ThresholdMultiplier = 1;
                Name = "";
                Comment = "";
                ScalerMin = 0;
                ScalerMax = 1;
            }

            public int Index { get; set; }
            public string Name { get; set; }
            public long TrainingCount { get; set; }
            public int[] RGB { get; set; }
            public string Comment { get; set; }
            /// <summary>
            /// This will multiply the default threshold being used overall (say 65% is needed for positive), but this multiplier. So for this particular class, if ThresholdMultiplier = 1.2, then the prediction would have to be over 78% to be called positive
            /// </summary>
            public float ThresholdMultiplier { get; set; }
            public float ScalerMin { get; set; }
            public float ScalerMax { get; set; }
            public bool Ignore { get; set; }

            public override string ToString() { return Name; }

            [JsonIgnore]
            public Color Color => Color.FromArgb(RGB[0], RGB[1], RGB[2]);
        }

        public class TF_Image_Model
        {
            private static string MultiTargetRegexPattern_Measures = @"GT(\d+)_targets";
            private static string MultiTargetRegexPattern_Images = @"GT(\d+)_image";
            private static string CombinedRegexPattern = @"GT(\d+)_(targets|image)";
            private static string AutoEncoderTarget = "z_mean";

            protected Tensorflow.Graph graph;
            protected Session sess = null;
            protected Tensor Input;
            protected Tensor[] Outputs;      //Numerical
            protected Tensor[] ImageOutputs; //Image - usually segmentation
            protected Tensor[] Outputs_Combined; //Numerical and Image

            public string ModelName;
            private string _ModelNameSafe = "";
            public string ModelNameSafe
            {
                get
                {
                    if (_ModelNameSafe == "") _ModelNameSafe = CleanFolderName(ModelName);
                    return _ModelNameSafe;
                }
            }
            public string ModelPath => Metadata.ModelPath;
            public string ModelPathFull;
            public TF_Model_Metadata Metadata;
            public bool InputDimensionsKnown;
            public long Input_Width;
            public long Input_Height;
            public long Input_Channels;
            
            public static string CleanFolderName(string input, int maxLength = -1)
            {
                if (input == null) throw new ArgumentNullException(nameof(input));
                char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();
                string cleanName = new string(input.Where(ch => !invalidChars.Contains(ch)).ToArray());
                cleanName = cleanName.TrimEnd('.', ' ');
                if (maxLength > 0 && cleanName.Length > maxLength)
                    cleanName = cleanName.Substring(0, maxLength);
                return cleanName;
            }

            public override string ToString()
            {
                return ModelName;
            }

            //Block Copying is a faster way to Slice Array Segments Ideally this would be in Utilities since it's a common operation.
            public static float[] Slice(float[,] multiArray, int index)
            {
                int length = multiArray.GetLength(1);
                float[] result = new float[length];
                int size = sizeof(float); // Size of one float in bytes

                // Copy the data starting from the index-th row
                System.Buffer.BlockCopy(multiArray, index * length * size, result, 0, length * size);

                return result;
            }

            public static void AAATest()
            {
                var TF = LoadFromFrozen(@"S:\Raft\FIV827\M 20230124 MC\m acc=0.8177 CNN Cnns=6.5\frozen_graph.pb");
                var (Paths, XTest) = LoadImages(@"S:\Raft\FIV827\001 SF Sort\SmallerTrainingSet", "*.bmp", 0.25F);
                var (NDres, _) = TF.Predict(XTest);
                var res2 = NDArrayConvert(NDres);
                SaveNDArrayToFile(Paths, NDres, @"c:\temp\pred02.txt");
            }

            /// <summary>
            /// Loads a Frozen TF.Keras Model (not savedmodel or .h5, has the .pb extension, but must be explicetly frozen)
            /// </summary>
            /// <param name="PathToPB">Full filename of frozen model file</param>
            /// <param name="InputByName">Leave this blank to use the first part of the graph</param>
            /// <param name="OutputByName">Leave this blank to use the last part of the graph</param>
            /// <returns></returns>
            //This Uses a single Iteration with one Regex and uses Regex Cacheing to more quickly load images. 
            //Although the Logical Concepts are sound I have no way of testing it. 
            public static TF_Image_Model LoadFromFrozen(string PathToPB, string InputByName = "", string OutputByName = "")

            {
                var TFIM = new TF_Image_Model();
                TFIM.graph = tf.Graph().as_default();
                TFIM.graph.Import(PathToPB);
                TFIM.ModelName = Path.GetFileNameWithoutExtension(PathToPB);
                TFIM.Metadata = TF_Model_Metadata.LoadNeighbor(PathToPB);
                TFIM.ModelPathFull = PathToPB;
                if (TFIM.Metadata.ModelName.Length > TFIM.ModelName.Length) TFIM.ModelName = TFIM.Metadata.ModelName;
                bool isVae = TFIM.Metadata.ModelType.Contains("VAE");
                //For saving metadata from here..
                //var TFMM = TF_Model_Metadata.Example_acc81PerCNN();
                //TFMM.Save("S:\\Raft\\FIV827\\M 20230124 MC\\m acc=0.8177 CNN Cnns=6.5\\"); //Needs the trailing slash

                //We may be able to just access the first and the last instead of these named ones . .
                if (InputByName == "") TFIM.Input = TFIM.graph.First();
                else TFIM.Input = TFIM.graph.OperationByName(InputByName); //"x"

                var sh = TFIM.Input.shape;
                TFIM.Input_Width = sh.dims[1]; TFIM.Input_Height = sh.dims[2]; TFIM.Input_Channels = sh.dims[3];
                TFIM.InputDimensionsKnown = TFIM.Input_Width > 0;

                if (OutputByName != "")
                {
                    TFIM.Outputs = new Tensor[] { TFIM.graph.OperationByName(OutputByName) };
                    //TFIM.Outputs = new Tensor[] { TFIM.graph.Last() }; //"model_2/dense_7/Softmax" //8/7/2023
                }
                else if (!isVae)
                {
                    var targetOperations = TFIM.graph
                        .Where(o => Regex.IsMatch(o.name, isVae ? AutoEncoderTarget : MultiTargetRegexPattern_Measures))
                        .GroupBy(o => int.Parse(Regex.Match(o.name, isVae ? AutoEncoderTarget : MultiTargetRegexPattern_Measures).Groups[1].Value))
                        .OrderBy(g => g.Key)
                        .Select(g => g.Last().output) // Select the last node from each group (i.e., BiasAdd), the output part makes it a tensor instead of an operation
                        .ToArray();
                    if (targetOperations.Any())
                        TFIM.Outputs = (Tensor[])targetOperations;
                    else
                        TFIM.Outputs = new Tensor[] { TFIM.graph.Last() };

                    var imageOperations = TFIM.graph
                        .Where(o => Regex.IsMatch(o.name, MultiTargetRegexPattern_Images))
                        .GroupBy(o => int.Parse(Regex.Match(o.name, MultiTargetRegexPattern_Images).Groups[1].Value))
                        .OrderBy(g => g.Key)
                        .Select(g => g.Last().output) // Select the last node from each group (i.e., BiasAdd), the output part makes it a tensor instead of an operation
                        .ToArray();
                    if (imageOperations.Any())
                        TFIM.ImageOutputs = (Tensor[])imageOperations;
                    else
                        TFIM.ImageOutputs = new Tensor[] { };
                    TFIM.Outputs_Combined = TFIM.Outputs.Concat(TFIM.ImageOutputs).ToArray();
                }
                else
                {
                    var targetOperations = TFIM.graph
                        .Where(o => Regex.IsMatch(o.name, isVae ? AutoEncoderTarget : MultiTargetRegexPattern_Measures));
                    var c = targetOperations.Select(o => o.output);
                    var d = c.ToArray().Last().outputs;
                    if (d.Any())
                        TFIM.Outputs = (Tensor[])d;
                    else
                        TFIM.Outputs = new Tensor[] { TFIM.graph.Last() };

                    TFIM.ImageOutputs = new Tensor[] { };
                    TFIM.Outputs_Combined = TFIM.Outputs.Concat(TFIM.ImageOutputs).ToArray();
                }

                return TFIM;
            }

            public static TF_Image_Model LoadFromFrozenFast(string PathToPB, string InputByName = "", string OutputByName = "")
            {
                var TFIM = new TF_Image_Model();

                TFIM.graph = tf.Graph().as_default();
                TFIM.graph.Import(PathToPB);
                TFIM.ModelName = Path.GetFileNameWithoutExtension(PathToPB);
                TFIM.Metadata = TF_Model_Metadata.LoadNeighbor(PathToPB);

                if (TFIM.Metadata.ModelName.Length > TFIM.ModelName.Length)
                    TFIM.ModelName = TFIM.Metadata.ModelName;

                // Determine the input tensor

                TFIM.Input = string.IsNullOrEmpty(InputByName) ? TFIM.graph.First() : TFIM.graph.OperationByName(InputByName);

                // Set input dimensions
                var sh = TFIM.Input.shape;
                TFIM.Input_Width = sh.dims[1];
                TFIM.Input_Height = sh.dims[2];
                TFIM.Input_Channels = sh.dims[3];
                TFIM.InputDimensionsKnown = TFIM.Input_Width > 0;

                // Process graph operations to get outputs
                if (!string.IsNullOrEmpty(OutputByName))
                {
                    TFIM.Outputs = new Tensor[] { TFIM.graph.OperationByName(OutputByName) };

                }
                else
                {
                    var measuresList = new List<Tensor>();
                    var imagesList = new List<Tensor>();
                    Dictionary<string, (bool Success, string Type)> regexMatchCache = new Dictionary<string, (bool Success, string Type)>();

                    foreach (Operation operation in TFIM.graph)
                    {
                        if (!regexMatchCache.TryGetValue(operation.name, out var cachedMatch))
                        {
                            Match match = Regex.Match(operation.name, CombinedRegexPattern);
                            cachedMatch = (match.Success, match.Success ? match.Groups[2].Value : null);
                            regexMatchCache[operation.name] = cachedMatch;
                        }

                        if (cachedMatch.Success)
                        {
                            if (cachedMatch.Type == "targets")
                                measuresList.Add(operation.output);
                            else if (cachedMatch.Type == "image")
                                imagesList.Add(operation.output);
                        }
                    }

                    TFIM.Outputs = measuresList.Any() ? measuresList.ToArray() : new Tensor[] { TFIM.graph.Last() };
                    TFIM.ImageOutputs = imagesList.ToArray();
                    TFIM.Outputs_Combined = TFIM.Outputs.Concat(TFIM.ImageOutputs).ToArray();
                }

                return TFIM;
            }
            //lets use np.Stack to improve performance
            /*
			public static (string[] Paths, NDArray ImgData) LoadImages(string FolderWithImages, string Extension = "*.bmp", float FractionToInfer = 1)
            {
								
								
								   
                string[] ArrayFileName_Test;
                NDArray x_test;

                var ArrayPre = Directory.GetFiles(FolderWithImages, Extension, SearchOption.AllDirectories);
                ArrayFileName_Test = SubsetFiles(ArrayPre, FractionToInfer);
																											
                x_test = LoadImage_Direct(ArrayFileName_Test);

                return (ArrayFileName_Test, x_test);
            }
            */
            //rewrote this to use np.Stack which should theoretically be more efficient but I am confused about this function
            //I'm not sure about the -2 and just kind of uncertain in general so feel free to revert this part
            /*
           public static NDArray LoadImage_Direct(string[] imgPaths, bool UseTF = false)
           {
               NDArray[] tempArray1 = new NDArray[imgPaths.Length];
               NDArray[] tempArray2 = new NDArray[imgPaths.Length - 2]; // Adjust the size as necessary

               if (UseTF)
               {
                   Graph graph = tf.Graph().as_default();
                   for (int i = 0; i < imgPaths.Length; i++)
                   {
                       tempArray1[i] = ReadTensorFromImageFile(imgPaths[i], graph);
                   }
                   // Dispose the graph when no longer needed
                   graph.Dispose();
               }
               else
               {
                   for (int i = 0; i < imgPaths.Length - 2; i++)
                   {
                       tempArray2[i] = ReadTensorFromImageFile_Direct(imgPaths[i]);
                   }
               }

               // Convert the array to NDArray at the end np.stack is more efficient
               return UseTF ? np.stack(tempArray1) : np.array(tempArray2);
           }
           */

            public static (string[] Paths, NDArray ImgData) LoadImages(string FolderWithImages, string Extension = "*.bmp", float FractionToInfer = 1)
            {
                int img_h = 128;
                int img_w = 128;
                int n_channels = 3;
                string[] ArrayFileName_Test;
                NDArray x_test;

                var ArrayPre = Directory.GetFiles(FolderWithImages, Extension, SearchOption.AllDirectories);
                ArrayFileName_Test = SubsetFiles(ArrayPre, FractionToInfer);
                x_test = np.zeros((ArrayFileName_Test.Length, img_h, img_w, n_channels), dtype: tf.float32);
                LoadImage_Direct(ArrayFileName_Test, x_test); //1.5 seconds for 170 ish images, TF version was slower

                return (ArrayFileName_Test, x_test);
            }

            public static (string[] Paths, NDArray ImgData) LoadImagesToND(Dictionary<string, Bitmap> bmps, double rescale = 1)
            {
                //this would have blown up a long time ago if height and width were different
                int minWidth = int.MaxValue;
                int minHeight = int.MaxValue;

                //not certain if an iteration is needed but this is taking like ~5ms so i'm not really concerned.
                foreach (KeyValuePair<string, Bitmap> kvp in bmps)
                {
                    int localWidth = kvp.Value.Width;
                    int localHeight = kvp.Value.Height;
                    minWidth = localWidth < minWidth ? localWidth : minWidth;
                    minHeight = localHeight < minHeight ? localHeight : minHeight;
                }
                int n_channels = 3;
                float[,,,] imgArray = new float[bmps.Count, minWidth, minHeight, n_channels];
                string[] ArrayFileName_Test = new string[bmps.Count];
                float fraction = (float)(1 / rescale);
                int index = -1;

                Parallel.ForEach(bmps, kvp => //This gives a significant boost when we have 16 cores and since we usually have ~30 bitmaps this is great for Parallel.
                {
                    float[,,] imgTensor = ArrayFromBitmap(kvp.Value, minWidth, minHeight, n_channels);
                    int localIndex = Interlocked.Increment(ref index);  // Atomically increment the index and get the new value

                    ArrayFileName_Test[localIndex] = kvp.Key;
                    for (int i = 0; i < minWidth; i++)
                    {
                        for (int j = 0; j < minHeight; j++)
                        {
                            for (int k = 0; k < n_channels; k++)
                            {
                                imgTensor[i, j, k] *= fraction; //Remember to Multiply 
                                imgArray[localIndex, i, j, k] = imgTensor[i, j, k];
                            }
                        }
                    }
                });

                // Convert the float array to an NDArray
                NDArray x_test = np.array(imgArray);
                var dims = x_test.shape;
                return (ArrayFileName_Test, x_test);
            }

            public static (string[] Paths, float[,,,] ImgData) LoadImagesToArray(Dictionary<string, Bitmap> bmps)
            {
                int img_w = bmps.First().Value.Width;
                int img_h = bmps.First().Value.Height;
                int n_channels = 3;

                float[,,,] Arr = new float[bmps.Count, img_h, img_w, n_channels];
                var ArrayFileName_Test = new List<string>(bmps.Count);
                foreach (var KVP in bmps)
                {
                    ArrayFileName_Test.Add(KVP.Key);
                    Add2ArrayfromBitmap(KVP.Value, img_w, img_h, n_channels, ref Arr, ArrayFileName_Test.Count - 1);
                }

                return (ArrayFileName_Test.ToArray(), Arr);
            }

            //It looks like the different numbers of bitmaps mean the session structure changes so we need to recreate session.
            public (NDArray[] outputs, NDArray[] images) Predict(NDArray x_test)
            {
                if (Outputs_Combined.Length == 1)
                {
                    sess = tf.Session(graph);
                    NDArray probility_result = sess.run(Outputs_Combined[0], (Input, x_test));
                    return (new NDArray[] { probility_result }, null);
                }
                else
                {
                    //NDArray[] results = sess.run(Outputs_Combined, new FeedItem(Input, x_test)); //Josh just had this and no sess def, Willie changed 3/8/2024
                    sess = tf.Session(graph);
                    NDArray[] results = sess.run(Outputs_Combined, (Input, x_test));
                    var outputs = results.Take(Outputs.Length).ToArray();
                    var images = results.Skip(Outputs.Length).ToArray();

                    return (outputs, images);
                }
            }

            public static (float[,] fullResults, int[] maxIndex, float[] maxVal) NDArrayConvert(NDArray[] ArrayOfNDToConvert)
            {
                if (ArrayOfNDToConvert.Length == 1)
                    return NDArrayConvert(ArrayOfNDToConvert[0]); // Single NDArray case

                long totalSamples = ArrayOfNDToConvert[0].shape[0];
                long totalFeatures = ArrayOfNDToConvert.Sum(ndArray => ndArray.shape[1]);
                float[,] result = new float[totalSamples, totalFeatures];

                long currentFeatureIndex = 0;
                for (int n = 0; n < ArrayOfNDToConvert.Length; n++)
                {
                    float[,] ndArray = (float[,])NDArrayConverter.ToMultiDimArray<float>(ArrayOfNDToConvert[n]);
                    int featureCount = (int)ArrayOfNDToConvert[n].shape[1];

                    for (int i = 0; i < totalSamples; i++)
                    {
                        for (int j = 0; j < featureCount; j++)
                        {
                            result[i, currentFeatureIndex + j] = ndArray[i, j];
                        }
                    }
                    currentFeatureIndex += featureCount;
                }

                // Initialize maxIndex and maxVal arrays (optional, based on your use case)
                int[] maxIndex = new int[totalSamples];
                float[] maxVal = new float[totalSamples];

                return (result, maxIndex, maxVal);
            }

            public static (float[,] fullResults, int[] maxIndex, float[] maxVal) NDArrayConvert(NDArray ArrayToConvert)
            {
                int rows = (int)ArrayToConvert.shape[0];
                int cols = (int)ArrayToConvert.shape[1];
                float[,] outside = new float[rows, cols];
                int[] maxIndex = new int[rows];
                float[] maxVal = new float[rows];

                // Convert the NDArray to a multi-dimensional array of floats
                float[,] floatArray = (float[,])NDArrayConverter.ToMultiDimArray<float>(ArrayToConvert);

                // Cast the converted array to a 2D float array


                for (int i = 0; i < rows; i++)
                {
                    float tMax = float.MinValue;
                    int jMax = 0;

                    for (int j = 0; j < cols; j++)
                    {
                        float value = floatArray[i, j];
                        outside[i, j] = value;
                        if (value > tMax)
                        {
                            tMax = value;
                            jMax = j;
                        }
                    }
                    maxIndex[i] = jMax;
                    maxVal[i] = tMax;
                }

                return (outside, maxIndex, maxVal);
            }

            public static void SaveNDArrSetToImages(NDArray[] ArrayToConvert, string FolderPath, string[] RowLabels, NDArray xImgs, Dictionary<string, Bitmap> originalBMPDict, NDArray[] PredictionsOrMeasures, bool CombineChannels = false, bool parallel = false, string MetaName = "")
            {
                if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);
                int maxVal = 255;
                int batchSize = (int)ArrayToConvert[0].dims[0];
                var tFont = new Font("Arial", 10); var tBrush = new SolidBrush(Color.Pink); var tPoint = new Point(8, 8);
                if (parallel)
                {
                    SaveNDArrSetToImagesParallel(ArrayToConvert, FolderPath, RowLabels, xImgs, originalBMPDict, PredictionsOrMeasures, CombineChannels);
                }
                for (int batchIdx = 0; batchIdx < batchSize; batchIdx++)
                {
                    if (CombineChannels)
                    {
                        int originalWidth = originalBMPDict[RowLabels[batchIdx]].Width; int originalHeight = originalBMPDict[RowLabels[batchIdx]].Height;

                        var combinedBmp = new Bitmap(originalWidth * (ArrayToConvert.Length + 1), originalHeight, PixelFormat.Format24bppRgb);
                        var combinedData = combinedBmp.LockBits(new Rectangle(0, 0, combinedBmp.Width, combinedBmp.Height), ImageLockMode.WriteOnly, combinedBmp.PixelFormat);

                        //CopyToBitmap(xImgs, combinedData, 0, batchIdx, maxVal); //This is similar to what we need, except that there is a channel dimension, and the first 3 channels can be put into the RGB and any more can be ignored
                        for (int arrayIdx = 0; arrayIdx < ArrayToConvert.Length; arrayIdx++)
                            CopyToBitmap(ArrayToConvert[arrayIdx], combinedData, (arrayIdx + 1) * originalWidth, batchIdx, maxVal);

                        combinedBmp.UnlockBits(combinedData);
                        string savePath = Path.Combine(FolderPath, RowLabels[batchIdx] + "_combined.jpg");
                        combinedBmp.Save(savePath);
                    }
                    else
                    {
                        for (int targetIdx = 0; targetIdx < ArrayToConvert.Length; targetIdx++)
                        {
                            var tnsr = ArrayToConvert[targetIdx];
                            //var min_val = tnsr.Min();
                            //var max_val = tnsr.Max();
                            //var normalized_arr = (tnsr - min_val) / (max_val - min_val);

                            var bmp = new Bitmap((int)tnsr.dims[1], (int)tnsr.dims[2], PixelFormat.Format24bppRgb);
                            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                            CopyToBitmap(tnsr, bmpData, 0, batchIdx, maxVal);
                            bmp.UnlockBits(bmpData);
                            var PMi = PredictionsOrMeasures[targetIdx];
                            using (var g = Graphics.FromImage(bmp))
                            {
                                string tVal = "";
                                for (int subMeasures = 0; subMeasures < PMi.dims[1]; subMeasures++) tVal += ((double)PMi[batchIdx, subMeasures]).ToString("0.0") + "\r\n";
                                g.DrawString(tVal, tFont, tBrush, tPoint);
                            }

                            string savePath = Path.Combine(FolderPath, MetaName + "." + RowLabels[batchIdx] + "_" + targetIdx.ToString() + ".jpg");
                            bmp.Save(savePath);
                        }
                    }
                    string savePath2 = Path.Combine(FolderPath, RowLabels[batchIdx].ToString() + ".jpg");
                    originalBMPDict[RowLabels[batchIdx]].Save(savePath2);
                }
            }

            public static void SaveNDArrSetToImagesParallel(NDArray[] ArrayToConvert, string FolderPath, string[] RowLabels, NDArray xImgs, Dictionary<string, Bitmap> originalBMPDict, NDArray[] PredictionsOrMeasures, bool CombineChannels = false)
            {
                int maxVal = 255;
                int batchSize = (int)ArrayToConvert[0].dims[0];
                var tFont = new Font("Arial", 10); var tBrush = new SolidBrush(Color.Pink); var tPoint = new Point(8, 8);
                ConcurrentDictionary<string, Bitmap> keyValuePairs = new ConcurrentDictionary<string, Bitmap>(originalBMPDict);
                Parallel.For(0, batchSize, batchIdx =>
                {
                    if (CombineChannels)
                    {
                        int originalWidth = keyValuePairs[RowLabels[batchIdx]].Width; int originalHeight = keyValuePairs[RowLabels[batchIdx]].Height;

                        var combinedBmp = new Bitmap(originalWidth * (ArrayToConvert.Length + 1), originalHeight, PixelFormat.Format24bppRgb);
                        var combinedData = combinedBmp.LockBits(new Rectangle(0, 0, combinedBmp.Width, combinedBmp.Height), ImageLockMode.WriteOnly, combinedBmp.PixelFormat);

                        //CopyToBitmap(xImgs, combinedData, 0, batchIdx, maxVal); //This is similar to what we need, except that there is a channel dimension, and the first 3 channels can be put into the RGB and any more can be ignored
                        for (int arrayIdx = 0; arrayIdx < ArrayToConvert.Length; arrayIdx++)
                            CopyToBitmap(ArrayToConvert[arrayIdx], combinedData, (arrayIdx + 1) * originalWidth, batchIdx, maxVal);

                        combinedBmp.UnlockBits(combinedData);
                        string savePath = Path.Combine(FolderPath, RowLabels[batchIdx] + "_combined.jpg");
                        combinedBmp.Save(savePath);
                    }
                    else
                    {
                        for (int targetIdx = 0; targetIdx < ArrayToConvert.Length; targetIdx++)
                        {
                            var bmp = new Bitmap((int)ArrayToConvert[targetIdx].dims[1], (int)ArrayToConvert[targetIdx].dims[2], PixelFormat.Format24bppRgb);
                            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                            CopyToBitmap(ArrayToConvert[targetIdx], bmpData, 0, batchIdx, maxVal);
                            bmp.UnlockBits(bmpData);
                            var PMi = PredictionsOrMeasures[targetIdx];
                            using (var g = Graphics.FromImage(bmp))
                            {
                                string tVal = "";
                                for (int subMeasures = 0; subMeasures < PMi.dims[1]; subMeasures++) tVal += ((double)PMi[batchIdx, subMeasures]).ToString("0.0") + "\r\n";
                                g.DrawString(tVal, tFont, tBrush, tPoint);
                            }

                            string savePath = Path.Combine(FolderPath, RowLabels[batchIdx] + "_" + targetIdx.ToString() + ".jpg");
                            bmp.Save(savePath);
                        }
                    }
                    string savePath2 = Path.Combine(FolderPath, RowLabels[batchIdx].ToString() + ".jpg");
                    keyValuePairs[RowLabels[batchIdx]].Save(savePath2);
                });
            }

            private static void CopyToBitmap_New(NDArray arr, BitmapData bmpData, int xOffset, int batchIdx, int maxVal = 255) //>>>>>>> 4b43e08 (Updated TFModel to increase efficiency
            {
                //int maxVal = 255;
                //int batchSize = (int)ArrayToConvert[0].dims[0];
                //var tFont = new Font("Arial", 10); var tBrush = new SolidBrush(Color.Pink); var tPoint = new Point(8, 8);
                //ConcurrentDictionary<string, Bitmap> keyValuePairs = new ConcurrentDictionary<string, Bitmap>(originalBMPDict);
                //Parallel.For(0, batchSize, batchIdx =>
                //{
                //    if (CombineChannels)
                //    {
                //        int originalWidth = keyValuePairs[RowLabels[batchIdx]].Width; int originalHeight = keyValuePairs[RowLabels[batchIdx]].Height;

                //        var combinedBmp = new Bitmap(originalWidth * (ArrayToConvert.Length + 1), originalHeight, PixelFormat.Format24bppRgb);
                //        var combinedData = combinedBmp.LockBits(new Rectangle(0, 0, combinedBmp.Width, combinedBmp.Height), ImageLockMode.WriteOnly, combinedBmp.PixelFormat);

                //        //CopyToBitmap(xImgs, combinedData, 0, batchIdx, maxVal); //This is similar to what we need, except that there is a channel dimension, and the first 3 channels can be put into the RGB and any more can be ignored
                //        for (int arrayIdx = 0; arrayIdx < ArrayToConvert.Length; arrayIdx++)
                //            CopyToBitmap(ArrayToConvert[arrayIdx], combinedData, (arrayIdx + 1) * originalWidth, batchIdx, maxVal);

                //        combinedBmp.UnlockBits(combinedData);
                //        string savePath = Path.Combine(FolderPath, RowLabels[batchIdx] + "_combined.jpg");
                //        combinedBmp.Save(savePath);
                //    }
                //    else
                //    {
                //        for (int targetIdx = 0; targetIdx < ArrayToConvert.Length; targetIdx++)
                //        {
                //            var bmp = new Bitmap((int)ArrayToConvert[targetIdx].dims[1], (int)ArrayToConvert[targetIdx].dims[2], PixelFormat.Format24bppRgb);
                //            var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                //            CopyToBitmap(ArrayToConvert[targetIdx], bmpData, 0, batchIdx, maxVal);
                //            bmp.UnlockBits(bmpData);
                //            var PMi = PredictionsOrMeasures[targetIdx];
                //            using (var g = Graphics.FromImage(bmp))
                //            {
                //                string tVal = "";
                //                for (int subMeasures = 0; subMeasures < PMi.dims[1]; subMeasures++) tVal += ((double)PMi[batchIdx, subMeasures]).ToString("0.0") + "\r\n";
                //                g.DrawString(tVal, tFont, tBrush, tPoint);
                //            }

                //            string savePath = Path.Combine(FolderPath, RowLabels[batchIdx] + "_" + targetIdx.ToString() + ".jpg");
                //            bmp.Save(savePath);
                //        }
                //    }
                //    string savePath2 = Path.Combine(FolderPath, RowLabels[batchIdx].ToString() + ".jpg");
                //    keyValuePairs[RowLabels[batchIdx]].Save(savePath2);
                //});
            }

            private static unsafe void CopyToBitmap(NDArray arr, BitmapData bmpData, int xOffset, int batchIdx, int maxVal = 255)
            {
                int width = (int)arr.dims[1];
                int height = (int)arr.dims[2];

                float[,,,] floats = (float[,,,])NDArrayConverter.ToMultiDimArray<float>(arr);
                int stride = bmpData.Stride;

                byte* ptr = (byte*)bmpData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    int yPos = y * bmpData.Stride;
                    for (int x = 0; x < width; x++)
                    {

                        float val = Math.Clamp(arr[batchIdx, y, x, 0], 0f, 1f);
                        byte value = (byte)(val * maxVal);

                        int position = yPos + ((x + xOffset) * 3);
                        ptr[position] = ptr[position + 1] = ptr[position + 2] = value; // Simplified assignment

                    }
                }
            }

            public static void SaveNDArrayToFile(string[] RowLabels, NDArray probility_result, string FileSavePath)
            {
                var sb = new StringBuilder();
                for (int i = 0; i < probility_result.shape[0]; i++)
                {
                    var file = RowLabels[i];
                    var res = probility_result[i].ToArray();
                    sb.Append(file + "\t");
                    foreach (var item in res) sb.Append(item + "\t");
                    sb.Append("\r\n");

                }
                File.WriteAllText(FileSavePath, sb.ToString());
            }

            public static string[] SubsetFiles(string[] FileList, float Fraction)
            {
                var _Rand = new Random(119);
                int newLength = (int)(FileList.Length * Fraction);
                var newList = new List<string>(newLength);
                foreach (var file in FileList)
                {
                    if (_Rand.NextDouble() < Fraction)
                    {
                        newList.add(file);
                        if (newList.Count >= newLength) break;
                    }
                }
                return newList.ToArray();
            }

            public static void LoadImage_Direct(string[] imgPaths, NDArray toReturn, bool UseTF = false)
            {
                if (UseTF)
                {
                    using var graph = tf.Graph().as_default();

                    for (int i = 0; i < imgPaths.Length; i++)
                    {
                        toReturn[i] = ReadTensorFromImageFile(imgPaths[i], graph);
                        if (i % 10 == 0) print(i);
                    }
                }
                else
                {
                    for (int i = 0; i < imgPaths.Length - 2; i++) //The -2 is to avoid a weird memory error, but it is incomplete
                    {
                        toReturn[i] = ReadTensorFromImageFile_Direct(imgPaths[i]);
                        if (i % 10 == 0) print(i);
                    }
                }
            }

            public static NDArray ReadTensorFromImageFile_Direct(string file_name)
            {
                using (var bitmap = new System.Drawing.Bitmap(file_name))
                {
                    return TensorfromBitmap(bitmap);
                }
            }

            public unsafe static float[,,] ArrayFromBitmap(Bitmap bitmap, int height, int width, int channels, bool parallel = false)
            {
                float[,,] data = new float[height, width, channels];
                BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

                int stride = bmpData.Stride;
                byte* ptr = (byte*)bmpData.Scan0; // Access the raw pixel data as a byte pointer

                // Use parallel processing to speed up the loop, make sure the bitmap is at least 1/8th of an overlay
                // if we are talking about something smaller than a 16 bit integer (less than 1/8th of an overlay) we are unlikely to see any meaningful benefit.
                if (parallel && height * width >= 65000)
                {
                    Parallel.For(0, height, y =>
                    {
                        byte* row = ptr + (y * stride);
                        for (int x = 0; x < width; x++)
                        {
                            int i = x * 3; // Calculate the correct position in the byte array
                            data[y, x, 0] = row[i + 2];  // blue channel
                            data[y, x, 1] = row[i + 1];  // green channel
                            data[y, x, 2] = row[i];      // red channel
                        }
                    });
                }
                else
                {
                    for (int y = 0; y < height; y++)
                    {
                        byte* row = ptr + (y * stride);
                        for (int x = 0; x < width; x++)
                        {
                            int i = x * 3; // Calculate the correct position in the byte array
                            data[y, x, 0] = row[i + 2];  // blue channel
                            data[y, x, 1] = row[i + 1];  // green channel
                            data[y, x, 2] = row[i];      // red channel
                        }
                    }
                }
                bitmap.UnlockBits(bmpData);
                return data;
            }

            //function that returns the concept of a Tensor as a Float Matrix instead of an NP array.
            public static float[,,] TensorArrayfromBitmap(Bitmap bitmap, int width = -1, int height = -1, int channels = -1)
            {
                float[,,] data = ArrayFromBitmap(bitmap, width < 1 ? bitmap.Width : width, height < 1 ? bitmap.Height : height, channels < 1 ? 1 : channels);
                return data;
            }

            public static NDArray TensorfromBitmap(Bitmap bitmap, int width = -1, int height = -1, int channels = -1)
            {
                var data = ArrayFromBitmap(bitmap, width < 1 ? bitmap.Width : width, height < 1 ? bitmap.Height : height, channels < 1 ? 1 : channels);
                var array = np.array(data);
                return array;
            }

            public static void Add2ArrayfromBitmap(Bitmap bitmap, int width, int height, int channels, ref float[,,,] tensor, int currentIndex)
            {
                var image = ArrayFromBitmap(bitmap, width, height, channels);
                Array.Copy(image, 0, tensor, currentIndex * height * width * channels, height * width * channels);
            }

            private static NDArray ReadTensorFromImageFile(string file_name, Graph graph)
            {
                if (Path.GetExtension(file_name) == ".db") return null;
                var file_reader = tf.io.read_file(file_name, "file_reader");
                var get_img = tf.image.decode_image(file_reader);
                var cast = tf.cast(get_img, tf.float32);

                using (var sess = tf.Session(graph))
                {
                    return sess.run(cast);
                }
            }

            public static List<TF_Image_Model> FromListOfPaths(List<string> Folders)
            {
                var models = new List<TF_Image_Model>();
                foreach (var path in Folders)
                {
                    var (success, FileOrFolderName) = GetModelFile_FromFolder(path);
                    if (!success)
                        continue;
                    var TFIM = TF_Image_Model.LoadFromFrozen(FileOrFolderName);
                    if (TFIM != null) models.add(TFIM);
                }

                return models;
            }

            public static (bool, string) GetModelFile_FromFolder(string FileOrFolderName)
            {
                if (!File.Exists(FileOrFolderName))
                {
                    if (Directory.Exists(FileOrFolderName))
                    {
                        var Fs = Directory.GetFiles(FileOrFolderName, "*.pb");
                        FileOrFolderName = Fs[0];
                    }
                    else
                    {
                        return (false, FileOrFolderName);
                    }
                }
                return (true, FileOrFolderName);
            }
        }

        public class TF_Measure_Results
        {
            public Dictionary<string, TF_Measure_Result_DataPoint> DataPoints;
            public ClassDefinition ClassDef;

            public TF_Measure_Results(ClassDefinition Classdef)
            {
                DataPoints = new Dictionary<string, TF_Measure_Result_DataPoint>();
                ClassDef = Classdef;
            }

            public void AddSet(TF_Image_Model Model, string[] y, float[,] results, Dictionary<string, ImageCheck_Point> annotations, Dictionary<string, double> milliLoad, Dictionary<string, double> milliCrop, double milliClassify, double milliMeasure = -1)
            {
                //The classes tell us what they are. We can now convert these results into the proper number using the scalers and go from there . . 
                for (int r = 0; r < y.Length; r++)
                {
                    var dataPre = TF_Image_Model.Slice(results, r);
                    if (Model.Metadata.DoRescale)
                    {
                        //Rescale the data
                        for (int c = 0; c < Model.Metadata.ClassesCount; c++)
                        {
                            float Max = Model.Metadata.Rebuilt_Index2ClassInfo[c].ScalerMax;
                            float Min = Model.Metadata.Rebuilt_Index2ClassInfo[c].ScalerMin;
                            if (Max == 0) Max = 1;
                            dataPre[c] = Min + (dataPre[c] * (Max - Min));
                        }
                    }
                    var ICP = (annotations == null) ? null : annotations[y[r]];
                    if (ICP == null) { ICP = new ImageCheck_Point() { RaftID = y[r] }; } //string AnnoT = (string)ICP.Annotations[0].Value;
                    //if (ICP.PlateID.Length > 10 || ICP.PlateID.StartsWith("20X")) //This was commented out on 5/21/2024, we aren't sure why it was there
                    //if (ICP.PlateID == "UNK") ICP.PlateID = shoobalooba
                    if (ICP.PlateID == "") ICP.PlateID = "UNK";
                    Add(Model, dataPre, ICP, milliLoad == null ? -1 : milliLoad[y[r]], milliCrop == null ? -1 : milliCrop[y[r]], milliClassify, milliMeasure);
                }
            }

            public void Add(TF_Image_Model Model, float[] Results, ImageCheck_Point ICP, double milliLoad_WholeBMP, double milliCropPart, double milliPredict_SingleCrop, double milliPredictMeasure_SingleCrop = -1)
            {
                var DP = new TF_Measure_Result_DataPoint(this, Model)
                {
                    Meta = ICP,
                    data = Results,
                    milliLoad_WholeBMP = milliLoad_WholeBMP,
                    milliCrop_Part = milliCropPart,
                    milliPred_singleCrop = milliPredict_SingleCrop,
                    milliPredictMeasure_singleCrop = milliPredictMeasure_SingleCrop
                };
                DP.class_label = ClassDef == null ? "UNK" : ClassDef.GetClassLabel(ICP.PlateID, ICP.Well_Label);
                Add(DP);
            }

            public string KeyMakerDP(TF_Measure_Result_DataPoint Datapoint)
            {
                return KeyMaker(Datapoint.Meta.RaftID, Datapoint.Model.ModelName);
            }

            public static string KeyMaker(string UniqueID, string ModelName)
            {
                return UniqueID + "_" + ModelName;
            }

            public void Add(TF_Measure_Result_DataPoint Datapoint)
            {
                string key = KeyMakerDP(Datapoint);
                if (DataPoints.ContainsKey(key))
                {
                    //Debugger.Break(); //Why is there more than 1 of these?
                }
                else
                {
                    DataPoints.Add(key, Datapoint);
                }
            }

            public static char Delim = '\t';

            public string Export(string PathToExport, string FileName = "ModelMeasure01.txt")
            {
                var sB = new StringBuilder(); var sBH = new StringBuilder(); string headers = "";
                if (FileName == null || FileName == "") FileName = "ModelMeasure01.txt";
                var dt = DateTime.Now.ToString();
                void WriteRow(TF_Measure_Result_DataPoint DP, bool Headers)
                {
                    var Model = (DP == null ? DataPoints.First().Value.Model : DP.Model);
                    bool isVae = Model.Metadata.ModelType == "VAE 01";
                    if (Headers)
                        sBH.Append("PlateID" + Delim + "WELL LABEL" + Delim + "ROW" + Delim + "COLUMN" + Delim + "FOV" + Delim + "RaftID" + Delim + "OBJECT ID" + Delim + "mlClassLayout" + Delim + "RunDate" + Delim + "Row Timestamp" + Delim + "Model Name Short" + Delim + "Model Name" + Delim + "Model Path" + Delim + "Annotation" + Delim + "A Score" + Delim + "A Full" + Delim + "Anno Source" + Delim + "Load ms (whole BMP)" + Delim + "Crop ms (per)" + Delim + "Pred ms (SingleCrop)" + Delim + "Pred2 ms (SingleCrop)" + Delim);
                    else
                    {
                        var M = DP.Meta; var A = DP.Meta.Annotations.Count == 0 ? new ImageCheck_Annotation() : DP.Meta.Annotations[0];
                        string aNote = A.Note.Replace("\r\n", "|"); aNote = aNote.Replace("\n", "|");
                        sB.Append(M.PlateID + Delim + M.Well_Label + Delim + M.Well_Row + Delim + M.Well_Col + Delim + M.FOV + Delim + M.RaftID + Delim + "0" + Delim + DP.class_label + Delim + dt + Delim + DP.created.ToLongTimeString() + Delim + Model.ModelName + Delim + Model.Metadata.ModelName + Delim + Model.Metadata.ModelPath + Delim + A.Value + Delim + A.Score + Delim + aNote + Delim + A.Name + Delim + DP.milliLoad_WholeBMP + Delim + DP.milliCrop_Part + Delim + DP.milliPred_singleCrop + Delim + DP.milliPredictMeasure_singleCrop + Delim);
                    }
                    for (int c = 0; isVae ? c < DataPoints.First().Value.data.Length : c < Model.Metadata.ClassesCount; c++)
                    {
                        if (Headers)
                            sBH.Append(isVae? $"LE_{c}" + Delim : Model.Metadata.Rebuilt_Index2ClassInfo[c].Name + Delim);
                        else
                            sB.Append(DP.data[c].ToString() + Delim);
                    }

                    sB.Append("\r\n");
                    if (Headers) { sBH.Append("\r\n"); headers = sBH.ToString(); }
                    else { DP.Exported = true; }
                }

                //Export results that haven't been exported (Headers come later only if needed)
                foreach (var DP in DataPoints.Values) 
                    if (!DP.Exported) 
                        WriteRow(DP, false);
                if (sB.Length == 0) return ""; //Probably already all exported

                if (!Directory.Exists(PathToExport)) Directory.CreateDirectory(PathToExport);
                string ExportFullPath = Path.Combine(PathToExport, FileName);
                
                if (File.Exists(ExportFullPath))
                {
                    File.AppendAllText(ExportFullPath, sB.ToString()); //Just append
                }
                else
                {
                    WriteRow(null, true); //Store headers 
                    File.WriteAllText(ExportFullPath, headers + sB.ToString());
                }
                return ExportFullPath;
            }

            internal TF_Measure_Result_DataPoint GetDataPoint(string UniqueID, string SecondaryKey = "")
            {
                return null;
            }
        }

        public class TF_Measure_Result_DataPoint
        {
            public TF_Measure_Results Parent;
            public TF_Image_Model Model;

            public string Other = "";
            public ImageCheck_Point Meta = new ImageCheck_Point();
            public double milliLoad_WholeBMP = 0;
            public double milliCrop_Part = 0;
            public double milliPred_singleCrop = 0;
            public double milliPredictMeasure_singleCrop = 0;
            public string class_label;
            public DateTime created { get; private set; }
            public bool Exported;

            public float[] data;

            public TF_Measure_Result_DataPoint(TF_Measure_Results parent, TF_Image_Model model)
            {
                created = DateTime.Now;
                Parent = parent;
                Model = model;
                Exported = false;
            }
        }
    }

}
