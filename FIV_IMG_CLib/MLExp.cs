﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIVE
{

    namespace ActivateEnviron
    {
        public class TFSimple
        {
            /// <summary>
            /// This triggers an Anaconda environment and runs a Python Command in it (good for batch, but seems computer specific and therefore not ideal)
            /// </summary>
            public void Go()
            {
                //https://stackoverflow.com/questions/49082312/activating-conda-environment-from-c-sharp-code-or-what-is-the-differences-betwe
                // Set working directory and create process
                var workingDirectory = Path.GetFullPath("Scripts");
                var process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "cmd.exe",
                        RedirectStandardInput = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        WorkingDirectory = workingDirectory
                    }
                };
                process.Start();
                // Pass multiple commands to cmd.exe
                using (var sw = process.StandardInput)
                {
                    if (sw.BaseStream.CanWrite)
                    {
                        // Vital to activate Anaconda
                        sw.WriteLine("C:\\PathToAnaconda\\anaconda3\\Scripts\\activate.bat");
                        // Activate your environment
                        sw.WriteLine("activate your-environment");
                        // Any other commands you want to run
                        sw.WriteLine("set KERAS_BACKEND=tensorflow");
                        // run your script. You can also pass in arguments
                        sw.WriteLine("python YourScript.py");
                    }
                }

                // read multiple output lines
                while (!process.StandardOutput.EndOfStream)
                {
                    var line = process.StandardOutput.ReadLine();
                    Console.WriteLine(line);
                }
            }
        }
    }

    namespace MLProjects_3DBlocks
    {
        public class BlockSetCollection : IEnumerable<BlockSet>
        {
            private List<BlockSet> _List;

            public int Count => _List.Count;

            public BlockSetCollection()
            {
                _List = new List<BlockSet>();
            }

            public void Add(BlockSet BSet) { _List.Add(BSet); }

            public BlockSet this[int index]
            {
                get { return _List[index]; }
            }

            /// <summary>
            /// Exports the sparse representation of the blocks
            /// </summary>
            public string Export()
            {
                var sB = new StringBuilder();
                bool first = true;
                foreach (var BlockSet in this)
                {
                    sB.Append(BlockSet.Export(first));
                    first = false;
                }
                return sB.ToString();
            }

            /// <summary>
            /// Exports the fully encoded tensor version
            /// </summary>
            public string Tensor()
            {
                int MinEachDimension = (int)Math.Min(Math.Min(this.Min(x => x.X_Min), this.Min(x => x.Y_Min)), this.Min(x => x.Z_Min));
                int MaxEachDimension = (int)Math.Max(Math.Max(this.Max(x => x.X_Max), this.Max(x => x.Y_Max)), this.Max(x => x.Z_Max));

                //MinEachDimension = -4;
                //MaxEachDimension = 4;

                var sB = new StringBuilder();
                bool first = true;
                foreach (var BlockSet in this)
                {
                    sB.Append(BlockSet.Tensor(first, MinEachDimension, MaxEachDimension));
                    first = false;
                }
                return sB.ToString();
            }

            public static void BlockSetGo(int BlockSets, int BlocksPerSet)
            {
                var BSC = new BlockSetCollection();
                for (int i = 0; i < BlockSets; i++)
                {
                    BSC.Add(BlockSet.GenerateInterconnected(BlocksPerSet));
                    BSC[BSC.Count - 1].Index = i;
                    if (i % 50 == 0) System.Diagnostics.Debug.Print(i.ToString());
                }
                string Fldr = @"R:\FIVE\EXP\FIV688\1 Data\";
                string after = "05";
                System.IO.File.WriteAllText(Path.Combine(Fldr, "Blocks3D_" + after + ".txt"), BSC.Export());
                System.IO.File.WriteAllText(Path.Combine(Fldr, "Tensor3D_" + after + ".txt"), BSC.Tensor());
            }

            public IEnumerator<BlockSet> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _List.GetEnumerator();
            }
        }

        public class BlockSet : IEnumerable<Block>
        {
            private Dictionary<XYZ, Block> _Dict;

            public List<Side> Sides_Open;
            public List<Side> Sides_Connected;
            public int Count => _Dict.Count;

            public string Name { get; set; }

            public int Index { get; set; }

            public int Leaves => this.Count(x => x.ConnectedSides == 1);

            public int MaxDistToLeaves => this.Max(x => x.DistToLeaf);

            public double X_Min => _Dict.Keys.Min(k => k.X);
            public double Y_Min => _Dict.Keys.Min(k => k.Y);
            public double Z_Min => _Dict.Keys.Min(k => k.Z);
            public double X_Max => _Dict.Keys.Max(k => k.X);
            public double Y_Max => _Dict.Keys.Max(k => k.Y);
            public double Z_Max => _Dict.Keys.Max(k => k.Z);

            private static Random Rnd = new Random();

            public static BlockSet GenerateInterconnected(int TotalBlocks)
            {
                var BSet = new BlockSet(1);
                for (int i = 0; i < TotalBlocks - 1; i++)
                {
                    var AddFromSide = BSet.Sides_Open[Rnd.Next(0, BSet.Sides_Open.Count)];
                    BSet.AddNeighbor(AddFromSide);
                }

                // - - - - -  Add Distance to Leaf
                Calculate_DistanceToLeaf(BSet);
                return BSet;
            }

            private static void Calculate_DistanceToLeaf(BlockSet BSet)
            {
                int NeedsDistToLeaf = 0;
                //First add the leaves
                foreach (var block in BSet) if (block.ConnectedSides == 1) block.DistToLeaf = 0; else { block.DistToLeaf = int.MaxValue; NeedsDistToLeaf++; }
                //Sometimes, there are NO leaves
                if (NeedsDistToLeaf == BSet.Count)
                {
                    foreach (var block in BSet) block.DistToLeaf = 2;
                    return;
                }

                while (NeedsDistToLeaf > 0)
                {
                    SetLeafDist(BSet, true);
                }
                SetLeafDist(BSet, false); SetLeafDist(BSet, false);
                SetLeafDist(BSet, false); //Do this thrice to clean things up

                void SetLeafDist(BlockSet BSet, bool OnlyUnSet)
                {
                    foreach (var block in OnlyUnSet ? BSet.Where(x => x.DistToLeaf > BSet.Count) : BSet.Where(x => x.DistToLeaf > 0))
                    {
                        int dMin = block.Neighbors.Values.Min(x => x.DistToLeaf);
                        if (dMin == int.MaxValue) continue;
                        block.DistToLeaf = dMin + 1;
                        NeedsDistToLeaf--;
                    }
                }
            }

            public BlockSet(int CubeStartSize = 1)
            {
                _Dict = new Dictionary<XYZ, Block>();
                Sides_Open = new List<Side>();
                Sides_Connected = new List<Side>();

                if (CubeStartSize > 1)
                {
                    System.Diagnostics.Debugger.Break(); //NOT IMPLEMENTED
                }

                int start = (int)Math.Round((double)CubeStartSize / 2, 0);
                for (int i = start; i < start + CubeStartSize; i++)
                    Add(new Block(this, i, i, i));
            }

            public void Add(Block block)
            {
                _Dict.Add(block.XYZ, block);
                foreach (var Side_Connect in block.Sides_Connected)
                {
                    if (Sides_Connected.Contains(Side_Connect.Key)) continue;
                    if (Side_Connect.Value)
                    {
                        Sides_Open.Remove(Side_Connect.Key);
                        Sides_Connected.Add(Side_Connect.Key);
                    }
                    else
                    {
                        Sides_Open.Add(Side_Connect.Key);
                        Sides_Connected.Remove(Side_Connect.Key);
                    }
                }
                Recheck_Sides();
            }

            public Block AddNeighbor(Side AddBlockToThisExistingSide)
            {
                var bl = new Block(AddBlockToThisExistingSide);
                Add(bl);

                return bl;
            }

            public void Recheck_Sides()
            {
                //The issue is that sometimes you add a neighbor and you realize one of the sides, but other sides have to be connected as well
                foreach (var block in this)
                {
                    foreach (var Side in block.Sides.Values)
                    {
                        if (!block.Sides_Connected[Side])
                        {
                            if (_Dict.ContainsKey(Side.NeighborXYZ))
                            {
                                block.AddNeighbor(Side.SType, _Dict[Side.NeighborXYZ]);
                            }
                        }
                    }
                }
            }

            public string Export(bool ExportHeader = false)
            {
                var sB = new StringBuilder();
                bool First = ExportHeader;
                foreach (var block in this)
                {
                    if (First)
                    {
                        sB.Append("BSetIdx" + Block.d + "Name" + Block.d + "Blocks" + Block.d + "Leaves" + Block.d + "MaxDist2Leaves" + Block.d);
                        sB.Append(block.Export(true) + "\r\n");
                        First = false;
                    }
                    sB.Append(Index.ToString() + Block.d + Name + Block.d + Count + Block.d + Leaves + Block.d + MaxDistToLeaves + Block.d);
                    sB.Append(block.Export() + "\r\n");
                }
                return sB.ToString();
            }

            public string Tensor(bool ExportHeader = false, int minEach = int.MaxValue, int maxEach = int.MinValue)
            {
                var sB = new StringBuilder();

                //Now we re-encode this into the full set
                for (int i = ExportHeader ? 0 : 1; i < 2; i++)
                {
                    if (i == 0)
                        sB.Append("BSetIdx" + Block.d + "Name" + Block.d + "Blocks" + Block.d + "Leaves" + Block.d + "MaxDist2Leaves" + Block.d);
                    else
                        sB.Append(Index.ToString() + Block.d + Name + Block.d + Count + Block.d + Leaves + Block.d + MaxDistToLeaves + Block.d);
                    for (int x = (minEach == int.MaxValue ? (int)this.X_Min : minEach); x <= (maxEach == int.MinValue ? (int)this.X_Max : maxEach); x++)
                    {
                        for (int y = (minEach == int.MaxValue ? (int)this.Y_Min : minEach); y <= (maxEach == int.MinValue ? (int)this.Y_Max : maxEach); y++)
                        {
                            for (int z = (minEach == int.MaxValue ? (int)this.Z_Min : minEach); z <= (maxEach == int.MinValue ? (int)this.Z_Max : maxEach); z++)
                            {
                                var Key = new XYZ(x, y, z);
                                int Val = _Dict.ContainsKey(Key) ? 1 : 0;

                                if (i == 0)
                                    sB.Append(Key.ToString() + Block.d); //headers
                                else
                                    sB.Append(Val.ToString() + Block.d); //values
                            }
                        }
                    }
                    sB.Append("\r\n");
                }
                return sB.ToString();
            }

            public IEnumerator<Block> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

        }

        public class Block
        {
            public override string ToString()
            {
                return XYZ + "  n=" + Neighbors.Count;
            }

            private void Init()
            {
                XYZ = new XYZ(double.NaN, double.NaN, double.NaN);
                Sides = new Dictionary<BlockSideType, Side>();
                Neighbors = new Dictionary<BlockSideType, Block>();
                Sides_Connected = new Dictionary<Side, bool>();
                Parent = null;
                Index = -1;
            }

            private void SetupSides()
            {
                for (int i = 0; i < Side.BlockSideCount; i++)
                {
                    var tS = new Side(this, (BlockSideType)i);
                    Sides.Add(tS.SType, tS);
                    Sides_Connected.Add(tS, false);
                }
            }

            public Block()
            {
                Init();
                SetupSides();
            }

            public Block(BlockSet Parent, double x, double y, double z)
            {
                Init();
                XYZ = new XYZ(x, y, z);
                this.Parent = Parent;
                this.Index = Parent.Count;
                SetupSides();
            }

            public Block(Side Neighborside)
            {
                Init();
                this.Parent = Neighborside.Parent.Parent;
                this.Index = Parent.Count;
                XYZ = Neighborside.NeighborXYZ;
                SetupSides();

                AddNeighbor(Side.Cognate[Neighborside.SType], Neighborside.Parent); //Add neighbor to me
            }

            public void AddNeighbor(BlockSideType SType, Block NewNeighbor)
            {
                //Add it to the list
                Neighbors.Add(SType, NewNeighbor);

                //Signal my side as connected
                var sd = Sides[SType];
                Sides_Connected[sd] = true;
                Parent.Sides_Open.Remove(sd);
                Parent.Sides_Connected.Add(sd);

                if (!NewNeighbor.Neighbors.ContainsValue(this))
                    NewNeighbor.AddNeighbor(Side.Cognate[SType], this);
            }

            public const char d = '\t';

            public string Export(bool Headers = false)
            {
                bool H = Headers;
                return (H ? "Index" : Index.ToString()) + d + (H ? "X" : XYZ.X.ToString()) + d + (H ? "Y" : XYZ.Y.ToString()) + d + (H ? "Z" : XYZ.Z.ToString()) + d + (H ? "Sides" : ConnectedSides) + d + (H ? "DistToLeaf" : DistToLeaf) + d + (H ? "Attrib" : Attrib);
            }

            public XYZ XYZ;

            public int Index;

            public string Attrib = "";

            public int ConnectedSides => Sides_Connected.Values.Where(x => x == true).Count();

            public int DistToLeaf { get; set; }

            public Dictionary<BlockSideType, Side> Sides { get; set; }

            public Dictionary<Side, bool> Sides_Connected { get; set; }

            public Dictionary<BlockSideType, Block> Neighbors { get; set; }

            public BlockSet Parent { get; set; }
        }

        public class Side
        {
            public override string ToString()
            {
                return SType.ToString();
            }

            private XYZ _NeighborXYZ = new XYZ(double.NaN, 0, 0);

            /// <summary>
            /// Gets the coordinates that a neighbor would have on this side
            /// </summary>
            public XYZ NeighborXYZ
            {
                get
                {
                    if (double.IsNaN(_NeighborXYZ.X))
                    {
                        //Take the parents coordinates, and add 1 or subtract based on which side
                        double x = Parent.XYZ.X; double y = Parent.XYZ.Y; double z = Parent.XYZ.Z;
                        switch (SType)
                        {
                            case BlockSideType.u:
                                z--;
                                break;
                            case BlockSideType.d:
                                z++;
                                break;
                            case BlockSideType.l:
                                x--;
                                break;
                            case BlockSideType.r:
                                x++;
                                break;
                            case BlockSideType.f:
                                y++;
                                break;
                            case BlockSideType.b:
                                y--;
                                break;
                            default:
                                break;
                        }
                        _NeighborXYZ = new XYZ(x, y, z);
                    }
                    return _NeighborXYZ;
                }
            }

            public Block Parent;

            public BlockSideType SType;

            public Side(Block ParentBlock, BlockSideType type)
            {
                Parent = ParentBlock;
                SType = type;
            }

            public const int BlockSideCount = 6;

            public static Dictionary<BlockSideType, BlockSideType> Cognate =
                new Dictionary<BlockSideType, BlockSideType>() {
                    { BlockSideType.u, BlockSideType.d }, { BlockSideType.d, BlockSideType.u },
                    { BlockSideType.l, BlockSideType.r }, {BlockSideType.r, BlockSideType.l},
                    { BlockSideType.b, BlockSideType.f }, {BlockSideType.f, BlockSideType.b}
                };
        }

        public struct XYZ
        {
            public double X;
            public double Y;
            public double Z;

            public XYZ(double x, double y, double z)
            {
                X = x; Y = y; Z = z;
            }

            public override string ToString()
            {
                return X + "," + Y + "," + Z;
            }
        }

        public enum BlockSideType
        {
            u = 0,
            d = 1,
            l = 2,
            r = 3,
            f = 4,
            b = 5
        }
    }
}
