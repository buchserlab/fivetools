﻿using FIVE.InCellLibrary;
using Npgsql;
using NpgsqlTypes;
using System.Collections;
using System.Text;

namespace FeatureDBLib
{
    public class FeaturedB
    {
        public Simple_Collector Collector = new Simple_Collector();
        public dBWrap dB;

        public FeaturedB()
        {

        }

        public static FeaturedB OpenDefaultConnection()
        {
            FeaturedB FdB = new FeaturedB();
            FdB.dB = new dBWrap("Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=ge2;TrustServerCertificate=true;");
            FdB.dB.SimulationOnly = false;
            //FdB.dB = new dBWrap("Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=mlab4;TrustServerCertificate=true;");
            return FdB;
        }

        public static FeaturedB OpenConnection(string fs_dbname, string fs_Port, string fs_Username, string fs_Password, string Server = "localhost")
        {
            FeaturedB FdB = new FeaturedB();
            FdB.dB = new dBWrap($"Server={Server};Port={fs_Port};Database={fs_dbname};UserId={fs_dbname};Password={fs_Password};TrustServerCertificate=true;");
            FdB.dB.SimulationOnly = false;
            return FdB;
        }

        public bool DeleteTables()
        {
            //Deletes all the tables that are made by InsertingFromInCarta
            PG_Cols.DeleteAllTables(dB.Connections[0]);
            return true;
        }

        public string InsertFromFile_InCarta(string PathTo_RenameNorm_File, System.ComponentModel.BackgroundWorker? BW = null)
        {
            FileInfo FI = new FileInfo(PathTo_RenameNorm_File); if (!FI.Exists) return "File Not Found";
            if (BW != null) { BW.ReportProgress(0, "Loading File . . "); if (BW.CancellationPending) return "Cancelled"; }
            Collector = Simple_Collector.LoadInCartaFile(PathTo_RenameNorm_File);

            // - - - 
            //Select PlateID Table
            dB.Open();

            //Add any new cell data // Works recursively //ProcessLevel_Insert(Collector); //11 minutes on this dataset of 30K cells - 
            //Add all the tables from largest first, using Copy (not Insert) - took 800ms!
            if (BW != null) { BW.ReportProgress(10, "Inserting new Cells into dB Hierarchy . . "); if (BW.CancellationPending) return "Cancelled pre Cell Metadata"; }
            ISimpleBase Level = Collector.First();
            do
            {
                ProcessLevel_Copy(Level);
                if (Level.Table_Name == Simple_Cell.TableName) break;
                Level = ((IEnumerable<ISimpleBase>)Level).First();
            } while (true);

            // - - - - - - - - - - - - - - - 
            //Select the main Feature Tables
            if (BW != null) { BW.ReportProgress(40, $"Inserting new Measurements ({Collector.Headers.Cols_Measurement.Count}) . . "); if (BW.CancellationPending) return "Cancelled pre Measurement Table"; }
            SetupMeasurementTable();

            // - - - 
            //Add the new data entries to the large table
            if (BW != null) { BW.ReportProgress(50, $"Inserting Data (slowest step, {Collector.Cells.Count()} cells) . . "); if (BW.CancellationPending) return "Cancelled Pre Measurement Data"; }
            AddMeasurementData(BW);

            if (BW != null) BW.ReportProgress(100, "Done!");
            return "";
        }

        

        private void SetupMeasurementTable()
        {
            Dictionary<string, int> indB_MeasureIDs; PG_Cols MeasureCols;
            GetTableIDs_Measures(Collector.Headers.Cols_Measurement, out indB_MeasureIDs, out MeasureCols); //This gets the list of wtUIDs and dbIDs from the database
            //Add any feature information that isn't there already, or connect the existing with the database
            var ValList = new List<Dictionary<string, object>>();
            int idMeas = (indB_MeasureIDs.Count == 0) ? 0 : indB_MeasureIDs.Values.Max() + 1;
            foreach (var hInfo in Collector.Headers.Cols_Measurement)
            {
                if (!indB_MeasureIDs.ContainsKey(hInfo.HeaderName))
                {
                    var Vals = new Dictionary<string, object>();
                    Vals.Add("id", ++idMeas); Vals.Add("name", hInfo.HeaderName); ValList.Add(Vals);
                    indB_MeasureIDs.Add(hInfo.HeaderName, idMeas);
                }
                hInfo.Id = indB_MeasureIDs[hInfo.HeaderName];
            }
            if (ValList.Count > 0) MeasureCols.InsertMulti(ValList, dB.Connections[1]); //Here is the "INSERT" step
        }

        public void GetTableIDs_Measures(List<Header_Info> cols_Measurement, out Dictionary<string, int> indB_MeasureIDs, out PG_Cols Cols)
        {
            NpgsqlConnection Conn = dB.Connections[0];
            Cols = PG_Cols.FromHeaderSet(Header_Info.TableName, Header_Info.ListOfNames_Types, false);
            Cols.CreateTable(Conn); //Create this if it doesn't exist
            indB_MeasureIDs = Cols.Query2Dict(Cols[0], Cols[1], Conn);
        }

        private void AddMeasurementData(System.ComponentModel.BackgroundWorker? BW = null)
        {
            Dictionary<string, int> indB_DataIDs; PG_Cols DataCols;
            GetTableIDs_Data(Collector.Headers.Cols_Measurement, out indB_DataIDs, out DataCols); 
            var ValList = new List<Dictionary<string, object>>();            
            foreach (Simple_Cell Cell in Collector.Cells)
            {
                if (BW != null) { if (BW.CancellationPending) return; }
                foreach (var Meas in Collector.Headers.Cols_Measurement)
                {
                    var Vals = new Dictionary<string, object>();
                    Vals.Add(FSDataTable.ListOfNames_Types[0].Item1, Cell.dbID); 
                    Vals.Add(FSDataTable.ListOfNames_Types[1].Item1, Meas.Id); 
                    Vals.Add(FSDataTable.ListOfNames_Types[2].Item1, double.Parse(Cell.Data_FromHeaderInfo(Meas).ToString()));
                    ValList.Add(Vals);
                }
            }
            if (ValList.Count > 0) DataCols.InsertMulti(ValList, dB.Connections[1]); //Here is the "INSERT" step
        }

        public void GetTableIDs_Data(List<Header_Info> cols_Measurement, out Dictionary<string, int> indB_DataIDs, out PG_Cols Cols)
        {
            NpgsqlConnection Conn = dB.Connections[0];
            Cols = PG_Cols.FromHeaderSet(FSDataTable.TableName, FSDataTable.ListOfNames_Types, false);
            Cols.CreateTable(Conn); //Create this if it doesn't exist
            indB_DataIDs = new Dictionary<string, int>(); //Cols.Query2Dict(Cols[0], Cols[1], Conn);
        }

        public bool ProcessLevel_Copy(ISimpleBase Level)
        {
            //Fast Version
            Dictionary<string, int> indB_IDs; PG_Cols Cols;
            GetTableIDs_wtUID(Level, out indB_IDs, out Cols); //This gets the list of wtUIDs and dbIDs from the database
            var Siblings = Level.GetAllSiblings();

            var ValList = new List<Dictionary<string, object>>();
            int dbid = (indB_IDs.Count == 0) ? 0 : indB_IDs.Values.Max() + 1;
            foreach (var item in Siblings)
            {
                if (!indB_IDs.ContainsKey(item.wtUID)) //If it isn't already in the dB, add it
                {
                    var Vals = item.CellBase.Data_MultiHeaderInfo(Header_Info.Subset(item.Table_Name));
                    Vals.Add("dbid", ++dbid); Vals.Add("wtuid", item.wtUID); Vals.Add("parentid", item.parentID);
                    ValList.Add(Vals);
                    indB_IDs.Add(item.wtUID, dbid);
                }
                item.dbID = indB_IDs[item.wtUID]; //Record the new dBID so we can use it
            }
            if (ValList.Count > 0)
                Cols.InsertMulti(ValList, dB.Connections[1]); //Here is the "INSERT" step

            return true;
        }

        public bool ProcessLevel_Insert(IEnumerable<ISimpleBase> Level)
        {
            //Slow version
            if (Level.Count() < 1) return false;
            ISimpleBase tChild = Level.First();
            Dictionary<string, int> indB_IDs; PG_Cols Cols;
            GetTableIDs_wtUID(tChild, out indB_IDs, out Cols);

            foreach (var Child in Level)
            {
                if (!indB_IDs.ContainsKey(Child.wtUID)) Insert(Cols, indB_IDs, Child);
                Child.dbID = indB_IDs[Child.wtUID]; //Gets the db ID

                if (tChild.Table_Name != Simple_Cell.TableName)
                    ProcessLevel_Insert((IEnumerable<ISimpleBase>)Child);
            }
            return true;
        }

        private void GetTableIDs(string tableName, string KeyCol, out Dictionary<string, int> indB_IDsMap, out PG_Cols Cols)
        {
            NpgsqlConnection Conn = dB.Connections[0];
            Cols = PG_Cols.FromHeaderSet(tableName, Header_Info.Subset(tableName));
            Cols.CreateTable(Conn); //Create this if it doesn't exist
            indB_IDsMap = Cols.Query2Dict(Cols[0], Cols.ColFromName(KeyCol), Conn);
        }

        private void GetTableIDs_wtUID(ISimpleBase Base, out Dictionary<string, int> indB_IDsMap, out PG_Cols Cols)
        {
            NpgsqlConnection Conn = dB.Connections[0];
            Cols = PG_Cols.FromHeaderSet(Base.Table_Name, Header_Info.Subset(Base.Table_Name));
            Cols.CreateTable(Conn); //Create this if it doesn't exist
            indB_IDsMap = Cols.Query2Dict(Cols[0], Cols.ColFromName("wtUID"), Conn);
        }

        private void Insert(PG_Cols Cols, Dictionary<string, int> IDs, ISimpleBase Base)
        {
            NpgsqlConnection Conn = dB.Connections[0];
            int dbid = 0;
            if (IDs.Count > 0) dbid = IDs.Values.Max() + 1;
            var Vals = Base.CellBase.Data_MultiHeaderInfo(Header_Info.Subset(Base.Table_Name));
            Vals.Add("dbid", dbid);
            Vals.Add("wtUID", Base.wtUID);
            int ret = Cols.Insert(Vals, Conn);
            if (ret == 1) IDs.Add(Base.wtUID, dbid);
            else
            {

            }
        }
    }

    public class dBWrap
    {
        public string ConnectionString = "";
        public List<NpgsqlConnection> Connections;
        public bool SimulationOnly = true;

        public static int ConnectionCount = 3;

        public dBWrap(string connectionString)
        {
            ConnectionString = connectionString;
            Connections = new List<NpgsqlConnection>();
        }

        public void Open()
        {
            Connections = new List<NpgsqlConnection>();
            if (SimulationOnly) return;

            for (int i = 0; i < ConnectionCount; i++)
            {
                Connections.Add(new NpgsqlConnection(ConnectionString));
            }
            foreach (var conn in Connections)
            {
                conn.Open();
            }
        }

        public Dictionary<string, int> TableBasics(string TableName, string ColumnToQuery, int Parent_id = -1)
        {
            Dictionary<string, int> Dict = new Dictionary<string, int>();
            if (SimulationOnly)
            {
                Dict.Add("A", 0); Dict.Add("B", 1); Dict.Add("C", 2);
            }
            return Dict;
        }

        #region Experimenting - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        private static Random _Rand = new Random();

        internal void TestInsert()
        {
            //This is trying to be a new way to o this
            var Cols = new PG_Cols("test04");
            Cols.Add(new PG_Col("id", TypeHelperPG.Int)); Cols.Add(new PG_Col("desr", TypeHelperPG.String)); Cols.Add(new PG_Col("value", TypeHelperPG.Double));

            var Ret = Cols.Query2Cols(Cols[0], Cols[1], Connections[0]);

            Cols.CreateTable(Connections[0]);
            var tVals = new List<Dictionary<string, object>>();
            for (int i = 0; i < 5; i++) tVals.Add(new Dictionary<string, object>() { { "id", _Rand.Next(0, 1000) }, { "desr", "Rare" }, { "value", _Rand.NextDouble() } });
            Cols.InsertMulti(tVals, Connections[0]);
        }

        #endregion
    }

    /// <summary>
    /// Holds a set of columns which are the complete set for a table in Postgres
    /// </summary>
    public class PG_Cols : IEnumerable<PG_Col>
    {
        private List<PG_Col> _List;
        private Dictionary<string, PG_Col> _Dict;

        public string TableName { get; set; }

        public PG_Cols()
        {
            TableName = "";
            _List = new List<PG_Col>();
            _Dict = new Dictionary<string, PG_Col>();
        }

        public PG_Cols(string tableName)
        {
            TableName = tableName;
            _List = new List<PG_Col>();
            _Dict = new Dictionary<string, PG_Col>();
        }

        public PG_Col ColFromName(string ColName)
        {
            if (_Dict.ContainsKey(ColName)) return _Dict[ColName];
            if (_Dict.ContainsKey(ColName.ToUpper())) return _Dict[ColName.ToUpper()];
            if (_Dict.ContainsKey(ColName.ToLower())) return _Dict[ColName.ToLower()];
            return null;
        }

        public void Add(PG_Col Column)
        {
            _List.Add(Column);
            _Dict.Add(Column.dBName, Column);

            //Make this dirty . . 
            _Statement_Insert = ""; _Statement_CreateTable = ""; _Statement_Select = "";
            if (_CommaList == "") _CommaList = "\"" + Column.dBName + "\"";
            else _CommaList += ", \"" + Column.dBName + "\"";
        }

        #region Select  - - - - - - - - - - - - - - - - -

        private string _Statement_Select = "";

        public string Statement_Select
        {
            get
            {
                if (_Statement_Select == "") _Statement_Select = GetStatement_Select(this);
                return _Statement_Select;
            }
            set { _Statement_Select = value; }
        }

        public static string GetStatement_Select(PG_Cols Cols)
        {
            var sB = new StringBuilder();
            sB.Append("SELECT ");
            sB.Append(string.Join(", ", Cols.Select(x => x.dBName)));
            sB.Append(" FROM public." + Cols.TableName + ";");
            return sB.ToString();
        }

        /// <summary>
        /// Queries all columns (Copy instead of Select which is faster)
        /// </summary>
        public List<Dictionary<string, object>> QueryAllCols(NpgsqlConnection Conn)
        {
            return QueryCols(this, Conn);
        }

        public List<Dictionary<string, object>> Query2Cols(PG_Col Col1, PG_Col Col2, NpgsqlConnection Conn)
        {
            var ColsNew = this.MakeSubset(new PG_Col[2] { Col1, Col2 });
            return QueryCols(ColsNew, Conn);
        }

        internal Dictionary<string, int> Query2Dict(PG_Col ColID, PG_Col Col2, NpgsqlConnection Conn)
        {
            var Res = new Dictionary<string, int>();
            var tRes = Query2Cols(ColID, Col2, Conn);
            foreach (var item in tRes)
            {
                Res.Add(item[Col2.dBName].ToString(), (int)item[ColID.dBName]);
            }
            return Res;
        }

        /// <summary>
        /// Static version that actually does the Query and connects with the dB
        /// </summary>
        public static List<Dictionary<string, object>> QueryCols(PG_Cols Cols, NpgsqlConnection Conn)
        {
            var Res = new List<Dictionary<string, object>>();
            using (var reader = Conn.BeginBinaryExport($@"COPY {Cols.TableName} ({Cols.CommaList}) TO STDOUT (FORMAT BINARY)"))
            {
                while (reader.StartRow() > -1)
                {
                    var Row = new Dictionary<string, object>();
                    Res.Add(Row);
                    for (int c = 0; c < Cols.Count; c++)
                    {
                        var Col = Cols[c];
                        object? T = null;
                        switch (Col.thType.dbType)
                        {
                            case NpgsqlDbType.Double:
                                T = reader.Read<double>(); break;
                            case NpgsqlDbType.Integer:
                                T = reader.Read<int>(); break;
                            case NpgsqlDbType.Real:
                                T = reader.Read<float>(); break;
                            case NpgsqlDbType.Smallint:
                                T = reader.Read<short>(); break;
                            case NpgsqlDbType.Boolean:
                                T = reader.Read<bool>(); break;
                            case NpgsqlDbType.Text:
                                T = reader.Read<string>(); break;
                            case NpgsqlDbType.Bytea:
                                T = reader.Read<byte[]>(); break;
                            case NpgsqlDbType.TimestampTz:
                                T = reader.Read<DateTime>(); break;
                            case NpgsqlDbType.Interval:
                                T = reader.Read<TimeSpan>(); break;
                            case NpgsqlDbType.Varbit:
                                T = reader.Read<BitArray>(); break;
                        }
                        Row.Add(Col.dBName, T);
                    }
                }
            }
            return Res;
        }

        internal void TestQuery(NpgsqlConnection Con)
        {
            var SQL = new NpgsqlCommand(Statement_Select, Con);
            SQL.Prepare();
            var DR = SQL.ExecuteReader();
            var sB = new StringBuilder();
            while (DR.Read())
            {
                for (int i = 0; i < DR.FieldCount; i++) sB.Append(DR[i] + "\t");
                sB.Append("\r\n");
            }
            string T = sB.ToString();
        }

        #endregion

        #region Insert - - - - - - - - - - - - - - - - - - - - - 

        private string _Statement_Insert = "";
        public string Statement_Insert
        {
            get
            {
                if (_Statement_Insert == "")
                    _Statement_Insert = GetStatement_Insert(this);
                return _Statement_Insert;
            }
            set { _Statement_Insert = value; }
        }

        public static string GetStatement_Insert(PG_Cols Cols)
        {
            var sB_Cols = new StringBuilder(); var sB_Vals = new StringBuilder();
            PG_Col Col; bool Last = false;
            string InsertDataString = "INSERT INTO \"" + Cols.TableName.ToLower() + "\"(";
            for (int r = 0; r < Cols.Count; r++)
            {
                Last = (r == Cols.Count - 1);
                Col = Cols[r];
                sB_Cols.Append("\"" + Col.dBName + "\"" + (Last ? "" : ", "));
                sB_Vals.Append(Col.dBNameAt + (Last ? "" : ", "));
            }
            return InsertDataString + sB_Cols.ToString() + ") Values(" + sB_Vals.ToString() + ")";
        }

        internal void AddCmdValues(NpgsqlCommand insertDataCmd, string dbName, object value)
        {
            string key = dbName;
            if (!_Dict.ContainsKey(key)) key = dbName.ToLower();
            var Col = _Dict[key];
            insertDataCmd.Parameters.AddWithValue(Col.dBNameAt, Col.thType.dbType, value);
        }

        public int Insert(Dictionary<string, object> values, NpgsqlConnection Conn)
        {
            PG_Cols Cols = this;
            var InsertDataCmd = new NpgsqlCommand(Cols.Statement_Insert, Conn);

            foreach (var item in values) Cols.AddCmdValues(InsertDataCmd, item.Key, item.Value);
            //Check for missing values
            try
            {
                InsertDataCmd.Prepare();
                InsertDataCmd.ExecuteNonQuery();
                return 1;
            }
            catch { }
            return -1;
        }

        public void InsertMulti(List<Dictionary<string, object>> ListOfData, NpgsqlConnection Conn)
        {
            using (var writer = Conn.BeginBinaryImport($@"COPY {TableName} ({CommaList}) FROM STDIN (FORMAT BINARY)"))
            {
                for (int r = 0; r < ListOfData.Count; r++)
                {
                    writer.StartRow();
                    for (int c = 0; c < Count; c++)
                    {
                        var Col = this[c];
                        string ColKey = Col.dBName;
                        if (!ListOfData[r].ContainsKey(ColKey)) ColKey = Col.cbName;
                        writer.Write(ListOfData[r][ColKey], Col.thType.dbType);
                    }
                }
                writer.Complete();
            }
        }

        private string _CommaList = "";
        public string CommaList { get => _CommaList; }

        public override string ToString()
        {
            return CommaList;
        }

        #endregion

        #region CreateTable - - - - - - - - - - - - - - - - - - - - - - - 

        private string _Statement_CreateTable = "";
        public string Statement_CreateTable
        {
            get
            {
                if (_Statement_CreateTable == "") _Statement_CreateTable = GetStatement_CreateTable(this);
                return _Statement_CreateTable;
            }
            set { _Statement_CreateTable = value; }
        }

        public static string GetStatement_CreateTable(PG_Cols Cols)
        {
            var sB = new StringBuilder(); PG_Col Col;
            sB.Append("CREATE TABLE " + Cols.TableName + "(\r\n");
            for (int i = 0; i < Cols.Count; i++)
            {
                Col = Cols[i];
                sB.Append("\"" + Col.dBName + "\" " + Col.thType.pgTypeName + (Cols.Count == (i + 1) ? "" : ",") + "\r\n");
            }
            sB.Append(")");
            //string t = $@"CREATE TABLE EB4(
            //    Id serial primary key,
            //    MetaDataKey int,
            //    AQP text,
            //    PLATEID text,
            //    ""PLATE ID"" text,
            //    ValT double precision
            //    )";
            return sB.ToString();
        }

        /// <summary>
        /// Create table only if it doesn't exist
        /// </summary>
        /// <returns>true if table was created, false if it already exists</returns>
        internal bool CreateTable(NpgsqlConnection Conn)
        {
            if (TableExists(Conn)) return false;

            var CMD = new NpgsqlCommand(Statement_CreateTable, Conn);
            CMD.Prepare();
            CMD.ExecuteNonQuery();
            return true;
        }

        public string SQL_TableExists => "SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename  = '" + TableName.ToLower() + "')";
        public bool TableExists(NpgsqlConnection Conn)
        {
            bool Res = false;
            var CMD = new NpgsqlCommand(SQL_TableExists, Conn);
            CMD.Prepare();
            var DR = CMD.ExecuteReader();

            DR.Read();
            string? t = DR[0].ToString(); if (t == null) t = "";
            DR.Close();
            if (t.ToUpper() == "FALSE") return false;
            return true;
        }

        #endregion

        #region DeleteTable - - - - - - - - - - - - - - - - - - - 

        //private string _Statement_DeleteTable = "";
        //public string Statement_DeleteTable
        //{
        //    get
        //    {
        //        if (_Statement_CreateTable == "") _Statement_CreateTable = GetStatement_CreateTable(this);
        //        return _Statement_CreateTable;
        //    }
        //    set { _Statement_CreateTable = value; }
        //}

        public static string GetStatement_DeleteTable(string TableName)
        {
            return "drop table if exists " +TableName+ " cascade;";
        }

        internal static bool DeleteTable(string TableName, NpgsqlConnection Conn)
        {
            //if (TableExists(Conn)) return false;

            var CMD = new NpgsqlCommand(GetStatement_DeleteTable(TableName), Conn);
            CMD.Prepare();
            CMD.ExecuteNonQuery();
            return true;
        }

        public static bool DeleteAllTables(NpgsqlConnection Conn)
        {
            DeleteTable(Simple_File.TableName, Conn);
            DeleteTable(Simple_Scan.TableName, Conn);
            DeleteTable(Simple_ZTR.TableName, Conn);
            DeleteTable(Simple_Well.TableName, Conn);
            DeleteTable(Simple_FOV.TableName, Conn);
            DeleteTable(Simple_Raft.TableName, Conn);
            DeleteTable(Simple_Cell.TableName, Conn);

            DeleteTable(FSDataTable.TableName, Conn);
            DeleteTable(Header_Info.TableName, Conn);
            return true;
        }

        #endregion

        #region ListStuff - - - - - - - - - - - - - - - - - - - - 
        public PG_Col this[int index]
        {
            get { return _List[index]; }
        }

        public int Count => _List.Count;

        public PG_Cols MakeSubset(PG_Col[] ColsToCopy)
        {
            var P = new PG_Cols(this.TableName);
            for (int i = 0; i < ColsToCopy.Length; i++)
                P.Add(ColsToCopy[i]);
            return P;
        }

        public static PG_Cols FromHeaderSet(string tableName, List<Tuple<string, string>> listOf_Names_Types, bool Add_dbID = true)
        {
            var P = new PG_Cols(tableName);
            if (Add_dbID) P.Add(new PG_Col("dbID", TypeHelperPG.Int));
            if (Add_dbID) P.Add(new PG_Col("wtUID", TypeHelperPG.String));
            if (Add_dbID) P.Add(new PG_Col("parentID", TypeHelperPG.Int));
            foreach (var tup in listOf_Names_Types)
            {
                var th = TypeHelperPG.Lookup_cbName(tup.Item2);
                P.Add(new PG_Col(tup.Item1, th));
            }
            return P;
        }

        public IEnumerator<PG_Col> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        internal static object FromHeaderSet(object tableName, object listOfNames_Types, object alse)
        {
            throw new NotImplementedException();
        }

        internal static object FromHeaderSet(object tableName, List<Tuple<string, string>> listOfNames_Types, object alse)
        {
            throw new NotImplementedException();
        }





        #endregion

    }

    /// <summary>
    /// Holds onto a Table Column, keeps track of names and types
    /// </summary>
    public class PG_Col
    {
        public PG_Col()
        {
            dBName = cbName = "";
        }

        public PG_Col(string Name, TypeHelperPG Type)
        {
            cbName = Name;
            dBName = Name.ToLower().Trim();
            thType = Type;
        }

        public string dBName { get; set; }

        public string dBNameAt { get => Get_dbNameAt(dBName); }

        public static string Get_dbNameAt(string dbName) { return "@" + dbName.Trim().Replace(" ", "__").Replace("-", "__").ToLower(); }
        public string cbName { get; set; }
        public TypeHelperPG thType { get; set; }
    }

    /// <summary>
    /// Keep track of Type conversions between Postgres and c#
    /// </summary>
    public class TypeHelperPG
    {
        //
        // R:\dB\Software\Postgres\Types.xlsx
        // https://www.npgsql.org/doc/types/basic.html
        //
        public static void SetupLookUps()
        {
            if (IsSetup) return;
            cAdd(new TypeHelperPG(typeof(bool), NpgsqlDbType.Boolean, "boolean", "bool"));
            cAdd(new TypeHelperPG(typeof(short), NpgsqlDbType.Smallint, "smallint", "short"));
            cAdd(new TypeHelperPG(typeof(int), NpgsqlDbType.Integer, "integer", "int"));
            cAdd(new TypeHelperPG(typeof(long), NpgsqlDbType.Bigint, "bigint", "long"));
            cAdd(new TypeHelperPG(typeof(float), NpgsqlDbType.Real, "real", "float"));
            cAdd(new TypeHelperPG(typeof(double), NpgsqlDbType.Double, "double precision", "double"));
            cAdd(new TypeHelperPG(typeof(decimal), NpgsqlDbType.Numeric, "numeric", "decimal"));
            cAdd(new TypeHelperPG(typeof(string), NpgsqlDbType.Text, "text", "string"));
            cAdd(new TypeHelperPG(typeof(char), NpgsqlDbType.Char, "char", "char"));
            cAdd(new TypeHelperPG(typeof(byte[]), NpgsqlDbType.Bytea, "bytea", "byte[]"));
            cAdd(new TypeHelperPG(typeof(DateTime), NpgsqlDbType.TimestampTz, "timestamp with time zone", "DateTime"));
            cAdd(new TypeHelperPG(typeof(TimeSpan), NpgsqlDbType.Interval, "interval", "TimeSpan"));
            cAdd(new TypeHelperPG(typeof(BitArray), NpgsqlDbType.Varbit, "bit varying", "BitArray"));
            cAdd(new TypeHelperPG(typeof(IDictionary<string, string>), NpgsqlDbType.Hstore, "hstore", "IDictionary<string, string>"));
            IsSetup = true;
        }

        private static bool IsSetup = false;

        private static Dictionary<Type, TypeHelperPG> _LookupCSType = new Dictionary<Type, TypeHelperPG>();
        private static Dictionary<string, TypeHelperPG> _LookupCSName = new Dictionary<string, TypeHelperPG>();

        private static void cAdd(TypeHelperPG thPG)
        {
            _LookupCSType.Add(thPG.cbType, thPG);
            _LookupCSName.Add(thPG.cbTypeName, thPG);
        }

        public static TypeHelperPG Lookup_cbT(Type cbType)
        {
            SetupLookUps();
            return _LookupCSType[cbType];
        }

        public static TypeHelperPG Lookup_cbName(string cbName)
        {
            SetupLookUps();
            return _LookupCSName[cbName];
        }

        public TypeHelperPG()
        {
            pgTypeName = ""; cbTypeName = "";
        }

        public TypeHelperPG(Type cbtype, NpgsqlDbType dbtype, string pgtypeName, string cbtypeName)
        {
            cbType = cbtype; dbType = dbtype; pgTypeName = pgtypeName; cbTypeName = cbtypeName;
        }

        public Type cbType { get; set; }
        public string cbTypeName { get; set; }
        public NpgsqlDbType dbType { get; set; }
        public string pgTypeName { get; set; }

        public static TypeHelperPG Int => Lookup_cbT(typeof(int));
        public static TypeHelperPG String => Lookup_cbT(typeof(string));
        public static TypeHelperPG Double => Lookup_cbT(typeof(double));
    }
}