﻿/*
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Internal;

public class MemoryCacheing
{
    private readonly IMemoryCache _memoryCache;

    public MemoryCacheing(IMemoryCache memoryCache) =>
        _memoryCache = memoryCache;
  



    public void OnGetCacheGetOrCreate(Object item)
    {
        Object key = "";
        item = _memoryCache.GetOrCreate<Object>(item, v =>
        {
            v.SlidingExpiration = TimeSpan.FromHours(1);
            v.SetValue(item);
            return item;
        });
    }

    public void SetEntry(Object entry)
    {
        var CurrentDateTime = DateTime.Now;

        if (!_memoryCache.TryGetValue(entry, out DateTime cacheValue))
        {
            cacheValue = CurrentDateTime;
          
            _memoryCache.Set(entry, cacheValue, TimeSpan.FromHours(1));
        }

        var CacheCurrentDateTime = cacheValue;
    }

    public void OnGet(Object entry)
    {

        if (!_memoryCache.TryGetValue(entry, out Object cacheValue))
        {
            cacheValue = entry;

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromHours(1));

            _memoryCache.Set(entry, cacheValue, cacheEntryOptions);
        }

        cacheValue = entry;
    }
    public class MyMemoryCache
    {
        SystemClock clock = new SystemClock();
        public MemoryCache Cache { get; } = new MemoryCache(
            new MemoryCacheOptions
            {

                SizeLimit  = 10000000000000
            });
    }
}

*/