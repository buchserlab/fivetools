﻿namespace WebApplication9
{

    public class NonMeasurementData
    {
        public class ScanTable
        {

            public int Id { get; set; }
            public string PlateId { get; set; } = null!;
            public string PlateSectionId { get; set; } = null!;
            public string AQP { get; set; } = null!;
            public string INCARATA_ProtocolName { get; set; } = null!;
            public string ResultsId { get; set; } = null!;
            public int MLPlateIndex { get; set; }
            public DateTime LastInsert { get; set; }
            public string FileId { get; set; } = null!;
            //public virtual FileRelatedData File { get; set; } = null!;
            public virtual WellTable WellTable { get; set; } = null!;

        }
        public class WellTable
        {
            public int ScanTableId { get; set; }
            public string WellLable { get; set; } = null!;
            public string MltrainingLayout { get; set; } = null!;
            public string MlClassLayout { get; set; } = null!;
            public string PlateWell { get; set; } = null!;
            public string Mlreplicates { get; set; } = null!;
            public string Replicates { get; set; } = null!;
            public string MlcrossvalFold { get; set; } = null!;
            public string MplateIndex { get; set; } = null!;
            public string Row { get; set; } = null!;
            public string Column { get; set; } = null!;
            public DateTime LastInsert { get; set; }
            public string FiledId { get; set; } = null!;
            public ScanTable ScanTable { get; set; } = null!;
            public FovTable FovTable { get; set; } = null!;
        }
        public class FovTable
        {
            public int Id { get; set; }
            public int WellTableId { get; set; }
            public string ImageName { get; set; } = null!;
            public double FocuzZPos { get; set; }
            public double FocusZPosOfAdjacentFields { get; set; }
            public string AcquisitionTimeStamp { get; set; } = null!;
            public int Fovid { get; set; }
            public DateTime? LastInsert { get; set; }
            public string FileId { get; set; } = null!;

            public virtual WellTable WellTable { get; set; } = null!;
            public virtual CellTable CellTable { get; set; } = null!;

        }

        public class RaftTable
        {
            //public AList<FOV> fOVs { get; set; } = null!;
            public int[] FovIdArrayKeys { get; set; } = null!;
            public string RaftId { get; set; } = null!;
            public string Raftrow { get; set; } = null!;
            public string Raftcol { get; set; } = null!;
            public double Umfromborderofraft { get; set; }
            public string Imagecheck { get; set; } = null!;
            public string Mlquad { get; set; } = null!;
            public bool Mlfiduciaryraft { get; set; }
            public string Mlimagecheck { get; set; } = null!;
            public bool Mlc0allowedregion { get; set; }
            public bool Mlvalidrafts { get; set; }
            public int Mlcellsperraft { get; set; }
            public bool Mlc2allowedperraft { get; set; }
        }

        public partial class CellTable
        {
            public int Id { get; set; }
            public int FovtableId { get; set; }
            public int ObjectId { get; set; }
            public double WellXUm { get; set; }
            public double WellYUm { get; set; }
            public double PlateXUm { get; set; }
            public double PlateYUm { get; set; }
            public bool Mlcellsuse { get; set; }
            public long Inputrowindex { get; set; }
            public bool Mlthreshold { get; set; }
            public bool Mlc1cellular { get; set; }
            public double Mitoorginten { get; set; }
            public double NucleiAntihealth { get; set; }
            public DateTimeOffset? LastInsert { get; set; }
            public string FileId { get; set; } = null!;
            public FovTable Fovtable { get; set; } = null!;
        }
    }

    public class MeasurementData
    {


        public class MeasurementTable
        {
            public int Id { get; set; }
            public string FileIdName { get; set; } = null!;
            public int FeatureId { get; set; }
            public int MaskId { get; set; }
            public int WaveLengthId { get; set; }
            public double MeasurementValues { get; set; }
            public DateTime? LastInsert { get; set; }
            public Featuretable Feature { get; set; } = null!;
            //public FileRelatedData FileIdNameNavigation { get; set; } = null!;
            public Masktable Mask { get; set; } = null!;
            public Wavelengthtable WaveLength { get; set; } = null!;
        }

        public class Wavelengthtable
        {
            public Wavelengthtable()
            {
                MeasurementTables = new HashSet<MeasurementTable>();
            }

            public int Id { get; set; }
            public string? WaveLength { get; set; }
            public DateTime? InsertDate { get; set; }
            public bool? IsActive { get; set; }

            public ICollection<MeasurementTable> MeasurementTables { get; set; }
        }


        public class Featuretable
        {
            public Featuretable()
            {
                MeasurementTables = new HashSet<MeasurementTable>();
            }

            public int Id { get; set; }
            public string Feature { get; set; } = null!;
            public bool Active { get; set; }
            public DateTime LastInsert { get; set; }

            public ICollection<MeasurementTable> MeasurementTables { get; set; }
        }


        public class Masktable
        {
            public Masktable()
            {
                MeasurementTables = new HashSet<MeasurementTable>();
            }
            public int Id { get; set; }
            public string? Mask { get; set; }
            public bool? IsActive { get; set; }
            public DateTime? LastInsert { get; set; }
            public ICollection<MeasurementTable> MeasurementTables { get; set; }
        }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class BSONFile
    {
        public BSONFile()
        {
            Vals = new Dictionary<short, KeyValuePair<object, bool>>();
            fileName = "";
            RCnt = 0;
            CCnt = 0;
            IdxCnt = 0;
            IsPart = false;
        }

        public Dictionary<Int16, KeyValuePair<Object, Boolean>> Vals { get; set; }
        public string fileName { get; set; }
        public int RCnt { get; set; }
        public int CCnt { get; set; }
        public int IdxCnt { get; set; }
        public bool IsPart { get; set; }
    }

    public class detectdelimiter
    {
        public static char DetectDelimiter(TextReader reader, int rowCount)
        {

            List<Char> seperators = new List<char>() { ',', '\t', ';', '|' }.ToList();

            IList<int> separatorsCount = new int[seperators.Count];

            int character;

            int row = 0;

            bool quoted = false;
            bool firstChar = true;

            while (row < rowCount)
            {
                character = reader.Read();

                switch (character)
                {
                    case '"':
                        if (quoted)
                        {
                            if (reader.Peek() != '"') // Value is quoted and 
                                                      // current character is " and next character is not ".
                                quoted = false;
                            else
                                reader.Read(); // Value is quoted and current and 
                                               // next characters are "" - read (skip) peeked qoute.
                        }
                        else
                        {
                            if (firstChar)  // Set value as quoted only if this quote is the 
                                            // first char in the value.
                                quoted = true;
                        }
                        break;
                    case '\n':
                        if (!quoted)
                        {
                            ++row;
                            firstChar = true;
                            continue;
                        }
                        break;
                    case -1:
                        row = rowCount;
                        break;
                    default:
                        if (!quoted)
                        {
                            int index = seperators.IndexOf((char)character);
                            if (index != -1)
                            {
                                ++separatorsCount[index];
                                firstChar = true;
                                continue;
                            }
                        }
                        break;
                }

                if (firstChar)
                    firstChar = false;
            }

            int maxCount = separatorsCount.Max();
            reader.Close();
            return maxCount == 0 ? '\0' : seperators[separatorsCount.IndexOf(maxCount)];
        }

        public static string DetectDelimiterOld(String? headers)
        {
            if (headers == null) return "";
            
            // assume one of following delimiters
            int comma = 0;
            int tab = 0;
            int bar = 0;
            int semicolon = 0;

            foreach (char character in headers)
            {
                if (character == ',')
                {
                    comma += 1;
                }
                else if (character == ';')
                {
                    semicolon += 1;
                }
                else if (character == '\t')
                {
                    tab += 1;
                }
                else if (character == '|')
                {
                    bar += 1;
                }
                else
                {
                    continue;
                }
            }
            Dictionary<String, int> collector = new Dictionary<String, int>();

            collector.Add("|", bar);
            collector.Add(";", semicolon);
            collector.Add("\t", tab);
            collector.Add(",", comma);
            String delimiter = collector.MaxBy(t => t.Value).Key;
            return delimiter;
        }
    }
}

