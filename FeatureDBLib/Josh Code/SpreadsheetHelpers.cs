﻿using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using static WebApplication9.TypeValueMaps;
static class Globals
{
    public static String file = "FIV593 CellsUse v01_rnnorm";
    public static String extension = ".txt";
}

public static class SpreadSheetTools
{


    public static  T[] RemoveFromArray<T>(this T[] original, T itemToRemove)
    {
        int numIdx = System.Array.IndexOf(original, itemToRemove);
        if (numIdx == -1) return original;
        List<T> tmp = new List<T>(original);
        tmp.RemoveAt(numIdx);
        return tmp.ToArray();
    }
    public static bool IsMeasurementData(String Column)
    {
        Dictionary<String, int> FeatureId = Maps.FeatureIdMap();
        Dictionary<String, int> MaskIdMap = Maps.MaskIdMap();
        Dictionary<String, int> WavelengthIdMap = Maps.WavelengthIdMap();
        if (Column == null) return false;
        String[] splitter = Column.Split(" ");
        String WaveLengthId = splitter.Last();
        String MaskId = splitter.First();
        String Feature = String.Join(" ", splitter.Where(t => t != WaveLengthId && t != MaskId));
        if (FeatureId.ContainsKey(Feature))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static Object ParseOrDefault(String oo)
    {
        object j = oo;
        return ParseOrDefault(j);
    }



    public static Object ParseOrDefault(Object o, bool? MeasurementData = false)
    {
        object? t = new object();
        //null rules
        if (o == null && MeasurementData == true)
        {
            return DBNull.Value;
        }
        if(o == null && MeasurementData == false)
        {
            if (o != null && String.IsNullOrWhiteSpace(o?.ToString()) && (o?.GetType() == typeof(String).GetType()))
            {
                //it seems like our string convention is to return some type of symbol if it's white space we should give it a symbol
                return "_";
            }
            else
            {
                return DBNull.Value;
            }
        }
        string tryParseMe = o.ToString();
        bool b2;
        if(bool.TryParse(tryParseMe, out b2) && (tryParseMe.ToUpper() == "TRUE" || tryParseMe.ToUpper() == "FALSE"))
        {
            t = b2;
            return b2;
        }
        float k2;
        if (tryParseMe.ToString().Length <= 8 && tryParseMe.ToString().Contains('.') && float.TryParse(tryParseMe, out k2))
        {
            t = k2;
            return t;
        }
        double d234;
        if (tryParseMe.ToString().Contains('.') && double.TryParse(tryParseMe, out d234))
        {
            t =  d234;
            return t;
        }
        int K38;
        if (int.TryParse(tryParseMe, out K38))
        {
            t = K38;
            return t;
        }
        long m;
        if (tryParseMe.ToString().Length <= 8 && long.TryParse(tryParseMe, out m))
        {
            t =  m;
            return t;
        }
        uint uK38;
        if (tryParseMe.ToString().Length <= 8 && uint.TryParse(tryParseMe, out uK38))
        {
            t = uK38;
            return t;
        }
        ulong um;
        if (tryParseMe.ToString().Length <= 8 && ulong.TryParse(tryParseMe, out um))
        {
            t =  um;
            return t;
        }
        char c0;
        if (char.TryParse(tryParseMe, out c0) && tryParseMe.Length == 1 && char.IsAscii(c0) == true)
        {
            t = c0;
            return c0;
        }
        if (o is String)
        {
            return o.ToString();
        }
        return t;
    }

    public static bool TryParse(this string strInput, out JToken output)
    {
        if (String.IsNullOrWhiteSpace(strInput))
        {
            output =null;
            return false;
        }
        strInput = strInput.Trim();
        if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
            (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
        {
            try
            {
                output = JToken.Parse(strInput);
                return true;
            }
            catch// (JsonReaderException jex)
            {
                //Exception in parsing json
                //optional: LogError(jex);
                output = null;
                return false;
            }
        }
        else
        {
            output = null;
            return false;
        }
    }

    public static bool TryCast<T>(this object value, out T result)
    {
        var destinationType = typeof(T);
        var inputIsNull = (value == null || value == DBNull.Value);

        /*
         * If the given value is null, we'd normally set result to null and be done with it.
         * HOWEVER, if T is not a nullable type, then we can't REALLY cast null to that type, so
         * TryCast should return false.
         */
        if (inputIsNull)
        {
            // If T is nullable, this will result in a null value in result.
            // Otherwise this will result in a default instance in result.
            result = default(T);

            // If the input is null and T is nullable, we report success.  Otherwise we report failure.
            return default(T) == null && typeof(T).BaseType != null && "ValueType".Equals(typeof(T).BaseType.Name);
        }

        // Convert.ChangeType fails when the destination type is nullable.  If T is nullable we use the underlying type.
        var underlyingType = Nullable.GetUnderlyingType(destinationType) ?? destinationType;

        try
        {
            /*
             * At the moment I cannot remember why I handled Guid as a separate case, but
             * I must have been having problems with it at the time or I'd not have bothered.
             */
            if (underlyingType == typeof(Guid))
            {
                if (value is string)
                {
                    value = new Guid(value as string);
                }
                if (value is byte[])
                {
                    value = new Guid(value as byte[]);
                }

                result = (T)Convert.ChangeType(value, underlyingType);
                return true;
            }

            result = (T)Convert.ChangeType(value, underlyingType);
            return true;
        }
        catch (Exception ex)
        {
            // This was originally used to help me figure out why some types weren't casting in Convert.ChangeType.
            // It could be removed, but you never know, somebody might comment on a better way to do THAT to.
            var traceMessage = ex is InvalidCastException || ex is FormatException || ex is OverflowException
                                    ? string.Format("The given value {0} could not be cast as Type {1}.", value, underlyingType.FullName)
                                    : ex.Message;
            Trace.WriteLine(traceMessage);

            result = default(T);
            return false;
        };

    }
    public static Boolean MakeQuery(List<Object> parameters)
    {

        var jsonParam = new NpgsqlParameter(Newtonsoft.Json.JsonConvert.SerializeObject(parameters), NpgsqlDbType.Json);

        try
        {
            String ConnnectionString = Maps.GrabConnectionString();
            NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
            conn.Open();
            using (NpgsqlCommand? command = new NpgsqlCommand(@"INSERT INTO ""DataLevel"".""SpreadSheetData"" (""ColumnValues"") VALUES (@jsonparam)", conn))
            {
                int i = command.ExecuteNonQuery();
                if (i == -1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        catch (NpgsqlException ex)
        {
            throw ex;
        }
    }
    public static int InsertToFileTable(String fileName, NpgsqlConnection conn)
    {

        String FileSqlStatment = @"INSERT INTO ""datalevel"".""FileData""(""FileIdName"") Values(@Id) RETURNING ""FileId""";
        try
        {

            using var cmd = new NpgsqlCommand(FileSqlStatment, conn);
            cmd.Parameters.AddWithValue("@fileName", NpgsqlDbType.Integer, Path.GetFileName(fileName));
            int fileForeignKey = (int)cmd.ExecuteScalar();
            return fileForeignKey;
        }
        catch (NpgsqlException ex)
        {
            if ("FileNameUnique" == ex.Data["ConstraintName"].ToString() || ex.Data["ConstraintName"].ToString() == "FileIdUnique")
            {
                return -1;
            }
            else
            {
                throw ex;
            }
        }
    }


    public static Boolean RunTransaction(List<Object> parameters)
    {
        String ConnnectionString = Maps.GrabConnectionString();
        NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
        conn.Open();
        NpgsqlCommand dbcmd = conn.CreateCommand();
        var jsonParam = new NpgsqlParameter(Newtonsoft.Json.JsonConvert.SerializeObject(parameters), NpgsqlDbType.Json);

        try
        {
            NpgsqlCommand d = conn.CreateCommand();
            dbcmd.Parameters.AddWithValue("@jsonparam", NpgsqlDbType.Json, jsonParam);
            dbcmd.CommandText = @"INSERT INTO ""DataLevel"".""SpreadSheetData""  (""ColumnValues"") VALUES (@jsonparam)";
            using (var transaction = conn.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                using (var command = new NpgsqlCommand(dbcmd.CommandText, conn, transaction))
                {
                    command.CommandTimeout = 120;
                    try
                    {
                        int i = command.ExecuteNonQuery();
                        transaction.Commit();
                        if (i == -1)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    catch (NpgsqlException ex)
                    {
                        //todo Exception database table, for now it's fine to just eat them and test things out in the early stages.
                        //for now we can debug it locally
                        System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
                        System.Diagnostics.Trace.WriteLine(ex?.Data);
                        System.Diagnostics.Trace.WriteLine(ex?.Message);
                        System.Diagnostics.Trace.WriteLine(ex?.BatchCommand);
                        System.Diagnostics.Trace.WriteLine(ex?.ErrorCode);
                        System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                        System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                        System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                        System.Diagnostics.Trace.WriteLine(ex?.StackTrace);
                        throw ex ?? new Exception("File Failed to open or has no records Debug locally");
                        throw;
                    }
                }
            }
        }
        catch (NpgsqlException ex)
        {
            Console.Write(ex);
            if (ex.Data == null)
            {
                System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
                System.Diagnostics.Trace.WriteLine(ex?.Data);
                System.Diagnostics.Trace.WriteLine(ex?.Message);
                System.Diagnostics.Trace.WriteLine(ex?.BatchCommand);
                System.Diagnostics.Trace.WriteLine(ex?.ErrorCode);
                System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                System.Diagnostics.Trace.WriteLine(ex?.SqlState);
                System.Diagnostics.Trace.WriteLine(ex?.StackTrace);
                throw;
            }
            else
            {
                return false;
                //CancellationToken token = new CancellationToken();
            }
        }
    }

    public static char DetectDelimiter(TextReader reader, int rowCount)
    {

        List<Char> seperators = new List<char>() { ',', '\t', ';', '|' }.ToList();

        IList<int> separatorsCount = new int[seperators.Count];

        int character;

        int row = 0;

        bool quoted = false;
        bool firstChar = true;

        while (row < rowCount)
        {
            character = reader.Read();

            switch (character)
            {
                case '"':
                    if (quoted)
                    {
                        if (reader.Peek() != '"') // Value is quoted and 
                                                  // current character is " and next character is not ".
                            quoted = false;
                        else
                            reader.Read(); // Value is quoted and current and 
                                           // next characters are "" - read (skip) peeked qoute.
                    }
                    else
                    {
                        if (firstChar)  // Set value as quoted only if this quote is the 
                                        // first char in the value.
                            quoted = true;
                    }
                    break;
                case '\n':
                    if (!quoted)
                    {
                        ++row;
                        firstChar = true;
                        continue;
                    }
                    break;
                case -1:
                    row = rowCount;
                    break;
                default:
                    if (!quoted)
                    {
                        int index = seperators.IndexOf((char)character);
                        if (index != -1)
                        {
                            ++separatorsCount[index];
                            firstChar = true;
                            continue;
                        }
                    }
                    break;
            }

            if (firstChar)
                firstChar = false;
        }

        int maxCount = separatorsCount.Max();
        reader.Close();
        return maxCount == 0 ? '\0' : seperators[separatorsCount.IndexOf(maxCount)];
    }
        public static string DetectDelimiterOld(String? headers)
        {
            int i = 0;
            if (headers == null)
                return null;
            // assume one of following delimiters


            int comma = 0;
            int tab = 0;
            int bar = 0;
            int semicolon = 0;

            foreach (char character in headers)
            {
                if (character == ',')
                {
                    comma += 1;
                }
                else if (character == ';')
                {
                    semicolon += 1;
                }
                else if (character == '\t')
                {
                    tab += 1;
                }
                else if (character == '|')
                {
                    bar += 1;
                }
                else
                {
                    continue;
                }
            }
            Dictionary<String, int> collector = new Dictionary<String, int>();

            collector.Add("|", bar);
            collector.Add(";", semicolon);
            collector.Add("\t", tab);
            collector.Add(",", comma);
            String delimiter = collector.MaxBy(t => t.Value).Key;
         
            return delimiter;
        }

        public static DataTable GetDataSourceFromFile(string fileName, char delimiter)
        {
            DataTable dt = new DataTable("FileData");
            string[] columns = null;

            string[]? lines = File.ReadAllLines(fileName);

            // assuming the first row contains the columns information
            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { delimiter });

                foreach (string? column in columns)
                {
                    if (columns.Contains("\r\n"))
                    {
                        columns.ToList().RemoveAll(t => t == "\r\n");
                    }
                    else
                    {
                        dt.Columns.Add(column);
                    }
                }
            }

            // reading rest of the data
            for (int i = 1; i < lines.Count(); i++)
            {
                DataRow dr = dt.NewRow();
                string[] values = lines[i].Split(new char[] { delimiter });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                dt.Rows.Add(dr);
            }
            return dt;
        }

    }




    

