﻿using System.Numerics;
using System.Data;
using System.Collections.Immutable;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Drawing;
using FIVE.FOVtoRaftID;

namespace WebApplication9
{

    public  static class TypeValueMaps
    {
        static Dictionary<String, int> MaskIdMap = Maps.MaskIdMap();
        static Dictionary<String, int> WavelengthIdMap = Maps.WavelengthIdMap();
        static Dictionary<String, int> FeatureIdMap = Maps.FeatureIdMap();
        public static ValueTuple<(int wavelength, int mask, int feature, double valueFloat)> GenerateMeasurementTuple(Object obj)
        {
            String value = obj?.ToString();
            if (String.IsNullOrWhiteSpace(value.ToString()))
            {
                return default(ValueTuple<(int wave, int mas, int featur, double valueFlo)>);
            }
            String[] splitter = value.Split(" ");
            string maskId = splitter.First().ToUpper();
            string wavelengthId = splitter.Last().ToUpper();
            String Feature = String.Join(" ", splitter.Where(t => t!= maskId && t!= wavelengthId));
            double valueFloat = 1.0F;
            if (Feature == "0")
            {
                //double zeroFloat = 0.0F;
            }
            if (Feature == "1")
            {
                //double oneFloat = 1.0F;
            }
            int feature = FeatureIdMap.GetValueOrDefault(Feature);
            int mask = MaskIdMap.GetValueOrDefault(maskId);
            int wavelength = WavelengthIdMap.GetValueOrDefault(wavelengthId);
            Boolean Measurement = double.TryParse(Feature, out valueFloat);


            return new ValueTuple<(int wave, int mas, int featur, double valueFlo)>((wavelength, mask, feature, valueFloat));


        }



        public static async void InsertDataAsync(String filename)
        {

            DataTable MyTable = SpreadSheetTools.GetDataSourceFromFile(filename, '\t');

            System.Collections.Generic.HashSet<String> UndesiredData = new System.Collections.Generic.HashSet<String>() {
                "Column1",
                "MLPREDICTIONPROB",
                "FILTERED",
                "MLCLASSPREDICTED",
                "VAL COUNT"};

            String ConnnectionString = "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=N3onblue;TrustServerCertificate=true;";
            NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
            NpgsqlConnection conn2 = new Npgsql.NpgsqlConnection(ConnnectionString);
            NpgsqlConnection conn3 = new Npgsql.NpgsqlConnection(ConnnectionString);
            conn.Open();
            conn2.Open();
            conn3.Open();
            Dictionary<int, ValueTuple<(String Column, String Table)>> OrdinalToValue = new Dictionary<int, ValueTuple<(String Col, String Table)>>();

            HashSet<string> ScanSet = new System.Collections.Generic.HashSet<String>(Maps.ScanTableDictionary().Keys.Select(t => t.ToUpper()));
            HashSet<String> WellSet = new System.Collections.Generic.HashSet<String>(Maps.WellDictionary().Keys.Select(t => t.ToUpper()));
            HashSet<String> FovSet = new System.Collections.Generic.HashSet<String>(Maps.FOVDictionary().Keys.Select(t => t.ToUpper()));
            HashSet<String> RaftSet = new System.Collections.Generic.HashSet<String>(Maps.RaftDictionary().Keys.Select(t => t.ToUpper()));
            HashSet<String> CellSet = new System.Collections.Generic.HashSet<String>(Maps.CellDictionary().Keys.Select(t => t.ToUpper()));
            HashSet<String> UnknownSet = new System.Collections.Generic.HashSet<String>();

            Boolean OrdinalFirst = false;
            Dictionary<string, int> featureIdMap = TypeValueMaps.FeatureIdMap;

            foreach (DataColumn column in MyTable.Columns)
            {

                if (OrdinalFirst)
                {
                    OrdinalToValue.Add(OrdinalToValue.Count + 1, new ValueTuple<(String Column, String Table)>(("FileIdName", "FileTable")));
                    break;
                }


                String? value = column.ColumnName.ToUpper().ToString();
                int IntegerValue = column.Ordinal;
                String ColumnName = column.ColumnName.ToUpper();
                String[] splitter = value.Split(" ");
                string maskId = splitter.First().ToUpper();
                string wavelengthId = splitter.Last().ToUpper();
                String Feature = String.Join(' ',splitter.Where(t => t != maskId && t != wavelengthId).ToArray());
                if (UndesiredData.Contains(ColumnName))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Undesired_Table", ColumnName)));
                    continue;
                }
                else if (featureIdMap.ContainsKey(Feature))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Measurement_Table", ColumnName)));
                    continue;
                }
                else if (ScanSet.Contains(ColumnName))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Scan_Table", ColumnName)));
                    continue;
                }
                else if (WellSet.Contains(ColumnName))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Well_Table", ColumnName)));
                    continue;

                }
                else if (FovSet.Contains(ColumnName))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Fov_Table", ColumnName)));
                    continue;
                }
                else if (CellSet.Contains(ColumnName))
                {
                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Cell_Table", ColumnName)));
                    continue;
                }
                else if (RaftSet.Contains(ColumnName))
                {

                    OrdinalToValue.Add(IntegerValue, new ValueTuple<(String Table, String Column)>(("Raft_Table", ColumnName)));
                    continue;
                }
                else
                {
                    continue;
                }
            }
            String file = Path.GetFileName(filename);
            /*
            static int getMaxKey(String Schema, String Table, String Column, NpgsqlConnection conn)
            {
                try
                {
                    String GetForeignKeys = $@"SELECT ""{Column}"" FROM
                                             ""{Schema}"".""{Table}""
                                             ORDER BY ""{Column}"" DESC";


                    NpgsqlCommand cmd = new NpgsqlCommand(GetForeignKeys, conn);
                    if (cmd.ExecuteScalar() == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return (int)cmd.ExecuteScalar();
                    }
                }
                catch
                {
                    return 0;
                }
            }
            */



            List<Object> OtherList = new List<Object>();
            List<Object> FovList = new List<Object>();
            List<Object> ScanList = new List<Object>();
            List<Object> RaftList = new List<Object>();
            List<Object> WellList = new List<Object>();
            List<Object> CellList = new List<Object>();

            List<ValueTuple<(String Column, String Value)>> UnknownList = new List<ValueTuple<(String Column, String Value)>>();
            //int ScanForeignKey = 0;
            int WellForeignKey = 0;
            int FovForeignKey = 0;
            int CellForeignKey = 0;
            int RaftForeignKeyOrMany = 0;

            List<int> ScanTablePk = new List<int>() { WellForeignKey };
            List<int> WellForeignKeys = new List<int>() { FovForeignKey };
            List<int> FovForeignKeys = new List<int>() { CellForeignKey };
            List<int> CellForeignKeys = new List<int>() { RaftForeignKeyOrMany };
            List<ValueTuple<(int wavelength, int mask, int feature, double valueFloat)>> MeasurementList = new List<ValueTuple<(int wavelength, int mask, int feature, double valueFloat)>>();
            for (int i = 0; i < MyTable.Rows.Count; i++)
            {
                DataRow row = MyTable.Rows[i];
                Object InsertValue = "";

                for (int j = 0; j < row.ItemArray.Length; j++)
                {
                    String ColumnName = MyTable.Columns[j].ColumnName;
                    var item = new ValueTuple<(int wavelength, int mask, int feature, double valueFloat)>();
                    if (UndesiredData.Contains(ColumnName))
                    {
                        continue;
                    }
                    InsertValue = row.ItemArray[j];
                    if (InsertValue == null) continue;

                    int ColumnValue = MyTable.Columns[j].Ordinal;
                    switch (OrdinalToValue?.GetValueOrDefault(ColumnValue).Item1.Column)
                    {
                        case "Scan_Table":
                            ScanList.Add(InsertValue);
                            break;
                        case "Well_Table":
                            WellList.Add(InsertValue);
                            break;
                        case "Measurement_Table":
                            item = GenerateMeasurementTuple(ColumnName);
                            item.Item1.valueFloat = Double.Parse(InsertValue.ToString());
                            MeasurementList.Add(item);
                            break;
                        case "Fov_Table":
                            FovList.Add(InsertValue);
                            break;
                        case "Cell_Table":
                            CellList.Add(InsertValue);
                            break;
                        case "Raft_Table":
                            RaftList.Add(InsertValue);
                            break;
                        default:
                            UnknownList.Add(new ValueTuple<(string Column, string Value)>((Column: ColumnName, Value: InsertValue.ToString())));
                            break;
                    }

                }
            }
        }
                //int newFileId = 1;
               //s await InsertToMeasurementTableAsync(MeasurementList, conn, filename);
                
                //InsertToScanTable(ScanList, conn2, newFileId);
                //InsertToCellTable(CellList, conn3, CellForeignKey);
                //InsertToWellTable(WellList, conn3, WellForeignKey);
                //InsertToFovTable(FovList,   conn,  FovForeignKey);
                //InsertToRaftTable(RaftList, conn, randomIntsForRaft);
                //InsertNewColumn(conn2, UnknownList, fileName); needs a little more testing but it should just
                //dump Column, Value, Type, Time This is for extra columns



        
        public static class Maps
        {
           
            public static int InsertToFileTable(String fileName, NpgsqlConnection conn)
            {

                String FileSqlStatment = @"INSERT INTO ""datalevel"".""FileData""(""FileIdName"") Values(@FileIdName) RETURNING ""Id""";
                try
                {

                    using var cmd = new NpgsqlCommand(FileSqlStatment, conn);
                    cmd.Parameters.AddWithValue("@FileIdName", NpgsqlDbType.Text, Path.GetFileName(fileName));
                    int fileForeignKey = (int)cmd.ExecuteScalar();
                    return fileForeignKey;
                }
                catch (NpgsqlException ex)
                {
                    if ("FileNameUnique" == ex.Data["ConstraintName"].ToString() || ex.Data["ConstraintName"].ToString() == "FileIdUnique")
                    {
                        return -1;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
        
            public static int getMaxKey(String Schema, String Table, String Column, NpgsqlConnection conn)
            {
                try
                {
                    String GetForeignKeys = $@"SELECT ""{Column}"" FROM
                                             ""{Schema}"".""{Table}""
                                             ORDER BY ""{Column}"" DESC";


                    NpgsqlCommand cmd = new NpgsqlCommand(GetForeignKeys, conn);
                    if (cmd.ExecuteScalar() == null)
                    {
                        conn.Close();
                        return 0;
                    }
                    else
                    {
                        return (int)cmd.ExecuteScalar();
                    }
                }
                catch 
                {
                    return 0;
                }
            }

            public static Lookup<String, int> GetLookup()
            {
                return null;
                //return (Lookup<string, int>)ScanTableDictionary().Append(WellDictionary()).Append(FOVDictionary()).Append(CellDictionary()).Append(RaftDictionary()).ToLookup();
            }
            public static ImmutableHashSet<String> CreateMeasurementHashSet()
            {
                return ImmutableHashSet.Create<String>();
                //return FeatureIdMap().Keys.Append(MaskIdMap().Keys.Append(WavelengthIdMap().Keys)).AsEnumerable().ToImmutableHashSet();
            }

            public static  Dictionary<String, int> RoughValuesPerTable()
            {
                Dictionary<String, int> Table_Value = new Dictionary<String, int>();
                Table_Value.Add("SCAN_TABLE", 8);
                Table_Value.Add("WELL_TABLE", 12);
                Table_Value.Add("FOV_TABLE", 7);
                Table_Value.Add("CELL_TABLE", 11);
                Table_Value.Add("RAFT_TABLE", 13);
                Table_Value.Add("FILE_TABLE", 1);
                Table_Value.Add("MEASUREMENT_TABLE", 192);
                Table_Value.Add("OTHER_VALUES", 5);
                return Table_Value;
            }

            public static Dictionary<String, int> ScanTableDictionary()
            {
                Dictionary<String, int> ScanDictionary = new Dictionary<String, int>();
                ScanDictionary.Add("ScanTablePK", 0);
                ScanDictionary.Add("PLATEID", 1);
                ScanDictionary.Add("PLATE ID", 2);
                ScanDictionary.Add("AQP", 3);
                ScanDictionary.Add("INCARTA_PROTOCOLNAME", 4);
                ScanDictionary.Add("RESULTSID", 5);
                ScanDictionary.Add("INDEX", 6);
                ScanDictionary.Add("FILEID", 7);
                return ScanDictionary;
            }

            public static Dictionary<string, int> WellDictionary()
            {
                Dictionary<String, int> Well_Dictionary = new Dictionary<String, int>();
                Well_Dictionary.Add("ScanTableId", 0);
                Well_Dictionary.Add("WELL LABEL", 1);
                Well_Dictionary.Add("ROW", 2);
                Well_Dictionary.Add("COLUMN", 3);
                Well_Dictionary.Add("MLTRAININGLAYOUT", 4);
                Well_Dictionary.Add("MLCLASSLAYOUT", 5);
                Well_Dictionary.Add("MLPLATEINDEX", 6);
                Well_Dictionary.Add("MLMIX", 7);
                Well_Dictionary.Add("PLATE-WELL", 8);
                Well_Dictionary.Add("MLREPLICATES", 9);
                Well_Dictionary.Add("MLCLASSLAYOUT REPLICATES", 10);
                Well_Dictionary.Add("MLCROSSVALFOLD", 11);
                return Well_Dictionary;
            }
            public static HashSet<String> UnneeededHashSet()
            {
                System.Collections.Generic.HashSet<String> UndesiredData = new System.Collections.Generic.HashSet<String>() { "Column1", "MLPREDICTIONPROB", "FILTERED", "MLCLASSPREDICTED", "VAL COUNT" }; ;
                return UnneeededHashSet();
            }
            public static Dictionary<string, int> FOVDictionary()
            {
                Dictionary<String, int> FovDictionary = new Dictionary<String, int>();
                FovDictionary.Add("WellTableId", 0);
                FovDictionary.Add("FOV", 1);
                FovDictionary.Add("IMAGENAME", 2);
                FovDictionary.Add("FOCUS Z POS", 3);
                FovDictionary.Add("FOCUS Z AVG OF ADJACENT FIELDS", 4);
                FovDictionary.Add("ACQUISITION TIME STAMP", 5);
                FovDictionary.Add("CELLSPERFIELD", 6);
                return FovDictionary;
            }

     
            public static Dictionary<String, int> CellDictionary()
            {
                Dictionary<String, int> CellDictionary = new Dictionary<String, int>();
                CellDictionary.Add("FovTableId", 0);
                CellDictionary.Add("OBJECT ID", 1);
                CellDictionary.Add("WELL X UM", 2);
                CellDictionary.Add("WELL Y UM", 3);
                CellDictionary.Add("PLATE X UM", 4);
                CellDictionary.Add("PLATE Y UM", 5);
                CellDictionary.Add("MLCELLSUSE", 6);
                CellDictionary.Add("INPUTROWINDEX", 7);
                CellDictionary.Add("MLTHRESHOLD", 8);
                CellDictionary.Add("MLC1CELLULAR", 9);
                CellDictionary.Add("MITOORGINTEN", 10);
                return CellDictionary;
            }

            public static Dictionary<String, int> RaftDictionary()
            {
                Dictionary<String, int> RaftDictionary = new Dictionary<String, int>();
                RaftDictionary.Add("RaftArrayKeys", 0);
                RaftDictionary.Add("RAFTID", 1);
                RaftDictionary.Add("RAFTROW", 2);
                RaftDictionary.Add("RAFTCOL", 3);
                RaftDictionary.Add("UMFROMBORDEROFRAFT", 4);
                RaftDictionary.Add("IMAGECHECK", 5);
                RaftDictionary.Add("MLQUAD", 6);
                RaftDictionary.Add("MLFIDUCIARYRAFT", 7);
                RaftDictionary.Add("MLIMAGECHECK", 8);
                RaftDictionary.Add("MLC0ALLOWEDREGION", 9);
                RaftDictionary.Add("MLVALIDRAFTS", 10);
                RaftDictionary.Add("MLCELLSPERRAFT", 11);
                RaftDictionary.Add("MLC2ALLOWEDPERRAFT", 12);
                return RaftDictionary;
            }
            public static Dictionary<String, int> FeatureIdMap()
            {
                Dictionary<String, int> Features = new Dictionary<String, int>();
                Features.Add("INTENSITY", 1809);
                Features.Add("TOTAL INTENSITY", 1810);
                Features.Add("INTENSITY CV", 1811);
                Features.Add("LIGHT FLUX", 1812);
                Features.Add("INTENSITY/GLOBAL BCKG", 1813);
                Features.Add("INTENSITY SD", 1814);
                Features.Add("MAX INTENSITY", 1815);
                Features.Add("SKEWNESS", 1816);
                Features.Add("KURTOSIS", 1817);
                Features.Add("RUN LENGTH NON UNIFORMITY", 1823);
                Features.Add("INTENSITY (CELL)", 1824);
                Features.Add("INTENSITY (CYTO)", 1825);
                Features.Add("ENERGY", 1818);
                Features.Add("ENTROPY", 1819);
                Features.Add("GREY LEVEL NON UNIFORMITY", 1820);
                Features.Add("HIGH GREY LEVEL RUN EMPHASIS", 1821);
                Features.Add("LOW GREY LEVEL RUN EMPHASIS", 1822);
                Features.Add("TOTAL INTENSITY (CELL)", 1826);
                Features.Add("TOTAL INTENSITY (CYTO)", 1827);
                Features.Add("INTENSITY CV (CELL)", 1828);
                Features.Add("INTENSITY CV (CYTO)", 1829);
                Features.Add("INTENSITY SPREADING", 1830);
                Features.Add("NUC/CYTO INTENSITY", 1831);
                Features.Add("INTENSITY-BCKG (CELL)", 1832);
                Features.Add("INTENSITY-BCKG(CYTO)", 1833);
                Features.Add("TOTAL INTENSITY-BCKG (CELL)", 1834);
                Features.Add("CELL/BCKG INTENSITY", 1835);
                Features.Add("CYTO/BCKG INTENSITY", 1836);
                Features.Add("CYTO INTENSITY/GLOBAL BCKG", 1837);
                Features.Add("INTENSITY SD (CELL)", 1838);
                Features.Add("INTENSITY SD (CYTO)", 1839);
                Features.Add("ORG PER CELL", 1840);
                Features.Add("AREA", 1841);
                Features.Add("TOTAL AREA", 1842);
                Features.Add("FORM FACTOR", 1843);
                Features.Add("ELONGATION", 1844);
                Features.Add("COMPACTNESS", 1845);
                Features.Add("CHORD RATIO", 1846);
                Features.Add("GYRATION RADIUS", 1847);
                Features.Add("ORGANELLE/CYTO INTENSITY", 1848);
                Features.Add("ORGANELLE/BCKG INTENSITY", 1849);
                Features.Add("INTENSITY-BCKG", 1850);
                Features.Add("TOTAL INTENSITY-BCKG", 1851);
                Features.Add("DISTANCE TO NUC", 1852);
                Features.Add("SPACING", 1853);
                Features.Add("NEIGHBOR COUNT", 1854);
                Features.Add("MAX LEFT BORDER", 1855);
                Features.Add("MAX TOP BORDER", 1856);
                Features.Add("MAX WIDTH", 1857);
                Features.Add("MAX HEIGHT", 1858);
                Features.Add("NUC/CELL AREA", 1859);
                Features.Add("DIAMETER", 1860);
                Features.Add("PERIMETER", 1861);
                Features.Add("DISPLACEMENT", 1862);
                Features.Add("CG X", 1863);
                Features.Add("CG Y", 1864);
                Features.Add("MAJOR AXIS", 1865);
                Features.Add("MINOR AXIS", 1866);
                Features.Add("MAJOR AXIS ANGLE", 1867);
                Features.Add("NEIGHBOR COUNT (SOI)", 1868);
                Features.Add("ANTIHEALTH", 1870);
                return Features;
            }

            public static Dictionary<String, int> MaskIdMap()
            {
                Dictionary<String, int> Mask = new Dictionary<string, int>();
                Mask.Add("NUCLEI", 12);
                Mask.Add("WHOLE_CELL", 14);
                Mask.Add("MITONETWORKS", 16);
                Mask.Add("NEURITES", 17);
                Mask.Add("SOMA", 18);
                Mask.Add("MEMBRANE", 19);
                Mask.Add("PUNCTUA", 20);
                Mask.Add("CELLS", 13);
                Mask.Add("CYTO", 15);
                Mask.Add("noMaskPresent", 20);
                return Mask;
            }

            public static Dictionary<String, int> WavelengthIdMap()
            {
                Dictionary<String, int> Wavelengths = new Dictionary<string, int>();
                Wavelengths.Add("WVH", 1);
                Wavelengths.Add("WVT", 2);
                Wavelengths.Add("WVB", 3);
                Wavelengths.Add("WVM", 4);
                Wavelengths.Add("noWavelength", 5);
                return Wavelengths;
            }


            public static String GrabConnectionString()
            {
                return "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=mlab4;TrustServerCertificate=true;";
                //return "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=N3onblue;TrustServerCertificate=true;";
            }
            private static bool IsJson(string input)
            {
                try
                {
                    Newtonsoft.Json.Linq.JToken.Parse(input); // Or use System.Text.Json.JsonDocument.Parse
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            public static NpgsqlDbType? ObjClassTypeToSQLType(Object obj)
            {
                return obj switch
                {
                    // Numeric types
                    double => (NpgsqlDbType?)NpgsqlDbType.Double,
                    float => (NpgsqlDbType?)NpgsqlDbType.Real,
                    int => (NpgsqlDbType?)NpgsqlDbType.Integer,
                    uint => (NpgsqlDbType?)NpgsqlDbType.Integer,
                    short => (NpgsqlDbType?)NpgsqlDbType.Smallint,
                    ushort => (NpgsqlDbType?)NpgsqlDbType.Smallint,
                    byte => (NpgsqlDbType?)NpgsqlDbType.Smallint,
                    sbyte => (NpgsqlDbType?)NpgsqlDbType.InternalChar,
                    long => (NpgsqlDbType?)NpgsqlDbType.Bigint,
                    ulong => (NpgsqlDbType?)NpgsqlDbType.Bigint,
                    decimal => (NpgsqlDbType?)NpgsqlDbType.Numeric,
                    System.Numerics.BigInteger => (NpgsqlDbType?)NpgsqlDbType.Numeric, // Maps to PostgreSQL NUMERIC

                    // Boolean
                    bool => (NpgsqlDbType?)NpgsqlDbType.Boolean,

                    // String and character types
                    string json when IsJson(json) => (NpgsqlDbType?)NpgsqlDbType.Json, // For plain JSON strings
                    string => (NpgsqlDbType?)NpgsqlDbType.Text,
                    char => (NpgsqlDbType?)NpgsqlDbType.InternalChar, // Single character
                    char[] => (NpgsqlDbType?)NpgsqlDbType.Char,

                    // Date and time types
                    DateTime => (NpgsqlDbType?)NpgsqlDbType.Timestamp, // Without time zone
                    DateTimeOffset => (NpgsqlDbType?)NpgsqlDbType.TimestampTz, // With time zone
                    TimeSpan => (NpgsqlDbType?)NpgsqlDbType.Interval,

                    // Byte arrays
                    byte[] => (NpgsqlDbType?)NpgsqlDbType.Bytea,

                    // UUID
                    Guid => (NpgsqlDbType?)NpgsqlDbType.Uuid,

                    // JSON and JSONB
                    Newtonsoft.Json.Linq.JObject => (NpgsqlDbType?)NpgsqlDbType.Jsonb,
                    System.Text.Json.JsonDocument => (NpgsqlDbType?)NpgsqlDbType.Jsonb,

                    //Objects
                    Rectangle  => (NpgsqlDbType?)NpgsqlDbType.Box,
                    RectangleF  => (NpgsqlDbType?)NpgsqlDbType.Box,
                    Point  => (NpgsqlDbType?)NpgsqlDbType.Point,
                    PointF  => (NpgsqlDbType?)NpgsqlDbType.Point,
                    Line => (NpgsqlDbType?)NpgsqlDbType.Line,
                   
                    // Hstore (dictionary mapping)
                    Dictionary<string, string> => (NpgsqlDbType?)NpgsqlDbType.Hstore,
                    // Hstore (dictionary mapping)
                    KeyValuePair<string, string> => (NpgsqlDbType?)NpgsqlDbType.Hstore,
                    ValueTuple<string, string> => (NpgsqlDbType?)NpgsqlDbType.Hstore,
                    Tuple<string, string> => (NpgsqlDbType?)NpgsqlDbType.Hstore,

                    // Default case
                    _ => null,
                };
            }

            public static Dictionary<String, Type> NonMeasurementTypeMap()
            {
                Dictionary<String, Type> ColumnTypeMap = new Dictionary<String, Type>();
                ColumnTypeMap.Add("PLATEID", typeof(string));
                ColumnTypeMap.Add("PLATE ID", typeof(string));
                ColumnTypeMap.Add("AQP", typeof(string));
                ColumnTypeMap.Add("INCARTA_PROTOCOLNAME", typeof(string));
                ColumnTypeMap.Add("RESULTSID", typeof(string));
                ColumnTypeMap.Add("INDEX", typeof(int));
                ColumnTypeMap.Add("FILEID", typeof(int));
                ColumnTypeMap.Add("SCANTABLEID", typeof(int));
                ColumnTypeMap.Add("WELL LABEL", typeof(string));
                ColumnTypeMap.Add("ROW", typeof(string));
                ColumnTypeMap.Add("COLUMN", typeof(string));
                ColumnTypeMap.Add("MLTRAININGLAYOUT", typeof(string));
                ColumnTypeMap.Add("MLCLASSLAYOUT", typeof(string));
                ColumnTypeMap.Add("MLPLATEINDEX", typeof(string));
                ColumnTypeMap.Add("MLMIX", typeof(string));
                ColumnTypeMap.Add("PLATE-WELL", typeof(string));
                ColumnTypeMap.Add("MLREPLICATES", typeof(string));
                ColumnTypeMap.Add("MLCLASSLAYOUT REPLICATES", typeof(string));
                ColumnTypeMap.Add("MLCROSSVALFOLD", typeof(string));
                ColumnTypeMap.Add("WELLTABLEID", typeof(int));
                ColumnTypeMap.Add("FOV", typeof(int));
                ColumnTypeMap.Add("IMAGENAME", typeof(string));
                ColumnTypeMap.Add("FOCUS Z POS", typeof(double));
                ColumnTypeMap.Add("FOCUS Z AVG OF ADJACENT FIELDS", typeof(double));
                ColumnTypeMap.Add("ACQUISITION TIME STAMP", typeof(int));
                ColumnTypeMap.Add("CELLSPERFIELD", typeof(int));
                ColumnTypeMap.Add("FOVTABLEID", typeof(int));
                ColumnTypeMap.Add("OBJECT ID", typeof(int));
                ColumnTypeMap.Add("WELL X UM", typeof(double));
                ColumnTypeMap.Add("WELL Y UM", typeof(double));
                ColumnTypeMap.Add("PLATE X UM", typeof(double));
                ColumnTypeMap.Add("PLATE Y UM", typeof(double));
                ColumnTypeMap.Add("MLCELLSUSE", typeof(string));
                ColumnTypeMap.Add("INPUTROWINDEX", typeof(int));
                ColumnTypeMap.Add("MLTHRESHOLD", typeof(string));
                ColumnTypeMap.Add("MLC1CELLULAR", typeof(bool));
                ColumnTypeMap.Add("MITOORGINTEN", typeof(double));
                ColumnTypeMap.Add("RAFTARRAYKEYS", typeof(double));
                ColumnTypeMap.Add("RAFTID", typeof(string));
                ColumnTypeMap.Add("RAFTROW", typeof(string));
                ColumnTypeMap.Add("RAFTCOL", typeof(string));
                ColumnTypeMap.Add("UMFROMBORDEROFRAFT", typeof(double));
                ColumnTypeMap.Add("IMAGECHECK", typeof(string));
                ColumnTypeMap.Add("MLQUAD", typeof(string));
                ColumnTypeMap.Add("MLFIDUCIARYRAFT", typeof(bool));
                ColumnTypeMap.Add("MLIMAGECHECK", typeof(string));
                ColumnTypeMap.Add("MLC0ALLOWEDREGION", typeof(bool));
                ColumnTypeMap.Add("MLVALIDRAFTS", typeof(bool));
                ColumnTypeMap.Add("MLCELLSPERRAFT", typeof(int));
                ColumnTypeMap.Add("MLC2ALLOWEDPERRAFT", typeof(bool));
                return ColumnTypeMap;
            }

            public static Dictionary<Type, NpgsqlDbType> ClassTypeToSQLType()
            {
                Dictionary<Type, NpgsqlDbType> ClassToDB = new Dictionary<Type, NpgsqlDbType>();
                ClassToDB.Add(typeof(float), NpgsqlDbType.Real);
                ClassToDB.Add(typeof(double), NpgsqlDbType.Double);
                ClassToDB.Add(typeof(long), NpgsqlDbType.Bigint);
                ClassToDB.Add(typeof(int), NpgsqlDbType.Integer);
                ClassToDB.Add(typeof(short), NpgsqlDbType.Smallint);
                ClassToDB.Add(typeof(byte), NpgsqlDbType.Bytea);
                ClassToDB.Add(typeof(sbyte), NpgsqlDbType.Bytea);
                ClassToDB.Add(typeof(Newtonsoft.Json.JsonToken), NpgsqlDbType.Json);
                ClassToDB.Add(typeof(System.Net.Http.Json.JsonContent), NpgsqlDbType.Json);
                ClassToDB.Add(typeof(Newtonsoft.Json.JsonContainerAttribute), NpgsqlDbType.Json);
                ClassToDB.Add(typeof(uint), NpgsqlDbType.Integer);
                ClassToDB.Add(typeof(ushort), NpgsqlDbType.Smallint);
                ClassToDB.Add(typeof(ulong), NpgsqlDbType.Bigint);
                ClassToDB.Add(typeof(string), NpgsqlDbType.Text);
                ClassToDB.Add(typeof(bool), NpgsqlDbType.Boolean);
                ClassToDB.Add(typeof(char), NpgsqlDbType.Char);
                ClassToDB.Add(typeof(BigInteger), NpgsqlDbType.Numeric);
                ClassToDB.Add(typeof(byte[]), NpgsqlDbType.Bytea);
                ClassToDB.Add(typeof(KeyValuePair<String, String>), NpgsqlDbType.Hstore);
                ClassToDB.Add(typeof(KeyValuePair<String, int>), NpgsqlDbType.Hstore);
                ClassToDB.Add(typeof(DateTime), NpgsqlDbType.Timestamp);
                ClassToDB.Add(typeof(TimeSpan), NpgsqlDbType.Timestamp);
                return ClassToDB;
            }
        }
    }
}



