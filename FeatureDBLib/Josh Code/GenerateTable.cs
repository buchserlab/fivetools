﻿/*
using Loyc.Collections;
using Npgsql.TypeMapping;
using NpgsqlTypes;
using Npgsql;
using Npgsql.NameTranslation;
using System.IO;
using Loyc.Geometry;

namespace Temp
{
    public static class GenerateTable
    {
        //header, //npgsqltype                                                    
        public static List<String> AlterTableStrings(List<KeyValuePair<int, Object>> block, List<String> headers)
        {
            List<String> alterTableStrings = new List<String>();
            Dictionary<String, String> TypeConverter = new Dictionary<String, String>();
            List<String> needsExamination = new List<String>();
            int headerCount = headers.Count;
            headers.Reverse();
            for (int i = 0; i < headers.Count; i++)
            {
                KeyValuePair<int, Object> keyValuePa = new(0, headers[i]);

                block.Insert(0, keyValuePa);
            }

            BMultiMap<String, String> score = new BMultiMap<String, String>();
            score.Keys.WithIndexes();
            score.Values.WithIndexes();
            int count = 0;
            int totalCounter = 0;
            String? name = "";
            bool subtract = false;
            foreach (KeyValuePair<int, Object> item in block)
            {
                totalCounter = totalCounter + 1;
                name = item.Value.ToString();
                count = count + 1;
             
                if (name?.ToString() == "0")
                {
                    name = "examine";
                }
                else
                {
                    if (String.IsNullOrWhiteSpace(name?.ToString()))
                    {
                        needsExamination.Add(name);
                    }
                    else if (name.Contains("."))
                    {
                        name = "Double Precision";
                    }
                    else if (
                    name.StartsWith("10") ||
                    name.StartsWith("11") ||
                    name.StartsWith("12") ||
                    name.StartsWith("13") ||
                    name.StartsWith("14") ||
                    name.StartsWith("15") ||
                    name.StartsWith("16") ||
                    name.StartsWith("17") ||
                    name.StartsWith("18") ||
                    name.StartsWith("19") ||
                    name.StartsWith("2") ||
                    name.StartsWith("3") ||
                    name.StartsWith("4") ||
                    name.StartsWith("5") ||
                    name.StartsWith("6") ||
                    name.StartsWith("7") ||
                    name.StartsWith("8") ||
                    name.StartsWith("9"))
                    {
                        name = "integer";
                    }
                    else if (name == "0")
                    {
                    }
                    else if (name.ToUpper() == "TRUE"
                    || name == "T" ||
                    name == "Y" ||
                    name == "YES"
                    || name == "F" ||
                    name == "FALSE"
                    || name == "N" ||
                    name == "FALSE" ||
                    name == "NO" ||
                    name == "OFF")
                    {
                        name ="Boolean";
                    }

                    else
                    {
                        name = "text";
                    }
                }
                if (totalCounter == 25000)
                    break;

                String t = "ALTER TABLE IF EXISTS  experimentschema.experimenttable  ADD  COLUMN"   + "   " +  headers[count - 1]  + "   " + name +  "  NOT  NULL";
                if (headers.Count == count)
                {
                    count = 1;
                }
                alterTableStrings.Add(t);

                

            }

            List<String> Final = new List<string>();
            using (TextReader filereader = new StreamReader("C:\\Users\\jdmso\\OneDrive\\Documents\\CreateOutputFile.txt"))
            {
                alterTableStrings.OrderBy(t => t.Count()).ThenBy(f => f.ToString()).ToList();
                foreach (String da in alterTableStrings)
                {


                    String? Line = filereader.ReadLine(); ;
                    String alterTable = "ALTER TABLE IF EXISTS experimentschema.experimenttable  ADD  " +  Line + "  NOT NULL";

                }
            }

            return new List<string>();

        }

    }
}








/*
 
 AQP	text		
PLATEID	text		
INCARTA_PROTOCOLNAME	text
RESULTSID	text
INDEX	Integer
VAL COUNT	Integer		
OBJECT ID	Integer		
ROW	text		
COLUMN	Integer		
FOV	Integer	
WELL LABEL	text		
NUCLEI MAX WIDTH WVH	Double Precision		
NUCLEI MAX HEIGHT WVH	Double Precision		
NUCLEI AREA WVH	Double Precision		
NUCLEI FORM FACTOR WVH	Double Precision		
NUCLEI ELONGATION WVH	Double Precision		
NUCLEI COMPACTNESS WVH	Double Precision		
NUCLEI CHORD RATIO WVH	Double Precision		
NUCLEI GYRATION RADIUS WVH	Double Precision		
NUCLEI DISPLACEMENT WVH	Double Precision		
NUCLEI DIAMETER WVH	Double Precision		
NUCLEI PERIMETER WVH	Double Precision		
NUCLEI INTENSITY WVH	Double Precision		
NUCLEI TOTAL INTENSITY WVH	Double Precision		
NUCLEI INTENSITY CV WVH	Double Precision		
NUCLEI LIGHT FLUX WVH	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVH	Double Precision		
NUCLEI INTENSITY SD WVH	Double Precision		
NUCLEI MAX INTENSITY WVH	Double Precision		
NUCLEI CG X WVH	Double Precision		
NUCLEI CG Y WVH	Double Precision		
NUCLEI MAJOR AXIS WVH	Double Precision		
NUCLEI MINOR AXIS WVH	Double Precision		
NUCLEI MAJOR AXIS ANGLE WVH	Double Precision		
NUCLEI NEIGHBOR COUNT (SOI) WVH	Integer		
NUCLEI SKEWNESS WVH	Double Precision		
NUCLEI KURTOSIS WVH	Double Precision		
NUCLEI ENERGY WVH	Double Precision		
NUCLEI ENTROPY WVH	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVH	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVH	Double Precision		
NUCLEI INTENSITY WVM	Double Precision		
NUCLEI TOTAL INTENSITY WVM	Double Precision		
NUCLEI INTENSITY CV WVM	Double Precision		
NUCLEI LIGHT FLUX WVM	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVM	Double Precision		
NUCLEI INTENSITY SD WVM	Double Precision		
NUCLEI MAX INTENSITY WVM	Double Precision		
NUCLEI SKEWNESS WVM	Double Precision		
NUCLEI KURTOSIS WVM	Double Precision		
NUCLEI ENERGY WVM	Double Precision		
NUCLEI ENTROPY WVM	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVM	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVM	Double Precision		
NUCLEI INTENSITY WVT	Double Precision		
NUCLEI TOTAL INTENSITY WVT	Double Precision		
NUCLEI INTENSITY CV WVT	Double Precision		
NUCLEI LIGHT FLUX WVT	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVT	Double Precision		
NUCLEI INTENSITY SD WVT	Double Precision		
NUCLEI MAX INTENSITY WVT	Double Precision		
NUCLEI SKEWNESS WVT	Double Precision		
NUCLEI KURTOSIS WVT	Double Precision		
NUCLEI ENERGY WVT	Double Precision		
NUCLEI ENTROPY WVT	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVT	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVT	Double Precision		
CELLS MAX LEFT BORDER WVB	Double Precision		
CELLS MAX TOP BORDER WVB	Double Precision		
CELLS MAX WIDTH WVB	Double Precision		
CELLS MAX HEIGHT WVB	Double Precision		
CELLS AREA WVB	Double Precision		
CELLS FORM FACTOR WVB	Double Precision		
CELLS ELONGATION WVB	Double Precision		
CELLS COMPACTNESS WVB	Double Precision		
CELLS CHORD RATIO WVB	Double Precision		
CELLS GYRATION RADIUS WVB	Double Precision		
CELLS NUC/CELL AREA WVB	Double Precision		
CELLS DIAMETER WVB	Double Precision		
CELLS PERIMETER WVB	Double Precision		
CELLS INTENSITY (CELL) WVB	Double Precision		
CELLS INTENSITY (CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVB	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVB	Double Precision		
CELLS INTENSITY CV (CELL) WVB	Double Precision		
CELLS INTENSITY CV (CYTO) WVB	Double Precision		
CELLS INTENSITY SPREADING WVB	Double Precision		
CELLS LIGHT FLUX WVB	Double Precision		
CELLS NUC/CYTO INTENSITY WVB	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS CELL/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVB	Double Precision		
CELLS INTENSITY SD (CELL) WVB	Double Precision		
CELLS INTENSITY SD (CYTO) WVB	Double Precision		
CELLS MAX INTENSITY WVB	Double Precision		
CELLS SKEWNESS WVB	Double Precision		
CELLS KURTOSIS WVB	Double Precision		
CELLS ENERGY WVB	Double Precision		
CELLS ENTROPY WVB	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVB	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVB	Double Precision		
CELLS INTENSITY (CELL) WVM	Double Precision		
CELLS INTENSITY (CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVM	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVM	Double Precision		
CELLS INTENSITY CV (CELL) WVM	Double Precision		
CELLS INTENSITY CV (CYTO) WVM	Double Precision		
CELLS INTENSITY SPREADING WVM	Double Precision		
CELLS LIGHT FLUX WVM	Double Precision		
CELLS NUC/CYTO INTENSITY WVM	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS CELL/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVM	Double Precision		
CELLS INTENSITY SD (CELL) WVM	Double Precision		
CELLS INTENSITY SD (CYTO) WVM	Double Precision		
CELLS MAX INTENSITY WVM	Double Precision		
CELLS SKEWNESS WVM	Double Precision		
CELLS KURTOSIS WVM	Double Precision		
CELLS ENERGY WVM	Double Precision		
CELLS ENTROPY WVM	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVM	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVM	Double Precision		
CELLS INTENSITY (CELL) WVT	Double Precision		
CELLS INTENSITY (CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVT	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVT	Double Precision		
CELLS INTENSITY CV (CELL) WVT	Double Precision		
CELLS INTENSITY CV (CYTO) WVT	Double Precision		
CELLS INTENSITY SPREADING WVT	Double Precision		
CELLS LIGHT FLUX WVT	Double Precision		
CELLS NUC/CYTO INTENSITY WVT	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS CELL/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVT	Double Precision		
CELLS INTENSITY SD (CELL) WVT	Double Precision		
CELLS INTENSITY SD (CYTO) WVT	Double Precision		
CELLS MAX INTENSITY WVT	Double Precision		
CELLS SKEWNESS WVT	Double Precision		
CELLS KURTOSIS WVT	Double Precision		
CELLS ENERGY WVT	Double Precision		
CELLS ENTROPY WVT	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVT	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVT	Double Precision		
MITONETWORKS ORG PER CELL WVM	Double Precision		
MITONETWORKS AREA WVM	Double Precision		
MITONETWORKS TOTAL AREA WVM	Double Precision		
MITONETWORKS FORM FACTOR WVM	Double Precision		
MITONETWORKS ELONGATION WVM	Double Precision		
MITONETWORKS COMPACTNESS WVM	Double Precision		
MITONETWORKS CHORD RATIO WVM	Double Precision		
MITONETWORKS GYRATION RADIUS WVM	Double Precision		
MITONETWORKS INTENSITY WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY CV WVM	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVM	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVM	Double Precision		
MITONETWORKS INTENSITY SPREADING WVM	Double Precision		
MITONETWORKS INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS DISTANCE TO NUC WVM	Double Precision		
MITONETWORKS SPACING WVM	Double Precision		
MITONETWORKS NEIGHBOR COUNT WVM	Double Precision		
MITONETWORKS INTENSITY WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY CV WVH	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVH	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVH	Double Precision		
MITONETWORKS INTENSITY SPREADING WVH	Double Precision		
MITONETWORKS INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS INTENSITY WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY CV WVT	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVT	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVT	Double Precision		
MITONETWORKS INTENSITY SPREADING WVT	Double Precision		
MITONETWORKS INTENSITY-BCKG WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVT	Double Precision		
ACQUISITION TIME STAMP	Double Precision		
PLATE ID	text		
WELL X UM	Double Precision		
WELL Y UM	Double Precision		
PLATE X UM	Double Precision		
PLATE Y UM	Double Precision		
IMAGENAME	text		
FOCUS Z POS	Double Precision		
FOCUS Z AVG OF ADJACENT FIELDS	Double Precision		
RAFTID	text		
RAFTROW	text		
RAFTCOL	text		
UMFROMBORDEROFRAFT	Double Precision		
IMAGECHECK	text		
CELLSPERFIELD	Integer		
PLATE-WELL	text		
MLTRAININGLAYOUT	text		
MLCLASSLAYOUT	text		
MLREPLICATES	text		
MLCELLSUSE	text		
INPUTROWINDEX	Integer		
MLCLASSLAYOUT REPLICATES	text		
MLPREDICTIONPROB	Double Precision		
MLCLASSPREDICTED	text		
MLQUAD	Integer		
MLPLATEINDEX	Integer		
MLFIDUCIARYRAFT	Boolean		
MLTHRESHOLD	text		
MLIMAGECHECK	text		
MLMIX	text		
MLC0ALLOWEDREGION	Boolean		
MLC1CELLULAR	Boolean		
MLVALIDRAFTS	Boolean		
MLCROSSVALFOLD	Integer		
MLCELLSPERRAFT	Integer		
MLC2ALLOWEDPERRAFT	Boolean		
MITOORGINTEN	Double Precision
Instances:   104   Item:   [ALTER TABLE IF EXISTS  experimentschema.experimenttable  ADD  COLUMN   AQP	text		
PLATEID	text		
INCARTA_PROTOCOLNAME	text
RESULTSID	text
INDEX	Integer
VAL COUNT	Integer		
OBJECT ID	Integer		
ROW	text		
COLUMN	Integer		
FOV	Integer	
WELL LABEL	text		
NUCLEI MAX WIDTH WVH	Double Precision		
NUCLEI MAX HEIGHT WVH	Double Precision		
NUCLEI AREA WVH	Double Precision		
NUCLEI FORM FACTOR WVH	Double Precision		
NUCLEI ELONGATION WVH	Double Precision		
NUCLEI COMPACTNESS WVH	Double Precision		
NUCLEI CHORD RATIO WVH	Double Precision		
NUCLEI GYRATION RADIUS WVH	Double Precision		
NUCLEI DISPLACEMENT WVH	Double Precision		
NUCLEI DIAMETER WVH	Double Precision		
NUCLEI PERIMETER WVH	Double Precision		
NUCLEI INTENSITY WVH	Double Precision		
NUCLEI TOTAL INTENSITY WVH	Double Precision		
NUCLEI INTENSITY CV WVH	Double Precision		
NUCLEI LIGHT FLUX WVH	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVH	Double Precision		
NUCLEI INTENSITY SD WVH	Double Precision		
NUCLEI MAX INTENSITY WVH	Double Precision		
NUCLEI CG X WVH	Double Precision		
NUCLEI CG Y WVH	Double Precision		
NUCLEI MAJOR AXIS WVH	Double Precision		
NUCLEI MINOR AXIS WVH	Double Precision		
NUCLEI MAJOR AXIS ANGLE WVH	Double Precision		
NUCLEI NEIGHBOR COUNT (SOI) WVH	Integer		
NUCLEI SKEWNESS WVH	Double Precision		
NUCLEI KURTOSIS WVH	Double Precision		
NUCLEI ENERGY WVH	Double Precision		
NUCLEI ENTROPY WVH	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVH	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVH	Double Precision		
NUCLEI INTENSITY WVM	Double Precision		
NUCLEI TOTAL INTENSITY WVM	Double Precision		
NUCLEI INTENSITY CV WVM	Double Precision		
NUCLEI LIGHT FLUX WVM	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVM	Double Precision		
NUCLEI INTENSITY SD WVM	Double Precision		
NUCLEI MAX INTENSITY WVM	Double Precision		
NUCLEI SKEWNESS WVM	Double Precision		
NUCLEI KURTOSIS WVM	Double Precision		
NUCLEI ENERGY WVM	Double Precision		
NUCLEI ENTROPY WVM	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVM	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVM	Double Precision		
NUCLEI INTENSITY WVT	Double Precision		
NUCLEI TOTAL INTENSITY WVT	Double Precision		
NUCLEI INTENSITY CV WVT	Double Precision		
NUCLEI LIGHT FLUX WVT	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVT	Double Precision		
NUCLEI INTENSITY SD WVT	Double Precision		
NUCLEI MAX INTENSITY WVT	Double Precision		
NUCLEI SKEWNESS WVT	Double Precision		
NUCLEI KURTOSIS WVT	Double Precision		
NUCLEI ENERGY WVT	Double Precision		
NUCLEI ENTROPY WVT	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVT	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVT	Double Precision		
CELLS MAX LEFT BORDER WVB	Double Precision		
CELLS MAX TOP BORDER WVB	Double Precision		
CELLS MAX WIDTH WVB	Double Precision		
CELLS MAX HEIGHT WVB	Double Precision		
CELLS AREA WVB	Double Precision		
CELLS FORM FACTOR WVB	Double Precision		
CELLS ELONGATION WVB	Double Precision		
CELLS COMPACTNESS WVB	Double Precision		
CELLS CHORD RATIO WVB	Double Precision		
CELLS GYRATION RADIUS WVB	Double Precision		
CELLS NUC/CELL AREA WVB	Double Precision		
CELLS DIAMETER WVB	Double Precision		
CELLS PERIMETER WVB	Double Precision		
CELLS INTENSITY (CELL) WVB	Double Precision		
CELLS INTENSITY (CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVB	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVB	Double Precision		
CELLS INTENSITY CV (CELL) WVB	Double Precision		
CELLS INTENSITY CV (CYTO) WVB	Double Precision		
CELLS INTENSITY SPREADING WVB	Double Precision		
CELLS LIGHT FLUX WVB	Double Precision		
CELLS NUC/CYTO INTENSITY WVB	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS CELL/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVB	Double Precision		
CELLS INTENSITY SD (CELL) WVB	Double Precision		
CELLS INTENSITY SD (CYTO) WVB	Double Precision		
CELLS MAX INTENSITY WVB	Double Precision		
CELLS SKEWNESS WVB	Double Precision		
CELLS KURTOSIS WVB	Double Precision		
CELLS ENERGY WVB	Double Precision		
CELLS ENTROPY WVB	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVB	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVB	Double Precision		
CELLS INTENSITY (CELL) WVM	Double Precision		
CELLS INTENSITY (CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVM	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVM	Double Precision		
CELLS INTENSITY CV (CELL) WVM	Double Precision		
CELLS INTENSITY CV (CYTO) WVM	Double Precision		
CELLS INTENSITY SPREADING WVM	Double Precision		
CELLS LIGHT FLUX WVM	Double Precision		
CELLS NUC/CYTO INTENSITY WVM	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS CELL/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVM	Double Precision		
CELLS INTENSITY SD (CELL) WVM	Double Precision		
CELLS INTENSITY SD (CYTO) WVM	Double Precision		
CELLS MAX INTENSITY WVM	Double Precision		
CELLS SKEWNESS WVM	Double Precision		
CELLS KURTOSIS WVM	Double Precision		
CELLS ENERGY WVM	Double Precision		
CELLS ENTROPY WVM	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVM	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVM	Double Precision		
CELLS INTENSITY (CELL) WVT	Double Precision		
CELLS INTENSITY (CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVT	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVT	Double Precision		
CELLS INTENSITY CV (CELL) WVT	Double Precision		
CELLS INTENSITY CV (CYTO) WVT	Double Precision		
CELLS INTENSITY SPREADING WVT	Double Precision		
CELLS LIGHT FLUX WVT	Double Precision		
CELLS NUC/CYTO INTENSITY WVT	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS CELL/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVT	Double Precision		
CELLS INTENSITY SD (CELL) WVT	Double Precision		
CELLS INTENSITY SD (CYTO) WVT	Double Precision		
CELLS MAX INTENSITY WVT	Double Precision		
CELLS SKEWNESS WVT	Double Precision		
CELLS KURTOSIS WVT	Double Precision		
CELLS ENERGY WVT	Double Precision		
CELLS ENTROPY WVT	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVT	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVT	Double Precision		
MITONETWORKS ORG PER CELL WVM	Double Precision		
MITONETWORKS AREA WVM	Double Precision		
MITONETWORKS TOTAL AREA WVM	Double Precision		
MITONETWORKS FORM FACTOR WVM	Double Precision		
MITONETWORKS ELONGATION WVM	Double Precision		
MITONETWORKS COMPACTNESS WVM	Double Precision		
MITONETWORKS CHORD RATIO WVM	Double Precision		
MITONETWORKS GYRATION RADIUS WVM	Double Precision		
MITONETWORKS INTENSITY WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY CV WVM	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVM	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVM	Double Precision		
MITONETWORKS INTENSITY SPREADING WVM	Double Precision		
MITONETWORKS INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS DISTANCE TO NUC WVM	Double Precision		
MITONETWORKS SPACING WVM	Double Precision		
MITONETWORKS NEIGHBOR COUNT WVM	Double Precision		
MITONETWORKS INTENSITY WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY CV WVH	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVH	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVH	Double Precision		
MITONETWORKS INTENSITY SPREADING WVH	Double Precision		
MITONETWORKS INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS INTENSITY WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY CV WVT	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVT	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVT	Double Precision		
MITONETWORKS INTENSITY SPREADING WVT	Double Precision		
MITONETWORKS INTENSITY-BCKG WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVT	Double Precision		
ACQUISITION TIME STAMP	Double Precision		
PLATE ID	text		
WELL X UM	Double Precision		
WELL Y UM	Double Precision		
PLATE X UM	Double Precision		
PLATE Y UM	Double Precision		
IMAGENAME	text		
FOCUS Z POS	Double Precision		
FOCUS Z AVG OF ADJACENT FIELDS	Double Precision		
RAFTID	text		
RAFTROW	text		
RAFTCOL	text		
UMFROMBORDEROFRAFT	Double Precision		
IMAGECHECK	text		
CELLSPERFIELD	Integer		
PLATE-WELL	text		
MLTRAININGLAYOUT	text		
MLCLASSLAYOUT	text		
MLREPLICATES	text		
MLCELLSUSE	text		
INPUTROWINDEX	Integer		
MLCLASSLAYOUT REPLICATES	text		
MLPREDICTIONPROB	Double Precision		
MLCLASSPREDICTED	text		
MLQUAD	Integer		
MLPLATEINDEX	Integer		
MLFIDUCIARYRAFT	Boolean		
MLTHRESHOLD	text		
MLIMAGECHECK	text		
MLMIX	text		
MLC0ALLOWEDREGION	Boolean		
MLC1CELLULAR	Boolean		
MLVALIDRAFTS	Boolean		
MLCROSSVALFOLD	Integer		
MLCELLSPERRAFT	Integer		
MLC2ALLOWEDPERRAFT	Boolean		
MITOORGINTEN	Double Precision

AQP	text		
PLATEID	text		
INCARTA_PROTOCOLNAME	text
RESULTSID	text
INDEX	Integer
VAL COUNT	Integer		
OBJECT ID	Integer		
ROW	text		
COLUMN	Integer		
FOV	Integer	
WELL LABEL	text		
NUCLEI MAX WIDTH WVH	Double Precision		
NUCLEI MAX HEIGHT WVH	Double Precision		
NUCLEI AREA WVH	Double Precision		
NUCLEI FORM FACTOR WVH	Double Precision		
NUCLEI ELONGATION WVH	Double Precision		
NUCLEI COMPACTNESS WVH	Double Precision		
NUCLEI CHORD RATIO WVH	Double Precision		
NUCLEI GYRATION RADIUS WVH	Double Precision		
NUCLEI DISPLACEMENT WVH	Double Precision		
NUCLEI DIAMETER WVH	Double Precision		
NUCLEI PERIMETER WVH	Double Precision		
NUCLEI INTENSITY WVH	Double Precision		
NUCLEI TOTAL INTENSITY WVH	Double Precision		
NUCLEI INTENSITY CV WVH	Double Precision		
NUCLEI LIGHT FLUX WVH	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVH	Double Precision		
NUCLEI INTENSITY SD WVH	Double Precision		
NUCLEI MAX INTENSITY WVH	Double Precision		
NUCLEI CG X WVH	Double Precision		
NUCLEI CG Y WVH	Double Precision		
NUCLEI MAJOR AXIS WVH	Double Precision		
NUCLEI MINOR AXIS WVH	Double Precision		
NUCLEI MAJOR AXIS ANGLE WVH	Double Precision		
NUCLEI NEIGHBOR COUNT (SOI) WVH	Integer		
NUCLEI SKEWNESS WVH	Double Precision		
NUCLEI KURTOSIS WVH	Double Precision		
NUCLEI ENERGY WVH	Double Precision		
NUCLEI ENTROPY WVH	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVH	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVH	Double Precision		
NUCLEI INTENSITY WVM	Double Precision		
NUCLEI TOTAL INTENSITY WVM	Double Precision		
NUCLEI INTENSITY CV WVM	Double Precision		
NUCLEI LIGHT FLUX WVM	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVM	Double Precision		
NUCLEI INTENSITY SD WVM	Double Precision		
NUCLEI MAX INTENSITY WVM	Double Precision		
NUCLEI SKEWNESS WVM	Double Precision		
NUCLEI KURTOSIS WVM	Double Precision		
NUCLEI ENERGY WVM	Double Precision		
NUCLEI ENTROPY WVM	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVM	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVM	Double Precision		
NUCLEI INTENSITY WVT	Double Precision		
NUCLEI TOTAL INTENSITY WVT	Double Precision		
NUCLEI INTENSITY CV WVT	Double Precision		
NUCLEI LIGHT FLUX WVT	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVT	Double Precision		
NUCLEI INTENSITY SD WVT	Double Precision		
NUCLEI MAX INTENSITY WVT	Double Precision		
NUCLEI SKEWNESS WVT	Double Precision		
NUCLEI KURTOSIS WVT	Double Precision		
NUCLEI ENERGY WVT	Double Precision		
NUCLEI ENTROPY WVT	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVT	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVT	Double Precision		
CELLS MAX LEFT BORDER WVB	Double Precision		
CELLS MAX TOP BORDER WVB	Double Precision		
CELLS MAX WIDTH WVB	Double Precision		
CELLS MAX HEIGHT WVB	Double Precision		
CELLS AREA WVB	Double Precision		
CELLS FORM FACTOR WVB	Double Precision		
CELLS ELONGATION WVB	Double Precision		
CELLS COMPACTNESS WVB	Double Precision		
CELLS CHORD RATIO WVB	Double Precision		
CELLS GYRATION RADIUS WVB	Double Precision		
CELLS NUC/CELL AREA WVB	Double Precision		
CELLS DIAMETER WVB	Double Precision		
CELLS PERIMETER WVB	Double Precision		
CELLS INTENSITY (CELL) WVB	Double Precision		
CELLS INTENSITY (CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVB	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVB	Double Precision		
CELLS INTENSITY CV (CELL) WVB	Double Precision		
CELLS INTENSITY CV (CYTO) WVB	Double Precision		
CELLS INTENSITY SPREADING WVB	Double Precision		
CELLS LIGHT FLUX WVB	Double Precision		
CELLS NUC/CYTO INTENSITY WVB	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS CELL/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVB	Double Precision		
CELLS INTENSITY SD (CELL) WVB	Double Precision		
CELLS INTENSITY SD (CYTO) WVB	Double Precision		
CELLS MAX INTENSITY WVB	Double Precision		
CELLS SKEWNESS WVB	Double Precision		
CELLS KURTOSIS WVB	Double Precision		
CELLS ENERGY WVB	Double Precision		
CELLS ENTROPY WVB	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVB	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVB	Double Precision		
CELLS INTENSITY (CELL) WVM	Double Precision		
CELLS INTENSITY (CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVM	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVM	Double Precision		
CELLS INTENSITY CV (CELL) WVM	Double Precision		
CELLS INTENSITY CV (CYTO) WVM	Double Precision		
CELLS INTENSITY SPREADING WVM	Double Precision		
CELLS LIGHT FLUX WVM	Double Precision		
CELLS NUC/CYTO INTENSITY WVM	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS CELL/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVM	Double Precision		
CELLS INTENSITY SD (CELL) WVM	Double Precision		
CELLS INTENSITY SD (CYTO) WVM	Double Precision		
CELLS MAX INTENSITY WVM	Double Precision		
CELLS SKEWNESS WVM	Double Precision		
CELLS KURTOSIS WVM	Double Precision		
CELLS ENERGY WVM	Double Precision		
CELLS ENTROPY WVM	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVM	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVM	Double Precision		
CELLS INTENSITY (CELL) WVT	Double Precision		
CELLS INTENSITY (CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVT	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVT	Double Precision		
CELLS INTENSITY CV (CELL) WVT	Double Precision		
CELLS INTENSITY CV (CYTO) WVT	Double Precision		
CELLS INTENSITY SPREADING WVT	Double Precision		
CELLS LIGHT FLUX WVT	Double Precision		
CELLS NUC/CYTO INTENSITY WVT	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS CELL/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVT	Double Precision		
CELLS INTENSITY SD (CELL) WVT	Double Precision		
CELLS INTENSITY SD (CYTO) WVT	Double Precision		
CELLS MAX INTENSITY WVT	Double Precision		
CELLS SKEWNESS WVT	Double Precision		
CELLS KURTOSIS WVT	Double Precision		
CELLS ENERGY WVT	Double Precision		
CELLS ENTROPY WVT	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVT	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVT	Double Precision		
MITONETWORKS ORG PER CELL WVM	Double Precision		
MITONETWORKS AREA WVM	Double Precision		
MITONETWORKS TOTAL AREA WVM	Double Precision		
MITONETWORKS FORM FACTOR WVM	Double Precision		
MITONETWORKS ELONGATION WVM	Double Precision		
MITONETWORKS COMPACTNESS WVM	Double Precision		
MITONETWORKS CHORD RATIO WVM	Double Precision		
MITONETWORKS GYRATION RADIUS WVM	Double Precision		
MITONETWORKS INTENSITY WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY CV WVM	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVM	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVM	Double Precision		
MITONETWORKS INTENSITY SPREADING WVM	Double Precision		
MITONETWORKS INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS DISTANCE TO NUC WVM	Double Precision		
MITONETWORKS SPACING WVM	Double Precision		
MITONETWORKS NEIGHBOR COUNT WVM	Double Precision		
MITONETWORKS INTENSITY WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY CV WVH	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVH	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVH	Double Precision		
MITONETWORKS INTENSITY SPREADING WVH	Double Precision		
MITONETWORKS INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS INTENSITY WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY CV WVT	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVT	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVT	Double Precision		
MITONETWORKS INTENSITY SPREADING WVT	Double Precision		
MITONETWORKS INTENSITY-BCKG WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVT	Double Precision		
ACQUISITION TIME STAMP	Double Precision		
PLATE ID	text		
WELL X UM	Double Precision		
WELL Y UM	Double Precision		
PLATE X UM	Double Precision		
PLATE Y UM	Double Precision		
IMAGENAME	text		
FOCUS Z POS	Double Precision		
FOCUS Z AVG OF ADJACENT FIELDS	Double Precision		
RAFTID	text		
RAFTROW	text		
RAFTCOL	text		
UMFROMBORDEROFRAFT	Double Precision		
IMAGECHECK	text		
CELLSPERFIELD	Integer		
PLATE-WELL	text		
MLTRAININGLAYOUT	text		
MLCLASSLAYOUT	text		
MLREPLICATES	text		
MLCELLSUSE	text		
INPUTROWINDEX	Integer		
MLCLASSLAYOUT REPLICATES	text		
MLPREDICTIONPROB	Double Precision		
MLCLASSPREDICTED	text		
MLQUAD	Integer		
MLPLATEINDEX	Integer		
MLFIDUCIARYRAFT	Boolean		
MLTHRESHOLD	text		
MLIMAGECHECK	text		
MLMIX	text		
MLC0ALLOWEDREGION	Boolean		
MLC1CELLULAR	Boolean		
MLVALIDRAFTS	Boolean		
MLCROSSVALFOLD	Integer		
MLCELLSPERRAFT	Integer		
MLC2ALLOWEDPERRAFT	Boolean		
MITOORGINTEN	Double Precision
Instances:   104   Item:   [ALTER TABLE IF EXISTS  experimentschema.experimenttable  ADD  COLUMN   AQP	text		
PLATEID	text		
INCARTA_PROTOCOLNAME	text
RESULTSID	text
INDEX	Integer
VAL COUNT	Integer		
OBJECT ID	Integer		
ROW	text		
COLUMN	Integer		
FOV	Integer	
WELL LABEL	text		
NUCLEI MAX WIDTH WVH	Double Precision		
NUCLEI MAX HEIGHT WVH	Double Precision		
NUCLEI AREA WVH	Double Precision		
NUCLEI FORM FACTOR WVH	Double Precision		
NUCLEI ELONGATION WVH	Double Precision		
NUCLEI COMPACTNESS WVH	Double Precision		
NUCLEI CHORD RATIO WVH	Double Precision		
NUCLEI GYRATION RADIUS WVH	Double Precision		
NUCLEI DISPLACEMENT WVH	Double Precision		
NUCLEI DIAMETER WVH	Double Precision		
NUCLEI PERIMETER WVH	Double Precision		
NUCLEI INTENSITY WVH	Double Precision		
NUCLEI TOTAL INTENSITY WVH	Double Precision		
NUCLEI INTENSITY CV WVH	Double Precision		
NUCLEI LIGHT FLUX WVH	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVH	Double Precision		
NUCLEI INTENSITY SD WVH	Double Precision		
NUCLEI MAX INTENSITY WVH	Double Precision		
NUCLEI CG X WVH	Double Precision		
NUCLEI CG Y WVH	Double Precision		
NUCLEI MAJOR AXIS WVH	Double Precision		
NUCLEI MINOR AXIS WVH	Double Precision		
NUCLEI MAJOR AXIS ANGLE WVH	Double Precision		
NUCLEI NEIGHBOR COUNT (SOI) WVH	Integer		
NUCLEI SKEWNESS WVH	Double Precision		
NUCLEI KURTOSIS WVH	Double Precision		
NUCLEI ENERGY WVH	Double Precision		
NUCLEI ENTROPY WVH	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVH	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVH	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVH	Double Precision		
NUCLEI INTENSITY WVM	Double Precision		
NUCLEI TOTAL INTENSITY WVM	Double Precision		
NUCLEI INTENSITY CV WVM	Double Precision		
NUCLEI LIGHT FLUX WVM	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVM	Double Precision		
NUCLEI INTENSITY SD WVM	Double Precision		
NUCLEI MAX INTENSITY WVM	Double Precision		
NUCLEI SKEWNESS WVM	Double Precision		
NUCLEI KURTOSIS WVM	Double Precision		
NUCLEI ENERGY WVM	Double Precision		
NUCLEI ENTROPY WVM	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVM	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVM	Double Precision		
NUCLEI INTENSITY WVT	Double Precision		
NUCLEI TOTAL INTENSITY WVT	Double Precision		
NUCLEI INTENSITY CV WVT	Double Precision		
NUCLEI LIGHT FLUX WVT	Double Precision		
NUCLEI INTENSITY/GLOBAL BCKG WVT	Double Precision		
NUCLEI INTENSITY SD WVT	Double Precision		
NUCLEI MAX INTENSITY WVT	Double Precision		
NUCLEI SKEWNESS WVT	Double Precision		
NUCLEI KURTOSIS WVT	Double Precision		
NUCLEI ENERGY WVT	Double Precision		
NUCLEI ENTROPY WVT	Double Precision		
NUCLEI GREY LEVEL NON UNIFORMITY WVT	Double Precision		
NUCLEI HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
NUCLEI RUN LENGTH NON UNIFORMITY WVT	Double Precision		
CELLS MAX LEFT BORDER WVB	Double Precision		
CELLS MAX TOP BORDER WVB	Double Precision		
CELLS MAX WIDTH WVB	Double Precision		
CELLS MAX HEIGHT WVB	Double Precision		
CELLS AREA WVB	Double Precision		
CELLS FORM FACTOR WVB	Double Precision		
CELLS ELONGATION WVB	Double Precision		
CELLS COMPACTNESS WVB	Double Precision		
CELLS CHORD RATIO WVB	Double Precision		
CELLS GYRATION RADIUS WVB	Double Precision		
CELLS NUC/CELL AREA WVB	Double Precision		
CELLS DIAMETER WVB	Double Precision		
CELLS PERIMETER WVB	Double Precision		
CELLS INTENSITY (CELL) WVB	Double Precision		
CELLS INTENSITY (CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVB	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVB	Double Precision		
CELLS INTENSITY CV (CELL) WVB	Double Precision		
CELLS INTENSITY CV (CYTO) WVB	Double Precision		
CELLS INTENSITY SPREADING WVB	Double Precision		
CELLS LIGHT FLUX WVB	Double Precision		
CELLS NUC/CYTO INTENSITY WVB	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVB	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVB	Double Precision		
CELLS CELL/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO/BCKG INTENSITY WVB	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVB	Double Precision		
CELLS INTENSITY SD (CELL) WVB	Double Precision		
CELLS INTENSITY SD (CYTO) WVB	Double Precision		
CELLS MAX INTENSITY WVB	Double Precision		
CELLS SKEWNESS WVB	Double Precision		
CELLS KURTOSIS WVB	Double Precision		
CELLS ENERGY WVB	Double Precision		
CELLS ENTROPY WVB	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVB	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVB	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVB	Double Precision		
CELLS INTENSITY (CELL) WVM	Double Precision		
CELLS INTENSITY (CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVM	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVM	Double Precision		
CELLS INTENSITY CV (CELL) WVM	Double Precision		
CELLS INTENSITY CV (CYTO) WVM	Double Precision		
CELLS INTENSITY SPREADING WVM	Double Precision		
CELLS LIGHT FLUX WVM	Double Precision		
CELLS NUC/CYTO INTENSITY WVM	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVM	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVM	Double Precision		
CELLS CELL/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO/BCKG INTENSITY WVM	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVM	Double Precision		
CELLS INTENSITY SD (CELL) WVM	Double Precision		
CELLS INTENSITY SD (CYTO) WVM	Double Precision		
CELLS MAX INTENSITY WVM	Double Precision		
CELLS SKEWNESS WVM	Double Precision		
CELLS KURTOSIS WVM	Double Precision		
CELLS ENERGY WVM	Double Precision		
CELLS ENTROPY WVM	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVM	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVM	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVM	Double Precision		
CELLS INTENSITY (CELL) WVT	Double Precision		
CELLS INTENSITY (CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY (CELL) WVT	Double Precision		
CELLS TOTAL INTENSITY (CYTO) WVT	Double Precision		
CELLS INTENSITY CV (CELL) WVT	Double Precision		
CELLS INTENSITY CV (CYTO) WVT	Double Precision		
CELLS INTENSITY SPREADING WVT	Double Precision		
CELLS LIGHT FLUX WVT	Double Precision		
CELLS NUC/CYTO INTENSITY WVT	Double Precision		
CELLS INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS INTENSITY-BCKG(CYTO) WVT	Double Precision		
CELLS TOTAL INTENSITY-BCKG (CELL) WVT	Double Precision		
CELLS CELL/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO/BCKG INTENSITY WVT	Double Precision		
CELLS CYTO INTENSITY/GLOBAL BCKG WVT	Double Precision		
CELLS INTENSITY SD (CELL) WVT	Double Precision		
CELLS INTENSITY SD (CYTO) WVT	Double Precision		
CELLS MAX INTENSITY WVT	Double Precision		
CELLS SKEWNESS WVT	Double Precision		
CELLS KURTOSIS WVT	Double Precision		
CELLS ENERGY WVT	Double Precision		
CELLS ENTROPY WVT	Double Precision		
CELLS GREY LEVEL NON UNIFORMITY WVT	Double Precision		
CELLS HIGH GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS LOW GREY LEVEL RUN EMPHASIS WVT	Double Precision		
CELLS RUN LENGTH NON UNIFORMITY WVT	Double Precision		
MITONETWORKS ORG PER CELL WVM	Double Precision		
MITONETWORKS AREA WVM	Double Precision		
MITONETWORKS TOTAL AREA WVM	Double Precision		
MITONETWORKS FORM FACTOR WVM	Double Precision		
MITONETWORKS ELONGATION WVM	Double Precision		
MITONETWORKS COMPACTNESS WVM	Double Precision		
MITONETWORKS CHORD RATIO WVM	Double Precision		
MITONETWORKS GYRATION RADIUS WVM	Double Precision		
MITONETWORKS INTENSITY WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY CV WVM	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVM	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVM	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVM	Double Precision		
MITONETWORKS INTENSITY SPREADING WVM	Double Precision		
MITONETWORKS INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVM	Double Precision		
MITONETWORKS DISTANCE TO NUC WVM	Double Precision		
MITONETWORKS SPACING WVM	Double Precision		
MITONETWORKS NEIGHBOR COUNT WVM	Double Precision		
MITONETWORKS INTENSITY WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY CV WVH	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVH	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVH	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVH	Double Precision		
MITONETWORKS INTENSITY SPREADING WVH	Double Precision		
MITONETWORKS INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVH	Double Precision		
MITONETWORKS INTENSITY WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY CV WVT	Double Precision		
MITONETWORKS ORGANELLE/CYTO INTENSITY WVT	Double Precision		
MITONETWORKS ORGANELLE/BCKG INTENSITY WVT	Double Precision		
MITONETWORKS INTENSITY/GLOBAL BCKG WVT	Double Precision		
MITONETWORKS INTENSITY SPREADING WVT	Double Precision		
MITONETWORKS INTENSITY-BCKG WVT	Double Precision		
MITONETWORKS TOTAL INTENSITY-BCKG WVT	Double Precision		
ACQUISITION TIME STAMP	Double Precision		
PLATE ID	text		
WELL X UM	Double Precision		
WELL Y UM	Double Precision		
PLATE X UM	Double Precision		
PLATE Y UM	Double Precision		
IMAGENAME	text		
FOCUS Z POS	Double Precision		
FOCUS Z AVG OF ADJACENT FIELDS	Double Precision		
RAFTID	text		
RAFTROW	text		
RAFTCOL	text		
UMFROMBORDEROFRAFT	Double Precision		
IMAGECHECK	text		
CELLSPERFIELD	Integer		
PLATE-WELL	text		
MLTRAININGLAYOUT	text		
MLCLASSLAYOUT	text		
MLREPLICATES	text		
MLCELLSUSE	text		
INPUTROWINDEX	Integer		
MLCLASSLAYOUT REPLICATES	text		
MLPREDICTIONPROB	Double Precision		
MLCLASSPREDICTED	text		
MLQUAD	Integer		
MLPLATEINDEX	Integer		
MLFIDUCIARYRAFT	Boolean		
MLTHRESHOLD	text		
MLIMAGECHECK	text		
MLMIX	text		
MLC0ALLOWEDREGION	Boolean		
MLC1CELLULAR	Boolean		
MLVALIDRAFTS	Boolean		
MLCROSSVALFOLD	Integer		
MLCELLSPERRAFT	Integer		
MLC2ALLOWEDPERRAFT	Boolean		
MITOORGINTEN	Double Precision 
PROBPR WBPP 03 16FEATURESB Text  NOT NULL
 
 */