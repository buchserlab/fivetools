﻿/*
using Loyc.Collections;
using Loyc.Geometry;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using WebApplication9;
using static WebApplication9.CompressedBSON;

namespace WebApplication9
{
    public class GenericSpreadSheet
    {
        public GenericSpreadSheet(String filePathWithFile, int ColumnNum, int recordNum, Dictionary<int, KeyValuePair<String, Object>> allRecords)
        {

            FileInfo Info = new FileInfo(filePathWithFile);
            GenericSpreadSheet.FileSizeInBytes = Info.Length;
            GenericSpreadSheet.FileExtension  = Info.Extension;
            GenericSpreadSheet.LastAccessed = Info.LastWriteTime.ToString();
            GenericSpreadSheet.FileName = Info.Name;
            GenericSpreadSheet.FilePath = Info.FullName;
            GenericSpreadSheet.CreationTime = Info.CreationTime.ToString();
            GenericSpreadSheet.ColumnNum = ColumnNum;
            GenericSpreadSheet.RecordNum =  recordNum;
            GenericSpreadSheet.RowNum = (recordNum-1)/(ColumnNum-1);
            GenericSpreadSheet.theRecords = allRecords;
            if (allRecords.Count == 0)
            {
                GenericSpreadSheetData.GenericSpreadsheetWriter(filePathWithFile);
            }
        }
        public static String? FilePath { get; set; }

        public static String? FileName { get; set; }

        public static String? FileExtension { get; set; }

        public static String? CreationTime { get; set; }

        public static Int64 FileSizeInBytes { get; set; }

        public static string? LastAccessed { get; set; }

        public static Int32 RowNum { get; set; }

        public static Int32 RecordNum { get; set; }

        public static Int32 ColumnNum { get; set; }

        public String? Delimiter { get; set; }

        public static Dictionary<int, KeyValuePair<String, Object>>? theRecords { get; set; }

        public class GenericSpreadSheetData
        {


               
            public static void GenericSpreadsheetWriter(String filePathWithFile)
            {
                List<String> Final = new List<string>();

                using (StreamReader reader = new StreamReader("C:\\Users\\jdmso\\OneDrive\\Documents\\CreateOutputFile.txt"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        String v = "ALTER TABLE IF EXISTS experimentschema.experimenttable  ADD COLUMN "  +  line  + " NOT  NULL";
                        string newstr = v.Replace(@"\", "/");
                        Final.Add(newstr);

                    }
                }

        



                Dictionary<String, int> Wavelengths = new Dictionary<String, int>();
                Wavelengths.Add("WVH", 1); Wavelengths.Add("WVT", 2); Wavelengths.Add("WVB", 3);
                Wavelengths.Add("WVM", 4); Wavelengths.Add("noWavelength", 5);

                Dictionary<String, int> Masks = new Dictionary<String, int>();
                Masks.Add("Nuclei", 12); 
                Masks.Add("Whole Cell", 14); 
                Masks.Add("MitoNetworks", 16); 
                Masks.Add("Neurites", 17); Masks.Add("Soma", 18); 
                Masks.Add("Membrane", 19);
                Masks.Add("Punctua", 20); Masks.Add("Cells", 13); 
                Masks.Add("Cyto", 15);

                Dictionary<String, int> Features = new Dictionary<String, int>();
                Features.Add("INTENSITY", 1809);
                Features.Add("TOTAL INTENSITY", 1810);
                Features.Add("INTENSITY CV", 1811);
                Features.Add("LIGHT FLUX", 1812);
                Features.Add("INTENSITY/GLOBAL BCKG", 1813);
                Features.Add("INTENSITY SD", 1814);
                Features.Add("MAX INTENSITY", 1815);
                Features.Add("SKEWNESS", 1816);
                Features.Add("KURTOSIS", 1817);
                Features.Add("ENERGY", 1818);
                Features.Add("ENTROPY", 1819);
                Features.Add("GREY LEVEL NON UNIFORMITY", 1820);
                Features.Add("HIGH GREY LEVEL RUN EMPHASIS", 1821);
                Features.Add("LOW GREY LEVEL RUN EMPHASIS", 1822);
                Features.Add("RUN LENGTH NON UNIFORMITY", 1823);
                Features.Add("INTENSITY (CELL)", 1824);
                Features.Add("INTENSITY (CYTO)", 1825);
                Features.Add("TOTAL INTENSITY (CELL)", 1826);
                Features.Add("TOTAL INTENSITY (CYTO)", 1827);
                Features.Add("INTENSITY CV (CELL)", 1828);
                Features.Add("INTENSITY CV (CYTO)", 1829);
                Features.Add("INTENSITY SPREADING", 1830);
                Features.Add("NUC/CYTO INTENSITY", 1831);
                Features.Add("INTENSITY-BCKG (CELL)", 1832);
                Features.Add("INTENSITY-BCKG(CYTO)", 1833);
                Features.Add("TOTAL INTENSITY-BCKG (CELL)", 1834);
                Features.Add("CELL/BCKG INTENSITY", 1835);
                Features.Add("CYTO/BCKG INTENSITY", 1836);
                Features.Add("CYTO INTENSITY/GLOBAL BCKG", 1837);
                Features.Add("INTENSITY SD (CELL)", 1838);
                Features.Add("INTENSITY SD (CYTO)", 1839);
                Features.Add("ORG PER CELL", 1840);
                Features.Add("AREA", 1841);
                Features.Add("TOTAL AREA", 1842);
                Features.Add("FORM FACTOR", 1843);
                Features.Add("ELONGATION", 1844);
                Features.Add("COMPACTNESS", 1845);
                Features.Add("CHORD RATIO", 1846);
                Features.Add("GYRATION RADIUS", 1847);
                Features.Add("ORGANELLE/CYTO INTENSITY", 1848);
                Features.Add("ORGANELLE/BCKG INTENSITY", 1849);
                Features.Add("INTENSITY-BCKG", 1850);
                Features.Add("TOTAL INTENSITY-BCKG", 1851);
                Features.Add("DISTANCE TO NUC", 1852);
                Features.Add("SPACING", 1853);
                Features.Add("NEIGHBOR COUNT", 1854);
                Features.Add("MAX LEFT BORDER", 1855);
                Features.Add("MAX TOP BORDER", 1856);
                Features.Add("MAX WIDTH", 1857);
                Features.Add("MAX HEIGHT", 1858);
                Features.Add("NUC/CELL AREA", 1859);
                Features.Add("DIAMETER", 1860);
                Features.Add("PERIMETER", 1861);
                Features.Add("DISPLACEMENT", 1862);
                Features.Add("CG X", 1863);
                Features.Add("CG Y", 1864);
                Features.Add("MAJOR AXIS", 1865);
                Features.Add("MINOR AXIS", 1866);
                Features.Add("MAJOR AXIS ANGLE", 1867);
                Features.Add("NEIGHBOR COUNT (SOI)", 1868);
                Features.Add("NEIGHBOR COUNT (SOI)", 1868);
                Features.Add("NEIGHBOR COUNT (SOI)", 1868);



             

            }

        }
    }
}




/*
    TextReader delimiterreader = new StreamReader(filePathWithFile);
    char seperatorChar = detectdelimiter.DetectDelimiter(delimiterreader, 2);
    String fileName = filePathWithFile;

    Dictionary<String, String> TypeConverter = new Dictionary<string, string>();
    BMultiMap<String, Object> map = new();

    int columnCount = 0;
    int rowCount = 0;
    int totalCount = 0;
    List<String> AllLines = new List<string>();
    using (var reader = new StreamReader(filePathWithFile))
    {
        string? readtoeend = reader.ReadToEnd();
        AllLines = readtoeend.Split(seperatorChar).ToList();
    }

    int count = 0;
    bool InFirst = true;
    String first = "";
    List<string> recordFirst = new List<String>();
    string others = "";
    int maxCount = 0;
    List<KeyValuePair<int, Object>> tupleList = new List<KeyValuePair<int, Object>>();
    List<String> theHeaders = new List<string>();
    foreach (string value in AllLines)
    {
        others = value;
        if (others.Contains("/r/n") || others.Contains("/n") || others.Contains(Environment.NewLine))
        {

            others = others.Replace(@"/r/n", "");
            others = others.Replace(@"/n", "");
            others = others.Replace(Environment.NewLine, "");
            Object o = TypeHelper.ParseItem(others);
            rowCount++;
            KeyValuePair<int, Object> d = new KeyValuePair<int, Object>(rowCount, o);
            tupleList.Prepend(d);
            InFirst = false;
        }

        else
        {
            if (InFirst == false)
            {
                Object o = TypeHelper.ParseItem(others);
                KeyValuePair<int, Object> d = new KeyValuePair<int, Object>(rowCount, o);
                tupleList.Add(d);
            }
            else
            {
                theHeaders.Add(others);
            }
        }




    }
}
}
}
}

List<String> Final = new List<string>();
using (TextReader filereader = new StreamReader("C:\\Users\\jdmso\\OneDrive\\Documents\\CreateOutputFile.txt"))
{
alterTableStrings.OrderBy(t => t.Count()).ThenBy(f => f.ToString()).ToList();
foreach (String da in alterTableStrings)
{


String? Line = filereader.ReadLine(); ;
String alterTable = "ALTER TABLE IF EXISTS experimentschema.experimenttable  ADD  " +  Line + "  NOT NULL";

}
}

using (TextWriter filewriter = new StreamWriter("C:\\Users\\jdmso\\OneDrive\\Documents\\CreateOutputFile.txt"))
{
foreach (String da in alterTableStrings)
{


filewriter.WriteLine(alterTableStrings.IndexesOf(da).Count() +  "   Item:   [" + da + "]");


}
return alterTableStrings;
}

/*


for (int j = 0; j < rowCount; j++)
{
List<KeyValuePair<int, Object>> fae = tupleList.Take(theHeaders.Count).ToList();

try
{
data.Add(j.ToString(), fae);
}
catch (Exception ex)
{
ex.ToString();
}
}


//                    


//String has header in it
//KeyValuePair
//Object has Value
//we take care of duplicates server side
//we add the filename to a seperate column






int a = 0;
//List<String> headers = new List<String>();
//List<String> records = new List<String>();


/*
using (TextReader filereader = File.OpenText(fileName))
{
//headers = filereader.ReadLine()?.Split(seperatorChar)?.ToList() ?? new List<string>();
//records = filereader.ReadLine()?.Split(seperatorChar)?.ToList() ?? new List<string>();
}

if (headers.Count == 0 || records.Count == 0)
{
return;
}







int indexCount = -1;
bool inHeaders = true;
Dictionary<int, KeyValuePair<String, Object>> allRecords = new Dictionary<int, KeyValuePair<String, Object>>();
for (int j = 0; j< records.Count(); j++)
{ 
indexCount = indexCount + 1;
if (records[j].Contains("\r\n") || records[j].Contains("\n") || inHeaders == true)
{
inHeaders = false;
indexCount = 0;
}
if (String.IsNullOrWhiteSpace(records[j]))
{
Object o = "Null";
allRecords.Add(j, new KeyValuePair<string, object>(headers[indexCount], "Null"));
continue;
}
int tem;
if (int.TryParse(records[j].ToString(), out tem) && !records[j].ToString().Contains("."))
{
object o = tem;
allRecords.Add(j, new KeyValuePair<string, object>(headers[indexCount], o));
continue;
}
float temp;
if (float.TryParse(records[j].ToString(), out temp)&& records[j].ToString().Contains("."))
{
object o = temp;
allRecords.Add(j, new KeyValuePair<String, Object>(headers[indexCount], o));
continue;
}
bool theBool;
if (bool.TryParse(records[j].ToString(), out theBool))
{
object o = temp;
allRecords.Add(j, new KeyValuePair<String, Object>(headers[indexCount], o));
continue;
}
if (records[j].ToString() != null)
{
string rec = records[j].ToString();
object o = rec;
allRecords.Add(j, new KeyValuePair<String, Object>(headers[indexCount], o));
continue;
}
}


String files = Path.GetFileName(fileName);

try { 
String ConnnectionString = "Server=localhost;Port=5432;Database=postgres;UserId=postgres;Password=N3onblue;TrustServerCertificate=true;";
NpgsqlConnection conn = new Npgsql.NpgsqlConnection(ConnnectionString);
conn.Open(); ; ;
NpgsqlCommand dbcmd = conn.CreateCommand();


for(int j = 0; j < rowCount; j++)
{

var ending = JsonConvert.SerializeObject(data[j], Formatting.None);
ending.Replace("Key", "k");
ending.Replace("Value", "v");



var jsonParam = ending;
dbcmd.Parameters.AddWithValue("@jsonparam", NpgsqlDbType.Jsonb, ending);
dbcmd.CommandText = @"INSERT INTO ""DataLevel"".""SpreadSheetData""  (""ColumnValues"") VALUES (@jsonparam)";
dbcmd.ExecuteNonQuery();
}
}

catch (NpgsqlException ex)
{
Console.Write(ex);
if (ex.Data == null)
{
throw;
}
else
{
System.Diagnostics.Trace.WriteLine(ex.InnerException?.Message);
System.Diagnostics.Trace.WriteLine(ex?.Data);
System.Diagnostics.Trace.WriteLine(ex?.Message);
System.Diagnostics.Trace.WriteLine(ex?.BatchCommand);
System.Diagnostics.Trace.WriteLine(ex?.ErrorCode);
System.Diagnostics.Trace.WriteLine(ex?.SqlState);
System.Diagnostics.Trace.WriteLine(ex?.SqlState);
System.Diagnostics.Trace.WriteLine(ex?.SqlState);
System.Diagnostics.Trace.WriteLine(ex?.StackTrace);
CancellationToken token = new CancellationToken();
}
}
}
}
}
}

*/