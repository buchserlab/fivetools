﻿using static WebApplication9.MeasurementData;

namespace WebApplication9
{
    public class UniqueFileData
    {
        public class FileRelatedData
        {
            public int Id { get; set; }
            public string FileIdName { get; set; } = null!;
            public TimeOnly? LastInsert { get; set; }
            public virtual ICollection<MeasurementTable> MeasurementTables { get; set; }
            public NonMeasurementData NonMeasurementData { get; set; }
        }
        public partial class Unknowndatatable
        {
            public int Id { get; set; }
            public String FileIdName { get; set; }
            public string Columnnname { get; set; } = null!;
            public string Columnvalues { get; set; } = null!;
            public string Rowdatatype { get; set; } = null!;
            public bool Nulloremptypresent { get; set; }
            public DateTime? LastInsert { get; set; }
        }
    }

}

