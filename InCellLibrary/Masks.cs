﻿using FIVE.InCellLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace FIVE
{
    namespace Masks
    {
        //4/2021 Bay Johnson original code set
        //if rotate transform is being used, make sure major axis is making the ellipse longer in the y direction before rotation
        //Josh 12/8/2024 Updated this code to be more readable as well as more efficient.
        public class MaskListEntry
        {
            public MaskList Parent { get; set; }
            public string[] Cols { get; set; }
            public string PlateID { get; set; }
            public string FileName { get; set; }
            public string WellLabel { get; set; }
            public int FOV { get; set; }
            public string WellField { get => XDCE_Image.WellFieldKey(WellLabel, FOV); }
            public string ObjectID { get; set; }
            public float Left_pix { get; set; }
            public float Top_pix { get; set; }
            public float Width_pix { get; set; }
            public float Height_pix { get; set; }
            public PointF Pos_Pix { get => new PointF(Left_pix, Top_pix); }
            public SizeF Size_Pix { get => new SizeF(Width_pix, Height_pix); }
            
            [XmlIgnore]
            public PointF Center_Pix { 
                get => new PointF(Left_pix + (Width_pix / 2), Top_pix + (Height_pix /2)); 
            }

            

            [XmlIgnore]
            public Color DisplayOnlyColor { get; set; } 

            public float axisAngle_deg { get; set; }
            //public bool Grayscale { get; set; }
            public float Intensity { get; set; }
            public string Intensity_Method { get; set; }
            /// <summary>
            /// These are half-width and half-height
            /// </summary>
            public int majorAxisLength_Pix { get => (int)(majorAxisLength_um / Parent.PixelWidth_um); }
            public int minorAxisLength_Pix { get => (int)(minorAxisLength_um / Parent.PixelWidth_um); }
            public float majorAxisLength_um { get; set; }
            public float minorAxisLength_um { get; set; }

            

            public MaskListEntry()
            {
                FOV = 0;
                WellLabel = "N0";
            }
        }

        //how these get formated into the table can also be changed later when input of funtion is decided
        public class MaskList : IEnumerable<MaskListEntry>
        {
            private List<MaskListEntry> _entries;
            public Dictionary<string, List<MaskListEntry>> WellFieldDict;
            public double PixelWidth_um { get; set; }
            public MaskList()
            {
                Init();
            }

            private void Init()
            {
                _entries = new List<MaskListEntry>();
                WellFieldDict = new Dictionary<string, List<MaskListEntry>>();
                //_FeatureNames = null;
            }

            public static char delimeter = '\t';

            private static readonly Dictionary<MaskFeatureEnum, Func<int, string>> FeatureNameLookup = new()
            {
                { MaskFeatureEnum.PlateID, _ => "PLATEID" },
                { MaskFeatureEnum.WellLabel, _ => "WELL LABEL" },
                { MaskFeatureEnum.FOV, _ => "FOV" },
                { MaskFeatureEnum.ObjectID, _ => "OBJECT ID" },
                { MaskFeatureEnum.FileName, _ => "IMAGENAME" },
                { MaskFeatureEnum.X_pix, wv => $"NUCLEI MAX LEFT BORDER WV{wv}" },
                { MaskFeatureEnum.Y_pix, wv => $"NUCLEI MAX TOP BORDER WV{wv}" },
                { MaskFeatureEnum.Width_pix, wv => $"NUCLEI MAX WIDTH WV{wv}" },
                { MaskFeatureEnum.Height_pix, wv => $"NUCLEI MAX HEIGHT WV{wv}" },
                { MaskFeatureEnum.AxisAngle_Deg, wv => $"NUCLEI MAJOR AXIS ANGLE WV{wv}" },
                { MaskFeatureEnum.MinorAxis_um, wv => $"NUCLEI MINOR AXIS WV{wv}" },
                { MaskFeatureEnum.MajorAxis_um, wv => $"NUCLEI MAJOR AXIS WV{wv}" },
                { MaskFeatureEnum.Intensity, _ => "INTENSITY" },
                { MaskFeatureEnum.Intensity_Method, _ => "INTENSITY_METHOD" }
            };


            public static string GetFeatureName(MaskFeatureEnum feature, int wv = 1)
            {
                if (!FeatureNameLookup.TryGetValue(feature, out var nameGenerator))
                    throw new ArgumentOutOfRangeException(nameof(feature), $"Unknown feature: {feature}");

                return nameGenerator(wv);
            }

            //---------------------------------------------------------------------------------

            public Dictionary<MaskFeatureEnum, int> FeatureIndex { get; set; }

            public static bool CheckStringAgainst_IntensityType(string toCheck, MaskIntensityType intensityType)
            {
                // Normalize the input for comparison
                if (toCheck == null || toCheck == "") 
                    return false;
                toCheck = toCheck.ToUpper().Trim();

                // Switch on the actual intensityType value
                return intensityType switch
                {
                    MaskIntensityType.Binary => toCheck == "BINARY",
                    MaskIntensityType.Dither => toCheck == "DITHER",
                    MaskIntensityType.Skeleton => toCheck == "SKELETON",
                    MaskIntensityType.Grayscale => toCheck == "GRAYSCALE",
                    _ => false,
                };
            }

            //---------------------------------------------------------------------------------

            public MaskList(string FullPath, string PlateID = "", double pixel_width_um = 1)
            {
                Init();
                PixelWidth_um = pixel_width_um; int i;
                if (FullPath == "") { INCELL_Folder.ErrorLog += "No file given"; return; }
                FileInfo FI = new FileInfo(FullPath); if (!FI.Exists) { INCELL_Folder.ErrorLog += "MaskList New Cant Find File"; return; }
                try
                {
                    string[] lines = File.ReadAllLines(FullPath);
                    INCELL_Folder.ErrorLog += lines == null ? "Empty Mask File" : "Lines.len " + lines.Length;
                    string[] Headers = lines[0].ToUpper().Split(delimeter);
                    SetupHeader_Lookup(Headers);
                    if (FeatureIndex.Count < 12) return;

                    string[] cols;
                    for (i = 1; i < lines.Length; i++)
                    {
                        cols = lines[i].Split(delimeter);
                        Add(cols, this, PlateID);
                    }
                }
                catch
                {
                    INCELL_Folder.ErrorLog += "Issue parsing mask.";
                }
                INCELL_Folder.ErrorLog += "\r\n Entries count " + _entries.Count + "\r\n";
            }

            private void SetupHeader_Lookup(string[] headers)
            {
                // Normalize and build HeaderDict
                Dictionary<string, int> HeaderDict = new Dictionary<string, int>(headers.Length, StringComparer.OrdinalIgnoreCase);
                for (int i = 0; i < headers.Length; i++)
                {
                    string normalizedHeader = headers[i].Replace("WV1", "WVH").Trim();
                    HeaderDict[normalizedHeader] = i;
                }

                // Preallocate FeatureIndex
                FeatureIndex = new Dictionary<MaskFeatureEnum, int>();

                // Track missing features
                List<string> Missing = new List<string>();

                // Iterate through MaskFeatureEnum values
                IEnumerable<MaskFeatureEnum> features = Enum.GetValues(typeof(MaskFeatureEnum)).Cast<MaskFeatureEnum>();
                foreach (MaskFeatureEnum feature in features)
                {
                    string key = feature.ToString(); // Direct enum-to-string conversion
                    if (HeaderDict.TryGetValue(key, out int index))
                    {
                        FeatureIndex[feature] = index;
                    }
                    else
                    {
                        Missing.Add(key); // Collect missing features
                    }
                }

                // Log missing features
                if (Missing.Count > 0)
                {
                    INCELL_Folder.ErrorLog += $"\r\nFeatures missing: {string.Join(", ", Missing)}\r\n";
                }
            }

            public void Add(MaskListEntry Entry)
            {
                //Add to the main list (we may not even need this one)
                _entries.Add(Entry);

                //Add to the Dictionary
                if (!WellFieldDict.ContainsKey(Entry.WellField)) WellFieldDict.Add(Entry.WellField, new List<MaskListEntry>());
                WellFieldDict[Entry.WellField].Add(Entry);
            }

            public void Add(string[] cols, MaskList Parent, string PlateID = "")
            {
                MaskListEntry Mask = new MaskListEntry();
                Mask.Parent = Parent;
                Mask.Cols = cols;

                Mask.Left_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.X_pix]]);
                Mask.Top_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Y_pix]]);
                Mask.Width_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Width_pix]]);
                Mask.Height_pix = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Height_pix]]);
                Mask.majorAxisLength_um = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.MajorAxis_um]]);
                Mask.minorAxisLength_um = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.MinorAxis_um]]);
                Mask.axisAngle_deg = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.AxisAngle_Deg]]);
                Mask.FOV = int.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.FOV]]) - 1;
                Mask.WellLabel = cols[Parent.FeatureIndex[MaskFeatureEnum.WellLabel]];
                Mask.ObjectID = cols[Parent.FeatureIndex[MaskFeatureEnum.ObjectID]];
                Mask.FileName = cols[Parent.FeatureIndex[MaskFeatureEnum.FileName]];
                Mask.PlateID = cols[Parent.FeatureIndex[MaskFeatureEnum.PlateID]];
                if (Parent.FeatureIndex.ContainsKey(MaskFeatureEnum.Intensity_Method))
                {
                    //Mask.Grayscale = bool.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Grayscale]]);
                    Mask.Intensity = float.Parse(cols[Parent.FeatureIndex[MaskFeatureEnum.Intensity]]);
                    Mask.Intensity_Method = (cols[Parent.FeatureIndex[MaskFeatureEnum.Intensity_Method]]).Trim().ToUpper();
                }

                //TODO TURN THIS BACK ON!
                //if (PlateID == "" || PlateID.ToUpper() == Mask.PlateID.ToUpper())

                Add(Mask);
                //else INCELL_Folder.ErrorLog += "\r\nNot Added because PlateID:" + PlateID + "\t" + Mask.PlateID;
            }



            public MaskListEntry this[int index]
            {
                get { return _entries[index]; }
            }

            public int Count { get => _entries.Count; }

            public IEnumerator<MaskListEntry> GetEnumerator()
            {
                return _entries.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _entries.GetEnumerator();
            }

            internal static List<XDCE_Image> XDCEImages_from_MaskList(XDCE_ImageGroup Plate, string FullPathMaskList)
            {
                MaskList ML = new MaskList(FullPathMaskList, Plate.PlateID, Plate.ParentFolder.XDCE.PixelWidth_in_um);
                INCELL_Folder.ErrorLog += "XDCEImages_from_MaskList " + ML.Count;
                List<XDCE_Image> XList = new List<XDCE_Image>();
                foreach (string Key in ML.WellFieldDict.Keys)
                {
                    XDCE_Image XI = Plate.GetImage_FromWellFOV(Key);
                    if (XI != null) 
                        XList.Add(XI);
                    else
                    {
                        INCELL_Folder.ErrorLog += "XDCEImages_from_MaskList Key UnMatched" + Key;
                    }
                }
                return XList;
            }


        }

        public enum MaskIntensityType
        {
            Binary = 0,
            Dither = 10,
            Skeleton = 15,
            Grayscale = 100
        }

        public enum MaskFeatureEnum
        {
            FileName = 0,
            PlateID = 1,
            WellLabel = 2,
            FOV = 3,
            X_pix = 4,
            Y_pix = 5,
            X_um = 6,
            Y_um = 7,
            Width_pix = 20,
            Height_pix = 21,
            AxisAngle_Deg = 8,
            MajorAxis_pix = 9,
            MinorAxis_pix = 10,
            MajorAxis_um = 11,
            MinorAxis_um = 12,
            ObjectID = 13,
            //make sure to double check this intensity
            Grayscale = 22,
            Intensity = 23,
            Intensity_Method = 24
        }
    }

    public class sCoordinateSet : IEnumerable<sCoordinate>
    {
        public List<sCoordinate> List;

        private Dictionary<Point, sCoordinate> _Dict;

        public float minX, maxX, minY, maxY;
        public int minC, maxC, minR, maxR;

        public Dictionary<Point, sCoordinate> Dict
        {
            get
            {
                if (_Dict == null)
                {
                    // Determine the minimum and maximum x and y values
                    minX = (int)this.Min(c => c.x);
                    maxX = (int)this.Max(c => c.x);
                    minY = (int)this.Min(c => c.y);
                    maxY = (int)this.Max(c => c.y);

                    // Use SortedSet to collect unique and sorted values
                    SortedSet<float> sortedX = new SortedSet<float>();
                    SortedSet<float> sortedY = new SortedSet<float>();

                    foreach (var cor in this)
                    {
                        sortedX.Add(cor.x);
                        sortedY.Add(cor.y);
                    }

                    // Convert SortedSet to arrays for binary search
                    float[] uniqueX = new float[sortedX.Count]; 
                    sortedX.CopyTo(uniqueX);
                    float[] uniqueY = new float[sortedY.Count]; 
                    sortedY.CopyTo(uniqueY);

                    minC = 0; maxC = uniqueX.Length;
                    minR = 0; maxR = uniqueY.Length;

                    // Preallocate the dictionary with the exact capacity
                    _Dict = new Dictionary<Point, sCoordinate>(this.List.Count);

                    // Populate the dictionary and determine rows/columns in one step
                    foreach (sCoordinate cor in this)
                    {
                        // Use Array.BinarySearch to find the row and column indices
                        cor.row = Array.BinarySearch(uniqueY, cor.y);
                        cor.col = Array.BinarySearch(uniqueX, cor.x);

                        // Add to the dictionary
                        _Dict[new Point(cor.row, cor.col)] = cor;
                    }
                }
                return _Dict;
            }
        }

        public sCoordinateSet(int size = 0)
        {
            List = new List<sCoordinate>(size);
        }

        public void Add(sCoordinate Cor)
        {
            _Dict = null; //Invalidates the dictionary
            List.Add(Cor);
        }

        public static sCoordinateSet CreateGrid(int Size)
        {
            sCoordinateSet Cors = new sCoordinateSet(Size);
            int s2 = (Size / 2); int Counter = 0;
            for (int r = 0; r < Size; r++)
            {
                int y = r - s2;
                bool s = r % 2 == 0;
                sCoordinate[] row = new sCoordinate[Size];
                for (int c = 0; c < Size; c++)
                {
                    row[c] = new sCoordinate()
                    {
                        FOV_20x = Counter++,
                        FOV_10x = -1,
                        x = s ? 1 + c - s2 : s2 - c,
                        y = y
                    };
                }
                Cors.List.AddRange(row);
            }
            return Cors;
        }

        private void AssignBinValues()
        {
            Dictionary<Point, sCoordinate> coordinates = Dict;

            // Iterate through the coordinates and assign values to the "b" column
            int b = 0;
            for (int r = minR; r <= maxR; r += 2)
            {
                int v = minC - 2;
                int w = maxC + 1;
                bool bflip = ((r - minR) *0.5) % 2 == 0; // Reverse direction every second row-pair
                int cStart = bflip ? v : w;
                int cEnd = bflip ? w : v;
                int cStep = bflip ? 2 : -2;

                for (int c = cStart; c != cEnd + cStep; c += cStep)
                {
                    // Define the four points (2x2 grid) explicitly
                    var points = new[]
                    {
                        new Point(r, c),
                        new Point(r, c + 1),
                        new Point(r + 1, c),
                        new Point(r + 1, c + 1)
                    };

                    int foundCount = 0;
                    foreach (Point point in points)
                    {
                        if (coordinates.TryGetValue(point, out var coordinate))
                        {
                            coordinate.FOV_10x = b; // Assign the current bin index
                            foundCount++;
                        }
                    }

                    if (foundCount > 0) 
                        b++; // Increment bin index if any points were found
                }
            }
        }


        public static void AssignBinValues2(List<sCoordinate> coordinates)
        {
            // Determine the number of rows and columns in the grid
            int rows = (int)Math.Sqrt(coordinates.Count);
            int cols = rows;

            // Iterate through the coordinates and assign values to the "b" column
            for (int i = 0; i < coordinates.Count; i++)
            {
                // Determine the row and column of the current coordinate
                int row = i / cols;
                int col = i % cols;

                // Assign a value to the "b" column based on the row and column
                if (row % 2 == 0)
                {
                    // For even rows, the "b" value increases as the column increases
                    coordinates[i].FOV_10x = row * (cols / 2) + (col / 2);
                }
                else
                {
                    // For odd rows, the "b" value decreases as the column increases
                    coordinates[i].FOV_10x = (row + 1) * (cols / 2) - (col / 2) - 1;
                }
            }
        }

        public static sCoordinateSet Load(string XMLPath)
        {
            sCoordinateSet sSet = new sCoordinateSet();
            string tAll = File.ReadAllText(XMLPath).ToUpper().Trim().Replace("\r\n", "").Replace(" ", "");
            string[] delims = new string[2] { "<POINT", "/>" };
            string[] spl = tAll.Split(delims, StringSplitOptions.RemoveEmptyEntries);
            sCoordinate sC; string[] points; int Counter = 0;

            for (int i = 0; i < spl.Length; i++)
            {
                if (!spl[i].StartsWith("X")) 
                    continue;
                points = spl[i].Split('\"');
                sC = new sCoordinate() { FOV_20x = Counter++, x = float.Parse(points[1]), y = float.Parse(points[3]) };
                sSet.Add(sC);
            }

            //Now that it is inished, we can assign the bins
            sSet.AssignBinValues();
            return sSet;
        }


   
        public string ToText()
        {
            const char delimiter = '\t';
            const string newLine = "\r\n";

            var stringBuilder = new StringBuilder();

            // Append header
            stringBuilder.Append('x').Append(delimiter)
                         .Append('y').Append(delimiter)
                         .Append("row").Append(delimiter)
                         .Append("col").Append(delimiter)
                         .Append("FOV 20x").Append(delimiter)
                         .Append("FOV 10x").Append(newLine);

            // Append each item's details
            foreach (var c in this)
            {
                stringBuilder.Append(c.x).Append(delimiter)
                             .Append(c.y).Append(delimiter)
                             .Append(c.row).Append(delimiter)
                             .Append(c.col).Append(delimiter)
                             .Append(c.FOV_20x).Append(delimiter)
                             .Append(c.FOV_10x).Append(newLine);
            }

            return stringBuilder.ToString();
        }
        public IEnumerator<sCoordinate> GetEnumerator()
        {
            return ((IEnumerable<sCoordinate>)List).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)List).GetEnumerator();
        }
    }

    public class sCoordinate
    {
        public int row;
        public int col;
        public float x;
        public float y;
        public int FOV_20x;
        public int FOV_10x;

        public override string ToString()
        {
            return "(" + x + "," + y + "," + FOV_10x + ")";
        }


    }

}
