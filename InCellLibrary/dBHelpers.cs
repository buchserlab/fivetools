﻿using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Linq;
using System.ComponentModel;
using System;

namespace FIVE
{
    namespace InCellLibrary
    {

        public class FSDataTable
        {
            public static string TableName = "d10data";

            public static List<Tuple<string, string>> ListOfNames_Types
            {
                get
                {
                    return new List<Tuple<string, string>>() { new Tuple<string, string>("idCell", "int"), new Tuple<string, string>("idMeas", "int"), new Tuple<string, string>("val", "double") };
                }
            }

        }

        public class Header_Info
        {
            public static string TableName = "m10measures";
            public Header_Info(string columnOrig, short Order)
            {
                ColumnName_Original = columnOrig; ord = Order; Id = -1; //Not connected with dB yet
                HeaderName = columnOrig.ToUpper().Trim();

                Metadata_TableName = Feature_Mask = Feature_Measure = Feature_Wavelength = "";
                IsMeasurement = false; IsUndesired = false; IsCellMetadata = false;

                if (HeaderName.Length > 6)
                {
                    if (HeaderName.Substring(HeaderName.ToUpper().Length - 4, 3) == " WV") IsMeasurement = true;
                }
                if (IsMeasurement)
                {
                    Type_inDB = "double precision";
                    Type_inClass = "double";
                    //Split it up Appropriately

                    Feature_Wavelength = HeaderName.Substring(HeaderName.Length - 1, 1);
                    string t = HeaderName.Substring(0, HeaderName.Length - 4);
                    if (t.Contains(" "))
                    {
                        Feature_Mask = t.Substring(0, t.IndexOf(" "));
                        Feature_Measure = t.Substring(t.IndexOf(" ") + 1);
                    }
                    else
                    {
                        Feature_Mask = "UNK";
                        Feature_Measure = t;
                    }
                }
                else
                {
                    //Now we have to figure out if it is metadata and which table it belongs in

                    Metadata_TableName = FindMetadataEntry(HeaderName);
                    IsUndesired = (Metadata_TableName == "" || Metadata_TableName.StartsWith("_"));
                    IsCellMetadata = !IsUndesired;
                }
            }

            public override string ToString()
            {
                return ColumnName_Original;
            }

            public string ColumnName_Original { get; set; }
            public string HeaderName { get; set; }
            public bool IsNullable { get; set; }
            public bool IsMeasurement { get; set; }
            public bool IsCellMetadata { get; set; }
            public bool IsUndesired { get; set; }
            public object Type_inDB { get; set; }
            public string Type_inClass { get; set; }
            public int ord { get; set; }
            public int Id { get; set; }

            public string Metadata_TableName { get; set; }
            public string Feature_Mask { get; set; }
            public string Feature_Measure { get; set; }
            public string Feature_Wavelength { get; set; }
            public static List<Tuple<string, string>> ListOfNames_Types
            {
                get
                {
                    return new List<Tuple<string, string>>() { new Tuple<string, string>("id", "int"), new Tuple<string, string>("Name", "string") };
                }
            }

            private static Dictionary<string, MemberTable_Type> _MetadataColumnLookup;

            public static List<Tuple<string, string>> Subset(string SimpleTableName)
            {
                FindMetadataEntry(""); //Forces this to get made
                var Res = new List<Tuple<string, string>>();
                foreach (var item in _MetadataColumnLookup)
                {
                    if (item.Value.Table == SimpleTableName)
                        Res.Add(new Tuple<string, string>(item.Key, item.Value.TypeName));
                }
                return Res;
            }

            public static string FindMetadataEntry(string ColumnName)
            {
                if (_MetadataColumnLookup == null)
                {
                    var MCL = _MetadataColumnLookup = new Dictionary<string, MemberTable_Type>();

                    MCL.Add("FFNS", new MemberTable_Type(Simple_File.TableName, "string"));
                    MCL.Add("SM_FILENAME", new MemberTable_Type(Simple_File.TableName, "string"));
                    MCL.Add("SM_FILEPATH", new MemberTable_Type(Simple_File.TableName, "string"));
                    MCL.Add("SM_FILETIMESTAMP", new MemberTable_Type(Simple_File.TableName, "string"));
                    MCL.Add("SM_INSERTTIMESTAMP", new MemberTable_Type(Simple_File.TableName, "string"));

                    MCL.Add("PLATEID", new MemberTable_Type(Simple_Scan.TableName, "string"));
                    MCL.Add("PLATE ID", new MemberTable_Type(Simple_Scan.TableName, "string"));
                    MCL.Add("AQP", new MemberTable_Type(Simple_Scan.TableName, "string"));
                    MCL.Add("MLPLATEINDEX", new MemberTable_Type(Simple_Scan.TableName, "int"));
                    MCL.Add("FIVNUM", new MemberTable_Type(Simple_Scan.TableName, "string"));

                    MCL.Add("WELL LABEL", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("ROW", new MemberTable_Type(Simple_Well.TableName, "char"));
                    MCL.Add("COLUMN", new MemberTable_Type(Simple_Well.TableName, "int"));
                    MCL.Add("MLTRAININGLAYOUT", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("MLCLASSLAYOUT", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("MLMIX", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("PLATE-WELL", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("MLREPLICATES", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("REPLICATES", new MemberTable_Type(Simple_Well.TableName, "string"));
                    MCL.Add("MLCROSSVALFOLD", new MemberTable_Type(Simple_Well.TableName, "string"));

                    MCL.Add("FOV", new MemberTable_Type(Simple_FOV.TableName, "int"));
                    MCL.Add("IMAGENAME", new MemberTable_Type(Simple_FOV.TableName, "string"));
                    MCL.Add("FOCUS Z POS", new MemberTable_Type(Simple_FOV.TableName, "double"));
                    MCL.Add("FOCUS Z AVG OF ADJACENT FIELDS", new MemberTable_Type(Simple_FOV.TableName, "double"));
                    MCL.Add("ACQUISITION TIME STAMP", new MemberTable_Type(Simple_FOV.TableName, "double"));
                    MCL.Add("CELLSPERFIELD", new MemberTable_Type(Simple_FOV.TableName, "double"));

                    MCL.Add("INCARTA_PROTOCOLNAME", new MemberTable_Type(Simple_ZTR.TableName, "string"));
                    MCL.Add("RESULTSID", new MemberTable_Type(Simple_ZTR.TableName, "string"));
                    MCL.Add("Z", new MemberTable_Type(Simple_ZTR.TableName, "double"));
                    MCL.Add("T", new MemberTable_Type(Simple_ZTR.TableName, "double"));

                    MCL.Add("OBJECT ID", new MemberTable_Type(Simple_Cell.TableName, "int"));
                    MCL.Add("WELL X UM", new MemberTable_Type(Simple_Cell.TableName, "double"));
                    MCL.Add("WELL Y UM", new MemberTable_Type(Simple_Cell.TableName, "double"));
                    MCL.Add("PLATE X UM", new MemberTable_Type(Simple_Cell.TableName, "double"));
                    MCL.Add("PLATE Y UM", new MemberTable_Type(Simple_Cell.TableName, "double"));
                    MCL.Add("MLCELLSUSE", new MemberTable_Type(Simple_Cell.TableName, "string"));
                    MCL.Add("INPUTROWINDEX", new MemberTable_Type(Simple_Cell.TableName, "int"));
                    MCL.Add("MLTHRESHOLD", new MemberTable_Type(Simple_Cell.TableName, "string"));
                    MCL.Add("MLC1CELLULAR", new MemberTable_Type(Simple_Cell.TableName, "bool"));
                    MCL.Add("MITOORGINTEN", new MemberTable_Type(Simple_Cell.TableName, "double"));
                    MCL.Add("NUCLEI ANTIHEALTH", new MemberTable_Type(Simple_Cell.TableName, "double"));

                    MCL.Add("RAFTID", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("RAFTROW", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("RAFTCOL", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("UMFROMBORDEROFRAFT", new MemberTable_Type(Simple_Raft.TableName, "double"));
                    MCL.Add("IMAGECHECK", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("MLQUAD", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("MLFIDUCIARYRAFT", new MemberTable_Type(Simple_Raft.TableName, "bool"));
                    MCL.Add("MLIMAGECHECK", new MemberTable_Type(Simple_Raft.TableName, "string"));
                    MCL.Add("MLC0ALLOWEDREGION", new MemberTable_Type(Simple_Raft.TableName, "bool"));
                    MCL.Add("MLVALIDRAFTS", new MemberTable_Type(Simple_Raft.TableName, "bool"));
                    MCL.Add("MLCELLSPERRAFT", new MemberTable_Type(Simple_Raft.TableName, "int"));
                    MCL.Add("MLC2ALLOWEDPERRAFT", new MemberTable_Type(Simple_Raft.TableName, "bool"));

                    MCL.Add("MLPREDICTIONPROB", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLCLASSPREDICTED", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("FILTERED", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("INDEX", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("VAL COUNT", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLCLASSLAYOUT REPLICATES", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLREPDENSITY", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLREPPLATES", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLREPTIME", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("MLREPHEALTH", new MemberTable_Type("_Ignore", ""));
                    MCL.Add("COLUMN1", new MemberTable_Type("_Ignore", ""));
                }
                string Key = ColumnName.ToUpper();
                if (_MetadataColumnLookup.ContainsKey(Key)) return _MetadataColumnLookup[Key].Table;
                return "";
            }



        }

        public class MemberTable_Type
        {
            //[Description("Test description in MemberTable"), Category("Test")]
            public string Table { get; set; }

            public string TypeName { get; set; }

            public MemberTable_Type()
            {

            }

            public MemberTable_Type(string table, string type)
            {
                Table = table;
                TypeName = type;

                // Gets the attributes for the property. - 3/2022 Willie is experimenting with this
                //var attributes = TypeDescriptor.GetProperties(this)["Table"].Attributes;
                //var da = attributes[typeof(DescriptionAttribute)];
            }
        }


        #region Simple Collector Classes - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        public class Simple_Collector : IEnumerable<Simple_File>
        {
            internal Dictionary<string, Simple_File> _Set;
            public INCARTA_Cell_Headers Headers { get; internal set; }
            public IEnumerable<Simple_Cell> Cells
            {
                get
                {
                    List<ISimpleBase> T = _SiblingsRepo[Simple_Cell.TableName];
                    return T.Cast<Simple_Cell>();
                }
            }

            private Dictionary<string, List<ISimpleBase>> _SiblingsRepo = new Dictionary<string, List<ISimpleBase>>();

            internal void AddSibling(ISimpleBase ToAdd)
            {
                string Key = ToAdd.Table_Name;
                if (!_SiblingsRepo.ContainsKey(Key)) _SiblingsRepo.Add(Key, new List<ISimpleBase>());
                _SiblingsRepo[Key].Add(ToAdd);
            }

            public IEnumerable<ISimpleBase> GetSiblingsAtLevel(ISimpleBase ExampleLevel)
            {
                return _SiblingsRepo[ExampleLevel.Table_Name];
            }

            public Simple_Collector()
            {
                _Set = new Dictionary<string, Simple_File>();
            }

            public static Simple_Collector LoadInCartaFile(string PathToFile)
            {
                var SC = new Simple_Collector(); var tCell = new Simple_Cell();
                string FullFileSource = PathToFile.ToUpper().Trim();
                using (StreamReader SR = new StreamReader(PathToFile))
                {
                    SC.Headers = new INCARTA_Cell_Headers(SR.ReadLine(), null);

                    //Here is the actual start of the reading
                    while (!SR.EndOfStream)
                    {
                        tCell = new Simple_Cell(SC.Headers, SR.ReadLine(), SC.Headers.delim, SC, FullFileSource);
                        if (tCell.IsValid) //There are some lines in these files that aren't really a cell
                        {
                            SC.AddCell(tCell);
                        }
                    }
                    SR.Close();
                    return SC;
                }
            }

            public void Add(Simple_File tFile)
            {
                _Set.Add(tFile.FullFileNameSource, tFile);
            }

            public void AddCell(Simple_Cell tCell)
            {
                string key = tCell.FullFileNameSource;
                if (!_Set.ContainsKey(key)) Add(new Simple_File(key, this));
                _Set[key].AddCell(tCell);
            }

            public IEnumerator<Simple_File> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        public class Simple_File : IEnumerable<Simple_Scan>, ISimpleBase
        {
            internal Dictionary<string, Simple_Scan> _Set;

            private int _id;
            public int dbID { get { return _id; } set { _id = value; } }

            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public string Key_Col => "FFNS";
            public string Key_Val => FFNS;
            public static string TableName = "c10file";

            private string _FFNS = "-1";
            public string FFNS
            {
                get
                {
                    if (_FFNS == "-1") _FFNS = BitConverter.ToString(SHA.ComputeHash(System.Text.Encoding.UTF8.GetBytes(FullFileNameSource))).Substring(0, 6);
                    return _FFNS;
                }
            }

            private static System.Security.Cryptography.SHA256 SHA = System.Security.Cryptography.SHA256.Create();
            public string wtUID => FFNS + "|";
            private Simple_Collector _Parent;
            public ISimpleBase Parent { get => (ISimpleBase)_Parent; }
            public int parentID { get => -1; } //There isn't the same type of parent here, this is top level within the dB, but is owned by an object in the code
            public Simple_Collector Collector { get => _Parent; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }

            public string FullFileNameSource { get; set; }

            public override string ToString()
            {
                return FullFileNameSource;
            }

            public Simple_File(string FullfileNameSource, Simple_Collector collector)
            {
                FullFileNameSource = FullfileNameSource;
                SetupFileParams(FullfileNameSource);

                _Set = new Dictionary<string, Simple_Scan>(1);
                _Parent = collector; Collector.AddSibling(this);
            }

            internal Dictionary<string, string> FileParams;
            private void SetupFileParams(string FullfileNameSource)
            {
                FileParams = new Dictionary<string, string>();
                FileParams.Add(Key_Col, Key_Val);
                FileParams.Add("SM_FILENAME", Path.GetFileName(FullFileNameSource));
                FileParams.Add("SM_FILEPATH", Path.GetDirectoryName(FullFileNameSource));
                FileParams.Add("SM_FILETIMESTAMP", File.GetCreationTime(FullfileNameSource).ToString());
                FileParams.Add("SM_INSERTTIMESTAMP", DateTime.Now.ToString());
            }

            public void Add(Simple_Scan Scan)
            {
                _Set.Add(Scan.PlateID, Scan);
            }

            internal void AddCell(Simple_Cell tCell)
            {
                //Make sure the extended columns are added
                foreach (var item in FileParams)
                {
                    if (!tCell.Headers.Contains(item.Key)) tCell.Headers.AddNewColumn(item.Key);
                    tCell.AddData(item.Key, item.Value);
                }

                string key = tCell.PlateID;
                if (!_Set.ContainsKey(key)) Add(new Simple_Scan(key, this));
                _Set[key].AddCell(tCell);
                if (_BaseCell == null) _BaseCell = tCell;
            }

            public IEnumerator<Simple_Scan> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        public class Simple_Scan : IEnumerable<Simple_ZTR>, ISimpleBase
        {
            internal Dictionary<string, Simple_ZTR> _Set;

            private int _id;
            public int dbID { get { return _id; } set { _id = value; } }

            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public string FIVNum { get; set; }
            public string Key_Col => "PlateID";
            public string Key_Val => PlateID;
            public static string TableName = "c20scan";
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_File _Parent;
            public ISimpleBase Parent { get => _Parent; }
            public int parentID { get => Parent.dbID; }
            public Simple_Collector Collector { get => _Parent.Collector; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }

            public string PlateID { get; internal set; }


            public override string ToString()
            {
                return PlateID;
            }

            public Simple_Scan(string PlateID, Simple_File file)
            {
                this.PlateID = PlateID;
                _Set = new Dictionary<string, Simple_ZTR>(1);
                _Parent = file; Collector.AddSibling(this);

                if (PlateID.StartsWith("FIV"))
                {
                    //Add FIVNUM
                    int l0 = PlateID.IndexOfAny(new char[4] { 'A', 'P', '_', 'S' }, 3);
                    FIVNum = PlateID.Substring(0, l0);
                } else
                {
                    FIVNum = "FIV000";
                }
            }

            public void Add(Simple_ZTR ZTR)
            {
                _Set.Add(ZTR.ZTResultID, ZTR);
            }

            internal void AddCell(Simple_Cell tCell)
            {
                //Add the FIVNum to the cell data
                string Key = "FIVNUM";
                if (!tCell.Headers.Contains(Key)) tCell.Headers.AddNewColumn(Key);
                tCell.AddData(Key, FIVNum);

                //Now add the cell
                string key = tCell.ZTResultID;
                if (!_Set.ContainsKey(key)) Add(new Simple_ZTR(key, this));
                _Set[key].AddCell(tCell);
                if (_BaseCell == null) _BaseCell = tCell;
            }

            public IEnumerator<Simple_ZTR> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        public class Simple_ZTR : IEnumerable<Simple_Well>, ISimpleBase
        {
            internal Dictionary<string, Simple_Well> _Set;
            private int _id;
            public int dbID { get { return _id; } set { _id = value; } }
            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public static string TableName = "c30ztr";
            public string Key_Col => "ZTR"; //TODO (needs 3 key cols which is different)
            public string Key_Val => ZTResultID;
            public override string ToString()
            {
                return Key_Val;
            }
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_Scan _Parent;
            public ISimpleBase Parent { get => _Parent; }
            public int parentID { get => Parent.dbID; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }
            public Simple_Collector Collector { get; internal set; }
            public Simple_ZTR(string ztResultsID, Simple_Scan parent)
            {
                ZTResultID = ztResultsID;
                _Set = new Dictionary<string, Simple_Well>();
                _Parent = parent; Collector = parent.Collector; Collector.AddSibling(this);
            }

            public void AddCell(Simple_Cell Cell)
            {
                if (!Cell.Headers.Contains(Key_Col)) Cell.Headers.AddNewColumn(Key_Col);
                Cell.AddData(Key_Col, Key_Val);

                string key = Cell.WellLabel;
                if (!_Set.ContainsKey(key)) Add(new Simple_Well(key, this));
                _Set[key].AddCell(Cell);
                if (_BaseCell == null) _BaseCell = Cell;
            }

            public void Add(Simple_Well Well)
            {
                _Set.Add(Well.WellLabel, Well);
            }

            public string ZTResultID { get; set; }
            public IEnumerator<Simple_Well> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }


        public class Simple_Well : IEnumerable<Simple_FOV>, ISimpleBase
        {
            internal Dictionary<string, Simple_FOV> _Set;
            private int _id = -1;
            public int dbID { get { return _id; } set { _id = value; } }

            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public string Key_Col => "WELLLABEL";
            public string Key_Val => WellLabel;
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_ZTR _Parent;
            public ISimpleBase Parent { get => _Parent; }
            public int parentID { get => Parent.dbID; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }

            public Simple_Collector Collector { get; internal set; }
            public Simple_Well(string WellLabel, Simple_ZTR parent)
            {
                this.WellLabel = WellLabel;
                _Set = new Dictionary<string, Simple_FOV>(384);
                _Parent = parent; Collector = _Parent.Collector; Collector.AddSibling(this);
            }

            public string WellLabel { get; internal set; }
            public static string TableName = "c40well";

            public override string ToString()
            {
                return WellLabel;
            }

            public void Add(Simple_FOV FOV)
            {
                _Set.Add(FOV.FOV, FOV);
            }

            internal void AddCell(Simple_Cell tCell)
            {
                string key = tCell.FOV.ToString();
                if (!_Set.ContainsKey(key)) Add(new Simple_FOV(key, this));
                _Set[key].AddCell(tCell);
                if (_BaseCell == null) _BaseCell = tCell;
            }

            public IEnumerator<Simple_FOV> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        public class Simple_FOV : IEnumerable<Simple_Raft>, ISimpleBase
        {
            internal Dictionary<string, Simple_Raft> _Set;
            public string FOV;
            private int _id = -1;
            public int dbID { get { return _id; } set { _id = value; } }

            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public string Key_Col => "FOV";
            public string Key_Val => FOV;
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_Well _Parent;
            public ISimpleBase Parent { get => (ISimpleBase)_Parent; }
            public int parentID { get => Parent.dbID; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }
            public Simple_Collector Collector { get; internal set; }

            public Simple_FOV(string FOV, Simple_Well parent)
            {
                this.FOV = FOV;
                _Set = new Dictionary<string, Simple_Raft>(40);
                _Parent = parent; Collector = parent.Collector; Collector.AddSibling(this);
            }

            public override string ToString()
            {
                return FOV;
            }

            public static string TableName = "c50fov";

            public void Add(Simple_Raft Raft)
            {
                _Set.Add(Raft.RaftID, Raft);
            }

            internal void AddCell(Simple_Cell tCell)
            {
                string key = tCell.Headers.HasRaftHeader ? tCell.RaftID : "Z0Z0";
                if (!_Set.ContainsKey(key)) Add(new Simple_Raft(key, this));
                _Set[key].AddCell(tCell);
                if (_BaseCell == null) _BaseCell = tCell;
            }

            public IEnumerator<Simple_Raft> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        public class Simple_Raft : IEnumerable<Simple_Cell>, ISimpleBase
        {
            internal Dictionary<string, Simple_Cell> _Set;
            private int _id = -1;
            public int dbID { get { return _id; } set { _id = value; } }
            private Simple_Cell _BaseCell;
            public Simple_Cell CellBase { get => _BaseCell; }
            public string Table_Name => TableName;
            public string Key_Col => "RAFTID";
            public string Key_Val => RaftID;
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_FOV _Parent;
            public ISimpleBase Parent { get => (ISimpleBase)_Parent; }
            public int parentID { get => Parent.dbID; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }
            public Simple_Collector Collector { get; internal set; }

            public Simple_Raft(string RaftID, Simple_FOV parent)
            {
                this.RaftID = RaftID;
                _Set = new Dictionary<string, Simple_Cell>();
                _Parent = parent; Collector = parent.Collector; Collector.AddSibling(this);
            }

            public static string TableName = "c60raft";

            public string RaftID;

            public override string ToString()
            {
                return RaftID;
            }

            public void AddCell(Simple_Cell tCell)
            {
                string key = tCell.ObjectID;
                tCell.Parent = this;
                _Set.Add(key, tCell);
                if (_BaseCell == null) _BaseCell = tCell;
            }

            public IEnumerator<Simple_Cell> GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Set.Values.GetEnumerator();
            }
        }

        /// <summary>
        /// An InCarta "Cell" Object, designed for holding onto basic data, does NOT link out to more complex types
        /// </summary>
        public class Simple_Cell : ISimpleBase
        {
            public INCARTA_Cell_Headers Headers { get; internal set; }
            public string Line;
            public char delim = ',';
            internal const int Pre_DefaultVal = -129;
            internal string[] _DataPerHeader; //Simplified this 3/16/2022
            internal Dictionary<short, object> _DataPerHeaderExtended; //Extra data can now be added here
            public bool CountValues = true;
            private int _id = -1;
            public int dbID { get { return _id; } set { _id = value; } }
            public Simple_Cell CellBase { get => this; }
            public string Table_Name => TableName;
            public string Key_Col => "ObjectID";
            public string Key_Val => ObjectID;
            public string wtUID => Parent.wtUID + Key_Val + "|";
            private Simple_Raft _Parent;
            public ISimpleBase Parent { get => _Parent; set { _Parent = (Simple_Raft)value; } }
            public int parentID { get => Parent.dbID; }
            public IEnumerable<ISimpleBase> GetAllSiblings()
            {
                return Collector.GetSiblingsAtLevel(this);
            }
            public Simple_Collector Collector { get; internal set; } //Collector = collector; Collector.AddSibling(this);
            public bool IsValid { get; internal set; }

            public object DataPerHeader(string HeaderColumn)
            {
                if (_DataPerHeader == null) PrepDataPerHeader();
                short Key = Headers.ShortKeyTable(HeaderColumn);
                if (Key == -1) return "-1";
                if (Key < _DataPerHeader.Length) return _DataPerHeader[Key];
                if (_DataPerHeaderExtended.ContainsKey(Key)) return _DataPerHeaderExtended[Key];
                //if (_DataPerHeader.ContainsKey(Key)) return _DataPerHeader[Key]; //Removed this 3/16/2022, added _DataPerHeaderExtended
                return "-1";
            }

            public object Data_FromHeaderInfo(Header_Info HI)
            {
                return DataPerHeader(HI.HeaderName);
            }

            public Dictionary<string, object> Data_MultiHeaderInfo(List<Tuple<string, string>> listOf_ColName_Type)
            {
                var Res = new Dictionary<string, object>();
                foreach (var tup in listOf_ColName_Type)
                {
                    object reCast = DataPerHeader(tup.Item1); bool t;
                    if (tup.Item2 == "int") reCast = (reCast == (object)""?-1: int.Parse(reCast.ToString()));
                    if (tup.Item2 == "double") { if (reCast == (object)"") reCast = "Nan"; reCast = double.Parse(reCast.ToString()); }
                    if (tup.Item2 == "bool") reCast = bool.TryParse(reCast.ToString(), out t) ? bool.Parse(reCast.ToString()):false;
                    Res.Add(tup.Item1, reCast);
                }
                return Res;
            }

            internal void PrepDataPerHeader()
            {
                //_DataPerHeader = new Dictionary<short, object>(Headers.Count); //Removed this 3/16/2022
                string[] arr = Line.Split(delim);
                _DataPerHeader = arr;
                _DataPerHeaderExtended = new Dictionary<short, object>();
                if (!CountValues) return;
                for (short i = 0; i < arr.Length; i++)
                {
                    //_DataPerHeader.Add(i, arr[i]); //Removed 3/16/2022
                    if (arr[i] != "")
                    {
                        INCARTA_Cell_Headers.AddValueCount(Headers[i]);
                        _ValuesCount++;
                    }
                }
            }

            private int _ValuesCount = 0;
            public int ValuesCount
            {
                get
                {
                    if (_DataPerHeader == null) PrepDataPerHeader();
                    return _ValuesCount;
                }
            }

            private string _WellLabel;
            public string WellLabel
            {
                get
                {
                    if (_WellLabel == null) _WellLabel = DataPerHeader(INCARTA_Cell_Headers.col_Well).ToString();
                    return _WellLabel;
                }
            }

            private string _WellField;
            public string WellField
            {
                get
                {
                    if (_WellField == null) _WellField = WellLabel + "." + DataPerHeader(INCARTA_Cell_Headers.col_Field);
                    return _WellField;
                }
            }

            private int _FOV = Pre_DefaultVal;
            public int FOV
            {
                get
                {
                    if (_FOV == Pre_DefaultVal) _FOV = int.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Field)) - 1;
                    return _FOV;
                }
            }

            private string _ObjectID = "";
            public string ObjectID { get { if (_ObjectID == "") _ObjectID = (string)DataPerHeader(INCARTA_Cell_Headers.col_ObjectID); return _ObjectID; } }

            private string _PlateID = NotAssigned;
            public string PlateID
            {
                get
                {
                    if (_PlateID == NotAssigned) _PlateID = (string)DataPerHeader(INCARTA_Cell_Headers.col_PlateID);
                    if (_PlateID == "-1") _PlateID = (string)DataPerHeader("PlateID");
                    return _PlateID;
                }
            }

            public static string NotAssigned = "-1NAssigned";

            private string _RaftID = NotAssigned;
            public string RaftID
            {
                get
                {
                    if (_RaftID == NotAssigned) _RaftID = (string)DataPerHeader(INCARTA_Cell_Headers.col_CellsRaftID);
                    return _RaftID;
                }
            }

            private string _ZTResultID = NotAssigned;

            //The ZT Table stores the InCarta ResultsID and The Z and T of the cell
            public string ZTResultID
            {
                get
                {
                    if (_ZTResultID == NotAssigned)
                    {
                        string T = (string)DataPerHeader(INCARTA_Cell_Headers.col_ZT_T);
                        string Z = (string)DataPerHeader(INCARTA_Cell_Headers.col_ZT_Z);
                        string R = (string)DataPerHeader(INCARTA_Cell_Headers.col_ZT_ResultsID);
                        _ZTResultID = R + " t=" + T + "z=" + Z;
                    }
                    return _ZTResultID;
                }
            }

            public string FullFileNameSource { get; set; }

            public static string TableName = "c70cell";

            public Simple_Cell()
            {

            }

            public Simple_Cell(INCARTA_Cell_Headers Headers, string Line, char delim, Simple_Collector collector, string fullfilenameSource)
            {
                this.Headers = Headers;
                this.delim = delim;
                this.Line = Line;
                IsValid = true;
                CountValues = false;
                FullFileNameSource = fullfilenameSource;
                if (DataPerHeader(INCARTA_Cell_Headers.col_ObjectID).ToString() == "0") IsValid = false;
                Collector = collector;
                if (IsValid) Collector.AddSibling(this);
            }

            internal void AddData(string ExistingColumnHeader, object Data)
            {
                //From 3/16/2022 we are now using an extended table to get at this data
                short HeaderKey = Headers.ShortKeyTable(ExistingColumnHeader);
                if (_DataPerHeader == null) PrepDataPerHeader();
                _DataPerHeaderExtended.Add(HeaderKey, Data);
            }
        }

        public interface ISimpleBase
        {
            Simple_Cell CellBase { get; }
            string Table_Name { get; }
            string Key_Col { get; }
            string Key_Val { get; }
            string wtUID { get; } //Within Table UID - builds up per table
            int parentID { get; } //This is set only during table interaction since we may need to get this from the dB
            int dbID { get; set; }

            ISimpleBase Parent { get; }
            IEnumerable<ISimpleBase> GetAllSiblings();
        }

        #endregion

    }
}