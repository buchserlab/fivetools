﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FIVE
{

    namespace ImageCheck
    {
        public class ImageCheck_PointList 
        {
            public string Name { get; set; }
            public string Well { get; set; }
            public DateTime Created { get; set; }
            public List<ImageCheck_Point> Points { get; set; }

            public string Errors = "";

            public int Count { get => Points.Count; }
            public void Add(ImageCheck_Point Point)
            {
                Points.Add(Point);
                AddToDictionaries(Point);
                Dirty_Saved = true;
            }

         

            public void RemoveLast()
            {
                Dirty_Saved = true;
                ImageCheck_Point Pt = Points.Last();
                Points.Remove(Pt);
                _Dict = new Dictionary<string, List<ImageCheck_Point>>();
                _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
                _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
                RefreshDictionaries();
            }

            public void RemovePoints(List<ImageCheck_Point> pointsToRemove)
            {
                Dirty_Saved = true;
                Points = Points.Except(pointsToRemove).ToList();
                _Dict = new Dictionary<string, List<ImageCheck_Point>>();
                _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
                _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
                RefreshDictionaries();
            }

            private void AddToDictionaries(ImageCheck_Point Point)
            {
                string key1 = KeyMaker(Point);
                if (!_Dict.ContainsKey(key1)) _Dict.Add(key1, new List<ImageCheck_Point>());
                _Dict[key1].Add(Point);
                if (Point.RaftID != null)
                {
                    if (!_DictRaftID.ContainsKey(Point.RaftID)) _DictRaftID.Add(Point.RaftID, new List<ImageCheck_Point>());
                    _DictRaftID[Point.RaftID].Add(Point);
                }
                if (!_DictFOV.ContainsKey(Point.FOV)) _DictFOV.Add(Point.FOV, new List<ImageCheck_Point>());
                _DictFOV[Point.FOV].Add(Point);
            }

            internal void RefreshDictionaries()
            {
                //Also fix up the colors
                foreach (ImageCheck_Point Point in Points)
                {
                    if (Point.PlateID == null || Point.PlateID == "" || Point.PlateID == "UNK") 
                        Point.PlateID = new DirectoryInfo(Name).Name;

                    foreach (ImageCheck_Annotation ICA in Point.Annotations)
                    {
                        if (ICA.Color.IsEmpty && ICA.Name == "General" && (string)ICA.Value == "Bad") ICA.Color = System.Drawing.Color.Red;
                        if (ICA.Color.IsEmpty && ICA.Name == "General" && (string)ICA.Value == "Good") ICA.Color = System.Drawing.Color.LightGreen;
                    }
                    AddToDictionaries(Point);
                }
            }

            public ImageCheck_Point this[int index] { get => Points[index]; }

            public ImageCheck_PointList()
            {
                Points = new List<ImageCheck_Point>();
            }

            [XmlIgnore]
            public bool Dirty_Saved = false;

            public bool Save(string SavePath)
            {
                Created = DateTime.Now;
                XmlSerializer x = new XmlSerializer(typeof(ImageCheck_PointList));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    Dirty_Saved = false;
                    return true;
                }
            }

            public bool SaveDefault(string FolderPath)
            {
                string FullPath = Path.Combine(FolderPath, DefaultName);
                return Save(FullPath);
            }

            public string DefaultName { get => DefaultNameConstruct(Well); }
            public static string DefaultNameConstruct(string WellLabel = "") { return (DefaultSaveName + " " + WellLabel).Trim() + "." + DefaultExtension; }
            public static string DefaultSaveName = "ImageCheckList";
            public static string DefaultExtension = "XML";

            private Dictionary<string, List<ImageCheck_Point>> _Dict = new Dictionary<string, List<ImageCheck_Point>>();
            private Dictionary<string, List<ImageCheck_Point>> _DictRaftID = new Dictionary<string, List<ImageCheck_Point>>();
            private Dictionary<int, List<ImageCheck_Point>> _DictFOV = new Dictionary<int, List<ImageCheck_Point>>();
            public List<int> FOVs { get => _DictFOV.Keys.ToList(); }
            public IEnumerable<string> WellFields { get => _Dict.Keys; }

            public string Overview
            {
                get
                {
                    Dictionary<string, int> AnnoCount = new Dictionary<string, int>();
                   
                    foreach (var ICPoint in _Dict.Values)
                    {
                        foreach (ImageCheck_Point i2 in ICPoint)
                        {
                            foreach (var Anno in i2.Annotations)
                            {
                                if (!AnnoCount.ContainsKey(Anno.Value.ToString()))
                                    AnnoCount.Add(Anno.Value.ToString(), 0);
                                else
                                    AnnoCount[Anno.Value.ToString()]++;
                            }
                        }
                    }
                    StringBuilder SB = new StringBuilder();
                    foreach (KeyValuePair<string, int> KVP in AnnoCount)
                    { 
                        SB.Append($"{KVP.Key}:{KVP.Value},");
                    }

                    string Final = SB.ToString().TrimEnd(',');
                    return Final;
                }
            }
           
            

            public static string KeyMaker(string well_Row, int well_Col, int fOV, int Wavelength) { return well_Row + "_" + well_Col + "-" + fOV + ":0" /* + Wavelength*/; }
            public static string KeyMaker(ImageCheck_Point P) { return P.Well_Row + "_" + P.Well_Col + "-" + P.FOV + ":0" /* + P.WaveLength */; }

            public List<ImageCheck_Point> FromDictionary(string well_Row, int well_Col, int fOV, int Wavelength)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                string Key = KeyMaker(well_Row, well_Col, fOV, Wavelength);
                if (_Dict.ContainsKey(Key)) RetList = _Dict[Key];
                return RetList;
            }

            public IEnumerable<string> RaftIDs => _DictRaftID.Keys;

            public List<ImageCheck_Point> FromDictionaryRaftID(string raftID)
            {
                var RetList = new List<ImageCheck_Point>();
                if (_DictRaftID.ContainsKey(raftID)) RetList = _DictRaftID[raftID];
                return RetList;
            }

            public List<ImageCheck_Point> FromDictionarySpecific(string wellLabel, string fOV, string raftID)
            {
                var RetList = new List<ImageCheck_Point>();
                if (_DictRaftID.ContainsKey(raftID)) RetList = _DictRaftID[raftID];
                if (wellLabel == "" && fOV == "") return RetList;

                RetList.RemoveAll(x => x.Well_Label != wellLabel || x.FOV.ToString() != fOV);
                return RetList;
            }

            public string FromDictionaryRaftID_Combined(string raftID)
            {
                if (raftID == null) return "";
                List<ImageCheck_Point> RetList = FromDictionaryRaftID(raftID);
                if (RetList.Count == 0) return "";
                HashSet<string> Total = new HashSet<string>();
                foreach (var item in RetList) foreach (var item2 in item.Annotations) Total.Add(item2.Value.ToString()); //I tried to do this with LINQ but had a little trouble
                string T = string.Join(" ", Total);
                if (T.Length > 23)
                {

                }
                return T;
            }

            public List<ImageCheck_Point> FromDictionaryFOV(int FOV)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                if (_DictFOV.ContainsKey(FOV)) RetList = _DictFOV[FOV];
                return RetList;
            }

            public string FromDictionaryFOV_Combined(int FOV)
            {
                List<ImageCheck_Point> RetList = FromDictionaryFOV(FOV);
                if (RetList.Count == 0) return "";
                string T = RetList.Select(x => x.Result()).Distinct().Aggregate((v1, v2) => v1 + " " + v2);
                //string T = RetList.Select(x => x.Result()).Aggregate((v1, v2) => v1 + " " + v2);
                return T;
            }

            public List<ImageCheck_Point> FromDictionaryWellField(string well_Field)
            {
                List<ImageCheck_Point> RetList = new List<ImageCheck_Point>();
                RetList = _Dict[well_Field];
                return RetList;
            }

            internal static ImageCheck_PointList Load_Assumed(string FolderThatXDCEisIn, string WellLabel = "")
            {
                string PathAssumed = Path.Combine(FolderThatXDCEisIn, DefaultNameConstruct(WellLabel));
                FileInfo FI = new FileInfo(PathAssumed);
                if (FI.Exists) return Load(PathAssumed);
                return null;
            }

            public static ImageCheck_PointList Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(ImageCheck_PointList));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    try
                    {
                        var CR = (ImageCheck_PointList)x.Deserialize(F);
                        F.Close();
                        CR.RefreshDictionaries();
                        return CR;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            public IEnumerator<List<ImageCheck_Point>> GetEnumerator()
            {
                return _Dict.Values.GetEnumerator();
            }

            public static bool FromRaft(InCellLibrary.INCELL_Folder IC_Folder, RaftInfoS RaftInfo, ImageCheck_PointList ICPL, bool ExportMultiFieldsForSameRaft_ID = true)
            {
                string rID = RaftInfo.RaftID; HashSet<string> FOV_UniqueT; InCellLibrary.XDCE_Image Img;
                if (rID == null) return false;
                if (rID.Contains("/") || rID.Contains(".") || rID.Contains("-") || rID.Contains(",")) return false;
                if (IC_Folder.Rafts.Count == 0)
                    IC_Folder.XDCE.Refresh_Wells_Rafts();
                if (IC_Folder.Rafts.ContainsKey(rID))
                {
                    InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.Rafts[rID];
                    if ((RaftInfo.FOV != "" && RaftInfo.FOV != "-1") && (RaftInfo.WellLabel != ""))
                    {
                        //If the FOV and Well are specified, then do it directly
                        Img = SIG.GetImage_FromWellFOV(RaftInfo.WellField);
                        if (Img == null) { ICPL.Errors += RaftInfo.FOV + "_" + rID + "\t"; }
                        else
                        {
                            var ICP = ImageCheck_Point.FromImage(Img, rID);
                            ICP.Annotations.Add(new ImageCheck_Annotation() { Value = RaftInfo.ClassLabel });
                            ICPL.Add(ICP);
                        }
                        return true;
                    }
                    else
                    {

                    }
                    //If they aren't specified, then look through them
                    FOV_UniqueT = new HashSet<string>(SIG.Images.Select(x => x.WellField));
                    if (FOV_UniqueT.Count > 1)
                    {

                    }
                    foreach (string WellFOV in FOV_UniqueT)
                    {
                        Img = SIG.GetImage_FromWellFOV(WellFOV);
                        ICPL.Add(ImageCheck_Point.FromImage(Img, rID));
                        if (!ExportMultiFieldsForSameRaft_ID) continue;
                        //Most of the multi are actually just a sliver, and will get thrown out if we don't do it like this
                    }
                }
                return true;
            }

            public static ImageCheck_PointList FromRafts(InCellLibrary.INCELL_Folder IC_Folder, HashSet<RaftInfoS> Rafts)
            {
                // *** Restructure this so that each entry doesn't need to have a field/well as is the case here
                var T = IC_Folder.Rafts; //Not sure if this is necessary, but it seems to help, otherwise the Rafts won't be found

                var ICPL = new ImageCheck_PointList();
                bool ExportMultiFieldsForSameRaft_ID = true;
                //if (RaftIDs == null) RaftIDs = IC_Folder.Rafts.Keys;
                foreach (var rIS in Rafts)
                {
                    FromRaft(IC_Folder, rIS, ICPL, ExportMultiFieldsForSameRaft_ID);
                }
                if (ICPL.Errors != "")
                {

                }
                return ICPL; //Check error list
            }

            public static ImageCheck_PointList FromRafts(InCellLibrary.INCELL_Folder IC_Folder, IEnumerable<string> RaftIDs = null)
            {
                ImageCheck_PointList ICPL = new ImageCheck_PointList(); InCellLibrary.XDCE_Image Img; List<string> Removed = new List<string>();
                HashSet<string> FOV_UniqueT; bool ExportMultiFieldsForSameRaft_ID = true;
                if (RaftIDs == null) RaftIDs = IC_Folder.Rafts.Keys;
                foreach (string rID in RaftIDs)
                {
                    if (rID.Contains("/") || rID.Contains(".") || rID.Contains("-") || rID.Contains(",")) continue;
                    if (IC_Folder.Rafts.ContainsKey(rID))
                    {
                        InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.Rafts[rID];
                        FOV_UniqueT = new HashSet<string>(SIG.Images.Select(x => x.WellField));

                        foreach (string FOV in FOV_UniqueT)
                        {
                            Img = SIG.GetImage_FromWellFOV(FOV);
                            ICPL.Add(ImageCheck_Point.FromImage(Img, rID));
                            if (!ExportMultiFieldsForSameRaft_ID) continue;
                            //Most of the multi are actually just a sliver, and will get thrown out if we don't do it like this
                        }
                        Removed.Add(rID);
                    }
                }
                return ICPL;
            }

            public static ImageCheck_PointList FromWellFieldCells(InCellLibrary.INCELL_Folder IC_Folder, Dictionary<string, HashSet<CellInfoS>> WellField_Cells)
            {
                ImageCheck_PointList ICPL = new ImageCheck_PointList(); InCellLibrary.XDCE_Image Img;
                foreach (var KVP in WellField_Cells)
                {
                    if (IC_Folder.XDCE.WellFOV_Dict.ContainsKey(KVP.Key))
                    {
                        InCellLibrary.XDCE_ImageGroup SIG = IC_Folder.XDCE.WellFOV_Dict[KVP.Key];
                        Img = SIG.Images[0];
                        foreach (var Cell in KVP.Value) ICPL.Add(ImageCheck_Point.FromImageCell(Img, Cell));
                    }
                }
                return ICPL;
            }
        }

        public class ImageCheck_Point
        {
            public double Plate_Xum { get; set; }
            public double Plate_Yum { get; set; }
            public float PixelX_Fraction { get; set; }
            public float PixelY_Fraction { get; set; }
            public int WaveLength { get; set; }
            public string Well_Row { get; set; }
            public string Well_Col { get; set; }

            [XmlIgnore]
            public string Well_Label { get => Well_Row + " - " + Well_Col; }
            public int FOV { get; set; }
            public string RaftID { get; set; }
            public string PlateID { get; set; }
            public DateTime Created { get; set; }
            public List<ImageCheck_Annotation> Annotations { get; set; }

            [XmlIgnore]
            public bool IsCell = false;

            [XmlIgnore]
            public CellInfoS CellInfo = null;

            public Rectangle CellCoordinates(InCellLibrary.XDCE_Image XI, int Expand)
            {
                RectangleF rectangle = CellInfo.R;
                int PixelWidth = XI.Width_Pixels;
                int PixelHeight = XI.Height_Pixels;
                int L = (int)(rectangle.Left - Expand);
                L = L < 0 ? 0 : L;

                int T = (int)(rectangle.Top - Expand);
                T = T < 0 ? 0 : T;

                int R = (int)(rectangle.Right + Expand);
                R = R > PixelWidth ? PixelWidth : R;

                int B = (int)(rectangle.Bottom + Expand);
                B = B > PixelHeight ? PixelHeight : B;

                return Rectangle.FromLTRB(L, T, R - L, B - T);

            }

            public override string ToString()
            {
                return Well_Row + Well_Col;
            }

            public int Count { get => Annotations.Count; }
            public bool IsBad
            {
                get
                {
                    if (Annotations.Count < 1) return false;
                    return Annotations.Select(x => x.Value).Contains(ImageCheck_Annotation.Bad.Value);   //Annotations.Contains(ImageCheck_Annotation.Bad);
                }
            }

            public string Result()
            {
                string ret = Annotations.Select(x => x.Value).Aggregate((v1, v2) => string.Format("{0} {1}", v1, v2)).ToString();
                return ret;
            }

            public void Add(ImageCheck_Annotation imageCheck_Annotation)
            {
                Annotations.Add(imageCheck_Annotation);
            }

            public static ImageCheck_Point FromImage(InCellLibrary.XDCE_Image xDCE_Image, string RaftID)
            {
                var ICP = new ImageCheck_Point();
                ICP.RaftID = RaftID;
                ICP.FOV = xDCE_Image.FOV;
                ICP.Well_Row = xDCE_Image.Well_Row;
                ICP.Well_Col = xDCE_Image.Well_Column.ToString();
                ICP.PlateID = xDCE_Image.Parent.ParentFolder.PlateID;
                if (xDCE_Image.Rafts.ContainsKey(RaftID))
                {
                    //Old style
                    //var Points = (Tuple<double, double, float, float>)xDCE_Image.KVPs[RaftID]; //Refresh_Wells_Rafts() is what makes the points in the first place
                    //ICP.Plate_Xum = Points.Item1; ICP.Plate_Yum = Points.Item2;
                    //ICP.PixelX_Fraction = Points.Item3; ICP.PixelY_Fraction = Points.Item4;

                    var xiRI = xDCE_Image.Rafts[RaftID]; //New style 5/24/2022
                    ICP.Plate_Xum = xiRI.FullX; 
                    ICP.Plate_Yum = xiRI.FullY;
                    ICP.PixelX_Fraction = xiRI.FracXY.X; 
                    ICP.PixelY_Fraction = xiRI.FracXY.Y;
                }
                return ICP;
            }

            public static ImageCheck_Point FromImageCell(InCellLibrary.XDCE_Image xDCE_Image, CellInfoS cell)
            {
                ImageCheck_Point ICP = new ImageCheck_Point();
                ICP.FOV = xDCE_Image.FOV;
                ICP.Well_Row = xDCE_Image.Well_Row;
                ICP.Well_Col = xDCE_Image.Well_Column.ToString();
                ICP.PlateID = cell.PlateID;

                ICP.IsCell = true;
                ICP.CellInfo = cell;

                return ICP;
            }

            public ImageCheck_Point()
            {
                Annotations = new List<ImageCheck_Annotation>();
                PlateID = "";
                Created = DateTime.Now;
            }


        }

        public class ImageCheck_Annotation_Schema : IEnumerable<ImageCheck_Annotation>
        {
            //And if your class inherits a list and also has its own members, only the elements of the list get serialized. The data present in your class members is not captured. (From Stack Overflow)
            //I had this as inheriting IENumerable, but it didn't serialize other members

            private List<ImageCheck_Annotation> _List;
            private Dictionary<char, ImageCheck_Annotation> _DictMod;
            private Dictionary<String, ImageCheck_Annotation> _DictValue;

            public string Name { get; set; }
            public static ImageCheck_Annotation_Schema ActiveSchema = Neuronal_Schema();

            public static ImageCheck_Annotation_Schema Neuronal_Schema()
            {
                ImageCheck_Annotation_Schema ICAS = new ImageCheck_Annotation_Schema
                {
                    { "Neurons", "Unique 0", Color.LightGray, '0', 1 },
                    { "Neurons", "Poor focus", Color.DarkRed, '1', 1 },
                    { "Neurons", "No Neurons or dead", Color.Purple, '2', 1 },
                    { "Neurons", "Overgrown", Color.DarkOliveGreen, '3', 1 },
                    { "Neurons", "Unsure", Color.Yellow, '4', 1 },
                    { "Neurons", "Straight neurites or not adhered", Color.Orange, '5', 1 },
                    { "Neurons", "Degenerated axons", Color.Pink, '6', 1 },
                    { "Neurons", "Retraction bulbs", Color.LightBlue, '7', 1 },
                    { "Neurons", "Intact axons", Color.LightGreen, '8', 1 },
                    { "Neurons", "Short axons", Color.White, '9', 1 }
                };

                return ICAS;
            }

            public ImageCheck_Annotation_Schema()
            {
                _List = new List<ImageCheck_Annotation>();
                _DictMod = new Dictionary<char, ImageCheck_Annotation>();
                _DictValue = new Dictionary<string, ImageCheck_Annotation>();
            }

            public ImageCheck_Annotation FromValue(string Value)
            {
                if (_DictValue.ContainsKey(Value)) return _DictValue[Value];
                else return null;
            }

            public ImageCheck_Annotation FromModifier(char mod_key)
            {
                if (_DictMod.ContainsKey(mod_key))
                    return _DictMod[mod_key];
                else
                    return _List.First();
                //return new ImageCheck_Annotation() { Name = "New", Value = mod_key, Color = System.Drawing.Color.Magenta };
            }

            public void Add(string Name, object Value, char DefaultKey = ':', object DefaultButton = null)
            {
                var Annotate = new ImageCheck_Annotation() { Name = Name, Value = Value, Default_Key = DefaultKey, Default_Button = DefaultButton, Color = System.Drawing.Color.White };
                Add(Annotate);
            }

            public void Add(string Name, object Value, Color Color, char DefaultKey = ':', object DefaultButton = null)
            {
                var Annotate = new ImageCheck_Annotation() { Name = Name, Value = Value, Default_Key = DefaultKey, Default_Button = DefaultButton, Color = Color };
                Add(Annotate);
            }

            public void Add(ImageCheck_Annotation Annotation)
            {
                _List.Add(Annotation);
                _DictMod.Add(Annotation.Default_Key, Annotation);
                _DictValue.Add(Annotation.Value.ToString(), Annotation);
            }

            public string GetAsText(string delim = "\t")
            {
                return string.Join("\r\n", _List.Select(x => x.Name + delim + x.Default_Key + delim + x.Value));
            }

            public static ImageCheck_Annotation_Schema Load(string Path)
            {
                try
                {
                    var x = new XmlSerializer(typeof(ImageCheck_Annotation_Schema));
                    using (var F = new FileStream(Path, FileMode.Open))
                    {
                        var CR = (ImageCheck_Annotation_Schema)x.Deserialize(F);
                        F.Close();
                        return CR;
                    }
                }
                catch
                {
                    return Neuronal_Schema();
                }
            }

            public void Save(string Path)
            {
                var x = new XmlSerializer(typeof(ImageCheck_Annotation_Schema));
                using (var SW = new StreamWriter(Path))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                }
            }

            public IEnumerator<ImageCheck_Annotation> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _List.GetEnumerator();
            }
        }

        public class ImageCheck_Annotation
        {
            /// <summary>
            /// Like the type or subset of annotation
            /// </summary>
            public string Name { get; set; }
            public object Value { get; set; }
            public char Default_Key { get; set; }
            public object Default_Button { get; set; }
            public float Score { get; set; }
            public string Note { get; set; }
            public bool Ignore { get; set; }

            [XmlIgnore]
            public bool DisallowSkip = false; //Specific for Annotations use in Measurement

            public int[] FColor { get; set; }

            [XmlIgnore]
            public System.Drawing.Color Color
            {
                get
                {
                    if (FColor == null) return Color.Empty;
                    //if (FColor == null) FColor = new int[3] { 255, 255, 255 };
                    return Color.FromArgb(FColor[0], FColor[1], FColor[2]);
                }
                set
                {
                    FColor = new int[3] { value.R, value.G, value.B };
                }
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                // Append Value
                sb.Append(Value);

                // Append Score if it's not NaN
                if (!float.IsNaN(Score))
                {
                    sb.Append(' ');
                    sb.Append('(');
                    sb.AppendFormat("{0:0.00}", Score);
                    sb.Append(')');
                }

                // Append Name if it's not null or empty
                if (!string.IsNullOrEmpty(Name))
                {
                    sb.Append(" : ");
                    sb.Append(Name);
                }

                return sb.ToString();
            }

            public ImageCheck_Annotation()
            {
                Ignore = false;
                Score = float.NaN;
                Note = "";
                Name = "";
                Value = "";
                Color = Color.MintCream;
            }

            /// <summary>
            /// Default Key
            /// </summary>
            /// <returns></returns>
            public string Report()
            {
                return Default_Key + " = " + Name + " / " + Value;
            }

            public static ImageCheck_Annotation Bad = new ImageCheck_Annotation() { Name = "General", Value = "Bad", Color = System.Drawing.Color.Red };
            public static ImageCheck_Annotation Good = new ImageCheck_Annotation() { Name = "General", Value = "Good", Color = System.Drawing.Color.LightGreen };
        }

    }


}
