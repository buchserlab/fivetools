﻿using FIVE.ImageCheck;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.Marshalling;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace FIVE
{
    public class KDTree
    {
        private class Node
        {
            public CellInfoS Point;
            public Node Left;
            public Node Right;
            public int Axis;

            public Node(CellInfoS point, int axis)
            {
                Point = point;
                Axis = axis;
            }
        }

        private Node root;

        public KDTree(IEnumerable<CellInfoS> points)
        {
            int theCount;
            if (!points.TryGetNonEnumeratedCount(out theCount))
            {
                theCount = points.Count();
            }

            root = Build(points.ToArray(), 0, 0, theCount);
        }
    
        private Node Build(CellInfoS[] pointList, int axis, int start, int end)
        {
            if (start >= end)
                return null;

            int medianIndex = start + (end - start) / 2; // Safer median calculation
            CellInfoS median = SelectKth(pointList, medianIndex, axis, start, end);

            Node node = new Node(median, axis);

            // Alternate axis and recursively build subtrees
            //If there were hundreds of thousands of points we could consider Parallelism here using seperate tasks.
            int nextAxis = 1 - axis;
            node.Left = Build(pointList, nextAxis, start, medianIndex);
            node.Right = Build(pointList, nextAxis, medianIndex + 1, end);

            return node;
        }
        private static Random rand = new Random();
        private static float SelectPivot(CellInfoS[] group, int axis, int start, int end)
        {
            int randomIndex = rand.Next(start, end);
            return axis == 0
                ? group[randomIndex].R.Left
                : group[randomIndex].R.Top;
        }
        private static CellInfoS SelectKth(CellInfoS[] group, int k, int axis, int start, int end)
        {
            while (start <= end)
            {
          
                float pivotValue;

                if (end - start <= 2)
                {
                    // Directly return the Kth element for small ranges
                    return group[k];
                }
                else
                {
                    pivotValue = SelectPivot(group, axis, start, end);
                }
    
                int partitionIndex = PartitionInPlace(group, pivotValue, axis, start, end);
                // Narrow the search range based on the partition index
                if (partitionIndex == k)
                {
                    return group[k]; // Return the element if the partition matches K
                }
                else if (partitionIndex > k)
                {
                    end = partitionIndex; // Focus on the left sub-array
                }
                else
                {
                    start = partitionIndex + 1; // Focus on the right sub-array
                } // Focus on the right sub-array
            }

            // Return the element at the target index
            return group[start];
        }

        private static unsafe int PartitionInPlace(CellInfoS[] group, float pivotValue, int axis, int start, int end)
        {
            if (end <= start)
                return start; // Handle empty or invalid ranges.

            fixed (CellInfoS* pArray = group)
            {
                int i = start - 1; // Left boundary for swapping
                for (int j = start; j < end; j++)
                {
                    float value = axis == 0 ? (pArray + j)->R.Left : (pArray + j)->R.Top;

                    if (value < pivotValue)
                    {
                        i++; // Increment the left boundary
                        if (i != j)
                        {
                            // Swap the current element with the boundary element
                            Swap(ref *(pArray + i), ref *(pArray + j));
                        }
                    }
                }

                // Ensure a valid partition point
                if (i + 1 > end)
                    return end;
                return i + 1; // Return the partition index
            }
        }

        private static void Swap(ref CellInfoS a, ref CellInfoS b)
        {
            CellInfoS temp = a;
            a = b;
            b = temp;
        }

        private static float Median(float a, float b, float c)
        {
      

            if (b > c)
            {
                if (a > b)
                {
                    (a, b) = (b, a); // Ensure a <= b
                }
                return b; // b is the median
            }
            else
            {
                (b, c) = (c, b); // Ensure b <= c
                if (a > c)
                {
                    (a, c) = (c, a); // Ensure a <= c
                }
                return c; // c is the median
            }
        }
        public (CellInfoS Neighbor, double Distance) FindNearestWithDistance(PointF target)
        {
            Node bestNode = FindNearest(root, target, double.MaxValue);
            return (bestNode?.Point, bestNode != null ? Distance(bestNode.Point.CenterPoint, target) : double.MaxValue);
        }
        public CellInfoS FindNearest(PointF target)
        {
            return FindNearest(root, target, double.MaxValue).Point;
        }

        private Node FindNearest(Node node, PointF target, double maxDist)
        {
            if (node == null) return null;

            double d = Distance(node.Point.CenterPoint, target);
            Node best = (d < maxDist) ? node : null;
            maxDist = best != null ? d : maxDist;

            double diff = node.Axis == 0 ? target.X - node.Point.R.Left : target.Y - node.Point.R.Top;
            bool isTrue = diff <= 0;
            Node first = isTrue ? node.Left : node.Right;
            Node second = isTrue ? node.Right : node.Left;

            Node best1 = FindNearest(first, target, maxDist);
            if (best1 != null && (best == null || Distance(best1.Point.CenterPoint, target) < Distance(best.Point.CenterPoint, target)))
            {
                best = best1;
                maxDist = Distance(best.Point.CenterPoint, target);
            }

            if (Math.Abs(diff) < maxDist)
            {
                Node best2 = FindNearest(second, target, maxDist);
                if (best2 != null && (best == null || Distance(best2.Point.CenterPoint, target) < Distance(best.Point.CenterPoint, target)))
                {
                    best = best2;
                }
            }

            return best;
        }

        public static double Distance(PointF a, PointF b)
        {
            float x = a.X;
            float y = a.Y;
            float x1 = b.X;
            float y1 = b.Y;
            return Math.Sqrt((x- x1) * (x - x1) + (y - y1) * (y - y1));
        }

        public List<CellInfoS> Traverse()
        {
            List<CellInfoS> result = new List<CellInfoS>();
            Traverse(root, result);
            return result;
        }

        private void Traverse(Node node, List<CellInfoS> result)
        {
            if (node == null) return;

            result.Add(node.Point);
            Traverse(node.Left, result);
            Traverse(node.Right, result);
        }

        public void PrintTree()
        {
            PrintTree(root, "");
        }

        private void PrintTree(Node node, string indent)
        {
            if (node == null) return;

            Debug.WriteLine($"{indent}{node.Point.CenterPoint} (axis: {node.Axis})");
            PrintTree(node.Left, indent + "  ");
            PrintTree(node.Right, indent + "  ");
        }

    }

    /// <summary>
    /// Basically just for exporting the cell coordinates
    /// </summary>
    public class CellInfoS : RaftInfoS
    {
        public string ObjectID;
        public RectangleF R;

        public CellInfoS()
        {

        }
        public static CellInfoS[] GenerateTestArray(int size)
        {
            Random rand = new Random();
            CellInfoS[] cellArray = new CellInfoS[size];

            for (int i = 0; i < size; i++)
            {
                // Generate random Plate, Well, FOV, and Object IDs
                string plateID = $"Plate{rand.Next(1, 10)}";
                string wellLabel = $"Well{rand.Next(1, 100)}";
                string fov = $"FOV{rand.Next(1, 50)}";
                string objectID = $"Object{rand.Next(1, 1000)}";

                // Generate random rectangle coordinates
                string left = rand.Next(0, 500).ToString();
                string top = rand.Next(0, 500).ToString();
                string width = rand.Next(1, 100).ToString();
                string height = rand.Next(1, 100).ToString();

                // Create and add the CellInfoS instance to the array
                cellArray[i] = new CellInfoS(plateID, wellLabel, fov, objectID, left, top, width, height);
            }

            return cellArray;
        }
        public CellInfoS(string cPlate, string cWell, string cFOV, string cObjectID, string L, string T, string W, string H)
        {
            PlateID = cPlate;
            WellLabel = cWell;
            FOV = cFOV;
            RaftID = "";
            ObjectID = cObjectID;
            if (L == "") return;
            float fL = float.Parse(L);
            float fT = float.Parse(T);
            float fW = float.Parse(W);
            float fH = float.Parse(H);
            R = new RectangleF(fL, fT, fW, fH);
        }

        public CellInfoS NearestNeighbor = null;
        public double NNDist = double.NaN;

        public long ConnectID = -1;
        public double ConnectDist = double.NaN;

        public override string ToString()
        {
            return CenterPoint.ToString();
        }

        private PointF _CenterPoint = PointF.Empty;
        public PointF CenterPoint
        {
            get
            {
                if (_CenterPoint == PointF.Empty)
                {
                    _CenterPoint = new PointF(R.X + R.Width / 2, R.Y + R.Height / 2);
                }
                return _CenterPoint;
            }
            set { _CenterPoint = value; }
        }

        /// <summary>
        /// Simple method to Parse out PlateID and Rafts
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, Dictionary<string, HashSet<CellInfoS>>> Plate_WellField_CellInfo(string Path, string ColumnNameForAnnotation = "")
        {
            char delim = Path.EndsWith(".txt") ? '\t' : ',';
            var cols = new List<string> { "PLATEID", "WELL LABEL", "FOV", "OBJECT ID", "NUCLEI MAX LEFT BORDER WV1", "NUCLEI MAX TOP BORDER WV1", "NUCLEI MAX WIDTH WV1", "NUCLEI MAX HEIGHT WV1" };
            if (ColumnNameForAnnotation != "") cols.Add(ColumnNameForAnnotation.ToUpper());
            int[] iCols = new int[cols.Count]; string[] arr; var cHeaders = new Dictionary<string, int>(); var cHeaderList = new List<string>();
            var Output = new Dictionary<string, Dictionary<string, HashSet<CellInfoS>>>(); HashSet<CellInfoS> HS;

            string[] lines = File.ReadAllLines(Path);

            { //Setup Headers
                cHeaderList = lines[0].Split(delim).ToList();
                for (int i = 0; i < cHeaderList.Count; i++) cHeaders.Add(cHeaderList[i].ToUpper(), i);
                for (int i = 0; i < cols.Count; i++)
                {
                    string ColSearch = cols[i];
                    if (!cHeaders.ContainsKey(ColSearch))
                    {
                        if (ColSearch.Substring(ColSearch.Length - 1) == "1") //TODO: Make a centralized function for this
                        {
                            ColSearch = ColSearch.Substring(0, ColSearch.Length - 1) + "H"; //Convert to renamed version
                        }
                        if (ColSearch == "PLATEID")
                        {
                            ColSearch = "PLATE ID";
                        }
                        if (!cHeaders.ContainsKey(ColSearch))
                        {
                            if (ColSearch == ColumnNameForAnnotation.ToUpper())
                            {
                                //If we can't find the annotation column name, we can remove it from the list
                                cols.Remove(ColumnNameForAnnotation.ToUpper());
                                ColumnNameForAnnotation = "";
                                continue;
                            }
                            else
                            {
                                ParseError = "Need a column called: " + ColSearch;
                                return null;
                            }
                        }
                    }
                    iCols[i] = cHeaders[ColSearch];
                }
            } //Setup Headers

            string cPlateID, cWell, cFOV, cObj, cL, cT, cW, cH;
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split(delim);
                cPlateID = arr[iCols[0]]; cWell = arr[iCols[1]]; cFOV = arr[iCols[2]]; cObj = arr[iCols[3]];
                cL = arr[iCols[4]]; cT = arr[iCols[5]]; cW = arr[iCols[6]]; cH = arr[iCols[7]];

                if (cL == "") continue;
                var CI = new CellInfoS(cPlateID, cWell, cFOV, cObj, cL, cT, cW, cH);
                if (ColumnNameForAnnotation != "") CI.ClassLabel = arr[iCols[8]];
                if (!Output.ContainsKey(cPlateID)) Output.Add(cPlateID, new Dictionary<string, HashSet<CellInfoS>>());
                if (!Output[cPlateID].ContainsKey(CI.WellField)) Output[cPlateID].Add(CI.WellField, new HashSet<CellInfoS>());
                HS = Output[cPlateID][CI.WellField];
                HS.Add(CI);
            }
            return Output;
        }

        public CellInfoS BruteForce_FindNearest(IEnumerable<CellInfoS> listA)
        {
            return BruteForce_FindNearest(this.CenterPoint, listA);
        }

        public CellInfoS BruteForce_FindNearest(PointF target, IEnumerable<CellInfoS> listA)
        {
            CellInfoS closest = null;
            double minDist = double.MaxValue;

            foreach (var cell in listA)
            {
                double dist = DistanceTo(cell);
                if (dist < minDist)
                {
                    minDist = dist;
                    closest = cell;
                }
            }

            return closest;
        }

        public static void BruteForce_MatchB_to_A(IEnumerable<CellInfoS> listA, IEnumerable<CellInfoS> listB)
        {
            foreach (var cell in listB)
            {
                var nn = cell.BruteForce_FindNearest(listA);
                cell.PlateID += "B";
                cell.ConnectID = long.Parse(nn.ObjectID);
                cell.ConnectDist = cell.DistanceTo(nn);
            }
        }

        public double DistanceTo(CellInfoS cellB)
        {
            return Math.Sqrt(Math.Pow(this.R.X - cellB.R.X, 2) + Math.Pow(this.R.Y - cellB.R.Y, 2));
        }

        public string ExportNN(bool Headers = false)
        {
            if (Headers)
                return "PlateID" + "\t" + "WELL LABEL" + "\t" + "FOV" + "\t" + "OBJECT ID" + "\t" + "NN ID" + "\t" + "NN Dist";
            else
                return PlateID + "\t" + WellLabel + "\t" + FOV + "\t" + ObjectID + "\t" + NearestNeighbor.ObjectID + "\t" + NNDist;
        }
    }

    public class RaftInfoS
    {
        public string PlateID;
        public string RaftID;
        public string ClassLabel = "";
        public float Score = float.NaN;
        public string ModelName = "";
        public string Note = "";
        public string WellLabel = "";
        public string FOV = "";
        public Dictionary<string, int> Headers; //these refer to each other
        public List<string> HeaderList;
        public string[] Line;
        public string WellField => InCellLibrary.XDCE_Image.WellFieldKey(WellLabel, FOV);

        public override string ToString()
        {
            return RaftID;
        }

        private static Regex raftValidregex = new Regex(@"^[A-Za-z]\d[A-Za-z]\d$", RegexOptions.Compiled);

        public static bool IsRaftIDValid(string RaftID)
        {
            if (RaftID.Length > 4) return true;
            return raftValidregex.IsMatch(RaftID);
        }

        public string Export(int SubPlate, bool OnlyHeaders = false, bool RinseWell = false, string AppendStatus = "")
        {
            var sB = new StringBuilder();
            for (int i = 0; i < HeaderList.Count; i++)
            {
                sB.Append(OnlyHeaders ? HeaderList[i] : (HeaderList[i].ToUpper() == "PICKSUBPLATE" ? SubPlate.ToString() : (HeaderList[i].ToUpper() == "RAFTID" && RinseWell ? "B5B5" : Line[i])));
                sB.Append("\t");
            }
            sB.Append(OnlyHeaders ? "Status1" : AppendStatus);
            sB.Append("\r\n");
            return sB.ToString();
        }

        public static string ParseError = "";

        /// <summary>
        /// Simple method to Parse out PlateID and Rafts
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, HashSet<RaftInfoS>> Parse_PlateID_RaftInfo(string Path)
        {
            string[] cols = new string[4] { "PLATEID", "WELL LABEL", "FOV", "RAFTID" };
            int[] iCols = new int[cols.Length]; 
            string[] arr; var cHeaders = new Dictionary<string, int>(); 
            var cHeaderList = new List<string>();
            var Output = new Dictionary<string, HashSet<RaftInfoS>>(); HashSet<RaftInfoS> HS;
            int ClassLabelCol = -1;
            int AnnoCol = -1;

            string[] lines = File.ReadAllLines(Path);

            //Setup Headers
                cHeaderList = lines[0].Split('\t').ToList();
                for (int i = 0; i < cHeaderList.Count; i++) 
                    cHeaders.TryAdd(cHeaderList[i].ToUpper(), i);
                for (int i = 0; i < cols.Length; i++)
                {
                    if (!cHeaders.ContainsKey(cols[i]))
                    {
                        ParseError = "Need to include a column with the name: " + cols[i];
                        return null;
                    }
                    iCols[i] = cHeaders[cols[i]];
                }
                string ClassLbl = "MLCLASSLAYOUT";
                if (cHeaders.ContainsKey(ClassLbl)) ClassLabelCol = cHeaders[ClassLbl];
                string Anno = "PREDICTEDCLASS";
                if (cHeaders.ContainsKey(Anno)) AnnoCol = cHeaders[Anno];
             //Setup Headers

            string cPlateID, cWell, cFOV, cRaft;
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split('\t');
                cPlateID = arr[iCols[0]];
                cWell = arr[iCols[1]];
                cFOV = arr[iCols[2]];
                cRaft = arr[iCols[3]];

                if (!Output.ContainsKey(cPlateID)) 
                    Output.Add(cPlateID, new HashSet<RaftInfoS>());
                HS = Output[cPlateID];
                RaftInfoS RI = new RaftInfoS() { HeaderList = cHeaderList, Headers = cHeaders, WellLabel = cWell, FOV = cFOV, RaftID = cRaft, PlateID = cPlateID, Line = arr };
                if (ClassLabelCol >= 0) 
                    RI.ClassLabel = arr[ClassLabelCol];
                if (AnnoCol >= 0) 
                    RI.Note = arr[AnnoCol];
                HS.Add(RI);
            }
            ParseError = "";
            return Output;
        }

        /// <summary>
        /// Simple method to Parse out PlateID, Well, FOV, and RaftID
        /// </summary>
        /// <param name="Path">file to load</param>
        /// <returns>Returns them in a hierarchy</returns>
        public static Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>> Parse_PlateID_Well_FOV_RaftID(string Path)
        {
            //Written 12/2021, technically well label and FOV are optional, would be nice to implement that
            string[] lines = File.ReadAllLines(Path);
            var cols = new string[4] { "PLATEID", "WELL LABEL", "FOV", "RAFTID" };
            int[] iCols = new int[cols.Length];
            List<string> arr;

            arr = lines[0].ToUpper().Split('\t').ToList();
            for (int i = 0; i < cols.Length; i++)
            {
                if (!arr.Contains(cols[i]))
                {
                    ParseError = "Need to include a column with the name: " + cols[i];
                    return null;
                }
                iCols[i] = arr.IndexOf(cols[i]);
            }
            var PlateIDs_Well_FOV_RaftID = new Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>>();
            for (int i = 1; i < lines.Length; i++)
            {
                arr = lines[i].Split('\t').ToList();
                string PlateID = arr[iCols[0]];
                string Well = arr[iCols[1]];
                string FOV = arr[iCols[2]];
                string RaftID = arr[iCols[3]];
                if (!PlateIDs_Well_FOV_RaftID.ContainsKey(PlateID)) PlateIDs_Well_FOV_RaftID.Add(PlateID, new Dictionary<string, Dictionary<string, HashSet<string>>>());
                if (!PlateIDs_Well_FOV_RaftID[PlateID].ContainsKey(Well)) PlateIDs_Well_FOV_RaftID[PlateID].Add(Well, new Dictionary<string, HashSet<string>>());
                if (!PlateIDs_Well_FOV_RaftID[PlateID][Well].ContainsKey(FOV)) PlateIDs_Well_FOV_RaftID[PlateID][Well].Add(FOV, new HashSet<string>());
                PlateIDs_Well_FOV_RaftID[PlateID][Well][FOV].Add(RaftID);
            }
            ParseError = "";
            return PlateIDs_Well_FOV_RaftID;
        }
        public static Dictionary<string, HashSet<RaftInfoS>> FromAnnotationsJosh(InCellLibrary.INCELL_Folder folderToPass, int MaxToReturn = int.MaxValue)
        {
            var output = new Dictionary<string, HashSet<RaftInfoS>>();
            int testCount = 0;
            foreach (InCellLibrary.XDCE_ImageGroup well in folderToPass.XDCE.Wells.Values)
            {
                string plateId = well.PlateID;

                // Project points to RaftInfoS objects
                HashSet<RaftInfoS> raftInfos = new HashSet<RaftInfoS>(
                    well.ImageCheck_PointsList.Points.Select(ICP =>
                    {
                        ImageCheck_Annotation anno = ICP.Annotations[0];
                        return new RaftInfoS
                        {
                            HeaderList = null,
                            Headers = null,
                            Line = null,
                            WellLabel = ICP.Well_Label,
                            FOV = (ICP.FOV + 1).ToString(),
                            RaftID = ICP.RaftID,
                            ClassLabel = anno.Value.ToString(),
                            ModelName = anno.Name,
                            Score = anno.Score,
                            PlateID = plateId,
                            Note = anno.Note
                        };
                    }));

                // Check if PlateID already exists; merge if it does
                if (!output.ContainsKey(plateId))
                {
                    output[plateId] = raftInfos;
                }
                else
                {
                    output[plateId].UnionWith(raftInfos); // Merge only if necessary
                }

                // Early exit if the total count reaches MaxToReturn
                testCount += raftInfos.Count;
                if (testCount >= MaxToReturn)
                    break;
            }

            return output;
        }

        public static Dictionary<string, HashSet<RaftInfoS>> FromAnnotations(InCellLibrary.INCELL_Folder folderToPass, int MaxToReturn = int.MaxValue)
        {
            Dictionary<string, HashSet<RaftInfoS>> Output = new Dictionary<string, HashSet<RaftInfoS>>(); HashSet<RaftInfoS> HS;

            string cPlateID;
            int TestCount = 0;
            foreach (var Well in folderToPass.XDCE.Wells.Values)
            {
                ImageCheck_PointList ICPL = Well.ImageCheck_PointsList;
                if (ICPL == null) continue;
                foreach (var WellField in ICPL)
                {
                    foreach (var ICP in WellField)
                    {
                        cPlateID = folderToPass.PlateID;
                        if (!Output.ContainsKey(cPlateID)) Output.Add(cPlateID, new HashSet<RaftInfoS>());
                        HS = Output[cPlateID];
                        ImageCheck_Annotation Anno = ICP.Annotations[0];
                        RaftInfoS RI = new RaftInfoS()
                        {
                            HeaderList = null,
                            Headers = null,
                            Line = null,
                            WellLabel = ICP.Well_Label,
                            FOV = (ICP.FOV + 1).ToString(),
                            RaftID = ICP.RaftID,
                            ClassLabel = Anno.Value.ToString(),
                            ModelName = Anno.Name,
                            Score = Anno.Score,
                            PlateID = cPlateID,
                            Note = Anno.Note
                        };
                        HS.Add(RI);
                        if (TestCount++ > MaxToReturn) return Output;
                    }
                }
            }
            ParseError = "";
            return Output;
        }

        private const string RaftID_Chars_Set = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string RaftID_FromIdx(int idx)
        {
            //36^5 = 60,466,176
            var result = "";
            while (idx > 0)
            {
                result = RaftID_Chars_Set[(idx % 36)] + result;
                idx /= 36;
            }
            return result.PadLeft(5, '0');
        }
    }

    public static class Name_Link
    {
        public static void RenameTest()
        {

        }
    }

}
