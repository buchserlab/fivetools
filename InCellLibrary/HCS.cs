using FIVE.FOVtoRaftID;
using OpenCvSharp;
using OpenCvSharp.Aruco;
using OpenCvSharp.Detail;
using OpenCvSharp.Dnn;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using static System.Net.Mime.MediaTypeNames;

namespace FIVE
{
    namespace InCellLibrary
    {
        public static class INCELL_Helper
        {
            public static void CompileAnalyses(INCELL_DB dB, string ExportFolder, int MinCellsPerField, int MaxCellsPerField, List<INCARTA_Analysis_Folder> AFs, bool Rename_Wavelengths, System.ComponentModel.BackgroundWorker BW = null)
            {
                dB.CompileList_AFs = AFs;

                //Setup the cell filters to get rid of cells or fields that aren't needed
                List<Cell_Filter> Filters = new List<Cell_Filter>(2) { Cell_Filter.Default_FieldRestrict(MinCellsPerField, MaxCellsPerField), Cell_Filter.Default_Object0Remove() };

                //TODO: Bring in the standard metadata format along with the files
                bool FullVsPartial = false;
                string PathExport = Path.Combine(ExportFolder, AFs.First().PlateID + " x"+ AFs.Count.ToString() + " compiled_" + DateTime.Now.ToString("hhmmss") + ".csv");
                if (FullVsPartial)
                {
                    //The disadvantage of this approach is that it really fills up memory
                    //Compile and Export
                    var CombinedAnalysis = new INCARTA_Analysis(dB.CompileList_AFs, BW);
                    CombinedAnalysis.ApplyFilters(Filters);
                    if (BW != null) BW.ReportProgress(0, "Exporting..");
                    CombinedAnalysis.Export(PathExport);
                }
                else
                {
                    //This approach is less memory hungry
                    INCARTA_Analysis.CompileExport(dB.CompileList_AFs, PathExport, Rename_Wavelengths, BW);
                }
            }
        }

        public class xI_RaftInfo
        {
            public FOVtoRaftID.ReturnRaft RR { get; set; }
            public string RaftID { get => RR.RaftID; }
            public double FullX { get; set; }
            public double FullY { get; set; }
            public PointF FracXY { get; set; }

            public xI_RaftInfo(FOVtoRaftID.ReturnRaft rr, double fullX, double fullY, PointF fracXY)
            {
                RR = rr;
                FullX = fullX;
                FullY = fullY;
                FracXY = fracXY;
            }

            public bool ContainsPoint(double fullX, double fullY)
            {
                return RR.RaftCornersFullRect.Contains((float)fullX, (float)fullY);
            }
        }

        public class INCELL_MeasuredOffsets
        {
            //Metadata
            public string InstrumentName;
            public string InstrumentLocation;
            public string Objective;
            public string ScanPattern;

            //Measured settings
            public double Overlap_x_um { get; set; }
            public double Overlap_y_um { get; set; }
            public double AcrossXum_MovingMultiplier_Y { get; set; }
            public double AcrossYum_MovingMultiplier_X { get; set; }

            public static double StartingShiftX = 10000;
            public static double StartingShiftY = 10000;

            public double Transform_x(double ImageWidth_um, double PlateX_um, double PlateY_um)
            {
                return Math.FusedMultiplyAdd(PlateX_um, (ImageWidth_um + Overlap_x_um) / ImageWidth_um, (PlateY_um - StartingShiftX) * AcrossYum_MovingMultiplier_X);
            }

            public double Transform_y(double ImageHeight_um, double PlateX_um, double PlateY_um)
            {
                return Math.FusedMultiplyAdd(PlateY_um, (ImageHeight_um + Overlap_y_um) / ImageHeight_um, (PlateX_um - StartingShiftX) * AcrossXum_MovingMultiplier_Y);
            }

            public INCELL_MeasuredOffsets()
            {
                InstrumentName = "";
                InstrumentLocation = "";
                Objective = "";
                ScanPattern = "";
                Overlap_x_um = 0;
                Overlap_y_um = 0;
                AcrossYum_MovingMultiplier_X = 0;
                AcrossXum_MovingMultiplier_Y = 0;
            }

            public static INCELL_MeasuredOffsets WorseForTest()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 33xx 4444 Forest Park Ave",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = -40.3,
                    Overlap_y_um = -35.1,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.015
                };
                return IMO;
            }

            public static INCELL_MeasuredOffsets Couch06_01()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 6xx Couch Building",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = -4.1,
                    Overlap_y_um = -4.1,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.01
                };
                return IMO;
            }

            public static INCELL_MeasuredOffsets Couch06_02()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 6xx Couch Building",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = +20,
                    Overlap_y_um = -15,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.01
                };
                return IMO;
            }

            public static INCELL_MeasuredOffsets Default4444()
            {
                var IMO = new INCELL_MeasuredOffsets()
                {
                    InstrumentName = "InCell 6500HS",
                    InstrumentLocation = "Room 33xx 4444 Forest Park Ave",
                    Objective = "20x 0.45NA",
                    ScanPattern = "Horizontal Serpentine",
                    Overlap_x_um = -6.3,
                    Overlap_y_um = -3.1,
                    AcrossYum_MovingMultiplier_X = 0,
                    AcrossXum_MovingMultiplier_Y = 0.01
                };
                return IMO;
            }
        }

        public class XDCE_Image
        {
            public Dictionary<string, object> KVPs;

            public Dictionary<string, xI_RaftInfo> Rafts = new Dictionary<string, xI_RaftInfo>();
            public string RaftCalCode { get; set; }
            public XDCE Parent { get; set; }
            public bool IsTimeSeries { get; set; }

            public int TimeSeriesOrder = 1;
            public string PlateID { get => Parent.NameString; }
            public override string ToString() { return FileName; }
            public static string KVP_TO_XML(string Key, object Value, bool EndTag = true)
            {
                string[] Arr = Key.ToString().Split('_');
                string Front = "";
                switch (Arr.Length)
                {
                    case 1:
                        Front = Arr[0] + " FAKE";
                        break;
                    case 2:
                        Front = Arr[0] + " " + Arr[1];
                        break;
                    case int n when n >= 3:
                        Front = Arr[0] + " " + Arr[1] + "_" + Arr[2];
                        break;
                    default:
                        Debugger.Break();
                        break;
                }
                return "<" + Front + "=\"" + Value.ToString() + (EndTag ? "\"/>\r\n" : "\">\r\n");

                //return "<" + Front + ">" + Value.ToString() + "</" + Key + ">" + "\r\n"; 
            }

            public static Dictionary<string, object> XML_Extract_KVPs(string XML)
            {
                string oXML = XML;
                XML = oXML;
                if (XML.StartsWith("<?XML"))
                {
                    //XML = XML.Replace("<?XML VERSION=\"1.0\" ENCODING=\"ISO-8859-1\" STANDALONE=\"NO\"?>", ""); //Replaced 11/2023
                    int endIndex = XML.IndexOf("?>", 4, StringComparison.OrdinalIgnoreCase);
                    if (endIndex > 0) XML = XML.Substring(endIndex + 2).Trim();
                    if (XML.Contains("</AUTOLEADACQUISITIONPROTOCOL>"))
                        XML += "</IMAGESTACK>";
                    else
                        XML += "</" + (XML.Contains("<FIELDOFFSETS") ? "FIELDOFFSETS" : "POINTLIST") + "></ACQUISITIONMODE></PLATEMAP></AUTOLEADACQUISITIONPROTOCOL></IMAGESTACK>";
                    XML = XML.Replace("&", "");
                }
                var doc = XDocument.Parse(XML);
                var KVPs = new Dictionary<string, object>();
                foreach (XElement element in doc.Descendants()/*.Where(p => p.HasElements == false)*/)
                {
                    int keyInt = 0; string keyName = element.Name.LocalName;
                    if (element.HasAttributes)
                    {
                        foreach (XAttribute attribute in element.Attributes())
                        {
                            string Key2 = keyName + "_" + attribute.Name;
                            while (KVPs.ContainsKey(Key2)) Key2 = keyName + "_" + attribute.Name + "_" + keyInt++;
                            KVPs.Add(Key2, attribute.Value);
                        }
                    }
                    if (element.Value != null)
                        if (element.Value != "")
                        {
                            while (KVPs.ContainsKey(keyName)) keyName = element.Name.LocalName + "_" + keyInt++;
                            KVPs.Add(keyName, element.Value);
                        }
                }
                return KVPs;
            }

            /// <summary>
            /// Returns the full Plate Coordinates in Microns for a pixel location on this Image expressed in fractions
            /// </summary>
            public void PlateCoordinates_FromFraction(double x, double y, out double fullX, out double fullY)
            {
                fullX = 0; fullY = 0;
                PlateCoordinates_FromPixel((int)(x * (double)Parent.ImageWidth_in_Pixels), (int)(y * (double)Parent.ImageHeight_in_Pixels), out fullX, out fullY);
            }

            public void PlateCoordinates_FromPixel(int x, int y, out double fullX, out double fullY)
            {
                fullX = PlateX_um + Parent.PixelWidth_in_um * x;
                fullY = PlateY_um + Parent.PixelHeight_in_um * y;
            }

            public PointF Fraction_FromPlateCoordinates(PointF PlateCors)
            {
                var pPixel = Pixel_FromPlateCoordinates(PlateCors);
                return new PointF(pPixel.X / Parent.ImageWidth_in_Pixels, pPixel.Y / Parent.ImageHeight_in_Pixels);
            }

            public PointF Pixel_FromPlateCoordinates(PointF PlateCors)
            {
                PointF p = new PointF(
                    (float)((PlateCors.X - PlateX_um) / Parent.PixelWidth_in_um),
                    (float)((PlateCors.Y - PlateY_um) / Parent.PixelHeight_in_um));
                return p;
            }

            public XDCE_Image()
            {

            }

            public XDCE_Image(string XML, int index, XDCE Parent)
            {
                this.Parent = Parent; _Index = index;
                KVPs = XML_Extract_KVPs(XML);
            }

            public XDCE_Image(string filePath, XDCE xDCEParent, string WellLabel, int FOV_Param, short wavelength, RectangleF? newFOVRect_um = null, XDCE_Image relatedImage = null)
            {
                KVPs = relatedImage == null ? new Dictionary<string, object>() : new Dictionary<string, object>(relatedImage.KVPs);
                Parent = xDCEParent;
                _FileName = Path.GetFileName(filePath);
                _Index = FOV_Param;
                this.FOV = FOV_Param; _ColNumber = 0; _RowNumber = FOV_Param;
                this.wavelength = wavelength;
                if (newFOVRect_um == null)
                {
                    _PlateX_um = _RowNumber * 1000; _PlateY_um = 0; //Just so there are coordinates
                }
                else
                {
                    this.PlateX_um = newFOVRect_um.Value.X; this.PlateY_um = newFOVRect_um.Value.Y;
                }
                this.WellLabel = WellLabel;  //Unncessary?
            }

            private double _x = double.NaN;
            public double x
            {
                get
                {
                    if (double.IsNaN(_x))
                    {
                        if (!KVPs.ContainsKey("OFFSETFROMWELLCENTER_X")) _x = -1;
                        else _x = double.Parse((string)KVPs["OFFSETFROMWELLCENTER_X"]);
                    }
                    return _x;
                }
            }

            private double _y = double.NaN;
            public double y
            {
                get
                {
                    if (double.IsNaN(_y))
                    {
                        if (!KVPs.ContainsKey("OFFSETFROMWELLCENTER_Y")) _y = -1;
                        else _y = double.Parse((string)KVPs["OFFSETFROMWELLCENTER_Y"]);
                    }
                    return _y;
                }
            }

            private double _z = double.NaN;
            public double z { get { if (double.IsNaN(_z)) _z = double.Parse((string)KVPs["FOCUSPOSITION_Z"]); return _z; } }

            public static string ColName_AdjacentFocusAvg = "ADJACENT_FOCUS";
            private double _z_AvgAdj = double.NaN;
            public double z_AvgAdjacent
            {
                get
                {
                    if (double.IsNaN(_z_AvgAdj))
                    {
                        if (!KVPs.ContainsKey(ColName_AdjacentFocusAvg)) this.Parent.SetupFocusDeviation();
                        _z_AvgAdj = (double)KVPs[ColName_AdjacentFocusAvg];
                    }
                    return _z_AvgAdj;
                }
            }

            private string _timeStampSec = "-1";
            public string timeStampSec
            {
                get
                {
                    if (_timeStampSec == "-1")
                    {
                        if (KVPs.ContainsKey(INCARTA_Cell_Headers.col_TimeStampSec))
                            _timeStampSec = KVPs[INCARTA_Cell_Headers.col_TimeStampSec].ToString();
                    }
                    return _timeStampSec;
                }
            }

            private readonly static string platePosBase = "PLATEPOSITION_UM_";
            private readonly static string plposNameX = platePosBase + "X", plposNameY = platePosBase + "Y";
            /// <summary>
            /// Used to reset pre-calculated plateX and plateY when the measured offsets change
            /// </summary>
            internal void RecalcPlateCoordinates()
            {
                if (!KVPs.ContainsKey(plposNameX) || !KVPs.ContainsKey(plposNameY))
                {
                    _PlateX_um = -1; _PlateY_um = -1; return;
                }
                var rawX = double.Parse((string)KVPs[plposNameX]);
                var rawY = double.Parse((string)KVPs[plposNameY]);

                _PlateX_um = Parent.MeasuredOffsets.Transform_x(Width_microns, rawX, rawY);
                _PlateY_um = Parent.MeasuredOffsets.Transform_y(Height_microns, rawX, rawY);
            }

            private double _PlateX_um = double.NaN;
            public double PlateX_um
            {
                get { if (double.IsNaN(_PlateX_um)) RecalcPlateCoordinates(); return _PlateX_um; }
                internal set { _PlateX_um = value; KVPs[plposNameX] = _PlateX_um.ToString(); }
            }

            private double _PlateY_um = double.NaN;
            public double PlateY_um
            {
                get { if (double.IsNaN(_PlateY_um)) RecalcPlateCoordinates(); return _PlateY_um; }
                internal set { _PlateY_um = value; KVPs[plposNameY] = _PlateY_um.ToString(); }
            }
            public PointF PlateXY_um { get => new PointF((float)PlateX_um, (float)PlateY_um); }
            public PointF PlateXY_Center_um { get => new PointF((float)PlateX_um + ((float)Width_microns / 2), (float)PlateY_um + ((float)Height_microns / 2)); }

            private int _wavelength = int.MinValue;
            public int wavelength
            {
                get { if (_wavelength == int.MinValue) _wavelength = int.Parse((string)KVPs["IDENTIFIER_WAVE_INDEX"]); return _wavelength; }
                internal set { _wavelength = value; KVPs["IDENTIFIER_WAVE_INDEX"] = _wavelength.ToString(); }
            }

            public int Width_Pixels { get => Parent.ImageWidth_in_Pixels; }
            public int Height_Pixels { get => Parent.ImageWidth_in_Pixels; }
            public double Width_microns { get => Parent.ImageWidth_in_Pixels * Parent.PixelWidth_in_um; }
            public double Height_microns { get => Parent.ImageHeight_in_Pixels * Parent.PixelHeight_in_um; }

            private int _FOV = -12;
            public int FOV
            {
                get { if (_FOV == -12) _FOV = int.Parse((string)KVPs["IDENTIFIER_FIELD_INDEX"]); return _FOV; }
                internal set { _FOV = value; KVPs["IDENTIFIER_FIELD_INDEX"] = _FOV.ToString(); }
            }

            private string _fullPath; // Backing field for FullPath

            public string FullPath
            {
                get
                {
                    // Use the backing field for new format, construct dynamically for old format
                    return Parent.ParentFolder.isNewFormat
                        ? _fullPath
                        : Path.Combine(Parent.FolderPath, FileName);
                }
                set
                {
                    _fullPath = value;
                }
            }


            private string _FileName = "";
            public string FileName
            {
                get
                {
                    if (_FileName == "") _FileName = (string)KVPs["IMAGE_FILENAME"];
                    return _FileName;
                }
                set
                {
                    _FileName = value;
                }
            }

            public string FileName_GenImageBase { get => FileName_GenImageBaseWV(wavelength); }
            public string FileName_GenImageBaseWV(int wvUse)
            { return "" + Well_Row + "-" + Well_Column + "(F" + (FOV + 1) + " Z0 T0 W" + wvUse + ")"; } //added this 12/6/2020 to account for the gen images using different wavelengths than the one we might want to overlay from

            private int _Index;
            public int Index { get => _Index; }

            private int _RowNumber = -1;
            /// <summary>
            /// The zero-indexed row number of the well that the image is taken from (A = 0, B = 1, etc.)
            /// </summary>
            public int RowNumber
            {
                get
                {
                    if (_RowNumber == -1) _RowNumber = int.Parse((string)KVPs["ROW_NUMBER"]) - 1;
                    return _RowNumber;
                }
            }

            private int _ColNumber = -1;
            /// <summary>
            /// The zero-indexed column number of the well that the image is taken from
            /// </summary>
            public int ColNumber
            {
                get
                {
                    if (_ColNumber == -1) _ColNumber = int.Parse((string)KVPs["COLUMN_NUMBER"]) - 1;
                    return _ColNumber;
                }
            }

            public string Well_Row { get => ((char)(RowNumber + 1 + 64)).ToString(); } //Since RowNumber is 0 indexed (adjusted 12/6/2020)
            public int Well_Column { get => ColNumber + 1; } //Since ColNumber is 0 indexed (adjusted 12/6/2020)

            private const string Wellsz = "WELL_LABEL";
            public string WellLabel
            {
                get => KVPs.ContainsKey(Wellsz) ? KVPs[Wellsz].ToString() : "X - 0"; set
                {
                    if (!KVPs.ContainsKey(Wellsz)) KVPs.Add(Wellsz, value);
                    else KVPs[Wellsz] = value;
                }
            }

            public static string WellFieldKey(string WellLabel, int FOV)
            {
                return WellLabel + "." + (FOV + 1);
            }

            public static string WellFieldKey(string WellLabel, string FOV)
            {
                return WellFieldKey(WellLabel, int.Parse(FOV) - 1);
            }

            public string WellField
            {
                get => WellFieldKey(WellLabel, FOV);
            }
            public PointF CenterPixel { get => new PointF(Width_Pixels / 2, Height_Pixels / 2); }

            public string MaskFileName { get => Path.GetFileNameWithoutExtension(FileName).ToUpper() + ".TIF"; } //They are now usually 1-bit TIFFs
            public string MaskFullPath { get => Path.Combine(Parent.MaskFolder, MaskFileName); }

            public string MaskShiftedName { get => Path.GetFileNameWithoutExtension(FileName).ToUpper() + "_SHIFT.TIF"; }

            public string MaskShiftedFullPath { get => Path.Combine(Parent.MaskFolder, MaskShiftedName); }
            public bool HasMask
            {
                get
                {
                    FileInfo FI = new FileInfo(MaskFullPath);
                    return FI.Exists;
                }
            }

            internal static List<string> KVPHeaderOrder = new List<string>();

            public string Export(bool ExportHeader)
            {
                char delim = '\t';
                StringBuilder SB = new StringBuilder(); string res;
                SB.Append((ExportHeader ? "PlateID" : PlateID) + delim);

                if (KVPHeaderOrder.Count == 0) { foreach (KeyValuePair<string, object> KVP in KVPs) KVPHeaderOrder.Add(KVP.Key); }
                foreach (string Header in KVPHeaderOrder)
                {
                    res = KVPs.ContainsKey(Header) ? KVPs[Header].ToString() : "";
                    SB.Append((ExportHeader ? Header : res) + delim);
                    //SB.Append((ExportHeader ? KVP.Key : KVP.Value.ToString()) + delim);
                }
                XDCE_ImageGroup Well = Parent.Wells[WellLabel];
                SB.Append((ExportHeader ? "RaftCal" : Well.HasRaftCalibration.ToString()) + delim);
                SB.Append((ExportHeader ? "ImageCheck" : Well.HasImageCheck.ToString()) + delim);
                return SB.ToString();
            }

            internal bool ContainsPlateCoordinate(double plateX, double plateY)
            {
                //Check to see if this plate coordinate is inside the image
                if ((plateX >= this.PlateX_um) && (plateX <= (this.PlateX_um + this.Width_microns)))
                {
                    if ((plateY >= this.PlateY_um) && (plateY <= (this.PlateY_um + this.Height_microns)))
                    {
                        return true;
                    }
                }
                return false;
            }

            public PointF PlateCoordinates_um_InCenterOfRectangle(RectangleF FractionalRectangleOfThisImageInPixelCoordinates)
            {
                SizeF umPerPixelMultiplier = new SizeF(1, 1);
                return PlateCoordinates_um_InCenterOfRectangle(FractionalRectangleOfThisImageInPixelCoordinates, umPerPixelMultiplier);
            }

            public PointF PlateCoordinates_um_InCenterOfRectangle(RectangleF FractionalRectangleOfThisImageInPixelCoordinates, SizeF umPerPixelMultiplier)
            {
                RectangleF R = FractionalRectangleOfThisImageInPixelCoordinates;
                PointF Middle = new PointF(R.X + (R.Width / 2), R.Y + (R.Height / 2));
                return new PointF(
                    (float)(PlateX_um + Parent.PixelWidth_in_um * Middle.X / umPerPixelMultiplier.Width),
                    (float)(PlateY_um + Parent.PixelWidth_in_um * Middle.Y / umPerPixelMultiplier.Height));
            }

            public void RefreshRafts(XDCE_ImageGroup xParent, Dictionary<string, XDCE_ImageGroup> ParentRaftsInternal)
            {   //Updates 2023/03/22 by Willie
                ReturnRaft RR;
                if (!xParent.HasRaftCalibration)
                {
                    //Special Regions Case
                    foreach (var Region in Rafts.Values)
                    {
                        if (!ParentRaftsInternal.ContainsKey(Region.RaftID))
                        {
                            var SIG = new XDCE_ImageGroup();
                            SIG.FilePath = xParent.FilePath; SIG.Level = "Rafts";
                            SIG.NameAtLevel = Region.RaftID;
                            SIG.ParentFolder = xParent.ParentFolder;
                            ParentRaftsInternal.Add(Region.RaftID, SIG);
                        }
                        ParentRaftsInternal[Region.RaftID].Add(this);

                        //var xi_RI = new xI_RaftInfo(R2.Item1, R2.Item2, R2.Item3, R2.Item4);
                        //if (!this.Rafts.ContainsKey(xi_RI.RaftID)) this.Rafts.Add(xi_RI.RaftID, xi_RI); //Updated 5/24/2022
                    }
                    return;
                }

                // Local function to get the RR object
                (ReturnRaft RR, int row, int col) GetRRAndIndices(double fracXa, double fracYa)
                {
                    this.PlateCoordinates_FromFraction(fracXa, fracYa, out double fullXa, out double fullYa);
                    var RRa = xParent.CalibrationRaftSettings.FindRaftID_RR(fullXa, fullYa);
                    return (RRa, RRa.RaftRowIdx, RRa.RaftColIdx);
                }

                // Get RR objects and row and column indices for each corner combination
                var (RR_UpperLeft, upperLeftRow, upperLeftCol) = GetRRAndIndices(0, 0);
                var (RR_UpperRight, _, upperRightCol) = GetRRAndIndices(0, 1);
                var (RR_LowerLeft, lowerLeftRow, _) = GetRRAndIndices(1, 0);

                int numRaftsBetween = Math.Abs(upperRightCol - upperLeftCol) + 1;
                int numRaftsDown = Math.Abs(lowerLeftRow - upperLeftRow) + 1;

                // Get the center coordinates of the corner rafts
                PointF upperLeftCenter = RR_UpperLeft.RaftCenterFull;
                PointF upperRightCenter = RR_UpperRight.RaftCenterFull;
                PointF lowerLeftCenter = RR_LowerLeft.RaftCenterFull;

                List<float> centerYList = new List<float>(); // Create a list of center X, Y coordinates for the row,col
                List<float> centerXList = new List<float>();
                for (int i = 0; i < numRaftsBetween; i++)
                {
                    float centerY = upperLeftCenter.Y + i * (upperRightCenter.Y - upperLeftCenter.Y) / (numRaftsBetween - 1);
                    centerYList.Add(centerY);
                }
                for (int i = 0; i < numRaftsDown; i++)
                {
                    float centerX = upperLeftCenter.X + i * (lowerLeftCenter.X - upperLeftCenter.X) / (numRaftsDown - 1);
                    centerXList.Add(centerX);
                }

                this.RaftCalCode = xParent.CalibrationRaftSettings.CalCode;
                var RaftWithin = new Dictionary<string, Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>>();
                foreach (var xCenter in centerXList)
                {
                    foreach (var yCenter in centerYList)
                    {
                        RR = xParent.CalibrationRaftSettings.FindRaftID_RR(xCenter, yCenter);
                        var P1 = this.Fraction_FromPlateCoordinates(new PointF(xCenter, yCenter));
                        //var P2 = new PointF((float)(xCenter - upperLeftCenter.X) / (lowerLeftCenter.X - upperLeftCenter.X),(float)(upperRightCenter.Y - yCenter) / (upperRightCenter.Y - upperLeftCenter.Y));
                        RaftWithin.Add(RR.RaftID, new Tuple<FOVtoRaftID.ReturnRaft, double, double, PointF>(RR, xCenter, yCenter, P1));
                    }
                }
                foreach (var R2 in RaftWithin.Values)
                {
                    if (ParentRaftsInternal != null)
                    {
                        if (!ParentRaftsInternal.ContainsKey(R2.Item1.RaftID))
                        {
                            var SIG = new XDCE_ImageGroup();
                            SIG.FilePath = xParent.FilePath; SIG.Level = "Rafts";
                            SIG.NameAtLevel = R2.Item1.RaftID;
                            SIG.ParentFolder = xParent.ParentFolder;
                            ParentRaftsInternal.Add(R2.Item1.RaftID, SIG);
                        }
                        ParentRaftsInternal[R2.Item1.RaftID].Add(this);
                    }
                    var xi_RI = new xI_RaftInfo(R2.Item1, R2.Item2, R2.Item3, R2.Item4);
                    if (!this.Rafts.ContainsKey(xi_RI.RaftID)) this.Rafts.Add(xi_RI.RaftID, xi_RI); //Updated 5/24/2022
                }
            }

            /// <summary>
            /// Intended to be used when reconstructing at a different magnification
            /// </summary>
            public static XDCE_Image NewBasedOn(XDCE_ImageGroup NewParent, XDCE_Image RelatedImage, int FOV, RectangleF newFOVRect_um)
            {
                string newName = RelatedImage.SimilarName(FOV);
                string NewImagePath = Path.Combine(NewParent.ParentFolder.FullPath, newName);
                var xI = new XDCE_Image(NewImagePath, NewParent.ParentFolder.XDCE, NewParent.NameAtLevel, FOV, (short)RelatedImage.wavelength, newFOVRect_um, RelatedImage);
                NewParent.Add(xI, false);

                return xI;
            }
            Regex patttern = new Regex(@"\(FLD \d+", RegexOptions.Compiled);
            public string SimilarName(int newFOV)
            {
                string replacement = "(FLD " + (newFOV + 1).ToString("000"); // your new number goes here
                string nName = patttern.Replace(FileName, replacement);

                //string nName = this.FileName.Replace("(FLD " + (FOV + 1).ToString("000") + ")", "(FLD " + (newFOV + 1).ToString("000") + ")");
                //nName = this.FileName.Replace("(FLD " + (FOV + 1).ToString("00") + ")", "(FLD " + (newFOV + 1).ToString("00") + ")");
                return nName;
            }
        }

        public class XDCE_ImageGroup
        {
            public XDCE_ImageGroup()
            {

            }

            public string FolderPath = "";
            public string FilePath = ""; //XDCE Path
            public string Level = "";
            public string NameAtLevel = "";

            [XmlIgnore]
            public INCELL_Folder ParentFolder { get; set; }

            protected List<XDCE_Image> _Images;
            [XmlIgnore]
            public virtual List<XDCE_Image> Images { get { return _Images; } set { _Images = value; Dirty = true; } }

            public void Add(XDCE_Image Img, bool makeDirty = true)
            {
                if (_Images == null) _Images = new List<XDCE_Image>();
                _Images.Add(Img);
                if (makeDirty) MakeDirty();
            }

            private static Random _Rand = new Random();

            /// <summary>
            /// Picks out a random set of FOV #s from this plate's set of fields
            /// </summary>
            /// <param name="FOV_Count"></param>
            /// <returns></returns>
            public List<int> RandomFields(int FOV_Count, bool UseMaskFields = false)
            {
                XDCE_ImageGroup Fields = this;
                HashSet<int> FOVs_Picked = new HashSet<int>();
                HashSet<int> MaskFieldsHash = new HashSet<int>();
                if (UseMaskFields) MaskFieldsHash = new HashSet<int>(MaskFields.Select(x => x.FOV));
                int MaxFields = Math.Min(Fields.FOV_Max - Fields.FOV_Min, UseMaskFields ? MaskFields.Count : Fields.FOV_Max);
                while (FOVs_Picked.Count < Math.Min(FOV_Count, MaxFields))
                {
                    int FOV = _Rand.Next(Fields.FOV_Min, Fields.FOV_Max);
                    if (UseMaskFields)
                    {
                        if (!MaskFieldsHash.Contains(FOV)) continue;
                    }
                    FOVs_Picked.Add(FOV);
                }
                return FOVs_Picked.ToList();
            }

            public string PlateID { get => ParentFolder.PlateID; }

            public XDCE_ImageGroup WellPrevious(XDCE_ImageGroup Parent, string PlateID)
            {
                List<string> WellList = Parent.Wells.Keys.ToList();
                int idx = WellList.IndexOf(this.NameAtLevel);
                if (idx > 0) idx--;
                return Parent.Wells[WellList[idx]];
            }

            public XDCE_ImageGroup WellNext(XDCE_ImageGroup Parent, string PlateID)
            {
                List<string> WellList = Parent.Wells.Keys.ToList();
                int idx = WellList.IndexOf(this.NameAtLevel);
                if (idx < WellList.Count - 1) idx++;
                return Parent.Wells[WellList[idx]];
            }

            public void Refresh_Wells_Rafts(int grid = 21)
            {
                if (DirtyWells)
                {
                    _Wells = new Dictionary<string, XDCE_ImageGroup>();
                    _Rafts = new Dictionary<string, XDCE_ImageGroup>();
                    _ImageByName = new Dictionary<string, XDCE_Image>();
                    PathSearchMasks = Path.Combine(Path.GetDirectoryName(FilePath), INCELL_Folder.DefaultMaskFolderName, INCELL_Folder.DefaultMaskListName);
                    INCELL_Folder.ErrorLog = PathSearchMasks + "\r\n";
                    _MaskFields = Masks.MaskList.XDCEImages_from_MaskList(this, PathSearchMasks);
                    INCELL_Folder.ErrorLog += "\r\nMaskFields Count = " + _MaskFields.Count + "\r\n";
                    _MaskWells = _MaskFields.Select(x => x.WellLabel).Distinct().ToList();
                    if (Images == null) return;
                    foreach (XDCE_Image Img in Images)
                    {
                        if (!_Wells.ContainsKey(Img.WellLabel))
                        {
                            var SIG = new XDCE_ImageGroup();
                            SIG.FilePath = this.FilePath; SIG.Level = "Wells";
                            SIG.NameAtLevel = Img.WellLabel;
                            SIG.ParentFolder = ParentFolder;
                            _Wells.Add(Img.WellLabel, SIG);
                        }
                        _Wells[Img.WellLabel].Add(Img);
                        _ImageByName.Add(Path.GetFileNameWithoutExtension(Img.FileName.ToUpper()), Img);
                    }
                    foreach (var well in _Wells) well.Value.LoadWellRegions(); //Load regions if they exist
                    DirtyWells = false; //This was set to true 2/23/2021
                }
                //if (HasRaftCalibration) //11/2023 turned this off since we can now have rafts WITHOUT calibration
                if (DirtyRafts)
                {
                    //100x100 rafts have about 6 rafts per FOV, so that is why this is set to 21, we want multiple points so we can have one that is near the center
                    //int test = 0;
                    //grid = 5;
                    foreach (XDCE_Image Img in Images)
                    {
                        Img.RefreshRafts(this, _Rafts);
                        //if (test++ > 95) break;
                    }
                    DirtyRafts = false; //21=400, 
                }
            }

            protected Dictionary<string, XDCE_ImageGroup> _Wells;
            [XmlIgnore]
            public Dictionary<string, XDCE_ImageGroup> Wells
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _Wells;
                }
            }

            /// <summary>
            /// Only intended for 
            /// </summary>
            public void AddWellClean(XDCE_ImageGroup wellNew)
            {
                if (_Wells == null) _Wells = new Dictionary<string, XDCE_ImageGroup>();
                _Wells.Add(wellNew.NameAtLevel, wellNew);
                DirtyWells = false;
            }

            protected Dictionary<string, XDCE_ImageGroup> _Rafts;
            [XmlIgnore]
            public Dictionary<string, XDCE_ImageGroup> Rafts
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _Rafts;
                }
            }

            private int _Row_Count = -1, _Col_Count = -1;
            public int Row_Count { get { Check_XDCEImageGroup(); return _Row_Count; } } //Y
            public int Col_Count { get { Check_XDCEImageGroup(); return _Col_Count; } } //X

            protected int _FOV_Min;
            public int FOV_Min { get { Check_XDCEImageGroup(); return _FOV_Min; } }
            protected int _FOV_Max;
            public int FOV_Max { get { Check_XDCEImageGroup(); return _FOV_Max; } }

            protected ConcurrentDictionary<string, XDCE_ImageGroup> _WellFOV_Dict;

            protected Dictionary<string, List<XDCE_Image>> _WellFOV_to_Index;
            [XmlIgnore]
            public Dictionary<string, List<XDCE_Image>> WellFOV_to_Index { get { Check_XDCEImageGroup(); return _WellFOV_to_Index; } }

            protected SortedList<double, int> _X_to_Index;
            protected SortedList<double, int> _Y_to_Index;
            [XmlIgnore]
            public SortedList<double, int> X_to_Index { get { Check_XDCEImageGroup(); return _X_to_Index; } }
            [XmlIgnore]
            public SortedList<double, int> Y_to_Index { get { Check_XDCEImageGroup(); return _Y_to_Index; } }

            protected Dictionary<Tuple<int, int>, List<XDCE_Image>> _ImageByIndices;

            protected Dictionary<string, XDCE_Image> _ImageByName;
            [XmlIgnore]
            public Dictionary<string, XDCE_Image> ImageByName
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _ImageByName;
                }
            }

            public int Count { get { Check_XDCEImageGroup(); return Images.Count; } }

            protected void MakeDirty()
            {
                Dirty = DirtyWells = DirtyRafts = true;
            }

            protected bool Dirty = true;
            protected bool DirtyWells = true;
            protected bool DirtyRafts = true;

            public XDCE_Image GetImage(string FileName)
            {
                string Key = Path.GetFileNameWithoutExtension(FileName.ToUpper());
                if (ImageByName.ContainsKey(Key)) return ImageByName[Key];
                return null;
            }
            public XDCE_Image GetImage(int Row, int Col, int WavelenthIndex, int TimeSeriesOrder = 1)
            {
                Check_XDCEImageGroup();
                var Key = new Tuple<int, int>(Col, Row);

                if (!_ImageByIndices.ContainsKey(Key)) return null;
                List<XDCE_Image> Imgs = _ImageByIndices[Key];

                // Filter images based on both WavelengthIndex and TimeSeriesOrder
                var matchingImage = Imgs.FirstOrDefault(img => img.wavelength == WavelenthIndex && img.TimeSeriesOrder == TimeSeriesOrder);

                return matchingImage;
            }

            public XDCE_Image GetImage_FromWellFOV(string Well_Field)
            {
                List<XDCE_Image> xIs = GetImages_FromWellFOV(Well_Field);
                if (xIs == null) return null;
                return xIs[0];
            }

            public List<XDCE_Image> GetImages_FromWellFOV(string Well_Field)
            {
                Check_XDCEImageGroup();
                if (!WellFOV_to_Index.ContainsKey(Well_Field)) return null;
                List<XDCE_Image> xImages = WellFOV_to_Index[Well_Field];
                return xImages;
            }

            internal List<XDCE_Image> GetAdjacent(int FOVIndex_x, int FOVIndex_y, int Offset_x, int Offset_y)
            {
                Tuple<int, int> Key = new Tuple<int, int>(FOVIndex_x + Offset_x, FOVIndex_y + Offset_y);
                if (!_ImageByIndices.ContainsKey(Key)) return new List<XDCE_Image>();
                return _ImageByIndices[Key];
            }

            protected short _Wavelength_Count;
            [XmlIgnore]
            public short Wavelength_Count
            {
                get
                {
                    Check_XDCEImageGroup();
                    return _Wavelength_Count;
                }
                set { _Wavelength_Count = value; }
            }

            public IEnumerable<string> WellFOV_Set { get => WellFOV_to_Index.Keys; }

            [XmlIgnore]
            public ConcurrentDictionary<string, XDCE_ImageGroup> WellFOV_Dict { get { Check_XDCEImageGroup(); return _WellFOV_Dict; } }

            private double XY_Basket_Threshold;

            private void _FillOutXYGrid()
            {
                var xBaskets = new SortedList<double, List<double>>(Images.Count);
                var yBaskets = new SortedList<double, List<double>>(Images.Count);
                try
                {
                    XY_Basket_Threshold = Images[0].Width_microns / 4;
                }
                catch
                {
                    XY_Basket_Threshold = 10;
                }

                foreach (var img in Images)
                {
                    AddToBasket(xBaskets, img.PlateX_um);
                    AddToBasket(yBaskets, img.PlateY_um);
                }

                _X_to_Index = CreateIndexMapping(xBaskets);
                _Y_to_Index = CreateIndexMapping(yBaskets);
            }

            private void AddToBasket(SortedList<double, List<double>> baskets, double coordinate)
            {

                double? closestKey = null;
                double minDistance = double.MaxValue;
                //Binary Search 
                int low = 0, high = baskets.Count - 1;
                while (low <= high)
                {
                    int mid = low + (high - low) / 2;
                    double key = baskets.Keys[mid];
                    double distance = Math.Abs(key - coordinate);

                    if (distance < minDistance && distance <= XY_Basket_Threshold)
                    {
                        minDistance = distance; closestKey = key;
                    }
                    if (key < coordinate) low = mid + 1;
                    else if (key > coordinate) high = mid - 1;
                    else break;
                }
                if (closestKey.HasValue) baskets[closestKey.Value].Add(coordinate);
                else baskets[coordinate] = new List<double> { coordinate };
            }

            private SortedList<double, int> CreateIndexMapping(SortedList<double, List<double>> baskets)
            {
                var mapping = new SortedList<double, int>(baskets.Count);
                var uniqueRepresentatives = new HashSet<double>(baskets.Count);
                int index = 0;

                foreach (KeyValuePair<double, List<double>> basket in baskets)
                {
                    double representative = Math.Round(basket.Value.Average()); //This can be changed but rounding to 3 is to get rid of the possibility of bad math that comes from huge decimal numbers
                    // Ensure uniqueness of the representative value
                    double uniquenessFactor = 0.0000001; // A small value to adjust for uniqueness
                    while (uniqueRepresentatives.Contains(representative))
                        representative += uniquenessFactor; // Adjust the value slightly to make it unique
                    uniqueRepresentatives.Add(representative);

                    foreach (double value in basket.Value) mapping[value] = index;
                    index++;
                }

                return mapping;
            }

            private class InvertedComparer : IComparer<double>
            {
                public int Compare(double x, double y) { return y.CompareTo(x); }
            }



            protected void Check_XDCEImageGroup()
            {
                if (!Dirty) return;
                if (_Images == null) Refresh_Wells_Rafts(); //Added 8/23/2021
                if (_Images == null) return; //8/23/2021
                if (ParentFolder.Type_NoXDCE & _Images.Count == 0 && Wells.Count > 0) _Images = Wells.First().Value.Images; //This is mostly for the case where there is no XDCE
                //if we are checking the Image Group, need to Reassign TimeSeriesOrder.
                if (_Images != null && _Images.Any(t => t.FileName[0] == 't' || t.FileName[0] == 'T' && t.FileName[1] == '2'))
                {
                    _Images.ForEach(t =>
                    {
                        if (t.FileName.StartsWith("T", StringComparison.OrdinalIgnoreCase)
                        && int.TryParse(t.FileName[1..t.FileName.IndexOf('_')], out int order))
                        {
                            t.TimeSeriesOrder = order;
                            t.IsTimeSeries = true;
                        }
                    });
                }
                _FillOutXYGrid(); //This ensures that the pattern of the scan and the grid are setup correctly
                _WellFOV_to_Index = new Dictionary<string, List<XDCE_Image>>();
                _WellFOV_Dict = new ConcurrentDictionary<string, XDCE_ImageGroup>(); //Ideally we will only keep this one and get rid of the Well_FOV_toindex one . . 
                _ImageByIndices = new Dictionary<Tuple<int, int>, List<XDCE_Image>>();
                _Wavelength_Count = 0; _FOV_Max = int.MinValue; _FOV_Min = int.MaxValue;
                var _RowHash = new HashSet<int>(); var _ColHash = new HashSet<int>();

                foreach (XDCE_Image Image in _Images)
                {
                    var Key = new Tuple<int, int>(_X_to_Index[Image.PlateX_um], _Y_to_Index[Image.PlateY_um]);
                    if (!_ImageByIndices.ContainsKey(Key)) _ImageByIndices.Add(Key, new List<XDCE_Image>());
                    _ImageByIndices[Key].Add(Image);
                    _RowHash.Add(Key.Item2); _ColHash.Add(Key.Item1);

                    string WellField = Image.WellField;
                    if (!_WellFOV_to_Index.ContainsKey(WellField)) _WellFOV_to_Index.Add(WellField, new List<XDCE_Image>());
                    _WellFOV_to_Index[WellField].Add(Image);
                    if (!_WellFOV_Dict.ContainsKey(WellField))
                    { //Ideally we will only keep this one and get rid of the Well_FOV_toindex one . . 
                        var SIG = new XDCE_ImageGroup(); SIG.FilePath = this.FilePath; SIG.Level = "Field";
                        SIG.NameAtLevel = WellField; SIG.ParentFolder = ParentFolder; _WellFOV_Dict.TryAdd(WellField, SIG);
                    }
                    _WellFOV_Dict[WellField].Add(Image);

                    if (_ImageByIndices[Key].Count > _Wavelength_Count) _Wavelength_Count = (short)_ImageByIndices[Key].Count;
                    if (Image.FOV < _FOV_Min) _FOV_Min = Image.FOV;
                    if (Image.FOV > _FOV_Max) _FOV_Max = Image.FOV;
                }
                _Row_Count = _RowHash.Count(); _Col_Count = _ColHash.Count();
                Dirty = false; //Can't use things directly until after this is called!!
            }

            public List<XDCE_Image> GetClosestFields(double PlateX, double PlateY)
            {
                double X1, X2, Y1, Y2;
                ClosestPlateCoordinate(PlateX, true, out X1, out X2);
                ClosestPlateCoordinate(PlateY, false, out Y1, out Y2);
                var Xs = new List<double>(); if (!double.IsNaN(X1)) Xs.Add(X1); if (!double.IsNaN(X2)) Xs.Add(X2);
                var Ys = new List<double>(); if (!double.IsNaN(Y1)) Ys.Add(Y1); if (!double.IsNaN(Y2)) Ys.Add(Y2);

                //Now that we have the closest, we can see which of these actually CONTAIN the point of interest
                var SetOfImages = new List<List<XDCE_Image>>(); string FOVs = "";
                foreach (double X in Xs)
                {
                    foreach (double Y in Ys)
                    {
                        var Key = new Tuple<int, int>(_X_to_Index[X], _Y_to_Index[Y]);
                        if (_ImageByIndices.ContainsKey(Key))
                        {
                            List<XDCE_Image> IMGS = _ImageByIndices[Key];
                            FOVs += ", " + IMGS[0].FOV; //Just for testing
                            if (IMGS[0].ContainsPlateCoordinate(PlateX, PlateY))
                            {
                                SetOfImages.Add(IMGS);
                            }
                        }
                        //Since we sorted these to do the best matches first, we could return and not check the other positions . . 
                    }
                }

                //Here we should check to see if one of them has it on the edge. Keep the image that has the point closest to the middle
                if (SetOfImages.Count > 1)
                {
                    Debugger.Break();
                }
                return SetOfImages.Count == 0 ? null : SetOfImages[0];
            }

            public List<Tuple<List<XDCE_Image>, string>> GetClosestFieldsForLiveTile(double PlateX, double PlateY, double LEwidth_um)
            {
                double X1, X2, Y1, Y2, Xreal = 0, Yreal = 0;
                ClosestPlateCoordinate(PlateX, true, out X1, out X2);
                ClosestPlateCoordinate(PlateY, false, out Y1, out Y2);
                var SetOfImages = new List<Tuple<List<XDCE_Image>, string>>(); string FOVs = "";
                if ((PlateX - X1) > (LEwidth_um / 2) || (PlateY - Y1) > (LEwidth_um / 2))
                {
                    return SetOfImages;
                }
                var Xs = new List<double>(); if (!double.IsNaN(X1)) Xs.Add(X1); if (!double.IsNaN(X2)) Xs.Add(X2);
                var Ys = new List<double>(); if (!double.IsNaN(Y1)) Ys.Add(Y1); if (!double.IsNaN(Y2)) Ys.Add(Y2);

                //Now that we have the closest, we can see which of these actually CONTAIN the point of interest
                foreach (double X in Xs)
                {
                    foreach (double Y in Ys)
                    {
                        var Key = new Tuple<int, int>(_X_to_Index[X], _Y_to_Index[Y]);
                        if (_ImageByIndices.ContainsKey(Key))
                        {
                            List<XDCE_Image> IMGS = _ImageByIndices[Key];
                            FOVs += ", " + IMGS[0].FOV; //Just for testing
                            if (IMGS[0].ContainsPlateCoordinate(PlateX, PlateY))
                            {
                                Xreal = X; Yreal = Y;
                                if (!SetOfImages.Any(v => v.Item1.SequenceEqual(IMGS)))
                                {
                                    SetOfImages.Add(new Tuple<List<XDCE_Image>, string>(IMGS, "C"));
                                }
                            }
                        }
                        //Since we sorted these to do the best matches first, we could return and not check the other positions . . 
                    }
                }
                if (SetOfImages.Count > 0)
                {
                    X1 = Xreal; Y1 = Yreal;
                }
                var results = FindSurroundingValidImages(_X_to_Index[X1], _Y_to_Index[Y1]);
                if (results != null && results.Count > 0)
                {
                    foreach (var result in results)
                    {
                        if (!SetOfImages.Any(v => v.Item1.SequenceEqual(result.Item1)))
                        {
                            SetOfImages.Add(result);
                        }
                    }
                }
                return SetOfImages;


                List<Tuple<List<XDCE_Image>, string>> FindSurroundingValidImages(int xIndex, int yIndex)
                {
                    var surroundingKeys = new List<Tuple<int, int, string>>
                    {

                        new Tuple<int, int, string>(xIndex - 1, yIndex, "L"), // left
                        new Tuple<int, int, string>(xIndex + 1, yIndex, "R"), // right
                        new Tuple<int, int, string>(xIndex, yIndex - 1, "U"), // above
                        new Tuple<int, int, string>(xIndex, yIndex + 1, "D"),  // below
                        new Tuple<int, int, string>(xIndex - 1, yIndex - 1, "UL"),
                        new Tuple<int, int, string>(xIndex + 1, yIndex - 1, "UR"),
                        new Tuple<int, int, string>(xIndex - 1, yIndex + 1, "DL"),
                        new Tuple<int, int, string>(xIndex - 1, yIndex + 1, "DR")
                    };

                    var validImages = new List<Tuple<List<XDCE_Image>, string>>();

                    foreach (var vals in surroundingKeys)
                    {
                        var key = new Tuple<int, int>(vals.Item1, vals.Item2);
                        if (_ImageByIndices.ContainsKey(key) && _ImageByIndices[key] != null && _ImageByIndices[key].Count > 0)
                        {
                            var imageList = _ImageByIndices[key];

                            if (!validImages.Any(v => v.Item1.SequenceEqual(imageList)))
                            {
                                validImages.Add(new Tuple<List<XDCE_Image>, string>(imageList, vals.Item3));
                            }
                        }
                    }

                    return validImages; // return list of valid images 
                }
            }

            public void ClosestPlateCoordinate(double target, bool searchX,
                                              out double closest, out double nextClosest, bool allowExactMatch = true)
            {
                closest = nextClosest = double.NaN;
           

                // 1. Build/Sort the array of coordinates
                double[]  array = searchX ? _X_to_Index.Keys.ToArray() : Y_to_Index.Keys.ToArray();

                // If empty or single-element, handle trivial cases
                if (array.Length == 0) return;

                if (array.Length == 1)
                {
                    closest = array[0];
                    return;
                }

                // Handle the edge cases where the target is at the bounds
                if (array[^1] == target)
                {
                    closest = array[^1];
                    nextClosest = array[^2];
                    return;
                }

                if (array[0] == target)
                {
                    closest = array[0];
                    nextClosest = array[1];
                    return;
                }
                // 2. Use Array.BinarySearch to get insertion index
                int idx = Array.BinarySearch(array, target);

                // If not found, ~idx is insertion index.
                if (idx < 0)
                {
                    idx = ~idx; // insertion index
                }

                List<int> candidateIndices = new List<int>();
                for (int offset = -2; offset <= 2; offset++)
                {
                    int candidateIndex = idx + offset;
                    if (candidateIndex >= 0 && candidateIndex < array.Length)
                    {
                        candidateIndices.Add(candidateIndex);
                    }
                }

                // 4. Compute distance from target for each candidate by getting Euclidean Distance.
                var candidates = candidateIndices
                               .Select(i => new { Value = array[i], Dist = Math.Abs(array[i] - target) })
                               .OrderBy(c => c.Dist)
                               .ThenBy(c => c.Value) // Optional tie-break by value
                               .Take(2)             // Get the two best
                               .ToList();
                if (!allowExactMatch)
                { 
                     candidates = candidateIndices
                     .Select(i => new { Value = array[i], Dist = Math.Abs(array[i] - target) })
                     .Where(t => t.Value != target)
                     .OrderBy(c => c.Dist)
                     .ThenBy(c => c.Value) // Optional tie-break by value
                     .Take(2)             // Get the two best
                     .ToList();
                }
                if (candidates.Count == 0)
                {
                    return;
                }
                else if (candidates.Count == 1)
                {
                    closest = candidates[0].Value;
                    return;
                }
                else
                {
                    closest = candidates[0].Value;
                    nextClosest = candidates[1].Value;
                    return;
                }
            }

            //The Default value when Timeseries is Initialized is 1 so this is backwards compatible.



            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public List<XDCE_Image> GetFields(int FOV, int TimeSeriesOrder = 1)
            {
                List<XDCE_Image> FieldImages = new List<XDCE_Image>(9);
                foreach (XDCE_Image image in Images)
                {
                    if (image.FOV == FOV && image.TimeSeriesOrder == TimeSeriesOrder)
                    {
                        FieldImages.Add(image);
                    }
                }
                return FieldImages;

            }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public List<XDCE_Image> GetFields(IEnumerable<int> FOV, int TimeSeriesOrder = 1)
            {
                List<XDCE_Image> FieldImages = new List<XDCE_Image>(17);
                HashSet<int> Set = new HashSet<int>(FOV);

                for (int i = 0; i < _Images.Count; i++)
                {
                    XDCE_Image image = _Images[i];
                    if (Set.Contains(image.FOV) && image.TimeSeriesOrder == TimeSeriesOrder)
                    {
                        FieldImages.Add(image);
                    }
                }

                return FieldImages;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public XDCE_Image GetField(int FOV, short wavelength, int TimeSeriesOrder = 1)
            {
                List<XDCE_Image> FieldImages = GetFields(FOV, TimeSeriesOrder);
                if (FieldImages.Count < 1) return null;

                try
                {
                    // Filter by both wavelength and TimeSeriesOrder
                    return FieldImages.FirstOrDefault(x => x.wavelength == wavelength && x.TimeSeriesOrder == TimeSeriesOrder);
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// Only intended to be used for reconstruction at a new magnification
            /// </summary>
            public static XDCE_ImageGroup CopyExisting(XDCE_ImageGroup Original_XIG, INCELL_Folder NewFolder, float Magnification_New)
            {
                var tXIG = new XDCE_ImageGroup();
                tXIG.ParentFolder = NewFolder;
                tXIG.NameAtLevel = Original_XIG.NameAtLevel;
                tXIG.Level = Original_XIG.Level;
                return tXIG;
            }

            public string WellRegionFileName => "Regions_" + Images[0].WellLabel + ".json";
            public string WellRegionFilePath { get => Path.Combine(ParentFolder.XDCE.FolderPath, WellRegionFileName); }

            public void LoadWellRegions()
            {
                if (!File.Exists(WellRegionFilePath)) return;

                var ToLoad = new Dictionary<string, Dictionary<string, xI_RaftInfo>>();
                string json = File.ReadAllText(WellRegionFilePath);
                ToLoad = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, xI_RaftInfo>>>(json);

                foreach (var img in Images)
                {
                    if (ToLoad.ContainsKey(img.WellField))
                    {
                        img.Rafts = ToLoad[img.WellField];
                    }
                }
            }

            public void SaveWellRegions()
            {
                var well = this.ParentFolder.XDCE.Wells[this.Images[0].WellLabel];

                //First construct all the regions
                var ToSave = new Dictionary<string, Dictionary<string, xI_RaftInfo>>();
                foreach (var img in well.Images)
                {
                    if (img.Rafts.Count > 0 && !ToSave.ContainsKey(img.WellField))
                        ToSave.Add(img.WellField, img.Rafts);
                }

                //Then Serialize and save
                string js = JsonSerializer.Serialize(ToSave);
                File.WriteAllText(WellRegionFilePath, js);

                //Alternative approach - - - - - - - 
                //var x = new XmlSerializer(tRaft.GetType());
                //var stringBuilder = new StringBuilder();
                //using (var stringWriter = new StringWriter(stringBuilder))
                //{
                //    x.Serialize(stringWriter, tRaft);
                //    string xml = stringWriter.ToString();
                //    // Now xml contains the XML representation of your object
                //}
            }

            private bool _CheckedForCalSettings = false; private bool _CheckForPointsList = false;

            private ImageCheck.ImageCheck_PointList _ImageCheck_PointList = null;
            [XmlIgnore]
            public ImageCheck.ImageCheck_PointList ImageCheck_PointsList
            {
                get
                {
                    if (_ImageCheck_PointList == null && _CheckForPointsList == false)
                    {
                        //Look for the newest Calibration file in the folder, start with the standard name
                        //var FI = new FileInfo(FilePath);
                        var FI = new FileInfo(this.ParentFolder.FullPath);

                        _ImageCheck_PointList = ImageCheck.ImageCheck_PointList.Load_Assumed(FI.FullName, this.NameAtLevel);
                        //_ImageCheck_PointList = ImageCheck.ImageCheck_PointList.Load_Assumed(FI.DirectoryName, this.NameAtLevel);

                        _CheckForPointsList = true;
                    }
                    return _ImageCheck_PointList;
                }
                set
                {
                    _ImageCheck_PointList = value;
                }
            }

            private FOVtoRaftID.CalibrationRafts _CalibrationRaftSettings = null;
            [XmlIgnore]
            public FOVtoRaftID.CalibrationRafts CalibrationRaftSettings
            {
                get
                {
                    if (_CalibrationRaftSettings == null && _CheckedForCalSettings == false)
                    {
                        //Look for the newest Calibration file in the folder, start with the standard name
                        FileInfo FI = new FileInfo(this.ParentFolder.FullPath);
                        //FileInfo FI = new FileInfo(FilePath);
                        _CalibrationRaftSettings = FOVtoRaftID.CalibrationRafts.Load_Assumed(FI.FullName, this.NameAtLevel);
                        //_CalibrationRaftSettings = FOVtoRaftID.CalibrationRafts.Load_Assumed(fi.DirectoryName, this.NameAtLevel);

                        _CheckedForCalSettings = true;
                        //if (_CalibrationRaftSettings != null) Refresh_Wells_Rafts(); //5/24/2022 - the problem with putting this here is it is really slowwwww
                    }
                    return _CalibrationRaftSettings;
                }
                set
                {
                    _CalibrationRaftSettings = value;
                    _CheckedForCalSettings = true;
                }
            }

            public bool HasRaftCalibration { get => CalibrationRaftSettings != null; }
            public bool HasImageCheck { get => ImageCheck_PointsList != null; }
            public string ImageCheckInfo
            {
                get
                {
                    if (ImageCheck_PointsList == null) return "!No Annotations";
                    else return ImageCheck_PointsList.Overview;
                }
            }
            public int FOVCount { get => FOV_Max - FOV_Min; }

            private List<XDCE_Image> _MaskFields = null;
            private List<string> _MaskWells = null;

            [XmlIgnore]
            public string PathSearchMasks;

            [XmlIgnore]
            public List<XDCE_Image> MaskFields
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _MaskFields;
                }
            }

            [XmlIgnore]
            public List<string> MaskWells
            {
                get
                {
                    Refresh_Wells_Rafts();
                    return _MaskWells;
                }
            }

            [XmlIgnore]
            public bool HasMasks { get => MaskFields.Count > 0; }
        }

        public class XDCE : XDCE_ImageGroup
        {
            #region Declares --------------------------- ----------------------------------------------------
            public bool Valid;

            private INCELL_MeasuredOffsets _MeasuredOffsets = new INCELL_MeasuredOffsets();
            [XmlIgnore]
            public INCELL_MeasuredOffsets MeasuredOffsets
            {
                get => _MeasuredOffsets;
                set
                {
                    _MeasuredOffsets = value;
                    //Possibly trigger recalculations

                    foreach (var xI in Images) xI.RecalcPlateCoordinates();
                }
            }


            [XmlIgnore]
            public override List<XDCE_Image> Images
            {
                get
                {
                    CheckLoad();
                    return base.Images;
                }
            }

            public static bool isZipped = false;

            private Dictionary<string, object> _KVPs;
            [XmlIgnore]
            public Dictionary<string, object> KVPs
            {
                get
                {
                    if (_KVPs == null)
                        LoadNow();
                    return _KVPs;
                }
                set
                {
                    _KVPs = value;
                }
            }
            public string NameString;

            public bool Loaded;
            public string timePointPaths { get; set; }
            public override string ToString() { return NameString; }

            private string _Plate_Name = "[-1]";
            [XmlIgnore]
            public string Plate_Name
            {
                get
                {
                    if (KVPs == null) return "-1";
                    if (_Plate_Name == "[-1]") _Plate_Name = (string)KVPs["PLATE_NAME"];
                    return _Plate_Name;
                }
                set
                {
                    _Plate_Name = value;

                    //Can't just do this since it gets set during load and ends up causing trouble. .
                    //need something to figure out whom is setting . . 4/11/2023

                    //KVPs["PLATE_NAME"] = value;
                }
            }
            private double _PixelHeight_in_um = double.NaN;
            [XmlIgnore]
            public double PixelHeight_in_um
            {
                get
                {
                    if (double.IsNaN(_PixelHeight_in_um))
                    {
                        if (KVPs == null) return double.NaN;
                        string res = (string)KVPs["OBJECTIVECALIBRATION_PIXEL_HEIGHT"];
                        _PixelHeight_in_um = double.Parse(res);
                    }
                    return _PixelHeight_in_um;
                }
                set
                {
                    _PixelHeight_in_um = value;
                    KVPs["OBJECTIVECALIBRATION_PIXEL_HEIGHT"] = value.ToString();
                }
            }
            private double _PixelWidth_in_um = double.NaN;
            [XmlIgnore]
            public double PixelWidth_in_um
            {
                get
                {
                    if (double.IsNaN(_PixelWidth_in_um))
                    {
                        if (KVPs == null) return double.NaN;
                        string res = (string)KVPs["OBJECTIVECALIBRATION_PIXEL_WIDTH"];
                        _PixelWidth_in_um = double.Parse(res);
                    }
                    return _PixelWidth_in_um;
                }
                set
                {
                    _PixelWidth_in_um = value;
                    KVPs["OBJECTIVECALIBRATION_PIXEL_WIDTH"] = value.ToString();
                }
            }
            private int _ImageWidth_in_Pixels = -1;
            [XmlIgnore]
            public int ImageWidth_in_Pixels
            {
                get
                {
                    if (_ImageWidth_in_Pixels == -1)
                    {
                        if (KVPs == null) return -1;
                        string res = (string)KVPs["SIZE_WIDTH"];
                        _ImageWidth_in_Pixels = int.Parse(res);
                    }
                    return _ImageWidth_in_Pixels;
                }
                set
                {
                    _ImageWidth_in_Pixels = value;
                    KVPs["SIZE_WIDTH"] = value.ToString();
                }
            }
            private int _ImageHeight_in_Pixels = -1;
            [XmlIgnore]
            public int ImageHeight_in_Pixels
            {
                get
                {
                    if (_ImageHeight_in_Pixels == -1)
                    {
                        if (KVPs == null) return -1;
                        string res = (string)KVPs["SIZE_HEIGHT"];
                        _ImageHeight_in_Pixels = int.Parse(res);
                    }
                    return _ImageHeight_in_Pixels;
                }
                set
                {
                    _ImageHeight_in_Pixels = value;
                    KVPs["SIZE_HEIGHT"] = value.ToString();
                }
            }

            private float _ObjectiveMagnification = float.NaN;
            [XmlIgnore]
            public float ObjectiveMagnification
            {
                get
                {
                    if (double.IsNaN(_ObjectiveMagnification))
                    {
                        if (KVPs == null) return -1;
                        string res = (string)KVPs["OBJECTIVECALIBRATION_MAGNIFICATION"];
                        _ObjectiveMagnification = float.Parse(res);
                    }
                    return _ObjectiveMagnification;
                }
                set
                {
                    _ObjectiveMagnification = value;
                    KVPs["OBJECTIVECALIBRATION_MAGNIFICATION"] = value.ToString();
                }
            }

            private double _PlateXMax_um;
            public double PlateXMax_um { get { CheckPlate_um(); return _PlateXMax_um; } }
            private double _PlateXMin_um;
            public double PlateXMin_um { get { CheckPlate_um(); return _PlateXMin_um; } }
            private double _PlateYMax_um;
            public double PlateYMax_um { get { CheckPlate_um(); return _PlateYMax_um; } }
            private double _PlateYMin_um;
            public double PlateYMin_um { get { CheckPlate_um(); return _PlateYMin_um; } }

            public string MaskFolder { get => Path.Combine(ParentFolder.FullPath, INCELL_Folder.DefaultMaskFolderName); }

            private bool CheckPlate_um_Dirty = true;
            private void CheckPlate_um()
            {
                if (!CheckPlate_um_Dirty) return;
                foreach (XDCE_Image Image in Images)
                {
                    //Fused Multiply Add increases efficiency and reduces error by doing this in one CPU Instruction.
                    double xMax = Math.FusedMultiplyAdd(PixelWidth_in_um, ImageWidth_in_Pixels, Image.PlateX_um);
                    double yMax = Math.FusedMultiplyAdd(PixelHeight_in_um , ImageHeight_in_Pixels, Image.PlateY_um);
                    if (xMax > _PlateXMax_um) _PlateXMax_um = xMax; if (Image.PlateX_um < _PlateXMin_um) _PlateXMin_um = Image.PlateX_um;
                    if (yMax > _PlateYMax_um) _PlateYMax_um = yMax; if (Image.PlateY_um < _PlateYMin_um) _PlateYMin_um = Image.PlateY_um;
                }
                CheckPlate_um_Dirty = false;
            }

            #endregion

            #region Create and Initiate ----------------------------------------------------------------------
            public XDCE(string FilePath, bool LoadNow, string folderPath = null)
            {
                Init(FilePath, LoadNow, folderPath);
            }

            public XDCE(string FilePath, string folderPath = null)
            {
                Init(FilePath, false, folderPath);
            }

            public XDCE()
            {
                Loaded = false;
            }

            public static string GetXDCEPath(string FolderPath, XDCE xdce = null)
            {
                isZipped = false;
                if (FolderPath.ToUpper().EndsWith(".XDCE")) return FolderPath;
                DirectoryInfo DI = new DirectoryInfo(FolderPath);
                if (!DI.Exists) return "";
                FileInfo[] files = DI.GetFiles("*.xdce");
                if (files.Length == 0)
                {
                    var potPath = findXDCEinZip(DI);
                    if (potPath != "")
                    {
                        isZipped = true;
                        return potPath;
                    }
                    else return "";
                }

                return files[0].FullName;
            }

            public static string findXDCEinZip(DirectoryInfo DI)
            {
                if (!DI.Exists) return "";
                FileInfo[] zipFiles = DI.GetFiles("*.zip", SearchOption.TopDirectoryOnly);
                if (zipFiles.Length > 0)
                {
                    string tempPath = Path.GetTempPath();
                    foreach (FileInfo zip in zipFiles)
                    {
                        using (ZipArchive archive = ZipFile.OpenRead(zip.FullName))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                if (entry.Name.ToUpper().EndsWith(".XDCE"))
                                {
                                    var outputDir = Path.Combine(DI.Parent.FullName, "XDCE_info", DI.Name);
                                    var fileName = DI.Name + ".xdce";
                                    var outputPath = Path.Combine(outputDir, fileName);

                                    Directory.CreateDirectory(outputDir);

                                    if (File.Exists(outputPath))
                                    {
                                        //File.Delete(outputPath);
                                        return outputPath;
                                    }

                                    entry.ExtractToFile(outputPath);
                                    return outputPath;
                                }

                            }
                        }
                    }
                }
                return "";
            }

            private void Init(string FilePath, bool LoadNow, string folder = null)
            {
                Level = "XDCE";
                Valid = false;
                Loaded = false;
                FilePath = GetXDCEPath(FilePath);
                this.FilePath = FilePath;
                this.FolderPath = folder == null ? Path.GetDirectoryName(this.FilePath) : folder;
                NameString = Path.GetFileNameWithoutExtension(FilePath);
                Valid = true;
                if (LoadNow) this.LoadNow();
            }

            public void CheckLoad()
            {
                if (!Loaded) LoadNow();
            }

            public void LoadNow()
            {
                base.Images = new List<XDCE_Image>();
                _PlateXMax_um = _PlateYMax_um = double.MinValue; _PlateXMin_um = _PlateYMin_um = double.MaxValue;

                if (!Valid) return;

                string tLine; StringBuilder sB; XDCE_Image xImage; int idx = 0; var sB_Initial = new StringBuilder();
                FilePath = FilePath.ToUpper();
                var FI = new FileInfo(FilePath);
                if (!FI.Exists)
                {
                    //Try the adjusted Path
                    if (FilePath.Contains(@"I:\"))
                        FilePath = FilePath.Replace(@"I:\", @"E:\IMAGING\");
                    else
                        FilePath = FilePath.Replace(@"E:\IMAGING\", @"I:\");

                }
                FI = new FileInfo(FilePath);
                if (!FI.Exists) return;

                using (var SR = new StreamReader(FilePath))
                {
                    while (!SR.EndOfStream)
                    {
                        tLine = SR.ReadLine().Trim().ToUpper();
                        if (sB_Initial != null) sB_Initial.Append(tLine);
                        if (tLine.Contains("</AUTOLEADACQUISITIONPROTOCOL>"))
                        //if (tLine.Contains("OFFSET_POINT INDEX=")) //11/20/2023 used to be POINT INDEX
                        {
                            if (sB_Initial != null) { _KVPs = XDCE_Image.XML_Extract_KVPs(sB_Initial.ToString()); }
                            while (!SR.EndOfStream)
                            {
                                tLine = SR.ReadLine().Trim().ToUpper();
                                if (!tLine.Contains("POINT INDEX=")) break;
                            }
                            sB_Initial = null;
                        }
                        if (!tLine.StartsWith("<IMAGE GUID")) continue;

                        sB = new StringBuilder(); sB.Append(tLine);
                        while (!SR.EndOfStream)
                        {
                            tLine = SR.ReadLine().Trim().ToUpper();
                            sB.Append(tLine);
                            if (tLine != "</IMAGE>") continue;
                            xImage = new XDCE_Image(sB.ToString(), idx++, this);
                            Add(xImage);
                            break;
                        }
                    }
                    SR.Close();
                }
                Loaded = true;
                //var t = _Images[0].CenterPixel.X;
            }

            public void SaveFile()
            {
                this.DirtyWells = false; this.DirtyRafts = false; //To Prevent Recalculations
                this.CalibrationRaftSettings = null;
                var sB = new StringBuilder();
                sB.Append("<?XML VERSION=\"1.0\" ENCODING=\"ISO-8859-1\" STANDALONE=\"NO\"?>\r\n");
                //Write Initial Parts
                foreach (var KVP in KVPs)
                {
                    if (KVP.Key.StartsWith("IMAGESTACK_PLATEID"))
                        sB.Append(XDCE_Image.KVP_TO_XML(KVP.Key, KVP.Value.ToString(), false));
                    else
                        sB.Append(XDCE_Image.KVP_TO_XML(KVP.Key, KVP.Value.ToString()));
                }
                sB.Append("<AUTOLEADACQUISITIONPROTOCOL><PLATEMAP><ACQUISITIONMODE><FIELDOFFSETS>\r\n");
                sB.Append("<offset_point index=\"0\" x=\"-4269.720\" y=\"4269.720\"/>\r\n");
                sB.Append("</FIELDOFFSETS></ACQUISITIONMODE></PLATEMAP></AUTOLEADACQUISITIONPROTOCOL>\r\n");
                //Write the XDCE Image Parts
                foreach (var well in Wells.Values)
                {
                    foreach (var xI in well.Images)
                    {
                        //FIV834P5_20X_G - 03(FLD 01).TIF
                        sB.Append("<IMAGE GUID=\"9AC27986-764C-47D9-9EE8-9A86EEA5904B\" ACQUISITION_TIME_MS=\"-1\" CELL_COUNT=\"-1\" FILENAME=\"" + xI.FileName + "\" TIMESTAMP_SEC=\"-1\" VERSION=\"1.1\">\r\n");
                        foreach (var KVP in xI.KVPs)
                        {
                            if (!KVP.Key.StartsWith("IMAGE_"))
                                sB.Append("    " + XDCE_Image.KVP_TO_XML(KVP.Key, KVP.Value.ToString()));
                        }
                        sB.Append("</IMAGE>\r\n");
                    }
                }
                sB.Append("</IMAGESTACK>");

                if (this.FilePath == "")
                {
                    Debugger.Break();
                }
                File.WriteAllText(this.FilePath, sB.ToString());
            }

            private static HashSet<string> Compare_IgnoreList = new HashSet<string>() { "IMAGESTACK_PLATEID", "UUID_VALUE", "EXCLUDE_FIELDS", "AUTOLEADACQUISITIONPROTOCOL_FINGERPRINT" };

            public string Compare(XDCE ToCompareWith)
            {
                //This is just to look through the params and see what is different
                StringBuilder sB = new StringBuilder();
                Dictionary<string, object> Other = ToCompareWith.KVPs;
                foreach (KeyValuePair<string, object> KVP in KVPs)
                {
                    if (Compare_IgnoreList.Contains(KVP.Key)) continue;
                    if (Other.ContainsKey(KVP.Key))
                    {
                        if ((string)KVP.Value != (string)Other[KVP.Key])
                        {
                            sB.Append(KVP.Key + ": " + KVP.Value + ", " + ToCompareWith.KVPs[KVP.Key] + "\r\n\r\n");
                        }
                    }
                    else
                    {
                        sB.Append("MissingIndex\r\n");
                    }
                }
                return sB.ToString();
            }

            #endregion

            #region Statics -------------------------------------------------------------------------
            public static List<Tuple<int, int>> SurroundIndices = new List<Tuple<int, int>>() {
                                                                          new Tuple<int, int>(-2, 0),
                                             new Tuple<int, int>(-1, -1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1),
                new Tuple<int, int>( 0, -2), new Tuple<int, int>( 0, -1),                             new Tuple<int, int>( 0, 1), new Tuple<int, int>( 0, 2),
                                             new Tuple<int, int>( 1, -1), new Tuple<int, int>( 1, 0), new Tuple<int, int>( 1, 1),
                                                                          new Tuple<int, int>( 2, 0)
            };

            public static List<Tuple<int, int>> SurroundIndices_Fancy = new List<Tuple<int, int>>() {
                                             new Tuple<int, int>(-2, -1), new Tuple<int, int>(-2, 0), new Tuple<int, int>(-2, 1),
                new Tuple<int, int>(-1, -2), new Tuple<int, int>(-1, -1), new Tuple<int, int>(-1, 0), new Tuple<int, int>(-1, 1), new Tuple<int, int>(-1, 2),
                new Tuple<int, int>( 0, -2), new Tuple<int, int>( 0, -1),                             new Tuple<int, int>( 0, 1), new Tuple<int, int>( 0, 2),
                                             new Tuple<int, int>( 1, -1),                             new Tuple<int, int>( 1, 1),
                                             new Tuple<int, int>( 2, -1),                             new Tuple<int, int>( 2, 1)
            };

            /// <summary>
            /// Searches for all XDCE files, and exports a table next to the original file . . Use Compile if you want to compile a single table with everything
            /// </summary>
            /// <param name="BasePath">Place to start looking for XDCE files, is recursive</param>
            internal static void MakeTableFromEach(string BasePath)
            {
                DirectoryInfo DI = new DirectoryInfo(BasePath);
                foreach (DirectoryInfo DIsub in DI.GetDirectories())
                {
                    MakeTableFromEach(DIsub.FullName);
                }
                foreach (FileInfo file in DI.GetFiles("*.xdce"))
                {
                    XDCE xTemp = new XDCE(file.FullName);
                    xTemp.Export(Path.Combine(BasePath, Path.GetFileNameWithoutExtension(file.Name) + ".txt"));
                }
            }

            /// <summary>
            /// Newer (10/2019) version of exports, directed once the XDCE's have already been created
            /// </summary>
            /// <param name="XDCEs"></param>
            /// <param name="ExportPath"></param>
            public static void Compile(List<XDCE> XDCEs, string ExportPath)
            {
                var SW = new StreamWriter(ExportPath);
                bool First = true;
                foreach (XDCE xDCE in XDCEs)
                {
                    xDCE.Export(SW, First);
                    First = false;
                }
                SW.Close();
            }

            /// <summary>
            /// Similar to MakeTableFromEach, but this compiles them into a single table - Older, Slow (<9/2019)
            /// </summary>
            /// <param name="BasePath">Place to start looking for XDCE files, does search subfolders</param>
            /// <param name="ExportPath">Where to export</param>
            public static void Compile(string BasePath, string ExportPath)
            {
                var Files = new List<FileInfo>();
                var DI = new DirectoryInfo(BasePath);
                //- - This took 8 minutes . . not cool // Too many subdirectories with lots of files >> //Files = DI.GetFiles("*.xdce", SearchOption.AllDirectories).ToList();  
                //- - This version only took 13 s !!
                foreach (var DIsub in DI.GetDirectories())
                {
                    Files.AddRange(DIsub.GetFiles("*.xdce", SearchOption.TopDirectoryOnly).ToList());
                    foreach (var DIPlate in DIsub.GetDirectories())
                    {
                        Files.AddRange(DIPlate.GetFiles("*.xdce", SearchOption.TopDirectoryOnly).ToList());
                    }
                }
                var SW = new StreamWriter(ExportPath);
                XDCE xDCE; bool First = true;
                foreach (var file in Files)
                {
                    //if (!file.Name.ToUpper().Contains("RAFT")) continue;
                    xDCE = new XDCE(file.FullName, true);
                    //if (xDCE.Images.Count > 2000) continue;
                    xDCE.Export(SW, First);
                    First = false;
                }
                SW.Close();
            }

            internal static void Export(List<INCELL_Folder> Folders, string PathExport)
            {
                StreamWriter SW = new StreamWriter(PathExport);
                bool first = true;
                foreach (INCELL_Folder folder in Folders)
                {
                    folder.XDCE.Export(SW, first);
                    first = false;
                }
                SW.Close();
            }

            internal static void Export(List<XDCE> XDCEList, string PathExport)
            {
                StreamWriter SW = new StreamWriter(PathExport);
                bool first = true;
                foreach (XDCE x in XDCEList)
                {
                    x.Export(SW, first);
                    first = false;
                }
                SW.Close();
            }

            #endregion

            internal double Get_Adjacent_ZFocus_Average(XDCE_Image Image, int XIndex, int YIndex)
            {
                double SumZFocus = 0; double CountZFocus = 0; List<XDCE_Image> tAdj;
                foreach (Tuple<int, int> tupleOffset in SurroundIndices)
                {
                    tAdj = GetAdjacent(XIndex, YIndex, tupleOffset.Item1, tupleOffset.Item2);
                    if (tAdj.Count == 0) continue;
                    CountZFocus++; SumZFocus += tAdj[0].z;
                }
                if (CountZFocus > 0) return SumZFocus / CountZFocus;
                return 0;
            }

            private bool _SetupFocusDev_Done = false;
            internal void SetupFocusDeviation()
            {
                CheckLoad();
                if (_SetupFocusDev_Done) return;
                string colName = XDCE_Image.ColName_AdjacentFocusAvg;
                List<XDCE_Image> tImages; Tuple<int, int> Key;
                double AvgZFocus;
                for (int x = 0; x < X_to_Index.Count; x++)
                {
                    for (int y = 0; y < Y_to_Index.Count; y++)
                    {
                        Key = new Tuple<int, int>(x, y);
                        if (_ImageByIndices.ContainsKey(Key))
                        {
                            tImages = _ImageByIndices[Key];
                            AvgZFocus = Get_Adjacent_ZFocus_Average(tImages[0], x, y);
                            foreach (XDCE_Image img in tImages)
                            {
                                img.KVPs.Add(colName, AvgZFocus);
                            }
                        }
                    }
                }
                _SetupFocusDev_Done = true;
            }

            public IEnumerator<XDCE_Image> GetEnumerator()
            {
                return Images.GetEnumerator();
            }

            public void Export(string PathSave)
            {
                CheckLoad();
                StreamWriter SW = new StreamWriter(PathSave);
                Export(SW, true);
                SW.Close();
            }

            public void Export(StreamWriter SW, bool IncludeHeaders)
            {
                SetupFocusDeviation();
                if (IncludeHeaders)
                {
                    //Reset these just in case
                    KVPHeaderOrder = new List<string>(); XDCE_Image.KVPHeaderOrder = new List<string>();
                    SW.WriteLine(Images.First().Export(true) + ExportPres(true));
                }
                foreach (XDCE_Image xImage in Images)
                {
                    SW.WriteLine(xImage.Export(false) + ExportPres(false));
                }
            }

            protected static List<string> KVPHeaderOrder = new List<string>();

            public string ExportPres(bool Headers)
            {
                char delim = '\t';
                StringBuilder SB = new StringBuilder();

                //These KVPs are slightly variable . . so we need to make a static that everything goes in to keep it in order! (only Keys in the first dictionary will get exported)
                if (KVPHeaderOrder.Count == 0) { foreach (KeyValuePair<string, object> KVP in KVPs) KVPHeaderOrder.Add(KVP.Key); }
                string res;
                foreach (string Header in KVPHeaderOrder)
                {
                    res = KVPs.ContainsKey(Header) ? KVPs[Header].ToString() : "";
                    SB.Append((Headers ? Header : res) + delim);
                }
                return SB.ToString();
            }


        }

        public enum FIVToolsMode
        {
            Normal = 0,
            PlateID = 1,
            PlateID_WellList = 2,
            WellCalibrate = 11,
            FOVRefine = 12,
            Listening = 33,
            Silent = 1000
        }

        public class Cell_Filter
        {
            public Cell_Filter()
            {
                DefineRows = new List<object>();
            }

            public List<object> DefineRows;
            public string Name;

            public static Cell_Filter Default_FieldRestrict(int LowCells, int HighCells)
            {
                //Count the number of Cells per Field, add this as own column
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add(LowCells); CF.DefineRows.Add(HighCells);
                CF.Name = "Field Restrict";
                return CF;
            }

            public static Cell_Filter Default_ColumnsRemove(IEnumerable<string> ColumnsToRemove)
            {
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add("");
                CF.Name = "Columns Remove";
                return CF;
            }

            internal void Apply(INCARTA_Analysis iNCARTA_Analysis)
            {
                if (this.Name == "Field Restrict")
                {
                    List<INCARTA_Cell> CellsToDelete = new List<INCARTA_Cell>();
                    int cellsPerfield;
                    foreach (INCARTA_Cell cell in iNCARTA_Analysis.Cells)
                    {
                        cellsPerfield = int.Parse((string)cell.DataPerHeader(INCARTA_Cell_Headers.col_CellsPerField));
                        if (cellsPerfield < (int)DefineRows[0] || cellsPerfield > (int)DefineRows[1]) CellsToDelete.Add(cell);
                    }
                    iNCARTA_Analysis.Cells = iNCARTA_Analysis.Cells.Except(CellsToDelete).ToList();
                    //foreach (INCARTA_Cell cell in CellsToDelete)
                    //{
                    //    iNCARTA_Analysis.Cells.Remove(cell);
                    //}
                }
            }

            internal static Cell_Filter Default_Object0Remove()
            {
                //ObjectID = 0  (These are something special, but are not standard cells)
                Cell_Filter CF = new Cell_Filter();
                CF.DefineRows.Add("");
                CF.Name = "Object0 Remove";
                return CF;
            }
        }

        public class INCARTA_Analysis
        {
            public INCARTA_Cell_Headers CombinedHeaders { get; }
            public List<INCARTA_Cell> Cells { get; internal set; }
            public static int CellsRemoved;
            public static int CellsPerField_Min;
            public static int CellsPerField_Max;
            public static double CellsPerField_Mean;
            public static int FOV_Count;

            public INCARTA_Analysis(IEnumerable<INCARTA_Analysis_Folder> Analyses, System.ComponentModel.BackgroundWorker BW = null)
            {
                CellsRemoved = CellsPerField_Max = FOV_Count = 0; CellsPerField_Mean = 0; CellsPerField_Min = 100000;
                CombinedHeaders = new INCARTA_Cell_Headers();
                Cells = new List<INCARTA_Cell>();
                foreach (INCARTA_Analysis_Folder folder in Analyses)
                {
                    if (BW != null) BW.ReportProgress(0, folder.FolderName);
                    Cells.AddRange(LoadFromFolder(folder, BW));
                    if (Cells.Count > 0)
                        CombinedHeaders.AddMerge(Cells.Last().Headers);
                }

                CellsPerField_Mean = FOV_Count == 0 ? 0 : Cells.Count / FOV_Count;
            }

            public INCARTA_Analysis(INCARTA_Analysis_Folder folder, BackgroundWorker BW = null)
            {
                Cells = LoadFromFolder(folder, BW);
            }

            private static List<INCARTA_Cell> LoadFromFolder(INCARTA_Analysis_Folder folder, BackgroundWorker BW = null)
            {
                //Read in the INCARTA Analysis line, store it so that we can easily re-arrange the columns
                List<INCARTA_Cell> CellList = new List<INCARTA_Cell>(); long counter = 0;
                FileInfo FI = new FileInfo(folder.Path_CellExport_Adjusted); if (!FI.Exists) return CellList;
                using (StreamReader SR = new StreamReader(FI.FullName))
                {
                    string tLine = SR.ReadLine();
                    var Headers = new INCARTA_Cell_Headers(tLine, folder);
                    INCARTA_Cell tCell;
                    //This bit of code records the Cells/Field
                    string Well_Field = ""; var CurrentFOVCells = new HashSet<INCARTA_Cell>();
                    while (!SR.EndOfStream)
                    {
                        tLine = SR.ReadLine();
                        if (BW != null) { if (counter++ % 2500 == 0) BW.ReportProgress(0, counter / 2500); }
                        tCell = new INCARTA_Cell(folder, Headers, tLine);
                        if (tCell.IsValid) //There are some lines in these files that aren't really a cell
                        {
                            Add_Cells_Coordinates_Name(tCell);
                            if (tCell.Well.HasRaftCalibration) Add_Cells_RaftID(tCell);
                            else { if (tCell.Well.HasImageCheck) Add_Cells_ImageCheck(tCell); }
                            CellList.Add(tCell);
                            if (Well_Field != tCell.WellField)
                            {
                                //Go through the previous Cell list, and add the field column
                                Add_CellsPerFieldColumn(CurrentFOVCells);
                                //Start the new set
                                Well_Field = tCell.WellField; CurrentFOVCells = new HashSet<INCARTA_Cell>();
                            }
                            CurrentFOVCells.Add(tCell);
                        }
                        else { CellsRemoved++; }
                    }
                    SR.Close();
                    Add_CellsPerFieldColumn(CurrentFOVCells);
                }
                return CellList;
            }

            private static void Add_Cells_Coordinates_Name(INCARTA_Cell tCell)
            {
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_X_Well_um, tCell.x_well_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_Y_Well_um, tCell.y_well_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_X_Plate_um, tCell.x_plate_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_Y_Plate_um, tCell.y_plate_um);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageName, tCell.RelatedImage.FileName);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ZRaw, tCell.RelatedImage.z);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ZAvgAdjacent, tCell.RelatedImage.z_AvgAdjacent);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_TimeStampSec, tCell.RelatedImage.timeStampSec);
            }

            private static void Add_Cells_RaftID(INCARTA_Cell tCell)
            {
                double x = tCell.x_plate_um;  //Convert to Plate Coordinates
                double y = tCell.y_plate_um;
                FOVtoRaftID.ReturnRaft RR = tCell.Well.CalibrationRaftSettings.FindRaftID_RR(x, y);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftID, RR.RaftID);
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftRow, RR.RaftID.Substring(0, 2));
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftCol, RR.RaftID.Substring(2, 2));
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_CellsRaftDistBorder, RR.MinDistToEdge);
                if (tCell.Well.HasImageCheck)
                {
                    ImageCheck.ImageCheck_PointList PL = tCell.Well.ImageCheck_PointsList;

                    string Res = PL.FromDictionaryRaftID_Combined(RR.RaftID); if (Res == "") Res = "-";
                    AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageCheckAnnotation, Res);
                }
            }
            private static void Add_Cells_ImageCheck(INCARTA_Cell tCell)
            {
                ImageCheck.ImageCheck_PointList PL = tCell.Well.ImageCheck_PointsList;
                string Res = PL.FromDictionaryFOV_Combined(tCell.FOV); if (Res == "") Res = "-";
                AddData_AndMaybeNewColumn(tCell, INCARTA_Cell_Headers.col_ImageCheckAnnotation, Res);
            }

            internal static void AddData_AndMaybeNewColumn(INCARTA_Cell tCell, string ColumnHeaderName, object Value)
            {
                if (!tCell.Headers.Contains(ColumnHeaderName))
                    tCell.Headers.AddNewColumn(ColumnHeaderName);
                tCell.AddData(ColumnHeaderName, Value);
            }

            private static void Add_CellsPerFieldColumn(HashSet<INCARTA_Cell> currentFOVCells)
            {
                if (currentFOVCells.Count == 0) return;
                int n = currentFOVCells.Count;
                if (n < CellsPerField_Min) CellsPerField_Min = n;
                if (n > CellsPerField_Max) CellsPerField_Max = n;
                FOV_Count++;
                if (!currentFOVCells.First().Headers.Contains(INCARTA_Cell_Headers.col_CellsPerField))
                {
                    currentFOVCells.First().Headers.AddNewColumn(INCARTA_Cell_Headers.col_CellsPerField);
                }
                string szCount = n.ToString();
                foreach (INCARTA_Cell cell in currentFOVCells)
                {
                    cell.AddData(INCARTA_Cell_Headers.col_CellsPerField, szCount);
                }
            }

            public void ApplyFilters(List<Cell_Filter> Filters)
            {
                //First filter the columns
                //This won't work since we don't parse the lines until we start to export
                /*
                List<string> ColumnsToRemove = new List<string>();
                foreach (string Column in CombinedHeaders)
                {
                    if (INCARTA_Cell_Headers.ValueCount[Column] < 10) ColumnsToRemove.Add(Column);
                }
                */

                //Now apply specific filters
                foreach (Cell_Filter filter in Filters)
                {
                    filter.Apply(this);
                }
            }

            public void Export(string Path)
            {
                using (StreamWriter SW = new StreamWriter(Path))
                {
                    ExportCells(CombinedHeaders, Cells, true, false, SW);
                }
            }

            internal static void ExportCells(INCARTA_Cell_Headers CHeaders, List<INCARTA_Cell> CellsList, bool ExportHeaders, bool RenameWavelengths, StreamWriter SW)
            {
                if (ExportHeaders) SW.WriteLine(RenameWavelengths ? CHeaders.ExportLine_Rename : CHeaders.ExportLine);
                foreach (INCARTA_Cell cell in CellsList)
                {
                    SW.WriteLine(cell.ExportReOrder(CHeaders));
                }
            }

            internal static void CompileExport(List<INCARTA_Analysis_Folder> compileList_AFs, string pathExport, bool Rename_Wavelengths, BackgroundWorker bW)
            {
                StreamWriter SW = new StreamWriter(pathExport);
                string prevHeaders = "";
                foreach (INCARTA_Analysis_Folder folder in compileList_AFs)
                {
                    if (bW != null) bW.ReportProgress(0, folder.FolderName);
                    prevHeaders = CompileExport_I1(folder, prevHeaders, SW, Rename_Wavelengths, bW);
                    SW.Flush();
                }
                SW.Close();
            }

            private static Tuple<List<int>, string> RemoveDuplicateHeaders(List<string> headers)
            {
                HashSet<string> seenHeaders = new HashSet<string>();
                //List<string> uniqueLines = new List<string>();
                string uniqueLines = "";
                List<int> removedIndices = new List<int>();
                int index = 0;
                foreach (string line in headers)
                {

                    List<string> parts = line.Split(',').ToList();
                    //List<string> uniqueParts = new List<string>();
                    string uniqueParts = "";

                    foreach (string part in parts)
                    {
                        if (!seenHeaders.Contains(part.Trim()))
                        {
                            seenHeaders.Add(part.Trim());
                            //uniqueParts.Add(part);
                            uniqueParts += part + ",";
                        }
                        else removedIndices.Add(index);
                        index++;
                    }

                    //uniqueLines.Add(string.Join(", ", uniqueParts));  // Rejoin the unique parts into a single string
                    uniqueLines += uniqueParts;
                }

                return new Tuple<List<int>, string>(removedIndices, uniqueLines.Remove(uniqueLines.Length - 1));
            }
            internal static string CompileExport_I1(INCARTA_Analysis_Folder folder, string PrevHeaders, StreamWriter SW, bool Rename_Wavelengths, BackgroundWorker BW)
            {
                string GetCommonPrefix(string[] strs)
                {
                    if (strs.Length == 0) return "";
                    string prefix = strs[0];

                    foreach (string s in strs)
                    {
                        int i = 0;
                        while (i < prefix.Length && i < s.Length && prefix[i] == s[i])
                            i++;
                        prefix = prefix.Substring(0, i);
                        if (prefix == "") break;
                    }
                    return prefix;
                }

                string GetCommonSuffix(string[] strs)
                {
                    if (strs.Length == 0) return "";
                    string suffix = strs[0];

                    foreach (string s in strs)
                    {
                        int i = 0;
                        while (i < suffix.Length && i < s.Length && suffix[suffix.Length - 1 - i] == s[s.Length - 1 - i])
                            i++;
                        suffix = suffix.Substring(suffix.Length - i, i);
                        if (suffix == "") break;
                    }
                    return suffix;
                }



                if (folder.Path_CellExport != null)
                    return CompileExport_I1_old(folder, PrevHeaders, SW, Rename_Wavelengths, BW);
                long counter = 0;
                bool first = false;
                // folder.isNewFormat = folder.newPaths_CellExport?.Count > 1;

                // Determine files to be read
                //folder.newPaths_CellExport = new List<string>();
                var DI = new DirectoryInfo(folder.FolderPath); int i1 = 0;
                string[] searchTerms = { "CELL", "COLLAR", "NUCLEI" };
                string fallBack = "";
                while (folder.newPaths_CellExport.Count == 0 && i1 < searchTerms.Length)
                {
                    var Options = new List<string>();
                    foreach (var File in DI.GetFiles("*_ObjectData.csv"))
                    {
                        fallBack = File.FullName;
                        if (File.Name.ToUpper().Contains(searchTerms[i1].ToUpper()))
                            Options.Add(File.FullName);
                    }
                    if (Options.Count>1)
                    {
                        string purest = Options.OrderBy(s => s.Length).FirstOrDefault();
                        Options = new List<string>() { purest };
                        //string commonPrefix = GetCommonPrefix(Options.ToArray());
                        //string commonSuffix = GetCommonSuffix(Options.ToArray());

                        //// Extract the core part (remove prefix and suffix)
                        //var coreParts = Options.Select(p => p.Substring(commonPrefix.Length, p.Length - commonPrefix.Length - commonSuffix.Length));

                        //// Find the "purest" version containing "Cells"
                        //string purestVersion = coreParts
                        //    .OrderBy(s => s.Length)  // Pick the simplest version
                        //    .FirstOrDefault();
                    }
                    if (Options.Count == 1)
                    {
                        folder.newPaths_CellExport.Add(Options[0]); break;
                    }
                    i1++;
                }
                if (folder.newPaths_CellExport.Count == 0) folder.newPaths_CellExport.Add(fallBack);
                var filePaths = folder.newPaths_CellExport;

                //List<string> filePaths = new List<string>();
                //filePaths.Add(folder.Path_CellExport);
                // Open all files at once
                List<StreamReader> readers = filePaths.Select(path => new StreamReader(path)).ToList();
                List<string> currentLines = readers.Select(sr => sr.ReadLine()).ToList();  // Read the first line from each
                List<int> dupeIndices = new List<int>();
                string bigHeaderLine = "";
                Tuple<List<int>, string> indicesAndHeaders = RemoveDuplicateHeaders(currentLines);
                dupeIndices = indicesAndHeaders.Item1;
                bigHeaderLine = indicesAndHeaders.Item2;
                INCARTA_Cell_Headers bigHeader = new INCARTA_Cell_Headers(bigHeaderLine, folder);
                // Check headers
                if (!string.IsNullOrEmpty(PrevHeaders) && currentLines.Any(line => line != PrevHeaders))
                {
                    foreach (var sr in readers)
                        sr.Dispose();
                    Debugger.Break();
                    return PrevHeaders;
                }

                if (string.IsNullOrEmpty(PrevHeaders))
                    first = true;

                PrevHeaders = bigHeaderLine;  // Assuming all headers are the same

                var currentFOVCells = new HashSet<INCARTA_Cell>();
                var cellList = new List<INCARTA_Cell>();
                string wellField = "";
                INCARTA_Cell tCell;
                bool exit = false;
                // Process files - - - - - - - - -- - - - - - - - - - - - - - - - - - - - - - 
                while (readers.Any(sr => !sr.EndOfStream))
                {
                    string bigTLine = "";
                    for (int i = 0; i < readers.Count; i++)
                    {
                        if (readers[i].EndOfStream)
                        {
                            exit = true; continue;
                        }
                        if (exit) break;
                        string tLine = readers[i].ReadLine();
                        bigTLine += tLine + ",";
                        if (BW != null && counter++ % 1500 == 0) BW.ReportProgress(0, counter / 1500);
                    }
                    if (!exit)
                    {
                        // need to split on commas, and remove at dupedIndices
                        bigTLine = bigTLine.Remove(bigTLine.Length - 1);
                        List<string> tLineArray = bigTLine.Split(",").ToList();
                        for (int j = dupeIndices.Count - 1; j > -1; j--)
                        {
                            tLineArray.RemoveAt(dupeIndices[j]);
                        }
                        int count = 0;
                        foreach (string data in tLineArray)
                        {
                            if (data == "") count++;
                        }
                        if (count > tLineArray.Count / (readers.Count + 1)) continue;
                        bigTLine = string.Join(",", tLineArray);
                        tCell = new INCARTA_Cell(folder, bigHeader, bigTLine);
                        if (tCell.IsValid)
                        {
                            if (wellField != tCell.WellField)
                            {
                                DoExport();
                                wellField = tCell.WellField;
                                currentFOVCells = new HashSet<INCARTA_Cell>();
                                cellList = new List<INCARTA_Cell>();
                            }
                            Add_Cells_Coordinates_Name(tCell);
                            if (tCell.Well.HasRaftCalibration) Add_Cells_RaftID(tCell);
                            else if (tCell.Well.HasImageCheck) Add_Cells_ImageCheck(tCell);
                            currentFOVCells.Add(tCell);
                            cellList.Add(tCell);
                        }
                    }
                }

                foreach (var sr in readers) sr.Dispose();

                if (counter == 0 && BW != null) BW.ReportProgress(0, "Empty File");

                DoExport();
                return PrevHeaders;

                void DoExport()
                {
                    if (string.IsNullOrEmpty(wellField)) return;
                    Add_CellsPerFieldColumn(currentFOVCells);
                    ExportCells(bigHeader, cellList, first, Rename_Wavelengths, SW);
                    first = false;
                }

            }

            // Might be returning to this way of Compiling, assuming Cell_ObjectData (or some other singular ObjectData file) has everything we care about. 
            internal static string CompileExport_I1_old(INCARTA_Analysis_Folder folder, string PrevHeaders, StreamWriter SW, bool Rename_Wavelengths, BackgroundWorker BW)
            {
                long counter = 0; bool First = false;
                FileInfo FI = new FileInfo(folder.Path_CellExport_Adjusted); if (!FI.Exists) return PrevHeaders;
                StreamReader SR = new StreamReader(FI.FullName);
                //This next bit figures out whether the headers match
                string tLine = SR.ReadLine();
                var Headers = new INCARTA_Cell_Headers(tLine, folder);
                if (PrevHeaders == "") { First = true; }
                else
                {
                    var H1 = Headers.ExportLine;
                    var H2 = new INCARTA_Cell_Headers(PrevHeaders, folder).ExportLine;
                    if (H1 != H2)
                    {
                        /*                        var t = PrevHeaders.Split(',');
                                                var x = tLine.Split(',');
                                                foreach (var item in t)
                                                {
                                                    if (!x.Contains(item))
                                                    {
                                                        _ = 0;
                                                    }
                                                }*/
                        Debugger.Break(); //The program can deal with this if we go through all the files first just reading the headers, then going back (not yet implemented)
                        return PrevHeaders;
                    }
                }
                string Well_Field = ""; INCARTA_Cell tCell; //This bit of code records the Cells/Field
                List<INCARTA_Cell> CellList = null; HashSet<INCARTA_Cell> CurrentFOVCells = null;
                PrevHeaders = tLine;
                var HeadersExport = Headers.ExcludeLeaky();
                bool TargetDataFolderMode = false; string[] TargetFiles = new string[0]; int TargetFileIdx = 0;

                void DoExport()
                {
                    if (Well_Field == "") return; //First time, nothing to export yet
                    Add_CellsPerFieldColumn(CurrentFOVCells); //Go through the previous Cell list, and add the field column
                    ExportCells(Headers, CellList, First, Rename_Wavelengths, SW);  //Export
                    First = false;
                }

                void NextFileTargetData() //Added 4/2022 so that we could automatically load files that didn't get fully processed into the output file. This automtaically loads them
                {
                    if (TargetDataFolderMode)
                    {
                        if (SR.EndOfStream && (TargetFileIdx < TargetFiles.Length))
                        {
                            SR.Close(); SR = new StreamReader(TargetFiles[TargetFileIdx++]);
                            SR.ReadLine();
                        }
                    }
                }

                //Here is the actual start of the reading
                TargetDataFolderMode = SR.EndOfStream;  //If the main file only has the headers, then switch to the individual files
                                                        //TargetDataFolderMode = true; //Turn this to TRUE to load from the individual files . . 
                if (TargetDataFolderMode)
                {
                    string TDataPath = Path.Combine(folder.FolderPath, "TargetData");
                    if (!Directory.Exists(TDataPath))
                    {
                        Debugger.Break(); //Couldn't find Target Data folder
                        if (BW != null) BW.ReportProgress(0, "!No data found");
                    }
                    TargetFiles = Directory.GetFiles(TDataPath, "singleTargetData*.csv");
                    NextFileTargetData();
                }
                while (!SR.EndOfStream)
                {
                    if (BW != null) { if (counter++ % 1500 == 0) BW.ReportProgress(0, counter / 1500); }
                    tCell = new INCARTA_Cell(folder, Headers, tLine = SR.ReadLine());
                    if (tCell.IsValid) //There are some lines in these files that aren't really a cell
                    {
                        if (Well_Field != tCell.WellField) //Export then Start the new set
                        {
                            DoExport();
                            Well_Field = tCell.WellField;
                            CurrentFOVCells = new HashSet<INCARTA_Cell>();
                            CellList = new List<INCARTA_Cell>();
                        }
                        Add_Cells_Coordinates_Name(tCell);
                        if (tCell.Well.HasRaftCalibration) Add_Cells_RaftID(tCell);
                        else { if (tCell.Well.HasImageCheck) Add_Cells_ImageCheck(tCell); }
                        CurrentFOVCells.Add(tCell);
                        CellList.Add(tCell);
                    }
                    else { CellsRemoved++; }
                    NextFileTargetData();
                }
                if (counter == 0 && BW != null) BW.ReportProgress(0, "Empty File");
                SR.Close();
                DoExport();


                return PrevHeaders;
            }
        }


        #region Feature Headers - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        public class INCARTA_Cell_Headers : IEnumerable<string>
        {
            public char delim = ',';

            public static string col_PlateID = "PLATEID";
            public static string col_ObjectID = "OBJECT ID";
            public static string col_CellsPerField = "CellsPerField";
            public static string col_CellsRaftID = "RaftID";
            public static string col_CellsRaftRow = "RaftRow";
            public static string col_CellsRaftCol = "RaftCol";
            public static string col_ZT_Z = "Z";
            public static string col_ZT_T = "T";
            public static string col_ZT_ResultsID = "RESULTSID";
            public static string col_CellsRaftDistBorder = "umFromBorderOfRaft";
            public static string col_ImageCheckAnnotation = "ImageCheck";
            public static string col_Well = "WELL LABEL";
            public static string col_Field = "FOV";
            public static string col_X_Well_um = "Well X um";
            public static string col_Y_Well_um = "Well Y um";
            public static string col_X_Plate_um = "Plate X um";
            public static string col_Y_Plate_um = "Plate Y um";
            public static string col_ZRaw = "Focus Z pos";
            public static string col_ZAvgAdjacent = "Focus Z Avg of Adjacent Fields";
            public static string col_TimeStampSec = "IMAGE_TIMESTAMP_SEC";
            public static string col_ImageName = "ImageName";
            public static string col_Xcg = "Nuclei cg X wv1";
            public static string col_Ycg = "Nuclei cg Y wv1";
            public static string col_Xborder = "Nuclei Max left border wv1";
            public static string col_Yborder = "Nuclei Max top border wv1";
            public static string newcol_Xcg = "Nuclei Center of Gravity X WVH";
            public static string newcol_Ycg = "Nuclei Center of Gravity Y WVH";


            public INCELL_WV_Notes WVNotes;
            private Dictionary<string, short> _Name2Index;
            private Dictionary<short, string> _Index2Name;
            public Dictionary<short, Header_Info> Index2HeaderInfo;

            public bool HasRaftHeader { get; internal set; }

            public INCARTA_Cell_Headers(string HeaderLine, INCARTA_Analysis_Folder AFolder)
            {
                HasRaftHeader = false;
                if (AFolder != null) WVNotes = AFolder.Parent.InCell_Wavelength_Notes; //This lets us decode the wv index into a real name
                init();
                string[] arr = HeaderLine.Split(delim);
                if (arr.Length < 5)
                {
                    string[] arr2 = HeaderLine.Split('\t');
                    if (arr2.Length > arr.Length)
                    {
                        delim = '\t'; arr = arr2;
                    }
                }

                // Appending WV# to column headers; Not always obvious when this needs to happen but the None (x) is a giveaway. It's just not always there.
                Regex regex = new Regex(@"\d :", RegexOptions.Compiled);
                Regex oldForm = new Regex(@"wv\d", RegexOptions.Compiled);
                Regex regex2 = new Regex(@"\|", RegexOptions.Compiled);
                var featureTypes = new Dictionary<int, string> { { -1, "ZZZ" } }; int k = 0;
                foreach (var stainAbbreviation in WVNotes.RenameHash) featureTypes.Add(k++, stainAbbreviation.Value);
                foreach (string column in arr)
                {
                    string input = translateHeader(column);
                    MatchCollection matches = regex.Matches(column);
                    MatchCollection matches2 = oldForm.Matches(column);

                    if (AFolder.Parent.isNewFormat)
                    {
                        if (matches2.Count > 0)
                        {
                            int y = 0;
                            int t = Int32.Parse(matches2[0].Value.Substring(2, 1));
                            foreach (INCELL_WV_Note note in WVNotes)
                            {
                                if (note.Index1 == t)
                                {
                                    y = note.Index1; break;
                                }
                            }
                            input.Replace(matches2[0].Value, $"WV{featureTypes[y - 1]}");
                        }

                        else if (matches.Count != 0)
                        {
                            input = translateHeader(column.Substring(0, matches[0].Index).Trim());
                            for (int mc = 0; mc < matches.Count; mc++)
                            {
                                input += $" WV{featureTypes[Int32.Parse(matches[mc].Value.Substring(0, 1)) - 1]}";
                            }
                        }
                        else  // Use look up table to determine appropriate name
                        {
                            var wvNote = translateNameToStain(input);
                            if (wvNote != -2) // if not metadata
                            {
                                input += $" WV{featureTypes[wvNote]}";
                            }
                        }

                    }
                    if (_Name2Index.ContainsKey(input))
                    {
                        Debugger.Break(); //Add an index on the end
                    }
                    else
                    {
                        AddNewColumn(input);
                    }
                }

                string translateHeader(string preTranslation)
                {
                    MatchCollection matches = regex2.Matches(preTranslation.ToUpper());
                    if (matches.Count != 0)
                    {
                        return preTranslation.Substring(matches[0].Index + 1).Trim();
                    }
                    else { return preTranslation; }
                }
                int translateNameToStain(string input)
                {
                    Dictionary<string, string> organelleToStain = new Dictionary<string, string>();
                    var potStains = findOrganelleStains(input);

                    if (potStains == null)
                        return -2; // case: metadata column
                    foreach (var potStain in potStains)
                    {
                        foreach (var stain_info in WVNotes.List)
                        {
                            string stain = stain_info.QuickName;
                            // create list of stains matching this organelle
                            if (stain.ToUpper().Contains(potStain))
                            {
                                return stain_info.Index1 - 1; // case: recognized stain 
                            }
                        }
                    }
                    return -1; // Case: no/unrecognized stain
                }

                List<string> findOrganelleStains(string input)
                {
                    switch (input.ToUpper())
                    {
                        case string s when s.StartsWith("CELL"):
                            return new List<string> { "TUB", "MASK", "MEMB", "TRIT" };
                        case string s when s.StartsWith("COLL"):
                            return new List<string> { "CELL" };
                        case string s when s.StartsWith("NUC"):
                            return new List<string> { "DAPI", "HOE", "NUC", "SPY" };
                        case string s when s.StartsWith("LYSO"):
                            return new List<string> { "FITC", "LYSO" };
                        case string s when s.StartsWith("MITO"):
                            return new List<string> { "SOX", "MITO" };
                        case string s when s.StartsWith("GOLG"):
                            return new List<string> { "GOL" };
                        default:
                            return null;
                    }
                }
            }

            public INCARTA_Cell_Headers()
            {
                init();
            }

            private void init()
            {
                _Name2Index = new Dictionary<string, short>();
                _Index2Name = new Dictionary<short, string>();
                Index2HeaderInfo = new Dictionary<short, Header_Info>();
            }

            public string RepairColumnName(string column)
            {
                //This is done to all new columns
                column = column.Replace("Mitochondria", "Mito");
                column = column.Replace("MITOCHONDRIA", "Mito");
                return column;
            }

            /// <summary>
            /// Maintains the starting names
            /// </summary>
            public string ExportLine
            {
                get
                {
                    StringBuilder sB = new StringBuilder();
                    foreach (string column in _Name2Index.Keys) sB.Append(column + delim);
                    return INCARTA_Analysis_Folder.ExportLine(delim) + delim + "Val Count" + delim + sB;
                }
            }

            /// <summary>
            /// Renames wv1 => WVH, etc. If no attached WVNotes then it just exports the same as ExportLine
            /// </summary>
            public string ExportLine_Rename
            {
                get
                {
                    if (WVNotes == null) return ExportLine;

                    StringBuilder sB = new StringBuilder();
                    foreach (string column in _Name2Index.Keys) sB.Append(WVNotes.Rename(column) + delim);
                    return INCARTA_Analysis_Folder.ExportLine(delim) + delim + "Val Count" + delim + sB;
                }
            }

            public short ShortKeyTable(string HeaderColumnName)
            {
                string Key = HeaderColumnName;
                if (_Name2Index.ContainsKey(Key)) return _Name2Index[Key];

                //Added 3/16/2022
                Key = Key.ToUpper().Trim();
                if (_Name2Index.ContainsKey(Key))
                {
                    //Add this to the database so we can get it automtically next time around
                    short v = _Name2Index[Key];
                    _Name2Index.Add(HeaderColumnName, v);
                }
                return -1;
            }

            internal void AddNewColumn(string column)
            {
                column = RepairColumnName(column);
                if (column == "RAFTID") column = "RaftID";
                if (column == "RaftID") HasRaftHeader = true;
                _Name2Index.Add(column, (short)_Name2Index.Count);
                _Index2Name.Add((short)(_Name2Index.Count - 1), column);
                Index2HeaderInfo.Add((short)(_Name2Index.Count - 1), new Header_Info(column, (short)(_Name2Index.Count - 1)));
            }

            public int Count { get => _Name2Index.Count; }

            public bool Contains(string value) { return _Name2Index.ContainsKey(value); }

            public string this[short index]
            {
                get { return _Index2Name[index]; }
            }

            public IEnumerator<string> GetEnumerator()
            {
                return _Name2Index.Keys.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Name2Index.GetEnumerator();
            }

            internal void AddMerge(INCARTA_Cell_Headers headers)
            {
                foreach (string column in headers)
                {
                    if (!Contains(column))
                    {
                        AddNewColumn(column);
                    }
                }
            }

            internal static Dictionary<string, int> ValueCount { get; private set; }

            internal static void AddValueCount(string HeaderName)
            {
                if (ValueCount == null) ValueCount = new Dictionary<string, int>(1);
                if (!ValueCount.ContainsKey(HeaderName)) ValueCount.Add(HeaderName, 0);
                ValueCount[HeaderName]++;
            }

            internal static HashSet<string> InCartaHeadersExcludeList = new() { "GLOBAL", "BCKGRND" };

            internal INCARTA_Cell_Headers Exclude_Headers_With(IEnumerable<string> inCartaHeadersExcludeList)
            {
                return null;
            }

            internal INCARTA_Cell_Headers ExcludeLeaky()
            {
                return null;
            }

            public List<Header_Info> Cols_Measurement => Index2HeaderInfo.Where(x => x.Value.IsMeasurement).Select(x => x.Value).ToList();
            public HashSet<Header_Info> Cols_CellMetadata => new HashSet<Header_Info>(Index2HeaderInfo.Where(x => x.Value.IsCellMetadata).Select(x => x.Value));
            public List<Header_Info> Cols_Undesired => Index2HeaderInfo.Where(x => x.Value.IsUndesired).Select(x => x.Value).ToList();
            public HashSet<string> Set_Masks => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Mask).Distinct());
            public HashSet<string> Set_WVs => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Wavelength).Distinct());
            public HashSet<string> Set_Measures => new HashSet<string>(Cols_Measurement.Select(x => x.Feature_Measure).Distinct());

        }

        #endregion

        /// <summary>
        /// An InCarta "Cell" Object, DOES link out to other types, so needs to be constructed with an InCell Folder and Analysis specifically
        /// </summary>
        public class INCARTA_Cell : Simple_Cell
        {
            private XDCE_ImageGroup _Well;
            public XDCE_ImageGroup Well
            {
                get
                {
                    if (_Well == null)
                    {
                        _Well = Parent.RelatedXDCE.Wells[this.WellLabel];
                    }
                    return _Well;
                }
            }

            private XDCE_Image _RelatedImage;
            public XDCE_Image RelatedImage
            {
                get
                {
                    if (_RelatedImage == null) _RelatedImage = Parent.RelatedXDCE.GetImage_FromWellFOV(WellField);
                    return _RelatedImage;
                }
            }
            public INCARTA_Analysis_Folder Parent { get; }

            public string ExportLine
            {
                get
                {
                    Debugger.Break(); //OLD VERSION, NOT UPDATED
                    return Parent.FolderName + delim + Parent.Index + delim + Line;
                }
            }

            private void SetupPositions()
            {
                XDCE_Image rImage = RelatedImage;
                _ = 0;
                _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.newcol_Xcg));
                _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.newcol_Ycg));
                if (_Xcg == -1)
                {
                    //First check if it is the old format
                    _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xcg));
                    _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Ycg));
                }
                if (_Xcg == -1)
                {
                    //Next check to see if it is measured in wv2, since that is the easiest
                    _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xcg.Replace("wv1", "wv2")));
                    _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Ycg.Replace("wv1", "wv2")));
                }
                if (_Xcg == -1)
                {
                    //If not here, then try the x/y border, but these aren't quite the same as the cg (Center)
                    _Xcg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Xborder));
                    _Ycg = double.Parse((string)DataPerHeader(INCARTA_Cell_Headers.col_Yborder));
                    _Xcg = _Xcg * rImage.Parent.PixelWidth_in_um; _Ycg = _Ycg * rImage.Parent.PixelHeight_in_um;
                }

                _x_well_um = _Xcg + rImage.x;
                _y_well_um = -_Ycg + (rImage.Parent.PixelHeight_in_um * rImage.Parent.ImageHeight_in_Pixels) - rImage.y;

                _x_plate_um = _Xcg + rImage.PlateX_um;
                _y_plate_um = -_Ycg + (rImage.Parent.PixelHeight_in_um * rImage.Parent.ImageHeight_in_Pixels) + rImage.PlateY_um;
            }

            private double _Xcg = Pre_DefaultVal;
            public double Xcg { get { if (_Xcg == Pre_DefaultVal) SetupPositions(); return _Xcg; } }

            private double _Ycg = Pre_DefaultVal;
            public double Ycg { get { if (_Ycg == Pre_DefaultVal) SetupPositions(); return _Ycg; } }

            private double _x_well_um = Pre_DefaultVal;
            public double x_well_um { get { if (_x_well_um == Pre_DefaultVal) SetupPositions(); return _x_well_um; } }

            private double _y_well_um = Pre_DefaultVal;
            public double y_well_um { get { if (_y_well_um == Pre_DefaultVal) SetupPositions(); return _y_well_um; } }

            private double _x_plate_um = Pre_DefaultVal;
            public double x_plate_um { get { if (_x_plate_um == Pre_DefaultVal) SetupPositions(); return _x_plate_um; } }

            private double _y_plate_um = Pre_DefaultVal;
            public double y_plate_um { get { if (_y_plate_um == Pre_DefaultVal) SetupPositions(); return _y_plate_um; } }

            public INCARTA_Cell(INCARTA_Analysis_Folder Parent, INCARTA_Cell_Headers Headers, string Line, char delim_override = '0')
            {
                if (delim_override != '0') delim = delim_override;
                IsValid = true;
                this.Parent = Parent;
                this.Headers = Headers;
                this.Line = Line;
                if (DataPerHeader(INCARTA_Cell_Headers.col_ObjectID).ToString() == "0") IsValid = false;
                if (DataPerHeader(Parent.isNewFormat ? INCARTA_Cell_Headers.newcol_Xcg : INCARTA_Cell_Headers.col_Xcg).ToString() == "") IsValid = false;
            }
            public string ExportReOrder(INCARTA_Cell_Headers HeadersOrder)
            {
                var sB = new StringBuilder();
                sB.Append(Parent.ExportLineThis(delim) + delim + ValuesCount + delim);
                foreach (string Column in HeadersOrder)
                {
                    if (Headers.Contains(Column))
                    {
                        sB.Append(DataPerHeader(Column));
                    }
                    sB.Append(delim);
                }
                _DataPerHeader = null;
                return sB.ToString();
            }
        }

        public class QStats
        {
            public string ColumnNameOriginal;
            public int IndexOriginal;
            public double Sum;
            public int Count;
            public int Errors;
            public int NormErrors;
            public HashSet<double> UniqueValues;

            public double Mean { get => Sum / Count; }

            public QStats(string Name, int OriginalIndex)
            {
                ColumnNameOriginal = Name;
                IndexOriginal = OriginalIndex;
                Sum = 0;
                Count = 0;
                UniqueValues = new HashSet<double>();
                Errors = 0;
            }

            public void AddValue(string ToParse)
            {
                double v1;
                if (double.TryParse(ToParse, out v1))
                {
                    Count++;
                    Sum += v1;
                    UniqueValues.Add(v1);
                }
                else
                {
                    Errors++;
                }
            }

            public double Norm(double val)
            {
                return val / Mean;
            }

            public double Norm(string ToParse)
            {
                double v1;
                if (double.TryParse(ToParse, out v1))
                {
                    return Norm(v1);
                }
                else
                {
                    NormErrors++;
                    return 0;
                }
            }
        }

        public class INCELL_DB
        {
            public List<INCELL_Folder> Folders { get; internal set; }
            public string BasePath { get; set; }
            public DateTime LastUpdated { get; set; }

            [XmlIgnore]
            public List<INCARTA_Analysis_Folder> CompileList_AFs { get; set; }
            [XmlIgnore]
            public List<INCELL_Folder> CompileList_Folders { get; set; }

            private List<INCARTA_Analysis_Folder> _AnalysisFolders;
            [XmlIgnore]
            public List<INCARTA_Analysis_Folder> AnalysisFolders
            {
                get
                {
                    if (_AnalysisFolders == null)
                    {
                        _AnalysisFolders = new List<INCARTA_Analysis_Folder>(Folders.Count);
                        foreach (INCELL_Folder folder in Folders)
                        {
                            AnalysisFolders.AddRange(folder.AnalysisFolders);
                        }
                    }
                    return _AnalysisFolders;
                }
            }

            public INCELL_DB()
            {
                BasePath = "";
                Folders = new List<INCELL_Folder>();
            }

            public INCELL_DB(string BasePath)
            {
                Init(BasePath, null);
            }

            public INCELL_DB(string BasePath, System.ComponentModel.BackgroundWorker BW)
            {
                Init(BasePath, BW);
            }

            private void Init(string BasePath, System.ComponentModel.BackgroundWorker BW)
            {
                this.BasePath = BasePath.Trim().ToUpper();
                Folders = INCELL_Folder.GetFromFolder(this.BasePath, BW);
                LastUpdated = DateTime.Now;
                Save();
            }

            public static string DefaultDBXMLName = "InCell_FIVETools.db";

            public bool Save()
            {
                //Saves it in the path of the
                return Save(Path.Combine(BasePath, DefaultDBXMLName));
            }

            public bool Save(string SavePath)
            {
                //Whenever saving, copy the path
                foreach (INCELL_Folder fldr in this.Folders) fldr.BasePath_Saved = fldr.BasePath_Current;
                var x = new XmlSerializer(typeof(INCELL_DB));
                using (var SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    return true;
                }
            }

            public static INCELL_DB Load_Or_Create(string BasePath, BackgroundWorker BW)
            {
                FileInfo FI = new FileInfo(Path.Combine(BasePath, DefaultDBXMLName));
                INCELL_DB dB;
                if (FI.Exists) dB = Load(FI.FullName);
                else dB = new INCELL_DB(BasePath, BW);
                foreach (INCELL_Folder fldr in dB.Folders) fldr.BasePath_Current = BasePath;
                return dB;
            }

            public static INCELL_DB Load_From_BasePath(string BasePath)
            {
                try
                {
                    FileInfo FI = new FileInfo(Path.Combine(BasePath, DefaultDBXMLName));
                    if (FI.Exists)
                    {
                        INCELL_DB dB = Load(FI.FullName);
                        foreach (INCELL_Folder fldr in dB.Folders) fldr.ReAssociate(BasePath);
                        return dB;
                    }
                }
                catch (Exception E) { RecentError = E.Message; }
                return null;
            }

            public static string RecentError = "";

            public static INCELL_DB Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(INCELL_DB));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    INCELL_DB CR = (INCELL_DB)x.Deserialize(F);
                    F.Close();
                    return CR;
                }
            }

            public void Refresh(BackgroundWorker BW, string LimitSearch = "")
            {
                //This assumes you already loaded this and we just want to check.  FOr now, we will just create a new one for testing
                Folders = INCELL_Folder.GetFromFolder(BasePath, BW, LimitSearch);
                _AnalysisFolders = null;
                int c = AnalysisFolders.Count; //This should trigger a refresh
                LastUpdated = DateTime.Now;
                Save();
            }
        }

        public class INCELL_WV_Note
        {
            public string WAVELENGTH_INDEX;
            public string WAVELENGTH_LABEL;
            public string WAVELENGTH_NAME;
            public string WAVELENGTH_IMAGING_MODE;
            public string WAVELENGTH_APERTURE_ROWS;
            public string WAVELENGTH_LASER_POWER;
            public string WAVELENGTH_OPEN_APERTURE;
            public string Threshold = "90";
            public string Staining = "N/A";
            public string Abbrev = "-";
            public string ExtraNote = "N/A";

            public INCELL_WV_Note()
            {

            }

            public override string ToString()
            {
                return QuickName;
            }

            private int _Index1 = -1;
            public int Index1
            {
                get
                {
                    if (_Index1 == -1)
                    {
                        if (WAVELENGTH_INDEX is null) _Index1 = 0;
                        else _Index1 = int.Parse(WAVELENGTH_INDEX) + 1;
                    }
                    return _Index1;
                }
                set { _Index1 = value; }
            }

            public string QuickName
            {
                get
                {
                    if (WAVELENGTH_NAME == null) WAVELENGTH_NAME = "";
                    if (WAVELENGTH_LABEL == null) WAVELENGTH_LABEL = "";
                    return Staining == "N/A" ? WAVELENGTH_LABEL == "" ? WAVELENGTH_NAME : WAVELENGTH_LABEL : Staining;
                }
            }

            public double Threshold_Value
            {
                get
                {
                    return double.Parse(Threshold);
                }
            }
        }

        public class INCELL_WV_Notes
        {
            //And if your class inherits a list and also has its own members, only the elements of the list get serialized. The data present in your class members is not captured. (From Stack Overflow)
            //I had this as inheriting IENumerable, but it didn't serialize other members
            private List<INCELL_WV_Note> _List;
            [XmlIgnore]
            internal bool LoadedFromXML = false;

            public List<INCELL_WV_Note> List { get => _List; set { _List = value; } }

            public string AQPNote { get; set; }

            public string AdditionalNote { get; set; }

            public string XDCEFolder { get; set; }

            public string ParentFolder { get; 
                set; }

            /// <summary>
            /// There are two versions of these, and this one is saved with the specific PlateID, whereas the other one is user specific defaults
            /// </summary>
            public wvDisplayParams DisplayParams { get; set; }

            public short NoXDCE_ChannelCount { get; set; }
            public double NoXDCE_PixelWidthMicrons { get; set; }
            public int NoXDCE_ImageWidthPixels { get; set; }
            public string NoXDCE_WellLabel { get; set; }

            public INCELL_WV_Notes()
            {
                LoadedFromXML = false;
                AQPNote = "NA";
                AdditionalNote = "NA";
                _List = new List<INCELL_WV_Note>();
            }

            public int Count { get => _List.Count; }

            public void Add(INCELL_WV_Note wvNote)
            {
                _List.Add(wvNote);
            }

            public IEnumerator<INCELL_WV_Note> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            public string DefaultXMLPath { get => DefaultXMLPathXDCE(XDCEFolder); }
            public static string DefaultXMLPathXDCE(string XDCEFolder)
            {
                return Path.Combine(XDCEFolder, "WavelengthNotes.xml");
            }

            public static INCELL_WV_Notes Load(string XMLFullPath)
            {
                //FileInfo FI = new FileInfo(XMLFullPath);

                try
                {
                    var x = new XmlSerializer(typeof(INCELL_WV_Notes));
                    using (var F = new FileStream(XMLFullPath, FileMode.Open))
                    {
                        var CR = (INCELL_WV_Notes)x.Deserialize(F);
                        F.Close();
                        CR.LoadedFromXML = true;
                        return CR;
                    }
                }
                catch (Exception E)
                {
                    string tar = E.Message;
                    return null;
                }
            }

            public static INCELL_WV_Notes Load_Folder(string XDCEFolder)
            {
                return Load(DefaultXMLPathXDCE(XDCEFolder));
            }

            public bool Save(string SavePath)
            {
                var x = new XmlSerializer(typeof(INCELL_WV_Notes));
                using (var SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    return true;
                }
            }

            public bool Save()
            {
                return Save(ParentFolder != null ? ParentFolder + "\\WavelengthNotes.xml" : DefaultXMLPath);
            }

            public static INCELL_WV_Notes GetFromXDCE(XDCE LookHere, string FolderPath)
            {
                if (LookHere == null || LookHere.KVPs == null) return null;
                var IWNs = new INCELL_WV_Notes();
                var Note = new INCELL_WV_Note(); //I had added { WAVELENGTH_INDEX = "0" } here, but that screws up the ordering of the indices (since it thinks it has already seen that index and moves on
                IWNs.Add(Note);

                IWNs.XDCEFolder = FolderPath;
                if (LookHere.KVPs.ContainsKey("USERCOMMENT"))
                    IWNs.AQPNote = LookHere.KVPs.ContainsKey("USERCOMMENT") ? LookHere.KVPs["USERCOMMENT"].ToString() : "NA";
                IWNs.AdditionalNote = "NA";

                Type t = typeof(INCELL_WV_Note);
                System.Reflection.FieldInfo[] rFields = t.GetFields();
                Dictionary<string, System.Reflection.FieldInfo> Fields = rFields.ToDictionary(x => x.Name, x => x);
                string Fieldname; string SaveVal;
                bool hasWavelength_Label = LookHere.KVPs.ContainsKey("WAVELENGTH_LABEL");
                foreach (KeyValuePair<string, object> XMLVal in LookHere.KVPs)
                {
                    if (XMLVal.Key.StartsWith("WAVEL") || XMLVal.Key.StartsWith("EMISSIONFILTER_NAME"))
                    {
                        Fieldname = XMLVal.Key;
                        if (Fieldname.Substring(Fieldname.Length - 2, 1) == "_") Fieldname = Fieldname.Substring(0, Fieldname.Length - 2);
                        if (Fieldname.Substring(Fieldname.Length - 3, 1) == "_") Fieldname = Fieldname.Substring(0, Fieldname.Length - 3);
                        if (!hasWavelength_Label && Fieldname == "EMISSIONFILTER_NAME") Fieldname = "WAVELENGTH_LABEL";
                        if (Fields.ContainsKey(Fieldname))
                        {
                            if (Fields[Fieldname].GetValue(Note) != null) //If we already added a value, then we should go to the next note
                            {
                                Note = new INCELL_WV_Note(); IWNs.Add(Note);
                            }
                            SaveVal = XMLVal.Value.ToString();
                            Fields[Fieldname].SetValue(Note, SaveVal);
                        }
                    }
                }
                if (IWNs.List[0].WAVELENGTH_INDEX == null) IWNs.List[0].WAVELENGTH_INDEX = "0"; //This is for No XDCE I think (swapped from the beginning on 9/1/2021)
                                                                                                //If the wavelengths aren't there, why add more 10/13/2021?
                bool addextra = false;
                if (addextra)
                {
                    for (int i = IWNs.Count; i < 5; i++)
                        IWNs.Add(new INCELL_WV_Note() { WAVELENGTH_INDEX = i.ToString() });
                }
                return IWNs;
            }

            public void CheckSave()
            {
                //Basically creates the file for this and saves it if it didn't exist
                if (!LoadedFromXML) Save();
            }

            private Dictionary<string, string> _RenameHash;
            [XmlIgnore]
            public Dictionary<string, string> RenameHash
            {
                get
                {
                    if (_RenameHash == null)
                    {
                        _RenameHash = new Dictionary<string, string>();
                        //_RenameHash.Add("1", "H"); _RenameHash.Add("2", "M"); _RenameHash.Add("3", "T"); _RenameHash.Add("4", "U");
                        foreach (INCELL_WV_Note item in List)
                        {
                            _RenameHash.Add(item.Index1.ToString(), item.Abbrev);
                        }
                    }
                    return _RenameHash;
                }
            }

            public bool HasAllAbbreviations
            {
                get
                {
                    int Count = 0;
                    foreach (INCELL_WV_Note note in List)
                    {
                        if (note.Abbrev != "-" && note.Abbrev != "") Count++;
                    }
                    return Count == List.Count;
                }
            }

            public string Rename(string originalName)
            {
                if (originalName.Contains(" wv"))
                {
                    string szWV = originalName.Substring(originalName.IndexOf(" wv") + 3);
                    string szNew = originalName.Substring(0, originalName.IndexOf(" wv") + 3);
                    if (RenameHash.ContainsKey(szWV))
                    {
                        return (szNew + RenameHash[szWV]).ToUpper().Replace("  ", " ").Trim();
                    }
                    else
                    {

                    }
                }
                return originalName.Trim().Replace("  ", " ").ToUpper();
            }
        }

        public class wvDisplayParam
        {
            private static char delim = ' ';
            public override string ToString()
            {
                return wvIdx_Txt + delim + Brightness_Txt + delim + Color;
            }

            public float Brightness { get; set; }
            public float Brightness_Low { get; set; } = 0;
            public string Brightness_Txt
            {
                get
                {
                    var tStr = Brightness.ToString();
                    if (Brightness_Low > 0) tStr += "/" + Brightness_Low.ToString();
                    return tStr;
                }
                set
                {
                    float T;
                    string valFirst = value;
                    valFirst = valFirst.Replace(",", "/");
                    if (value.Contains("/"))
                    {
                        var Arr = value.Split('/');
                        valFirst = Arr[0];
                        if (float.TryParse(Arr[1].Trim(), out T)) Brightness_Low = T;
                        else Brightness_Low = 0;
                    }
                    if (float.TryParse(valFirst.Trim(), out T)) Brightness = T;
                    else Brightness = 1;
                }
            }
            public short wvIdx { get; set; }
            public string wvIdx_Txt
            {
                get => wvIdx.ToString();
                set
                {
                    short T;
                    if (short.TryParse(value, out T)) wvIdx = T;
                    else wvIdx = 0;
                }
            }

            private string _Color;
            public string Color
            {
                get => _Color;
                set
                {
                    if (value == null) _Color = "FFFFF";
                    if (value == "") _Color = "FFFFF";
                    _Color = value;
                    FromString(_Color, out c_R, out c_G, out c_B);
                }
            }
            public static Regex regexString = new Regex("[^A-F0-9]", RegexOptions.Compiled);
            public static void FromString(string HexColor, out double cR, out double cG, out double cB)
            {
                if (HexColor == null) HexColor = "FFFFF";
                HexColor = HexColor.Trim().ToUpper();
                HexColor = regexString.Replace(HexColor, "");
                while (HexColor.Length < 6) HexColor += "0";
                cR = (double)Convert.ToUInt16(HexColor.Substring(0, 2), 16) / 255;
                cG = (double)Convert.ToUInt16(HexColor.Substring(2, 2), 16) / 255;
                cB = (double)Convert.ToUInt16(HexColor.Substring(4, 2), 16) / 255;
            }

            public static Color FromStringToColor(string HexColor)
            {
                double cR, cG, cB;
                FromString(HexColor, out cR, out cG, out cB);
                var C = System.Drawing.Color.FromArgb((byte)(cR * 255), (byte)(cG * 255), (byte)(cB * 255));
                return C;
            }

            [XmlIgnore]
            public double c_R;
            [XmlIgnore]
            public double c_G;
            [XmlIgnore]
            public double c_B;

            public bool Active { get; set; }

            public float Threshold { get; set; }
            public bool ObjectAnalysis { get; set; }
            public bool DI_Analysis { get; set; }
        }

        public class wvDisplayParams //: IEnumerable<wvDisplayParam> //Screws up serialization
        {
            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.Append(ColorCalcTypeStr);
                sb.Append(" ");
                sb.Append(UseClipping);
                sb.Append(" ");
                sb.Append(ClippingExpand_positive);
                sb.Append(",");
                sb.Append(ClippingExpand_negative);
                sb.Append(" ");
                string ActiveString = string.Join("|", Actives.Select(x => x.ToString()));
                sb.Append(ActiveString);
                return sb.ToString();
            }
            public List<wvDisplayParam> List
            {
                get { return _List; }
                set { _List = value; }
            }

            private List<wvDisplayParam> _List;
            public List<wvDisplayParam> Actives { get => _List.Where(x => x.Active).ToList(); }

            public List<wvDisplayParam> Tracings { get => _List.Where(x => x.ObjectAnalysis).ToList(); }

            public string ColorCalcTypeStr { get; set; }

            public bool UseClipping { get; set; }

            public int ClippingExpand_positive { get; set; }
            public int ClippingExpand_negative { get; set; }

            public bool UseSquareMode { get; set; }

            public int Count { get => _List.Count; }

            public wvDisplayParam this[int index] { get => _List[index]; }

            public wvDisplayParams()
            {
                _List = new List<wvDisplayParam>();
            }

            public IEnumerator<wvDisplayParam> GetEnumerator()
            {
                return _List.GetEnumerator();
            }

            public void Add(wvDisplayParam wvDisplayParam)
            {
                _List.Add(wvDisplayParam);
            }



            public static string DefaultPath = Path.Combine(Path.GetTempPath(), "FIVToolsCal_settings.xml");

            public static wvDisplayParams Load()
            {
                return Load(DefaultPath);
            }

            public static wvDisplayParams Load(string Path)
            {
                if (!File.Exists(Path)) goto JUMPHEREISSUE;
                try
                {
                    using (var stream = File.OpenRead(Path))
                    {
                        var serializer = new XmlSerializer(typeof(wvDisplayParams));
                        var T = (wvDisplayParams)serializer.Deserialize(stream);
                        if (T == null) goto JUMPHEREISSUE;
                        if (T.Count == 0)
                            T.Add(new wvDisplayParam() { Active = true, Brightness = 2, Color = "FFFFFF", wvIdx = 0, ObjectAnalysis = false, Threshold = 128 });
                        return T;
                    }
                }
                catch
                {
                    goto JUMPHEREISSUE;
                }
            JUMPHEREISSUE:
                var P = new wvDisplayParams();
                P.Add(new wvDisplayParam() { Active = true, Brightness = 2, Color = "FFFFFF", wvIdx = 0, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 1, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 2, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 3, ObjectAnalysis = false, Threshold = 128 });
                P.Add(new wvDisplayParam() { Active = false, Brightness = 2, Color = "FFFFFF", wvIdx = 4, ObjectAnalysis = false, Threshold = 128 });
                return P;
            }

            public void Save()
            {
                Save(DefaultPath);
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }
        }

        public class RaftImage_Export_Settings
        {
            public bool FileName_ByAnnotation { get; set; }
            public bool FolderName_ByAnnotation { get; set; }
            public bool Include_Fiducial_Rafts { get; set; }
            public string ExportFolderBase { get; set; }

            public bool SubFolderByPlateID = true;

            public bool SubFolderBy_PlateID_Well = false;
            public bool Resize_And_MLExport { get; set; }
            public float Min_AspectRatio_Keep { get; set; }
            public int PixelList_ExportSize { get; set; }//Makes a square image that gets exported just as pixel data (grayscale, 8 bit), only applies to exported ML
            public int PixelsExpand { get; set; }
            public bool TIF16Bit_Export { get; set; }
            public bool OnlyExportSquare { get; set; }
            public bool OnlyExportFullSize { get; set; }
            public string OnlyExportAnnotationPipeSep { get; set; }
            private HashSet<string> _onlyExportAnnotations = null;
            [XmlIgnore]
            public HashSet<string> OnlyExportAnnotations
            {
                get
                {
                    if (_onlyExportAnnotations == null)
                    {
                        if (OnlyExportAnnotationPipeSep == null) OnlyExportAnnotationPipeSep = "";
                        if (OnlyExportAnnotationPipeSep.Trim() == "")
                        {
                            _onlyExportAnnotations = new HashSet<string>();
                        }
                        else
                            _onlyExportAnnotations = new HashSet<string>(OnlyExportAnnotationPipeSep.ToUpper().Trim().Split('|'));
                    }
                    return _onlyExportAnnotations;
                }
            }
            public float ResizeMultiplier { get; set; }
            public float CropToMaxSize { get; set; }
            public string ImageExtension { get; set; }
            public bool Export_4Rotations { get; set; }
            public int CellImageExpandPixels { get; set; }
            public double DownsampleFraction { get; set; } //If you set this to 0.1, then it will only export 1 in every 10 cell images

            public string ColumnNameForAnnotation { get; set; }

            public RaftImage_Export_Settings()
            {
                TIF16Bit_Export = false;
            }

            public RaftImage_Export_Settings(RaftImage_Export_Settings Copy)
            {
                foreach (var item in typeof(RaftImage_Export_Settings).GetProperties())
                {
                    if (item.Name == "OnlyExportAnnotations") continue;
                    item.SetValue(this, item.GetValue(Copy));
                }
            }

            public static string DefaultPath = Path.Combine(Path.GetTempPath(), "FIVTools_RaftExport_Settings.xml");


            public static RaftImage_Export_Settings Load()
            {
                return Load(DefaultPath);
            }

            public static RaftImage_Export_Settings Load(string Path)
            {
                if (!File.Exists(Path)) goto JUMPHEREISSUE;
                try
                {
                    using (var stream = File.OpenRead(Path))
                    {
                        var serializer = new XmlSerializer(typeof(RaftImage_Export_Settings));
                        var RS = (RaftImage_Export_Settings)serializer.Deserialize(stream);
                        if (RS.DownsampleFraction <= 0) RS.DownsampleFraction = 1;
                        if (RS.ResizeMultiplier <= 0) RS.ResizeMultiplier = 1;
                        return RS;
                    }
                }
                catch
                {

                }
            JUMPHEREISSUE:
                RaftImage_Export_Settings P = Defaults();
                P.Save();
                return P;
            }

            public static RaftImage_Export_Settings Defaults()
            {
                var P = new RaftImage_Export_Settings
                {
                    ExportFolderBase = @"c:\temp\RaftExport\",
                    ImageExtension = "bmp",
                    FileName_ByAnnotation = false,
                    FolderName_ByAnnotation = true,
                    Resize_And_MLExport = false,
                    Include_Fiducial_Rafts = false,
                    Min_AspectRatio_Keep = 0.75F,
                    PixelsExpand = 0,
                    TIF16Bit_Export = false,
                    OnlyExportSquare = true,
                    OnlyExportFullSize = true,
                    PixelList_ExportSize = 52,
                    ResizeMultiplier = 1, // Settings.Resize_And_MLExport ? 0.84F : 1F;
                    CropToMaxSize = 500, // < 1 means don't do this, never expand
                    Export_4Rotations = false,
                    CellImageExpandPixels = 8,
                    DownsampleFraction = 1,
                    OnlyExportAnnotationPipeSep = ""
                };
                return P;
            }

            public void Save()
            {
                Save(DefaultPath);
            }

            public void Save(string Path)
            {
                using (var writer = new StreamWriter(Path))
                {
                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);
                    writer.Flush();
                }
            }

            public void SaveCurrent(INCELL_Folder iCFolder, wvDisplayParams WVParams, string Path)
            {
                if (WVParams == null) return;
                using (var writer = new StreamWriter(Path))
                {
                    writer.WriteLine(iCFolder.PlateID);

                    var serializer = new XmlSerializer(this.GetType());
                    serializer.Serialize(writer, this);

                    var X = WVParams;
                    serializer = new XmlSerializer(X.GetType());
                    serializer.Serialize(writer, X);

                    writer.Flush();
                }
            }
        }

        public class INCELL_Folder
        {
            Regex rePlateIndex = new Regex(@"\D+\d+\D+(\d+)", RegexOptions.Compiled);
            static Regex Reg = new System.Text.RegularExpressions.Regex(@"\D+(\d+)\..+", RegexOptions.Compiled);
            Regex TimePointReg = new Regex(@"^timepoint\d+$", RegexOptions.Compiled);
            Regex TimePointInt = new Regex(@"\d+$", RegexOptions.Compiled);
            Regex RFiv = new Regex(@"(FI\D\d\d\d)\D", RegexOptions.Compiled);

            public static string XDCEFileContains = ".xdce";

            public XDCE XDCE;
            public string FullPath { get; set; }

            public string timePointFullPath { get; set; }

            public bool isNewFormat { get { return timePointFullPath != null; } }
            public string AdjustedPath
            {
                get
                {
                    return AdjustPathOverall(this.BasePath_Saved, this.BasePath_Current, FullPath);
                }
            }

            public static string DefaultMaskFolderName = "Masks";
            public static string DefaultMaskListName = "MaskList.txt";
            public string MaskList_FullPath { get => Path.Combine(MaskFolder, DefaultMaskListName); }
            public string MaskFolder { get => Path.Combine(this.AdjustedPath, DefaultMaskFolderName); }
            public static string AdjustPathOverall(string PrevBase, string NewBase, string PathToAdjust)
            {
                // Default PrevBase to a specific value if null
                if (PrevBase == null)
                    PrevBase = "E:\\Imaging\\";
                PrevBase = PrevBase.Trim();
                NewBase = NewBase.Trim();
                PathToAdjust= PathToAdjust.Trim();
                // If PrevBase and NewBase are equivalent (case-insensitive), return the path unchanged
                if (string.Equals(PrevBase, NewBase, StringComparison.OrdinalIgnoreCase))
                    return PathToAdjust;

                // Replace PrevBase with NewBase in PathToAdjust (case-insensitive)
                return PathToAdjust.Replace(PrevBase, NewBase, StringComparison.OrdinalIgnoreCase);
            }

            public static string AdjustPathOverall(INCELL_Folder Folder, string PathToAdjust)
            {
                string PrevBase = Folder.BasePath_Saved;
                string NewBase = Folder.BasePath_Current;
                char backslash = '\\';
                if (PrevBase.EndsWith(backslash) && !NewBase.EndsWith(backslash))
                    NewBase += backslash;
                if (!PrevBase.EndsWith('\\') && NewBase.EndsWith(backslash))
                    PrevBase += backslash;

                return AdjustPathOverall(PrevBase, NewBase, PathToAdjust);
            }

            private ConcurrentDictionary<string, XDCE_ImageGroup> _Rafts = null;
            [XmlIgnore]
            public ConcurrentDictionary<string, XDCE_ImageGroup> Rafts
            {
                get
                {
                    if (_Rafts == null)
                    {
                        _Rafts = new ConcurrentDictionary<string, XDCE_ImageGroup>();
                        foreach (XDCE_ImageGroup tWell in XDCE.Wells.Values)
                        {
                            foreach (KeyValuePair<string, XDCE_ImageGroup> KVP in tWell.Rafts)
                            {
                                if (!_Rafts.ContainsKey(KVP.Key))
                                    _Rafts.TryAdd(KVP.Key, KVP.Value);
                            }
                        }
                    }
                    return _Rafts;
                }
            }

            public string AQP { get => AcquisitionProtocol; }
            public string AcquisitionProtocol { get; set; }

            private string _PlateID_NoScanIndex = "|-`1|";
            public string PlateID_NoScanIndex
            {
                get
                {
                    if (_PlateID_NoScanIndex == "|-`1|")
                    {
                        _PlateID_NoScanIndex = PlateID;
                        int l = PlateID.LastIndexOf("_"); int idx2;
                        string idx = PlateID.Substring(l + 1);
                        if (int.TryParse(idx, out idx2))
                        {
                            _PlateID_NoScanIndex = PlateID.Substring(0, l);
                        }
                    }
                    return _PlateID_NoScanIndex;
                }
            }

            private string _PlateIndex = "|-1|";
            public string PlateIndex
            {
                get
                {
                    if (_PlateIndex == "|-1|")
                    {
                        var M = rePlateIndex.Match(PlateID_NoScanIndex);
                        if (M == null) _PlateIndex = "-1";
                        else _PlateIndex = M.Groups[1].Value.Trim();
                    }
                    return _PlateIndex;
                }
            }

            [XmlIgnore]
            public bool Type_NoXDCE = false;

            [XmlIgnore]
            public Tuple<short, double, int, string> NoXDCE_Params;

            public string PlateID { get; set; }
            [XmlIgnore]
            public string BasePath_Current { get; set; }
            public string BasePath_Saved { get; set; }

            private bool _AnalysisFolder_ReAssignParents = false;
            private List<INCARTA_Analysis_Folder> _AnalysisFolders;
            public List<INCARTA_Analysis_Folder> AnalysisFolders
            {
                get
                {
                    if (!_AnalysisFolder_ReAssignParents && _AnalysisFolders != null)
                    {
                        if (_AnalysisFolders.Count > 0)
                        {
                            foreach (INCARTA_Analysis_Folder IAF in _AnalysisFolders)
                            {
                                IAF.Parent = this;
                            }
                            _AnalysisFolder_ReAssignParents = true;
                        }
                    }
                    return _AnalysisFolders;
                }
                set
                {
                    _AnalysisFolders = value;
                }
            }

            public static INCELL_Folder CreateWithNoXDCE(string Fldr, INCELL_WV_Notes WVNotes)
            {
                return CreateWithNoXDCE(Fldr, WVNotes.NoXDCE_ChannelCount, WVNotes.NoXDCE_WellLabel, WVNotes.NoXDCE_PixelWidthMicrons, WVNotes.NoXDCE_ImageWidthPixels);
            }

            /// <summary>
            /// Creates the variables even when there is no XDCE file Present
            /// </summary>
            public static INCELL_Folder CreateWithNoXDCE(string Fldr, short Channels, string WellLabel = "A - 1", double PixelWidth_um = 0.325, int ImageWidth_Pixels = 2048)
            {
                //TODO, this should be built right into the main Constructor, but if there is no XDCE file found, it defaults to this
                var IC_F = new INCELL_Folder();
                var Well = new XDCE_ImageGroup(); Well.NameAtLevel = WellLabel; var DI = new DirectoryInfo(Fldr);
                IC_F.PlateID = DI.Name; IC_F.FullPath = Fldr;
                IC_F.XDCE = new XDCE(); IC_F.XDCE.KVPs = new Dictionary<string, object>();
                IC_F.XDCE.ParentFolder = Well.ParentFolder = IC_F;
                IC_F.XDCE.FilePath = Well.FilePath = IC_F.XDCE.FolderPath = Well.FolderPath = Fldr;
                IC_F.XDCE.Wavelength_Count = Channels;
                IC_F.XDCE.PixelWidth_in_um = IC_F.XDCE.PixelHeight_in_um = PixelWidth_um;
                IC_F.XDCE.ImageWidth_in_Pixels = IC_F.XDCE.ImageWidth_in_Pixels = ImageWidth_Pixels;
                IC_F.XDCE.Wells.Add(Well.NameAtLevel, Well);
                IC_F.Type_NoXDCE = true;
                IC_F.NoXDCE_Params = new Tuple<short, double, int, string>(Channels, PixelWidth_um, ImageWidth_Pixels, WellLabel);

                XDCE_Image xI; string FileOnly; string Ext; int FileNum = 0; int FOV; short wv; Dictionary<string, int> FOVTrack = new Dictionary<string, int>();

                var FOV_Countup = 0;
                foreach (string file in Directory.EnumerateFiles(Fldr))
                {
                    FileOnly = Path.GetFileName(file); Ext = Path.GetExtension(file).ToUpper();
                    if (FileOnly.StartsWith("._") || FileOnly == "thumbs.db") continue;
                    if (Ext == ".TXT" || Ext == ".XML") continue;
                    if (FileOnly.EndsWith("Illum.tif"))
                    {
                        string[] FParts = FileOnly.Split('_');
                        string FKey = FParts[0] + "_" + FParts[1];
                        if (!FOVTrack.ContainsKey(FKey)) FOVTrack.Add(FKey, FOVTrack.Count);
                        FOV = FOVTrack[FKey];
                        wv = (short)(FileOnly.Contains("DAPI") ? 0 : 1);
                    }
                    else
                    {
                        var M = Reg.Match(FileOnly); if (M.Success) if (M.Groups.Count == 2) { FileNum = int.Parse(M.Groups[1].Value); }
                        FOV = (int)((double)(FileNum + 1) / Channels) - 1;
                        wv = (short)((FileNum - 1) % Channels);
                    }
                    if (wv < 0) wv = 0;
                    if (FOV < 0) FOV = FOV_Countup++;
                    xI = new XDCE_Image(file, IC_F.XDCE, Well.NameAtLevel, FOV, wv);
                    Well.Add(xI);
                }
                return IC_F;
            }

            private DateTime _LatestEvent = DateTime.MinValue;
            public DateTime LatestEvent
            {
                get
                {
                    if (_LatestEvent == DateTime.MinValue)
                    {
                        FileInfo FI = new FileInfo(this.XDCE.FilePath);
                        _LatestEvent = FI.CreationTime;
                    }
                    return _LatestEvent;
                }
            }

            private INCELL_WV_Notes _InCell_Wavelength_Notes = null;

            [XmlIgnore]
            public INCELL_WV_Notes InCell_Wavelength_Notes
            {
                get
                {
                    if (_InCell_Wavelength_Notes == null)
                    {
                        //First try to load from a saved file, if not regenerate from the XDCE
                        _InCell_Wavelength_Notes = INCELL_WV_Notes.Load_Folder(this.FullPath);
                        if (_InCell_Wavelength_Notes == null)
                        {
                            if (XDCE != null) _InCell_Wavelength_Notes = INCELL_WV_Notes.GetFromXDCE(XDCE, FullPath);
                        }
                        else if (this.AnalysisFolders.Count > 0 && !this.AnalysisFolders[0].isNewFormat)
                        {
                            _InCell_Wavelength_Notes.XDCEFolder = this.XDCE.FolderPath; //This ensures that we don't load the wrong folder if things were moved
                        }
                        if (_InCell_Wavelength_Notes.List.Count > 0 && _InCell_Wavelength_Notes.List.Last().WAVELENGTH_IMAGING_MODE == null)
                        {
                            //This is to try and get rid of empty wv that sometimes creep in . . 
                            _InCell_Wavelength_Notes.List.RemoveAt(_InCell_Wavelength_Notes.Count - 1);
                        }
                    }
                    return _InCell_Wavelength_Notes;
                }
            }

            public override string ToString()
            {
                return PlateID;
            }

            public INCELL_Folder()
            {
                FullPath = ""; PlateID = ""; AcquisitionProtocol = "";
                AnalysisFolders = new List<INCARTA_Analysis_Folder>();
            }

            public INCELL_Folder(string XDCEFolder, string parentFolder = null, bool IncartaTouched = false)
            {
                FileAttributes attr = File.GetAttributes(XDCEFolder);
                if (!attr.HasFlag(FileAttributes.Directory))
                {
                    FullPath = Path.GetDirectoryName(XDCEFolder);
                }
                else
                {
                    FullPath = XDCEFolder;
                }
                if (parentFolder != null)
                    FullPath = parentFolder;

                //Now check whether there is an XDCE File
                if (IncartaTouched)
                {
                    try
                    {
                        // Load the XML document
                        XDocument doc = XDocument.Load(XDCEFolder);

                        // Find the 'Images' element and get the 'path' attribute
                        XElement imagesElement = doc.Descendants("Images").FirstOrDefault();
                        if (imagesElement != null)
                        {
                            XAttribute pathAttribute = imagesElement.Attribute("path");
                            if (pathAttribute != null)
                            {
                                FullPath = pathAttribute.Value;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"An error occurred: {ex.Message}");
                    }
                }
                if (XDCE.GetXDCEPath(XDCEFolder) == "")
                {
                    //Can't do this in here, but you can do this >
                    //CreateWithNoXDCE();
                }
                else
                {
                    List<string> directories = Directory.GetDirectories(parentFolder, "timepoint*").ToList();
                    bool xdce_info = XDCEFolder.ToLower().Contains("xdce_info");
                    XDCE = null;
                    if (xdce_info)
                    {

                        timePointFullPath = parentFolder + "\\timepoint0";
                        XDCE = new XDCE(XDCEFolder, false, timePointFullPath);
                    }
                    else
                    {
                        XDCE = new XDCE(XDCEFolder, false);
                    }

                    if (directories.Count > 1 && xdce_info)
                    {
                        XDCE.timePointPaths = string.Join("|", directories);
                        // Map timepoint folders for quick lookup
                        Dictionary<int, string> timepointMap = directories
                         .Select(dir => new
                         {
                             Path = dir,
                             Name = Path.GetFileName(dir)
                         })
                         .Where(d => TimePointReg.Match(d.Name).Success) // Ensure it matches "timepoint" format
                         .ToDictionary(
                             d => int.Parse(TimePointInt.Match(d.Name).Value), // Extract integer from "timepointX"
                             d => d.Path
                         );
                        XDCE.Images.ForEach(t =>
                        {
                            // Extracting and assigning TimeSeriesOrder from the filename (e.g., "t1_B01_s1_w1_z1")
                            // Assuming the time series order is indicated by the first numeric value after 't'
                            if (int.TryParse(t.FileName[0..t.FileName.IndexOf('_')], out int order))
                            {
                                t.TimeSeriesOrder = order;
                                t.IsTimeSeries = true;
                            }
                        });
                        // Update each image's FullPath to the corresponding timepoint directory

                        for (int i = 0; i < XDCE.Images.Count; i++)
                        {
                            XDCE_Image image = XDCE.Images[i];

                            // Find the matching timepoint folder using TimeSeriesOrder
                            if (timepointMap.TryGetValue(image.TimeSeriesOrder, out string matchingPath))
                            {
                                // Replace FullPath to point to the correct directory
                                image.FullPath = matchingPath + "\\" + image.FileName;
                            }
                            else
                            {
                                // Handle cases where the timepoint folder does not exist
                                Debug.WriteLine($"Warning: No matching folder for TimeSeriesOrder {image.TimeSeriesOrder}");
                            }
                        }
                    }
                    else
                    {
                        if (directories.Count > 1)
                        {
                            XDCE.timePointPaths = parentFolder + "\\timepoint0";
                        }

                    }
                    XDCE.ParentFolder = this;
                    DirectoryInfo DI = new DirectoryInfo(FullPath);
                    DirectoryInfo DP = DI.Parent;
                    AcquisitionProtocol = DP.Name;
                    int expected_PlateIDLen = (DI.Name.Length - AcquisitionProtocol.Length) - 1;
                    PlateID = DI.Name.ToUpper().Replace(AcquisitionProtocol.ToUpper() + "_", ""); //Problem with this is it may multi-replace
                    if (PlateID.Length < (expected_PlateIDLen - 1))
                    {
                        PlateID = DI.Name.Substring(AcquisitionProtocol.Length + 1);
                    }

                    DirectoryInfo DI_Analysis = new DirectoryInfo(Path.Combine(FullPath, "Results"));
                    if (DI_Analysis.Exists)
                        AnalysisFolders = INCARTA_Analysis_Folder.GetAnalysesFromFolder(DI_Analysis.FullName, this);
                    else
                        AnalysisFolders = new List<INCARTA_Analysis_Folder>();
                }
            }

            public static List<INCELL_Folder> GetFromFolder(string BasePath) { return GetFromFolder(BasePath, null); }

            public static List<INCELL_Folder> GetFromFolder(string BasePath, BackgroundWorker BW, string LimitSearch = "")
            {
                ConcurrentDictionary<string, string[]> directoryCache = new ConcurrentDictionary<string, string[]>();
                ConcurrentQueue<(FileInfo, string)> filesBag = new ConcurrentQueue<(FileInfo, string)>();
                ConcurrentQueue<string> progressBatch = new ConcurrentQueue<string>(); // Simpler collection for batching
                ConcurrentBag<INCELL_Folder> Folders = new ConcurrentBag<INCELL_Folder>();
                int processedCount = 0;

                // Directory caching helper
                EnumerationOptions options = new EnumerationOptions();
                options.BufferSize = 16384;
                options.IgnoreInaccessible = true;
                options.AttributesToSkip = FileAttributes.SparseFile | FileAttributes.Hidden | FileAttributes.System | FileAttributes.Temporary | FileAttributes.Offline;
                string[] GetCachedDirectories(string path) => directoryCache.GetOrAdd(path, p => Directory.GetDirectories(p, "*", options));
                object reportProgressLock = new object();
                // Parallel processing of subdirectories
                // Lock outside of the array not inside.
                Parallel.ForEach(Directory.EnumerateDirectories(BasePath, "*", options), DIsubPath =>
                {
                    try
                    {
                        foreach (string DIPlatePath in GetCachedDirectories(DIsubPath))
                        {
                            string plateName = Path.GetFileName(DIPlatePath);

                            // Filter by LimitSearch
                            if (!string.IsNullOrEmpty(LimitSearch) && !plateName.Contains(LimitSearch, StringComparison.OrdinalIgnoreCase))
                                continue;

                            try
                            {
                                // Determine XDCE path
                                string xdcePath = XDCE.GetXDCEPath(DIPlatePath);
                                if (!string.IsNullOrEmpty(xdcePath))
                                {
                                    filesBag.Enqueue((new FileInfo(xdcePath), DIPlatePath));
                                    progressBatch.Enqueue(plateName);
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Print($"Error processing: {DIPlatePath} - {ex.Message}");
                                if (BW != null)
                                    BW.ReportProgress(0, $"Error processing {DIPlatePath}: {ex.Message}");
                                continue;
                            }
                        }
                        int currentCount = Interlocked.Add(ref processedCount, progressBatch.Count);
                        if (BW != null)
                        {
                            lock (reportProgressLock)
                            {
                                string[] snapshot = progressBatch.ToArray(); // Snapshot the current state
                                progressBatch.Clear();                  // Clear the queue
                                BW.ReportProgress(currentCount, $"Processed items: {string.Join(", ", snapshot)}");
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        return;
                    }
                });

                // Final progress flush
                if (BW != null)
                {
                    if (progressBatch.Any())
                    {
                        BW.ReportProgress(100, $"Final Names: {string.Join(", ", progressBatch.ToArray())}");
                    }
                }

                if (string.IsNullOrEmpty(LimitSearch))
                {
                    // Parallel processing for the unfiltered case
                    Parallel.ForEach(filesBag.DistinctBy(x => x.Item1.FullName), fileTuple =>
                    {
                        Folders.Add(new INCELL_Folder(fileTuple.Item1.FullName, fileTuple.Item2)
                        {
                            BasePath_Current = BasePath
                        });
                    });
                }
                else
                {
                    // Parallel processing for the filtered case
                    Parallel.ForEach(filesBag.DistinctBy(x => x.Item1.FullName), fileTuple =>
                    {
                        var (file, DIPlateName) = fileTuple;
                        if (file.Name.Contains(LimitSearch, StringComparison.OrdinalIgnoreCase))
                        {
                            Folders.Add(new INCELL_Folder(file.FullName, DIPlateName)
                            {
                                BasePath_Current = BasePath
                            });
                        }
                    });
                }
                if (BW == null)
                    Debug.Print("Processing complete.");
                else
                    BW.ReportProgress(100, "Processing complete.");

                return Folders.OrderBy(t=>t.FullPath).ToList();
            }
            internal void RemoveAF(INCARTA_Analysis_Folder AF)
            {
                AnalysisFolders.Remove(AF);
            }

            internal void ReAssociate(string BasePath)
            {
                BasePath_Current = BasePath;
                XDCE.ParentFolder = this;
            }

            /// <summary>
            /// The point of this is to regenerate (quickly) and see what new images are present, usually for the NoXDCE version
            /// </summary>
            public void RefreshImages()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Intended to be used when reconstructing to a different mag
            /// </summary>
            public static INCELL_Folder CopyFrom(INCELL_Folder ToCopy, string PlateID, float Magnification, bool MemoryOnly = false)
            {
                var ICFolder_New = new INCELL_Folder();
                string ParentPath = Directory.GetParent(ToCopy.FullPath).FullName;
                ICFolder_New.FullPath = Path.Combine(ParentPath, ToCopy.AcquisitionProtocol + "_" + PlateID);
                ICFolder_New.PlateID = PlateID;
                if (!MemoryOnly)
                    Directory.CreateDirectory(ICFolder_New.FullPath);
                //Process.Start(new ProcessStartInfo(ICFolder_New.FullPath) { UseShellExecute = true });

                double MagRatio = ToCopy.XDCE.ObjectiveMagnification / Magnification;
                //Have to copy more stuff over about the Folder
                ICFolder_New.XDCE = new XDCE();
                ICFolder_New.XDCE.ParentFolder = ICFolder_New;
                ICFolder_New.XDCE.KVPs = ToCopy.XDCE.KVPs;

                ICFolder_New.XDCE.FolderPath = ICFolder_New.FullPath;
                ICFolder_New.XDCE.FilePath = Path.Combine(ParentPath, ToCopy.AcquisitionProtocol + "_" + PlateID, ToCopy.AcquisitionProtocol + "_" + PlateID + ".xdce");
                ICFolder_New.XDCE.ObjectiveMagnification = Magnification;
                ICFolder_New.XDCE.PixelWidth_in_um = ToCopy.XDCE.PixelWidth_in_um * MagRatio;
                ICFolder_New.XDCE.PixelHeight_in_um = ToCopy.XDCE.PixelHeight_in_um * MagRatio;
                ICFolder_New.XDCE.ImageWidth_in_Pixels = ToCopy.XDCE.ImageWidth_in_Pixels;
                ICFolder_New.XDCE.ImageHeight_in_Pixels = ToCopy.XDCE.ImageHeight_in_Pixels;

                ICFolder_New.AcquisitionProtocol = ToCopy.AcquisitionProtocol;

                return ICFolder_New;
            }

            public bool HasMasks
            {
                get
                {
                    DirectoryInfo DI = new DirectoryInfo(MaskFolder);
                    return DI.Exists;
                }
            }

            private string _FIViD = "";
            public string FIViD
            {
                get
                {
                    if (_FIViD == "")
                    {
                        if (PlateID.Length < 6) return PlateID;
                    
                        Match M = RFiv.Match(PlateID);
                        if (M.Success) _FIViD = M.Groups[1].Value; else _FIViD = PlateID.Substring(6);
                    }
                    return _FIViD;
                }
                set { _FIViD = value; }
            }

            public static string ErrorLog { get; set; }
        }

        public class INCARTA_Analysis_Folder
        {
            static Regex Re = new Regex(@"(\w+-\w+\d-).+\(\w\d+ \w\d+ \w\d+ W(\d+)\)(.*)", RegexOptions.Compiled);

            public static string CellAnalysisFileContains = "_Single_Target_Data.csv";
            public static string NewCellAnalysisFileContains = "_ObjectData.csv";
            public static List<string> FilesInFolder_INCARTAAnalysis = new List<string>(2) { NewCellAnalysisFileContains, CellAnalysisFileContains };

            public static List<INCARTA_Analysis_Folder> GetAnalysesFromFolder(string BasePath, INCELL_Folder ParentFolder)
            {
                DirectoryInfo DI = new DirectoryInfo(BasePath);
                INCARTA_Analysis_Folder IAF;
                List<INCARTA_Analysis_Folder> Analyses = new List<INCARTA_Analysis_Folder>();
                List<INCARTA_Analysis_Folder> TList;
                if (ContainsAnalysisFiles(DI))
                {
                    IAF = GetAnalysisFromFolder(DI.FullName, ParentFolder);
                    Analyses.Add(IAF); return Analyses;
                }
                foreach (DirectoryInfo DISub in DI.GetDirectories())
                {
                    if (DISub.Name == "thumbs" || DISub.Name == "temp") continue;
                    TList = GetAnalysesFromFolder(DISub.FullName, ParentFolder);
                    Analyses.AddRange(TList);
                }
                return Analyses;
            }
            public static INCARTA_Analysis_Folder GetAnalysisFromFolder(string fullName, INCELL_Folder ParentFolder)
            {
                INCARTA_Analysis_Folder IAF = new INCARTA_Analysis_Folder(fullName, ParentFolder);
                return IAF;
            }
            public static bool ContainsAnalysisFiles(DirectoryInfo DI)
            {
                foreach (string item in FilesInFolder_INCARTAAnalysis)
                {
                    FileInfo[] files = DI.GetFiles("*" + item, SearchOption.TopDirectoryOnly);
                    if (files.Length > 0) return true;
                }
                return false;
            }

            //----------------------------------------------------------------------------------------

            public string FolderPath { get; set; }
            public bool isNewFormat
            {
                get
                {
                    return RelatedXDCE.FolderPath.ToUpper().Contains("XDCE_INFO");
                }
            }
            public string FolderPath_GenImages { get => Path.Combine(FolderPath, "GeneratedImages"); }
            public string FolderName { get; set; }
            public string Path_CellExport { get; set; }
            public List<string> newPaths_CellExport { get; set; }
            public string Path_CellExport_Adjusted
            {
                get
                {
                    return INCELL_Folder.AdjustPathOverall(Parent, Path_CellExport);
                }
            }

            public string AQP { get => AcquisitionProtocol; }
            public string AcquisitionProtocol { get; set; }
            public string PlateID { get; set; }
            public string INCARTA_ProtocolName { get; set; }
            public string ResultsID { get; set; }
            [XmlIgnore]
            public XDCE RelatedXDCE { get => Parent.XDCE; }
            [XmlIgnore]
            public INCELL_Folder Parent { get; set; }

            public int Index { get; set; }
            private static int IndexCounter = 0;

            public override string ToString()
            {
                return PlateID;
            }

            public INCARTA_Analysis_Folder(string FolderPath, INCELL_Folder ParentFolder)
            {
                Parent = ParentFolder;
                this.FolderPath = FolderPath;
                Init();
            }

            public INCARTA_Analysis_Folder()
            {
                Parent = null;
                this.FolderPath = "";
                Init();
            }
    
            private void Init()
            {
                Index = IndexCounter++;
                newPaths_CellExport = new List<string> { };
                if (FolderPath != "")
                {
                    string ParentName = "";
                    DirectoryInfo DI = new DirectoryInfo(FolderPath);
                    DirectoryInfo dataSamplingDir = DI.GetDirectories("DataSampling").FirstOrDefault();
                    if (dataSamplingDir != null)
                    {
                        FileInfo biggestFile = dataSamplingDir.GetFiles("*.csv")
                            .OrderByDescending(f => f.Length)
                            .FirstOrDefault();
                        if (biggestFile != null)
                        {
                            string fileName = biggestFile.Name;
                            ParentName = fileName.Replace("_data_Sampling\\.csv",  "", StringComparison.OrdinalIgnoreCase);
                        }
                    }
                    //Possibly the bracket goes here and not at the end 8/28/2024
                    if (NewCellAnalysisFileContains == "_ObjectData.csv")
                    {
                        if (ParentName == "")
                            NewCellAnalysisFileContains = "_Cells" + NewCellAnalysisFileContains;
                        else
                            NewCellAnalysisFileContains = "_" + ParentName + NewCellAnalysisFileContains;
                    }
                    //Determine the Cell Analysis File (CSV)
                    foreach (FileInfo file in DI.GetFiles())
                    {
                        if (file.Name.Contains(CellAnalysisFileContains) || file.Name.Contains(NewCellAnalysisFileContains))
                        {
                            Path_CellExport = file.FullName;
                            break;
                        }
                    }
                    //Figure out the name
                    DirectoryInfo DP = DI.Parent;
                    DP = DP.Parent;
                    FolderName = DP.Name;
                    AcquisitionProtocol = DP.Parent.Name;
                    PlateID = FolderName.Replace(AcquisitionProtocol + "_", "");
                    int l1 = DI.Name.LastIndexOf("_");
                    if (l1 < 2)
                    {
                        INCARTA_ProtocolName = "_Unknown";
                        ResultsID = DI.Name;
                    }
                    else
                    {
                        INCARTA_ProtocolName = DI.Name.Substring(0, l1);
                        ResultsID = DI.Name.Substring(l1 + 1);
                    }
                }
            }

            internal static string ExportLine(char delim)
            {
                return string.Join(delim, new string[] { "AQP", "PlateID", "INCARTA_ProtocolName", "ResultsID", "Index" });
            }

            internal string ExportLineThis(char delim)
            {
                return string.Join(delim, new string[] { AQP, PlateID, INCARTA_ProtocolName, ResultsID, Index.ToString() });
            }

            public void Delete()
            {
                DirectoryInfo DI = new DirectoryInfo(this.FolderPath);
                //Deletes this whole analysis folder and removes from Parent
                Parent.RemoveAF(this);
                DI.Delete(true);
            }

            /// <summary>
            /// This isn't implemented yet
            /// </summary>
            public void ExportTracedImages()
            {
                foreach (XDCE_Image image in RelatedXDCE.Images)
                {
                    string f = image.FileName_GenImageBase;
                    string h = this.FolderPath_GenImages;

                    //lbl-cell0-B-2(F1 Z0 T0 W2)
                    //lbl-cell0-B-3(F48 Z0 T0 W2)_cell_zoi
                    //lbl-nuc0-B-2(F4 Z0 T0 W1)
                    //seg-cell0-B-3(F8 Z0 T0 W2)
                    //seg-nuc0-B-2(F24 Z0 T0 W1)

                }
            }

            [XmlIgnore]
            public string[] GenImages_AppendList
            {
                get => GenImages_Dict.Keys.ToArray();
            }

            [XmlIgnore]
            public Dictionary<string, string[]> GenImages_Dict;

            public void Refresh_GenList()
            {
                GenImages_Dict = new Dictionary<string, string[]>();

                DirectoryInfo DI = new DirectoryInfo(FolderPath_GenImages);
                List<string> tList = new List<string>();

                MatchCollection MC; string t; string key; string[] arr;
                foreach (FileInfo fi in DI.GetFiles())
                {
                    t = Path.GetFileNameWithoutExtension(fi.Name);
                    MC = Re.Matches(t); if (MC.Count != 1) continue;
                    //if (!MC[0].Groups[1].Value.StartsWith("seg")) continue; //this exclude the labels, which are encoded into the tifs
                    arr = new string[3] { MC[0].Groups[1].Value, MC[0].Groups[2].Value, MC[0].Groups[3].Value };
                    key = String.Join("|", arr);
                    if (!GenImages_Dict.ContainsKey(key)) GenImages_Dict.Add(key, arr);
                }

                //>The segs with no other _XXX are usually the best ones to display
                //                _wcells == bin3
                //_skel = _imskel(very thin, skeletons), _imBF similar, but less scaled
                //_prob > saled does have data. .can see neurites

            }
        }
    }


}
