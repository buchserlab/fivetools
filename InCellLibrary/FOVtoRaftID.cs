﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace FIVE
{
    namespace FOVtoRaftID
    {
        //------------------------------------------------------------------------------------------------------------
        //Data Objects
        //------------------------------------------------------------------------------------------------------------


        /// <summary>
        /// Calibration information
        /// </summary>
        public class CalibrationRafts
        {
            public CalibrationRafts()
            {
                //To Serialize we need an empty constructor
                Dirty = false;
            }

            // constructor, order does not matter!
            public CalibrationRafts(KnownRaft raftOne, KnownRaft raftTwo, KnownRaft raftThree, KnownRaft raftFour)
            {
                Init(raftOne, raftTwo, raftThree, raftFour);
            }

            private void Init(KnownRaft raftOne, KnownRaft raftTwo, KnownRaft raftThree, KnownRaft raftFour)
            {
                // fudge factor for if rafts make a perfectly vertical line (which is bad in cartesia)
                double fudgeFactor = 0.000001;

                CalibrationVersion = "4-Point 01"; //So we can distinguish

                // sort by x coordinate
                KnownRaft[] xOrder = new KnownRaft[4];
                xOrder[0] = raftOne;
                xOrder[1] = raftTwo;
                xOrder[2] = raftThree;
                xOrder[3] = raftFour;
                string sBT = "";
                for (int i = 0; i < xOrder.Length; i++)
                {
                    xOrder[i].Parent = this; //Know who my parent is 5/24/2022

                    // this code reorders them so that they are sorted by x - Willie should convert to a sorted structure
                    if (true)
                    {
                        for (int j = i + 1; j < xOrder.Length; j++)
                        {
                            if (xOrder[j].X < xOrder[i].X)
                            {
                                KnownRaft temp = xOrder[j]; xOrder[j] = xOrder[i]; xOrder[i] = temp;
                                sBT += "ReOr ";
                            }
                        }
                    }
                }

                // assign known rafts in the correct orientation
                if (xOrder[0].Y < xOrder[1].Y)
                {
                    sBT += "01a ";
                    this.UpperLeft = xOrder[0];
                    this.LowerLeft = xOrder[1];
                }
                else
                {
                    sBT += "01b ";
                    this.UpperLeft = xOrder[1];
                    this.LowerLeft = xOrder[0];
                }

                if (xOrder[2].Y < xOrder[3].Y)
                {
                    sBT += "23a ";
                    this.UpperRight = xOrder[2];
                    this.LowerRight = xOrder[3];
                }
                else
                {
                    sBT += "23b ";
                    this.UpperRight = xOrder[3];
                    this.LowerRight = xOrder[2];
                }

                if (this.UpperLeft.X == this.LowerLeft.X)
                {
                    sBT += "LeftFF ";
                    this.UpperLeft.X += fudgeFactor;
                }

                if (this.UpperRight.X == this.LowerRight.X)
                {
                    sBT += "RightFF ";
                    this.UpperRight.X += fudgeFactor;
                }
                Debug.Print(sBT);
            }

            public CalibrationRafts(List<KnownRaft> knownRafts)
            {
                Init(knownRafts[0], knownRafts[1], knownRafts[2], knownRafts[3]);
            }

            public KnownRaft LowerLeft { get; set; }
            public KnownRaft UpperLeft { get; set; }
            public KnownRaft UpperRight { get; set; }
            public KnownRaft LowerRight { get; set; }
            public DateTime DateCalibrated { get; set; }
            [XmlIgnore]
            public bool Dirty;
            public string Well { get; set; }

            private Dictionary<string, KnownRaft> _KnownRaftSet = null;
            [XmlIgnore]
            public Dictionary<string, KnownRaft> KnownRaftSet
            {
                get
                {
                    if (_KnownRaftSet == null)
                    {
                        _KnownRaftSet = new Dictionary<string, KnownRaft>();
                        foreach (var R in KnownRafts) _KnownRaftSet.Add(R.RaftID, R);
                    }
                    return _KnownRaftSet;
                }
            }

            private List<KnownRaft> _KnownRafts = null;
            [XmlIgnore]
            public List<KnownRaft> KnownRafts
            {
                get
                {
                    if (_KnownRafts == null)
                    {
                        if (LowerLeft != null && LowerRight != null) _KnownRafts = new List<KnownRaft>(4) { LowerLeft, UpperLeft, UpperRight, LowerRight };
                        else _KnownRafts = new List<KnownRaft>(4);
                    }
                    return _KnownRafts;
                }
            }

            /// <summary>
            /// Check whether any of the RAFT IDs in the provided list are Calibration Rafts
            /// </summary>
            /// <param name="RaftIDs">List of RAFT IDs</param>
            /// <returns>The Known Raft Objects for those rafts in the list </returns>
            public IEnumerable<KnownRaft> KnownRaftsContained(IEnumerable<string> RaftIDs)
            {
                List<KnownRaft> tList = new List<KnownRaft>();
                foreach (var RaftID in RaftIDs)
                {
                    if (KnownRaftSet.ContainsKey(RaftID))
                    {
                        tList.Add(KnownRaftSet[RaftID]);
                        KnownRaftSet[RaftID].Parent = this; //Not sure why it sometimes loses this . . 
                    }
                }
                return tList;
            }

            public ReturnRaft FindRaftID_RR(InCellLibrary.XDCE_Image xI, string RaftID)
            {
                var Point = ImageCheck.ImageCheck_Point.FromImage(xI, RaftID);
                return FindRaftID_RR(Point.Plate_Xum, Point.Plate_Yum);
            }

            public static bool RecordDebug = false;

            public ReturnRaft FindRaftID_RR(double fullX, double fullY)
            {
                RefreshPreCalcs(); //Don't need to do this everytime, but for now it is still faster than the previous method

                var u = new UnknownRaft(fullX, fullY);

                int row = FindRow(fullX, fullY);
                int col = FindCol(fullX, fullY);

                string raftID = GetRaftID(row, col);
                var rr = new ReturnRaft(u);

                rr.XOnRaft = FindXOnRaft(fullX, fullY);
                rr.YOnRaft = FindYOnRaft(fullX, fullY);

                rr.MinDistToEdge = FindMinDistToEdge(fullX, fullY);// Min Dist to Edge
                rr.RaftCorners = FindRaftCorners(fullX, fullY, FindXDist(fullX, fullY), FindYDist(fullX, fullY), rr.XOnRaft, rr.YOnRaft);// raft corners
                rr.RaftID = raftID;

                return rr;
            }

            internal double leftVertDist, rightVertDist, topHorDist, bottomHorDist;
            internal Line left, right, top, bottom;
            internal double offset = 0.5;

            public static int GetRow_FromRaftID(string RaftID)
            {
                if (RaftID.Length > 4) return 0;
                return (Convert.ToInt32(RaftID[0]) - 65) * 10 + (Convert.ToInt32(RaftID[1]) - 48);
            }
            public static int GetCol_FromRaftID(string RaftID)
            {
                if (RaftID.Length > 4) return 0;
                return (Convert.ToInt32(RaftID[2]) - 65) * 10 + (Convert.ToInt32(RaftID[3]) - 48);
            }

            private void GetSubdivisions(out int leftVertSubdivisions, out int rightVertSubdivisions, out int topHorSubdivisions, out int bottomHorSubdivisions)
            {
                //Remember the letters are tilted from our perspective, which is why things seem odd
                int llRowNum = GetRow_FromRaftID(LowerLeft.RaftID);
                int ulRowNum = GetRow_FromRaftID(UpperLeft.RaftID);
                int urRowNum = GetRow_FromRaftID(UpperRight.RaftID);
                int lrRowNum = GetRow_FromRaftID(LowerRight.RaftID);

                int llColNum = GetCol_FromRaftID(LowerLeft.RaftID);
                int ulColNum = GetCol_FromRaftID(UpperLeft.RaftID);
                int urColNum = GetCol_FromRaftID(UpperRight.RaftID);
                int lrColNum = GetCol_FromRaftID(LowerRight.RaftID);

                if (ulRowNum < urRowNum) // is Left
                {
                    leftVertSubdivisions = ulColNum - llColNum;
                    rightVertSubdivisions = urColNum - lrColNum;
                    topHorSubdivisions = urRowNum - ulRowNum;
                    bottomHorSubdivisions = lrRowNum - llRowNum;
                }
                else if (ulRowNum > urRowNum) // is Right
                {
                    leftVertSubdivisions = llColNum - ulColNum;
                    rightVertSubdivisions = lrColNum - urColNum;
                    topHorSubdivisions = ulRowNum - urRowNum;
                    bottomHorSubdivisions = llRowNum - lrRowNum;
                }
                else
                {
                    if (ulRowNum < llRowNum) // is Up
                    {
                        leftVertSubdivisions = llRowNum - ulRowNum;
                        rightVertSubdivisions = lrRowNum - urRowNum;
                        topHorSubdivisions = urColNum - ulColNum;
                        bottomHorSubdivisions = lrColNum - llColNum;
                    }
                    else if (ulRowNum > llRowNum) // is Down
                    {
                        leftVertSubdivisions = ulRowNum - llRowNum;
                        rightVertSubdivisions = urRowNum - lrRowNum;
                        topHorSubdivisions = ulColNum - urColNum;
                        bottomHorSubdivisions = llColNum - lrColNum;
                    }
                    else //Is Equal
                    {

                        leftVertSubdivisions = ulRowNum - llRowNum;
                        rightVertSubdivisions = urRowNum - lrRowNum;
                        topHorSubdivisions = ulColNum - urColNum;
                        bottomHorSubdivisions = llColNum - lrColNum;
                    }
                }
            }
            private void GetHorVertDistances(out double leftVertDist, out double rightVertDist, out double topHorDist, out double bottomHorDist)
            {
                //// make horizontal subdivisions according to orientation
                int leftVertSubdivisions, rightVertSubdivisions, topHorSubdivisions, bottomHorSubdivisions;
                GetSubdivisions(out leftVertSubdivisions, out rightVertSubdivisions, out topHorSubdivisions, out bottomHorSubdivisions);

                // y distance between subdivisions
                leftVertDist = (LowerLeft.Y - UpperLeft.Y) / (leftVertSubdivisions);
                rightVertDist = (LowerRight.Y - UpperRight.Y) / (rightVertSubdivisions);

                // x distance between subdivisions
                topHorDist = (UpperRight.X - UpperLeft.X) / (topHorSubdivisions);
                bottomHorDist = (LowerRight.X - LowerLeft.X) / (bottomHorSubdivisions);
            }

            public void RefreshPreCalcs()
            {
                GetHorVertDistances(out leftVertDist, out rightVertDist, out topHorDist, out bottomHorDist);

                left = new Line((LowerLeft.X - offset * bottomHorDist, LowerLeft.Y - offset * leftVertDist),
                                (UpperLeft.X - offset * topHorDist, UpperLeft.Y - offset * leftVertDist));
                right = new Line((LowerRight.X - offset * bottomHorDist, LowerRight.Y - offset * rightVertDist),
                                (UpperRight.X - offset * topHorDist, UpperRight.Y - offset * rightVertDist));
                top = new Line((UpperLeft.X - offset * topHorDist, UpperLeft.Y - offset * leftVertDist),
                              (UpperRight.X - offset * topHorDist, UpperRight.Y - offset * rightVertDist));
                bottom = new Line((LowerLeft.X - offset * bottomHorDist, LowerLeft.Y - offset * leftVertDist),
                                 (LowerRight.X - offset * bottomHorDist, LowerRight.Y - offset * rightVertDist));
            }

            public int FindRow(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                //if (RecordDebug) Debug.Print("Row: " + (leftRowY - rightRowY).ToString("0.0") + " " + up + " " + currentRowLine.m.ToString("0.0"));  //When the slope gets negative . . 

                int currentRow = 0;
                if (!up)
                {
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;    // go to next line
                        leftRowY += leftVertDist;
                        rightRowY += rightVertDist;
                        currentRowLine.setPoints((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));
                    }
                    return currentRow - 1; // return the row
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break; // go to next line
                        leftRowY -= leftVertDist;
                        rightRowY -= rightVertDist;
                        currentRowLine.setPoints((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));
                    }
                    return currentRow; // return the row
                }
            }

            public int FindCol(double x, double y)
            {
                // figure out if we need to go left or right
                double topColX = UpperLeft.X - (offset * topHorDist);
                double bottomColX = LowerLeft.X - (offset * bottomHorDist);
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);
                char d = '\t'; string dNote = right.ToString() + d + topHorDist + d + bottomHorDist + d + topColX.ToString("0.0") + d + bottomColX.ToString("0.0") + d + top.getY(topColX) + d + bottom.getY(bottomColX) + d + currentColLine.m.ToString("0.0") + d + currentColLine.b.ToString("0.0") + d + x + d + y;

                int currentCol = 0;
                if (right)
                {
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        bottomColX += bottomHorDist;
                        currentColLine.setPoints((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));
                    }
                    currentCol--;
                }
                else
                {
                    //double tx = x + (bottomHorDist/2);
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        //tx += bottomHorDist;
                        topColX -= topHorDist;
                        bottomColX -= bottomHorDist;
                        currentColLine.setPoints((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));
                    }
                }
                if (RecordDebug) Debug.Print(currentCol.ToString() + d + dNote);
                return currentCol;
            }

            public double FindXOnRaft(double x, double y)
            {
                // figure out if we need to go left or right
                double topColX = UpperLeft.X - offset * topHorDist;
                double bottomColX = LowerLeft.X - offset * bottomHorDist;
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);
                int currentCol = 0;
                if (right)
                {
                    double topY;
                    double bottomY;
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        topY = top.getY(topColX);
                        bottomColX += bottomHorDist;
                        bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    topColX -= topHorDist;
                    topY = top.getY(topColX);
                    bottomColX -= bottomHorDist;
                    bottomY = bottom.getY(bottomColX);
                    currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));

                    // return the x difference
                    return x - currentColLine.getX(y);
                }
                else
                {
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        topColX -= topHorDist;
                        double topY = top.getY(topColX);
                        bottomColX -= bottomHorDist;
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    // return the row
                    return x - currentColLine.getX(y);
                }
            }

            public double FindYOnRaft(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                int currentRow = 0;
                if (!up)
                {
                    double leftX;
                    double rightX;
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;
                        // go to next line
                        leftRowY = leftRowY + leftVertDist;
                        rightRowY = rightRowY + rightVertDist;
                        leftX = left.getX(leftRowY);
                        rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    leftRowY = leftRowY - leftVertDist;
                    leftX = left.getX(leftRowY);
                    rightRowY = rightRowY - rightVertDist;
                    rightX = right.getX(rightRowY);
                    currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    // return the row
                    return y - currentRowLine.getY(x);
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break;
                        // go to next line
                        leftRowY = leftRowY - leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY - rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    return y - currentRowLine.getY(x);
                }
            }

            public double FindXDist(double x, double y)
            {
                // figure out if we need to go left or right
                int currentCol = 0;
                double topColX = UpperLeft.X - offset * topHorDist;
                double bottomColX = LowerLeft.X - offset * bottomHorDist;
                Line currentColLine = new Line((topColX, top.getY(topColX)), (bottomColX, bottom.getY(bottomColX)));

                bool right = currentColLine.isRight(x, y);

                if (right)
                {
                    while (currentColLine.isRight(x, y))
                    {
                        if (currentCol++ > 500) break;
                        // go to next line
                        topColX += topHorDist;
                        bottomColX += bottomHorDist;
                        double topY = top.getY(topColX);
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }
                    // return the row
                    topColX = topColX - topHorDist;
                    double topY2 = top.getY(topColX);
                    bottomColX = bottomColX - bottomHorDist;
                    double bottomY2 = bottom.getY(bottomColX);
                    Line prevColLine = new Line((topColX, topY2), (bottomColX, bottomY2));

                    return currentColLine.getX(y) - prevColLine.getX(y);
                }
                else
                {
                    while (!currentColLine.isRight(x, y))
                    {
                        if (currentCol-- < -500) break;
                        // go to next line
                        topColX = topColX - topHorDist;
                        double topY = top.getY(topColX);
                        bottomColX = bottomColX - bottomHorDist;
                        double bottomY = bottom.getY(bottomColX);
                        currentColLine.setPoints((topColX, topY), (bottomColX, bottomY));
                    }

                    // return the row
                    topColX = topColX + topHorDist;
                    double topY2 = top.getY(topColX);
                    bottomColX = bottomColX + bottomHorDist;
                    double bottomY2 = bottom.getY(bottomColX);
                    Line nextColLine = new Line((topColX, topY2), (bottomColX, bottomY2));

                    return nextColLine.getX(y) - currentColLine.getX(y);
                }
            }

            public double FindYDist(double x, double y)
            {
                // figure out if we need to go up or down
                double leftRowY = UpperLeft.Y - offset * leftVertDist;
                double rightRowY = UpperRight.Y - offset * rightVertDist;
                Line currentRowLine = new Line((left.getX(leftRowY), leftRowY), (right.getX(rightRowY), rightRowY));

                bool up = !currentRowLine.isBelow(x, y);
                int currentRow = 0;
                if (!up)
                {
                    while (currentRowLine.isBelow(x, y))
                    {
                        if (currentRow++ > 500) break;
                        // go to next line
                        leftRowY = leftRowY + leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY + rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }
                    // return the row
                    leftRowY = leftRowY - leftVertDist;
                    double leftX2 = left.getX(leftRowY);
                    rightRowY = rightRowY - rightVertDist;
                    double rightX2 = right.getX(rightRowY);
                    Line prevRowLine = new Line((leftX2, leftRowY), (rightX2, rightRowY));
                    return currentRowLine.getY(x) - prevRowLine.getY(x);
                }
                else
                {
                    while (!currentRowLine.isBelow(x, y))
                    {
                        if (currentRow-- < -500) break;
                        // go to next line
                        leftRowY = leftRowY - leftVertDist;
                        double leftX = left.getX(leftRowY);
                        rightRowY = rightRowY - rightVertDist;
                        double rightX = right.getX(rightRowY);
                        currentRowLine.setPoints((leftX, leftRowY), (rightX, rightRowY));
                    }

                    leftRowY = leftRowY + leftVertDist;
                    double leftX2 = left.getX(leftRowY);
                    rightRowY = rightRowY + rightVertDist;
                    double rightX2 = right.getX(rightRowY);
                    Line nextRowLine = new Line((leftX2, leftRowY), (rightX2, rightRowY));
                    return nextRowLine.getY(x) - currentRowLine.getY(x);
                }
            }

            public double FindMinDistToEdge(double x, double y)
            {
                double XOnRaft = FindXOnRaft(x, y);
                double YOnRaft = FindYOnRaft(x, y);
                double XDist = FindXDist(x, y);
                double YDist = FindYDist(x, y);
                double[] DistToEdgeArray = { //dist from top, right, bottom, and left edges in array indeces 0,1,2,3
                    Math.Abs(YOnRaft),
                    Math.Abs(XDist - XOnRaft),
                    Math.Abs(YDist - YOnRaft),
                    Math.Abs(XOnRaft)
                };
                double MinDistToEdge = DistToEdgeArray[0];
                //double MinDistToEdge = XOnRaft;
                for (int i = 1; i <= DistToEdgeArray.Length - 1; i++)
                {
                    if (DistToEdgeArray[i] < MinDistToEdge) MinDistToEdge = DistToEdgeArray[i];
                }

                return MinDistToEdge;
            }

            public static KnownRaft ExtrapolateFourthRaft(KnownRaft ll, KnownRaft ul, KnownRaft ur)
            {
                // uncomment this if you want to use the cal rafts stored in this object
                //string llRow = LowerLeft.RaftID;
                //string ulRow = UpperLeft.RaftID;
                //string urRow = UpperRight.RaftID;

                int ulRowNum = GetRow_FromRaftID(ul.RaftID);
                int urRowNum = GetRow_FromRaftID(ur.RaftID);

                // find orientation & find new RaftID
                string RaftID;
                if ((ulRowNum < urRowNum) | (ulRowNum > urRowNum)) // is left or right
                {
                    RaftID = ur.RaftID[0].ToString() + ur.RaftID[1].ToString() + ll.RaftID[2].ToString() + ll.RaftID[3].ToString();
                }
                else
                {
                    // is up or down
                    RaftID = ll.RaftID[0].ToString() + ll.RaftID[1].ToString() + ur.RaftID[2].ToString() + ur.RaftID[3].ToString();
                }

                // do maths to figure out X and Y
                double Ax = ll.X; double Ay = ll.Y;
                double Bx = ul.X; double By = ul.Y;
                double Cx = ur.X; double Cy = ur.Y;

                double Dx = Ax + Cx - Bx;
                double Dy = Ay + Cy - By;

                // return known raft
                KnownRaft lr = new KnownRaft(RaftID, Dx, Dy);
                return lr;
            }

            public static List<Tuple<double, double>> FindRaftCorners(double x, double y, double XDist, double YDist, double XOnRaft, double YonRaft)
            {
                double raftUpperLeftX = x - XOnRaft;
                double raftUpperLeftY = y - YonRaft;
                List<Tuple<double, double>> raftCornersMicrons = new List<Tuple<double, double>>
                    {
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + XDist, raftUpperLeftY),
                        new Tuple<double, double>(raftUpperLeftX + XDist, raftUpperLeftY + YDist),
                        new Tuple<double, double>(raftUpperLeftX, raftUpperLeftY + YDist)
                    };
                return raftCornersMicrons;
            }

            public string GetRaftID(int row, int col)
            {
                int newRow;
                int newCol;


                int llRowNum = GetRow_FromRaftID(LowerLeft.RaftID);
                int ulRowNum = GetRow_FromRaftID(UpperLeft.RaftID);
                int urRowNum = GetRow_FromRaftID(UpperRight.RaftID);
                int lrRowNum = GetRow_FromRaftID(LowerRight.RaftID);

                if (ulRowNum < urRowNum) // is Left
                {
                    newRow = col + (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48);
                    newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) - row;
                }
                else if (ulRowNum > urRowNum) // is Right
                {
                    newRow = (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48) - col;
                    newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) + row;
                }
                else
                {
                    if (ulRowNum < llRowNum) // is Up
                    {
                        newRow = row + (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48);
                        newCol = col + (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48);
                    }
                    else // is Down
                    {
                        newRow = (System.Convert.ToInt32(this.UpperLeft.RaftID[0]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[1]) - 48) - row;
                        newCol = (System.Convert.ToInt32(this.UpperLeft.RaftID[2]) - 65) * 10 + (System.Convert.ToInt32(this.UpperLeft.RaftID[3]) - 48) - col;
                    }
                }

                string rowString = ((char)((int)(newRow / 10) + 65)).ToString() + ((char)(newRow % 10 + 48)).ToString();
                string colString = ((char)((int)(newCol / 10) + 65)).ToString() + ((char)(newCol % 10 + 48)).ToString();

                return rowString + colString;
            }

            public bool Save(string SavePath)
            {
                DateCalibrated = DateTime.Now;
                XmlSerializer x = new XmlSerializer(typeof(CalibrationRafts));
                using (StreamWriter SW = new StreamWriter(SavePath))
                {
                    x.Serialize(SW, this);
                    SW.Close();
                    Dirty = false;
                    return true;
                }
            }

            public bool SaveDefault(string FolderPath)
            {
                string p = Path.Combine(FolderPath, CalibrationFileName);
                return Save(p);
            }

            public string CalibrationFileName { get => CalibrationNameConstruct(Well); }

            public static string CalibrationNameConstruct(string WellLabel = "") { return (CalibrationNamePart + " " + WellLabel).Trim() + "." + CalibrationExtension; }
            public static string CalibrationNamePart = "RaftCalibration";
            public static string CalibrationExtension = "XML";

            /// <summary>
            /// Give this the folder that the XDCE file is in, and it will guess on the file name here.  This is fast, but if it returns null, there may be a calibration of a different name or in a different place
            /// </summary>
            public static CalibrationRafts Load_Assumed(string FolderThatXDCEisIn, string WellLabel = "")
            {
                string PathAssumed = Path.Combine(FolderThatXDCEisIn, CalibrationNameConstruct(WellLabel));
                FileInfo FI = new FileInfo(PathAssumed);
                if (FI.Exists) return Load(PathAssumed);
                return null;
            }

            /// <summary>
            /// Must give the fully qualified path, will not look for a calibration file (use Load_Assumed for that)
            /// </summary>
            public static CalibrationRafts Load(string LoadPath)
            {
                XmlSerializer x = new XmlSerializer(typeof(CalibrationRafts));
                using (FileStream F = new FileStream(LoadPath, FileMode.Open))
                {
                    CalibrationRafts CR = (CalibrationRafts)x.Deserialize(F);
                    F.Close();
                    if (CR.DateCalibrated < new DateTime(2019, 8, 15)) CR.DateCalibrated = new DateTime(2019, 8, 15);

                    if (CR.LowerRight == null && CR.UpperLeft != null)
                    {
                        CR.LowerRight = CalibrationRafts.ExtrapolateFourthRaft(CR.LowerLeft, CR.UpperLeft, CR.UpperRight);
                        CR.CalibrationVersion = "3-Point 01";
                    }

                    foreach (var KR in CR.KnownRafts) KR.Parent = CR; //Reassociate parent 5/24/2022

                    return CR;
                }
            }

            public string CalibrationVersion { get; set; }
            public bool Calibrated { get => KnownRafts.Count == 4; }
            private Random _Rnd = new Random();
            private string _CalCode = "";
            [XmlIgnore]
            public string CalCode
            {
                get
                {
                    if (_CalCode == "")
                    {
                        _CalCode = _Rnd.Next(100000, 999999).ToString();
                    }
                    return _CalCode;
                }
            }

            /// <summary>
            /// This allows the rafts to be refreshed, usually when shifting
            /// </summary>
            public void CalCodeShift()
            {
                _CalCode += "s";
                _CalCode = _CalCode.Replace("sssss", "t");
                _CalCode = _CalCode.Replace("ttttt", "u");
                _CalCode = _CalCode.Replace("uuuuu", "v");
            }

            public static bool OnSameVertAxis(string raftOne, string raftTwo)
            {
                return (raftOne[0] == raftTwo[0] && raftOne[1] == raftTwo[1]);
            }

            public static bool OnSameHorAxis(string raftOne, string raftTwo)
            {
                return (raftOne[2] == raftTwo[2] && raftOne[3] == raftTwo[3]);
            }
        }

        /// <summary>
        /// Known Raft Position, i.e. from the calibration itself
        /// </summary>
        public class KnownRaft
        {
            public KnownRaft()
            {
                // need this to serialize
            }

            public KnownRaft(string RaftID, double x, double y)
            {
                this.RaftID = RaftID;
                this.X = x;
                this.Y = y;
            }

            [XmlIgnore]
            public CalibrationRafts Parent { get; set; }

            public string RaftID { get; set; }
            public double X { get; set; }
            public double Y { get; set; }

            private ReturnRaft _ReturnRaft;

            [XmlIgnore]
            public ReturnRaft ReturnRaft
            {
                get
                {
                    if (_ReturnRaft == null && Parent != null)
                    {
                        _ReturnRaft = Parent.FindRaftID_RR(X, Y);
                    }
                    return _ReturnRaft;
                }
                set { _ReturnRaft = value; }
            }

            [XmlIgnore]
            public object DragPoint { get; set; }
        }

        /// <summary>
        /// Unknown Raft Position
        /// </summary>
        public class UnknownRaft
        {
            public UnknownRaft(double x, double y)
            {
                this.X = x;
                this.Y = y;
            }

            public double X { get; set; }
            public double Y { get; set; }
        }

        /// <summary>
        /// Primary way we work with a raft that came from some part of a calibrated image
        /// </summary>
        public class ReturnRaft
        {
            public ReturnRaft(UnknownRaft u)
            {
                this.U = u;
            }
            public UnknownRaft U { get; set; }
            public string RaftID { get; set; }

            public bool IsFiducial { get => IsRaftAnyFiducial(RaftID); }

            public double XOnRaft { get; set; }
            public double YOnRaft { get; set; }
            public double MinDistToEdge { get; set; }

            //In Microns, convert back to pixels
            public List<Tuple<double, double>> RaftCorners { get; set; }

            public RectangleF RaftCornersFullRect
            {
                get
                {
                    if (RaftCorners == null || RaftCorners.Count < 4)
                        return RectangleF.Empty;
                    return new RectangleF((float)RaftCorners[0].Item1,
                        (float)RaftCorners[0].Item2,
                        (float)(RaftCorners[2].Item1 - RaftCorners[0].Item1),
                        (float)(RaftCorners[2].Item2 - RaftCorners[0].Item2));
                }
            }

            public PointF RaftCenterFull => new PointF((float)(RaftCorners[0].Item1 + RaftCorners[2].Item1) / 2, (float)(RaftCorners[0].Item2 + RaftCorners[2].Item2) / 2);

            public int RaftRowIdx => CalibrationRafts.GetRow_FromRaftID(RaftID);

            public int RaftColIdx => CalibrationRafts.GetCol_FromRaftID(RaftID);

            public static bool IsRaftAnyFiducial(string RaftID)
            {
                return IsRaftFiducialLabels(RaftID) || IsRaftFiducialDot(RaftID);
            }

            public static bool IsRaftFiducialLabels(string RaftID)
            {
                if (RaftID.Length > 4) return false;
                return (RaftID[1] == '0' & RaftID[3] == '0');
            }

            public static bool IsRaftFiducialDot(string RaftID)
            {
                if (RaftID.Length > 4) return false;
                return (RaftID[1] == '5' & RaftID[3] == '0') || (RaftID[1] == '0' & RaftID[3] == '5') || (RaftID[1] == '5' & RaftID[3] == '5');
            }

            public static PointF PointCenterOnImage(SizeF pB, InCellLibrary.XDCE_Image xI, ReturnRaft rR)
            {
                var Ps = PointsFromCornersOnImage(pB, xI, rR);
                if (Ps.Count == 0) return PointF.Empty;

                var P = new PointF((Ps[0].X + Ps[2].X) / 2, (Ps[0].Y + Ps[2].Y) / 2);
                return P;
            }

            public static List<PointF> PointsFromCornersOnImage(SizeF pB, InCellLibrary.XDCE_Image xI, ReturnRaft rR)
            {
                if (rR == null || rR.RaftCorners == null)
                    return new List<PointF>();
                // Project corner points to image space
                List<PointF> Points = new List<PointF>(rR.RaftCorners.Count);
                foreach (Tuple<double, double> pt in rR.RaftCorners)
                    Points.Add(CornerToPoint(pB, xI, pt));
                Points.Add(Points[0]);

                return Points;
            }

            public static RectangleF RectFromCorners(InCellLibrary.XDCE_Image xI, ReturnRaft RR, SizeF Bitmap_size)
            {
                List<PointF> Points = PointsFromCornersOnImage(Bitmap_size, xI, RR);
                float X = Math.Max(0, Points[0].X);
                float Y = Math.Max(0, Points[0].Y);
                float Width = Math.Min(Bitmap_size.Width, Points[2].X) - X;
                float Height = Math.Min(Bitmap_size.Height, Points[2].Y) - Y;
                return new RectangleF(X, Y, Width, Height);
            }

            public static PointF CornerToPoint(SizeF pB, InCellLibrary.XDCE_Image xI, Tuple<double, double> pt)
            {
                // Extract frequently used properties to local variables
                double plateX = xI.PlateX_um;
                double plateY = xI.PlateY_um;
                InCellLibrary.XDCE parent = xI.Parent;
                double pixelWidth = parent.PixelWidth_in_um;
                double pixelHeight = parent.PixelHeight_in_um;
                float imageWidth = parent.ImageWidth_in_Pixels;
                float imageHeight = parent.ImageHeight_in_Pixels;

                // Calculate the relative positions
                float relativeX = (float)((pt.Item1 - plateX) / pixelWidth);
                float relativeY = (float)((pt.Item2 - plateY) / pixelHeight);

                // Scale to PictureBox dimensions
                float scaledX = pB.Width * relativeX / imageWidth;
                float scaledY = pB.Height * relativeY / imageHeight;

                return new PointF(scaledX, scaledY);
            }
            public static PointF Point_from_ImageCheck(ImageCheck.ImageCheck_Point Point, SizeF size)
            {
                float X = Point.PixelX_Fraction * size.Width;
                float Y = Point.PixelY_Fraction * size.Height;
                PointF pRet = new PointF(X, Y);
                return pRet;
            }

            public override string ToString()
            {
                return RaftID;
            }
        }

        /// <summary>
        /// New class 4/2022 by Vinay for 4-point method of calibration
        /// </summary>
        public class Line
        {
            // constructor
            public Line((double, double) pointOne, (double, double) pointTwo)
            {
                // point two is to the right of point one
                if (pointTwo.Item1 > pointOne.Item1)
                {
                    this.pointOne = pointOne;
                    this.pointTwo = pointTwo;
                }
                else
                {
                    this.pointTwo = pointOne;
                    this.pointOne = pointTwo;
                }

                this.m = (this.pointTwo.Item2 - this.pointOne.Item2) / (this.pointTwo.Item1 - this.pointOne.Item1);
                this.b = this.pointOne.Item2 - (this.m * this.pointOne.Item1);
                //this.b = this.pointOne.Item2 - (this.m * pointOne.Item1); //WB Found this bug 9/2022
            }

            // mutator
            public void setPoints((double, double) pointOne, (double, double) pointTwo)
            {
                // point two is to the right of point one
                if (pointTwo.Item1 > pointOne.Item1)
                {
                    this.pointOne = pointOne;
                    this.pointTwo = pointTwo;
                }
                else
                {
                    this.pointTwo = pointOne;
                    this.pointOne = pointTwo;
                }
                this.m = (this.pointTwo.Item2 - this.pointOne.Item2) / (this.pointTwo.Item1 - this.pointOne.Item1);
                this.b = this.pointOne.Item2 - this.m * this.pointOne.Item1;
                //this.b = this.pointOne.Item2 - this.m * pointOne.Item1; //WB Found this bug 9/2022
            }

            // get x
            public double getX(double y)
            {
                return (y - this.b) / this.m;
            }

            // get y
            public double getY(double x)
            {
                return this.m * x + this.b;
            }

            // returns true if line is below a point
            public bool isBelow(double x, double y)
            {
                double bPrime = y - this.m * x;
                return (bPrime > this.b); // line below point
            }

            // is right
            public bool isRight(double x, double y)
            {
                double lineX = this.getX(y);
                return lineX <= x;
            }

            public (double, double) pointOne { get; set; }
            public (double, double) pointTwo { get; set; }
            public double m { get; set; }
            public double b { get; set; }
        }

    }
}
